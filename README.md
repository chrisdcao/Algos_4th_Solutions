---------- BEFORE REVIEW ----------
1. FILES THAT LOOKED INTO SOLUTIONS

- Any files that depend on the solutions will have an "_S" suffix (for example: ex_01_01_01_S.cc) so that viewer could pick the Solution-dependent problem right from the directory (without having to go into the file). Defaultly, these files have all surpassed 8 hours of thinking.

- A comment on top of the file (inside the program, line 1): "// LOOKED INTO SOLUTION" so that viewer could look for the info again without having to escape into the directory


2. FILES THAT COST MORE THAN 8 HOURS (BUT NOT LOOKED INTO SOLUTIONS):

- These files will have a "_>8" suffix (so they don't change the order of the files, for example: ex_01_01_20_>8.cc), so viewer can see what topic is hard for the viewer, what's the viewer's weakness 


3. IN-PROGRESS FILES ( < 8 HOURS, IN PROGRESS, HAVEN'T LOOKED INTO SOLUTIONS)

- These files will have a "_loading" suffix (so viewers don't have to view these files as they are not finished). For example: ex_01_01_28_loading.cc


4. COMPLETED FILES (THINK < 8 HOURS + NOT LOOKED INTO SOLUTIONS)

- These files will have NO prefix and NO suffix in their filenames. For example: ex_01_01_20.cc

5. FILES HAVING MULTIPLE PARTS (THAT HAVE TO BE REPRESENTED IN DIFFERENT FORMAT)

- These files will have 'part_i' right after the name of the exercises. For example: "ex_XX_XX_XX_part_i_other_suffixes_follow"


---------- AFTER REVIEW ----------

1. FILES that are SAME to Solutions, and run CORRECTly:             _review_same_correct
2. FILES that are SAME to Solutions, but run INCORRECTly:           _review_same_incorrect
3. FILES that are DIFFERENT from Solutions, and run CORRECTly:      _review_diff_correct
4. FILES that are DIFFERENT from Solutions, and run INCORRECTly:    _review_diff_incorrect

5. If not understand the Solution after > 2 hours of trial:         _review_stuck
