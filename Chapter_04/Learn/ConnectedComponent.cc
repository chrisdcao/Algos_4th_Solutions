#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <stack>
#include <queue>
#include "Bag.h"

using namespace std;

class Graph {
private:
    int V;
    int E;
    vector<Bag<int>> adjList;
    
public:
    Graph(int V)  {
        this->V = V;
        this->E = 0;
        adjList.resize(V);
    }
    
    void addEdge(int v, int w) {
        this->adjList[v].add(w);
        this->adjList[w].add(v);
        E++; // have to increment each time we form an edge
    }
    
    int getE() { return E; }
    int getV() { return V; }
    
    Bag<int>& adj(int v) { // return all the adjacent vertices of vertex 'v'
        return adjList[v];
    }
    
    // we don't actually store the keyVertice, bởi nó sẽ phải đi dần từ [0, V-1] theo thứ tự nên nó chính là index luôn, không cần phải store
    string toString() {
        ostringstream oss;
        oss << V << " vertices, " << E << " edges " << endl;
        for (int keyVertex = 0; keyVertex < V; keyVertex++) {
            oss << keyVertex << ": ";
            for (const auto& adjVertex : adj(keyVertex))
                oss << adjVertex << " ";
            oss << endl;
        }
        return oss.str();
    }
};

class CC {
private:
    vector<bool> marked;
    vector<int> id;
    int count;
    
public:
    CC(Graph G) {
        marked.resize(G.getV());
        id.resize(G.getV());
        for (int s = 0; s < G.getV(); s++)
            if (!marked[s]) { // with this, we don't have to worry about over-writing a traversed element with new id
                dfs(G, s);
                count++;
            }
    }
    
    void dfs(Graph& G, int v) {
        marked[v] = true;
        // if 2 components are connected, they will have the same id because the count only increment when it's done a whole dfs (as seen above) of a source vertex (so until end of link (no connected Comp) does the count increase)
        id[v] = count; 
        for (auto w : G.adj(v)) 
            if (!marked[w]) 
                dfs(G,w);
    }
    
    bool connected(int v, int w) { return id[v] == id[w]; }
    
    int id(int v) { return id[v]; }
    
    int count() { return count; }
};

int main() {
    int length;
    cin >> length;
    int V, E;
    cin >> V >> E;
    Graph graph(V);
    for (int i = 0; i < length; i++) {
        int v, w;
        cin >> v >> w;
        graph.addEdge(v,w);
    }
    
    CC cc(graph);
    int M = cc.count();
    cout << M << endl;
    
    BagArray<Bag<int>> components;
    for (int v = 0; v < graph.getV(); v++)
        components[cc.id(v)].add(v);
    for (int i = 0; i < M; i++) {
        for (int v : components[i])
            cout << v << " ";
        cout << endl;
    }
}
