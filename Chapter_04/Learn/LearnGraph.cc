#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <vector>
#include "Bag.h"

using namespace std;

class Graph {
private:
    int V;
    int E;
    vector<Bag<int>> adj;
    
public:
    Graph() = default;
    
    Graph(int V) {
        this->V = V;
        this->E = 0;
        adj.resize(V);
    }
    
    //friend istream& operator>>(istream& is, Graph& graph) {
        //is >> graph.V >> graph.E;
        //graph.adj.resize(graph.V);
        //for (int i = 0; i < graph.E; i++) {
            //int v, w;
            //is >> v >> w;
            //graph.addEdge(v,w);
        //}
        //return is;
    //}
    
    void setV(int V) { this->V = V; }
    void setE(int E)  { this->E = E; }
    
    int getV() { return V; }
    int getE() { return E; }
    
    void addEdge(int v, int w) {
        // we have to add twice for each vertex in the edge the complement vertex
        this->adj[v].add(w);
        this->adj[w].add(v);
        E++;
    }
    
    Bag<int>& adjList(int v) {
        return adj[v];
    }

    string toString() {
        ostringstream oss;
        oss << V << " vertices, " << E << "edges\n";
        for (int v = 0; v < V; v++) {
            oss <<  v << ": ";
            for (const auto& w : adjList(v))
                oss <<  w << " ";
            oss << "\n";
        }
        return oss.str();
    }

};

int main() {
    int length;
    cin >> length;
    int V, E;
    cin >> V >> E;
    Graph graph(V);
    graph.setE(E);
    int v, w;
    for (int i = 0; i < length; i++) {
        cin >> v >> w;
        graph.addEdge(v,w);
    }
    cout << endl;
    cout << graph.toString();
}




//int degree(Graph G, int v) {
    //int degree = 0;
    //for (int w : G.adjList(v)) degree++;
    //return degree;
//}

////the maxDegree in graph G
//int maxDegree(Graph G) {
    //int max = 0;
    //for (int v = 0; v < G.getV(); v++)
        //if (degree(G,v) > max)
            //max = degree(G, v);
    //return max;
//}

//int avgDegree(Graph G) {
    ////2 * num of edegs / num of vertices
    //return 2 * G.E() / G.V();
//}

//int numberOfSelfLoops(Graph G) {
    //int count = 0;
    //for (int v = 0; v < G.V(; v++))
        //// nhìn xem đỉnh kề của nó
        //for (int w : G.adjList(v))
            //if (v == w) count++; // nếu là chính nó -> self loop (hơi giống union)
    //return count/2; // because we go throguh 2 same vertices 
//}
