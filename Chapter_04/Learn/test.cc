#include "DataStructures.h"

using namespace std;

int main() {
    Bag<int> bag;
    bag.add(1);
    bag.add(1);
    bag.add(1);
    bag.add(1);
    bag.add(1);
    for (const auto& s : bag)
        cout << s << " ";
    cout << endl;
}
