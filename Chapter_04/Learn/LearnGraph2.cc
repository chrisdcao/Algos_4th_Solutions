#include <string>
#include <stack>
#include "Bag.h"
#include <iostream>
#include <sstream>
#include <numeric>

using namespace std;

class Graph {
private:
    int V;
    int E;
    vector<Bag<int>> adjList;
    
public:
    Graph(int V)  {
        this->V = V;
        this->E = 0;
        adjList.resize(V);
    }
    
    void addEdge(int v, int w) {
        this->adjList[v].add(w);
        this->adjList[w].add(v);
        E++; // have to increment each time we form an edge
    }
    
    int getE() { return E; }
    int getV() { return V; }
    
    Bag<int>& adj(int v) { // return all the adjacent vertices of vertex 'v'
        return adjList[v];
    }
    
    // we don't actually store the keyVertice, bởi nó sẽ phải đi dần từ [0, V-1] theo thứ tự nên nó chính là index luôn, không cần phải store
    string toString() {
        ostringstream oss;
        oss << V << " vertices, " << E << " edges " << endl;
        for (int keyVertex = 0; keyVertex < V; keyVertex++) {
            oss << keyVertex << ": ";
            for (const auto& adjVertex : adj(keyVertex))
                oss << adjVertex << " ";
            oss << endl;
        }
        return oss.str();
    }
};

class DepthFirstSearch {
private:
    vector<bool> marked;
    int count = 0;
    
public:
    DepthFirstSearch() = default;
    
    DepthFirstSearch(Graph G) { marked.resize(G.getV()); }
    
    DepthFirstSearch(Graph& G, int sourceVertex) {
        marked.resize(G.getV());
        dfs(G,sourceVertex);
    }
    
    void dfs(Graph& G, int keyVertex) {
        marked[keyVertex] = true;
        count++; //tổng số đỉnh chúng ta đã visited
        for (auto adjVertex : G.adj(keyVertex)) {
            if (!marked[adjVertex]) {
                cout << "Visiting vertex: "  << adjVertex << endl;
                dfs(G,adjVertex);
            }
        }
    }

    bool getMarked(int w) {
        return marked[w];
    }
    
    int getCount() { 
        return count; 
    }
            
};

class DepthFirstPaths {
private:
    vector<int> edgeTo;
    vector<bool> marked;
    int s;
    
public:
    DepthFirstPaths(Graph& G, int s) {
        edgeTo.resize(G.getV());
        marked.resize(G.getV());
        this->s = s;
        dfs(G, s);
    }
    
    void dfs(Graph& G, int v) {
        marked[v] = true;
        for (auto w : G.adj(v))
            if (!marked[w]) { // if in the adj list of (v) having (w) which haven't been visited, we mark it as edgeTo(w) = v
                edgeTo[w] = v;
                dfs(G, w);
            }
    }
    
    bool hasPathTo(int v) { return marked[v]; }
    
    stack<int> pathTo(int v) {
        if (!hasPathTo(v)) throw runtime_error("pathTo(): there's no path from source to input");
        stack<int> path;
        for (int x = v; x != s; x = edgeTo[x]) 
            path.push(x);
        path.push(s);
        return path;
    }
};

int main() {
    int length;
    cin >> length;
    int V, E;
    cin >> V >> E;
    Graph graph(V);
    for (int i = 0; i < V; i++) {
        int v, w;
        cin >> v >> w;
        graph.addEdge(v,w);
    }
    cout << "Source Vertex: " << 1 << endl;
    DepthFirstSearch dfs(graph,1);
    cout << dfs.getMarked(3) << endl;
    cout << dfs.getCount() << endl;
    
    DepthFirstPaths dfp(graph,1);
    stack<int> path = dfp.pathTo(3);
    // this is not going to be shortest path, it's just simply a possible path, that's all
    cout << "Path to vertex '3' from '1': ";
    while (path.size() > 1) {
        cout << path.top() << "-";
        path.pop();
    }
    cout << path.top() << endl;
    path.pop();
}
