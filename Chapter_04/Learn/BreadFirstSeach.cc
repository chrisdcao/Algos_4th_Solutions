//Breadth First Search is usually for finding the shortest path (since it's nature is spanning around the nearest edges and check the connection)
#include <iostream>
#include <queue>
#include <sstream>
#include <vector>
#include <stack>
#include "Bag.h"

class Graph {
private:
    int V;
    int E;
    vector<Bag<int>> adjList;
public:
    Graph() = default;
    
    Graph(int V) {
        this->V = V;
        this->E = 0;
        adjList.resize(V);
    }
    
    int getV() { return this->V; }
    int getE() { return this->E;}
    
    void addEdge(int v, int w) {
        this->adjList[v].add(w);
        this->adjList[w].add(v);
        E++;
    }
    
    Bag<int>& adj(int v) {
        return adjList[v];
    }
    
    string toString() {
        ostringstream oss;
        oss << V << " vertices, " << E << " edges" << endl;
        for (int v = 0; v < V; v++) {
            oss << v << ": ";
            for (const auto& s : adj(v))
                oss << s << " ";
            oss << endl;
        }
        return oss.str();
    }
};

class BreadthFirstPaths {
private:
    vector<bool> marked;
    vector<int> edgeTo;
    int s;
    
public:
    BreadthFirstPaths(Graph& G, int s) {
        marked.resize(G.getV());
        edgeTo.resize(G.getV());
        this->s = s;
        bfs(G, s);
    }
    
    void bfs(Graph& G, int s) {
        queue<int> q;
        marked[s] = true;
        q.push(s);
        while (!q.empty()) {
            int v = q.front();
            q.pop();
            for (auto w : G.adj(v))
                if (!marked[w]) {
                    edgeTo[w] = v;
                    marked[w] = true;
                    q.push(w);
                }
        }
    }
    
    bool hasPathTo(int v) {
        return marked[v];
    }
    
    stack<int> pathTo(int v) {
        if (!hasPathTo(v)) throw runtime_error("PathTO(v): no path");
        stack<int> path;
        for (int x = v; x != s; x = edgeTo[x])  {// this is how we iterate backward: x moving one edge back
            path.push(x);
        }
        path.push(s);
        return path;
    }
    
};

int main() {
    int length;
    cin >> length;
    int V, E;
    cin >> V >> E;
    Graph graph(V);
    for (int i = 0; i < V; i++) {
        int v, w;
        cin >> v >> w;
        graph.addEdge(v,w);
    }
    
    BreadthFirstPaths dfp(graph,1);
    stack<int> path = dfp.pathTo(3);
    // this is not going to be shortest path, it's just simply a possible path, that's all
    cout << "shortest path to vertex '3' from '1': ";
    while (path.size() > 1) {
        cout << path.top() << "-";
        path.pop();
    }
    cout << path.top() << endl;
    path.pop();
}


