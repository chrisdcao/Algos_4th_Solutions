#include "../Common/Graph.h"
#include <stdio.h>
#include <float.h>
#include <string.h>
#include <limits.h>
#include <assert.h>
#include <algorithm>
#include <stdlib.h>

#define EWD EdgeWeightedDirectedGraph

int N;
char* dummy=nullptr; // dummy for double conversion (that gives back non-double string into this `dummy`)
ssize_t bytes_read=0;

int main() {
	freopen("jobsPC.txt", "r", stdin);

    char* line=nullptr;
    size_t len=0;
    getline(&line,&len,stdin);
    N=strtol(line,&dummy,10);

    // BEGIN CPM
	EWD G(2*(N+1));
    int s=2*N,
        t=2*N+1;
    for (int i=0;i<N;i++) {
        if ((bytes_read=getline(&line,&len,stdin)==-1))  {
            fprintf(stderr,"Invalid input!");
            exit(EXIT_FAILURE);
        }
        char* token=strtok(line," \n");
        double duration=strtod(token,&dummy);
        G.addEdge(Edge(i,i+N,duration));
        G.addEdge(Edge(s,i,0.0));
        G.addEdge(Edge(i+N,t,0.0));

        token=strtok('\0'," \n"); // Tips: parent string will have extra '0' character at end
        int m=strtol(token,&dummy,10);
        for (int j=0;j<m;j++) {
            token=strtok('\0'," \n");
            int precedent=strtol(token,&dummy,10);
            G.addEdge(Edge(i+N,precedent,0.0));
        }
    }
    //printf("Graph:\n%s",G.toString().c_str());
    
    AcyclicLP lp(G,s);
    printf(" job   start  finish\n");
    printf("--------------------\n");
    for (int i = 0; i < N; i++) 
        printf("%4d %7.1f %7.1f\n", i, lp.getCostTo(i), lp.getCostTo(i+N));
    printf("Finish time: %7.1f\n", lp.getCostTo(t));
    //END CPM
    
    /*
     *AcyclicSP lp(G,s);
     *printf("Start times:\n");
     *for (int i=0;i<N;i++)
     *    printf("%4d: %5.1f\n",i,lp.getCostTo(i));
     *printf("%d to %d(%.2f): ",s,t,lp.getCostTo(t));
     *for (auto& e : lp.getPathTo(t))
     *    printf("%d->%d %.2f  ",e.from,e.to,e.cost);
     *printf("\n");
     */

	return (0);
}
