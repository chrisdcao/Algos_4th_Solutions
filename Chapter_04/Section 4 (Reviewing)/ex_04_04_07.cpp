#include "../Common/Graph.h"
#include <time.h>
#include <stdio.h>
#include <queue>

#define THRESHOLD 0.00000001

#define EWD EdgeWeightedDirectedGraph

class Node {
public:
	int id;
	double cost;
	Node(int id, double cost): id(id), cost(cost) {}
	~Node() {}
	bool operator > (const Node& node) const {
		return this->cost-node.cost<=THRESHOLD ? this->id > node.id : this->cost-node.cost>THRESHOLD;
	}
};

class DijkstraSP {
public:
	vector<double> optimalCost;
	vector<Edge*> optimalEdgeTo;
	priority_queue<Node, vector<Node>, greater<Node>> pq;

	vector<double> secondOptimalCost;
	vector<Edge*> secondOptimalEdgeTo;
	priority_queue<Node, vector<Node>, greater<Node>> pq2;
	
	DijkstraSP(EWD& G, int s): optimalCost(G.V), 
							   optimalEdgeTo(G.V), 
							   secondOptimalCost(G.V), 
							   secondOptimalEdgeTo(G.V) {
		fill(optimalCost.begin(), optimalCost.end(), DBL_MAX);
		fill(optimalEdgeTo.begin(), optimalEdgeTo.end(), nullptr);
		optimalCost[s]=0.0;
		pq.push(Node(s,optimalCost[s]));
		while(!pq.empty()) {
			Node node = pq.top();
			pq.pop();
			if (node.cost-optimalCost[node.id]>=THRESHOLD) continue;
			relax(G,node);
		}

		fill(secondOptimalCost.begin(), secondOptimalCost.end(), DBL_MAX);
		fill(secondOptimalEdgeTo.begin(), secondOptimalEdgeTo.end(), nullptr);
		secondOptimalCost[s]=0.0;
		pq2.push(Node(s,secondOptimalCost[s]));
		while (!pq2.empty()) {
			Node node = pq2.top();
			pq2.pop();
			if (node.cost-secondOptimalCost[node.id]>=THRESHOLD) continue;
			relax2(G,node);
		}
	}

	void relax(EWD& G, Node node) {
		for (auto& e : G.adj[node.id]) {
			if (optimalCost[e.to]-(optimalCost[e.from]+e.cost)>THRESHOLD) {
				optimalCost[e.to]=optimalCost[e.from]+e.cost;
				optimalEdgeTo[e.to]=&e;
				pq.push(Node(e.to,optimalCost[e.to]));
			}
		}
	}

	// Find the second shortest path here
	void relax2(EWD& G, Node node) {
		for (auto& e : G.adj[node.id]) {
			if (secondOptimalCost[e.to]-(secondOptimalCost[e.from]+e.cost)>THRESHOLD
			 	&& secondOptimalCost[e.from]+e.cost-optimalCost[e.to]>THRESHOLD) {
				secondOptimalCost[e.to]=secondOptimalCost[e.from]+e.cost;
				secondOptimalEdgeTo[e.to]=&e;
				pq2.push(Node(e.to,secondOptimalCost[e.to]));
			}
		}
	}

	inline bool hasPathTo(int v) { return optimalCost[v] < DBL_MAX; }

	inline bool hasSecondSPTo(int v) { return secondOptimalCost[v] < DBL_MAX; }

	inline double getCostTo(int v) { return optimalCost[v]; }

	inline double getSecondCostTo(int v) { return secondOptimalCost[v]; }

	vector<Edge> getShortestPathTo(int v) {
		vector<Edge> vec;
		if (!hasPathTo(v)) return vec;
		for (Edge* x=optimalEdgeTo[v]; x!=nullptr; x=optimalEdgeTo[x->from])
			vec.push_back(*x);
		reverse(vec.begin(),vec.end());
		return vec;
	}

	vector<Edge> getSecondSPTo(int v) {
		vector<Edge> vec;
		if (!hasSecondSPTo(v)) return vec;
		for (Edge* x=secondOptimalEdgeTo[v]; x!=nullptr; x=secondOptimalEdgeTo[x->from])
			vec.push_back(*x);
		return vec;
	}
};

int V,E,a,b;
double weight;

int main() {
	freopen("tinyEWD2.txt", "r", stdin);

	// time
	clock_t startTime=clock();

	// init
	scanf("%d %d", &V, &E);
	EWD G(V);
	for (int i=0;i<E;i++) {
		scanf("%d %d %lf",&a,&b,&weight);
		G.addEdge(a,b,weight);
	}

	printf("%s", G.toString().c_str());

	// dijkstra
	for (int s=0;s<V;s++) {
		DijkstraSP dijkstra(G,s);
		for (int v=0;v<V;v++) {
			if (dijkstra.hasSecondSPTo(v)) {
				printf("%d to %d (%.2f): ",s,v,dijkstra.getSecondCostTo(v));
				for (auto e : dijkstra.getSecondSPTo(v))
					printf("%d->%d %.2f  ",e.from,e.to,e.cost);
				printf("\n");
			} else
				goto continue_loop;
		}
		if (s!=V-1)
			printf("\n\n");
		else
			printf("\n");
continue_loop:
		continue;
	}

	clock_t endTime=clock();
	printf("Time: %.2f ms", static_cast<double>(endTime-startTime));

	return (0);
}
