#include "../Common/Graph.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#include <queue>

#define EWD EdgeWeightedDirectedGraph

FILE* output=fopen("log.txt","w");
bool isDebug=false;

struct Node {
	int id;
	double cost;
	bool operator>(const Node& node) const {
		return cost<node.cost;
	}
};

/*
 * PROCEDURE findCriticalEdges(graph,source,dest):
 * 		DIJKSTRA(source, dest)
 * 		SET shortestPath <- CALL getPath()
 * 		SET shortestCost <- CALL getCost()
 * 		SET maxDiff <- -INFINITY
 * 		SET shortestPath2 <- {None}
 * 		SET criticalEdge <- None
 * 		FOREACH edge in shortestPath:				|> N
 *			DIJKSTRA W/O EDGE(source, dest, edge)		|> NlogN
 *			newCost <- CALL getNewCost()
 *			SET diff <- newCost - shortestCost
 *			IF diff > maxDiff:
 *				SET criticalEdge <- e
 *				SET maxDiff <- diff
 *				SET shortestPath2 <- CALL getNewPath()
 *		RETURN criticalEdge
 *
 * Time Complexity:  ~O(N^2.logN())
 * Space Complexity: ~O(N)
 */
class CriticalEdges {
public:
	vector<double> minCostTo, minCostTo2;
	vector<Edge*> minEdgeTo,minEdgeTo2;
	priority_queue<Node,vector<Node>,greater<Node>> pq,pq2;
	Edge criticalEdge;		
	double maxDiff;
	vector<Edge> alternateShortestPath;
	
	CriticalEdges(EWD& G, int source, int dest): minCostTo(G.V),minCostTo2(G.V),minEdgeTo(G.V),minEdgeTo2(G.V) {
		fill(minEdgeTo.begin(),minEdgeTo.end(),nullptr);
		dijkstra(G,source,dest);
		
		if (!hasPath(dest)) {
			string message="No path from" + to_string(source) + " to " + to_string(dest);
			throw runtime_error(message);
		}
		
		vector<Edge> shortestPath=getPath(dest);
		maxDiff=-DBL_MAX;
		for (auto& e : shortestPath) {
			dijkstraWithoutEdge(G,source,dest,e);
			
			if (hasPath2(dest)) {
				double diff=abs(minCostTo[dest]-minCostTo2[dest]);
				if (diff > maxDiff) {
					criticalEdge=e;
					maxDiff=diff;
					alternateShortestPath=getPath2(dest);
				}
			} 
		}
		
		if (isDebug) {
			fprintf(output,"minEdgeTo: ");
			for (int i=0;i<G.V;i++) {
				fprintf(output, "%d: ", i);
				if (minEdgeTo[i])
					fprintf(output,"%s, ",minEdgeTo[i]->toString().c_str());
				else
					fprintf(output, "null, ");
			}
			fprintf(output,"\n");
			
			fprintf(output,"minEdgeTo2: ");
			for (int i=0;i<G.V;i++) {
				fprintf(output, "%d: ", i);
				if (minEdgeTo2[i])
					fprintf(output,"%s, ",minEdgeTo2[i]->toString().c_str());
				else
					fprintf(output, "null, ");
			}
			fprintf(output,"\n");
		}		
	}
	
	void dijkstra(EWD& G, int source, int dest) {
		fill(minCostTo.begin(),minCostTo.end(),DBL_MAX);
		minCostTo[source]=0.0;
		pq.push({source,minCostTo[source]});
		while(!pq.empty()) {
			const Node node=pq.top();
			pq.pop();
//			if (node.id==dest) return;
			if (node.cost > minCostTo[node.id]) continue;
			relax(G,node);
		}
 	}
	
	void relax(EWD& G, const Node& node) {
		for (auto& e : G.adj[node.id]) {
			if (minCostTo[e.to]>minCostTo[e.from]+e.cost) {
				minCostTo[e.to]=minCostTo[e.from]+e.cost;
				minEdgeTo[e.to]=&e;
				pq.push({e.to,minCostTo[e.to]});
			}
		}
	}
	
	void dijkstraWithoutEdge(EWD& G, int source, int dest, const Edge& e) {
		fill(minCostTo2.begin(),minCostTo2.end(),DBL_MAX);
		minCostTo2[source]=0.0;
		pq2.push({source,minCostTo2[source]});
		while(!pq2.empty()) {
			const Node node=pq2.top();
			pq2.pop();
//			if (node.id==dest) return;
//			if (node.cost > minCostTo2[node.id]) continue;
			relaxWithoutEdge(G,node,e);
		}
		
		// reset pq
		priority_queue<Node,vector<Node>,greater<Node>> new_pq;
		pq2=new_pq;
 	}
	
	void relaxWithoutEdge(EWD& G, const Node& node, const Edge& e_in) {
		for (auto& e : G.adj[node.id]) {
			if (e==e_in) continue;
			if (minCostTo2[e.to]>minCostTo2[e.from]+e.cost) {
				minCostTo2[e.to]=minCostTo2[e.from]+e.cost;
				minEdgeTo2[e.to]=&e;
				pq2.push({e.to,minCostTo2[e.to]});
			}
		}
	}
	
	inline bool hasPath(const int& v) const { return minCostTo[v] < DBL_MAX; }
	
	vector<Edge> getPath(const int& v) {
		vector<Edge> vec;
		if (!hasPath(v)) return vec;
		for (auto x=minEdgeTo[v];x!=nullptr;x=minEdgeTo[x->from])
			vec.push_back(*x);
		reverse(vec.begin(),vec.end());
		return vec;
	}
	
	inline bool hasPath2(const int& v) const { return minCostTo2[v] < DBL_MAX; }
	
	vector<Edge> getPath2(const int& v) {
		vector<Edge> vec;
		if (!hasPath2(v)) return vec;
		for (auto x=minEdgeTo2[v];x!=nullptr;x=minEdgeTo2[x->from])
			vec.push_back(*x);
		reverse(vec.begin(),vec.end());
		return vec;
	}
	
	inline double getCost(const int& v) const { return minCostTo[v]; }
	
	inline double getCost2(const int& v) const { return minCostTo2[v]; }
	
	~CriticalEdges() {}
	
};

int N,M,a,b;
double c;
int main() {
	auto startTime=clock();
	freopen("tinyEWD.txt", "r", stdin);
	scanf("%d%d",&N,&M);
	EWD G(N);
	for (int i=0;i<M;i++) {
		scanf("%d%d%lf",&a,&b,&c);
		G.addEdge(a,b,c);
	}
	printf("%s\n",G.toString().c_str());
	
	int source=1,dest=5;
	printf("source: %d, dest: %d\n\n",source,dest);
	CriticalEdges ce(G,source,dest);
	
	printf("shortest path: ");
	for (auto& e : ce.getPath(dest))
		printf("%s ",e.toString().c_str());
	printf("\n");
	printf("Curr Cost: %.2f\n",ce.getCost(dest));
	
	printf("\nalternate shortest path: ");
	for (auto& e : ce.getPath2(dest))
		printf("%s ",e.toString().c_str());
	printf("\n");
	printf("Curr Cost: %.2f\n",ce.getCost2(dest));
	
	printf("\nCritical Edge: %s\nCost addition: %.2f\n",ce.criticalEdge.toString().c_str(),ce.maxDiff);
	
	fclose(output);
	auto endTime=clock();
	printf("\nTime: %.2f ms", static_cast<double>((endTime-startTime)*1000/CLOCKS_PER_SEC));
	
	return (0);
}
