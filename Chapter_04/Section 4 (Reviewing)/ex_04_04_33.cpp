#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include <queue>
#include <vector>
#include <algorithm>
#include <time.h>

using namespace std;

int** matrix;
int N;
int dr[8]={1,1,1,0,0,-1,-1,-1};
int dc[8]={-1,0,1,-1,1,-1,0,1};

class Pos {
public:
	int r,c;
	char* buf=new char[8];
	Pos(const int& row, const int& col): r(row), c(col) {}
	~Pos() {
		delete[] buf;
	}
	const char* toCString() {
		sprintf(buf,"%d-%d %d",r,c,matrix[r][c]);
		return buf;
	}
};

struct Node {
	int r;
	int c;
	int cost;
	bool operator>(const Node& node) const {
		return cost > node.cost;
	}
};

class Solution {
public:
	int** distTo;
	Pos*** edgeTo;
	priority_queue<Node,vector<Node>,greater<Node>> pq;
	
	Solution()=default;
	~Solution() {
		delete[] distTo;
		delete[] edgeTo;
	}
	
	void solve() {
		distTo=new int*[N];
		edgeTo=new Pos**[N];
		for (int i=0;i<N;i++) {
			distTo[i]=new int[N];	
			edgeTo[i]=new Pos*[N];
			for (int j=0;j<N;j++) {
				distTo[i][j]=INT_MAX;	
				edgeTo[i][j]=nullptr;
			}
		}
		distTo[0][0]=0.0;
		pq.push({0,0,distTo[0][0]});
		while (!pq.empty()) {
			const Node node=pq.top();
			pq.pop();
			if (node.r==N-1 && node.c==N-1) return;
			if (node.cost > distTo[node.r][node.c]) continue;
			relax(node);
		}
	}
	
	void relax(const Node& node) {
		for (int i=0;i<8;i++) {
			int newR=node.r+dr[i];
			int newC=node.c+dc[i];
			if (isValid(newR,newC)) {
				if (distTo[newR][newC]>distTo[node.r][node.c]+matrix[newR][newC]) {
					distTo[newR][newC]=distTo[node.r][node.c]+matrix[newR][newC];
					edgeTo[newR][newC]=new Pos(node.r,node.c);
					pq.push({newR,newC,distTo[newR][newC]});
				}
			}
		}		
	}
	
	inline bool hasPath() const {
		return distTo[N-1][N-1]<DBL_MAX;
	}
	
	vector<Pos> getSP() {
		vector<Pos> path;
		if (!hasPath()) return path;
		path.push_back(Pos(N-1,N-1));
		for (Pos* x=edgeTo[N-1][N-1];x!=nullptr;x=edgeTo[x->r][x->c])
			path.push_back(*x);
		reverse(path.begin(),path.end());
		return path;
	}
	
	inline int getCost() const {
		return distTo[N-1][N-1];
	}
	
	inline bool isValid(const int& r, const int& c) const {
		return r >= 0 && c >= 0 && r < N && c < N;
	}
};

bool isDebug=true;

int main() {
	auto startTime=clock();
	freopen("ex_04_04_32.txt","r",stdin);
	scanf("%d",&N);
	
	matrix=new int*[N];
	for (int i=0;i<N;i++)
		matrix[i]=new int[N];
		
	for (int i=0;i<N;i++) {
		for (int j=0;j<N;j++) {
			scanf("%d",&matrix[i][j]);
		}
	}
		
	if (isDebug) {
		for (int i=0;i<N;i++) {
			for (int j=0;j<N;j++)
				printf("%d ", matrix[i][j]);
			printf("\n");
		}	
	}
	
	Solution solution;
	solution.solve();
	printf("Shortest path from (%d,%d) to (%d,%d): ",0,0,N-1,N-1);	
	for (auto& p : solution.getSP())
		printf("%s  ",p.toCString());
	printf("\n");
	printf("Total cost: %d\n", solution.getCost());
	delete[] matrix;
	
	auto endTime=clock();
	printf("Time: %.2f ms",double((endTime-startTime)*1000/CLOCKS_PER_SEC));
	return (0);
}
