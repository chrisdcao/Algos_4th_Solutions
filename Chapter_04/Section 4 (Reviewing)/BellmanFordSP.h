#pragma once

#include "../Common/Graph.h"
#include <queue>

#define EWD EdgeWeightedDirectedGraph
#define EWDC EdgeWeightedDirectedCycle

class BellmanFordSP {
public:
	vector<Edge*> edgeTo;
	vector<bool> onQ;
	vector<double> cost;
	int passCnt=0;
	queue<int> q;
	vector<Edge> cycle;

	BellmanFordSP(EWD& G, int s): edgeTo(G.V), onQ(G.V), cost(G.V) {
		fill(edgeTo.begin(), edgeTo.end(), nullptr);
		fill(cost.begin(), cost.end(), DBL_MAX);
		cost[s]=0.0;
		q.push(s);
		onQ[s]=true;
		while (!q.empty()) {
			int v=q.front();
			q.pop();
			onQ[v]=false;
			relax(G,v);
		}
	}
	~BellmanFordSP() {}

	void relax(EWD& G, int v) {
		for (auto& e : G.adj[v]) {
			if (cost[e.to]>=cost[e.from]+e.cost) {
			/* test ex_04_04_05
				if (cost[e.to]==cost[e.from]+e.cost)
					cout << "equal happens" << endl;
			*/
				cost[e.to]=cost[e.from]+e.cost;
				edgeTo[e.to]=&e;
				if (!onQ[e.to]) {
					q.push(e.to);
					onQ[e.to]=true;
				}
			}
			if (++passCnt%G.V==0) {
				if (hasNegativeCycle()) return;
				findNegativeCycle();
			}
		}
	}

	inline bool hasNegativeCycle() const { return !cycle.empty(); }

	void findNegativeCycle() {
		int V=edgeTo.size();
		EWD spt(V);
		for (auto& e : edgeTo) 
			if (e)
				spt.addEdge(*e);
		EWDC cycleFinder(spt);
		cycle=cycleFinder.getCycle();
	}

	inline double getCostTo(int v) const {
		if (hasNegativeCycle()) throw runtime_error("Negative cycle detected! Cannot compute cost!");
		return cost[v];
	}

	inline bool hasPathTo(int v) const {
		return cost[v] < DBL_MAX;
	}

	vector<Edge> getPathTo(int v) {
		vector<Edge> vec;
		if (hasNegativeCycle()) throw runtime_error("Negative cycle detected! Cannot compute path!");
		if (!hasPathTo(v)) return vec;
		for (Edge* x = edgeTo[v]; x!=nullptr; x=edgeTo[x->from])
			vec.push_back(*x);
		reverse(vec.begin(), vec.end());
		return vec;
	}
};

void printSPT(BellmanFordSP& spt, int s) {
	int V=spt.edgeTo.size();
	for (int v=0;v<V;v++) {
		double costV=spt.getCostTo(v);
		cout << s << " to " << v << " (";
		if (costV==DBL_MAX)
			cout << "INF";
		else
			cout << costV;
		cout << "): ";
		for (auto& e : spt.getPathTo(v))
			cout << e.from << "->" << e.to << " " << e.cost << "  ";
		cout << endl;
	}
}

#undef EWD
#undef EWDC
