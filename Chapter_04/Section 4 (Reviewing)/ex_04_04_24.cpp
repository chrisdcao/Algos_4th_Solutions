#include "../Common/Graph.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#include <queue>
#include <unordered_map>
#include <unordered_set>

struct Node {
	int id;
	double cost;
	bool operator>(const Node& node) const { 
		return cost-node.cost>DBL_EPSILON; 
	}
};

class DijkstraMSP {
public:
    Edge** edgeTo=nullptr;
    double* cost=nullptr;
    priority_queue<Node,vector<Node>,greater<Node>> pq;
    int dummyVertex=-1;
    int vertexCnt=-1;

    DijkstraMSP(EWD& G1,const unordered_set<int>& sources): vertexCnt(G1.V) {
        assert(check(sources));
        // create copy of original G
        EWD G2(G1.V+1); 
        copy(G1,G2); 
        dummyVertex=vertexCnt;
        for (auto& source : sources)
            G2.addEdge({dummyVertex,source,0.0});

        // init with dummyVertex value in mind
        cost=new double[vertexCnt+1];
        edgeTo = new Edge*[vertexCnt+1];

        for (int v=0;v<=vertexCnt;v++) {
            edgeTo[v]=nullptr;
            cost[v]=DBL_MAX; // set all of this to MAX as usual
        }

        // dummyVertex will be our parent source
        dijkstra(G2,dummyVertex);
    }

    ~DijkstraMSP() {
        delete[] edgeTo;
        delete[] cost;
    }

    void dijkstra(EWD& G,int s) noexcept {
        cost[s]=0.0;
        pq.push({s, cost[s]});
        while (!pq.empty()) {
            Node node=pq.top();
            pq.pop();
            if (node.cost-cost[node.id]>DBL_EPSILON) continue;
            relax(G,node);
        }
    }

    void relax(EWD& G, const Node& node) noexcept {
        for (auto& e : G.adj[node.id]) {
            if (cost[e.to]-(cost[e.from]+e.cost)>DBL_EPSILON) {
                cost[e.to]=cost[e.from]+e.cost;
                edgeTo[e.to]= new Edge(e.from,e.to,e.cost);
                pq.push({e.to,cost[e.to]});
            }
        }
    }

    void copy(EWD& G1, EWD& G2) {
        for (int v=0;v<G1.V;v++)
            for (auto& e : G1.adj[v])
                G2.addEdge(e);
    }

    inline bool check(int v) const {
        return v >= 0 && v < vertexCnt;
    }

    bool check(const unordered_set<int>& M) const {
        for (auto& elem : M)
            assert(check(elem));
        return true;
    }

    inline double getCostTo(int v) {
        assert(check(v));
        return cost[v];
    }

    inline bool hasPathTo(int v) const {
        assert(check(v));
        return cost[v] < DBL_MAX;
    }

    vector<Edge> getPathTo(int v) const {
        assert(check(v));
        vector<Edge> path;
        if (!hasPathTo(v)) return path;
        for (auto x=edgeTo[v];x!=nullptr;x=edgeTo[x->from])
            if (x->from != dummyVertex && x->to != dummyVertex)
                path.push_back(*x);
        reverse(path.begin(),path.end());
        return path;
    }
};

int a,b,V,E;
double c;
int main() {
	freopen("tinyEWD.txt", "r", stdin);
	scanf("%d%d",&V,&E);
	EWD G(V);
	for (int i=0;i<E;i++) {
		scanf("%d%d%lf",&a,&b,&c);
		G.addEdge(a,b,c);
	}

    unordered_set<int> sources={1,2,3};
	DijkstraMSP dijkstraMSP(G, sources);

	printf("Ommiting dummy vertex %d:\n",dijkstraMSP.dummyVertex);
	for (auto& s : sources) {
		for (int v=0;v<V;v++) {
		    if (sources.count(v)) continue;
			printf("%d to %d",s,v);
			double weight=dijkstraMSP.getCostTo(v);
			(weight==DBL_MAX) ? printf("(INF): ") : printf("(%.2f): ",weight);
			for (auto& e : dijkstraMSP.getPathTo(v)) {
				printf("%d->%d %.2f  ",e.from,e.to,e.cost);
			}
			printf("\n");
		}
	}

	return (0);
}
