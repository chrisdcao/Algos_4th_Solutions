#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int V,E,a,b;
double c;
double** adjMatrix;

int main(int argc, char** argv) {
	printf("Directed Graph Matrix:\n");
	freopen(argv[1], "r", stdin);
	scanf("%d%d",&V,&E);
	adjMatrix=new double*[V];
	for (int i=0;i<V;i++) {
		adjMatrix[i]=new double[V];
		for (int j=0;j<V;j++)
			adjMatrix[i][j]=0;
	}
	for (int i=0;i<E;i++)
		scanf("%d%d%lf",&a,&b,&adjMatrix[a][b]);

	for (int i=0;i<V;i++) {
		for (int j=0;j<V;j++)
			(j<V-1) ? printf("%.2f, ",adjMatrix[i][j]) : printf("%.2f",adjMatrix[i][j]);
		printf("\n");
	}

	printf("Undirected Graph Matrix:\n");
	freopen(argv[1], "r", stdin);
	scanf("%d%d",&V,&E);
	// reset
	for (int i=0;i<V;i++)
		for (int j=0;j<V;j++)
			adjMatrix[i][j]=0;

	for (int i=0;i<E;i++) {
		scanf("%d%d%lf",&a,&b,&c);
		adjMatrix[a][b]=c;
		adjMatrix[b][a]=c;
	}

	for (int i=0;i<V;i++) {
		for (int j=0;j<V;j++)
			(j<V-1) ? printf("%.2f, ",adjMatrix[i][j]) : printf("%.2f",adjMatrix[i][j]);
		printf("\n");
	}

	return (0);
}
