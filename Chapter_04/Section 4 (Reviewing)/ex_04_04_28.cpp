#include "../Common/Graph.h"
#include <stdio.h>
#include <stdlib.h>
#include <vector>

using namespace std;

#define EWD EdgeWeightedDirectedGraph
#define Topological EdgeWeightedTopological

class AcyclicLP2 {
public:
	Edge** edgeTo;
	double* distTo;
	int V;

	AcyclicLP2(EWD& G, int s):V(G.V) {
		assert(check(s));
		edgeTo=new Edge*[G.V];
		distTo=new double[G.V];
		fill(&distTo[0],&distTo[G.V],DBL_MAX);
		memset(edgeTo,0x0,sizeof(Edge*)*G.V);
		distTo[s]=0.0;
		Topological top(G);
		// since the Graph is DAG -> number of edges = V-1 -> iterating V times will be able to relax all vertices
		for (auto& v : top.getOrder())
			relax(G,v);
	}

	void relax(EWD& G, int v) {
		for (auto& e : G.adj[v]) {
			if (distTo[e.to]>distTo[e.from]+e.cost) {
				distTo[e.to]=distTo[e.from]+e.cost;
				edgeTo[e.to]=&e;
			}
		}
	}

	inline bool check(const int& v) const {
		assert(checkSize(V));
		return v < V && v >=0;
	}

	inline double getDistTo(const int& v) const {
		assert(check(v));
		return distTo[v];
	}

	inline bool hasPathTo(const int& v) const {
		assert(check(v));
		return distTo[v] < DBL_MAX;
	}

	vector<Edge> getPathTo(const int& v) {
		vector<Edge> vec;
		if (hasPathTo(v)) return vec;
		for (auto x=edgeTo[v];x!=nullptr;x=edgeTo[x->from])
			vec.push_back(*x);
		reverse(vec.begin(),vec.end());
		return vec;
	}

	inline bool checkSize(const int& vertexcnt) const {
		return vertexcnt>0;
	}
};
#undef Topological

int N,M,a,b;
double c;
int main() {
	freopen("tinyEWD.txt","r",stdin);
	scanf("%d%d",&N,&M);
	EWD G(N);
	for (int i=0;i<M;i++) {
		scanf("%d%d%lf",&a,&b,&c);
		G.addEdge(Edge(a,b,c));
	}

	printf("Debug Graph: %s\n", G.toString().c_str());;

	for (int s=0;s<N;s++) {
		AcyclicLP alp(G,s);
		for (int v=0;v<N;v++) {
			printf("Path %d to %d:  ",s,v);
			if (!alp.hasPathTo(v)) {
				printf("No path!\n");
				continue;
			}
			for (auto& e : alp.getPathTo(v))
				printf("%s  ", e.toString().c_str());
			printf("\n");
		}
		printf("\n");
	}

	return (0);
}
