#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include "../Common/Graph.h"
#include <queue>

using namespace std;

int N,M;
double x1,y3,x2,y2;

#define point Point2D

struct Node {
	point id;
	double cost;
	bool operator>(const Node& node) const {
		return cost > node.cost;
	}
};

class Dijkstra2D {
public:
	unordered_map<point,Edge2D*> edgeTo;
	unordered_map<point,double> distTo;
	priority_queue<Node,vector<Node>,greater<Node>> pq;
	int V=-1;

	Dijkstra2D(EuclideanDigraph& G, const point& s): V(G.V) {
		//printf("%s\n",s.toString().c_str());
		assert(check(s));
		for (auto& pair : G.adj)
			distTo[pair.first]=DBL_MAX;
		distTo[s]=0.0;
		pq.push({s,distTo[s]});
		while(!pq.empty()) {
			Node node=pq.top();
			pq.pop();
			if (node.cost > distTo[node.id]) continue;
			relax(G,node);
		}
	}

	~Dijkstra2D() {}

	void relax(EuclideanDigraph& G, const Node& node) {
		for (auto& e : G.adj[node.id]) {
			if (distTo[e.to]>distTo[e.from]+e.cost) {
				distTo[e.to]=distTo[e.from]+e.cost;
				edgeTo[e.to]=&e;
				pq.push({e.to, distTo[e.to]});
			}
		}
	}

	inline bool check(const point& v) const {
		if (V !=-1)
			return v.x>=0 && v.y>=0 
				&& v.x<V && v.y<V;
		return true;
	}

	inline bool hasPathTo(const point& dest){
		assert(check(dest));
		return distTo[dest] < DBL_MAX;
	}

	vector<Edge2D> getPathTo(const point& dest) {
		vector<Edge2D> path;
		if (!hasPathTo(dest)) return path;
		for (auto x=edgeTo[dest];x!=nullptr;x=edgeTo[x->from])
			path.push_back(*x);
		reverse(path.begin(), path.end());
		return path;
	}
};

int main() {
	freopen("EuclideanGraph.txt","r",stdin);
	scanf("%d%d",&N,&M);
	EuclideanDigraph ED(N);
	for (int i=0;i<M;i++) {
		scanf("%lf%lf%lf%lf",&x1,&y3,&x2,&y2);
		ED.addEdge({x1,y3}, {x2,y2});
	}
	printf("Debug Graph:\n%s\n",ED.toString().c_str());

	point source({1,2});
	point dest({5,5});
	printf("%s to %s:  ", source.toString().c_str(),dest.toString().c_str());
	Dijkstra2D dijkstra2D(ED,source);
	for (auto& e : dijkstra2D.getPathTo(dest))
		printf("%s  ", e.toString().c_str());
	printf("\n");

	return (0);
}
