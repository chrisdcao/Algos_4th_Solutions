#include "../Common/Graph.h"
#include <queue>

#define EWD EdgeWeightedDirectedGraph
#define EWDC EdgeWeightedDirectedCycle

class BellmanFordSP {
public:
	vector<double> cost;
	vector<Edge*> edgeTo;
	vector<bool> onQ;
	vector<Edge> cycle;
	queue<int> q;
	int passCnt=0;

	BellmanFordSP(EWD& G, int s): cost(G.V), edgeTo(G.V), onQ(G.V) {
		fill(edgeTo.begin(), edgeTo.end(), nullptr);
		fill(cost.begin(), cost.end(), DBL_MAX);
		cost[s]=0;
		q.push(s);
		onQ[s]=true;
		while (!q.empty()) {
			int v = q.front();
			q.pop();
			onQ[v]=true;
			relax(G,v);
		}
	}

	~BellmanFordSP() {}

	void relax(EWD& G, int v) {
		for (auto& e : G.adj[v]) {
			if (cost[e.to]>cost[e.from]+e.cost) {
				cost[e.to]=cost[e.from]+e.cost;
				edgeTo[e.to]=&e;
				if (!onQ[e.to]) {
					q.push(e.to);
					onQ[e.to]=true;
				}
			}
			if (++passCnt%G.V==0) {
				if (hasNegativeCycle()) return;
				findNegativeCycle();
			}
		}
	}

	inline double getCostTo(int v) const {
		if (hasNegativeCycle()) throw runtime_error("Negative cycle detected! Cannot compute cost!");
		return cost[v];
	}

	inline bool hasNegativeCycle() const { return !cycle.empty(); }

	void findNegativeCycle() {
		int V=edgeTo.size();
		EWD spt(V);
		for (auto& e : edgeTo)
			if (e) spt.addEdge(*e);
		EWDC cycleFinder(spt);
		cycle = cycleFinder.getCycle();
	}

	inline bool hasPathTo(int v) const {
		return cost[v]<DBL_MAX;
	}

	vector<Edge> pathTo(int v) {
		vector<Edge> path;
		if (hasNegativeCycle()) throw runtime_error("Negative cycle detected! Cannot compute path!");
		if (!hasPathTo(v)) return path;
		for (Edge* x = edgeTo[v]; x!=nullptr;x=edgeTo[(*x).from])
			path.push_back(*x);
		reverse(path.begin(), path.end());
		return path;
	}
};

void deleteVertex(EWD& G, int vertex) {
	for (int v=0;v<G.V;v++) {
		if (v==vertex) continue;
		for (unsigned int i=0;i<G.adj[v].size();i++) {
			auto e = G.adj[v][i];
			if (e.to==vertex)
				G.adj[v].erase(G.adj[v].begin()+i);
		}
	}
	G.adj[vertex].clear();
}

EWD reverseGraph(EWD& G) {
	EWD newG(G.V);
	for (auto& edgesList : G.adj)
		for (auto& e : edgesList)
			newG.addEdge(e.to,e.from,e.cost);
	return newG;
}

void printSPT(BellmanFordSP& spt) {
	int V=spt.edgeTo.size();
	for (int v=0;v<V;v++) {
		double costV=spt.getCostTo(v);
		cout << "0 to " << v << " (";
		if (costV==DBL_MAX)
			cout << "INF";
		else
			cout << costV;
		cout << "): ";
		for (auto& e : spt.pathTo(v))
			cout << e.from << "->" << e.to << " " << e.cost << "  ";
		cout << endl;
	}
}

int a,b,V,E;
double c;

int main() {
	freopen("tinyEWD.txt", "r", stdin);
	cin >> V>>E;
	EWD G(V);
	for (int i=0;i<E;i++) {
		cin >> a >> b >> c;
		G.addEdge(a,b,c);
	}

	// Original 
	cout << "original parent link:\n";
	BellmanFordSP spt(G,0);
	printSPT(spt);
	cout << '\n';

	// Delete `7`
	cout << "Delete vertex `7`:\n";
	deleteVertex(G,7);
	BellmanFordSP spt2(G,0);
	printSPT(spt2);
	cout << '\n';

	// Reverse
	cout << "Reverse graph:\n";
	auto R = reverseGraph(G);
	BellmanFordSP spt3(R,0);
	printSPT(spt3);
	cout << '\n';

	return (0);
}
