#include "../Common/Graph.h"
#include <queue>

#define THRESHOLD 0.0000001

class Node {
public:
	int id;
	double cost;

	Node(int id, double cost): id(id), cost(cost) {}
	~Node() {}

	bool operator>(const Node& node) const {
		return cost-node.cost<=THRESHOLD ? id>node.id : cost-node.cost>THRESHOLD;
	}
};

class Dijkstra {
public:
	vector<double> cost;
	vector<Edge*> edgeTo;
	priority_queue<Node, vector<Node>, greater<Node>> pq;
	int s;

	Dijkstra(EWD& G, int s): cost(G.V), edgeTo(G.V), s(s) {
		fill(cost.begin(), cost.end(), DBL_MAX);
		fill(edgeTo.begin(), edgeTo.end(), nullptr);
		cost[s]=0.0;
		pq.push(Node(s, cost[s]));
		while (!pq.empty()) {
			Node node = pq.top();
			pq.pop();
			if (node.cost - cost[node.id]>THRESHOLD) continue;
			cout << "Pop " << node.id << " from visiting ";
			if (edgeTo[node.id])
				cout << edgeTo[node.id]->from;
			else
				cout << "NONE";
			cout << '\n';
			relax(G,node);
		}
		cout << endl;
	}

	void relax(EWD& G, const Node& node) {
		for (auto& e : G.adj[node.id]) {
			cout << "Is cost (" << s << "-" << e.to << "): ";
			if (cost[e.to]==DBL_MAX)
				cout << "INF";
			else
				cout << cost[e.to];
			cout << " > (" << s << "-" << e.from << ") + (" << e.from << "-" << e.to << "): ";
			auto output=cost[e.from]+e.cost;
			cout << output << "?: ";
			if (cost[e.to]-(cost[e.from]+e.cost)>THRESHOLD) {
				cout << "true. Replace!";
				cost[e.to]=cost[e.from]+e.cost;
				edgeTo[e.to]=&e;
				pq.push(Node(e.to, cost[e.to]));
			} else
				cout << "false.";
			cout << endl;
		}
	}

	inline bool hasPathTo(int v) const {
		return cost[v] < DBL_MAX;
	}

	vector<Edge> getPathTo(int v) {
		vector<Edge> path;
		if (!hasPathTo(v)) return path;
		for (Edge* x=edgeTo[v]; x!=nullptr; x=edgeTo[(*x).from])
			path.push_back(*x);
		reverse(path.begin(), path.end());
		return path;
	}
};

int V,E,a,b;
double c;
int main() {
	freopen("tinyEWD2.txt", "r", stdin);
	cin >> V >> E;
	EWD G(V);
	for (int i=0;i<E;i++) {
		cin >> a >> b >> c;
		G.addEdge(a,b,c);
	}
	//cout << G.toString() << endl;

	int s = 0;
	Dijkstra dijkstra(G,s);
	for (int v=0;v<V;v++) {
		cout << s << " to " << v << ": ";
		for (auto& e : dijkstra.getPathTo(v))
			cout << e.from << "->" << e.to << " " << e.cost << "  ";
		cout << endl;
	}

	return (0);
}
