#include <iostream>
#include <float.h>
#include <sstream>

using namespace std;

class EWD {
public:
	int V;
	int E;
	double** adjMatrix;

	EWD(int V):V(V), E(0) {
		adjMatrix=new double*[V];
		for (int v=0;v<V;v++) {
			adjMatrix[v]=new double[V];
			fill(&adjMatrix[v][0], &adjMatrix[v][V], DBL_MAX);
		}
	}

	~EWD() {
		for (int i = 0; i<V;i++) {
			delete adjMatrix[i];
			adjMatrix[i]=nullptr;
		}
		delete adjMatrix;
		adjMatrix=nullptr;
	}

	inline void addEdge(int u, int v, double cost) {
		adjMatrix[u][v]=cost;
		E++;
	}

	string toString() const {
		ostringstream oss;
		for (int v=0;v<V;v++) {
			oss << v << ": ";
			for (int w=0;w<V;w++) 
				if (adjMatrix[v][w] != DBL_MAX)
					oss << w << "-" << adjMatrix[v][w] << ", ";
			oss << endl;
		}
		return oss.str();
	}

	inline int getE() const { return E; }
	inline int getV() const { return V;}
};

int a,b,V,E;
double c;

int main() {
	freopen("tinyEWD.txt", "r", stdin);
	cin >> V >> E;
	EWD G(V);
	for (int i =0;i<E;i++) {
		cin >> a >> b >> c;
		G.addEdge(a,b,c);
	}
	cout << G.toString() << endl;

	return (0);
}
