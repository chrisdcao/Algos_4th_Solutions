#include "../Common/Graph.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include <float.h>
#include <queue>
#include <unordered_set>

#define EWD EdgeWeightedDirectedGraph

struct Node {
    int id;
    double cost;
    bool operator>(const Node& node) const {
        return cost - node.cost > DBL_EPSILON;
    }
};

class SPTwoSubsets{
public:
    priority_queue<Node,vector<Node>,greater<Node>> pq;
    double* cost=nullptr;
    Edge** edgeTo=nullptr;
    int V;
    int sp_to=-1;

    SPTwoSubsets(EWD& G, const unordered_set<int>& set1, const unordered_set<int>& set2): V(G.V) {
        // init
        cost=new double[G.V];
        edgeTo = new Edge*[G.V];
        memset(edgeTo,0x0,sizeof(Edge*)*V);
        for (int v=0;v<V;v++)
            cost[v]=DBL_MAX;

        dijkstra(G,set1,set2);
    }

    ~SPTwoSubsets() {
        delete[] cost;
        delete[] edgeTo;
    }

    void dijkstra(EWD& G, const unordered_set<int>& set1, const unordered_set<int>& set2) {
        // ensure set1 vertices are explored
        for (auto& elem : set1) {
            cost[elem]=0.0;
            pq.push({elem,cost[elem]});
        }

        while (!pq.empty()) {
            Node node=pq.top();
            pq.pop();
            if (set2.count(node.id)) {
                sp_to=node.id;
                // since PQ always put min node on top -> first node pop from here (that's in set 2) will be `to` vertex of the shortest path -> return
                return; 
            }
            if (node.cost-cost[node.id]>DBL_EPSILON) continue;
            relax(G,node);
        }
    }

    void relax(EWD& G, const Node& node) {
        for (auto& e : G.adj[node.id]) {
            if (cost[e.to]-(cost[e.from]+e.cost)>DBL_EPSILON) {
                cost[e.to]=cost[e.from]+e.cost;
                edgeTo[e.to]=&e;
                pq.push({e.to,cost[e.to]});
            }
        }
    }

    vector<Edge> getPath() {
        vector<Edge> vec;
        for (auto x=edgeTo[sp_to];x!=nullptr;x=edgeTo[x->from])
            vec.push_back(*x);
        if (!vec.empty())
            reverse(vec.begin(),vec.end());
        return vec;
    }

    inline double getCost() const {
        double totalCost=0.0;
        for (auto x=edgeTo[sp_to];x!=nullptr;x=edgeTo[x->from])
            totalCost+=x->cost;
        return totalCost;
    }
};

int a,b,V,E;
double c;

int main() {
	freopen("tinyEWD.txt", "r", stdin);
	scanf("%d%d",&V,&E);
	EWD G(V);
	for (int i=0;i<E;i++) {
		scanf("%d%d%lf",&a,&b,&c);
		G.addEdge(a,b,c);
	}

    unordered_set<int> set1={0,1};
    unordered_set<int> set2={5,6};

	SPTwoSubsets spTwoSubsets(G,set1,set2);

    vector<Edge> path=spTwoSubsets.getPath();

    if (path.empty())
        printf("There's no path from set1->set2\n");
    else {
        printf("Shortest path from set1->set2(%.2f):  ",spTwoSubsets.getCost());
        for (auto& e : path)
            printf("%s  ",e.toString().c_str());
        printf("\n");
    }

	return (0);
}
