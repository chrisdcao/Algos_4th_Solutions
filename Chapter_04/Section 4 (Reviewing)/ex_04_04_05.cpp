#include "BellmanFordSP.h"

#define EWD EdgeWeightedDirectedGraph
#define BFSP BellmanFordSP

int V,E,a,b;
double c;

int main() {
	freopen("tinyEWD2.txt", "r", stdin);
	cin >> V >> E;
	EWD G(V);
	for (int i=0;i<E;i++) {
		cin >> a >> b >> c;
		G.addEdge(a,b,c);
	}
	cout << "G:\n";
	cout << G.toString() << endl;

	BFSP spt(G,2);
	cout << "SPT:\n";
	printSPT(spt, 2);

	return (0);
}
