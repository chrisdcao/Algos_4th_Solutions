#include "../Common/Graph.h"
#include <stdio.h>
#include <string.h>

#define EWDC EdgeWeightedCycleFinder
#define EWDT EdgeWeightedTopological

class EdgeWeightedCycleFinder {
public:
	bool* marked;
	bool* onStack;
	Edge* edgeTo;
	vector<Edge> cycle;

	EdgeWeightedCycleFinder(EWD& G) {
		marked = new bool[G.V];
		edgeTo= new Edge[G.V];
		onStack = new bool[G.V];
		for (auto v=0;v<G.V;v++)
			if (!marked[v])
				dfs(G,v);
	}

	~EdgeWeightedCycleFinder() { 
		delete[] marked;
		delete[] onStack;
		delete[] edgeTo;
	}

	void dfs(EWD& G, int v) {
		marked[v]=true;
		onStack[v]=true;
		for (auto& e : G.adj[v]) {
			if (hasCycle()) return;
			if (!marked[e.to]) {
				edgeTo[e.to]=e;
				dfs(G,e.to);
			} else if (onStack[e.to])
				recordCycle(e);
		}
		onStack[v]=false;
	}

	inline bool hasCycle() { return !cycle.empty(); }

	void recordCycle(const Edge& e) {
		for (Edge x=e;x.from!=e.to;x=edgeTo[x.from])
			cycle.push_back(x);
		reverse(cycle.begin(), cycle.end());
	}

	inline vector<Edge>& getCycle() { return cycle; }
};

class EdgeWeightedDFO {
public:
	bool* marked;
	int* preorderCnt;
	int* postorderCnt;
	vector<int> preorder;
	vector<int> postorder;
	vector<int> reversepostorder;

	EdgeWeightedDFO(EWD& G) {
 	 	marked = new bool[G.V];
 	 	preorderCnt=new int[G.V];
 	 	postorderCnt=new int[G.V];

		for (int v=0;v<G.V;v++)
			if (!marked[v])
				dfs(G,v);

		reverse(reversepostorder.begin(),reversepostorder.end());
	}

	~EdgeWeightedDFO() {
		delete[] preorderCnt;
		delete[] postorderCnt;
		delete[] marked;
	}

	void dfs(EWD& G, int v) {
		marked[v]=true;

		preorderCnt[v]+=1;
		preorder.push_back(v);

		for (auto& e : G.adj[v]) {
			if (!marked[e.to]) 
				dfs(G,e.to);
		}

		postorder.push_back(v);
		postorderCnt[v]+=1;

		reversepostorder.push_back(v);
	}

	inline int pre(int v) { return preorderCnt[v]; }
	inline int post(int v) { return postorderCnt[v]; }

	inline vector<int>& getPreOrder() { return preorder; }
	inline vector<int>& getPostOrder() { return postorder; }
	inline vector<int>& getReversePostOrder() { return reversepostorder; }
};

class EdgeWeightedTopological {
public:
	vector<int> order;
	int* rank=nullptr;

	EdgeWeightedTopological(EWD& G) {
		EWDC cycleFinder(G);
		if (!cycleFinder.hasCycle()) {
			EdgeWeightedDFO dfo(G);
			order= dfo.getReversePostOrder();

			rank=new int[G.V];
			memset(rank,0,sizeof(int)*G.V);
			for (auto& v : order)
				rank[v]+=1;
		}
	}

	~EdgeWeightedTopological() { delete[] rank; }

	inline bool hasTopology() { return !order.empty(); }

	inline bool isDAG() { return hasTopology(); }

	inline vector<int>& getOrder() { return order; }
	
	inline int getRank(int v) { 
		if (rank==nullptr) 
			throw runtime_error("Has Cycle! Cannot find rank"); 
		return rank[v];
	}
};

int V,E,a,b;
double weight;

int main() {
	freopen("tinyEWD.txt", "r", stdin);
	scanf("%d%d",&V,&E);
	EWD G(V);
	for (int i=0;i<E;i++) {
		scanf("%d%d%lf",&a,&b,&weight);
		G.addEdge(a,b,weight);
	}
	printf("Debug Graph: %s\n",G.toString().c_str());

	EWDT ewdt(G);
	printf("ewdt.getOrder(): ");
	for (auto& v : ewdt.getOrder())
		printf("%d ", v);
	printf("\n\n");

	for (int v=0;v<V;v++)
		printf("ewdt.rank(%d): %d\n",v,ewdt.getRank(v));
	printf("\n");

	return (0);
}
