#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../Common/Graph.h"

#define EWDAM EWDAdjMatrix

struct Node {
    int id;
    double cost;
    bool operator>(const Node& node) const {
        return cost > node.cost;
    }
};

class Dijkstra {
public:
    double* cost;
    Edge** edgeTo;
    bool* marked;
    int V;

    Dijkstra(EWDAM& G, int s): V(G.V) {
        cost=new double[V];
        edgeTo=new Edge*[V];
        marked=new bool[V];
        fill(&cost[0],&cost[V],DBL_MAX);
        memset(edgeTo,0x0,sizeof(Edge*)*V);
        memset(marked,0,sizeof(bool)*V);
        cost[s]=0.0;
        for (int i=0;i<G.V;i++) {
            // finding min with marked[] -> we have to init the starting search value independently 
            // (!= take 1st value from array then search from that value and compare (this approach only work w/o marked))
        	int minVertex=-1;
        	double minWeight=DBL_MAX;
            for (int j=0;j<G.V;j++) {
            	if (marked[j]) continue;
            	//printf("cost[%d]: %.2f. current min: %.2f\n",j,cost[j],minWeight);
            	if (cost[j]<minWeight) {
            		minVertex=j;
            		minWeight=cost[j];
            	}
            }
            if (minVertex==-1) return;
            //printf("minVertex: %d\n",minVertex);
            relax(G,{minVertex,cost[minVertex]});
        }
    }

    ~Dijkstra() {
        delete[] cost;
        delete[] marked;
        delete[] edgeTo;
    }

    void relax(EWDAM& G, Node node) {
    	marked[node.id]=true;

        for (int i =0;i<G.V;i++) {
            if (G.adjMatrix[node.id][i]!=DBL_MAX) {
                if (cost[i]-(cost[node.id]+G.adjMatrix[node.id][i])>DBL_EPSILON) {
                    cost[i]=cost[node.id]+G.adjMatrix[node.id][i];
                    delete edgeTo[i];
                    edgeTo[i]=new Edge({node.id,i,G.adjMatrix[node.id][i]});
                }
            }
        }
    }

    inline bool hasPathTo(int v) const {
        assert(check(v));
        return cost[v] < DBL_MAX;
    }

    inline double getCostTo(int v) const {
        assert(check(v));
        return cost[v];
    }

    inline bool check(int v) const {
        return v >= 0 && v < V;
    }

    vector<Edge> getPathTo(int v) {
        assert(check(v));
        vector<Edge> path;
        if (!hasPathTo(v)) return path;
        for (auto x=edgeTo[v];x!=nullptr;x=edgeTo[x->from])
            path.push_back(*x);
        reverse(path.begin(),path.end());
        return path;
    }
};

int N,M,a,b;
double c;

int main() {
    freopen("tinyEWD.txt","r",stdin);
    scanf("%d%d",&N,&M);
    EWDAM G(N);
    for (int i=0;i<M;i++) {
        scanf("%d%d%lf",&a,&b,&c);
        G.addEdge(a,b,c);
    }
    
    for (int s=0;s<N;s++) {
        Dijkstra dijkstra(G,s);
        for (int v=0;v<N;v++) {
            if (!dijkstra.hasPathTo(v)) {
                printf("%d to %d: No path\n",s,v);
                continue;
            }
            printf("%d to %d (%.2f):  ",s,v,dijkstra.getCostTo(v));
            for (auto& e : dijkstra.getPathTo(v))
                printf("%d->%d %.2f  ",e.from,e.to,e.cost);
            printf("\n");
        }
        printf("\n");
    }

    return (0);
}
