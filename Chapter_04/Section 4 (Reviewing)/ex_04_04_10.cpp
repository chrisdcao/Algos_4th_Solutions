#include "../Common/Graph.h"
#include <algorithm>
#include <float.h>
#include <stdio.h>
#include <queue>

#define EWG EdgeWeightedUndirectedGraph

struct Node {
	int id;
	double cost;
	bool operator>(const Node& node) const {
		return cost-node.cost>DBL_EPSILON;
	}
};

class DijkstraSP {
public:
	priority_queue<Node,vector<Node>,greater<Node>> pq;
	vector<Edge*> edgeTo;	
	vector<double> cost;

	DijkstraSP(EWG& G, int s): edgeTo(G.V),cost(G.V) {
		fill(edgeTo.begin(),edgeTo.end(),nullptr);
		fill(cost.begin(),cost.end(),DBL_MAX);
		cost[s]=0.0;
		pq.push({s,cost[s]});
		while (!pq.empty()) {
			Node node= pq.top();
			pq.pop();
			if (node.cost-cost[node.id]>DBL_EPSILON) {
				printf("Node %d's cost NOT as optimal as cost[%d] => skip node `%d`\n",node.id,node.id,node.id);
				continue;
			}
			relax(G,node);
		}
	}

	void relax(EWG& G, Node node) {
		printf("visiting node %d adjacents: ",node.id);
		for (auto& e : G.adj[node.id])
			printf("%d ", e.to);
		printf("\n");
		for (auto& e : G.adj[node.id]) {
			if (cost[e.to]-(cost[e.from]+e.cost)>DBL_EPSILON) {
				printf("cost[%d]",e.to);
				(cost[e.to]==DBL_MAX)?printf("=INF"):printf("=%.2f",cost[e.to]);
				printf(">cost[%d]+%.2f=%.2f. Update cost[] + Push `%d` to pq!\n",e.from,e.cost,cost[e.from]+e.cost,e.to);
				cost[e.to]=cost[e.from]+e.cost;
				edgeTo[e.to]=&e;
				pq.push({e.to,cost[e.to]});
			} else
				printf("skip node `%d`\n",e.to);
		}
	}

	inline double getCostTo(int v) { return cost[v]; }

	inline bool hasPathTo(int v) const { return cost[v] < DBL_MAX; }

	vector<Edge> getPathTo(int v) {
		vector<Edge> vec;
		if (!hasPathTo(v)) return vec;
		for (auto x=edgeTo[v]; x!=nullptr; x=edgeTo[x->from])
			vec.push_back(*x);
		reverse(vec.begin(),vec.end());
		return vec;
	}
};

int V,E,a,b;
double weight;

int main() {
	freopen("tinyEWG.txt","r",stdin);
	scanf("%d%d",&V,&E);
	EWG G(V);
	for (int i=0;i<E;i++) {
		scanf("%d%d%lf",&a,&b,&weight);
		G.addEdge(a,b,weight);
	}
	printf("%s\n",G.toString().c_str());

	for (int s=0;s<G.V;s++)  {
		printf("\nFinding from source `%d`:\n", s);
		DijkstraSP dijkstra(G,s);
		for (int v=0;v<G.V;v++) {
			if (!dijkstra.hasPathTo(v)) goto continue_loop;
			printf("%d to %d(%.2f): ",s,v,dijkstra.getCostTo(v));
			for (auto& e : dijkstra.getPathTo(v))
				printf("%d->%d %.2f  ",e.from,e.to,e.cost);
			printf("\n");
		}
continue_loop:
		;
	}

	return (0);
}
