#include "../Common/Graph.h"
#include <stdio.h>
#include <float.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include <queue>
#include <algorithm>

struct Node {
	int id;
	double cost;
	bool operator>(const Node& node) const {
		return cost-node.cost>DBL_EPSILON;
	}
};

class DijkstraSP {
public:
	priority_queue<Node,vector<Node>,greater<Node>> pq;
	vector<Edge*> edgeTo;
	double* cost;

	DijkstraSP(EWD& G, int s): edgeTo(G.V) {
		assert(check(s));
		fill(edgeTo.begin(),edgeTo.end(), nullptr);
		cost=new double[G.V];
		for (int i=0;i<G.V;i++)
			cost[i]=DBL_MAX;
		cost[s]=0.0;
		pq.push({s,cost[s]});
		while (!pq.empty()) {
			Node node=pq.top();
			pq.pop();
			if (node.cost-cost[node.id]>DBL_EPSILON) continue;
			relax(G,node);
		}
	}

	~DijkstraSP() { delete[] cost; }

	void relax(EWD& G, Node node) {
		for (auto& e : G.adj[node.id]) {
			if (cost[e.to]-(cost[e.from]+e.cost)>DBL_EPSILON) {
				cost[e.to]=cost[e.from]+e.cost;
				edgeTo[e.to]=&e;
				pq.push({e.to, cost[e.to]});
			}
		}
	}

	inline double getCostTo(int v) const { return cost[v]; }

	inline bool hasPathTo(int v) const { return cost[v] < DBL_MAX; }

	inline bool check(int v) const { return v < int(edgeTo.size()) && v >=0; }

	vector<Edge> getPathTo(int v) {
		assert(check(v));
		vector<Edge> vec;
		if (!hasPathTo(v)) return vec;
		for (auto x=edgeTo[v];x!=nullptr;x=edgeTo[x->from])
			vec.push_back(*x);
		reverse(vec.begin(), vec.end());
		return vec;
	}
};

int V,E,a,b;
double weight;

int main() {
	freopen("tinyEWD.txt", "r", stdin);
	scanf("%d%d",&V,&E);
	EWD G(V);
	for (int i=0;i<E;i++) {
		scanf("%d%d%lf",&a,&b,&weight);
		if (a==5 && b==7) continue;
		G.addEdge(a,b,weight);
	}
	printf("Debug graph: %s", G.toString().c_str());

	for (int s=0;s<G.V;s++) {
		DijkstraSP dijkstra(G,s);
		for(int v=0;v<G.V;v++) {
			printf("%d to %d",s,v);
			(dijkstra.getCostTo(v)==DBL_MAX) ? printf("(INF): ") : printf("(%.2f): ",dijkstra.getCostTo(v));
			for (auto e :dijkstra.getPathTo(v))
				printf("%d->%d %.2f  ",e.from,e.to,e.cost);
			printf("\n");
		}
		printf("\n");
	}

	return (0);
}
