#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <queue>
#include <limits.h>
#include <float.h>
#include "../Common/Graph.h"

struct Node {
    int id;
    double cost;
    bool operator>(const Node& node) const {
        return cost > node.cost;
    }
};

class DijkstraSourceSink {
public:
    double* cost;
    Edge** edgeTo;
    int V,s,t;
    priority_queue<Node,vector<Node>,greater<Node>> pq;

    DijkstraSourceSink(EWD& G, int s,int t): V(G.V),s(s),t(t) {
        assert(check(s));
        assert(check(t));
        cost=new double[G.V];
        edgeTo=new Edge*[G.V];
        memset(edgeTo,0x0,sizeof(Edge*)*G.V);
        for (int i=0;i<G.V;i++)
            cost[i]=DBL_MAX;
        cost[s]=0.0;
        pq.push({s,cost[s]});

        while (!pq.empty()) {
            Node node=pq.top();
            pq.pop();
            if (node.cost-cost[node.id]>DBL_EPSILON) continue;
            if (node.id==t) return;
            relax(G,node);
        }
    }

    ~DijkstraSourceSink() {
        delete[] cost;
        delete[] edgeTo;
    }

    void relax(EWD& G,Node node) {
        for (auto& e : G.adj[node.id]) {
            if (cost[e.to]-(cost[e.from]+e.cost)>DBL_EPSILON) {
                cost[e.to]=cost[e.from]+e.cost;
                edgeTo[e.to]=&e;
                pq.push({e.to,cost[e.to]});
            }
        }
    }

    inline bool hasPath() const {
        return cost[t] < DBL_MAX;
    }

    inline double getCost() const {
        return cost[t];
    }

    vector<Edge> getPath() {
        vector<Edge> path;
        if (!hasPath()) return path;
        for (auto x=edgeTo[t];x!=nullptr;x=edgeTo[x->from])
            path.push_back(*x);
        reverse(path.begin(),path.end());
        return path;
    }

    inline bool check(int v) const {
        return v >=0 && v < V;
    }
};

int N,M,a,b;
double c;

int main() {
    freopen("tinyEWD.txt","r",stdin);
    scanf("%d%d",&N,&M);
    EWD G(N);
    for (int i=0;i<M;i++) {
        scanf("%d%d%lf",&a,&b,&c);
        G.addEdge(a,b,c);
    }
    
    int source=1,sink=6;
    DijkstraSourceSink dijkstraSourceSink(G,source,sink);
    if (!dijkstraSourceSink.hasPath()) {
        fprintf(stderr,"No path between %d to %d",source,sink);
        exit(EXIT_FAILURE);
    }
    printf("Path %d to %d(%.2f):  ",source,sink,dijkstraSourceSink.getCost());
    for (auto& e : dijkstraSourceSink.getPath())
        printf("%d->%d %.2f  ",e.from,e.to,e.cost);
    printf("\n");

    return (0);
}
