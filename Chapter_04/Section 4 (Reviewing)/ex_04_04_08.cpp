#include "../Common/Graph.h"
#include <stdio.h>
#include <queue>

#define EWD EdgeWeightedDirectedGraph

struct Node {
	int id;
	double cost;
	bool operator>(const Node& node) const {
		return cost==node.cost?id>node.id:cost>node.cost;
	}
};

class DijkstraSP {
public:
	vector<Edge*> edgeTo;
	vector<double> cost;
	priority_queue<Node,vector<Node>,greater<Node>> pq;
	int s;
	int dest;

	DijkstraSP(EWD& G, int s): edgeTo(G.V), cost(G.V), s(s) {
		fill(cost.begin(), cost.end(), DBL_MAX);
		fill(edgeTo.begin(), edgeTo.end(), nullptr);
		cost[s]=0.0;
		pq.push({s,cost[s]});
		while (!pq.empty()) {
			Node node = pq.top();
			pq.pop();
			if (node.cost>cost[node.id]) continue;
			relax(G,node);
		}
	}

	void relax(EWD& G, const Node& node) {
		for (auto& e : G.adj[node.id]) {
			if (cost[e.to]>cost[e.from]+e.cost) {
				cost[e.to]=cost[e.from]+e.cost;
				edgeTo[e.to]=&e;
				pq.push({e.to,cost[e.to]});
			}
		}
	}

	inline double getCostTo(int v) { return cost[v]; }
	
	inline bool hasPathTo(int v) { return cost[v] < DBL_MAX; }

	vector<Edge> getPathTo(int v) {
		vector<Edge> vec;
		if (!hasPathTo(v)) return vec;
		for (auto x=edgeTo[v]; x!=nullptr; x=edgeTo[x->from]) 
			vec.push_back(*x);
		reverse(vec.begin(), vec.end());
		return vec;
	}

	inline double getLongestPath() {
		double maxCost=-DBL_MAX;
		for (unsigned int v=0;v<cost.size();v++)
			if (cost[v]!=DBL_MAX)
				if (cost[v]>maxCost) {
					maxCost=cost[v];
					this->dest=v;
				}
		/* DEBUG PRINT
		for (unsigned int v=0;v<cost.size();v++)
			printf("%.2f ", cost[v]);
		printf("\n");
		*/
		return maxCost;
	}
};

struct ReturnValue {
	double cost;
	int source;
	int dest;
};

ReturnValue getDiameter(EWD& G) {
	double d=-DBL_MAX;
	int dest=-1,source=-1;
	for (int s=0;s<G.V;s++) {
		DijkstraSP dijkstra(G,s);
		auto lp=dijkstra.getLongestPath();
		if (lp > d) {
			d=lp;
			source=s;
			dest=dijkstra.dest;
		}
	}
	return {d,source,dest};
}

int V,E,u,v;
double weight;

int main() {
	freopen("tinyEWD.txt", "r", stdin);
	scanf("%d %d",&V,&E);
	EWD G(V);
	for (auto i=0;i<E;i++) {
		scanf("%d %d %lf",&u,&v,&weight);
		G.addEdge(u,v,weight);
	}
	// printf("Debug Graph:\n%s",G.toString().c_str());
	auto diameterInfo=getDiameter(G);
	DijkstraSP dijkstra(G,diameterInfo.source);
	printf("Diameter of G is path from %d to %d(%.2lf): ",diameterInfo.source,diameterInfo.dest,diameterInfo.cost);
	for (auto& e: dijkstra.getPathTo(diameterInfo.dest))
		printf("%d->%d %.2lf  ",e.from,e.to,e.cost);
	printf("\n");

	return (0);
}
