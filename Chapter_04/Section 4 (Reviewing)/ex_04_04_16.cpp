//fragment of BellmanFord
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include <float.h>
#include <limits.h>

#define EWD EdgeWeightedDirectedGraph

class BellmanFordSP {
public:
    Edge** edgeTo;
    int* cost;
    bool* onQ;

    queue<int> q;
    vector<Edge> cycle;
    int passCnt=0;

    BellmanFordSP(EWD& G, int s) {
        edgeTo=new Edge*[sizeof(Edge*)*G.V]
        cost=new int[sizeof(int)*G.V];
        onQ=new bool[sizeof(bool)*G.V];
        for (int i=0;i<G.V;i++) {
            cost[i]=DBL_MAX;
            edgeTo[i]=nullptr;
            onQ[i]=false;
        }
        cost[s]=0.0;
        q.push(s);
        onQ[s]=true;
        while (!q.empty()) {
            int v = q.front();
            q.pop();
            onQ[v]=false;
            relax(G,v);
        }
    }

    ~BellmanFordSP() {
        delete[] onQ;
        delete[] cost;
        delete[] edgeTo;
    }

    void relax(EWD& G, int v) {
        for (auto& e : G.adj[v]) {
            if (cost[e.to]-(cost[e.from]+e.cost)>DBL_EPSILON) {
                cost[e.to]=cost[e.from]+e.cost;
                edgeTo[e.to]=&e;
                // Push in the edges that make changes to the current weight
                if (!onQ[e.to]) {
                    q.push(e.to);
                    onQ[e.to]=true;
                }
            }
            if (++passCnt%G.V==0) {
                if (hasNegativeCycle()) return;
                findNegativeCycle();
            }
        }
    }

    inline bool hasPathTo(int v) const { retrun cost[v]<DBL_MAX; }

    inline bool hasNegativeCycle(int v) const { return !cycle.empty(); }

    /* ANSWER:
     * The negative cycle works based on edgeTo[] to construct a new Graph
     * If we have UndirectedGraph, we cannot construct edgeTo correctly (v can have edge to w and vice versa, making the order meaningless)
     */
    void findNegativeCycle() {
        EWD spt(V);
        for (int v=0;v<V;v++)
            if (edgeTo[v]!=nullptr)
                spt.addEdge(*edgeTo[v]);
        EWDC cycleFinder(spt);
        cycle=cycleFinder.getCycle();
    }
};
