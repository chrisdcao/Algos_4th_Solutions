#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#include <algorithm>
#include "../Common/Graph.h"
#include <queue>

#define EWDC EdgeWeightedDirectedCycle
#define EWD EdgeWeightedDirectedGraph

class BellmanFordSP {
public:
    Edge** edgeTo;
    bool* onQ;
    double* cost;
    int V=-1;

    queue<int> q;
    vector<Edge> cycle;
    int passCnt=0;

    BellmanFordSP(EWD& G, const int& s): V(G.V) {
        assert(check(s));
        edgeTo=new Edge*[V];
        cost=new double[V];
        onQ=new bool[V];
        memset(onQ,0,sizeof(bool)*V);
        for (int i=0;i<V;i++) {
            cost[i]=DBL_MAX;
            edgeTo[i]=nullptr;
        }
        cost[s]=0.0;
        q.push(s);
        onQ[s]=true;
        while (!q.empty()) {
            int v=q.front();
            q.pop();
            onQ[v]=false;
            relax(G,v);
        }
    }

    ~BellmanFordSP() {
        delete[] edgeTo;
        delete[] onQ;
        delete[] cost;
    }

    void relax(EWD& G, const int& v) noexcept {
        for (auto& e : G.adj[v]) {
            if (cost[e.to]-(cost[e.from]+e.cost)>DBL_EPSILON) {
                cost[e.to]=cost[e.from]+e.cost;
                edgeTo[e.to]=&e;
                if (!onQ[e.to]) {
                    q.push(e.to);
                    onQ[e.to]=true;
                }
            }
            if (++passCnt%V==0) {
                if (hasNegativeCycle()) return;
                findNegativeCycle();
            }
        }
    }

    inline bool check(const int& v) const noexcept { return v<V && v>=0; }

    inline double getCostTo(const int& v) const {
        if (hasNegativeCycle()) throw runtime_error("Has Negative Cycle! Cannot compute cost!");
        assert(check(v));
        return cost[v];
    }

    inline bool hasPathTo(const int& v) const { return cost[v]<DBL_MAX; }

    vector<Edge> getPathTo(const int& v) {
        if (hasNegativeCycle()) throw runtime_error("Has Negative Cycle! Cannot compute path!");
        assert(check(v));
        vector<Edge> vec;
        if (!hasPathTo(v)) return vec;
        for (auto x=edgeTo[v];x!=nullptr;x=edgeTo[(*x).from])
            vec.push_back(*x);
        reverse(vec.begin(),vec.end());
        return vec;
    }

    inline bool hasNegativeCycle() const noexcept { return !cycle.empty(); }

    void findNegativeCycle() noexcept {
        EWD spt(V);
        for (int i=0;i<V;i++)
            if (edgeTo[i]!=nullptr)
                spt.addEdge(*edgeTo[i]);
        EWDC cycleFinder(spt);
        cycle=cycleFinder.getCycle();
    }
};

int a,b,V,E;
double weight;
int main() {
    freopen("tinyEWD.txt", "r", stdin);
    scanf("%d%d",&V,&E);
    EWD G(V);
    for (int i=0;i<E;i++) {
        scanf("%d%d%lf",&a,&b,&weight);
        G.addEdge(a,b,weight);
    }

    for (int s=0;s<V;s++) {
        BellmanFordSP bmf(G,s);
        for (int v=0;v<V;v++) {
            printf("Path from %d to %d(%.2f):  ",s,v,bmf.getCostTo(v));
            for (auto& e:bmf.getPathTo(v))
                printf("%d->%d %.2f  ",e.from,e.to,e.cost);
            printf("\n");
        }
        printf("\n");
    }

    return (0);
}
