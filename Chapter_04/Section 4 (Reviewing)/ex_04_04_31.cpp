#include "../Common/Graph.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <queue>
#include <math.h>
#include <time.h>

using namespace std;

#define UWG EdgeWeightedUndirectedGraph

/*
 * Since all vertices have degree == 2 -> linked list structure
 */
class Solution {
public:
	double* prefixSumCost;
	Edge** edgeTo;
	bool* marked;
	int s;

	Solution() {
	}
	~Solution() {
		delete[] edgeTo;
		delete[] prefixSumCost;
		delete[] marked;
	}
	void solve(UWG& G) {
		edgeTo=new Edge*[G.V];
		prefixSumCost=new double[G.V];
		marked=new bool[G.V];
		memset(edgeTo,0x0,sizeof(Edge*)*G.V);
		memset(marked,0,sizeof(bool)*G.V);
		memset(prefixSumCost,0,sizeof(double)*G.V);
		
		// find either of the endpoint
		for (int i=0;i<G.V;i++)
			if (G.adj[i].size()==1) {
				s=i;
				break;
			}

		// bfs from there
		bfs(G,s);
	}

	// Time Complexity: O(V+E) ~= O(2V) ~= O(V) (since it's acyclic)
	void bfs(UWG& G, const int& s) {
		queue<int> q;
		q.push(s);
		marked[s]=true;
		while(!q.empty()) {
			int v = q.front();
			q.pop();
			for (auto& e : G.adj[v]) {
				int w=e.to;
				if (!marked[w]) {
					marked[w]=true;
					prefixSumCost[w]=prefixSumCost[v]+e.cost;
					q.push(w);
				}
			}
		}
	}

	// Time Complexity: ~O(1)
	double getMinCostFromTo(const int& from, const int& to) {
		return fabs(prefixSumCost[from]-prefixSumCost[to]);
	}
};

int a,b,N,M;
double c;
int main() {
	auto startTime=clock();
	freopen("Line.txt","r",stdin);
	Solution solution;
	scanf("%d%d",&N,&M);
	UWG G(N);
	for (int i=0;i<M;i++) {
		scanf("%d%d%lf",&a,&b,&c);
		G.addEdge(a,b,c);
	}
//	printf("Debug Graph:\n%s\n", G.toString().c_str());

	solution.solve(G);
	for (int i=0;i<N;i++)
		for (int j=0;j<N;j++)
			printf("Cost from %d to %d: %.2f\n",i,j,solution.getMinCostFromTo(i,j));
	printf("\n");
	auto endTime=clock();
	printf("Time: %.2f ms", double((endTime-startTime)*1000/CLOCKS_PER_SEC));	
	return (0);
}
