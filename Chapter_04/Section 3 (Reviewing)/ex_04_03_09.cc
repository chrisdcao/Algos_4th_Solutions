#include "Common.h"
#include "Edge.h"

using namespace std;

class EdgeWeightedGraph {
private:
    int V;
    int E;
    vector<vector<Edge>> adj;
public:
    EdgeWeightedGraph(int V): V(V), E(0), adj(V) {
        // not implemented
    }

// Read Graph from Input Stream
    EdgeWeightedGraph(istream& is) {
        int edgeNum;
        is >> V >> edgeNum;
        EdgeWeightedGraph(V);
        Edge e;
        for (int i = 0; i < edgeNum; i++) {
            is >> e;
            addEdge(e);
        }
    }
// End of Implementation

    void addEdge(Edge e) {
        int v = e.either(), 
            w = e.other(v);
        adj[v].push_back(e);
        adj[w].push_back(e);
        E++;
    }

    vector<Edge> getAdj(int v) { return adj[v]; }

    int getV() { return V; }
    int getE() { return E; }

    vector<Edge> getEdges() {
        vector<Edge> edges;
        for (int v = 0; v < V; v++)
            for (Edge e : getAdj(v))
                if (e.other(v) > v)
                    edges.push_back(e);
        return edges;
    }

    string toString() {
        ostringstream oss;
        for (int v =0; v < V; v++) {
            oss << v << ": ";
            size_t i;
            for (i = 0; i < getAdj(v).size()-1; i++)
                oss << adj[v][i] << ", ";
            oss << adj[v][i] << endl;
        }
        return oss.str();
    }
};

// driver code
int main(int argc, char** argv) {
    if (argc < 2) return -1;
    ifstream input(argv[1]);
    EdgeWeightedGraph G(input);
    cout << G.toString() << endl;

    return (0);
}
