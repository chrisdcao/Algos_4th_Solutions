#include "Common.h"
#include "UnionPathCompression.h"

using namespace std;

class Kruskal {
private:
    vector<Edge> mst;
public:
    Kruskal(EdgeWeightedGraph G) {
        UF uf(G.getV());
        auto pq = make_heap(G.getEdges().begin(), G.getEdges().end());
        while (!pq.empty() && mst.size()<G.getV()) {
            Edge e = pq.top();
            pq.pop();
            int v = e.either(),
                w = e.other(v);
            if (uf.isConnected(v,w)) continue;
            uf.connect(v,w);
            mst.push_back(e);
        }
    }

    vector<Edge> getEdges() { return mst; }

    double getWeight() {
        double totalWeight = 0.0;
        for (Edge e : getEdges()) 
            totalWeight += e.getWeight();
        return totalWeight;
    }
};

