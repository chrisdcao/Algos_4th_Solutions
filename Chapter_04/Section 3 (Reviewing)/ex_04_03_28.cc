#include "Common/GraphCore.h"
#include <queue>
#include <climits>
#include <float.h>
#include <limits>

using namespace std;

#define EWG EdgeWeightedGraph

class LazyPrimMemEff {
public:
    priority_queue<Edge, vector<Edge>, greater<Edge>> pq;
    vector<Edge> mst;
    vector<bool> marked;

    LazyPrimMemEff(EWG& ewg): marked(ewg.getV()) {
        visit(ewg,0);

        while (!pq.empty()) {
            const Edge minEdge=pq.top();
            pq.pop();
            if (marked[minEdge.to] && marked[minEdge.from]) continue;
            mst.push_back(minEdge);
            if (!marked[minEdge.from]) visit(ewg, minEdge.from);
            if (!marked[minEdge.to]) visit(ewg, minEdge.to);
        }
    }

    ~LazyPrimMemEff() {}

    void visit(EWG& ewg, int v) {
        marked[v]=true;
        for (const Edge& e : ewg.getAdj(v))
            if (!marked[e.to])
                pq.push(e);
    }

    vector<Edge>& getMST() { return mst; }

    long double getCost() {
        long double sum=0.0;
        for (auto& e : mst)
            sum += e.cost;
        return sum;
    }
};

int N, M, u,v;
double cost;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    freopen("tinyEWG.txt", "r", stdin);

    // read input && init 
    cin >> N >> M;
    EWG ewg(N); 
    for (int i= 0;i<M;i++) {
        cin >> u >> v >> cost;
        ewg.addEdge(u,v,cost);
    }
    cout << "ewg.toString():\n";
    cout << ewg.toString() << endl;

    // solve
    LazyPrimMemEff lpef(ewg);
    cout << "lpef.getMST():\n";
    for (auto e : lpef.getMST())
        cout << e << " ";
    cout << endl;

    return (0);
}

#undef EWG
