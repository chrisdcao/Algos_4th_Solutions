#ifndef CONSTANTS_H
#define CONSTANTS_H
#include "Common.h"

using namespace std;

static class Constants
{
public:
	static const float UFLOAT_MAX = std::numeric_limits<float>::max();
	static const float UFLOAT_MIN = std::numeric_limits<float>::min();
	static const float FLOAT_INFINITY = numeric_limits<float>::infinity();
	static const int INT_MAX = INT_MAX;
};
#endif