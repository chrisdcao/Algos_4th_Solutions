#include "Common.h"

using namespace std;

class PrimMST {
private:
public:
    PrimMST(EdgeWeightedGraph G): marked(G.getV()),
                                  distTo(G.getV()),
                                  edgeTo(G.getV()),
                                  indexMinPQ(G.getV()) {
        fill(distTo.begin(), distTo.end(), DOUBLE_MAX);
        // since a tree is an acyclic graph
        // we can defaultly set the distance to the tree root = 0.0 (min) since we won't expect any other edge connecting to it (and form a cycle)
        distTo[0] = 0.0;
        indexMinPQ.insert(0, 0.0);
        while (!indexMinPQ.empty()) {
            visit(G, indexMinPQ.delMin());
        }
    }

    void visit(EdgeWeightedGraph G, int v) {
        marked[v] = true;
        for (Edge e : G.getAdj(v)) {
            int w = e.other(v);
            if (!marked[w]) {
                if (e.getWeight() < distTo[w]) {
                    edgeTo[w] = e;
                    distTo[w] = e.getWeight();
                    if (indexMinPQ.contains(w)) 
                        indexMinPQ.change(w, distTo[w]);
                    if (!indexMinPQ.contains(w)) 
                        indexMinPQ.insert(w, distTo[w]);
                }
            }
        }
    }
}; 

int main() {

}
