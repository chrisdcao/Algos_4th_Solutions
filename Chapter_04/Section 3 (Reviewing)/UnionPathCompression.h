#ifndef UNIONPATHCOMPRESSION_H
#define UNIONPATHCOMPRESSION_H

#include <iostream>
#include <vector>

using namespace std;

class UF {
private:
    vector<int> id;
    vector<int> sz;
    int cnt;
public:
    UF(int N): cnt(N) {
        for (int i = 0; i < N; i++) {
            id.push_back(i);
            sz.push_back(1);
        }
    }

    int count() { return cnt; }

    int find(int p) {
        int root = p;
        while (root != id[root]) 
            root = id[root];

        while (p != root) {
            int next = id[p];
            id[p] = root;
            p = next;
        }

        return root;
    }

    bool isConnected(int p, int q) { return find(p) == find(q); }

    void connect(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(q);

        if (pRoot == qRoot) return;

        if (sz[pRoot] >= sz[qRoot]) {
            id[qRoot] = pRoot;
            sz[pRoot] += sz[qRoot];
        } else {
            id[pRoot] = qRoot;
            sz[qRoot] += sz[pRoot];
        }

        cnt--;
    }
};

#endif
