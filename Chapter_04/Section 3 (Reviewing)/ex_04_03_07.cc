#include "Common.h"
#include "IndexMinPQ.h"
#include "Constants.h"
#include "UF.h"
#include "EdgeWeightedGraph.h"

using namespace std;

class LazyPrimMaxMST
{
private:
    vector<Edge> maxMST;
    vector<bool> marked;
    priority_queue<Edge, vector<Edge>, less<int>> maxPQ;

public:

    LazyPrimMaxMST(EdgeWeightedGraph &G) : marked(G.getV())
    {
        visit(G, 0);
        while (!maxPQ.empty())
        {
            Edge e = maxPQ.top();
            maxPQ.pop();
            int v = e.either(),
                w = e.other(v);
            if (marked[v] && marked[w])
                continue;
            maxMST.push_back(e);
            if (!marked[v])
                visit(G, v);
            if (!marked[w])
                visit(G, w);
        }
    }

    void visit(EdgeWeightedGraph G, int v)
    {
        marked[v] = true;
        for (Edge e : G.getAdj(v))
        {
            int w = e.other(v);
            if (!marked[w])
                maxPQ.push(e);
        }
    }

    vector<Edge> getEdges() const
    {
        return maxMST;
    }

    double getWeight() const
    {
        double totalWeight = 0.0;
        for (Edge e : getEdges())
            totalWeight += e.getWeight();
        return totalWeight;
    }
};

class EagerPrimMax
{
private:
    vector<int> distTo;
    vector<Edge> edgeTo;
    vector<bool> marked;
    IndexMaxPQ<float> pq;

public:
    EagerPrimMax(EdgeWeightedGraph G) : distTo(G.getV()), edgeTo(G.getV()), marked(G.getV()), pq(G.getV())
    {
        fill(distTo.begin(), distTo.end(), Constants::UFLOAT_MAX);
        distTo[0] = 0.0;
        pq.insert(0, 0.0);
        while (!pq.isEmpty())
        {
            visit(G, pq.delMax());
        }
    }

    void visit(EdgeWeightedGraph G, int vertex)
    {
        for (Edge e : G.getAdj(vertex))
        {
            int v = e.either(),
                w = e.other(v);
            if (e.getWeight() > distTo[w])
            {
                edgeTo[w] = e;
                distTo[w] = e.getWeight();
                if (pq.containsIndex(w))
                    pq.change(w, distTo[w]);
                else
                    pq.insert(w, distTo[w]);
            }
        }
    }

    vector<Edge> getMaximumSpanningTree() { return edgeTo; }

    float getWeight()
    {
        auto sum = 0.0;
        for (int i = 0; i < edgeTo.size(); i++)
            sum += edgeTo[i].getWeight();
        return sum;
    }
};

class KruskalMaxMST
{
private:
    vector<Edge> maxMST;

public:
    KruskalMaxMST(EdgeWeightedGraph &G)
    {
        vector<Edge> maxPQ = G.getEdges();
        make_heap(maxPQ.begin(), maxPQ.end());
        UF uf(G.getV());
        while (!maxPQ.empty())
        {
            Edge e = maxPQ.front();
            pop_heap(maxPQ.begin(), maxPQ.end());
            maxPQ.pop_back();
            int v = e.either(),
                w = e.other(v);
            if (uf.isConnected(v, w))
                continue;
            uf.connect(v, w);
            maxMST.push_back(e);
        }
    }

    vector<Edge> getEdges() const
    {
        return maxMST;
    }

    double getWeight() const
    {
        double totalWeight = 0.0;
        for (Edge e : getEdges())
            totalWeight += e.getWeight();
        return totalWeight;
    }
};

int main()
{
    int V, E;
    cin >> V >> E;
    EdgeWeightedGraph G(V);
    Edge e;
    for (int i = 0; i < E; i++)
    {
        cin >> e;
        G.addEdge(e);
    }
    cout << G.toString() << endl;

    LazyPrimMaxMST lpmm(G);
    cout << "getWeight(): " << lpmm.getWeight() << endl;
    cout << "print(): \n";
    for (const Edge &e : lpmm.getEdges())
        cout << e << " ";
    cout << endl;

    KruskalMaxMST kmm(G);
    cout << "getWeight(): " << kmm.getWeight() << endl;
    cout << "print(): \n";
    for (const Edge &e : kmm.getEdges())
        cout << e << " ";
    cout << endl;

    return (0);
}
