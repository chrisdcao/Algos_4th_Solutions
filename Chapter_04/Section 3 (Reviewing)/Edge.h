#ifndef EDGE_H
#define EDGE_H

#include "Common.h"

using namespace std;

class Edge {
   private:
    int v;
    int w;
    double weight;

   public:
    Edge() = default;

    Edge(int v, int w, double weight) : v(v), w(w), weight(weight) {
        // not implemented
    }

    double getWeight() const {
        return weight;
    }

    int either() const {
        return v;
    }

    int other(int vertex) const {
        if (vertex == v)
            return w;
        else if (vertex == w)
            return v;
        throw runtime_error("Inconsistent Edge");
    }

    int compareTo(Edge that) {
        if (*this > that)
            return 1;
        else if (*this < that)
            return -1;
        return 0;
    }

    bool operator>(Edge that) {
        return getWeight() > that.getWeight();
    }

    bool operator>(const Edge& that) const {
        return getWeight() > that.getWeight();
    }

    bool operator<(Edge that) {
        return getWeight() > that.getWeight();
    }

    bool operator==(Edge that) {
        return v == that.v && w == that.w;
    }

    bool operator!=(Edge that) {
        return v != that.v && w != that.w;
    }

    bool compareWeight(Edge that) {
        if (weight < that.weight)
            return -1;
        else if (weight > that.weight)
            return 1;
        return 0;
    }

    string toString() const {
        ostringstream oss;
        oss << v << "-" << w << ": " << setprecision(5) << getWeight();
        return oss.str();
    }

    friend ostream& operator<<(ostream& os, const Edge& e) {
        os << e.toString();
        return os;
    }

    friend istream& operator>>(istream& is, Edge& e) {
        is >> e.v >> e.w >> e.weight;
        return is;
    }
};
#endif
