#include "Common.h"

using namespace std;

class LazyPrim {
private:
    vector<bool> marked;
    vector<Edge> mst;
    priority_queue<Edge, vector<Edge>, greater<Edge>> minPQ;
public:
    LazyPrim(EdgeWeightedGraph G): marked(G.getV()) {
        visit(G, 0);
        while (!minPQ.empty()) {
            Edge e = minPQ.top();
            minPQ.pop();
            int v = e.either(), 
                w = e.other(v);
            if (marked[w] && marked[v]) continue;
            mst.push_back(e);
            if (!marked[w]) visit(G, w);
            if (!marked[v]) visit(G, v);
        }
    }

    void visit(EdgeWeightedGraph G, int v) {
        marked[v] = true;
        for (Edge e : G.getAdj(v))
            if (!marked[e.other(v)])
                minPQ.push(e);
    }

    vector<Edge> getEdges() { return mst; }

    double getWeight() { 
        double totalWeight = 0.0;
        for (Edge e : mst)
            totalWeight += e.getWeight();
        return totalWeight;
    }
};

int main() {

}
