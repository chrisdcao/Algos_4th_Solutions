#include "Common.h"

using namespace std;

classd EdgeWeightedGraph {
private:
    vector<vector<double>> adjMatrix;
public:
    EdgeWeightedGraph(int V) {
        adjMatrix.resize(V);
        for (int v = 0; v < V; v++)
            adjMatrix[v].resize(V);
    }

    void addEdge(Edge e) {
        int v = e.either(),
            w = e.other(v);
        if (v == w) return; // disallow parallel edges
        adjMatrix[v][w] = e.getWeight();
        adjMatrix[w][v] = e.getWeight();
        E++;
    }

    vector<Edge> getAdj(int v) {
        vector<Edge> adj;
        for (int col = 0; col < V; col++) {
            if (isValid(adjMatrix[v][col])) {
                Edge edge(row,col,adjMatrix[v][col]);
                adj.push_back(edge);
            }
        }
        return adj;
    }

    int getV() { return V; }
    int getE() { return E; }

    vector<Edge> getEdges() {
        vector<Edge> edges;
        for (int row = 0; row < V; row++) {
            for (int col = row+1; col < V; col++) {
                if (isValid(adjMatrix[row][col])) {
                    Edge edge(row,col,adjMatrix[row][col]);
                    edges.push_back(edge);
                }
            }
        }
        return edges;
    }

    string toString() {
        ostringstream oss;
        for (int row = 0; row < V; row++) {
            for (int col = 0; col < V; col++) {
                if (isValid(adjMatrix[row][col])) {
                    oss << row << "-" << col << ": " << adjMatrix[row][col] << (col == V-1 ? "" : ", ");
                }
            }
            oss << endl;
        }
        return oss.str();
    }

    bool isValid(double value) {
        return value < DOUBLE_MAX;
    }

};
