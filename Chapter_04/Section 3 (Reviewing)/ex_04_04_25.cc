#include "Common/GraphCore.h"

using namespace std;

#define EWG EdgeWeightedGraph

/*
 * worst case happens when the pq is not optimizedly built (so the tree structure is a linear list)
 * this makes the traversal
 *
 */

class LazyPrim {
public:
    priority_queue<Edge, vector<Edge>, greater<Edge>> pq;
    bool* marked;
    Edge* mst;

    LazyPrim(EWG& G) {
        marked=new bool[G.getV()];
        mst=new int[G.getV()-1];
        int count=G.getV()-2;

        visit(G,0);
        while (!pq.empty()) {
            auto e = pq.top();
            pq.pop();
            if (marked[e.to] && marked[e.from]) continue;
            mst[count--] = e;
            if (!marked[e.to]) visit(G, e.to);
            if (!marked[e.from]) visit(G, e.from);
        }
    }
    
    void visit(EWG& G, int v) {
        for (auto e : G.getAdj(v))
            if (!marked[e.to])
                pq.push(e.to);
    }
};

int main() {

}
