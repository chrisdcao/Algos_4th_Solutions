#include "Common.h"

using namespace std;

class PrimMST: public MST {
  private:
      vector<bool> marked;
      queue<Edge> mst;
      priority_queue<Edge,vector<Edge>,greater<Edge>> pq;

      void visit(EdgeWeightedGraph G, int v) {
          // we only mark the source (v), not the destination(w)
          // thus each point is travelled as source (and we will not miss any point)
          marked[v] = true;
          for (Edge e : G.getAdj(v))
              if (!marked[e.other(v)])
                  pq.insert(e);
      }

  public:
      LazyPrimMST(EdgeWeightedGraph G) {
          marked.resize(G.getV());
          visit(G, 0);
          while (!pq.empty()) {
              Edge e = pq.top();
              pq.pop();
              int v = e.either(), 
                  w = e.other(v);
              if (marked[v] && marked[w]) continue;
              mst.push(e);
              if (!marked[v]) visit(G,v);
              if (!marked[w]) visit(G,w);
          }
      }

      vector<Edge>& getEdges() const {
          return mst;
      }

      double getTotalWeight() const {
          int totalWeight = 0;
          queue<Edge> temp = mst;
          while (!temp.empty()) {
            totalWeight += temp.front().getWeight();
            temp.pop();
          }
          return totalWeight;
      }
};

int main() {
    int V, E;
    cin >> V >> E;
    EdgeWeightedGraph ewg(V);
    Edge e;
    for (int i = 0; i < E; i++) {
        cin >> e;
        ewg.addEdge(e);
    }

    cout << ewg.toString() << endl;
    for (Edge e : ewg.getEdges())
        cout << e << endl;

    MST mst(G);
    for (Edge e : mst.getEdges()) 
        cout << e << endl;
    cout << endl;
    cout << mst.weight() << endl;

    return (0);
}
