#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <vector>

using namespace std;

#define Edge DirectedEdge
#define EWD EdgeWeightedDigraph

class DirectedEdge {
public:
    int from;
    int to;
    double cost;

    DirectedEdge(int from, int to, double cost): from(from),to(to),cost(cost) {
        //not impl
    }
    ~DirectedEdge() {
        // not impl
    }

    friend ostream& operator<<(ostream& os, const DirectedEdge& e) {
        os << "("<<e.from << ", " << e.to << "): " << e.cost;
        return os;
    }

    friend istream& operator>>(istream& is, DirectedEdge& e) {
        is >> e.from >> e.to >> e.cost;
        return is;
    }

    bool operator!=(const DirectedEdge& rhs) const {
        return !operator==(rhs);
    }

    bool operator==(const DirectedEdge& rhs) const {
        return from==rhs.from && to==rhs.to && cost==rhs.cost;
    }
    bool operator>(const DirectedEdge& rhs) const {
        if (cost==rhs.cost)
            return from==rhs.from?to>rhs.to:from>rhs.from;
        return cost > rhs.cost;
    }
    bool operator>=(const DirectedEdge& rhs) const {
        return operator==(rhs) || operator>(rhs);
    }
    bool operator<(const DirectedEdge& rhs) const {
        return !operator>=(rhs);
    }
    bool operator<=(const DirectedEdge& rhs) const {
        return !operator>(rhs);
    }
    string toString() const {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }
};

namespace std {
    template<>
    struct hash<DirectedEdge> {
        size_t operator()(const DirectedEdge& e) {
            return ((hash<int>()(e.from) >> 1) << 1)
                  ^((hash<int>()(e.to)>> 1) << 1)
                  ^((hash<double>()(e.cost)>>1)<<1);
        }
    };
}

class EdgeWeightedDigraph {
public:
    vector<vector<Edge>> adj;
    int V;
    int E; 

    EdgeWeightedDigraph(const int& V): adj(V+1), V(V), E(0) { /* *not impl */ }

    ~EdgeWeightedDigraph() { /* not impl */ }

    void addEdge(int u, int v, double cost) {
        adj[u].push_back(Edge(u,v,cost));
        E++;
    }

    vector<Edge>& getAdj(int v) { return adj[v]; }

    vector<Edge>& operator[](int v) { return adj[v]; }

    int getV() { return V; }
    int getE() { return E; }

    string toString() const {
        ostringstream oss;
        for (int v=0;v< V;v++) {
            oss << v << ": ";
            for (auto e : adj[v])
                oss << e << ", ";
            oss << endl;
        }
        return oss.str();
    }
};

class DirectedCycle {
public:
    int* prev;
    vector<int> cycle;
    double* cost;
    bool* marked;
    bool* onStack;

    DirectedCycle(EWD& ewd) {
        cost = new double[ewd.getV()];
        marked=new bool[ewd.getV()];
        onStack=new bool[ewd.getV()];
        prev = new int[ewd.getV()];

        for (int v= 0;v<ewd.getV();v++)
            if (!marked[v])
                dfs(ewd,v);
    }

    void dfs(EWD& ewd, int v) {
        marked[v]=true;
        onStack[v] = true;

        for (auto& e : ewd.getAdj(v)) {
            if (hasCycle()) return;
            if (!marked[e.to]) {
                prev[e.to] = e.from;
                dfs(ewd,e.to);
            } else if (onStack[e.to]) {
                for (int x = e.to; x!=e.from;x=prev[x])
                    cycle.push_back(x);
                cycle.push_back(e.from);
                reverse(cycle.begin(), cycle.end());
            }
        }

        onStack[v] = false;
    }

    bool hasCycle() { return cycle.size()>0; }
    
    vector<int>& getCycle() { return cycle; }
};

#define DFO DepthFirstOrder

class DepthFirstOrder {
public:
    vector<int> pre;
    vector<int> post;
    vector<int> reversePost;
    bool* marked;

    DepthFirstOrder(EWD& ewd) {
        marked=new bool[ewd.getV()];

        for (int v = 0; v < ewd.getV(); v++)
            if(!marked[v])
                dfs(ewd,v);

        for (int i= post.size()-1;i>=0;i--)
            reversePost.push_back(post[i]);
    }

    ~DepthFirstOrder() {
        delete[] marked;
    }

    void dfs(EWD& ewd, int v) {
        marked[v] = true;
        pre.push_back(v);
        for (auto e : ewd.getAdj(v))
            if (!marked[e.to])
                dfs(ewd, e.to);
        post.push_back(v);
    }

    vector<int>& getPre() { 
        return pre; 
    }

    vector<int>& getPost() {
        return post;
    }

    vector<int>& getReversePost() {
        return reversePost;
    }

    int getPre(int v) {
        return pre[v];
    }

    int getPost(int v) {
        return post[v];
    }

    int getReversePost(int v) {
        return reversePost[v];
    }

};
#undef EWD
#undef Edge
#undef DFO

class Edge {
public:
    int from;
    int to;
    double cost;

    Edge(int from, int to, double cost): from(from), to(to), cost(cost) {
        // not impl
    }
    ~Edge() {
        // not impl
    }

    bool operator==(const Edge& rhs) const {
        return cost==rhs.cost && from==rhs.from && to==rhs.to;
    }
    
    bool operator!=(const Edge& rhs) const {
        return !operator==(rhs);
    }

    bool operator>(const Edge& rhs) const {
        if (cost == rhs.cost)
            return from==rhs.from?to>rhs.to:from>rhs.from;
        return cost>rhs.cost;
    }

    bool operator>=(const Edge& rhs) const {
        return operator==(rhs)||operator>(rhs);
    }

    bool operator<(const Edge& rhs) const {
        return !operator>=(rhs);
    }

    bool operator<=(const Edge& rhs) const {
        return !operator>(rhs);
    }

    string toString() const {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

    friend ostream& operator<<(ostream& os, const Edge& e) {
        os << "("<<e.from << ", " << e.to << "): " << e.cost;
        return os;
    }

    friend istream& operator>>(istream& is, Edge& e) {
        is >> e.from >> e.to >> e.cost;
        return is;
    }
};

namespace std {
    template<>
    struct hash<Edge> {
        size_t operator()(const Edge& e) {
            return ((hash<int>()(e.from) >> 1) << 1)
                  ^((hash<int>()(e.to)>> 1) << 1)
                  ^((hash<double>()(e.cost)>>1)<<1);
        }
    };
}

class EdgeWeightedGraph {
public:
    vector<vector<Edge>> adj;
    int V;
    int E; 

    EdgeWeightedGraph(const int& V): adj(V+1), V(V), E(0) { /* *not impl */ }

    ~EdgeWeightedGraph() { /* not impl */ }

    void addEdge(int u, int v, double cost) {
        adj[u].push_back(Edge(u,v,cost));
        adj[v].push_back(Edge(v,u,cost));
        E++;
    }

    vector<Edge>& getAdj(int v) { return adj[v]; }

    vector<Edge>& operator[](int v) { return adj[v]; }

    int getV() { return V; }
    int getE() { return E; }

    string toString() const {
        ostringstream oss;
        for (int v=0;v< V;v++) {
            oss << v << ": ";
            for (auto e : adj[v])
                oss << e << ", ";
            oss << endl;
        }
        return oss.str();
    }
};
