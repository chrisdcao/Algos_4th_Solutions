#ifndef KRUSKAL_H
#define KRUSKAL_H

#include "Common.h"
#include "EdgeWeightedGraph.h"
#include "UF.h"

using namespace std;

class Kruskal {
private:
    vector<Edge> mst;
    double totalWeight = -DBL_MAX;
public:
    Kruskal(EdgeWeightedGraph G) {
        vector<Edge> pq = G.getEdges();
        make_heap(pq.begin(),pq.end(),greater<Edge>{});
        UF uf(G.getV());
        while (!pq.empty() && mst.size()<G.getV()-1) {
            Edge e = pq.front();
            pop_heap(pq.begin(), pq.end(), greater<Edge>{});
            pq.pop_back();
            int v = e.either(),
                w = e.other(v);
            if (uf.isConnected(v,w)) continue;
            uf.connect(v,w);
            mst.push_back(e);
        }
    }

    vector<Edge>& getMST() {
        return mst;
    }

    double getWeight() {
        if (totalWeight == -DBL_MAX) {
            totalWeight = 0.0;
            for (Edge e : getMST())
                totalWeight += e.getWeight();
        }
        return totalWeight;
    }
};

#endif
