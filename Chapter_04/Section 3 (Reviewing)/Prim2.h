#include "Common.h"

using namespace std;

class PrimMST {
private:
public:
    PrimMST(EdgeWeightedGraph G): edgeTo(G.getV()),
                                  distTo(G.getV()),
                                  marked(G.getV()),
                                  minIndexPQ(G.getV()) {
        fill(distTo.begin(), distTo.end(), DOUBLE_MAX);
        distTo[0] = 0.0;
        minIndexPQ.insert(0, 0.0);
        while (!minIndexPQ.empty())
            visit(G, minIndexPQ.delMin());
    }

    void visit(EdgeWeightedGraph G, int v) {
        marked[v] = true; 
        for (Edge e : G.getAdj(v)) {
            int w = e.other(v);
            if (!marked[w]) {
                if (e.getWeight() < distTo[w]) {
                    distTo[w] = e.getWeight();
                    edgeTo[w] = e;
                    if (!minIndexPQ.contains(w)) minIndexPQ.insert(w, distTo[w]);
                    else minIndexPQ.change(w, distTo[w]);
                }
            }
        }
    }
};

// driver code 
int main() {

}
