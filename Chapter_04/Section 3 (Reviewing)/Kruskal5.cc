#include "Common.h"

using namespace std;

class Kruskal {
private:
    vector<Edge> mst;
public:
    Kruskal(EdgeWeightedGraph G) {
        auto pq = make_heap(G.getEdges().begin(), G.getEdges().end());
        UF uf(G.getV());
        while (!pq.empty() && mst.size()<G.getV()-1) {
            Edge e = pq.top();
            pq.pop();
            int v = e.either(),
                w = e.other(v);
            if (uf.isConnected(v,w)) continue;
            uf.connect(v,w);
            mst.push_back(e);
        }
    }

    vector<Edge>& getEdges() const { return mst; }

    double getWeight() const {
        double totalWeight = 0.0;
        for (Edge e : getEdges())
            totalWeight += e.getWeight();
        return totalWeight;
    }
};
