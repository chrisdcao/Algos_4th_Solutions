#ifndef EDGEWEIGHTEDGRAPH_H
#define EDGEWEIGHTEDGRAPH_H

#include "Common.h"
#include "Edge.h"

using namespace std;

// Undirected Weighted Graph 
class EdgeWeightedGraph {
private:
    int V;
    int E;
    vector<vector<Edge>> adj;
public:
    EdgeWeightedGraph(int V): V(V) {
        adj.resize(V);
    }

    EdgeWeightedGraph(istream& is) {
        Edge e;
        while (!is.eof()) {
            is >> e;
            addEdge(e);
        }
    }

    EdgeWeightedGraph(EdgeWeightedGraph& G) {
        V = G.getV();
        E = 0;
        EdgeWeightedGraph(V);
        for (Edge e : G.getEdges())
            addEdge(e);
    }

    void addEdge(Edge e) {
        int v = e.either(), 
            w = e.other(v);
        adj[v].push_back(e);
        adj[w].push_back(e);
        E++;
    }

    vector<Edge> getAdj(int v) { return adj[v]; }

    int getV() { return V; }
    int getE() { return E; }

    vector<Edge> getEdges() {
        vector<Edge> edges;
        for (int v = 0; v < V; v++)
            for (Edge e : getAdj(v))
                if (e.other(v) > v) //this is because we have already included both way edges in the vertex adj list 
                    edges.push_back(e);
        return edges;
    }

    string toString() {
        ostringstream oss;
        for (int v =0; v < V; v++) {
            oss << v << ": ";
            size_t i;
            for (i = 0; i < getAdj(v).size()-1; i++)
                oss << adj[v][i] << ", ";
            oss << adj[v][i] << endl;
        }
        return oss.str();
    }
};

#endif
