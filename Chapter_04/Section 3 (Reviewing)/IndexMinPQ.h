#ifndef INDEXMINPQ_H
#define INDEXMINPQ_H

#include "Common.h"

using namespace std;

// use when want to pop out elements from min->max order
template <typename Key> 
class IndexMinPQ {
private:

    vector< Key > keys;
    vector< int > pq;
    vector< int > qp;
    int N = 0;

    bool greater( int i, int j ) {
        return keys[ i ] > keys[ j ];
    }

    void exchange( int i, int j ) {
        int temp = pq[ i ]; 
        pq[ i ] = pq[ j ];
        pq[ j ] = temp;
        qp[ pq[ i ] ] = i;
        qp[ pq[ j ] ] = j;
    }

    void swim( int k ) {
        while ( k > 1 && greater( pq[ k/2 ], pq[ k ] ) ) {
            exchange( k/2, k );
            k /= 2;
        }
    }

    void sink( int k ) {
        while ( 2 * k <= N ) {
            int j = 2 * k;
            if ( j < N && greater( pq[ j ], pq[ j+1 ] ) ) j++;
            if ( ! greater( pq[ k/2 ], pq[ k ] ) ) break;
            exchange( k/2, k );
            k = j;
        }
    }

public:
    IndexMinPQ( int maxN ): keys( maxN + 1 ), pq( maxN + 1 ), qp( maxN + 1 ) {}

    bool isEmpty() { return size() == 0; }

    bool containsIndex( int k ) { return qp[ k ] != -1; }

    bool contains(Key k) {
        for (Key key : keys)
            if (key == k) return true;
        return false;
    }

    int size() { return N; }

    Key getMin() { return keys[ pq[ 1 ] ]; }

    int getMinIndex() { return pq[ 1 ]; }

    void insert( int k, Key key ) {
        N++;
        // this pq[] operation is like normal PQ operation, where [ N ] contains the newly inputted key
        pq[ N ] = k;
        /////// similar operations
        keys[ k ] = key;
        qp[ k ] = N;
        ///////
        swim( N );
    }

    void deleteKey( int k ) {
        if ( ! contains( k ) ) throw invalid_argument( "deleteKey(): Invalid index" );
        int index = qp[ k ];
        exchange( index, N-- );
        // sink and swim from the position we delete, we don't have this operation in normal heap thus it looks a bit weird
        sink( index );
        swim( index );
        pq[ N+1 ] = -1;
        qp[ k ] = -1;
    }

    int delMin() {
        int min = pq[ 1 ];
        exchange( 1, N-- );
        sink( 1 );
        pq[ N+1 ] = -1;
        qp[ min ] = -1;
        return min;
    }

    void change( int k, Key key ) {
        if ( ! contains( k ) ) throw invalid_argument( "change(): Invalid index" );
        keys[ k ] = key;
        swim( qp[ k ] );
        sink( qp[ k ] );
    }

    friend ostream& operator <<( ostream& os, const IndexMinPQ< Key >& minPQ ) {
        for ( size_t i = 0; i < minPQ.pq.size(); i++ ) 
            os << i << " : " << minPQ.pq[ i ] << endl;

        os << "\nqp:\n";
        for( size_t i = 0; i < minPQ.qp.size(); i++ )
            os << i << " : " << minPQ.qp[ i ] << endl;

        os << "\nkeys:\n";
        for( size_t i = 0; i < minPQ.keys.size(); i++ )
            os << i << " : " << minPQ.keys[ i ] << endl;
        
        return os;
    }

};

#endif
