#include "Kruskal.h"

using namespace std;

int main() {
    int V, E; 
    cin >> V >> E;
    EdgeWeightedGraph G(V);
    Edge e;
    for (int i = 0; i < E; i++) {
        cin >> e;
        G.addEdge(e);
    }
    cout << "printGraph():\n";
    cout << G.toString() << endl;

    Kruskal kk(G);
    cout << "printMST():\n";
    for (Edge e : kk.getMST())
        cout << e << endl;
    cout << endl;

    cout << "getWeight: ";
    cout << kk.getWeight() << endl;

    return (0);
}
