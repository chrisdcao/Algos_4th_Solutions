#include "Common.h"
#include "Constants.h"
#include "IndexMinPQ.h"
#include "EdgeWeightedGraph.h"

using namespace std;

// scan on node basis
// check on node basis (in relative to the current OVERALL MST (the whole tree, not just edge or node)) to see if distance to the new node is optimal. If not, replace it with the current optimal edge
// MST still contains edges
class PrimMST
{
private:
    vector<bool> marked;
    vector<int> distTo;
    vector<Edge> edgeTo;
    IndexMinPQ<float> indexMinPQ;

public:
    PrimMST(EdgeWeightedGraph G) : marked(G.getV()),
                                   distTo(G.getV()),
                                   edgeTo(G.getV()),
                                   indexMinPQ(G.getV())
    {
        fill(distTo.begin(), distTo.end(), Constants::UFLOAT_MAX);
        distTo[0] = 0.0;
        indexMinPQ.insert(0, 0.0);
        while (!indexMinPQ.isEmpty())
        {
            visit(G, indexMinPQ.delMin());
        }
    }

    void visit(EdgeWeightedGraph G, int v)
    {
        marked[v] = true;
        for (Edge e : G.getAdj(v))
        {
            int w = e.other(v);
            if (!marked[w])
            {
                if (e.getWeight() < distTo[w])
                {
                    edgeTo[w] = e;
                    distTo[w] = e.getWeight();
                    if (indexMinPQ.contains(w))
                        indexMinPQ.change(w, distTo[w]);
                    else
                        indexMinPQ.insert(w, distTo[w]);
                }
            }
        }
    }

    vector<Edge> getMST() { return edgeTo; }

    double getWeight()
    {
        auto weight = 0.0;
        for (int i = 0; i < edgeTo.size(); i++)
            weight += edgeTo[i].getWeight();
        return weight;
    }
};

int main()
{
    int V, E;
    cin >> V >> E;
    EdgeWeightedGraph G(V);
    Edge e;

    for (int i = 0; i < E; i++)
    {
        cin >> e;
        G.addEdge(e);
    }

    cout << G.toString() << endl;

    PrimMST pmst(G);
    cout << pmst.getWeight() << endl;
    for (auto s : pmst.getMST())
        cout << s << endl;
    return (0);
}