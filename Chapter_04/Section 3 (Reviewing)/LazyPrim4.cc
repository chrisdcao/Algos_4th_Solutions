#include "Common.h"

using namespace std;

// the nature of prim is breadth first search on any un-marked points of the explored edge
// and maintaining the edges exploration in ascending order
class LazyPrim {
private:
    vector<bool> marked;
    vector<Edge> mst;
    priority_queue<Edge, vector<Edge>, greater<Edge>> minPQ;
public:
    LazyPrim(EdgeWeightedGraph G): marked(G.getV()) {
        visit(G, 0);
        while (!minPQ.empty()) {
            Edge e = minPQ.top();
            minPQ.pop();
            int v = e.either(),
                w = e.other(v);
            if (marked[v] && marked[w]) continue;
            mst.push_back(e);
            if (!marked[w]) visit(G, w);
            if (!marked[v]) visit(G, v);
        }
    }

    void visit(EdgeWeightedGraph G, int v)  {
        marked[v] = true;
        for (Edge e : G.getAdj(v))  {
            int w = e.other(v);
            if (!marked[w])
                minPQ.push(w);
        }
    }

    vector<Edge> getEdges() {
        return mst;
    }

    double getWeight() {
        double totalWeight = 0.0;
        for (Edge e : getEdges())
            totalWeight += e.getWeight();
        return totalWeight;
    }
};

// driver code
int main() {
    int V, E;
    cin >> V >> E;
    EdgeWeightedGraph G(V);
    Edge e;
    for (int i = 0; i < E; i++) {
        cin >> e;
        G.addEdge(e);
    }
    cout << G.toString() << endl;
    LazyPrim lp(G);
    for (Edge e : lp.getEdges())
        cout << e << endl; 
    cout << endl;

    return (0);
}
