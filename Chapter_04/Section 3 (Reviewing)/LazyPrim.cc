#include "Common.h"
#include "EdgeWeightedGraph.h"

using namespace std;

class LazyPrimMST {
private:
    vector<bool> marked;
    vector<Edge> mst;
    priority_queue<Edge, vector<Edge>, greater<Edge>> edgeMinPQ;
public:
    LazyPrimMST(EdgeWeightedGraph G): marked(G.getV()) {
        // pick `0` as the first node to visit
        visit(G, 0); // this `visit` pushes all the adj nodes of `0` to the MinPQ 
        while (!edgeMinPQ.empty()) {
            Edge e = edgeMinPQ.top();
            edgeMinPQ.pop();
            int v = e.either(),
                w = e.other(w);
            // if the min node from PQ is already visited -> we continue (gray edge)
            if (marked[w] && marked[v]) continue;
            // else we push it to the mst list
            mst.push_back(e);
            // keep adding to the edge min pq list
            if (!marked[v]) 
                visit(G, v);
            if (!marked[w]) 
                visit(G, w);
        }
    }
    
    void visit(EdgeWeightedGraph G, int v) {
        marked[v] = true;
        for (Edge adjEdge : G.getAdj())
            if (!marked[adjEdge.other(v)]) 
                edgeMinPQ.push(adjEdge);
    }
};
