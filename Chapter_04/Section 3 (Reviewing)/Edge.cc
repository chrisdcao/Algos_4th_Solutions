#include "Edge.h"
#include "EdgeWeightedGraph.h"

using namespace std;

int main() {
    /*
    Edge e1;
    cin >> e1;
    cout << e1 << endl;
    Edge e2;
    cin >> e2;
    cout << e2 << endl;
    printf("e1 > e2: %s\n", e1 > e2 ? "true" : "false");
    printf("e1 == e2: %s\n", e1 == e2 ? "true" : "false");
    */

    int V, E;
    cin >> V >> E;
    EdgeWeightedGraph ewg(V);
    Edge e;
    for (int i = 0; i < E; i++) {
        cin >> e;
        ewg.addEdge(e);
    }

    cout << ewg.toString() << endl;
    for (Edge e : ewg.getEdges())
        cout << e << endl;

    return (0);
}
