#include "Common.h"
#include "IndexMinPQ.h"
#include "Constants.h"
#include "EdgeWeightedGraph.h"

using namespace std;

class CriticalEdge {
   public:
    CriticalEdge(EdgeWeightedGraph& G, vector<Edge> MST): edgeTo(G.getV()), distTo(G.getV()), 
	indexMinPQ(G.getV()), marked(G.getV()) {
		// we just rewrite the MST construction algorithm again
		// then we keep count of the edges
		// if 2 edges having the same weight && not form cycle? -> not in the critical set
		// else if it's weight is distinct -> in the critical set
		// Eager Prim
		fill(distTo.begin(), distTo.end(), Constants::UFLOAT_MAX);
		indexMinPQ.insert(0,0.0);
		distTo[0] = 0.0;
		while (!indexMinPQ.isEmpty())
			visit(G, MST, indexMinPQ.delMin());
    }

	void visit(EdgeWeightedGraph G, vector<Edge> MST, int minVertex) {
		marked[minVertex] = true;
		for (Edge e : G.getAdj(minVertex)) {
			int v = e.either(),
				w = e.other(w);
			
		}
	}

   private:
	vector<float> distTo;
	vector<Edge> edgeTo;
	IndexMinPQ<float> indexMinPQ;
	vector<bool> marked;
};

int main() {
}
