#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>
#include <numeric>
#include <vector>
#include <queue>
#include <cstring>
#include <climits>
#include <limits>
#include <float.h>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <algorithm>

#endif
