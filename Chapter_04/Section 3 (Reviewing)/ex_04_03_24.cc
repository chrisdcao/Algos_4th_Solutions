#include "Common.h"
#include "EdgeWeightedGraph.h"

using namespace std;

class ReverseDeleteMST {
   private:
    int V;
    vector<Edge> edges;

   public:
    ReverseDeleteMST(EdgeWeightedGraph &G) : V(G.getV()) {
        edges = G.getEdges();
        sort(edges.begin(), edges.end(), less<Edge>{});
        for (int i = 0; i < edges.size(); i++) {
            vector<Edge> temp;
            for (int n = 0; n < edges.size(); n++) {
                if (edges[n] != edges[i])
                    temp.push_back(edges[i]);
            }
            CC cc(G, temp);
            if (cc.isGraphConnected()) {
                edges.erase(edges.begin() + i);
                //since we have remove 1 element, by staying still we have already got to the next element
                //thus we want to compensate for the next i++
                i--;
            }
        }
    }

    vector<Edge> getMST() {
        return edges;
    }
};

class CC {
   private:
    vector<bool> marked;
    vector<int> id;
    int count = 0;

   public:
    CC(EdgeWeightedGraph &G, vector<Edge> availableEdges) : marked(G.getV()), id(G.getV()) {
        for (Edge e : availableEdges) {
            int v = e.either(),
                w = e.other(w);
            dfs(G, v, availableEdges);
            count++;
        }
    }

    void dfs(EdgeWeightedGraph &G, int v, vector<Edge> availableEdges) {
        marked[v] = true;
        id[v] = count;
        for (Edge &e : G.getAdj(v)) {
            if (contains(availableEdges, e)) {
                int w = e.other(v);
                if (!marked[w])
                    dfs(G, w, availableEdges);
            }
        }
    }

    bool contains(vector<Edge> descendingEdges, Edge e) {
        return binarySearch(e, descendingEdges) != -1;
    }

    int binarySearch(Edge key, vector<Edge> descendingEdges) {
        int lo = 0, hi = descendingEdges.size()-1;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            // reverse the action because the edges are reverse-sorted
            if (descendingEdges[mid] < key)
                hi = mid - 1;
            else if (descendingEdges[mid] > key)
                lo = mid + 1;
            else
                return mid;
        }
        return -1;
    }

    int getCount() { return count; }

    int isConnected(int u, int v) { return id[v] == id[u]; }

    bool isGraphConnected() {
        return getCount() == 1;
    }
};

// driver code
int main() {
    // TODO: Copy-paste. modify and finish driver code
}