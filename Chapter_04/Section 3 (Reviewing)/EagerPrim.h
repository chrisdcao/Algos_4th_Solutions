#include "Common.h"

using namespace std;

class PrimMST {
  private:
      vector<int> edgeTo;
      vector<int> distTo;
      vector<bool> marked;
      priority_queue<double, vector<double>, greater<double>> pq;

  public:
      PrimMST(EdgeWeightedGraph G) {
          edgeTo.resize(G.getV());
          distTo.resize(G.getV());
          for (int v = 0; v < G.getV(); v++)
              distTo[v] = INT_MAX; // so that when we compare to replace we don't have wrong result
          marked.resize(G.getV());
          pq.resize(G.getV());

          distTo[0] = 0.0;
          pq.insert(0, 0.0);
          while (!pq.isEmpty())
              visit(G, pq.delMin());
      }

      void visit(EdgeWeightedGraph G, int v) {
          marked[v] = true;
          for (Edge e : G.getAdj(v)) {
              int w = e.other(v);
              if (marked[w]) continue;
              // see if Edge 'e' is the best connection from current tree to w
              if (e.weight() < distTo[w]) {
                  edgeTo[w] = e;
                  distTo[w] = e;
                  if (pq.contains(w)) pq.change(w, distTo[w]);
                  else                pq.insert(w, distTo[w]);
              }
          }
      }

      vector<Edge> getEdges() const { 
          vector<Edge> mst;
          for (int v = 1; v < edgeTo.size(); v++)
              mst.push_back(edgeTo[v]);
          return mst;
      }

      double getWeight() const {
          double totalWeight = 0.0;
          for (Edge e : getEdges())
              totalWeight += e.getWeight();
          return totalWeight;
      }
};

