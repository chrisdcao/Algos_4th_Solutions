#include "Common.h"

using namespace std;

class EagerPrim {
    const double DOUBLE_MAX = std::numeric_limits<double>::max();
private:
    vector<bool> marked;
    vector<double> distTo;
    vector<Edge> edgeTo;
    IndexMinPQ<double> indexMinPQ; // to save distance in ascending order
public:
    EagerPrim(EdgeWeightedGraph G): marked(G.getV()),
                                    edgeTo(G.getV()),
                                    distTo(G.getV()),
                                    indexMinPQ(G.getV()) {
        fill(distTo.begin(), distTo.end(), DOUBLE_MAX);
        distTo[0] = 0.0;
        indexMinPQ.insert(0, 0.0);
        while (!indexMinPQ.empty())
            visit(G, indexMinPQ.delMin());
    }

    void visit(EdgeWeightedGraph G, int v) {
        marked[v] = true;
        for (Edge e : G.getAdj(v)) {
            int w = e.other(v);
            if (!marked[w]) {
                if (e.getWeight() < distTo[w]) {
                    edgeTo[w] = e;
                    distTo[w] = e.getWeight();
                    if (indexMinPQ.contains(w)) pq.change(w, distTo[w]);
                    else                        pq.insert(w, distTo[w]);
                }
            }
        }
    }

    vector<Edge>& getEdges() { return edgeTo; }

    double weight() {
        double totalWeight = 0.0;
        for (Edge e : getEdges())
            totalWeight += e.getWeight();
        return totalWeight;
    }
};

// driver code
int main() {

};
