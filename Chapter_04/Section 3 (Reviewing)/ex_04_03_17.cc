#include "Common.h"
#include "Edge.h"

using namespace std;

class EdgeWeightedGraph {
private:
    vector<vector<Edge>> adj;
    int E;
    int V;
public:
    EdgeWeightedGraph(int V): V(V), E(0), adj(G.getV()) {}

    int getE() { return E; }
    int getV() { return V; }

    void addEdge(Edge e) {
        int v = e.either(),
            w = e.other(v);
        adj[v].push_back(e);
        adj[w].push_back(e);
        E++;
    }

    vector<Edge>& getAdj(int v) { return adj[v]; }

    vector<Edge>& getEdges() {
        vector<Edge> edges;
        for (int v = 0; v < V; v++) {
            for (Edge e : getAdj(v)) {
                int w = e.other(v);
                if (w > v)
                    edges.push_back(e);
            }
        }
        return edges;
    }

    string toString() {
        ostringstream oss;
        for (int v = 0; v < V; v++) {
            oss << v << ": ";
            for (int w : G.getAdj(v))
                oss << w << " ";
            oss << endl;
        }
        return oss.str();
    }
};

// driver code
int main() {
    int V, E;
    cin >> V >> E;
    EdgeWeightedGraph G(V);
    Edge e;
    for (int i = 0; i < E; i++) {
        cin >> e;
        G.addEdge(e);
    }
    cout << G.toString() << endl;

    return (0);
}
