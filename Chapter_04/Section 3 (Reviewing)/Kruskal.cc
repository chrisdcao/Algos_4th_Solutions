#include "Common.h"
#include "UnionPathCompression.h"

using namespace std;

// find the smallest edge-weight that DOESN'T cause a Cycle for the current MST (since MST (a tree Data Structure) cannot have any cycle)
class KruskalMST {
private:
    vector<Edge> mst;
public:
    KruskalMST(EdgeWeightedGraph G) {
        UF uf(G.getV());
        auto pq = make_heap(G.getEdges().begin(), G.getEdges().end());
        // Prim is always guarded with marked[] and thus doesn't need this condition (since when travelling above G.getV()-1 it will skipped the marked nodes)
        while (!pq.empty() && mst.size() < G.getV()-1) {
            Edge e = pq.delMin();
            int v = e.either(), w = e.other();
            if (uf.isConnected(v,w)) continue; // create cycle and thus skip
            uf.doUnion(v, w);
            mst.push_back(e);
        }
    }

    vector<Edge> getEdges() {
        return mst;
    }

    double getWeight() const {
        double totalWeight = 0.0;
        for (Edge e : getEdges())
            totalWeight += e.getWeight();

        return totalWeight;
    }
};

int main() {
    return (0);
}
