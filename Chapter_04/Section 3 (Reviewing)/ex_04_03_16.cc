#include "Common.h"
#include "EdgeWeightedGraph.h"
#include "EagerPrim.h"

using namespace std;

pair<double,double> getEdgeWeightRange(vector<Edge> MST) {
    double maxEdgeWeight = -DBL_MAX,
           minEdgeWeight = DBL_MAX;
    for (Edge e : MST) {
        double currentWeight = e.getWeight;
        maxEdgeWeight = std::max(currentWeight, maxEdgeWeight);
        minEdgeWeight = std::min(currentWeight, minEdgeWeight);
    }
    return std::make_pair(minEdgeWeight, maxEdgeWeight);
}

// driver code
int main(int argc, char** argv) {
    if (argc < 2) {
        throw runtime_error("Misisng commandline argument");
        return -1;
    }
    ifstream input(argv[1]);

    EdgeWeightedGraph G(input);
    cout << G.toString() << endl;
    
    PrimMST pm(G);
    auto range = getEdgeWeightRange(pm.getEdges());
    cout << "(" <<  range.first << ", " << range.second << ")" << endl;

    return (0);
}
