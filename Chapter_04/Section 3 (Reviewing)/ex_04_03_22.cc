#include "Common.h"
#include "EdgeWeightedGraph.h"
#include "EagerPrim.h"
#include "Kruskal.h"
#include "IndexMinPQ.h"

using namespace std;

class ConnectedComponents {
private:
	vector<bool> marked;
	vector<int> id;
	vector<int> roots;
	int count;
public:
	ConnectedComponents(EdgeWeightedGraph G) : marked(G.getV()),
		id(G.getV()),
		count(0) {
		for (int v = 0; v < G.getV(); v++) {
			if (!marked[v]) {
				dfs(G, v);
				roots.push_back(v);
				count++;
			}
		}
	}

	void dfs(EdgeWeightedGraph G, int v) {
		marked[v] = true;
		id[v] = count;
		for (Edge e : G.getAdj(v)) {
			int w = e.other(v);
			if (!marked[w])
				dfs(G, w);
		}
	}

	bool isConnected(int v, int w) { return id[v] == id[w]; }

	int getId(int v) { return id[v]; }

	int getCount() { return count; }

	vector<int> getRoots() { return roots; }
};

class PrimMSTFromSource {
private:
	vector<double> distTo;
	vector<Edge> edgeTo;
	vector<bool> marked;
	IndexMinPQ<Edge> indexMinPQ;

public:
	PrimMSTFromSource(EdgeWeightedGraph G, int source) : distTo(G.getV()),
		edgeTo(G.getV()),
		marked(G.getV()),
		indexMinPQ(G.getV()) {
		fill(distTo.begin(), distTo.end(), DBL_MAX);
		distTo[source] = 0.0;
		indexMinPQ.insert(source, 0.0);
		while (!indexMinPQ.empty())
			visit(G, indexMinPQ.delMin());
	}

	void visit(EdgeWeightedGraph G, int v) {
		marked[v] = true;
		for (Edge e : G.getAdj(v)) {
			int w = e.other(v);
			// if haven't visited
			if (!marked[w]) {
				// then check if current distance from this `w` to tree < distance from `w` to tree recorded in `distTo[]` (e.getWeight() < distTo[w])
				if (e.getWeight() < distTo[w]) {
					distTo[w] = e.getWeight();
					edgeTo[w] = e;
					if (indexMinPQ.contains(w)) indexMinPQ.change(w, distTo[w]);
					else indexMinPQ.insert(w, distTo[w]);
				}
			}
		}
	}

	vector<Edge> getEdges() const {
		vector<Edge> edges;
		for (int v = 1; v < edgeTo.size() - 1; v++)
			edges.push_back(edgeTo[v]);
		return edges;
	}

	double getWeight() const {
		double totalWeight = 0.0;
		for (Edge e : getEdges())
			totalWeight += e.getWeight();
		return totalWeight;
	}
};

class KruskalFromSource {
private:
public:
	KruskalFromSource(EdgeWeightedGraph G) {
		while (!pq.empty() && mst.size() < G.getV() - 1) {
			Edge e = pq.front();
			pop_heap(pq.begin(), pq.end())
		}
	}
};


class PrimMSF {
private:
	vector<vector<Edge>> msf;
public:
	PrimMSF(EdgeWeightedGraph G) {
		ConnectedComponents cc(G);
		auto roots = cc.getRoots();
		for (int w : roots) {
		}
	}
};

// scan all the possible roots (disconnected Components in the Graph)
class KruskalMSF {
private:

public:
	KruskalMSF(EdgeWeightedGraph G) {

	}
};

int main() {
}
