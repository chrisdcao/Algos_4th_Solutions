#pragma once

#include "../../Common.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <numeric>
#include <sstream>
#include <algorithm>
#include <vector>
#include <array>
#include <unordered_map>
#include <limits.h>
#include <float.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

class Edge {
public:
	int from;
	int to;
	double cost=DBL_MAX;
	char* buf=new char[50];
	Edge()=default;
	Edge(int from, int to, double cost): from(from), to(to), cost(cost) {}
	~Edge() {}
	string toString() {
	    ostringstream oss;
	    oss << from << "->" << to << " " << cost;
	    return oss.str();
	}
	const char* toCString() {
		sprintf(buf,"%d->%d %.2f",from,to,cost);
		return buf;
	}
	bool operator==(const Edge& e) const {
		return from==e.from && to==e.to;
	}
	bool operator!=(const Edge& e) const {
		return !operator==(e);
	}
};

// adj vertices
class DirectedGraph {
public:
	int V;
	int E;
	vector<vector<int>> adj;

	DirectedGraph(int V): V(V), E(0), adj(V) {}
	~DirectedGraph() {}

	int getV() { return V; }
	int getE() { return E; }

	void addEdge(int from, int to) {
		adj[from].push_back(to);
		E++;
	}

	vector<int>& getAdj(int v) {
		return adj[v];
	}

	string toString() {
		ostringstream oss;
		for (int v=0;v<V;v++) {
			oss << v << ": ";
			for (int& w : adj[v])
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

// adj edges
class EdgeWeightedDirectedGraph {
public:
	int V;
	int E;
	vector<vector<Edge>> adj;

	EdgeWeightedDirectedGraph(int V): V(V), E(0), adj(V) {
	}

#define EWD EdgeWeightedDirectedGraph
	EdgeWeightedDirectedGraph(const EWD& G): EdgeWeightedDirectedGraph(G.V) {
	    for (int v=0;v<G.V;v++)
	        for (auto& e:G.adj[v])
	            this->addEdge(e);
	}
#undef EWD
	~EdgeWeightedDirectedGraph() {
		delete[] buf;
	}

	inline int getV() const { return V; }
	inline int getE() const { return E; }

	void addEdge(int from, int to, double cost) {
		adj[from].push_back(Edge(from,to,cost));
		E++;
	}

	void addEdge(Edge e) {
		adj[e.from].push_back(e);
		E++;
	}

	vector<Edge>& getAdj(int v) {
		return adj[v];
	}

	string toString() {
	    ostringstream oss;
	    oss << V << " " << E << "\n";
		for (int v=0;v<V;v++) {
            oss << v << ": ";
			for (Edge& e : adj[v])
				//oss << e.from << " " << e.to << " " << fixed << setprecision(2)<< e.cost <<"\n";
                oss << e.from << "->" << e.to << " " << fixed << setprecision(2) << e.cost << "  ";
            oss << "\n";
		}
		return oss.str();
	}
};

// adj vertices 
class UndirectedGraph {
public:
	int V;
	int E;
	vector<vector<int>> adj;

	UndirectedGraph(int V): V(V), E(0), adj(V) {}
	~UndirectedGraph() {}

	int getV() { return V; }
	int getE() { return E; }

	void addEdge(int from, int to) {
		adj[from].push_back(to);
		adj[to].push_back(from);
		E++;
	}

	vector<int>& getAdj(int v) {
		return adj[v];
	}

	string toString() {
		ostringstream oss;
		for (int v=0;v<V;v++) {
			oss << v << ": ";
			for (int& w : adj[v])
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

// adj vertices 
class EdgeWeightedUndirectedGraph {
public:
	int V;
	int E;
	vector<vector<Edge>> adj;

	EdgeWeightedUndirectedGraph(int V): V(V), E(0), adj(V) {}
	~EdgeWeightedUndirectedGraph() {}

	int getV() { return V; }
	int getE() { return E; }

	void addEdge(int from, int to, double cost) {
		adj[from].push_back(Edge(from,to,cost));
		adj[to].push_back(Edge(to,from,cost));
		E++;
	}

	vector<Edge>& getAdj(int v) {
		return adj[v];
	}

	string toString() {
		ostringstream oss;
		for (int v=0;v<V;v++) {
			oss << v << ": ";
			for (Edge& e : adj[v])
				oss << "(" << e.from << "-" << e.to << ":" << e.cost << "), ";
			oss << endl;
		}
		return oss.str();
	}
};

#define EWD EdgeWeightedDirectedGraph

class EdgeWeightedDirectedCycle {
public:
	vector<bool> marked;
	vector<Edge*> edgeTo;
	vector<bool> onStack;
	vector<Edge> cycle;

	EdgeWeightedDirectedCycle(EWD& G): marked(G.V), edgeTo(G.V), onStack(G.V) {
		for (int v=0;v<G.V;v++) 
			dfs(G,v);
	}
	~EdgeWeightedDirectedCycle() {}

	void dfs(EWD& G, int v) {
		onStack[v]=true;
		marked[v]=true;
		for (auto& e : G.adj[v]) {
			int w =e.to;
			if (hasCycle()) return;
			if (!marked[w]) {
				edgeTo[w]=&e;
				dfs(G,w);
			}
			else if (onStack[w])
				recordCycle(e,w);
		}
		onStack[v]=false;
	}

	inline bool hasCycle() { return !cycle.empty(); }

	void recordCycle(Edge e,int w) {
        Edge x;
		for (x=e; x.from!=w; x=*edgeTo[x.from])
			cycle.push_back(x);
		cycle.push_back(x);
		reverse(cycle.begin(),cycle.end());
	}

	inline vector<Edge>& getCycle() { return cycle; }
};

class EdgeWeightedDFO {
public:
	bool* marked;
	int* preorderCnt;
	int* postorderCnt;
	vector<int> preorder;
	vector<int> postorder;
	vector<int> reversepostorder;

	EdgeWeightedDFO(EWD& G) {
 	 	marked = new bool[G.V];
 	 	preorderCnt=new int[G.V];
 	 	postorderCnt=new int[G.V];

		for (int v=0;v<G.V;v++)
			if (!marked[v])
				dfs(G,v);

		reverse(reversepostorder.begin(),reversepostorder.end());
	}

	~EdgeWeightedDFO() {
		delete[] preorderCnt;
		delete[] postorderCnt;
		delete[] marked;
	}

	void dfs(EWD& G, int v) {
		marked[v]=true;

		preorderCnt[v]+=1;
		preorder.push_back(v);

		for (auto& e : G.adj[v]) {
			if (!marked[e.to]) 
				dfs(G,e.to);
		}

		postorder.push_back(v);
		postorderCnt[v]+=1;

		reversepostorder.push_back(v);
	}

	inline int pre(int v) { return preorderCnt[v]; }
	inline int post(int v) { return postorderCnt[v]; }

	inline vector<int>& getPreOrder() { return preorder; }
	inline vector<int>& getPostOrder() { return postorder; }
	inline vector<int>& getReversePostOrder() { return reversepostorder; }
};

class EdgeWeightedTopological {
public:
	vector<int> order;
	int* rank=nullptr;

	EdgeWeightedTopological(EWD& G) {
		EdgeWeightedDirectedCycle cycleFinder(G);
		if (!cycleFinder.hasCycle()) {
			EdgeWeightedDFO dfo(G);
			order= dfo.getReversePostOrder();

			rank=new int[G.V];
			memset(rank,0,sizeof(int)*G.V);
			for (auto& v : order)
				rank[v]+=1;
		} else {
		    printf("Cycle is:\n");
		    auto edges=cycleFinder.getCycle();
		    for (auto& e : edges) {
		        printf("%d-%d ",e.from,e.to);
		        printf("Iterate is called\n");
		    }
		    printf("\n");
		    fprintf(stderr,"This graph has cycle-> Cannot compute Topology!\n");
		}
	}

	~EdgeWeightedTopological() { delete[] rank; }

	inline bool hasTopology() { return !order.empty(); }

	inline bool isDAG() { return hasTopology(); }

	inline vector<int>& getOrder() { return order; }
	
	inline int getRank(int v) { 
		if (rank==nullptr) 
			throw runtime_error("Has Cycle! Cannot find rank"); 
		return rank[v];
	}
};


#define Topological EdgeWeightedTopological

class AcyclicSP { // shortest path for acyclic graph
public:
	Edge** edgeTo;
	double* cost;
	int V=-1;

	inline bool check(const int& v) const { 
		return V==-1 ? (v >= 0) : (v < V && v >= 0);
	}

	AcyclicSP(EWD& G, int s): V(G.V) {
		assert(check(s));

		edgeTo=new Edge*[G.V];
		cost=new double[G.V];
		for (int v=0;v<G.V;v++) {
            edgeTo[v]=nullptr;
			cost[v]=DBL_MAX;
		}
		cost[s]=0.0;

		Topological top(G);
		for (auto v : top.getOrder())
			relax(G,v);
	}

	void relax(EWD& G, const int& v) {
		for (auto& e : G.adj[v]) {
			if (cost[e.to]-(cost[e.from]+e.cost)>DBL_EPSILON) {
				cost[e.to]=cost[e.from]+e.cost;
				edgeTo[e.to]=&e;
			}
		}
	}

	inline double getCostTo(const int& v) const { 
		assert(check(v));
		return cost[v];
	}

	inline bool hasPathTo(const int& v) const { 
		assert(check(v));
		return cost[v] < DBL_MAX;
	}

	vector<Edge> getPathTo(const int& v) {
		assert(check(v));
		vector<Edge> vec;
		if (!hasPathTo(v)) return vec;
		for (auto x=edgeTo[v];x!=nullptr;x=edgeTo[x->from])
			vec.push_back(*x);
		reverse(vec.begin(),vec.end());
		return vec;
	}
};

class AcyclicLP { // longest path for acyclic graph
public:
	Edge** edgeTo;
	double* cost;
	int V=-1;

	inline bool check(const int& v) const { 
		return V==-1 ? (v >= 0) : (v < V && v >= 0);
	}

	AcyclicLP(EWD& G, int s): V(G.V) {
		assert(check(s));

		edgeTo=new Edge*[G.V];
		cost=new double[G.V];
		for (int v=0;v<G.V;v++) {
            edgeTo[v]=nullptr;
			cost[v]=-DBL_MAX;
		}
		cost[s]=0.0;

		Topological top(G);
		for (auto v : top.getOrder())
			relax(G,v);
	}

	void relax(EWD& G, const int& v) {
		for (auto& e : G.adj[v]) {
			if (cost[e.from]+e.cost-cost[e.to]>DBL_EPSILON) {
				cost[e.to]=cost[e.from]+e.cost;
				edgeTo[e.to]=&e;
			}
		}
	}

	inline double getCostTo(const int& v) const { 
		assert(check(v));
		return cost[v];
	}

	inline bool hasPathTo(const int& v) const { 
		assert(check(v));
		return cost[v] < DBL_MAX;
	}

	vector<Edge> getPathTo(const int& v) {
		assert(check(v));
		vector<Edge> vec;
		if (!hasPathTo(v)) return vec;
		for (auto x=edgeTo[v];x!=nullptr;x=edgeTo[x->from])
			vec.push_back(*x);
		reverse(vec.begin(),vec.end());
		return vec;
	}
};

#undef Topological

class EWDAdjMatrix {
public:
    double** adjMatrix;
    int V,E;

    EWDAdjMatrix(int V): V(V), E(0) {
        adjMatrix=new double*[V];
        for (int i=0;i<V;i++) {
            adjMatrix[i]=new double[V];
            fill(&adjMatrix[i][0],&adjMatrix[i][V],DBL_MAX);
        }
    }

    void addEdge(int u,int v,double c) {
        assert(check(u));
        assert(check(v));
        adjMatrix[u][v]=c;
        E++;
    }

    inline int getV() const { return V; }
    inline int getE() const { return E; }
    inline bool check(int v) const { return v >=0 && v < V; }

    string toString() { // print in form of adj list
        ostringstream oss;
        for (int i=0;i<V;i++) {
            oss << i << ": ";
            for (int j=0;j<V;j++)
                if (adjMatrix[i][j]!=DBL_MAX)
                    oss << j << " ";
            oss << endl;
        }
        return oss.str();
    }
};

#define point Point2D
class EuclideanDigraph {
public:
	int V;
	int E;
	unordered_map<point,vector<Edge2D>> adj;

	EuclideanDigraph(int V): V(V), E(0), adj(V) {}
	~EuclideanDigraph() {}

	void addEdge(const point& from, const point& to) {
		adj[from].push_back(Edge2D(from,to));
		E++;
		if (adj.count(to)) return;
		vector<Edge2D> vec;
		adj.insert(make_pair(to,vec));
	}

	vector<Edge2D>& getAdj(const point& p) {
		assert(check(p));
		return adj[p];
	}

	inline bool check(const point& p) const {
		return p.x>=0 && p.x < V && p.y < V && p.y>=0;
	}

	string toString() {
		ostringstream oss;
		for (auto& pair : adj) {
			oss << pair.first << ": ";
			for (auto& e : pair.second)
				oss << e << " ";
			oss << "\n";
		}
		return  oss.str();
	}

	inline int getV() const { return V; }
	inline int getE() const { return E; }
};
#undef point
