#include <iostream>
#include <sys/types.h>
#include <string>
#include <vector>
#include <cstring>

using namespace std;

/*
Answer space (khong gian nghiem) S{} of the graph will have total of:
			[n*(n+1)/2] possible edges
		with:
		- n: total number of vertices
		- each edge being a pair of connected vertices: i.e. {1,2}
		- edges are distinct: i.e. {2,1} is considered same as {1,2}, and thus will not appear again in the space

Problem becomes: - How many combination (C) of E members can we generate from the Answer Space?
*/

int nCr(int n, int r) { // from `n` choose `r`
	if (r == 0 || r == n) return 1;
	return nCr(n-1,r-1) + nCr(n-1,r);
}

int main() {
	int V, E;
	cin >> V >> E;
	int answerSpace = V * (V+1) / 2;
	if (answerSpace < E) {
		cout << "Cannot generate that much edges from given vertices!" << endl;
		exit(EXIT_FAILURE);
	}
	
	cout << nCr(answerSpace, E) << endl;

	return (0);
}
