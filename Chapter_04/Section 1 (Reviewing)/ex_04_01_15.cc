#include <iostream>
#include <sstream>
#include <unordered_set>
#include <unordered_map>
#include "StringOperations.h"
#include <fstream>
#include <string>
#include <vector>

using namespace std;

class Graph {
private:
	int E;
	int V;
	unordered_map<int,vector<int>> adj;
public:
	Graph(string filename, char delimiter): E(0), V(0) {
		ifstream input(filename);
		string line;
		while (!input.eof()) {
			getline(input, line);
			vector<string> vertices = String::split(line,delimiter);
			if (vertices.size() == 0) break;
			int source = stoi(vertices[0]);
			for (size_t i = 1; i < vertices.size(); i++) {
				adj[source].push_back(stoi(vertices[i]));
				E++;
			}
			V++;
		}
	}

	int getV() { return V; }
	int getE() { return E; }

	vector<int>& adjList(int v) {
		return adj[v];
	}

	string toString() {
		ostringstream oss;
		for (const auto& pair : adj) {
			int v = pair.first;
			oss << v << ": ";
			for (const auto& w : adjList(v))
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

// driver code
int main(int argc, char** argv) {
	string filename = argv[1];
	char delimiter = argv[2][0];
	//cout << filename << " " << delimiter << endl;
	(void)argc;
	Graph graph(filename, delimiter);
	cout << graph.toString() << endl;
	cout << graph.getV() << endl;
	return (0);
}
