#include <iostream>
#include <numeric>
#include <vector>

using namespace std;

class Graph {
private:
	int E;
	int V;
	vector<vector<int>> adjList;
public:
	Graph(const int& V): V(V) {
		adjList.resize(V);
	}

	void getV() { return V; }

	void addEdge(const int& u, const int& v) {
		adjList[u].push_back(v);
		adjList[v].push_back(u);
		E++;
	}

	string toString() {
		ostringstream oss;
		for (int v = 0; v < V; v++) {
			oss << v << ": ";
			for (const auto& w : adjList(v))
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

class BreadthFirstPaths {
private:
	int s;
	vector<bool> marked;
	vector<int> edgeTo;
public:
	BreadthFirstPaths(Graph& graph, int s) {
		this->marked.resize(graph.getV());
		this->edgeTo.resize(graph.getV());
		this->s = s;
		bfs(graph, s);
	}

	void bfs(Graph& graph, int s) {
		marked[s] = true;
		queue<int> q;
		q.push(s);
		while (!q.empty()) {
			int v = q.front();
			q.pop();
			for (const auto& w : graph.adjList(v)) {
				marked[w] = true;
				edgeTo[w] = v;
				q.push(w);
			}
		}
		return q;
	}

	bool hasPathTo(const int& v) { return marked[v] == true; }

	stack<int> pathTo(const int& v) {
		stack<int> path;
		for (int x = v; x != s; x = edgeTo[x])
			path.push(x);
		path.push(s);
		return path;
	}
};

int main(int argc, char** argv) {
	int V;
	cin >> V;
	Graph graph(V);
	int E;
	cin >> E;
	for (int e = 0; e < E; e++) {
		int u,v;
		cin >> u >> v;
		graph.addEdge(u,v);
	}
	cout << graph.toString() << endl;
}
