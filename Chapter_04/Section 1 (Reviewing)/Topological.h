#ifndef TOPOLOGICALSORT_H
#define TOPOLOGICALSORT_H

#include "DepthFirstOrder.h"

using namespace std;

class Topological {
private:
	vector<int> order;
public:
	Topological(Digraph G) {
		DirectedCycle dc(G);
		if (!dc.hasCycle()) {
			DepthFirstOrder DFO(G);
			order = DFO.getReversePost();
		}
	}

	vector<int> getOrder() { return order; }

	bool isAcyclicDigraph() { return !order.empty(); }
};

#endif
