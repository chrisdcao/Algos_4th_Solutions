#include <iostream>
#include <string>
#include <vector>
#include <stack>

using namespace std;

class Graph {
private:
	vector<Bag<int>> adjacents;
	int edgeCount;
	int vertexCount;

public:
	Graph(int vertexCount): vertexCount(vertexCount), edgeCount(0) {
		adjacents.resize(vertexCount);
	}
	
	void addEdge(int vertex1, int vertex2) {
		adjacents[vertex1].add(vertex2);
		adjacents[vertex2].add(vertex1);
		edgeCount++;
	}

	int getEdgeCount() { return this.edgeCount; }

	int getVertexCount() { return this.vertexCount; }

	Bag<int>& adjacentList(int vertex) {
		return vertex <= vertexCount ? adjacents[vertex] : NULL;
	}

	string toString() {
		ostringstream oss;
		for (int v = 1; v <= vertexCount; v++) {
			oss << v << ": ";
			for (int w : adjacents) 
				oss << w << " "
			oss << "\n";
		}
		return oss.str();
	}
};

class DepthFirstSearch {
private:
	vector<int> edgeTo;
	vector<bool> marked;
	int sourceVertex;

public:
	DepthFirstSearch(Graph graph, int sourceVertex): sourceVertex(sourceVertex) {
		edgeTo.resize(graph.getVertexCount());
		marked.resize(graph.getVertexCount());
		dfs(graph, sourceVertex);
	}

	void dfs(Graph graph, int vertex) {
		marked[vertex] = true;
		for (int adjacentsVertex : graph.adjacentList(vertex)) {
			edgeTo[adjacentsVertex] = vertex;
			dfs(graph, adjacentsVertex);
		}
	}

	bool hasPathTo (int vertex) { return marked[vertex]; }

	stack<int> pathTo(int vertex) {
		if (!hasPathTo(vertex)) throw runtime_error("No path to inputted vertex");
		stack<int> path;
		for (int v = vertex; v != sourceVertex; v = edgeTo[v])
			path.push(v);
		path.push(sourceVertex);
		return path;
	}
}

int main() {
	int length;
	cin >> length;
	Graph graph(length);
	for (int i = 0; i < length; i++) {
		int v, u;
		cin >> v >> u;
		graph.addEdge(v,u);
	}
	cout << graph.toString() << endl;
}
