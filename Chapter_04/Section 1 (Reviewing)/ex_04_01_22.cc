#include "Graph.h"
#include <sstream>
#include <fstream>
#include "StringOperations.h"

using namespace std;

class SymbolGraph {
private:
    unordered_map<string, int> st;
    vector<string> keys;
    Graph G;
public:
    /**
    * @brief  
        create a network of movies and actors, that can retrieve from actors to movies, and from movies to actors in it
    * @details 
        we are going to do 2 passes of the same file (so from user perspective, they are only passing 1 filename) 
        to have 2 ways of retrieving item (value-index, index-value) so that all operations are O(1)
        by defining 2 separate input stream objects targeting the same file
    * @param stream 
    * @param sp 
    */
    SymbolGraph(string stream, string sp, char delimiter) {
        ifstream inputFirstPass(stream);
        // first pass
        while (!inputFirstPass.eof()) {
            getline(inputFirstPass, sp);
            vector<string> a = String::split(sp, delimiter);
            for (size_t i = 0; i < a.size(); i++)
                if (!st.count(a[i]))
                    st.insert({a[i], st.size()});
        }
        // create reverse map
        keys.resize(st.size());
        for (auto p : st) {
            //cout << p.first << ": " << p.second << endl;
            keys[p.second] = p.first;
        }
        // Second pass
        this->G.set(st.size());
        ifstream inputSecondPass(stream);
        while (!inputSecondPass.eof()) {
            getline(inputSecondPass, sp);
            //cout << sp << endl;
            vector<string> a = String::split(sp, delimiter);
            int v = st[a[0]];
            //cout << st[a[0]] << endl;
            //cout << v << ": " << endl;
            for (size_t i = 1; i < a.size(); i++) {
                //cout << st[a[i]] << " ";
                G.addEdge(v, st[a[i]]);
            }
            //cout << endl;
        }
        cout << G.toString() << endl;
    }

    bool contains(string s) { return st.count(s) > 0 ? true : false; }

    int index(string s) { return st[s]; }

    string name(int v) { return keys[v]; }

    Graph& getGraph() { return G; }
};

int main(void) {
    string str;
    SymbolGraph symbolGraph("movies.txt", str,'/');
    //cout << symbolGraph.name(2) << endl;
    //cout << symbolGraph.index("Deboy, David");
    // Graph A(symbolGraph.getGraph());
    //cout << A.toString() << endl;
    return 0;
}