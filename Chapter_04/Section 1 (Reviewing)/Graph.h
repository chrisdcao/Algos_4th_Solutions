#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <unordered_set>
#include <queue>
#include <algorithm>
#include <climits>
#include <vector>
#include <numeric>
#include <unordered_map>
#include <stack>
#include <sstream>

using namespace std;

class GraphZeroIndex {
private:
	size_t V;
	int E;
	vector<unordered_set<int>> adj;
public:
	GraphZeroIndex() = default;

	GraphZeroIndex(size_t V) { 
		this->set(V);
	}

	GraphZeroIndex(GraphZeroIndex& G) {
		this->set(G);
	}

	void set(GraphZeroIndex& G) {
		this->set(G.getV());
		for (size_t v = 0; v <= V; v++)
			for (int w : G.adjList(v))
				this->addEdge(w,v);
	}

	void set(size_t size) {
		this->E = 0;
		this->V = size;
		adj.resize(V+1);
	}

	unordered_set<int>& adjList(const int& v) { return adj[v]; }

	void addEdge(const int& v, const int& u) {
		this->adj[u].insert(v);
		this->adj[v].insert(u);
		E++;
	}

	int getV() { return V; }
	int getE() { return E; }

	string toString() {
		ostringstream oss;
		for (size_t v = 0; v < V; v++) {
			oss << v << ": ";
			for (auto& w : this->adjList(v))
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

class Graph {
private:
	size_t V;
	int E;
	vector<unordered_set<int>> adj;
public:
	Graph() = default;

	Graph(size_t V) { 
		this->set(V);
	}

	Graph(Graph& G) {
		this->set(G);
	}

	void set(Graph& G) {
		this->set(G.getV());
		for (size_t v = 1; v <= V; v++)
			for (int w : G.adjList(v))
				this->addEdge(w,v);
	}

	void set(size_t size) {
		this->E = 0;
		this->V = size;
		adj.resize(V+1);
	}

	unordered_set<int>& adjList(const int& v) { return adj[v]; }

	void addEdge(const int& v, const int& u) {
		this->adj[u].insert(v);
		this->adj[v].insert(u);
		E++;
	}

	int getV() { return V; }
	int getE() { return E; }

	string toString() {
		ostringstream oss;
		for (size_t v = 1; v <= V; v++) {
			oss << v << ": ";
			for (auto& w : this->adjList(v))
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

#endif
