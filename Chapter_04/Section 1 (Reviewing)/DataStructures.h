#ifndef DATASTRUCTURES_H
#define DATASTRUCTURES_H

#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <memory>

using namespace std;

template <typename Key>
class Bag {
private:
    class Node {
    public:
        Key key;
        int index = -1;
        Node* next = NULL;
    };
    
    Node* newNode(Key key) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        return allNodes.back().get();
    }
    
    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }
    
    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* initLoc) { current = initLoc; }
        
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        bool operator==(Iterator& rhs) {
            return this->current == rhs.current;
        }
        bool operator!=(Iterator& rhs) {
            return !operator==(rhs);
        }
        Key operator*() {
            return this->current->key;
        }
    };
    
    Node* head = NULL;
    Node* tail = NULL;
    int N = 0;
    vector<unique_ptr<Node>> allNodes;
    
public:
    Bag(): head(NULL), tail(NULL), N(0) {}
    
    int size() { return N; }
    
    bool isEmpty() { return size() == 0; }
    
    void add(Key key) {
        if (isEmpty()) {
            tail = newNode(key);
            head = tail;
        } else {
            Node* temp = newNode(key);
            tail->next = temp;
            tail = temp;
        }
        N++;
    }
    
    Iterator begin() { return Iterator(head); }
    Iterator end() { return NULL; }
};
    
#endif
