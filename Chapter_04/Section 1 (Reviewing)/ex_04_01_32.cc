#include <iostream>
#include <queue>
#include "NormalGraph.h"
#include <vector>

using namespace std;

// linear time -> visit all the nodes X times (X is const)
class GraphSmartOperations {
private:
	vector<bool> marked;
	bool bParallelEdge;

public:
	GraphSmartOperations(NormalGraph& G) {
		bParallelEdge = false;
		marked.resize(G.getV());
		for (int v = 0; v < G.getV(); v++)
			if (!marked[v])
				detectParallelEdge(G, v);
	}

	void detectParallelEdge(NormalGraph& G, const int& s) {
		queue<int> q;
		q.push(s);
		marked[s] = true;
		while (!q.empty()) {
			int v = q.front();
			q.pop();
			for (const auto& w : G.adjList(v)) {
				if (marked[w]) {
					bParallelEdge = true;
					return;
				}
				marked[v] = true;
				q.push(w);
			}
		}
	}

	bool hasParallelEdge() {
		return bParallelEdge;
	}
};

// driver code
int main(void) {
	int V, E;
	cin >> V >> E;
	NormalGraph G(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << G.toString() << endl;

	GraphSmartOperations operation(G);
	cout << operation.hasParallelEdge() << endl;

	return (0);
}
