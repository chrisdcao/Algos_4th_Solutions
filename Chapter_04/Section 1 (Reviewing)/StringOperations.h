#ifndef STRINGOPERATIONS_H
#define STRINGOPERATIONS_H

#include <string>
#include <vector>
#include <cstring>
#include <algorithm>
#include <iostream>
#include "VectorUtils.h"

using namespace std;

class String {
private:
public:
    static vector<string> split(string s, char delimiter) {
        //cout << "s.size(): " << s.size() << endl;
        vector<string> answer;
		/*
        if (s.size() <= 1) 
            throw logic_error("split(): nothing to split");
			*/
        size_t i=0, j=1;
        while (i < s.size()) {
            while (s[j] != delimiter && j < s.size()) j++; 
            if (j == s.size()) {
                answer.push_back(s.substr(i,j-i));
                break;
            }
            answer.push_back(s.substr(i,j-i));
            i = j+1;
            j = i+1;
        }
		/*
		cout << "answer: " << endl;
		for (const auto& s : answer)
			cout << s << " ";
		cout << endl;
		*/
        return answer;
    }
};

#endif
