#include <iostream>
#include <vector>
#include "Graph.h"
#include "StringOperations.h"

using namespace std;
// we want to detect the exact cycle created by 2 inputted vertices, NOT just any cycle within 2 vertices
/*
 * detectEdgeConnect:
 * for v from 0 to V:
 * 		for w : adjacent[v]:
 * 			if isBioConnected(v,w) = FALSE:
 * 				return TRUE 	//-> is the bridge -> the Graph is edge-connected
 * return FALSE
 *
 * isBioConnected: 
 * brief: bioConnected between 2 adjacent points means they must have at least one Cycle
 * -> if we detect a cycle between 2 adj points -> they are bio-connected
*/
class Cycle {
private:
	vector<bool> marked;
	bool isCyclic;
public:
	Cycle(Graph G): isCyclic(false) {
		marked.resize(G.getV());
	}

	// how can we put all the nodes in the cyclic into a DS so that later we can have determine if the Graph
	void dfs(Graph G, int node, int nodeParent) {
		marked[node] = true;
		for (const auto& neighborNode : node)
			if (!marked[neighborNode])
				dfs(G, neighborNode, node);
			else
				isCyclic = true;
	}

	bool hasCycle() { return isCyclic; }
};

// if there's a pair of point that is not bio-connected (meaning that we can only go from 1 to another through direct route)
// -> that will be the bridge
bool isEdgeConnected(Graph G) {
	for (int v = 0; v < V; v++)
		for (const auto& w : G.adjList(v))
			// since we only detect if there's an `edge` - in other words - `connection between 2 adjacent points`
			// -> we only check the Bio-connection of the adjacent points
			if (!isBioConnected(G, w, v)) return true;
	return false;
}

bool isBioConnected(Graph G, int u, int v) {
	Cycle cycle(G);
	cycle.dfs(G, u, v);
	if (!cycle.hasCycle()) return false;
	return true;
}

int main(void) {
	int V, E;
	cin >> V >> E;
	Graph G(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << G.toString() << endl;
	cout << (isEdgeConnected(G) ? "true" : "false") << endl;

	return (0);
}
