#include <iostream>
#include <string>
#include <stack>
#include "Graph.h"
#include <vector>

using namespace std;

// NO IT BECOMES DFS, because we keep search from the last node onward
// proof
class BFSPaths {
private:
	vector<bool> marked;
	vector<int> edgeTo;
	int s;

public:
	BFSPaths(Graph& G, int s): s(s) {
		marked.resize(G.getV());
		edgeTo.resize(G.getV());
		bfs(G,s);
	}

	void bfs(Graph& G, int s) {
		stack<int> stk;
		stk.push(s);
		marked[s] = true;
		while (!stk.empty()) {
			int v = stk.top();
			stk.pop();
			for (const auto& w : G.adjList(v)) {
				marked[w] = true;
				edgeTo[w] = v;
				stk.push(w);
			}
		}
	}

	bool hasPathTo(int v) { return marked[v]; }

	stack<int> pathTo(int v) {
		stack<int> path;
		if (!hasPathTo(v)) return path;
		for (int x = v; x != s; x = edgeTo[x])
			path.push(x);
		path.push(s);
		return path;
	}
};

int main(void) {
	// init string
	int V, E;
	cin >> V >> E;
	Graph G(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << G.toString() << endl;

	BFSPaths bfsPaths(G,1);
	stack<int> res = bfsPaths.pathTo(3);
	while (!res.empty()) {
		cout << res.top() << " ";
		res.pop();
	}
	cout << endl;

	return (0);
}
