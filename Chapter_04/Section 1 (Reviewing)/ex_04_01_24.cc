#include "Graph.h"
#include "StringOperations.h"
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <sstream>
#include <string>

using namespace std;

class ConnectedComponent {
private:
    vector<bool> marked;
    vector<int> id;
    int szLargestComponent;
    int sz;
    int rootId;

public:
    ConnectedComponent(Graph& G): szLargestComponent(0), sz(1), rootId(0) {
        marked.resize(G.getV());
        id.resize(G.getV());
        for (int v = 0; v < G.getV(); v++) {
            // prevent from traversing nodes that already has its ID found
            if (!marked[v]) {
                dfs(G, v);
                szLargestComponent = std::max(sz, szLargestComponent);
                sz = 1;
                rootId++;
            }
        }
    }

    void dfs(Graph& G, const int& node) {
        marked[node] = true;
        id[node] = rootId;
        for (const int& neighborNode : G.adjList(node))
            // avoid traversing neighbor nodes that has already been traversed (by another node) when backtrack
            if (!marked[neighborNode]) {
                sz++;
                dfs(G, neighborNode);
            }
    }

    int getLargestConnectedComponent() { return szLargestComponent; }

    int count() { return rootId; }

    int getId(int v) { return id[v]; }

    bool isConnected(int u, int v) { return id[u] == id[v]; }
};

// driver code
int main(int argc, char** argv) {
    if (argc < 2) {
        throw runtime_error("Not enough arguments!");
    }

    // pass 1 for deciphering resources into Integer
    ifstream input(argv[1]);
    string line;
    unordered_map<string, int> valueIndex;
    unordered_map<int,string> indexValue;
    while (!input.eof()) {
        getline(input, line);
        vector<string> wordChunks = String::split(line,'/');
        for (const string& s : wordChunks)
            if (!valueIndex.count(s)) {
                valueIndex.insert({s, valueIndex.size()});
                indexValue.insert({indexValue.size(), s});
            }
    }
    input.close();

    // pass 2 for inputting into graph (as now we have the integers)
    ifstream input2(argv[1]);
    Graph G(valueIndex.size());
    while (!input2.eof()) {
        getline(input2, line);
        vector<string> wordChunks = String::split(line,'/');
        int v = valueIndex[wordChunks[0]];
        for (size_t i = 1; i < wordChunks.size(); i++) {
            G.addEdge(v, valueIndex[wordChunks[i]]);
        }
    }
    input2.close();

    // print the result
    ostringstream oss;
    for (size_t v = 0; v < G.getV(); v++) {
        oss << indexValue[v] << ": ";
        for (const int& w : G.adjList(v))
            oss << indexValue[w] << ", ";
        oss << endl;
    }
    cout << oss.str() << endl;

    ConnectedComponent cc(G);
    cout << cc.getLargestConnectedComponent() << endl;

    return 0;
}
