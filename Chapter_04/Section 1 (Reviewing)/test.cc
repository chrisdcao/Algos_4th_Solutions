#include <iostream>
#include <vector>
#include "Graph.h"

using namespace std;

int main(void) {
    int E, V;
    cin >> V >> E;
    Graph G(V);
    for (int e = 0; e < E; e++) {
        int u, v;
        cin >> u >> v;
        G.addEdge(u,v);
    }
    cout << G.toString();
    return 0;
}
