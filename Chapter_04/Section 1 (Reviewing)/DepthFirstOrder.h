#ifndef DEPTHFIRSTORDER_H
#define DEPTHFIRSTORDER_H

#include "Common.h"
#include "Digraph.h"

using namespace std;

class DepthFirstOrder {
private:
	vector<bool> marked;
	vector<int> reversePost;
	vector<int> pre;
	vector<int> post;
public:
	DepthFirstOrder(Digraph G) {
		marked.resize(G.getV());
		for (int v = 0; v < G.getV(); v++)
			if (!marked[v])
				dfs(G,v);
		reversePost = post;
		reverse(reversePost.begin(), reversePost.end());
	}

	void dfs(Digraph G, int v) {
		pre.push_back(v);
		marked[v] = true;
		for (int w : G.adjList(v))
			if (!marked[w])
				dfs(G,w);
		post.push_back(v);
	}

	vector<int> getPre() { return pre; }
	vector<int> getPost() { return post; }
	vector<int> getReversePost() { return reversePost; }
};

#endif
