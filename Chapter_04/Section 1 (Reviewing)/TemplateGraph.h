#ifndef TEMPLATEGRAPH_H
#define TEMPLATEGRAPH_H

#include <iostream>
#include <sstream>
#include <string>
#include <set>
#include <unordered_map>
#include <vector>
#include "ImagePixel.h"

using namespace std;

// this must allow repetition for images
template <typename T>
class SymbolGraph {
//private:
public:
	unordered_map<int, T> reverser;
	unordered_map<T, int, PixelHasher, equalTo> converter;
	vector<vector<int>> adj;
	int V;
	int E;
	SymbolGraph(int V): V(V), E(0) {
		adj.resize(V);
	}

	int getV() { return V; }
	int getE() { return E; }

	void addEdge(T u, T v) { 
		// cout << u << " " << v << endl;
		// cout << getIndex(u) << " " << getIndex(v) << endl;
		if (!converter.count(u)) {
			reverser.insert({reverser.size(),u});
			converter.insert({u, converter.size()});
		}
		if (!converter.count(v)) {
			reverser.insert({reverser.size(),v});
			converter.insert({v, converter.size()});
		}
		adj[getIndex(u)].push_back(getIndex(v));
		adj[getIndex(v)].push_back(getIndex(u));
		E++;
	}

	string getName(int v) {
		ostringstream oss;
		oss << reverser[v];
		return oss.str();
	}

	ImagePixel getPixelById(int v) {
		return reverser[v];
	}

	int getIndex(T object) {
		return converter[object];
	}

	vector<int>& adjList(int v) { 
		return adj[v]; 
	}

	string toString() {
		ostringstream oss;
		for (size_t v = 0; v < adj.size(); v++) {
			oss << getName(v) << ": ";
			for (const auto& w : adjList(v))
				oss << getName(w) << " ";
			oss << endl;
		}
		return oss.str();
	}
	
};

#endif
