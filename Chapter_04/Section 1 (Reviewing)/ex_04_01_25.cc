#include <iostream>
#include <string>
#include "Graph.h"
#include "BFS.h"
#include "StringOperations.h"
#include "SymbolGraph.h"

using namespace std;

bool checkMovie(Graph& G, SymbolGraph& symbolGraph, const int& v, const string& inputYear) {
    string fullname = symbolGraph.name(v);
    int inYear = stoi(inputYear);
    size_t startPos = fullname.find("(");
    // if the vertex name is movie, that means we don't have to check its adj (for sure will be actors)
    if (startPos != string::npos) {
        size_t endPos = fullname.find(")");
        string year = fullname.substr(startPos + 1, endPos - (startPos+1));
        if (inYear - stoi(year) > 10)
            return false;
    } else {
        // if the vertex name is NOT movie, we have to check its adjacents (movies) 
        for (const auto& w : G.adjList(v)) {
            fullname = symbolGraph.name(w);
            startPos = fullname.find("(");
            if (startPos != string::npos) {
                size_t endPos = fullname.find(")");
                string year = fullname.substr(startPos + 1, endPos - (startPos+1));
                if (inYear - stoi(year) > 10)
                    return false;
            }
        }
    }

    return true;
}

int main(int argc, char** argv) {
    const string filename = argv[1];
    const char delimiter = argv[2][0];
    const string source = argv[3];
    const string inputYear = argv[4];

    SymbolGraph symbolGraph(filename, delimiter);
    Graph G = symbolGraph.getGraph();

    (void) argc;
    (void) argv;

    if (!symbolGraph.contains(source)) {
        string message = source + " is not in DB!";
        throw runtime_error(message);
    }

    int s = symbolGraph.index(source);
    BFSPaths bfsPaths(G, s);

    while (!cin.eof()) {
        string sink;
        getline(cin, sink);
        if (symbolGraph.contains(sink)) {
            int t = symbolGraph.index(sink);
            if (bfsPaths.hasPathTo(t)) {
                // exclude movies that's more than 'y' years-old
                for (const int& v : bfsPaths.pathTo(t))
                    if (checkMovie(G,symbolGraph,v,inputYear))
                        cout << " " << symbolGraph.name(v) << endl;
            } else
                cout << "Not connected" << endl;
        } else
            cout << "Not in DB" << endl;
    }
    return (0);
}