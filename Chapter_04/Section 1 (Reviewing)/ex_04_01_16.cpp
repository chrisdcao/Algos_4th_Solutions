#include <iostream>
#include <queue>
#include <algorithm>
#include <climits>
#include <vector>
#include <numeric>
#include <unordered_map>
#include <stack>
#include <sstream>

using namespace std;

class Graph {
private:
	int E; int V;
	vector<vector<int>> adj;
public:
	Graph() {
		throw runtime_error("Graph(): Please initialize graph with pre-defined size");
	}

	Graph(int V): V(V), E(0) { adj.resize(V); }

	Graph(const Graph& graph) {
		set(graph);
	}

	vector<int> adjList(int v) { return adj[v]; }

	void addEdge(const int& v, const int& u) {
		adj[u].push_back(v);
		adj[v].push_back(u);
		E++;
	}

	void set(const Graph& G) {
		this->E = 0;
		this->V = graph.getV();
		this->adj.resize(V);
		for (int v = 0; v < V; v++)
			for (const int& w : graph.adjList(v))
				this->addEdge(w,v);
	}

	int getV() { return V; }
	int getE() { return E; }

	string toString() {
		cout << "This is called" << endl;
		ostringstream oss;
		for (int v = 0; v < V; v++) {
			oss << v << ": ";
			for (const auto& w : this->adjList(v))
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

class BreadthFirstPaths {
private:
	vector<int> edgeTo;
	vector<bool> marked;
	int s;
	int currVertex;

public:
	BreadthFirstPaths(Graph& graph, int s): s(s) {
		this->edgeTo.resize(graph.getV());
		this->marked.resize(graph.getV());
		bfs(graph, s);
	}

	void bfs(Graph& graph, int s) {
		queue<int> q;
		q.push(s);
		marked[s] = true;
		while (!q.empty()) {
			int v = q.front();
			q.pop();
			for (const int& w : graph.adjList(v)) {
				edgeTo[w] = v;
				marked[w] = true;
				q.push(w);
			}
		}
	}

	int getBFSMaxPath(Graph& graph, int s) {
		queue<int> q;
		q.push(s);
		marked[s] = true;
		int currEdgeLength = 1, maxEdgeLength = 0;
		while (!q.empty()) {
			int v = q.front();
			q.pop();
			for (const int& w : graph.adjList(v)) {
				edgeTo[w] = v;
				marked[w] = true;
				maxEdgeLength = std::max(currEdgeLength, maxEdgeLength);
				q.push(w);
			}
			currEdgeLength++;
		}
		return maxEdgeLength;
	}

	bool hasPathTo(int v) { return marked[v] == true; }

	stack<int> pathTo(int v) {
		if (!hasPathTo(v) == false) throw runtime_error("pathTo(): No path to v");
		stack<int> path;
		for (int x = v; x != s; x = edgeTo[x])
			path.push(x);
		path.push(s);
		return path;
	}
};

/**
 * int v is the starting vertex
 * bfs from `v` to all other edges
 * while iterating:
 * 		eccentricity: get max (to the furthest vertex)
 * 		diameter: max of eccentricity of all vertices
 * 		radius: min of eccentricity
 * 		center: vertex that found out radius
 **/
class GraphProperties {
private:
	int eccentricity;
	int diameter;
	int radius;
	int center;
	unordered_map<int,int> vertexEccentricity;
	Graph G;

public:
	GraphProperties(Graph G): eccentricity(0), 
							  diameter(0), 
							  radius(INT_MAX), 
							  center(0) {
		this->G.set(G);
	}

	void init() {
		for (int v = 0; v < G.getV(); v++) {
			BreadthFirstPaths bfsPaths(G, v);
			int currPathLength = bfsPaths.getBFSMaxPath(G,v);
			vertexEccentricity.insert({v, currPathLength});
			diameter = std::max(diameter, currPathLength);
			if (currPathLength < radius) {
				radius = currPathLength;
				center = v;
			}
		}
	}

	int getEccentricity(int v) { return vertexEccentricity[v]; }

	int getDiameter() { return diameter; }

	int getRadius() { return radius; }

	int getCenter() { return center; }
};

// driver code
int main() {
	cout << "Welcome to the program" << endl;
	int E, V;
	cin >> V >> E;
	Graph G(V);
	for (int e = 0; e < E; e++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << "something" << endl;
	cout << "G:\n" << G.toString() << endl;

	GraphProperties graphProperties(G);
}
