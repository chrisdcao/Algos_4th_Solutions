#ifndef SYMBOLGRAPH_H
#define SYMBOLGRAPH_H

#include "Graph.h"
#include <sstream>
#include <fstream>
#include <unordered_map>
#include <unordered_set>
#include "StringOperations.h"

using namespace std;

class SymbolGraph {
private:
    unordered_map<string, int> st;
    vector<string> keys;
    Graph G;

public:
    /**
    * @brief  
        create a network of movies and actors, that can retrieve from actors to movies, and from movies to actors in it
    * @details 
        we are going to do 2 passes of the same file (so from user perstrective, they are only passing 1 filename) 
        to have 2 ways of retrieving item (value-index, index-value) so that all operations are O(1)
        by defining 2 separate input stream objects targeting the same file
    * Symbol Table Graph -> SymbolGraph
    * @param stream 
    * @param str 
    */
    SymbolGraph(string stream, char delimiter) {
        ifstream inputFirstPass(stream);
        string str;
        // first pass
        while (!inputFirstPass.eof()) {
            getline(inputFirstPass, str);
            vector<string> a = String::split(str, delimiter);
            for (size_t i = 0; i < a.size(); i++)
                if (!st.count(a[i]))
                    st.insert({a[i], st.size()});
        }
        // create reverse map
        keys.resize(st.size());
        for (auto p : st) {
            keys[p.second] = p.first;
        }
        // Second pass
        this->G.set(st.size());
        ifstream inputSecondPass(stream);
        while (!inputSecondPass.eof()) {
            getline(inputSecondPass, str);
            vector<string> a = String::split(str, delimiter);
            int v = st[a[0]];
            for (size_t i = 1; i < a.size(); i++) {
                G.addEdge(v, st[a[i]]);
            }
        }
        // for (auto s : st) {
        //     cout << s.first << ": " << s.second << endl;
        // }
        // cout << st.count("Blumenfeld, Alan") << endl;
    }

    bool contains(string s) { 
        // cout << s << " " << st.count(s) << endl;
        return st.count(s) > 0; 
    }

    int index(string s) { return st[s]; }

    string name(int v) { return keys[v]; }

    Graph& getGraph() { return G; }
};

#endif
