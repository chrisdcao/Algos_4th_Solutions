#ifndef SYMBOLGRAPH1_H
#define SYMBOLGRAPH1_H

#include "Graph.h"
#include "SymbolGraph.h"
#include "Graph.h"
#include "BFS.h"
#include "StringOperations.h"

using namespace std;

class SymbolGraph {
private:
    unordered_map<string,int> nameIndex;
    unordered_map<int,string> indexName;
    Graph G;

public:
    SymbolGraph(string inputFile, char delimiter) {
        ifstream input(inputFile);
        // init the symbol tables
        while (!input.eof()) {
            string line;
            getline(input, line);
            vector<string> wordChunks = split(line, delimiter);
            for (int i = 0; i < wordChunks.size(); i++)
                if (!nameIndex.count(wordChunks[i])) {
                    nameIndex.insert({wordChunks[i], nameIndex.size()});
                    // for later name translation
                    indexName.insert({indexName.size(), wordChunks[i]});
                }
        }
        // init the graph by decoding input to vertex (integer) based on the symbol table
        ifstream input2(inputFile);
        G.set(nameIndex.size());
        while (!input2.eof()) {
            getline(input2, line);
            vector<string> wordChunks = split(line, delimiter);
            int vertex = nameIndex[wordChunks[0]];
            for (int i = 1; i < wordChunks.size(); i++) {
                G.addEdge(vertex, nameIndex[wordChunks[i]]);
            }
        }
    }

    bool contains(string s) { return nameIndex.count(s) > 0; }

    int getIndex(string s) { return nameIndex[s]; }

    int getName(int vertex) { return indexName[vertex]; }

    Graph getGraph() { return G; }
};

#endif