#include <iostream>
#include <vector>
#include "Graph.h"
#include "StringOperations.h"

using namespace std;

class Cycle {
private:
	vector<bool> marked;
	bool isCyclic = false;

public:
	Cycle(Graph G) {
		marked.resize(G.getV());
		for (int v = 0; v < G.getV(); i++)
			if (!marked[v]) 
				dfs(G,v,v);
	}

	// must handle self-loop and parallel-edge
	void dfs(Graph G, int node, int parentNode) {
		marked[node] = true;
		for (const auto& neighborNode : G.adjList(node))
			if (!marked[neighborNode])
				dfs(G, neighborNode, node);
		// we don't put the constraint in here, thus it detects both self loop and parallel edge
			else
				isCyclic = true;
	}

	bool hasCycle() { return isCyclic; }
};

int main(int argc, char** argv) {
	int E, V;
	cin >> E >> v;
	for (int e = 0; e < E; v++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << G.toString();
	return (0);
}
