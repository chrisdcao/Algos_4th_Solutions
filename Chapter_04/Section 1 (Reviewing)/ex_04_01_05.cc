#include <iostream>
#include <sstream>
#include <vector>
#include "Bag.h"

using namespace std;

class Graph {
private:
    int V; // number of vertices
    int E; // number of edges
    vector<Bag<int>> adj; 
    
public:
    Graph() = default;
    
    Graph(int V): V(V), E(0) { adj.resize(V); }
    
    void setV(int V)  { this->V = V; }
    void setE(int E) { this->E = E; }
    
    int getV() const { return V; }
    int getE() const { return E; }
    
    void addEdge(int v, int u) {
        if (v == u) return;     // no self-loops
        if (hasEdge(v,u)) return;// no parallel edges
        this->adj[v].add(u);
        this->adj[u].add(v);
        E++;
    }
    
    // start - ex_04_01_04
    bool hasEdge(int v, int w) {
        if (v > V) throw logic_error("hasEdge(): v is invalid");
        if (adjList(v).size() == 0) return false;
        for (const auto& s : adjList(v)) // if the adjacent list of v contains w then we have the edge
            if (s == w)  return true;
        return false;
    }
    // end - ex_04_01_04
    
    Bag<int>& adjList(int v) { return adj[v]; }
    
    string toString() {
        ostringstream oss;
        for (int v = 0; v < V; v++) {
            oss << v << ": "; // print the vertex
            for (const auto& w : adjList(v))
                oss << w << " "; // print all the adjacent vertices
            oss << endl;
        }
        return oss.str();
    }
};

int main() {
    int length;
    cin >> length;
    Graph graph(length);
    for (int i = 0; i < length; i++) {
        int v,u;
        cin >> v >> u;
        graph.addEdge(v,u);
    }
    cout << endl;
    cout << "Graph: " << endl;
    cout << graph.toString() << endl;
    cout << endl;
}
