#ifndef BAG_H
#define BAG_H

#include <iostream>
#include <vector>
#include <cstring>
#include <memory>

using namespace std;

template <typename Key>
class Bag {
private:
    class Node {
    public:
        Key key;
        Node* next = NULL;
        int index = -1;
    };
    
    Node* newNode(Key key) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->key = key;
        return allNodes.back().get();
    }
    
    void freeNode(Node* x) {
        if (x->index < allNodes.size()-1) {
            allNodes[x->index].swap(allNodes.back());
            allNodes[x->index]->index = x->index;
        }
        allNodes.pop_back();
    }
    
    class Iterator {
    private:
        Node* currentLoc = NULL;
    public:
        Iterator(Node* initLoc) {
            currentLoc = initLoc;
        }
        Iterator& operator++() {
            currentLoc = currentLoc->next;
            return *this;
        }
        bool operator==(Iterator& rhs) {
            return currentLoc == rhs.currentLoc;
        }
        bool operator!=(Iterator& rhs) {
            return !operator==(rhs);
        }
        Key operator*() {
            return currentLoc->key;
        }
    };    
    
    vector<unique_ptr<Node>> allNodes;
    int N = 0;
    Node* head = NULL;
    Node* tail = NULL;
    
public:
    Bag() = default;
    
    int size() { return N; }
    
    bool isEmpty() { return size() == 0; }
    
    void add(Key key) {
        Node* temp = newNode(key);
        if (head == NULL) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        N++;
    }
    
    Iterator begin() { return Iterator(head); }
    Iterator end() { return Iterator(tail->next); }
};

template <typename Key>
class BagArray {
private:
    void resize(int M) {
        Key* keyTemp = new Key[M];
        if (keys)
            memmove(keys,keyTemp,sizeof(Key)*N);
        else
            memset(keyTemp,Key(),sizeof(Key)*N);
        keys = keyTemp;
        keyTemp = 0x0; delete keyTemp;
        this->M = M;
    }
    
    class Iterator {
    private:
        Key* currentLoc;
    public:
        Iterator(Key* initLoc) { currentLoc = initLoc; }
        Iterator& operator++() {
            currentLoc = currentLoc + 1;
            return *this;
        }
        Iterator& operator==(Iterator& rhs) {
            return this->currentLoc= rhs.currentLoc;
        }
        Iterator& operator!=(Iterator& rhs) {
            return !operator==(rhs);
        }
        Key operator*() { return *this->currentLoc; }
    };
    
    Key* keys = 0x0;
    int N = 0;
    int M = 0;
    
public:
    BagArray(int M): N(0), M(M) {
        resize(M);
    }
    
    void add(Key key) {
        if (N >= M/2) resize(M*2);
        keys[N++] = key;
    }
    
    int size() { return N; }
    
    bool isEmpty() { return N == 0; }
    
    Iterator begin() { return Iterator(keys);}
    Iterator end() { return Iterator(keys + N); }
};

#endif
