#include <iostream>
#include <unordered_map>
#include <stack>
#include <queue>
#include <sstream>
#include <string>
#include <cstring>
#include <vector>

using namespace std;

class Graph {
private:
    int E; int V;
    vector<vector<int>> adjList;
public:
    Graph(int V): V(V) {
        adjList.resize(V);
        this->E = 0;
    }
    
    void addEdge(int v, int u) {
        this->adjList[u].push_back(v);
        this->adjList[v].push_back(u);
        E++;
    }
    
    int getV() { return V; }
    int getE() { return E; }
    
    vector<int>& adj(int v) { return adjList[v]; }
    
    string toString() {
        ostringstream oss;
        for (int v = 0; v < V; v++) {
            oss << v << ": ";
            for (const auto& s : adj(v))
                oss << s << " ";
            oss << endl;
        }
        return oss.str();
    }
};

class BFS {
private:
    vector<bool> marked;
    vector<int> edgeTo;
    unordered_map<int,int> numOfEdges;
    int s;
    int countEdges;
    
public:
    BFS(Graph& G, int s) {
        this->marked.resize(G.getV());
        this->edgeTo.resize(G.getV());
        this->countEdges = 0;
        this->s = s;
        bfs(G,s);
    }
    
    void bfs(Graph& G, int s) {
        //cout << "this is called" << endl; 
        queue<int> nodes;
        nodes.push(s);
        marked[s] = true;
        while (!nodes.empty()) {
            cout << "first loop" << endl;
            int v = nodes.front();
            nodes.pop();
            for (auto w : G.adj(v))
                if (!marked[w]) {
                    marked[w] = true;
                    edgeTo[w] = v;
                    nodes.push(w);
                    countEdges++;
                    numOfEdges[w] = countEdges;
                }
        }
        //for (const auto& s : numOfEdges)
            //cout << s.first << "," << s.second << "  ";
        //cout << endl;
    }
    
    bool hasPathTo(int n) { return marked[n] == true; }
    
    stack<int> pathTo(int v) {
        stack<int> paths;
        if (!hasPathTo(v)) return paths;
        for (int w = v; w != s; w = edgeTo[w])
            paths.push(w);
        paths.push(s);
        return paths;
    }
    
    int distTo(int v) { 
        if (numOfEdges.find(v) != numOfEdges.end())
            return numOfEdges[v];
        return -1;
    }
};

int main() {
    int V, E;
    cin >> V >> E;
    Graph graph(V);
    for (int i = 0; i < E; i++) {
        int u, v;
        cin >> u >> v;
        graph.addEdge(u,v);
    }
    cout << graph.toString() << endl;
    
    BFS bfs(graph,3);
    cout << bfs.distTo(6) << endl;
    return 0;
}
