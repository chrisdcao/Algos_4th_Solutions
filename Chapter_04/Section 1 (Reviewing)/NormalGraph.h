#ifndef NORMALGRAPH_H
#define NORMALGRAPH_H

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

// directed graph
class NormalGraph {
private:
	int V;
	int E;
	vector<vector<int>> adj;
public:
	NormalGraph(int V): V(V), E(0) {
		adj.resize(V);
	}

	void addEdge(int u, int v) {
		// only add single edge
		adj[u].push_back(v);
		E++;
	}

	int getV() { return V; }

	vector<int>& adjList(int v) { return adj[v]; }

	string toString() {
		ostringstream oss;
		for (int v = 0; v < V; v++) {
			oss << v << ": ";
			for (const auto& w : adjList(v))
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

#endif
