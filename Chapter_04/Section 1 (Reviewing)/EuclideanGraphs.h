#ifndef EUCLIDEANGRAPH_H
#define EUCLIDEANGRAPH_H
#include <iostream>
#include <sstream>
#include <vector>
#include <set>
#include <map>
#include <utility>

using namespace std;

typedef pair<int,int> Point;

class EuclideanGraph {
private:
	int E;
	int V;
	map<Point, set<Point>> adj;

public:
	EuclideanGraph(int V): V(V), E(0) {
	}

	void addEdge(Point x, Point y) {
		adj[x].insert(y);
		adj[y].insert(x);
		E++;
	}
	
	int getV() { return V; }

	set<Point>& adjList(Point point) { return adj[point]; }

	string toString() {
		ostringstream oss;
		for (const auto& point : adj) {
			oss << "(" << point.first.first << ", " << point.first.second << "): ";
			for (const auto& neighborPoint : adjList(point.first))
				oss << "(" << neighborPoint.first << ", " << neighborPoint.second << ") ";
			oss << endl;
		}
		return oss.str();
	}
};

#endif
