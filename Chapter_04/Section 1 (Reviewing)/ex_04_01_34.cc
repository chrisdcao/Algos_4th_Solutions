#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include "Graph.h"
#include "StringOperations.h"

using namespace std;

class SymbolGraph {
private:
	GraphZeroIndex G;
	unordered_map<string,int> nameIndex;
	unordered_map<int,string> indexName;
	vector<int> keyVertices;

public:
	SymbolGraph(string inputFile, char delim) {
		ifstream input(inputFile);
		while(!input.eof()) {
			string line;
			getline(input, line);
			cout << line << endl;
			if (line.size() == 0) break;
			vector<string> words = String::split(line, delim);
			// create lookup table
			for (size_t i = 0; i < words.size(); i++) {
				nameIndex.insert({words[i], nameIndex.size()});
				indexName.insert({indexName.size(), words[i]});
			}
			keyVertices.push_back(nameIndex[words[0]]);
		}

		G.set(nameIndex.size());
		size_t k = 0;
		while (k < keyVertices.size()) {
			int start = keyVertices[k];
			int end = keyVertices[k+1];
			for (int i = start+1; i < end; i++) {
				G.addEdge(start, i);
			}
			k++;
		}
	}

	int getIndex(string name) { return nameIndex[name]; }

	string getName(int v) { return indexName[v]; }

	GraphZeroIndex& getGraph() { return G; }
};

int main(int argc, char** argv) {
	string inputFile = argv[1];
	char delim = argv[2][0];
	// cout << inputFile << " " << delim << endl;
	(void) argc;
	(void) argv;

	SymbolGraph sg(inputFile, delim);
	cout << sg.getGraph().toString();
	return (0);
}
