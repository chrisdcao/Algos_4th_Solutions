#include <iostream>
#include <sys/types.h>
#include <string>
#include <vector>
#include "Graph.h"
#include "StringOperations.h"
#include "BFS.h"
#include "DFS.h"
#include "SymbolGraph.h"

using namespace std;

/* Degree of Separation:
1. Brief: 
- Find F1, F2, F3, etc.. of the source node

2. Method:
- Pass 1 node as the reference point (the 'source')
- Interactively input other nodes 
and print out shortest path from that node to source

3. To setup:
- Create a graph indirectly through creating a graph symbol table (symbol graph for short)
- Check the corner case (to see if source not in graph) for quick end
- Get the interactive input */

int main(int argc, char** argv) {
    string inputFile = argv[1];
    char delim = argv[2][0];
    string source = argv[3];    

	(void) argc;
	(void) argv;

    SymbolGraph symbolGraph(inputFile, delim);
    Graph G = symbolGraph.getGraph();
    if (!symbolGraph.contains(source)) {
        cout << source << " not in DB!" << endl;
        exit(EXIT_FAILURE);
    }

    // convert source from name to vertex
    int s = symbolGraph.index(source);
    DFSPaths dfsPaths(G, s);
    while (!cin.eof()) {
        string line;
        getline(cin, line);
        if (symbolGraph.contains(line)) {
            int vertex = symbolGraph.index(line);
            // get bfs path from node to source
            if (dfsPaths.hasPathTo(vertex)) {
                for (int v : dfsPaths.pathTo(vertex))
                    cout << " " << symbolGraph.name(v) << endl;
            } else {
                cout << "not connected" << endl;
            }
        } else {
            cout << "not in DB" << endl;
        }
    }

    return (0);
}
