#include <iostream>
#include <vector>
#include "Graph.h"
#include "StringOperations.h"

using namespace std;

bool hasMoreThanOnePath(GraphZeroIndex& G, int startVertex, int targetVertex) {
	queue<int> q;
	q.push(startVertex);
	while (!q.empty()) {
		int v = q.front();
		q.pop();
		for (const auto& w : G.adjList(v)) {
			if (w == targetVertex) {
				return true;
			}
			q.push(w);
		}
	}
	return false;
}

bool hasBridge(GraphZeroIndex& G) {
	for (int v = 0; v < G.getV(); i++)
		for (const auto& w : G.adjList(v))
			if (hasMoreThanOnePath(G, v, w))
				return true;
}

bool isEdgeConnected(GraphZeroIndex& G) {
	return hasBridge(G);
}

int main(void) {
	int V, E;
	cin >> V >> E;
	GraphZeroIndex G(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << G.toString() << endl;
	cout << (isEdgeConnected(G) ? "true" : "false") << endl;

	return (0);
}
