#include "Graph.h"

using namespace std;

class CC {
private:
	vector<bool> marked;
	int rootId;
	bool isCyclic;

public:
	CC() {
		throw runtime_error("No default constructor");
	}

	CC(Graph& G): rootId(0), isCyclic(false) {
		marked.resize(G.getV());
		for (int node = 0; node < G.getV(); node++) {
			dfs(G,node,node);
			rootId++;
		}
	}

	void dfs(Graph& G, int node, int parentNode) {
		marked[node] = true;
		id[node] = rootId;
		for (const int& neighborNode : G.adjList(node))
			if (!marked[node])
				dfs(G, neighborNode, parentNode);
			else if (neighborNode != parentNode) 
				isCyclic = true;
	}

	bool hasCycle() { return isCyclic; }

	int getId(int node) { return id[node]; }

	int countConnectedComponent() { return rootId + 1; }
};

class BreadthFirstPaths {
private:
	vector<bool> marked;
	int s;
	int currVertex;

public:
	BreadthFirstPaths(Graph& graph, int s): s(s) {
		this->edgeTo.resize(graph.getV());
		this->marked.resize(graph.getV());
		this->s = s;
	}

	int getBFSMaxPath(Graph& graph, int s) {
		vector<bool> marked(graph.getV());
		queue<int> q;
		q.push(s);
		marked[s] = true;
		int currEdgeLength = 1;
		while (!q.empty()) {
			int v = q.front();
			q.pop();
			for (const int& w : graph.adjList(v)) {
				marked[w] = true;
				q.push(w);
			}
			currEdgeLength++;
		}
		return currEdgeLength;
	}

	int getBFSPathLength(Graph& graph, int s) {
		vector<bool> marked(graph.getV());
		queue<int> q;
		q.push(s);
		marked[s] = true;
		while (!q.empty()) {
			int node = q.front();
			q.pop();
			for (const int& neighborNode : graph.adjList(node)) {
				marked[neighborNode] = true;
				q.push(neighborNode);
			}
		}
	}
};

class GraphProperties {
private:
	int eccentricity;
	int diameter;
	int radius;
	int center;
	unordered_map<int,int> vertexEccentricity;
	Graph G;
	CC connectedComponent;

public:
	GraphProperties(Graph G): eccentricity(0), 
							  diameter(0), 
							  radius(INT_MAX), 
							  center(0) {
		this->G.set(G);
	}

	void init() {
		for (int v = 0; v < G.getV(); v++) {
			BreadthFirstPaths bfsPaths(G, v);
			int currPathLength = bfsPaths.getBFSMaxPath(G,v);
			vertexEccentricity.insert({v, currPathLength});
			// find the diamter from the max path length
			diameter = std::max(diameter, currPathLength);
			// find the radius && center of min path length
			if (currPathLength < radius) {
				radius = currPathLength;
				center = v;
			}
		}
	}

	int girth() {
		// test if acyclic
		// 		yes: return infinite
		// 		no: return shortest cycle
		// find shortest cycle
		CC connectedComponent(G);	
		if (connectedComponent.hasCycle() == false) return -1;	

	}

	int getEccentricity(int v) { return vertexEccentricity[v]; }

	int getDiameter() { return diameter; }

	int getRadius() { return radius; }

	int getCenter() { return center; }
};


// driver code
int main() {
	cout << "Welcome to the program" << endl;
	int E, V;
	cin >> V >> E;
	Graph G(V);
	for (int e = 0; e < E; e++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << "something" << endl;
	cout << "G:\n" << G.toString() << endl;

	GraphProperties graphProperties(G);
}
