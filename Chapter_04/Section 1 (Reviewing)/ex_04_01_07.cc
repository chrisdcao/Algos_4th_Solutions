#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include "Bag.h"

using namespace std;

class Graph {
private:
    int E;
    int V;
    vector<Bag<int>> adj;
    
public:
    Graph(int V): V(V), E(0) {
        adj.resize(V);
    }
    
    Graph() = default;
    
    int getE() { return E; }
    int getV() { return V; }
    int setV(int V) { 
        this->V = V;
        adj.resize(V); // resizing auto-fill in the existed chars
    }
    
    void addEdge(int v, int u) {
        this->adj[v].add(u);
        this->adj[u].add(v);
        E++;
    }
    
    Bag<int>& adjList(int v) { return adj[v]; }
    
    string toString() {
        ostringstream oss;
        for (int v = 0; v < V; v++) {
            oss << v << ": ";
            for (const auto& w : adjList(v))
                oss << w << " ";
            oss << endl;
        }
        return oss.str();
    }
};

int main(int argc, char** argv) {
    string inputFile;
    cin >> inputFile;
    ifstream input(inputFile);
    int V;
    input >> V;
    Graph graph(V);
    for (int i = 0; i < V; i++) {
        int v, w;
        input >> v >> w;
        graph.addEdge(v,w);
    }
    cout << "graph.toString(): " << endl;
    cout << graph.toString() << endl;
}
