#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "Bag.h"

using namespace std;

/*class UF {*/
/*private:*/
    /*vector<int> id;*/
    /*vector<int> sz;*/
    /*int cnt;*/
/*public:*/
    /*UF(int N): cnt(N) {*/
        /*for (int i = 0; i < N; i++) {*/
            /*id.push_back(i);*/
            /*sz.push_back(1);*/
        /*}*/
    /*}*/

    /*int count() { return cnt; }*/

    /*int find(int p) { // this should be find AND re-assigning ROOT node*/
        /*// we first assign the root as p*/
        /*int root = p;*/
        /*// then while the root != id[root] we traverse the root through its id (like edgeTo[] traverse)*/
        /*while (root != id[root]) root = id[root];*/
        /*// we find and keep assigning all the root of the child components to the upmost parent?*/
        /*while (p != root) {*/
            /*int next = id[p];*/
            /*id[p] = root;*/
            /*p = next;*/
        /*}*/
        /*return root; // we have done finding the root right from the first while (traversing to root)*/
    /*}*/

    /*bool connected(int p, int q) { return find(p) == find(q); }*/

    /*void doUnion(int p, int q) {*/
        /*int pRoot = find(p);*/
        /*int qRoot = find(q);*/

        /*if (pRoot = qRoot) return;*/

        /*if (sz[pRoot] >= sz[qRoot]) {*/
            /*id[qRoot] = pRoot;*/
            /*sz[pRoot] += sz[qRoot];*/
        /*}*/
    /*}*/

/*};*/

class Graph {
private:
	int E;
	int V;
	vector<Bag<int>> adj;
public:	
	Graph(int V): V(V) { adj.resize(V); }
	Graph() = default;
	
	void addEdge(int v, int u) {
		this->adj[u].add(v);
		this->adj[v].add(u);
		E++;
	}
	
	Bag<int>& adjList(int v) { 
		return adj[v];
	}
	
	int getE() { 
		return E;
	}
	
	int getV() {
		return V;
	}
	
	string toString() { 
		ostringstream oss;
		for (int v = 0; v < V; v++) {
			oss << v << ": " << endl;
			for (const auto& w : adjList(v))
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

class Search {
private:
    int count;
    vector<bool> marked;
public:
    Search(Graph& G, int s): count(0) {
        marked.resize(G.getV());
        dfs(G,s);
    }

    void dfs(Graph& G, int v) {
        marked[v] = true;
        count++;
        for (const auto& w : G.adjList(v)) 
            if (!marked[w]) {
                dfs(G,w);
            }
    }

    // check if vertice v is connected to vertice s
    bool markedNode(int v) { return marked[v]; }

    // return the number of vertices connected to s
    int countVertices() { return count; }
};

int main() {
    int V;
    cin >> V;
    Graph G(V);
    for (int i = 0; i < V; i++) {
        int u,v;
        cin >> u >> v;
        G.addEdge(u, v);
    }
    Search S(G,1);

    cout << "G.count(): " << S.countVertices() << endl; 
    cout << "G.marked(3): " << S.markedNode(3) << endl;

    return 0;
}
