#include <iostream>
#include <unordered_map>
#include <sstream>
#include <random>
#include <vector>
#include <utility>

using namespace std;

class ErdosRenyiGraph {
private:
	int V;
	int E;
	vector<vector<int>> adj;

	void addEdge() {
		pair<int,int> randomPair;
		for (int i = 0; i < E; i++) {
			randomPair = generateRandomPairs();
			// for DEBUG
			pairTrack.push_back(randomPair);
			//
			adj[randomPair.first].push_back(randomPair.second);
			adj[randomPair.second].push_back(randomPair.first);
		}
	}

	pair<int,int> generateRandomPairs() {
		int u = rand()%V; // because it's modded to V, it will be in range [0,V-1]
		int v = rand()%V;
		return make_pair(u,v);
	}

public:
	// for DEBUG
	vector<pair<int,int>> pairTrack;
	//
	
	ErdosRenyiGraph(int E, int V): V(V) {
		adj.resize(V);
		this->E = E;
		addEdge();
	}
	
	int getV() { return V; }

	vector<int>& adjList(int v) { return adj[v]; }

	string toString() { 
		ostringstream oss;
		for (int v = 0; v < V; v++) {
			oss << v << ": ";
			for (const auto& w : adjList(v))
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

// driver code
int main(int argc, char** argv) {
	(void)argc;
	(void)argv;
	int V, E;
	cin >> V >> E;
	ErdosRenyiGraph eGraph(E,V);
	cout << eGraph.getV() << endl;
	cout << eGraph.toString() << endl;
	for (const auto& pair : eGraph.pairTrack)
		cout << "(" << pair.first << ", " << pair.second << ")" << endl;
	return (0);
}
