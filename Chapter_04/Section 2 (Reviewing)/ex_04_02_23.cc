#include "Common.h"
#include "Digraph.h"

using namespace std;

class Solution {
private:
    vector<int> sccPlus;
    vector<vector<int>> allSCC;
public:
    Solution(Digraph G, int v) {
        Kosaraju scc(G);
        // this is to compute the SCC that contains vertex 'v'
        // O(N)
        for (int w = 0; w < G.getV(); w++)
            if (scc.isStronglyConnected(v, w))
                sccPlus.push_back(w);
        sccPlus.push_back(v);
    }

    Solution(Digraph G) {
        Kosaraju scc(G);
        allSCC.resize(scc.getNumOfSCC());
        // get all SCC 
        // O(N^2)
        for (int v = 0; v < G.getV(); v++) 
            for (int w = 0; w < G.getV(); w++) 
                if (scc.isStronglyConnected(v,w))
                    // allowing self-loop
                    allSCC[v].push_back(w);
    }

    vector<int> getSCCContaining(int v) { return sccPlus; }

    vector<vector<int>> getAllSCC() { return allSCC; }
};

class Kosaraju {
private:
    vector<bool> marked;
    vector<int> id;
    int count;

public:
    Kosaraju(Digraph G): count(0) {
        marked.resize(G.getV());
        id.resize(G.getV());
        DepthFirstOder dfsOrder(G);
        for (int w : dfsOrder.getReversePost())
            if (!marked[w]) {
                dfs(G,w);
                count++;
            }
    }
    
    void dfs(Digraph G, int v) {
        marked[v] = true;
        id[v] = count;
        for (int w : G.adjList(v))
            if (!marked[w])
                dfs(G, w);
    }

    int getNumOfSCC() { return count; }

    int getId(int v) { return id[v]; }

    bool isStronglyConnected(int v, int w) { 
        return id[v] == id[w]; 
    }
};

class TopologicalSort {
private:
    vector<int> order;
public:
    TopologicalSort(Digraph G) {
        DirectedCycle dc(G);
        if (!dc.hasCycle()) {
            DepthFirstOrder dfsOrder(G);
            order = dfsOrder.getReversePost();
        }
    }

    vector<int> getOrder() { return order; }
};

class DepthFirstOrder {
private:
    vector<bool> marked;
    vector<int> pre;
    vector<int> post;
    vector<int> reversePost;

public:
    DepthFirstOrder(Digraph G) {
        marked.resize(G.getV());
        for (int v = 0; v < G.getV(); v++)
            if (!marked[v])
                dfs(G, v);
        reverse(reversePost.begin(), reversePost.end());
    }

    void dfs(Digraph G, int v) {
        pre.push_back(w);
        marked[v] = true;
        for (int w : G.adjList(v))
            if (!marked[w])
                dfs(G, w);
        post.push_back(w);
        reversePost.push_back(w);
    }

    vector<int> getReversePost() { return reversePost; }
    vector<int> getPre() { return pre; }
    vector<int> getPost() { return post; }
};

class DirectedDFS {
private:
    vector<bool> marked;

public:
    DirectedDFS(Digraph G, int s) {
        marked.resize(G.getV());
        dfs(G, s);
    }

    DirectedDFS(Digraph G, vector<int> s) {
        marked.resize(G.getV());
        for (int vertex : s)
            if (!marked[vertex])
                dfs(G, vertex);
    }

    void dfs(Digaph G, int v) {
        marked[v] = true;
        for (int w : G.adjList(v))
            if (!marked[w])
                dfs(G, w);
    }

    bool contains(int v) {
        return marked[v] == true;
    }
};

class DirectedCycle {
private:
    vector<bool> onStack;
    vector<int> edgeTo;
    vector<bool> marked;
    stack<int> cycle;
public:
    DirectedCycle(Digraph G) {
        onStack.resize(G.getV());
        edgeTo.resize(G.getV());
        marked.resize(G.getV());
        for (int v = 0; v < G.getV(); v++)
            if (!marked[v])
                dfs(G, v);
    }

    void dfs(Digraph G, int v) {
        onStack[v] = true;
        marked[v] = true;
        for (int w : G.adjList(v)) {
            if (hasCycle()) return;
            else if (!marked[w]) {
                edgeTo[w] = v;
                dfs(G, w);
            } else {
                recordCycle(w, v);
            }
        }
        onStack[v] = false;
    }

    void recordCycle(int w, int v) {
        for (int x = v; x != w; x = edgeTo[x]) {
            cycle.push(x);
        }
        cycle.push(v);
        cycle.push(w);
    }

    stack<int> getCycle() { return cycle; }

    bool hasCycle() { return !cycle.empty(); }
}; 

int main() {

}
