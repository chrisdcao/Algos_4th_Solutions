#ifndef DIRECTEDDFS_H
#define DIRECTEDDFS_H

#include "Common.h"

using namespace std;

class DirectedDFS {
private:

	vector<bool> marked;

	void dfs(Digraph G, int v) {
		marked[v] = true;
		for (int w : G.adjList(v))
			if (!marked[w])
				dfs(G,w);
	}

public:

	DirectedDFS(Digraph G, int s) {
		marked.resize(G.getV());
		dfs(G,s);
	}

	DirectedDFS(Digraph G, vector<int> s) {
		marked.resize(G.getV());
		for (int v : s)
			if (!marked[v])
				dfs(G, v);
	}

	bool isMarked(int v) { return marked[v]; }

	bool contains(int v) { return marked[v]; }
};

#endif
