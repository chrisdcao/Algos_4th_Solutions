#include "Common.h"
#include "Digraph.h"
#include "DirectedDFS.h"

using namespace std;

class FindHeight {
private:
	int root;
	int dest;
	vector<bool> marked;
	vector<int> longest;
	vector<int> temp;
public:
	FindHeight(Digraph G, int root, int dest): root(root),
											   dest(dest) {
		marked.resize(G.getV());
		temp.push_back(root);
		dfs(G,root,dest);	
		longest.push_back(dest);
 	}

	void dfs(Digraph G, int curr, int dest) {
		marked[curr] = true;
		for (int w : G.adjList(curr)) {
			// at the end of each iteration
			// if current > longest path
			// assign longest to current
			if (w == dest) {
				if (temp.size() > longest.size())
					longest = temp;
				return;
			} else if (!marked[w]) {
				// this one is similar to backtracking
				temp.push_back(w);
				dfs(G,w,dest); 
				// after trying the sequence, we reset it back to before trying
				temp.pop_back();
			}
		}
	}

	vector<int>& getPath() { return longest; }

	int get() { return longest.size(); }
};

class LCA {
private:
	bool hasLCA;
	int lowestCommonAncestor;
	vector<int> commonAncestors;
	int maxHeight = -1;
public:
	LCA(Digraph G, int v, int w): hasLCA(false) {
		if (v == w) {
			lowestCommonAncestor = v;
		} else {
			int ancestor;
			for (ancestor = 0; ancestor < G.getV(); ancestor++) {
				DirectedDFS allDFSPathsFromSource(G, ancestor);
				if (allDFSPathsFromSource.contains(w) 
					&& allDFSPathsFromSource.contains(v)
					&& ancestor != v
					&& ancestor != w) {
					hasLCA = true;
					commonAncestors.push_back(ancestor);
				}
			}
			if (hasLCA) find(G,v,w);
		}
	}

	/**
	 * longest path length from root v to all common ancestors
	 * === longest path length from all common ancestors to v
	 * -> we can reverse so that we can traverse in appropriate direction
	 */
	void find(Digraph G, int v, int w) {
		for (int commonAncestor : commonAncestors) {
			FindHeight findHeight(G, commonAncestor, v);
			if (findHeight.get() > maxHeight) {
				maxHeight = findHeight.get();
				lowestCommonAncestor = commonAncestor;
			}
		}
	}

	int get() {
		return hasLCA == true ? lowestCommonAncestor : -1;
	}
};

int main() {
	int V, E;
	cin >> V >> E;
	Digraph G(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << G.toString() << endl;

	FindHeight findHeight(G,0,4);
	
	for (int w : findHeight.getPath())
		cout << w << " ";
	cout << endl;

	LCA lowestCommonAncestor(G,0,3);
	cout << lowestCommonAncestor.get() << endl;

	return (0);
}
