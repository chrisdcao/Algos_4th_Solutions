#include "Common.h"

using namespace std;

class Digraph {
private:
	int V;
	int E;
	vector<vector<int>> adj;

public:
	Digraph(int V): V(V), E(0) {
		adj.resize(V);
	}

	Digraph(Digraph& G) {
		this->V = G.getV();
		adj.resize(V);
		this->E = 0;
		for (int v = 0; v < G.getV(); v++)
			for (int w : G.adjList(v))
				addEdge(v,w);
	}

	void addEdge(int u, int v) {
		adj[u].push_back(v);
		E++;
	}

	int getV() { return V; }
	int getE() { return E; }

	vector<int> adjList(int v) { 
		return adj[v]; 
	}

	Digraph reverse() {
		Digraph R(V);
		for (int v = 0; v < V; v++)
			for (int w : adjList(v))
				R.addEdge(w,v);	
		return R;
	}

	string toString() { 
		ostringstream oss;
		for (int v = 0; v < V; v++) {
			oss << v << ": ";
			for (int w : adjList(v))
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

int main() {
	int V, E;
	cin >> V >> E;
	Digraph G1(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		G1.addEdge(u,v);
	}
	cout << G1.toString() << endl;


	Digraph G2(G1);
	cout << G2.toString() << endl;
	

	return (0);
}
