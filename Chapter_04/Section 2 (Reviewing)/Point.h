#ifndef POINT2D_H
#define POINT2D_H

#include "Common.h"

using namespace std;

class Point2D {
private:
    int x;
    int y;
public:
    Point2D(int x, int y): x(x), y(y) {}
    Point2D() = default;

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }
    void set(int x, int y) {
        setX(x); setY(y);
    }

    int getX() { return this->x; }
    int getY() { return this->y; }

    int compareX(Point2D rhs) {
        if (this->x > rhs.x) return 1;
        else if (this->x < rhs.x) return -1;
        return 0;
    }

    int compareY(Point2D rhs) {
        if (this->y > rhs.y) return 1;
        else if (this->y < rhs.y) return -1;
        return 0;
    }

    void operator=(Point2D rhs) {
        set(rhs.x, rhs.y);
    }

    friend ostream& operator<<(ostream& os, const Point2D& point) {
        os << point.toString();
        return os;
    }

    friend istream& operator >> (istream& is, Point2D& point) {
        int x, y;
        is >> x >> y;
        point.set(x,y);
        return is;
    }

    string toString() {
        ostringstream oss;
        oss << "(" << getX() << ", " << getY() << ")";
        return oss.str();
    }
};

#endif
