#ifndef VECTORUTILS_H
#define VECTORUTILS_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;

template<typename T>
void printVectorOneline(const vector<T>& vec) {
	for (const auto& s : vec)
		cout << s << " ";
	cout << endl;
}

template<typename T>
void printVectorFull(const vector<T>& vec) {
	for (const auto& s : vec)
		cout << s << endl;	
}

#endif
