#ifndef DIGRAPH_H
#define DIGRAPH_H

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

class Digraph {
private:
	int V;
	int E;
	vector<vector<int>> adj;

public:
	Digraph(int V): V(V), E(0) {
		adj.resize(V);
	}

	Digraph(istream& is) {
		is >> V;
		adj.resize(V);
		int tempE;
		is >> tempE;
		//cout << V << endl;
		//cout << tempE << endl;
		for (int i = 0; i < tempE && !is.eof(); i++) {
			int u, v;
			is >> u >> v;
			//cout << u << v << endl;
			addEdge(u,v);
		}
	}

	int getE() { return E; }
	int getV() { return V; }

	vector<int> adjList(int v) {
		return adj[v];
	}

	void addEdge(int u, int v) {
		adj[u].push_back(v);
		E++;
	}

	Digraph reverse() {
		Digraph R(V);
		for (int v = 0; v < V; v++)
			for (int w : adjList(v))
				R.addEdge(w,v);
		return R;
	}

	string toString() {
		ostringstream oss;
		for (int v = 0; v < V; v++) {
			oss << v << ": ";
			for (int w : adjList(v))
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

#endif
