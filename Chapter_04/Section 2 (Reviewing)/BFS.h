#ifndef BFS_H
#define BFS_H

#include <iostream>
#include <algorithm>
#include "Graph.h"
#include <vector>
#include <queue>
#include <stack>

using namespace std;

class BFSPaths {
private:
    vector<bool> marked;
    vector<int> edgeTo;
    int s;
public:
    BFSPaths(Graph& G, int s) {
        this->s = s;
        marked.resize(G.getV());
        edgeTo.resize(G.getV());
        bfs(G,s);
    }

    void bfs(Graph& G, int s) {
        queue<int> q;
        marked[s] = true;
        q.push(s);
        while (!q.empty()) {
            int v = q.front();
            q.pop();
            for (const int& w : G.adjList(v))
                if (!marked[w]) {
                    marked[w] = true;
                    edgeTo[w] = v;
                    q.push(w);
                }
        }
    }

    bool hasPathTo(int v) { return marked[v]; }

    vector<int> pathTo(int v) {
        if (!hasPathTo(v))
            throw runtime_error("pathTo(): doesn't have path!");
        vector<int> path;
        for (int x = v; x != s; x = edgeTo[x])
            path.push_back(x);
        path.push_back(s);
        std::reverse(path.begin(), path.end());
        return path;
    }
};

#endif