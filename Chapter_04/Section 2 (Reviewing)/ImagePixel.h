#ifndef IMAGEPIXEL_H
#define IMAGEPIXEL_H

#include <iostream>
#include <vector>

using namespace std;

class ImagePixel {
public:
	int R;
	int G;
	int B;

	ImagePixel() = default;

	ImagePixel(int R, int G, int B) {
		this->R = R;
		this->G = G;
		this->B = B;
	}

	bool operator>(ImagePixel& rhs) {
		if (this->R == rhs.R) {
			if (this->G == rhs.G) {
				return this->B > rhs.B;
			}
			return this->G > rhs.G;
		}
		return this->R > rhs.R;
	}

	bool operator==(ImagePixel& rhs) {
		return this->R == rhs.R && this->G == rhs.G && this->B == rhs.B;
	}

	bool operator<(ImagePixel& rhs) {
		return !operator==(rhs) && !operator>(rhs);
	}

	bool operator!=(ImagePixel& rhs) {
		return !operator==(rhs);
	}

	bool operator>=(ImagePixel& rhs) {
		return operator==(rhs) && operator>(rhs);
	}

	bool operator<=(ImagePixel& rhs) {
		return operator==(rhs) && operator<(rhs);
	}

	friend ostream& operator<<(ostream& os, ImagePixel pixel) {
		os << pixel.toString();
		return os;
	}

	string toString() {
		string rv = "";
		rv += "(" + to_string(R) + ", " + to_string(G) + ", " + to_string(B) + ")";
		return rv;
	}
};

struct PixelHasher
{
	std::size_t operator()(const ImagePixel& pixel) const
	{
		return std::hash<int>{}(pixel.R) + std::hash<int>{}(pixel.G) + std::hash<int>{}(pixel.B);
	}
};

struct equalTo
{
	bool operator()(const ImagePixel& pixel1,const ImagePixel& pixel2) const {
		ImagePixel pix1(pixel1.R,pixel1.G,pixel1.B);
		ImagePixel pix2(pixel2.R,pixel2.G,pixel2.B);
		return pix1 == pix2;
	}
};

#endif
