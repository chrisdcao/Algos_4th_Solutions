#ifndef IMAGEUTILS_H
#define IMAGEUTILS_H

using namespace std;

template <typename T>
class ImageUtils {
private:
	vector<bool> marked;
public:
	ImageUtils(SymbolGraph<T>& G) {
		marked.resize(G.getV());
		for (int v = 0; v < G.getV(); v++) {
			if (!marked[v]) {
				marked[v] = true;
				floodFill(G, G.getPixelById(v), G.getPixelById(v));
			}
		}
	}

	void floodFill(SymbolGraph<T>& G, ImagePixel px, ImagePixel sPx) {
		for (auto& adjIndex : G.adjList(G.getIndex(px))) {
			if (!marked[G.getIndex(px)]) {
				if (G.getPixelById(adjIndex) == sPx) {
					connect(G, G.getPixelById(adjIndex), sPx);
					marked[adjIndex] = true;
				}
				floodFill(G, G.getPixelById(adjIndex), sPx);
			}
		}
	}

	void connect(SymbolGraph<T>& G, ImagePixel px1, ImagePixel px2) {
		G.addEdge(px1, px2);
	}
};

#endif
