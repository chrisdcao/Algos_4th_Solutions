#include "Common.h"
#include "Point.h"

using namespace std;

class EuclidianDigraph {
private:
    int V;
    int E;
    vector<vector<int>> adj;

public:
    Digraph(int V): V(V), E(0) {
        adj.resize(V);
    }

    void addEdge(Point2D u, Point2D v) {
        adj[u].push_back(v);
        E++;
    }

    vector<int> adjList(int v) {
        return adj[v];
    }

    Digraph reverse() {
        Digraph R(V);
        for (int v = 0; v < V; v++) {
            for (int w : adjList(v))
                R.addEdge(w,v);
        }
        return R;
    }

    string toString() {
        ostringstream oss;
        for (int v = 0; v < V; v++) {
            oss << v << ": ";
            for (int w : adjList(v))
                oss << w << " ";
            oss << endl;
        }
        return oss.str();
    }
};

// driver code
int main() {

}
