#include "Digraph.h"
#include "DirectedCycle.h"
#include "Common.h"

using namespace std;

class QueueBasedTopo {
private:
    vector<int> indegrees;
    queue<int> sources;
    vector<int> order;
public:
    QueueBasedTopo(Digraph G) {
        DirectedCycle dc(G);
        printf("has cycle: %s", dc.hasCycle()?"true":"false");
        if (!dc.hasCycle()) {
            int V = G.getV();
            indegrees.resize(V);
            order.resize(V);
            for (int v = 0; v < V; v++) {
                sources.push(v);
                for (int w : G.adjList(v))
                    indegrees[w]++;
            }
            int orderIndex = 0;
            // the sources now contains the sources in ascending order
            int source;
            while (!sources.empty()) {
                source = sources.front();
                sources.pop();

                order[orderIndex++] = source;

                // only destination points will go through this logic
                // since the first source is the starting point so we don't need to 'sort' that source
                // we will push it instantly into `order`
                // logic: when destination 'fall off' the queue (meaning this dest node hasn't been the source), we have to push it back to explore it as a source later
                // if it's not '0' yet then other nodes when travelled will see it again (thus we don't have to push it in now)
                for (int dest : G.adjList(source)) {
                    indegrees[dest] -= 1;
                    if (indegrees[dest] == 0)
                        sources.push(dest);
                }
            }
        }
    }

    vector<int>& getOrder() { return order; }
};

int main() {
    int V, E;
    cin >> V >> E;
    Digraph G(V);
    for (int i = 0; i < E; i++) {
        int u, v;
        cin >> u >> v;
        G.addEdge(u,v);
    }
    cout << G.toString() << endl;

    QueueBasedTopo qbt(G);
    for (int w : qbt.getOrder())
        cout << w << " ";
    cout << endl;

    return (0);
}
