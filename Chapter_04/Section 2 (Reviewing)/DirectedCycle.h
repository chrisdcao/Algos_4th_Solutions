#include "Common.h"
#include "Digraph.h"

using namespace std;

class DirectedCycle {
private:
	vector<bool> marked;
	vector<int> cycle;
	vector<bool> onStack;
	vector<int> edgeTo;

public:
	DirectedCycle(Digraph G) {
		marked.resize(G.getV());
		onStack.resize(G.getV());
		for (int v = 0; v < G.getV(); v++)
			if (!marked[v])
				dfs(G,v);
	}

	void dfs(Digraph G, int v) {
		marked[v] = true;
		onStack[v] = true;
		for (int w : G.adjList(v)) {
			if (hasCycle()) return;
			else if (!marked[w]) {
				edgeTo[w] = v;
				dfs(G, w);
			}
			// 'w' will be the repeated node (already visited in this session) and will be the one marking cycle
			// since we're recording into stack and edgeTo is only directed from v->w (reverse of initial direction)
			// we go from v->w
			else if (onStack[w]) 
				recordCycle(w,v);
		}
		onStack[v] = false;
	}

	void recordCycle(int w, int v) {
		for (int x = v; x != w; x = edgeTo[x])
			cycle.push_back(x);
		cycle.push_back(w);
		cycle.push_back(v);
		reverse(cycle.begin(), cycle.end());
	}

	bool hasCycle() { return cycle.empty(); }

	vector<int> getCycle() { return cycle; }
};
