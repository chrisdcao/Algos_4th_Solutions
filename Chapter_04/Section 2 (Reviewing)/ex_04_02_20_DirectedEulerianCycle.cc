#include "Common.h"
#include "Degrees.h"
#include "ConnectedComponent.h"
#include "DirectedCycle.h"
#include "Digraph.h"

using namespace std;

class Euler {
private:
	bool isConnected = true;
	bool isEvenDegree = true;
public:
	Euler(Digraph G) {
		ConnectedComponent_Directed dcc(G);
		if (dcc.getNumOfComponents() == 1) {
			Degrees degrees(G);
			for (int v = 0; v < G.getV(); v++)
				if (degrees.indegree(v) != degrees.outdegree(v)) {
					isEvenDegree = false;
					break;
				}
		} else {
			isConnected = false;
		}
	}

	bool hasTour() {
		return isConnected == true 
			&& isEvenDegree == true;
	}

	bool isConnectedDAG() {
		return isConnected;
	}

	bool hasEqualIndegreeOutdegree() {
		return isEvenDegree;
	}
};

int main() {
	int V, E;
	cin >> V >> E;
	Digraph G(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << G.toString() << endl;

	Euler ec(G);
	printf("Does G have Eulerian Cycle?: %s\n", ec.hasTour() ? "True" : "false");
	printf("Does G have equal in and out degree?: %s\n", ec.hasEqualIndegreeOutdegree() ? "True" : "False");
	printf("Is G a connected Graph?: %s\n", ec.isConnectedDAG() ? "True" : "False");

	return (0);
}
