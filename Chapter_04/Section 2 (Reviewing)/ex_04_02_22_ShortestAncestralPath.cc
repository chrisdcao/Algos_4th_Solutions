#include "Common.h"
#include "Digraph.h"
#include "DirectedDFS.h"

using namespace std;

class FindHeight {
private:
	int root;
	int dest;
	vector<bool> marked;
	vector<int> longest;
	vector<int> temp;
public:
	FindHeight(Digraph G, int root, int dest): root(root),
											   dest(dest) {
		marked.resize(G.getV());
		temp.push_back(root);
		dfs(G,root,dest);	
		longest.push_back(dest);
 	}

	void dfs(Digraph G, int curr, int dest) {
		marked[curr] = true;
		for (int w : G.adjList(curr)) {
			// at the end of each iteration
			// if current > longest path
			// assign longest to current
			if (w == dest) {
				if (temp.size() > longest.size())
					longest = temp;
				return;
			} else if (!marked[w]) {
				// this one is similar to backtracking
				temp.push_back(w);
				dfs(G,w,dest); 
				// after trying the sequence, we reset it back to before trying
				temp.pop_back();
			}
		}
	}

	vector<int>& getPath() { return longest; }

	int get() { return longest.size(); }
};

class ShortestAncestralPath {
private:
	bool hasLCA;
	int lowestCommonAncestor;
	unordered_map<int,int> commonAncestorsAndTotalLength;
	vector<int> edgeTo;
public:
	LCA(Digraph G, int v, int w): hasLCA(false) {
		edgeTo.resize(G.getV());
		int ancestor;
		for (ancestor = 0; ancestor < G.getV(); ancestor++) {
			DirectedDFS allDFSPathsFromSource(G, ancestor);
			if (allDFSPathsFromSource.contains(w) 
				&& allDFSPathsFromSource.contains(v)
				&& ancestor != v
				&& ancestor != w) {
				hasLCA = true;
				commonAncestorsAndTotalLength[ancestor] = -1;
			}
		}
		if (hasLCA) find(G,v,w);
	}

	void find(Digraph G, int v, int w) {
		Digraph R = G.reverse();
		bfs(R,v);
		bfs(R,w);
	}

	void bfs(Digraph G, int s) {
		queue<int> q;
		q.push(s);
		int length = 0;
		while (!q.empty()) {
			int v = q.front();
			q.pop();
			for (int w : G.adjList(v)) {
				edgeTo[w] = v;
				if (commonAncestorsAndTotalLength.count(w) != 0)
					commonAncestorsAndTotalLength[w] += length;
				count++;
				q.push(w);
			}
		}
	}

	stack<int> get() {
		for 
	}
};

int main() {
	int V, E;
	cin >> V >> E;
	Digraph G(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << G.toString() << endl;

	FindHeight findHeight(G,0,4);
	
	for (int w : findHeight.getPath())
		cout << w << " ";
	cout << endl;

	LCA lowestCommonAncestor(G,0,3);
	cout << lowestCommonAncestor.get() << endl;

	return (0);
}
