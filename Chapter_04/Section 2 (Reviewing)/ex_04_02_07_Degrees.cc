#include "Common.h"
#include "Digraph.h"

using namespace std;

class Degrees {
private:
	unordered_map<int,int> vertexAndIndegreeCount;
	unordered_map<int,int> vertexAndOutdegreeCount;
	vector<int> sources;	
	vector<int> sinks;
public:
	Degrees(Digraph G) {
		int V = G.getV();
		for (int v = 0; v < V; v++) {
			for (int w : G.adjList(v)) {
				vertexAndOutdegreeCount[v]++;
				vertexAndIndegreeCount[w]++;
			}
		}
	}
	
	int indegree(int v) {
		return vertexAndIndegreeCount[v];
	}

	int outdegree(int v) {
		return vertexAndOutdegreeCount[v];
	}

	vector<int>& getSourceList() {
		if (sources.empty())
			for (pair<int,int> p : vertexAndIndegreeCount)
				if (p.second == 0)
					sources.push_back(p.first);
		return sources;
	}

	vector<int>& getSinkList() {
		if (sinks.empty())
			for (pair<int,int> p : vertexAndOutdegreeCount)
				if (p.second == 0)
					sinks.push_back(p.first);
		return sinks;
	}

	bool isMap() {
		for (pair<int,int> p : vertexAndOutdegreeCount)
			if (p.second!=1) return false;
		return true;
	}
};

int main() {
	int V, E;
	cin >> V >> E;
	Digraph G(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << G.toString() << endl;

	Degrees degrees(G);

	printf("Indegree Count():\n");
	for (int v = 0; v < V; v++)
		printf("%d: %d\n",v,degrees.indegree(v));

	printf("\nOutdegree Count():\n");
	for (int v = 0; v < V; v++)
		printf("%d: %d\n",v,degrees.outdegree(v));

	printf("getSourceList():\n");
	for (int o : degrees.getSourceList())
		printf("%d ", o);
	printf("\n");

	printf("getSinkList():\n");
	for (int o : degrees.getSinkList())
		printf("%d ", o);
	printf("\n");

	printf("isMap(): %s\n", degrees.isMap() ? "true" : "false");

	return (0);
}
