#include "Common.h"

using namespace std;

class Solution {
public:
    bool hasUniqueTopo = false;

    Solution(Digraph G) {
        HamiltonPath hp(G);
        if (hp.hasHamiltonPath())
            hasUniqueTopo = true;
    }
};

// driver code
int main() {

}
