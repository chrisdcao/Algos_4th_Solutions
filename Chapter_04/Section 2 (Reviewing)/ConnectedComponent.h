#ifndef CONNECTEDCOMPONENT_DIRECTED_H
#define CONNECTEDCOMPONENT_DIRECTED_H

#include "Common.h"
#include "Digraph.h"

class ConnectedComponent_Directed {
private:
	int count = 0;
	vector<bool> marked;
	vector<int> rootId;
public:
	ConnectedComponent_Directed(Digraph G) {
		marked.resize(G.getV());
		rootId.resize(G.getV());
		for (int v = 0; v < G.getV(); v++)
			if (!marked[v]) {
				dfs(G,v);
				count++;
			}
	}

	void dfs(Digraph G, int v) {
		marked[v] = true;
		rootId[v] = count;
		for (int w : G.adjList(v))
			if (!marked[w])
				dfs(G,w);
	}

	int getRootId(int v) {
		return rootId[v];
	}

	int getNumOfComponents() { 
		return count;
	}
};

#endif
