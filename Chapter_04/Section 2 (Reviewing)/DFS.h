#ifndef DFS_H
#define DFS_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include "Graph.h"

using namespace std;

class DFSPaths {
private:
	vector<bool> marked;
	vector<int> edgeTo;
	int s;

public:
	DFSPaths(Graph& G, int s) {
		this->s = s;
		marked.resize(G.getV());
		edgeTo.resize(G.getV());
		dfs(G, s);
	}

	void dfs(Graph& G, int s) {
		marked[s] = true;
		for (auto& v : G.adjList(s))
			if (!marked[v]) {
				edgeTo[v] = s;
				dfs(G, v);
			}
	}

	bool hasPathTo(int v) { return marked[v]; }

	vector<int> pathTo(int v) {
		if (!hasPathTo(v)) throw runtime_error("No path found");
		vector<int> path;
		for (int x = v; x != s; x = edgeTo[x])
			path.push_back(x);
		path.push_back(s);
		reverse(path.begin(), path.end());
		return path;
	}
};

#endif
