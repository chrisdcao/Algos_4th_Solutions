#ifndef DEGREES_H
#define DEGREES_H

#include "Common.h"
#include "Digraph.h"

using namespace std;

// even degree doesn't exist in directed graph -> thus we use the theory:  indegree == outdegree rather than eveness of the degree
// for undirected graph, indegree and outdegree don't exist -> thus we use the even degree theory
class Degrees {
private:
	unordered_map<int,int> vertexAndIndegreeCount;
	unordered_map<int,int> vertexAndOutdegreeCount;
	vector<int> sources;	
	vector<int> sinks;
public:
	Degrees(Digraph G) {
		int V = G.getV();
		for (int v = 0; v < V; v++) {
			for (int w : G.adjList(v)) {
				vertexAndOutdegreeCount[v]++;
				vertexAndIndegreeCount[w]++;
			}
		}
	}
	
	int indegree(int v) {
		return vertexAndIndegreeCount[v];
	}

	int outdegree(int v) {
		return vertexAndOutdegreeCount[v];
	}

	vector<int>& getSourceList() {
		if (sources.empty())
			for (pair<int,int> p : vertexAndIndegreeCount)
				if (p.second == 0)
					sources.push_back(p.first);
		return sources;
	}

	vector<int>& getSinkList() {
		if (sinks.empty())
			for (pair<int,int> p : vertexAndOutdegreeCount)
				if (p.second == 0)
					sinks.push_back(p.first);
		return sinks;
	}

	bool isMap() {
		for (pair<int,int> p : vertexAndOutdegreeCount)
			if (p.second!=1) return false;
		return true;
	}
};

#endif
