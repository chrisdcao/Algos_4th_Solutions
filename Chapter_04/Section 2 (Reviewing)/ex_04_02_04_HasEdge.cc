#include "Common.h"

using namespace std;

class Digraph {
private:
	int V;
	int E;
	vector<unordered_set<int>> adj;
public:
	Digraph(int V): V(V), E(0) {
		adj.resize(V);
	}

	void addEdge(int u, int v) {
		adj[u].insert(v);
		E++;
	}

	int getV() { return V; }
	int getE() { return E; }

	unordered_set<int> adjList(int v) { return adj[v]; }

	Digraph reverse() {
		Digraph R(V);
		for (int v = 0; v < V; v++)
			for (int w : adjList(v))
				R.addEdge(w,v);
		return R;
	}

	bool hasEdge(int v, int w) {
		return adj[v].count(w) != 0;
	}

	string toString() {
		ostringstream oss;
		for (int v = 0; v < V; v++) {
			oss << v << ": ";
			for (int w : adjList(v))
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

int main() {
	int V, E;
	cin >> V >> E;
	Digraph G(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << G.toString() << endl;

	cout << "hasEdge():" << endl;
	for (int v = 0; v < V-1; v++)
		printf("(%d, %d): %s\n",v,v+1,G.hasEdge(v,v+1) ? "true" : "false");
	cout << endl;

	cout << "reverse(): " << endl;
	cout << G.reverse().toString() << endl;

	return (0);
}
