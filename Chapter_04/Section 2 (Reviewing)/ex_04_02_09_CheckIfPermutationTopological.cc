#include "Common.h"
#include "Digraph.h"
#include "DirectedCycle.h"

using namespace std;

class DepthFirstOrder {
private:
	vector<bool> marked;
	vector<int> pre;
	vector<int> post;
	vector<int> reversePost;

	void dfs(Digraph G, int v) {
		marked[v] = true;
		pre.push_back(v);
		for (int w : G.adjList(v)) {
			if (!marked[w]) {
				dfs(G,w);
			}
		}
		post.push_back(v);
		reversePost.push_back(v);
	}

public:
	DepthFirstOrder(Digraph G) {
		marked.resize(G.getV());
		for (int v = 0; v < G.getV(); v++) {
			if (!marked[v]) {
				dfs(G,v);
			}
		}
		reverse(reversePost.begin(), reversePost.end());
	}

	vector<int>& getPre() { return pre; }
	vector<int>& getPost() { return post; }
	vector<int>& getReversePost() { return reversePost; }

	bool isPermutation(vector<int> permutatedVertices) {
		return (permutatedVertices == pre) 
			|| (permutatedVertices == post) 
			|| (permutatedVertices == reversePost);
	}
};

int main() {
	int V, E;
	cin >> V >> E;
	Digraph G(V);
	vector<int> permutatedVec;
	// initial setup
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	for (int i = 0; i < V; i++) {
		permutatedVec.push_back(i);
	}
	cout << G.toString() << endl;

	// test
	DepthFirstOrder dfsOrder(G);
	printf("Trial for 10 times:\n");
	for (int trial = 0; trial < 10; trial++) {
		random_shuffle(permutatedVec.begin(), permutatedVec.end());
		printf("%s\n", dfsOrder.isPermutation(permutatedVec) ? "true" : "false");
	}

	return (0);
}
