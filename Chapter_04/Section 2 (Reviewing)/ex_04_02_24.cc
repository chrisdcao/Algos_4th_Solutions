#include "Common.h"

using namespace std;

class HamiltonPath {
private:
    bool containsHamilton;

public:
    HamiltonPath (Digraph G): containsHamilton(true) {
        // Time & Space: O(N)
        // for later lookup
        unordered_map<int,set<int>> adj;
        for (int v = 0; v < G.getV(); v++)
            for (int w : G.adjList(v))
                adj[v].insert(w);
        // Time & Space: O(N)
        TopologicalSort ts(G);
        vector<int> order = ts.getOrder();
        for (int i = 0; i < order.size()-1; i++)
            if (adj[order[i]].count(order[i+1]) == 0)
                containsHamilton = false;
    }

    bool hasHamiltonPath() {
        return containsHamilton;
    }
};

// driver code
int main() {

}
