#include "Common.h"

using namespace std;

class Digraph {
private:
	int V;
	int E;
	vector<unordered_set<int>> adj;
public:
	Digraph(int V): V(V), E(0) {
		adj.resize(V);
	}

	void addEdge(const int& u, const int& v) {
		if (u == v) return; // disable self-loop
		if (adj[v].count(u) != 0) return; // disable parallel edge
		adj[u].insert(v);
		E++;
	}

	int getV() const { return V; }
	int getE() const { return E; }

	unordered_set<int> adjList(const int& v) const {
		return adj[v];
	}

	string toString() const {
		ostringstream oss;
		for (int v = 0; v < V; v++) {
			oss << v << ": ";
			for (const int& w : adjList(v))
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

int main() {
	int V, E;
	cin >> V >> E;
	Digraph G(V);
	for (int i = 0; i < E; i++) {
		int u, v;
		cin >> u >> v;
		G.addEdge(u,v);
	}
	cout << G.toString() << endl;

	return (0);
}
