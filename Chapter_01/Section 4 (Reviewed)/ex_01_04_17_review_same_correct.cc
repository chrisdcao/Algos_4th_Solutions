#include <iostream>
#include <vector>
using namespace std;

int main() {
    double x; 
    vector<double> vec;
    while (cin >> x)
        vec.push_back(x);
    int i = 0;
    for (int a = 1; a < vec.size(); a++) {      // ~ N
        if (vec[a] < vec[i])
            i = a;
    }
    double min = vec[i];

    int j = 0;
    for (int a = 1; a < vec.size(); a++) {      // ~ N
        if (vec[a] > vec[j])
            j = a;
    }
                                                // TOTAL: ~ O(N)
    double max = vec[j];
    cout << "The farthest pair is: " << max << " " << min 
         << " with the distance being: " << max - min << endl;
}
