#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int BinarySearchBound(int key, int lo, int hi, vector<int> vec) {
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key > vec[mid]) lo = mid + 1;
        else if (key < vec[mid]) hi = mid - 1;
        else return mid;
    }
    return -1;
}

// BinarySearch only applies for sorted array
int BinarySearch(int key, vector<int> vec) {
    int lo = 0;
    int hi = vec.size() - 1;
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key > vec[mid]) lo = mid + 1;
        else if (key < vec[mid] || mid > 0 && key == vec[mid - 1]) hi = mid - 1;        // make the loop run until vec[mid - 1] != key, which make it deduce to the minimal case where there be no element on the left of the key 
        else {
            int search2 = BinarySearchBound(key, lo, mid - 1, vec);     
            cout << search2 << endl;
            if (search2 != -1) return search2;                  // just in case 
            else return mid;
        }
    }
    return -1;
}

int main() {
    int x;
    vector<int> vec;
    while (cin >> x) {
        vec.push_back(x);
    }
    cout << BinarySearch(1, vec) << endl;
    cout << BinarySearch(2, vec) << endl;
    cout << BinarySearch(4, vec) << endl;
    cout << BinarySearch(11, vec) << endl;
    return 0;
}
