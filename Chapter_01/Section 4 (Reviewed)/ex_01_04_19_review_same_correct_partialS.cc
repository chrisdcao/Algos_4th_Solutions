#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

int main(int argc, char** argv) {
    int N;
    int x;

    // N (size of the NxN array)
    cin >> N; 
    int array[N][N];

    // Fill in the elements of the array below (full one row then move to the next row)
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            cin >> x;
            array[i][j] = x;
        } 
    }
    
    // DEFAULTLY AT THE CENTER OF THE MATRIX
    int row_boundary = N / 2;
    int col_boundary = N / 2;
    int row_min = row_boundary;
    int col_min = col_boundary;
    int toggle_found = -1;

    // LOOK FOR BOUNDARY THEN SEARCH FROM BOUNDARY - I LOOK AT SOLUTION FOR THIS
    // MY ORIGINAL ANSWER WAS SEARCHING FROM THE CENTER, RATHER THAN FROM THE BOUNDARY
    for (int i = 0; i < N; i++) {
        // check all the columns in first, last and center array
        if (array[row_min][col_min] > array[0][i]) {
            row_min = 0;
            col_min = i;
        }
        if (array[row_min][col_min] > array[N-1][i]) {
            row_min = N-1;
            col_min = i;
        }
        if (array[row_min][col_min] > array[N/2][i]) {
            row_min = N/2;
            col_min = i;
        }
        // check all the rows in first, last and center array
        if (array[row_min][col_min] > array[i][0]) {
            row_min = i;
            col_min = 0;
        }
        if (array[row_min][col_min] > array[i][N-1]) {
            row_min = i;
            col_min = N-1;
        }
        if (array[row_min][col_min] > array[i][N/2]) {
            row_min = i;
            col_min = N/2;
        }
        row_boundary = row_min;
        col_boundary = col_min;
    }

    while (0 <= row_boundary && row_boundary < N && 0 <= col_boundary && col_boundary < N) {
        // as soon as the current position satisfy the condition, we break the loop and return 
        if (array[row_boundary][col_boundary] < array[row_boundary+1][col_boundary] && array[row_boundary][col_boundary] < array[row_boundary-1][col_boundary] && array[row_boundary][col_boundary] < array[row_boundary][col_boundary+1] && array[row_boundary][col_boundary] < array[row_boundary][col_boundary-1]) {
            toggle_found = 1;
            break;
        }
        if (row_boundary == 0 && array[row_boundary][col_boundary] < array[row_boundary+1][col_boundary] && array[row_boundary][col_boundary] < array[row_boundary][col_boundary+1] && array[row_boundary][col_boundary] < array[row_boundary][col_boundary-1]) {
            toggle_found = 1;
            break;
        }
        if (row_boundary == N - 1 && array[row_boundary][col_boundary] < array[row_boundary-1][col_boundary] && array[row_boundary][col_boundary] < array[row_boundary][col_boundary+1] && array[row_boundary][col_boundary] < array[row_boundary][col_boundary-1]) {
            toggle_found = 1;
            break;
        }
        if (col_boundary == 0 && array[row_boundary][col_boundary] < array[row_boundary+1][col_boundary] && array[row_boundary][col_boundary] < array[row_boundary-1][col_boundary] && array[row_boundary][col_boundary] < array[row_boundary][col_boundary+1]) {
            toggle_found = 1;
            break;
        }
        if (col_boundary == N - 1 && array[row_boundary][col_boundary] < array[row_boundary+1][col_boundary] && array[row_boundary][col_boundary] < array[row_boundary-1][col_boundary] && array[row_boundary][col_boundary] < array[row_boundary][col_boundary-1]) {
            toggle_found = 1;
            break;
        }   
        // we search for the min element in the neighbor 
        if (array[row_boundary][col_boundary] > array[row_boundary+1][col_boundary])
            row_min = row_boundary + 1;
        if (array[row_min][col_boundary] > array[row_boundary][col_boundary+1])
            col_min = col_boundary + 1;
        if (array[row_min][col_min] > array[row_boundary-1][col_boundary])
            row_min = row_boundary - 1;
        if (array[row_boundary][col_boundary] > array[row_boundary][col_boundary-1])
            col_min = col_boundary - 1;

        // then at the end we continue searching from this min position
        row_boundary = row_min;
        col_boundary = col_min;
    }

    if (toggle_found == 1)
        cout << array[row_boundary][col_boundary] << endl;
    else        // if no element found after the loop then toggle_found still = -1
        cout << "No local minimum found!" << endl << endl;

    return 0;
}
