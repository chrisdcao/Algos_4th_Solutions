#include <iostream>
#include <vector>

using namespace std;

int BinarySearchA(int key, vector<int>& vec) {
    int lo = 0;
    int hi = vec.size() - 1;

    int temp;
    int FiboK = 0;
    int FiboK_1 = 1;
    
    while (FiboK <= vec.size()) {
        temp = FiboK;
        FiboK = FiboK + FiboK_1;
        FiboK_1 = temp;
    }

    while (lo <= hi) {
        // we narrow the range down close to the range of hi to lo
        // then repeat the process of examining each range of the 2 divided parts
        // we move the range backward to 1 unit of Fibo (Fk = Fk-1; Fk-1 = Fk-2)
        while (FiboK_1 > 0 && FiboK >= hi - lo) {
            temp = FiboK_1;
            FiboK_1 = FiboK - FiboK_1;      // Fk - 2
            FiboK = temp;           // Fk - 1 
        }

        // this index 'elementToCheck' act as the middle of the Normal BinarySearch
        // this is the boundary which we will compare our key to 
        // and determine which range will our search continues
        int elementToCheck = lo + FiboK_1;      // which is now Fk_2

        if (key < vec[elementToCheck]) hi = elementToCheck - 1;
        else if (key > vec[elementToCheck]) lo = elementToCheck + 1;
        else return elementToCheck;
    }
    return -1;
}

int main() {
    int x;
    string str;
    vector<int> vec;
    while (cin >> x) {
        vec.push_back(x);
    }

    //for (auto s : vec)
        //cout << s << endl;
    cin.clear();
    getline(cin, str);

    int key;
    cin >> key;
    cout << ((BinarySearchA(key,vec) != -1) ? "true" : "false") << endl;

    cin >> key;
    cout << ((BinarySearchA(key,vec) != -1) ? "true" : "false") << endl;

    cin >> key;
    cout << ((BinarySearchA(key,vec) != -1) ? "true" : "false") << endl;

    cin >> key;
    cout << ((BinarySearchA(key,vec) != -1) ? "true" : "false") << endl;

    cin >> key;
    cout << ((BinarySearchA(key,vec) != -1) ? "true" : "false") << endl;

    return 0;
}
