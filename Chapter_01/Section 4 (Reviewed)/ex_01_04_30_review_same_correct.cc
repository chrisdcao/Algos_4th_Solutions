#include <iostream>
#include <string>
#include "Stack.h"
#include "Steque.h" 

using namespace std;

template <typename T>
class Deque {
private:
    Stack<T> stackLeft;
    Steque<T> stequeRight;

public:
    Deque() {
        // not defined
    }

    int size() { return stackLeft.size() + stequeRight.size(); }
    bool isEmpty() { return stackLeft.isEmpty() && stequeRight.isEmpty(); }

    void pushLeft(T item) {
        stackLeft.push(item);
    }

    void pushRight(T item) {
        stequeRight.enqueue(item);
    }

    T popLeft() {
        if (isEmpty()) throw out_of_range("Underflow");
        if (stackLeft.isEmpty()) {
            int initSize = stequeRight.size();
            while (stequeRight.size() > 1)
                stackLeft.push(stequeRight.pop());
            T item = stequeRight.pop();
            while (initSize > 1) {
                stequeRight.push(stackLeft.pop());
                --initSize;
            }
            return item;
        }
        return stackLeft.pop();
    }

    T popRight() {
        if (isEmpty()) throw out_of_range("Underflow");
        if (stequeRight.isEmpty()) {
            while (!stackLeft.isEmpty())
                stequeRight.enqueue(stackLeft.pop());
            return stequeRight.pop();
        }
        return stequeRight.pop();
    }

};

int main() {
    Deque<string> deque;
    Deque<string> deque1;
    Deque<string> deque2;

    // if all elements on right
    // currentDeque: 
    //      left: khanh <-- long <-- khoa <-- kitty :right
    deque.pushRight("khanh");
    deque.pushRight("long");
    deque.pushRight("khoa");
    deque.pushRight("kitty");

    cout << deque.popLeft() << endl;
    cout << deque.popRight() << endl << endl;;

    // if all elements on left
    // currentDeque1: 
    //      left: cuong --> thinh --> phong :right
    deque1.pushLeft("phong");
    deque1.pushLeft("thinh");
    deque1.pushLeft("cuong");

    cout << deque1.popLeft() << endl;
    cout << deque1.popRight() << endl << endl;

    // if elements on both sides
    // currentDeque2: 
    //      left: cuong --> thinh --> phong || khanh <-- long <-- khoa <-- kitty :right
    deque2.pushLeft("phong");
    deque2.pushLeft("thinh");
    deque2.pushLeft("cuong");
    deque2.pushRight("khanh");
    deque2.pushRight("long");
    deque2.pushRight("khoa");
    deque2.pushRight("kitty");

    cout << deque2.popLeft() << endl;
    cout << deque2.popRight() << endl;

}
