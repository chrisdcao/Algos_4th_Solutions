#include <iostream>
#include <vector>

using namespace std;

int BinarySearchBound(int lo, int hi, vector<int>& vec) {
    int key = 1;
    while (lo <= hi)  {
        int mid = lo + (hi - lo) / 2;
        if (key > vec[mid]) lo = mid + 1;
        else if (key < vec[mid]) hi = mid - 1;
        else return mid;
    }

    return -1;
}

//finding the first floor when the break occurs is like finding most_left index of the key
//so this is the solution for logN

//solution for ~logN
int BinarySearchLeft(int lo, int hi, vector<int>& vec) {        
    int key = 1;
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key > vec[mid]) lo = mid + 1;
        else if (key < vec[mid] || mid > 0 && key == vec[mid-1]) hi = mid - 1;
        else {
            int most_Left = BinarySearchBound(lo, mid - 1, vec);
            if (most_Left != -1)  return most_Left;
            else return mid;
        }
    }

    return -1;
}

// solution for ~2logF
int BinarySearchLogF(vector<int>& vec) {
    int key = 1;
    int lo = 0;
    int boundary = 0;

    // terminate 2 corner cases
    if (vec[0] == 1) return 0;
    if (vec[vec.size() - 1] == 0) return -1;

    int i = 1;

    while (vec[i] != 1) {       // ~logF
        boundary = i;
        i *= 2;
        if (i >= vec.size() - 1) {
            i = boundary;
            break;
        }
    }

    // ~log(logF)
    return BinarySearchLeft(i/2, i, vec);
}

int main() {
    int x;
    string str;
    vector<int> vec;
    while (cin >> x) {
        vec.push_back(x);
    }

    cout << BinarySearchLogF(vec) << endl;

    cin.clear();
    getline(cin,str);

    vector<int> vec1;
    while (cin >> x) {
        vec1.push_back(x);
    }

    cout << BinarySearchLogF(vec1) << endl;

    return 0;
}
