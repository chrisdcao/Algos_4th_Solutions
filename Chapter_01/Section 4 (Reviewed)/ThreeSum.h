#ifndef THREESUM_H
#define THREESUM_H
#include <iostream>
#include <vector>
#include <cmath>
#include "BinarySearch.h"

using namespace std;

int ThreeSum(vector<int>& vec) {
    int count = 0;
    for (int i = 0; i < vec.size(); i++) {
        for (int j = i + 1; j < vec.size(); j++) {
            for (int k = j + 1; k < vec.size(); k++) {
                if (vec[i] + vec[j] + vec[k] == 0)
                    ++count;
            }
        }
    }
    return count;
}

int ThreeSumFast(vector<int>& vec) {
    int count = 0;
    for (int i = 0; i < vec.size(); i++) {
        for (int j = i + 1; j < vec.size(); j++) {
            if (BinarySearch(-vec[i]-vec[j], vec) > j)
                ++count;
        }
    }
    return count;
}

int ThreeSumNaive(vector<int>& vec) {
    int count = 0;
    for (int i = 0; i < vec.size(); i++) {
        for (int j = 0; j < vec.size(); j++) {
            for (int k = 0; k < vec.size(); k++) {
                if (i < j && j < k) {
                    if (vec[i] + vec[j] + vec[k] == 0)
                        count++;
                }
            }
        }
    }
    return count;
}
#endif
