#include <iostream>
#include <vector>
#include <cmath>
#include <cstdio>
#include "Timer.h"
#include "ThreeSum.h"
#include "Random.h"

using namespace std;

#define MAX 1000000

double timeTrialNaive(int N)  {
    Timer timer;
    timer.start();
    vector<int> a;
    for (int i = 0; i < N; i++) 
        a.push_back(randomUniformDistribution(-MAX,MAX));
    int cnt = ThreeSumNaive(a);
    timer.stop();
    return timer.elapsedSeconds();
}

double timeTrial(int N)  {
    Timer timer;
    timer.start();
    vector<int> a;
    for (int i = 0; i < N; i++) 
        a.push_back(randomUniformDistribution(-MAX,MAX));
    int cnt = ThreeSum(a);
    timer.stop();
    return timer.elapsedSeconds();
}

int main() {
    ios::sync_with_stdio(true);

    for (int N = 250; true; N += N) {
        double timeThreeSumNaive = timeTrialNaive(N);
        double timeThreeSum = timeTrial(N);
        double ratio = timeThreeSumNaive/timeThreeSum;
        printf("%s %7d %s %5.1f\n", "The ratio of Naive/Normal at N = ", N, "is :", ratio);
    }
}
