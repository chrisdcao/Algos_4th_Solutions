#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include "Random.h"
#include "Timer.h"
#include "ThreeSum.h"
#include "Stack.h"

using namespace std;

#define MAX 1000000

double timeTrial(int N)  {
    Timer timer;
    timer.start();
    vector<int> a;
    for (int i = 0; i < N; i++) 
        a.push_back(randomUniformDistribution(-MAX,MAX));
    int cnt = ThreeSum(a);
    timer.stop();
    return timer.elapsedSeconds();
}

int main(int argc, char** argv) {
    ios::sync_with_stdio(true);

    Stack<double> stackDev;
    for (int N = 250; true; N += N) {
        double count = 0;
        double sum = 0;
        double sum_dev = 0;
        double numeratorSum = 0.0;

        for (int i = 0; i < stoi(argv[1]); i++) {
            double time = timeTrial(N);
            printf("%s %5d %5.1f\n", "The current N-value is:", N, time);
            sum += time;
            count++;
            stackDev.push(time);
        }

        double mean = sum / count;

        while (!stackDev.isEmpty()) {
            double tu = pow(stackDev.pop() - mean,2);
            numeratorSum += tu;
        }

        double standard_dev = numeratorSum / count; 

        printf("%s %5.1f\n", "The mean of the current turn of trials is:", mean);
        printf("%s %5.1f\n", "The standard deviation previous-run trials is:", standard_dev);
    }
}
