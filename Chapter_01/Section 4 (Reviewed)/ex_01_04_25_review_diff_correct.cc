#include <iostream>
#include <vector>
#include <cmath>
#include <cstdio>

using namespace std;

// YOU ONLY HAVE 2 EGGS FOR THE TEST
// MEANING THAT, IF YOU THROW TO SEARCH FOR THE BOUNDARY, YOU LOSE ONE (EVERY THROW UNDER F THE EGG IS SAFE AND RE-USABLE)

int SquareRootSearchTwoLogN(vector<int>& vec) {
    if (vec[0] == 1) return 0;
    if (vec[vec.size() - 1] == 0) return -1;
    int step = sqrt(vec.size());
    int boundary = step;
    // ~ sqrt(N): as each step you step a value of sqrt(N), your maximum steps is sqrt(N) to reach the end of the array
    // one egg is broken (when we first reach/step inside the Broken Region)
    while(vec[boundary-1] == 0 && boundary <= vec.size() - 1) {
        if (boundary > vec.size() - 1)
            boundary = vec.size() - 1;
        boundary += step;
    }

    int maxBound = boundary;
    int minBound = maxBound - step;
    int i = minBound;
    // ~ sqrt(N): the number of values within this bound (from minBound to maxBound) is one step (or sqrt(N) number of values)
    // so in the worst case, if you walk them all, then you only did sqrt(N) step
    // one egg is broken (when we first reach the F floor)
    for (/**/; i < maxBound; i++) {
        if (vec[i] == 1)
            break;
    }
    return i;   
}   // total time complexity: ~ 2 * sqrt(N)
    // total eggs broken:       2 eggs

int SquareRootSearchCLogF(vector<int>& vec) {
    if (vec[0] == 1) return 0;
    if (vec[vec.size() - 1] == 0) return -1;
    int stepN = sqrt(vec.size());
    int boundaryF = stepN;

    // ~sqrt(F)
    while (vec[boundaryF - 1] == 0 && boundaryF < vec.size() - 1) {
        if (boundaryF > vec.size() - 1)
            boundaryF = vec.size() - 1;
        boundaryF += stepN;
    }
    int maxBound = boundaryF;
    int stepF = sqrt(boundaryF);
    int minBoundN = maxBound - stepN;
    int minBoundF = maxBound - stepF;
    int i = minBoundF;
    // ~sqrt(F) (stop right after)
    if (vec[minBoundF] == 0) {
        for (/**/; i < maxBound; i++)
            if (vec[i] == 1) return i;
    }
    // ~ O(1)
    if (vec[minBoundN] == 1) return minBoundN;
    // ~ sqrt(F)
    else {
        for(i = minBoundN; i < minBoundN + stepF; i++)
            if (vec[i] == 1) return i;
    }
    // ~sqrt(F)
    for(/**/; i < maxBound; i++) 
        if (vec[i] == 1) return i;
}   // total: ~ 2 * sqrt(F)

int main() {
    int x;
    vector<int> vec;
    while (cin >> x) 
        vec.push_back(x);
    cout << SquareRootSearchTwoLogN(vec) << endl;
    cout << SquareRootSearchCLogF(vec) << endl;
}
