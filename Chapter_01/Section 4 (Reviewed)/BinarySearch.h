#include <iostream>
#include <vector>

using namespace std;

int BinarySearch(int key, vector<int>& vec) {
    int lo = 0;
    int hi = vec.size() - 1;
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key > vec[mid]) lo = mid + 1;
        else if (key < vec[mid]) hi = mid - 1;
        else return mid;
    }
     return -1;
}
