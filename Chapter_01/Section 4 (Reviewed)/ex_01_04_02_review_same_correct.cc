#include <iostream>
#include <climits>
#include <limits>
#include <algorithm>
#include <memory>
#include <vector>

#define ll long long 

using namespace std;

// to check how many triples going to return the sum of 0

int BinarySearch(ll key, vector<ll> vec) {
    int low = 0;
    int hi = vec.size() - 1;
    while (low <= hi) {
        int mid = low + (hi - low) / 2;
        if (key < vec[mid]) hi = mid - 1;
        else if (key > vec[mid]) low = mid + 1;
        else return mid;
    }
    return -1;
}

int count(vector<ll>& a) {
    sort(a.begin(), a.end());
    int cnt = 0;
    for (int i = 0; i < a.size(); i++) {
        for (int j = i + 1; j < a.size(); j++) {
            if (a[i] + a[j] >= INT_MAX) { break; } 
            else {
                if (BinarySearch(-a[i]-a[j], a) > j)
                    cnt++;
            }
        }
    }
    return cnt;
}


int main() {
    ll x;
    vector<ll> Vec;
    while (cin >> x) {
        Vec.push_back(x);
    }
    cout << count(Vec) << endl;
}
