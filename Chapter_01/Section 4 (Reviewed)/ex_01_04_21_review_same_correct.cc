#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class StaticSETofInts {
private:
    vector<int> a;
    int BinarySearch(int key) {
        int lo = 0;
        int hi = a.size() - 1;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            if (key > a[mid]) lo = mid + 1;
            else if (key < a[mid]) hi = mid - 1;
            else return mid;
        }
        return -1;
    }

public:
    StaticSETofInts(vector<int>& keys) {
        for (int i = 0; i < keys.size(); i++)
            a.push_back(keys[i]);
        sort(a.begin(), a.end());
    }

    bool contains(int key) {
        return BinarySearch(key) != -1;
    }
};

int main() {
    int x;
    string str = "e";
    vector<int> vec;
    while (cin >> x)
        vec.push_back(x);
    cin.clear();

    StaticSETofInts test(vec);
    getline(cin,str);

    int key;
    cin >> key;
    cout << ((test.contains(key) == 0) ? "false" : "true") << endl;

    cin >> key;
    cout << ((test.contains(key) == 0) ? "false" : "true") << endl;

    cin >> key;
    cout << ((test.contains(key) == 0) ? "false" : "true") << endl;

    cin >> key;
    cout << ((test.contains(key) == 0) ? "false" : "true") << endl;
}
