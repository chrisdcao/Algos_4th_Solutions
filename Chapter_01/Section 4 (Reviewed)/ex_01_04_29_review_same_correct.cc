#include <iostream>
#include "Stack.h"

using namespace std;

template <typename T>
class StequeTwoStacks {
private:
    Stack<T> stackRight;
    Stack<T> stackLeft;

public:
    StequeTwoStacks() {
        // not defined
    }

    int size() { return stackLeft.size() + stackRight.size(); }
    bool isEmpty() { return stackLeft.isEmpty() && stackRight.isEmpty(); }

    void push(T item) {
        stackRight.push(item);
    }

    void enqueue(T item) {
        stackLeft.push(item);
    }

    T pop() {
        if (stackRight.isEmpty()) {
            while (!stackLeft.isEmpty()) {
                stackRight.push(stackLeft.pop());
            }
        }
        return stackRight.pop();
    }
};

int main() {
    StequeTwoStacks<string> steque;
    steque.push("phong");
    steque.push("thinh");
    steque.push("cuong");
    steque.enqueue("khanh");
    steque.enqueue("khoa");

    cout << steque.size() << endl;

    cout << steque.pop() << endl;
    cout << steque.pop() << endl;
    cout << steque.pop() << endl;
    cout << steque.pop() << endl;

    cout << steque.size() << endl;
}
