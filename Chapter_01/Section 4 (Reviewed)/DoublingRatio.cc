#include <iostream>
#include <vector>
#include <cstdio>
#include "Timer.h"
#include "Random.h"

using namespace std;

#define MAX 1000000

double timeTrial(int N)  {
    Timer timer;
    timer.start();
    vector<int> a;
    for (int i = 0; i < N; i++) 
        a.push_back(randomUniformDistribution(-MAX,MAX));
    int cnt = ThreeSum(a);
    timer.stop();
    return timer.elapsedSeconds();
}

int main(int argc, char** argv) {
    double prev = timeTrial(125);
    for (int N = 250; true; N += N) {
        double time = timeTrial(N);
        printf("%6d %7.1f ", N, time);
        printf("%5.1f\n", time/prev);
        prev = time;
    }
}
