1. Test with the case n = 0; n = 1; n = 2; n = 3 this statement all hold true (1)
2. Assume that the statement is true with N, meaning that from N items we can choose: N(N-1)(N-2) / 6
3. Then if we can prove that the statement also hold true for N+1 then it holds true for all cases (mathematical induction)

- Based on the model. from N+1 items we should be able to choose:
(N+1)(N+1-1)(N+1-2) / 6 = N(N+1)(N-1) / 6 different triples (2)


- Based on real calculation, we see:
<Number of triples from N+1> = <Number of triples from N> + <Number of new triples that only resulted from/thanks to (N+1)>*                // *: in other words, triples that must contain N+1 as one of the member
<Number of triples from N+1> = N(N-1)(N-2) / 6 + <Choose 2 from N (because N+1 is a must-have member, a 'fix', so we can only choose 2 to go with it from N)>              
<Number of triples from N+1> = N(N-1)(N-2) / 6 + N! / (2! * (N-2)!)
<Number of triples from N+1> = N(N-1)(N-2) / 6 + N(N-1) / 2
<Number of triples from N+1> = (N(N-1)(N-2) + 3N(N-1)) / 6
<Number of triples from N+1> = N(N-1)(N-2+3) / 6
<Number of triples from N+1> = N(N-1)(N+1) / 6 (3)

we see (3) = (2) -> this statement applies to all cases
