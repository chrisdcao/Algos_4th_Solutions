#include <iostream>
#include <array>
#include <cmath>
#include <vector>

using namespace std;

// Based on algebra we see that: If all 3 points are on the same line, they must belong to one line-equation
// Or in other words, the slope value: 'm' and the constant 'n' are fixed. We obtain 3 equations:
// y0 = mx0 + n     (1)
// y1 = mx1 + n     (2)
// y2 = mx2 + n     (3)
// (2) - (1) => y1 - y0 = m(x1-x0) + (n-n) => m = (y1-y0) / (x1-x0)     (4)          
// (3) - (2) => y2 - y1 = m(x2-x1) + (n-n) => m = (y2-y1) / (x2-x1)     (5)         
// => 3 points on same line iff: (y1-y0) / (x1-x0) = (y2-y1) / (x2-x1)  (6)     

// Based on (6): The two slopes must be equal if they are on the same line
// Taking what's given from the exercise, we have: y-coord = x-coord^3
// (b^3 - a^3)/(b-a) = (c^3 - b^3)/(c-b)            
// Taking what's given: a, b, c are distinctive (meaning the denominator != 0), divide both by the denominator
// b^2 + ab + a^2 = c^2 + bc + b^2
// Move all onto the left-side:
// ab - bc + a^2 - c^2 = 0
// b(a-c) + (a-c)(a+c) = 0
// (a - c)(a + b + c) = 0
// so either a = c (which cannot be because the points are distinct) or a + b + c = 0

int findTriplesAlgebra(vector<array<int,2>>& vec) {
    int count = 0;

    for (int i = 0; i < vec.size(); i++) {
        double x0 = vec[i][0]*1.00;
        double y0 = vec[i][1]*1.00;
        for (int j = i + 1; j < vec.size(); j++) {
            double x1 = vec[j][0]*1.00;
            double y1 = vec[j][1]*1.00;
            for (int k = j + 1; k < vec.size(); k++) {
                double x2 = vec[k][0]*1.00;
                double y2 = vec[k][1]*1.00;
                if ((y2-y1)/(x2-x1) == (y1-y0)/(x0-x1) && x1 != x2 && x1 != x0)     // else we cannot divide the denominator
                    ++count;
                else if (x1 == x2 && x1 == x0)
                    ++count;
            }
        }
    }

    return count;
}

int main() {
    vector<array<int,2>> vec = {{1,2}/*, {3,4}, {5,6}, {7,8}, {9,10}*/, {1,3}, {1,4}};
    vector<array<int,2>> vec2 = {{2,2}, {3,4}, {7,6}, {7,8}, {6,10}, {0,3}, {0,4}};
    cout << findTriplesAlgebra(vec) << endl;
    cout << findTriplesAlgebra(vec2) << endl;
}
