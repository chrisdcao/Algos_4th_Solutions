#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

int count(vector<int>& vec) { 
    if (vec.size() <= 1) return 0;

    // ~ O(Nlog N)
    sort(vec.begin(), vec.end());

    int i = 0;
    int j = i;
    int cnt = 0;
    int sum = 0;

    // ~ O(N)
    while (j < vec.size()) {
        if (vec[i] == vec[j]) {
            ++cnt; 
            ++j;
        }
        if (vec[i] != vec[j] || j == vec.size()) {
            sum += cnt * (cnt - 1) / 2;
            cnt = 0;
            i = j;
        }
    }

    return sum;
}
// total: NlogN

// client
int main() {
    vector<int> vec;
    int x;
    while (cin >> x) 
        vec.push_back(x);
    cout << count(vec) << endl;
    return 0;
}
