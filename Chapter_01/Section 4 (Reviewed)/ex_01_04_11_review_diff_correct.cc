#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int BinarySearchBound(int key, int lo, int hi, vector<int>& vec) {
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key > vec[mid]) lo = mid + 1;
        else if (key < vec[mid]) hi = mid - 1;
        else return mid;
    }
    return -1;
}

int BinarySearchRight(int key, vector<int>& vec) {          // ~ O(log(N))
    int lo = 0;
    int hi = vec.size() - 1;
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key > vec[mid] || mid > 0 && key == vec[mid + 1]) lo = mid + 1;
        else if (key < vec[mid]) hi = mid - 1;
        else {
            int maxIndex = BinarySearchBound(key, mid + 1, hi, vec);
            if (maxIndex != -1) return maxIndex;
            else return mid;
        }
    }
    return -1;
}

int BinarySearchLeft(int key, vector<int>& vec) {           // ~ O(log(N))
    int lo = 0;
    int hi = vec.size() - 1;
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key > vec[mid]) lo = mid + 1;
        else if (key < vec[mid] || mid > 0 && key == vec[mid - 1]) hi = mid - 1;
        else {
            int minIndex = BinarySearchBound(key, lo, mid - 1, vec);
            if (minIndex != -1) return minIndex;
            else return mid;
        }
    }
    return -1;
}

class StaticSETofInts {
private:
    vector<int> a;

public:
    StaticSETofInts() = default;
    StaticSETofInts(vector<int>& keys) {
        for (int i = 0; i < keys.size(); i++)
            a.push_back(keys[i]);
        sort(a.begin(), a.end());
    }

    bool contains(int key) { return BinarySearch(key) != -1; }

    int howMany(int key) {          // ~ O(2 * log(N)) = O(log(N))
        int minIndex = BinarySearchLeft(key, a);
        int maxIndex = BinarySearchRight(key, a);
        if (minIndex == -1 && maxIndex == -1) return 0;
        else return maxIndex - minIndex + 1;
    }

    int BinarySearch(int key) {
        int lo = 0;
        int hi = a.size() - 1;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            if (key > a[mid]) lo = mid + 1;
            else if (key < a[mid]) hi = mid - 1;
            else return mid;
        }
        return -1;
    }
};

int main() {
    vector<int> vec;
    int x;
    while (cin >> x) {
        vec.push_back(x);
    }
    StaticSETofInts vec2(vec);
    cout << vec2.howMany(2) << endl;
    cout << vec2.howMany(16) << endl;
    cout << vec2.howMany(128) << endl;
    cout << vec2.howMany(-99) << endl;
    return 0;
}
