#include <iostream>
#include <cstring>
#include <cstdio>
#include <string>
#include "Queue.h"

using namespace std;

template <typename T>
class StackWithQueue {
private:
    Queue<T> queue;

public:
    StackWithQueue() {
        /* not defined */
    }

    int size() { return queue.size(); }
    bool isEmpty() { return queue.isEmpty(); }

    void push(T item) {
        queue.enqueue(item);
    }

    T pop() {
        int a = queue.size();
        while(a - 1 > 0) {
            queue.enqueue(queue.dequeue());
            --a;
        }
        return queue.dequeue();
    }
};

int main(int argc, char** argv) {
    StackWithQueue<string> stack;
    stack.push(argv[1]);
    stack.push(argv[2]);
    stack.push(argv[3]);
    stack.push(argv[4]);
    stack.push(argv[5]);

    cout << stack.size() << endl;

    cout << stack.pop() << " " << stack.pop() << " " << stack.pop() << " " << stack.pop() << " " << stack.pop() << endl;

    cout << stack.size() << endl;

    return 0;
}

