#include <iostream>
#include <math.h>
#include <algorithm>
#include <vector>

using namespace std;

// TWO SUM FASTER
int count2Sum(vector<int>& vec) {
    sort(vec.begin(), vec.end());
    int j = vec.size() - 1; 
    int i = 0;
    int cnt = 0;
    int sum = 0;
    int j_bound = 0;

    if (vec[i] > 0 || vec[vec.size() - 1] < 0)  return cnt;                            // ~ 1
    
    for (/**/; j_bound < vec.size() - 1; j_bound++) {       // ~ N
        if (vec[j_bound] >= 0)
            break;
    }

    if (vec[j] + vec[j_bound-1] < 0) return cnt;            // MAX_POSSIBLE_VALUE = max positive + min negative. If < 0 then there won't be any pair that = 0
    if (vec[i] + vec[j_bound] > 0) return cnt;              // MIN_POSSIBLE_VALUE = min positive + max negative. If > 0 then there won't be any pair that = 0

    while (vec[i] < 0 || j >= j_bound) {                    // ~ N
        if (vec[j] == 0) {
            int cnt_zero = 0;
            while (vec[j] == 0 && j >= j_bound) {
                sum += cnt_zero;                                // caculating 2CN fast
                cnt_zero++;
                --j;
            }
        } else {
            if (vec[i] + vec[j]  < 0) ++i; 
            if (vec[i] + vec[j] == 0) {
                int i_temp = i;
                int j_temp = j;
                int cnt1 = 0;
                int cnt2= 0;
                while (vec[j_temp] + vec[i] == 0) {
                    --j_temp;
                    ++cnt1;
                }
                while (vec[i_temp] + vec[j] == 0) {
                    ++i_temp;
                    ++cnt2;
                }
                cnt += cnt1 * cnt2;
                i = i_temp;
                j = j_temp;
            }
            if (vec[i] + vec[j] > 0) --j;
        }
    }                                       

    return cnt + sum;
}                       


// RUNS IN N^2 IN ALL CASES (FOR SORTED ARRAY)
int count3Sum(vector<int>& vec) {
    sort(vec.begin(), vec.end());
    int cnt = 0;
    int sum = 0;
    int positive_bound = 0;             // first element that's positive (smallest element index that's positive)
                                        // positive_bound - 1 will return the last element that's negative

    if (vec[0] > 0 || vec[vec.size() - 1] < 0) return cnt;                                                     // BEST CASE: ~ O(1)

    for (/**/; positive_bound < vec.size(); ++positive_bound) {
        if (vec[positive_bound] >= 0)
            break;
    }

    if (positive_bound == 0) {          // if the first element to be positive is the first element of the sorted array
        while (vec[positive_bound] == 0 && positive_bound < vec.size()) {
            ++cnt;
            ++positive_bound;
        }
        if (cnt < 3) return 0;                          // if there's less than 3 elements that's == 0 then NO TRIO as well
        else if (cnt == 3) return 1;                    // if there's 3 then 1
        else return cnt * (cnt -1) * (cnt - 2) / 6;     // else it's 3CN (with N being number of 0)
    } else {
        // THE ALL '0' TRIO
        int cnt_zero = 0;
        int zero_indexes = positive_bound;
        if (vec[positive_bound] == 0) {
            while (vec[zero_indexes] == 0 && zero_indexes < vec.size()) {
                ++cnt_zero;
                ++zero_indexes;
            }
        }
        if (cnt_zero < 3) sum = 0;     
        else if (cnt_zero == 3) sum = 1;
        else sum = cnt_zero * (cnt_zero - 1) * (cnt_zero - 2) / 6;
    }

    for (int i = 0; i < positive_bound; i++) {           // ~ N
        int j = i + 1;
        int k = vec.size() - 1;
        while (j <= positive_bound) {                    // ~ N. Don't need && != 0 as 'i' will never be 0 -> we will never violate ALL-ZERO-TRIO
            if (vec[j] + vec[k]  < -vec[i]) ++j; 
            if (vec[j] + vec[k] == -vec[i]) {
                int j_temp = j;
                int k_temp = k;
                int cnt1 = 0;
                int cnt2= 0;
                while (vec[k_temp] + vec[j] == -vec[i]) {
                    --k_temp;
                    ++cnt1;
                }
                while (vec[j_temp] + vec[k] == -vec[i]) {
                    ++j_temp;
                    ++cnt2;
                }
                cnt += cnt1 * cnt2;
                j = j_temp;
                k = k_temp;
            }
            if (vec[j] + vec[k] > -vec[i]) --k;
        }                                     
    }                                                   // total: ~ N^2

    return cnt + sum;
}

int main() {
    int x;
    vector<int> vec;
    while (cin >> x)
        vec.push_back(x);
    cout << "The 2 Sum is: " << count2Sum(vec) << endl;
    cout << "The 3 Sum is: " << count3Sum(vec) << endl;
    return 0;
}
