#include <iostream>
#include <string>
#include <vector>
#include "Stack.h"

using namespace std;

template <typename T>
class DequeWithThreeStack {
private:
    Stack<T> stackLeft;
    Stack<T> stackRight;
    Stack<T> stackOutside;

public:
    DequeWithThreeStack() {
        // not defined
    }

    int size() { return stackLeft.size() + stackRight.size(); }
    bool isEmpty() { return stackLeft.isEmpty() && stackRight.isEmpty(); } 

    void pushRight(T item) {
        stackRight.push(item);
    }

    void pushLeft(T item) {
        stackLeft.push(item);
    }

    T popLeft() {
        if (isEmpty()) throw out_of_range("underflow");
        if (stackLeft.isEmpty()) {
            int initSize = stackRight.size();
            while (stackRight.size() > 1)
                stackOutside.push(stackRight.pop());
            T item = stackRight.pop();
            while (initSize > 1) {
                stackRight.push(stackOutside.pop());
                --initSize;
            }
            return item;
        }
        return stackLeft.pop();
    }
    T popRight() {
        if (isEmpty()) throw out_of_range("underflow");
        if (stackRight.isEmpty()) {
            int initSize = stackLeft.size();
            while (stackLeft.size() > 1)
                stackOutside.push(stackLeft.pop());
            T item = stackLeft.pop();
            while (initSize > 1) {
                stackLeft.push(stackOutside.pop());
                --initSize;
            }
            return item;
        }
        return stackRight.pop();
    }
};

int main() {
    DequeWithThreeStack<string> DequeWithThreeStack1;
    DequeWithThreeStack<string> DequeWithThreeStack2;
    DequeWithThreeStack<string> DequeWithThreeStack3;

    // if all elements on right
    // currentDequeWithThreeStack: 
    //      left: khanh <-- long <-- khoa <-- kitty :right
    DequeWithThreeStack1.pushRight("khanh");
    DequeWithThreeStack1.pushRight("long");
    DequeWithThreeStack1.pushRight("khoa");
    DequeWithThreeStack1.pushRight("kitty");

    cout << DequeWithThreeStack1.popLeft() << endl;
    cout << DequeWithThreeStack1.popRight() << endl << endl;;

    // if all elements on left
    // currentDequeWithThreeStack1: 
    //      left: cuong --> thinh --> phong :right
    DequeWithThreeStack2.pushLeft("phong");
    DequeWithThreeStack2.pushLeft("thinh");
    DequeWithThreeStack2.pushLeft("cuong");

    cout << DequeWithThreeStack2.popLeft() << endl;
    cout << DequeWithThreeStack2.popRight() << endl << endl;

    // if elements on both sides
    // currentDequeWithThreeStack2: 
    //      left: cuong --> thinh --> phong || khanh <-- long <-- khoa <-- kitty :right
    DequeWithThreeStack3.pushLeft("phong");
    DequeWithThreeStack3.pushLeft("thinh");
    DequeWithThreeStack3.pushLeft("cuong");
    DequeWithThreeStack3.pushRight("khanh");
    DequeWithThreeStack3.pushRight("long");
    DequeWithThreeStack3.pushRight("khoa");
    DequeWithThreeStack3.pushRight("kitty");

    cout << DequeWithThreeStack3.popLeft() << endl;
    cout << DequeWithThreeStack3.popRight() << endl;

}
