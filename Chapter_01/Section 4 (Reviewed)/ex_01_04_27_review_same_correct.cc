#include <iostream>
#include <string>
#include "Stack.h"

using namespace std;

template <typename T> 
class QueueWith2Stacks {
private:
    Stack<T> stackStore;
    Stack<T> stackReverse;
    int N;

public:
    QueueWith2Stacks() {
        /* not defined */
    }

    int size() { return stackStore.size(); }
    bool isEmpty() { return stackStore.isEmpty(); }

    void enqueue(T item) {
        stackStore.push(item);
    }

    T dequeue() {
        int initSize = stackStore.size();
        while(!stackStore.isEmpty())
            stackReverse.push(stackStore.pop());
        T item = stackReverse.pop();
        while(initSize > 1) {
            stackStore.push(stackReverse.pop());
            --initSize;
        }
        return item; 
    }

};

int main() {
    QueueWith2Stacks<string> queue;
    queue.enqueue("phong");
    queue.enqueue("thinh");
    queue.enqueue("cuong");
    queue.enqueue("khanh");

    cout << queue.size() << endl;

    cout << queue.dequeue() << endl;

    queue.enqueue("phong1");
    queue.enqueue("phong2");
    queue.enqueue("phong3");
    cout << queue.dequeue() << endl;
    cout << queue.dequeue() << endl;
    cout << queue.dequeue() << endl;
    cout << queue.dequeue() << endl;
    cout << queue.dequeue() << endl;
    cout << queue.dequeue() << endl;

    cout << queue.size() << endl;
}

