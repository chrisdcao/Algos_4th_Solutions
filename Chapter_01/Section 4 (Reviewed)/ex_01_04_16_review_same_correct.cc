#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <cmath>

using namespace std;

// LAST RESORT, USING STD::SORT WHICH HAVE TIME COMPLEXITY OF N logN
// When an array is sorted, the relative minimum value of difference are between 2 closest number
// Then we will compare these relative minimums
int main() {
    ios::sync_with_stdio(true);
    double x;
    vector<double> vec;
    while (cin >> x) 
        vec.push_back(x);

    sort(vec.begin(), vec.end());                           // ~ N * log N

    int i = 0;
    for (int a = 1; a < vec.size() - 1; a++) {                  // ~ N
        if (vec[a + 1] - vec[a] == 0) {
            i = a;
            break;
        }
        if ((vec[a + 1] - vec[a]) < (vec[i + 1] - vec[i]))     
            i = a;
    }
    cout << "The closest pair is: ";
    printf("%.1f %.1f\n", vec[i], vec[i + 1]);

    return 0;
}

