#ifndef QUEUE_H
#define QUEUE_H
#include <iostream>
#include <string>
#include <vector>
#include <memory>
using namespace std;

template <typename T>
class Queue {
private:
    class Node {
    public:
        T item;
        Node* next;
        int index = -1;
    };

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;

    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* InitLoc) { current = InitLoc; }
        
        Iterator& operator++() {
            current = current->next;
            return *this;
        }

        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }

        T operator*() {
            return this->current->item;
        }
    };

    Node* tail;
    Node* head;
    int N;

public:
    Queue() {
        N = 0;
        head = NULL;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void enqueue(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        N++;
    }

    T dequeue() {
        //if (N == 0) return T();
        T item = head->item;
        Node* oldHead = head;
        head = head->next;
        freeNode(oldHead);
        N--;
        return item;
    }

    Iterator begin() { return Iterator(head); }
    Iterator end() { return Iterator(tail->next); }
};

#endif
