#include <iostream>
#include <vector>
#include <cstdio>
#include <algorithm>
using namespace std;

int localMinimum(vector<int>& vec) {
    if (vec.size() == 0) return -1;
    if (vec.size() == 1) return 0;
    if (vec.size() == 2) {
        if (vec[0] < vec[1])
            return 0;
        else return 1;
    }

    int lo = 0;
    int hi = vec.size() - 1;
    while (lo <= hi) {          // ~ logN
        int mid = lo + (hi - lo) / 2;
        if (vec[mid] < vec[mid-1] && vec[mid] > vec[mid+1]) 
            lo = mid + 1;
        else if (vec[mid] > vec[mid-1] && vec[mid] < vec[mid+1]) 
            hi = mid - 1;
        else if (vec[mid] < vec[mid-1] && vec[mid] < vec[mid+1])
            return mid;
        else 
            hi = mid - 1;
        if (mid == 0) 
            if (vec[mid] < vec[mid + 1]) return mid;
        if (mid == vec.size() - 1)
            if (vec[mid] < vec[mid - 1]) return mid;
    }

    return -1;
}                               // ~ 2logN

int main() {
    int x;
    vector<int> vec;
    while (cin >> x)
        vec.push_back(x);
    cout << "The index of the Local Minimum: " << localMinimum(vec) << endl;
    return 0;
}
