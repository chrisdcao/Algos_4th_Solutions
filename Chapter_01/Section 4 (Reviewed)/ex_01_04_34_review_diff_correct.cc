#include <iostream>
#include <vector>
#include <random>
#include <cmath>
#include <cstdio>

#define hotter   1 
#define colder   0
#define neutral -1

using namespace std;

// you cannot use > or <
// you can only use ==, hotter or colder
// each time you travel, you cannot know whether your current position is > or <, or even how far from the key is it
// you can only know if it's nearer or not

int BinarySearch(int key, vector<int>& vec) {
    int lo = 0;
    int hi = vec.size() - 1;
    while(lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key > vec[mid]) lo = mid + 1;
        else if (key < vec[mid]) hi = mid - 1;
        else return mid;
    }
    return -1;
}

class HotOrCold {
private:
    int key_index;
    int oldDistance;
    int distance;
    int count;

public:
    HotOrCold(int key, vector<int>& vec) {
        key_index = BinarySearch(key, vec);
        oldDistance = (0 + (vec.size() - 1 - 0) / 2) - key_index;
        count = 0;
    }

    int temperature(int index) {
        if (count > 0)
            oldDistance = distance;
        distance = abs(index - key_index);
        //cout << oldDistance << " " << distance << endl;

        if (distance < oldDistance) {
            oldDistance = distance;
            return hotter;
        }
        if (distance > oldDistance) {
            oldDistance = distance;
            return colder;
        } 
        ++count;
        return neutral;
    }
};

// ~ 1logN solution: where you calculate on both sides of mid
// whichever hotter you follow, stop when equal
int BinaryTemp(int key, vector<int>& vec) {
    if (key > vec.size() - 1) return -1;
    if (key < 0) return -1;
    int lo = 0;
    int hi = vec.size() - 1;
    HotOrCold hotOrCold(key, vec);
    int mid = lo + (hi - lo) / 2;
    if (vec[mid] == key) return mid;
    while (lo <= hi) {
        int mid1 = lo + (mid - lo) / 2;
        int mid2 = mid + (hi - mid) / 2;

        int temp1 = hotOrCold.temperature(mid1);
        int temp2 = hotOrCold.temperature(mid2);

        if (key == vec[mid1]) return mid1;
        if (key == vec[mid2]) return mid2;
        if (key == vec[mid]) return mid;
        if (temp2 == colder) {
            hi = mid;
            mid = mid1;
        } else if (temp2 = hotter) {
            lo = mid;
            mid = mid2;
        } else return mid1+(mid2-mid1)/2;
    }
    return -1;
}

int main() {
    vector<int> vec;
    for (int x = 1; x < 100; x++)
        vec.push_back(x);

    cout << "Expected: 2" << ". Real outcome: \n";  // from 1 to N, the index is k-1
    cout << BinaryTemp(3,vec) << endl;
    cout << "Expected: 4" << ". Real outcome: \n";  // from 1 to N, the index is k-1
    cout << BinaryTemp(5,vec) << endl;
    cout << "expected: 56" << ". real outcome: \n";  // from 1 to n, the index is k-1
    cout << BinaryTemp(57,vec) << endl;

    vector<int> vec2;
    for (int x = 0; x < 20; x++)
        vec2.push_back(x);
    cout << "expected: 11" << ". real outcome: \n";  // from 1 to n, the index is k-1
    cout << BinaryTemp(12, vec) << endl;

    vector<int> vec3;
    for (int x = 0; x < 10; x++)
        vec3.push_back(x);
    cout << "expected: 2" << ". real outcome: \n";   // from 1 to n, the index is k-1
    cout << BinaryTemp(3, vec) << endl;
    cout << "expected: -1" << ". real outcome: \n";  // from 1 to n, the index is k-1
    cout << BinaryTemp(11, vec) << endl;
}
