#ifndef STEQUE_H
#define STEQUE_H
#include <iostream>
#include <vector>
#include <memory>

using namespace std;

template <typename T>
class Steque {
private:
    class Node {
    public:
        T item;
        Node* prev = NULL;
        Node* next = NULL;
        int index = -1;
    };

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;

    Node* head;
    Node* tail;
    int Nleft;
    int Nright;


public:
    Steque() {
        Nleft = 0;
        Nright = 0;
        tail = NULL;
        head = NULL;
    }

    int size() { return Nleft + Nright; }
    bool isEmpty() { return Nleft == 0 && Nright == 0; }

    void push(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->prev = NULL;
        if (size() == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->next = head;
            head->prev = temp;
            head = temp;
        }
        ++Nleft;
    }

    void enqueue(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->next = NULL;
        if (size() == 0) {
            temp->prev = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->prev = tail;
            tail->next =temp;
            tail = temp;
        }
        ++Nright;
    }

    T pop() {
        if (size() == 0) 
            throw out_of_range("no more item to pop!");
        T popItem;
        if (Nleft != 0) {
            popItem = head->item;
            Node* oldHead = head;
            head = head->next;
            if (head != NULL)
                head->prev = NULL;
            freeNode(oldHead);
            --Nleft;
        } else {
            popItem = tail->item;
            Node* oldTail = tail;
            tail = tail->prev;
            if (tail != NULL)
                tail->next = NULL;
            freeNode(oldTail);
            --Nright;
        }
        return popItem;
    }

};
#endif
