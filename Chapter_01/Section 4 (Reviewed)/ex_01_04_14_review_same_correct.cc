#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int BinarySearchBound(int, int, int, vector<int>&);
int BinarySearchLeft(int, vector<int>& key);
int BinarySearchRight(int, vector<int>& key);
int BinarySearch(int, vector<int>& vec);

// 4SUM - WORK WITH ALL KINDS OF ARRAYS
int countRepeated(vector<int>& vec) {
    sort(vec.begin(), vec.end());
    int cnt = 0;
    int N = vec.size();
    for (int i = 0; i < N; i++) {
        for (int j = i + 1; j < N; j++) {
            for (int k = j + 1; k < N; k++) {
                int left = BinarySearchLeft(-vec[i]-vec[j]-vec[k], vec);
                int right = BinarySearchRight(-vec[i]-vec[j]-vec[k], vec);
                if (right > k) {
                    if (left > k)
                        cnt += right - left + 1;
                    else
                        cnt += right - k;
                }
            }
        }
    }
    return cnt;
}

// 4SUM - WORK ONLY WITH DISTINCTIVE ELEMENTS
int count(vector<int>& vec) {
    sort(vec.begin(), vec.end());
    int cnt = 0;
    int N = vec.size();
    for (int i = 0; i < N; i++) {
        for (int j = i + 1; j < N; j++) {
            for (int k = j + 1; k < N; k++) {
                if (BinarySearch(-vec[i]-vec[j]-vec[k]) > k)
                    cnt++;
            }
        }
    }
    return cnt;
}

// client test with 4SUM WORKING with all kinds of arrays
int main(int argc, char** argv) {
    vector<int> vec;
    int x;
    while (cin >> x)
        vec.push_back(x);
    cout << countRepeated(vec) << endl;
}

int BinarySearchLeft(int key, vector<int>& vec) {       // ~ O(log(N))
    int lo = 0;
    int hi = vec.size() - 1;
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key < vec[mid] || mid > 0 && key == vec[mid-1]) hi = mid - 1;
        else if (key > vec[mid]) lo = mid + 1;
        else {
            int minIndex = BinarySearchBound(key, lo, mid - 1, vec);
            if (minIndex != -1) return minIndex;
            else return mid;
        }
    }
    return -1;
}

int BinarySearchRight(int key, vector<int>& vec) {       // ~ O(log(N))
    int lo = 0;
    int hi = vec.size() - 1;
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key < vec[mid]) hi = mid - 1;
        else if (key > vec[mid] || mid > 0 && key == vec[mid+1]) lo = mid + 1;
        else {
            int maxIndex = BinarySearchBound(key, mid + 1, hi, vec);
            if (maxIndex != -1) return maxIndex;
            else return mid;
        }
    }
    return -1;
}

int BinarySearchBound(int key, int lo, int hi, vector<int>& vec) {
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key < vec[mid]) hi = mid - 1;
        else if (key > vec[mid]) lo = mid + 1;
        else return mid;
    }
    return -1;
}

int BinarySearch(int key, vector<int>& vec) {
    int lo = 0;
    int hi = vec.size() - 1;
    while (lo <= hi) {
        int mid = lo + (hi - lo);
        if (key > vec[mid]) lo = mid + 1;
        else if (key < vec[mid]) hi = mid - 1;
        else return mid;
    }
    return -1;
}
