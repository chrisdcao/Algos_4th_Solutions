#include <iostream>
#include <vector>

using namespace std;

int main() {
    //vector<int> vec1 = {1,2,3,3,4,5,6,7,7,8};
    //vector<int> vec2 = {2,2,2,2,4,4,4,5,5,6};
    
    vector<int> vec1 = {0,1,2,2,5,6,6,8,25,25};
    vector<int> vec2 = {-2,0,1,2,2,2,3,4,5,10,20,25,25};

    int i = 0;
    int j = 0;

    while (i < vec1.size() && j < vec2.size()) {
        while (vec1[i] < vec2[j]) ++i;
        while (vec1[i] > vec2[j]) ++j;
        if (vec1[i] == vec2[j]) {
            if (i == 0)
                cout << vec1[i] << endl;
            else {
                if (vec1[i] != vec1[i-1])
                    cout << vec1[i] << endl;
            }
            ++i; ++j;
        }
    }

    return 0;
}
