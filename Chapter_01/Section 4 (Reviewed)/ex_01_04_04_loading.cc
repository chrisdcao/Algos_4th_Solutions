#include <iostream>
#include <algorithm>
#include <vector>
#include <memory>

using namespace std;

#define ll long long

int BinarySearch(ll key, vector<ll> vec) {
    int lo = 0;
    int hi = vec.size() - 1;
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key < vec[mid]) hi = mid - 1;
        else if (key > vec[mid]) lo = mid + 1;
        else return mid;
    }
    return -1;
}

// TWO SUM FAST
int count(vector<ll>& a) {
    sort(a.begin(), a.end()) ;
    int cnt = 0;
    for (int i = 0; i < a.size(); i++)          // calling vec.size() to be N, this step costs N
        if (BinarySearch(-a[i], a) > i)         // BinarySearch costs log(N) each run
            ++cnt;                              // increment costs '1' each run
    return cnt;                                 // total: N * (log(N) + 1) ~ O(Nlog(N))
}

// TWO SUM
int count(vector<ll>& a) {
    sort(a.begin(), a.end()) ;
    int cnt = 0;
    for (int i = 0; i < a.size(); i++)          
        for (int j = i; j < a.size(); j++) {    // the two step cost N * (N + 1) / 2
            if (a[i] + a[j] == 0)
                ++cnt;                          // increment costs '1' each run
        }
    return cnt;                                 // total: N^2 + N / 2  ~ O(N^2)
}

// client
int main() {
    ll x;
    vector<ll> Vec;
    while (cin >> x) {
        Vec.push_back(x);
    }
    cout << count(Vec) << endl;
}
