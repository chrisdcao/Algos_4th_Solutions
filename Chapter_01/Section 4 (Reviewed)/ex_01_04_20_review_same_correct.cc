#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

// Bitonic is just 2 sorted array faced into each other like this:
//       |
//      ||
//     ||||
//   .||||||.
// we conduct 2 separate BinarySearch on this Bitonic to find the key
// but first, we have to find the peak (the boundary where the direction change)
// which is actually the local maximum, because it's the only point larger than both sides (which does not happen in any sorted-distinctive array)
// so we apply the mindset of finding local-minimum to find local-maximum: which should take ~logN
// then 2 BinarySearch on 2 sorted array ~ 2 logN
// total: ~ 3 logN

// Small - go right, Big - go left (REVERSE!)
int BinarySearchBoundReverse(int key, int lo, int hi, vector<int>& vec) {
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key > vec[mid]) hi = mid - 1;
        else if (key < vec[mid]) lo = mid + 1;
        else return mid;
    }
    return -1;
}

// Small - go left, Big - go right (NORMAL)
int BinarySearchBound(int key, int lo, int hi, vector<int>& vec) {
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key > vec[mid]) lo = mid + 1;
        else if (key < vec[mid]) hi = mid - 1;
        else return mid;
    }
    return -1;
}

int localMaximum(vector<int>& vec) {
    int lo = 0;
    int hi = vec.size() - 1;
    while (lo <= hi) {
        // in a 2-sorted arrays like bitonic, there's no local minimum, so we don't need to cover the case
        int mid = lo + (hi - lo) / 2;
        // where vec[mid] < vec[mid+1] && vec[mid] < vec[mid-1]
        if (vec[mid] > vec[mid+1] && vec[mid] > vec[mid-1])
            return mid;
        else if (vec[mid] > vec[mid+1] && vec[mid] < vec[mid-1])
            hi = mid - 1;
        else if (vec[mid] < vec[mid+1] && vec[mid] > vec[mid-1])
            lo = mid + 1;
        else continue;
    }
    return -1;
}

int main() {
    int key;
    cin >> key;

    vector<int> vec;
    int x;
    while (cin >> x)
        vec.push_back(x);

    // this is the index where direction changes
    // cut the vector into 2 sorted array from here and 
    // perform BinarySearch on each for the key
    // keep in the mind the 2nd array is reverse
    int boundary = localMaximum(vec);
    //cout << boundary << endl;

    int leftside_result = BinarySearchBound(key, 0, boundary, vec);
    int rightside_result = BinarySearchBoundReverse(key, boundary, vec.size() - 1, vec);

    //cout << leftside_result << " " << rightside_result << endl;

    if (leftside_result == -1) {
        if (rightside_result == -1) {
            cout << leftside_result << endl;
        } else
            cout << rightside_result << endl;
    } else
        cout << leftside_result << endl;

    return 0;
}
