#include <iostream>
#include <iterator>
using namespace std;

int gcd(int a, int b) {
    if (b == 0) return a;
    int r = a % b;
    return gcd(b, r);
}

int main() {
    int n,          // array-size
        r = 0,      // rows marking
        c = 0;      // columns marking
    cout << "Please input the desired size of the N x N array: ";
    cin >> n;
    cout << endl;
    bool a[n][n];
    for (int i = 0; i < n; i++) {
        cout << "   "<< c << "  ";
        c++;
    }
    cout << endl;
    for (int i = 0; i < n; i++) {
        cout << r << " ";
        for (int j = 0; j < n; j++) {
            if (gcd(i, j) == 1)
                a[i][j] = true;
            else 
                a[i][j] = false;
            if (a[i][j])
                cout << "true" << " ";
            else
                cout << "false" << " ";
        }
        r++;
        cout << "\n" << endl;
    }
    return 0;
}

