// We have a program like this
#include <iostream>
using namespace std;

int main() {
    int a,b,c;
    cin >> a >> b >> c;
    if (a>b) { t = a; a = b; b = t;}
    if (a>c) { t = a; a = c; c = t;}
    if (b>c) { t = b; b = c; c = t;}
    return 0;
}

// The nature of each code block within the 'if' statement 
// is to swap the position of (a & b) if a > b (t acts as intermediary)
// and of (a & c) if a > c
// and of (b & c) if b > c
// and so the smallest number will always be swapped/pushed to the left side of the 3.
// Which will give us back an ascending array
