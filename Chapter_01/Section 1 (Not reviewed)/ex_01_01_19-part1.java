// The largest number of N (compute under 1 hour) is: 56
import java.util.*;

public class Fibonacci {
    // This is using the backward approach (the function ends at the beginning of the Fibonacci - 0 and 1)
    // making it very  slow
    // Fibonacci: So sau = tong 2 so truoc
    public static long F(int N) {
        if (N == 0) return 0;
        if (N == 1) return 1;
        return F(N-1) + F(N-2);
    }
    // And the program start from 
    public static void main(String[] args) {
        for (int N = 0; N < 100; N++)
            System.out.println(N + " " + F(N));
    }
}
