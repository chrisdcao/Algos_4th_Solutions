#include <iostream>
#include <vector>
#include <iterator>
using namespace std;

int main() {
    vector<char> s;
    int N = 0;
    cout << "Xin hay nhap 1 so nguyen N: ";
    cin >> N;
    for (int n = N; n > 0; n/=2)  {
        if (n%2 == 0)
            s.push_back('0');
        else
            s.push_back('1');
    }
    for (auto m:s)
        cout << m;
    cout << endl;
}
