#include <iostream>
#include <iterator>
using namespace std;

int main() {
    int m,n;
    int k;
    cout << "Please input number of rows: ";
    cin >> m;
    cout << "Please input number of columnns: ";
    cin >> n;
    int array[m][n];
    cout << "Please assign the values into the array below: " << endl;
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            cin >> k;
            array[i][j]=k;
        }
        cout << endl;
    }
    cout << "transpose of the array is: " << endl;
    for (int column = 0; column < n; ++column) {
        for (int row = 0; row < m; ++row) {
            cout << array[row][column] << " ";
        }
        cout << endl;
    }
    return 0;
}
