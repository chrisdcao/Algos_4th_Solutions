// Recursive has exponential complexity + always have to create new memory stack each time a recursion is called (retrieving data from RAM is much slower than computing on the CPU)
// So to optimize a Recursive function, we de-recursive it, rewrite it in a non-recursive form

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int main() {
    int n = 0;
    int first = 1, second = 1;
    int sum = first + second;
    int current = 0;
    cout << "Please input the length of Fibonacci array you want: ";
    cin >> n;
    if (n == 1) {
        cout << "The Fibonacci array is: " << first;
        sum = 1;
    }
    else if (n == 2) {
        cout << "The Fibonacci array is: " << first << " " << second;
        sum = 2;
    } else {
        cout << "The Fibonacci array is: " << first << " " << second << " ";
        for (int i = 1; i <= n-2; i++)  {
            current = first + second;
            first = second;
            second = current;
            sum += current;
            cout << current << " ";
        }
    }
    cout << "\nThe sum of the array is: " << sum << endl;
    return 0;
}
