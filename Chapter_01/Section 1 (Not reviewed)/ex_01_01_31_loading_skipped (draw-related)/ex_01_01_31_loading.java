import java.util.*;
import java.lang.*;

public class ex_01_01_31_loading {
    public static void main(String[] args) {
        System.out.println("Please input the number of points of the Circle: ");
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        System.out.println("Please input the probability of an occurence of a line (<1 && >0): "); 
        Scanner input1 = new Scanner(System.in);
        double p = input1.nextDouble();
        double radians = Math.PI / N;
        double radius = Math.sqrt(0.00125 / (1 - Math.cos(radians)));
        // Tam cua duong tron nam tai diem O(r, r)
        double xOrigin = radius,
               yOrigin = radius; 
        // M is the tangent point of the Circle to y-axis
        double xM = 0.00,
               yM = radius;
        double[] xArray = new double[N];
        double[] yArray = new double[N];
        xArray[0] = xM;
        yArray[0] = yM;
        // Phep quay qua tam O(r, r)
        for (int i = 1; i <= N; i++) {
            xM = (xM - radius) * Math.cos(radians) - (yM - radius) * Math.sin(radians) + radius;
            yM = (xM - radius) * Math.sin(radians) + (yM - radius) * Math.cos(radians) + radius;
            xArray[i] = xM;
            yArray[i] = yM;
        }
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (j != i) {
                    if (p == Math.random()) {
                        StdDraw.line(xArray[i], yArray[i], xArray[j], yArray[j]);
                    }
                }
            }
        }
    }
}
