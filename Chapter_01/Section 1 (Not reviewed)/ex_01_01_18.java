package java.util.*;

public class ex1118 {
    public static int mystery(int a, int b) {
        if (b == 0) 
            return 0;
        if (b % 2 == 0)
            return mystery(a + a, b/2);
        else
            return mystery(a+a, b/2) + a;
    }
}

// Q1: a = 2, b = 25
// mystery(2+2, )
