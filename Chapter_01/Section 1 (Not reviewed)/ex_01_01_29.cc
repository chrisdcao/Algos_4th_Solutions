#include <iostream>
#include <iterator>
using namespace std;

static int array_size;
// Return the first index of the key, rather than the mid = key
int BinarySearchPlus(int key, int a[], int lo, int hi) {
    if (lo > hi) return -1;
    int mid = lo + (hi - lo)/2;
    if (key < a[mid]) 
        return BinarySearchPlus(key, a, lo, mid-1);
    else if (key > a[mid]) 
        return BinarySearchPlus(key, a, mid+1, hi);
    else {
        int p = mid;
        while (p > lo) {
            if (a[p] == key)
                p--;
        }
        return (p + 1);     // because the decrement will lead to the earliest position of key then minus one more (because the condition is still satisfied at this moment), so we have to plus one back in order to have the right index
    }
}

int ranking(int key, int a[]);
int count (int key, int a[]);

int main() {
    int a[] = { 1, 2, 3, 4, 5, 6, 6, 6, 7, 7, 7, 7, 9 };
    array_size = sizeof(a)/sizeof(a[0]) - 1;
    cout << BinarySearchPlus(7, a, 0, array_size) << endl;
    cout << ranking(7, a) << endl;
    cout << count(7, a) << endl;
    cout << count(9, a) << endl;
    return 0; 
}

int ranking(int key, int a[]) {
    return BinarySearchPlus(key, a, 0, array_size); // since this new Binary return the first index (from the left) of key, anything on the left side will be smaller than the key
}

int count(int key, int a[]) {
    int forward = 0, 
        backward = 0,
        count = 0;
    forward = backward = BinarySearchPlus(key, a, 0, array_size);
    while (forward < array_size && backward > 0) {
        ++forward; --backward;
        if (a[forward] == key) ++count;
        if (a[backward] == key) ++count;
    }
    if (count > 0)
        return count;
    else
        return -1;
}
