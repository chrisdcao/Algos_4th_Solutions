import java.util.*;
import java.lang.*;

public class ex_01_01_38 {
    public static int rank(int key, int[] a) {
        for (int i = 0; i < a.length; i++)
            if(a[i] == key) return i;
        return -1;
    }

    public static void main(String[] args) {
        int a[] = {1, 2, 3, 4, 5};
        cout << rank(2, a) << endl;
    }
}
