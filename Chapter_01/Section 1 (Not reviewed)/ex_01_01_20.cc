#include <iostream>
#include <cmath>
using namespace std;


int FAC(int n) {
    if (n == 1) return 1;
    else 
        return FAC(n-1)*n;
}

double LN(int n) {
    return log(FAC(n));
}

int main() {
    int n;
    cout << "Please input the desired factorial length: ";
    cin >> n;
    if (n <= 0) 
        cout << "Please input at leat one number!" << endl;
    else {
        cout << "The rounded result of ln(" << n << "!) is: " << LN(n) << endl;
    }
    return 0;
}
