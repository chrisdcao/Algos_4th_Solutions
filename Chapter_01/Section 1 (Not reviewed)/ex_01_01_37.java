import java.util.*;
import java.lang.*;

public class ex_01_01_37 {
    public class void shuffle(double[] a) {
        int N = a.length;
        for (int i = 0; i < N; i++) {
            int r = i + StdRandom.uniform(N - i);
            double temp = a[r];
            a[r] = a[i];
            a[i] = temp;
        }
    }
    
    public class void badShuffle(double[] a) {
        int N = a.length;
        for (int i = 0; i < N; i++) {
            int r = StdRandom.uniform(N);
            double temp = a[r];
            a[r] = a[i];
            a[i] = temp;
        }
    }

    public static void main(String[] args) {
       double[] a = {1, 2, 3, 4, 5, 6, 7};
       double[] a2 = {1, 2, 3, 4, 5, 6, 7};
       shuffle(a);
       badShuffle(a2);
       for (int i = 0; i < a.length; i++) {
           System.out.print(a[i] + " ");
       }
       for (int i = 0; i < a2.length; i++) {
           System.out.print(a2[i] + " ");
       }
       System.out.println();
    }
}

// EXPLAIN WHY BAD SHUFFLE IS NOT AS TRUE TO THE NATURE OF THE PROBABILITY AS THE SHUFFLE (MATHEMATICALLY):

// Re-arrange the current array is equivalent to keep choosing a random element from the current array and put it in a random index of another array (says a new array, for example) until all the elements are move (all are shuffled).

// On the first turn, we choose 1 out of N elements because none have been chosen and moved: 1CN. Then we move this element from the initial array into the empty array. Now we have N-1 element left.

// Second turn,  we choose 1 out of N-1 element (all we have left): 1C(N-1). Then we move this element to an index of the new array. Gradually, it keep decreasing and will be choosing from N - 2, N - 3, ... Till it becomes 1C1 because we only have one element left to choose from to move into the other array.

// So we have to reduce the range of StdRandom.uniform to present this. Choosing a random number on the first try will be StdRandom.uniform(N) ((O to N - 1) - or N elements), but then we only have N-1 elements left to re-arrange, and so we use a 'runnning' i variable to keep decreasing the value of N to fit the number of elements we have left.

// This loop depicts accurately the process from real life

// BAD SHUFFLING, on the other hand, imply that we keep having the same number of elements even after many choosing and moving turns, making it logically false, though the program might not produce any error technically.
