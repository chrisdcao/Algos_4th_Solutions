#include <iostream>
#include <iterator>
using std::cin;
using std::cout;
using std::endl;

int main() {
    // after-the-sort array
    int a[] = { 1, 1, 1, 1, 2, 2, 4, 4, 5, 5, 9, 9, 12, 12, 13, 14, 15, 15, 17 };
    int arraysize = sizeof(a) / sizeof(a[0]);
    int* begin = &a[0];
    int* end = &a[0] + arraysize;
    while (begin != end) {
        int* beginsearch = begin + 1;
        int* result = beginsearch;
        while (beginsearch != end) {
            if (!(*beginsearch == *begin)) {
                *result = std::move(*beginsearch);
                ++result;
            } else {
                --arraysize;
            }
            ++beginsearch;
            if (*result == *begin) {
                *result = std::move(*beginsearch);
            }
        }
        ++begin;
        end = &a[0] + arraysize;
    }
    for (int i = 0; i < arraysize; i++) {
        cout << a[i] << " ";
    }
}
