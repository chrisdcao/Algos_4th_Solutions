#include <iostream>
#include <iterator>
using namespace std;

static string indent = "";
static int depth = 0;

int ranking(int key, int a[], int lo, int hi) {
    if (hi < lo) {
        cout << indent << "None found. Return ";
        return -1;
    }
    int mid = lo + (hi - lo)/2;
    indent += "    ";
    depth += 1;
    if (key < a[mid]) {
        cout << indent << "The current (lo & hi) is: (" << lo << " & " << hi << ")" << endl;
        cout << indent << "Current depth: " << depth << endl;
        return ranking(key, a, lo, (mid - 1));
    } else if (key > a[mid]) {
        cout << indent << "The current (lo & hi) is: (" << lo << " & " << hi << ")" << endl;
        cout << indent << "Current depth: " << depth << endl;
        return ranking(key, a, (mid + 1), hi);
    } else {
        cout << indent << "The current (lo & hi) is: (" << lo << " & " << hi << ")" << endl;
        cout << indent << "Current depth: " << depth << endl;
        cout << indent << "Found at index: ";
        return mid;
    }
}
// NEED TO ADD SORT ALGO FOR THE SORTING AS WELL
int main() {
    int n, key;
    int a[] = {1, 2, 5, 12, 16, 18, 24, 33, 34, 35, 36, 44, 45, 56, 64, 78, 89, 97, 103};
    int end = sizeof(a)/sizeof(a[0]);
    cout << "The current array is: ";
    for (auto s:a) {
        cout << s << " ";
    }
    cout << "\nPlease input the 'integer' you want to look for: ";
    cin >> n;
    cout << ranking(n,a,0,end) << endl;
    return 0;
}
