import java.util.*;
import java.lang.*;

public class ex_01_01_39 {
    public static int BinarySearch(double key, double a[]) {
        int lo = 0;
        int hi = a.length - 1;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            if (key < a[mid]) hi = mid - 1;
            else if (key > a[mid]) lo = mid + 1;
            else return mid;
        }
        return -1;
    }

    static int[] avgArr;
    public static void BinSearchPlus(int T, int N) {
        avgArr = new int[T];
        for(int i = 0; i < T; i++) {
            int count = 0;
            double[] array1 = new double[N];
            double[] array2 = new double[N];
            for (int j = 0; j < N; j++) {
                array1[j] = 1000000 * Math.random();
                array2[j] = 1000000 * Math.random();
            }
            Arrays.sort(array1);
            Arrays.sort(array2);
            for (int k = 0; k < N; k++) {
                if (BinarySearch(array1[k], array2) != -1)
                    ++count;
            }
            avgArr[i] = count;
        }
    }

    public static void main(String[] args) {
        System.out.print("Please input number of trials: ");
        Scanner input = new Scanner(System.in);
        int T = input.nextInt();
        System.out.print("Please state the size(N) of the trial array: ");
        Scanner input2 = new Scanner(System.in);
        int N = input2.nextInt();
        System.out.println("");
        BinSearchPlus(T, N);
        for (int i = 0; i < T; i++)
            System.out.print(avgArr[i] + " ");
        System.out.println();
    }
}
