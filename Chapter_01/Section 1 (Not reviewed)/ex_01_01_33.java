import java.lang.Math;
import java.util.*;


public class ex_01_01_33 {
    public static class Matrix {
        static double dot(double[] x, double[] y) {;
            double sum = 0.00;
            if (x.length != y.length) 
                throw new ArithmeticException("Vector size inappropriate for mulitplying");
            else for (int i = 0; i < x.length; i++) 
                sum += x[i] * y[i];
            return sum;
        }

        static double[][] mult(double[][] a, double[][] b) {
            int rowA = a.length;
            int colA = a[0].length;
            int rowB = b.length;
            int colB = b[0].length;
            double[][] r = new double[rowA][colB];
            if (colA == rowB) {
                for (int i = 0; i < colB; i++) {
                    for (int j = 0; j < rowA; j++) {
                        double sum = 0.00;
                        for (int m = 0; m < rowB; m++) {
                            sum += a[j][m] * b[m][i];
                        }
                        r[j][i] = sum;
                    }
                }
            } else {
                throw new ArithmeticException("Matrix dimensions not multiply-able");
            }
            return r;
        }

        static double[][] transpose(double[][] a) {
            int rowA = a.length;
            int colA = a[0].length;
            double[][] r = new double[colA][rowA];
            for (int i = 0; i < colA; i++) {
                for (int j = 0; j < rowA; j++) {
                    r[i][j] = a[j][i];
                }
            }
            return r;
        }

        static double[] mult(double[][] a, double[] x) {
            int rowA = a.length;
            int colA = a[0].length;
            double[] r = new double[colA];
            if (x.length == rowA) {
                for (int i = 0; i < colA; i++) {
                    double sum = 0.00;
                    for (int j = 0; j < rowA; j++) {
                        sum += a[j][i] * x[j];
                    }
                    r[i] = sum;
                }
            } else {
                throw new ArithmeticException("Vector dimensions not multiply-able (must be equal to matrix's row)");
            }
            return r;
        }

        static double[] mult(double[] y, double[][] a) {
            return mult(a, y);
        }
    }

    public static void main(String[] args) {
       double[][] testMatrix1 = {{1, -1, 2}, 
                                 {0, -3, 1}};
       double[][] testMatrix2 = {{1, 2}, 
                                 {3, 4}, 
                                 {5, 6}};
       double[] testVector1 = {2, 1, 0};
       double[] testVector2 = {1, 2};
       double[][] r = Matrix.mult(testMatrix1, testMatrix2);
       for (int i = 0; i < r.length; i++) {
            for (int j = 0; j < r[0].length; j++) {
                System.out.print(r[i][j] + " ");
            }
            System.out.println();
       }
       double[] r2 = Matrix.mult(testMatrix1, testVector2);
       for (int i = 0; i < r2.length; i++) {
            System.out.print(r2[i] + " ");
       }
       Matrix.dot(testVector1, testVector2);
    }
}
