import java.util.*;

public class ex116 {
    public static void main(String[] args) {
        System.out.println(exR1(6));
    }
}

public static String exR1(int n) {
    if (n <= 0) return "";
    return exR1(n-3) + n + exR1(n-2) + n;
}

// EXPLANATION
// bottom to top result return
// 1st call : return exR1(3) + 6 + exR1(4) + 6;
// 2nd call: return (exR1(0) + 6 + exR1(1) + 6) + 6 + (exR1(1) + 6 + exR1(2) + 6) + 6
// (("")+6+(""+6+""+ 6)+6) + 6 + ((""+6+""+6)+6+(""+6+""+6)+6) + 6
// 666666666666
