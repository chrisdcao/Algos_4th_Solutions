#include <iostream>
#include <iterator>
using namespace std;

// NEU NGUOI DUNG INPUT THI CHUNG TA SE FILL TU TRAI QUA PHAI, TREN XUONG DUOI
// 0.TAO VONG LAP INPUT CHO TOI KHI HET
// 1.LAP TRINH KI TU INPUT
// 2. DE MOI INPUT VAO VI TRI HOP LI
// 3. FILL HET ARRAY
int main() {
    int row = 3, column = 4;
    char a = '*', b = ' ';
    bool array[row][column];
    bool k;
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < column; j++) {
            cin >> k;
            array[i][j] = k;
        }
        cout << endl;
    }
    for (int i = 0; i <= row; i++) {
        for (int j = 0; j <= column; j++) {
            if (array[i][j] == true)
                cout << a << " ";
            else
                cout << b << " ";
        }
        cout << endl;
    }
}

