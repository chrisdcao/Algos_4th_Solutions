#include <iostream>
#include <vector>
using namespace std;

class Matrix {
    public:
    double dot(vector<double>& x, vector<double>& y) {
        double sum = 0.00;
        if (x.size() != y.size()) {}
        else for (int i = 0; i < x.size(); i++) 
            sum += x[i] * y[i];
        return sum;
    }

    vector<vector<double>> mult(vector<vector<double>> a, vector<vector<double>> b) {
        int rowA = a.size();
        int colA = a[0].size();
        int rowB = b.size();
        int colB = b[0].size();
        vector<vector<double>> r(rowA);
        if (colA == rowB) {
            for (int i = 0; i < colB; i++) {
                for (int j = 0; j < rowA; j++) {
                    double sum = 0.00;
                    for (int m = 0; m < rowB; m++) {
                        sum += a[j][m] * b[m][i] * 1.00;
                    }
                    r[j].push_back(sum);
                }
            }
        }
        return r;
    }

    vector<vector<double>> transpose(vector<vector<double>> a) {
        int rowA = a.size();
        int colA = a[0].size();
        vector<vector<double>> r(colA);
        for (int i = 0; i < colA; i++) {
            for (int j = 0; j < rowA; j++) {
                r[i][j] = a[j][i];
            }
        }
        return r;
    }

    vector<double> mult(vector<vector<double>> a, vector<double> x) {
        int rowA = a.size();
        int colA = a[0].size();
        vector<double> r;
        if (x.size() == rowA) {
            for (int i = 0; i < colA; i++) {
                double sum = 0.00;
                for (int j = 0; j < rowA; j++) {
                    sum += a[j][i] * x[j] * 1.00;
                }
                r.push_back(sum);
            }
        }
        return r;
    }

    vector<double> mult(vector<double> y, vector<double> a) {
        return mult(a, y);
    }
};

int main() {
   vector<vector<double>> testMatrix1 = {{1, -1, 2}, 
                             {0, -3, 1}};
   vector<vector<double>> testMatrix2 = {{1, 2}, 
                             {3, 4}, 
                             {5, 6}};
   vector<double> testVector1 = {2, 1, 0};
   vector<double> testVector2 = {1, 2};
   Matrix matrix;
   vector<vector<double>> r = matrix.mult(testMatrix1, testMatrix2);
   for (int i = 0; i < r.size(); i++) {
        for (int j = 0; j < r[0].size(); j++) {
            cout << r[i][j] << " ";
        }
        cout << endl;
   }
   vector<double> r2 = matrix.mult(testMatrix1, testVector2);
   matrix.dot(testVector1, testVector2);
}
