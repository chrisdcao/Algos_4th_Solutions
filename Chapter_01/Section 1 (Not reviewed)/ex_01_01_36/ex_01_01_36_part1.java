import java.util.*;
import java.lang.*;

public class ex_01_01_36_part1 {
    public static void shuffle(double[] a) {
        int N = a.length;
        for(int i = 0; i < N; i++) {
            int r = i + StdRandom.uniform(N - i);
            double temp = a[i];
            a[i] = a[r];
            a[r] = temp;
        }
    }

    public static void main(String[] args) {
       double[] a = {1, 2, 3, 4, 5, 6, 7};
       shuffle(a);
       for (int i = 0; i < a.length; i++) {
           System.out.print(a[i] + " ");
       }
       System.out.println();
    }
}
