import java.util.*;
import java.lang.*;

public class ex_01_01_36_part2 {
    static double[][] count;
    // MATHEMATICALLY-CORRECT SHUFFLE
    public static void shuffle(double[] a, int N) {
        count = new double[a.length][a.length];
        if (N <= a.length)  {
            for (int i = 0; i < N; i++) {
                int r = i + StdRandom.uniform(N - i);
                double temp = a[i];
                a[i] = a[r];
                a[r] = temp;
                if (i != r) {
                    ++count[i][r];
                    ++count[r][i];
                } else 
                    ++count[i][r];
            }
        } else {
            int mult = N / a.length;
            int remainder = N - mult * a.length;
            while (mult > 0) {
                for (int i = 0; i < a.length; i++) {
                    int r = i + StdRandom.uniform(a.length - i);
                    double temp = a[i];
                    a[i] = a[r];
                    a[r] = temp;
                    if (i != r) {
                        ++count[i][r];
                        ++count[r][i];
                    } else 
                        ++count[i][r];
                }
                mult--;
            }
            for (int i = 0; i < remainder; i++) {
                int r2 = i + StdRandom.uniform(remainder - i);
                double temp = a[i];
                a[i] = a[r2];
                a[r2] = temp;
                if (i != r2) {
                    ++count[i][r2];
                    ++count[r2][i];
                } else 
                    ++count[i][r2];
            }
        }
    }

    public static void main(String[] args) {
        System.out.print("Please input the number of elements in the array: ");
        Scanner input = new Scanner(System.in);
        int M = input.nextInt();
        double[] a = new double[M];
        for (int i = 0; i < M; i++) 
            a[i] = i;
        System.out.print("Please input the amount of time you want to shuffle: ");
        Scanner input2 = new Scanner(System.in);
        int N = input2.nextInt();
        shuffle(a, N);
        System.out.println("The shuffled array is:");
        for (int i = 0; i < a.length; i++)
            System.out.print(a[i] + " ");
        System.out.println("\n\nHow many time value at row-i has been at index-j (column j) of the array during the shuffle:");
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++)
                System.out.print(count[i][j] + " ");
            System.out.println();
        }
        System.out.println();
    }
}
