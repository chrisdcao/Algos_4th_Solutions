#include <iostream>
using namespace std;

// We have a piece of code
double binomial(int N, int k, double p) {
    if ((N == 0) || (k < 0)) return 1.0;
    return (1.0 -p)*binomial(N-1, k) + p*binomial(N-1, k-1);
}
// the code stops either when the last element executes its last recursion call 
// the code that done executing first will keep on holding the value it created
// so the recursion will cost MAX(N,k) steps
