#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;

int main() {
    ifstream inputfile("ex_1_1_21_input.txt");
    float avg = 0.000,
          number1 = 0.000,
          number2 = 0.000;
    string name = "",
           line;
    cout << "NAME" << "\t" << "1ST" << "\t" << "2ND" << "\t" << "DIV" << endl;
    while(getline(inputfile,line)) {
        istringstream record(line);
        record >> name;
        record >> number1;
        record >> number2;
        if (number2 != 0) {
            avg = number1/number2;
            cout << name << "\t" << number1 << "\t" << number2 << "\t" << avg << endl;
        }
        else 
            cout << name << "\t" << number1 << "\t" << number2 << "\t" << "NOT DEFINED" << endl;
    }
    return 0;
}
