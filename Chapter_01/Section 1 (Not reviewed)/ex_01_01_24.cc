#include <iostream>
using namespace std;

static string indent = "  ";
static int depth = 0;

int gcd(int a, int b) {
    if (b == 0) {
        cout << indent << "Greatest common divisor is: ";
        return a;
    }
    int r = a % b;
    depth += 1;
    indent += "  ";
    cout << indent << "Current recursion depth: " << depth
                   << ". Current (b & r): " << b << " & " << r << endl;
    return gcd(b,r);
}

int main() {
    int a, b;
    cout << "Please input 2 numbers you want to find gdc: ";
    cin >> a >> b;
    cout << gcd(a,b) << endl;
    return 0;
}
