Both must have return in order for the recursive to be able to end. 

If only (n <= 0) then return; the String s will keep invoking the function itself even at n <= 0 and won't be able to end
