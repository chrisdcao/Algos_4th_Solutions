import java.util.*;
import java.lang.*;

public class ex_01_01_32_loading {
    public static void main(String[] args) {
        System.out.println("Please input the size of random-generated array: ");
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        double[] array = new double[N];
        for (int i = 0; i < N; i++) 
            array[i] = Math.random();
        System.out.println("Please input the desire range (left and right): ");
        Scanner inputl = new Scanner(System.in);
        double l = inputl.nextDouble();
        Scanner inputr = new Scanner(System.in);
        double r = inputr.nextDouble();
        for (int i = 0; i < N; i++) {
            if (array[i] >= l && array[i] <= r) {
                double x = 1.0 * i / N;
                double y = 0.5 * array[i];
                double rw = 0.5 * r / N;        // half the width
                double rh = 0.5 * array[i];     // half the height
                StdDraw.filledRectangle(x, y, rw, rh);
            }
        }
    }
}
