#include <iostream>
#include <string>
#include <memory>
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <vector>

using namespace std;

// implementation of Weighted Quick-Union
class UF {
private:
    vector<int> id;
    vector<int> sz;
    int cnt = 0;
    int value = 0;
    int newConnection = 0;

public:
    friend ostream& operator << (ostream& os, UF& uf) {
        for (auto s : uf.id)
            os << s << " ";
        return os;
    }

    UF(): cnt(0), value(0), newConnection(0) {}

    int count() { return cnt; }

    int connectionGenerated() { return newConnection; }

    bool connected(int p, int q) { return find(p) == find(q); }

    int find(int p) {
        while (p != id[p]) p = id[p];
        return p;
    }

    int newSite() { 
        id.push_back(value);
        sz.push_back(1);
        value++; cnt++;

        return value;
    }

    void unionn(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(q);

        if (pRoot == qRoot) return;

        if (sz[pRoot] >= sz[qRoot]) {
            id[qRoot] = pRoot;
            sz[pRoot] += sz[qRoot];
        } else {
            id[pRoot] = qRoot;
            sz[qRoot] += sz[pRoot];
        }

        for (auto s : id)
            printf("%2d ", s);
        cout << endl;

        for (auto s : sz)
            printf("%2d ", s);
        cout << endl << endl;

        newConnection++;
        cnt--;
    }
};

int main(int argc, char** argv) {
    int N;
    cin >> N;
    UF uf;

    for (int i = 0; i < N; i++)
        uf.newSite();

    cout << uf << endl;

    int p, q;
    while (cin >> p >> q) {
        if (uf.connected(p, q)) continue;
        else uf.unionn(p, q);
    }

    cout << uf.count() << " components" << endl;
    cout << uf.connectionGenerated() << " connections generated!" << endl;

    return 0;
}
