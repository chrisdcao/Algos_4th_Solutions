#include <iostream>
#include <vector>

using namespace std;

class QuickUnionUF {
private:
    vector<int> id;
    int cnt;
public:
    QuickUnionUF(int N): cnt(N) {
        for (int i = 0; i < N; i++) id.push_back(i);
    }

    int count() { return cnt; }

    bool connected(int p, int q) { return find(p) == find(q); }

    int find(int p) {
        while (p != id[p]) p = id[p];
        return p;
    }

    void unionn(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(q);

        if (pRoot == qRoot) return;
        id[pRoot] = qRoot;

        cnt--;
    }
};

class QuickFindUF {
private:
    vector<int> id;
    int cnt;
public:
    QuickFindUF(int N): cnt(N) {
        for (int i = 0; i < N; i++) id.push_back(i);
    }

    int count() { return cnt; }

    bool connected(int p, int q) { return find(p) == find(q); }

    int find(int p) { return id[p]; }

    void unionn(int p, int q) {
        int pID = find(p);
        int qID = find(q);

        if (pID == qID) return;

        for (int i = 0; i < id.size(); i++) {
            if (id[i] == pID) id[i] = qID;
        }
        cnt--;
    }
};

int main() {
    int N;

    cin >> N;
    QuickFindUF qfuf(N);
    QuickUnionUF quuf(N);

    int p, q;
    while (cin >> p >> q) {
        if (qfuf.connected(p,q)) continue;
        qfuf.unionn(p,q);
    }

    cin.clear();
    string str;
    getline(cin, str);

    cout << qfuf.count() << " components" << endl;
    
    while (cin >> p >> q) {
        if (quuf.connected(p,q)) continue;
        quuf.unionn(p,q);
    }

    cout << qfuf.count() << " components" << endl;

    return 0;
}

