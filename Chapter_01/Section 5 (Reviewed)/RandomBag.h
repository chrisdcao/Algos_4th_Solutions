#ifndef RANDOMBAG_H
#define RANDOMBAG_H

#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include "Random.h"

using namespace std;

template <typename T>
class RandomBag {
private:
    T* a;
    int capacity;
    int N;
    int i;

    void resize(int max) {
        T* temp = new T[max];
        for (int i = 0; i < N; i++) 
            temp[i] = a[i];
        delete a;
        a = temp;
    }

    class RandomIterator {
    private:
        T* current;
    public:
        RandomIterator(T* initLoc) {
            current = initLoc;
        }

        RandomIterator& operator++() {
            ++current;
            return *this;
        }

        bool operator!=(RandomIterator& rhs) {
            return this->current != rhs.current;
        }

        T operator*() {
            return *this->current;
        }
    };

public:
    RandomBag(): capacity(1), N(0), i(0) { a = new T[1]; }

    friend ostream& operator << (ostream& os, RandomBag& randomBag) {
        for (auto s : randomBag)
            os << s << " ";
        return os;
    }

    bool isEmpty() { return N == 0; }

    int size() { return N; }

    void add(T item) {
        if (N == capacity) {
            capacity *= 2;
            resize(capacity);
        }
        if (N >= 1) {
            for (int j = 0; j < N; j++) {
                int shuffle1 = randomUniformDistribution(0, N-1);
                int shuffle2 = randomUniformDistribution(0, N-1);
                swap(a[shuffle1], a[shuffle2]);
            }
        }

        a[i] = item;
        i++; N++;
    }

    RandomIterator begin() { return RandomIterator(a); }
    RandomIterator end() { return RandomIterator(a+N); }
};

#endif
