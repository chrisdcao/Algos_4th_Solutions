#include <iostream>
#include <vector>

using namespace std;

// quick-find
class UF {
private:
    vector<int> id;
    int cnt;
    int arrayAccessedCnt;
public:
    UF(int N): cnt(N), arrayAccessedCnt(0) {
        for (int i = 0; i < N; i++) id.push_back(i);
    }

    friend ostream& operator << (ostream& os, UF& uf) {
        for (auto s : uf.id)
            os << s << " ";
        os << endl;
        return os;
    }

    bool connected(int p, int q) { 
        ++arrayAccessedCnt; 
        return find(p) == find(q); 
    }

    int count() { return cnt; }

    int find(int p) { 
        ++arrayAccessedCnt; 
        return id[p]; 
    }

    void unionn(int p, int q) {
        int pID = find(p);
        int qID = find(q);

        // no array access
        if (pID == qID) return ;

        // counting here
        for (int i = 0; i < id.size(); i++) {
            // find the position of <the value from the input> in the array
            // and make it = q (it could be vice versa, we just take one value and make it equal to the other one so that it's connected)
            if (id[i] == pID) id[i] = qID;
            ++arrayAccessedCnt;
        }
        cout << "Array access for (" << p << "-" << q << ") is: " << arrayAccessedCnt << " times" << endl;
        arrayAccessedCnt = 0;
        cnt--;
    }
};

int main() {
    int N;
    cin >> N;
    UF uf(N);
    // printing the id[] array
    cout << uf;

    int p, q;
    while (cin >> p >> q) {
        if (uf.connected(p, q)) continue;
        uf.unionn(p,q);
    }

    cout << uf.count() << " components" << endl;

    return 0;
} 
