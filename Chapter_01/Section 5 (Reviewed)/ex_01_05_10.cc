// ORIGINAL FRAGMENT OF CODE
// OF WEIGHTED QUICK-UNION
void unionn(int p, int q) {
    int qRoot = find(q);
    int pRoot = find(p);

    if (qRoot == pRoot) return;

    if (sz[qRoot] > sz[pRoot]) {
        id[pRoot] = qRoot;              // the exercise asks what happens if we set 'id[pRoot] = q' instead of 'qRoot'
                                        // By changing 'qRoot' to 'q' we are allowing Root-Member connection (connecting a Root-Node to a Member-Node)
                                        // meaning that the root of one group will start from the level of the Member-node it attaches to
                                        // and that will make the tree taller (since Member-Node level always >= Root Level(0))
                                        // This means the time-cost will be higher in the worst case
                                        // because to find a Node within that Member-Root tree we might have to travel through deeper branch (since the tree is taller)
        sz[qRoot] += sz[pRoot];
    } else {
        id[qRoot] = pRoot;
        sz[pRoot] += sz[qRoot];
    }

    cnt--;
}
