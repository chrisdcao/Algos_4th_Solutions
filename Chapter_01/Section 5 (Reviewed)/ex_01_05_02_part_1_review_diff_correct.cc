#include <iostream>
#include <vector>

using namespace std;

class UF {
private:
    vector<int> id;
    int cnt;
    int accessCnt;
public:
    UF(int N): cnt(N), accessCnt(0) {
        for (int i = 0; i < N; i++)
            id.push_back(i);
    }

    friend ostream& operator << (ostream& os, UF& uf) {
        for (auto s : uf.id)
            os << s << " ";
        os << endl;
        return os;
    }

    int count() { return cnt; }

    // when checking connection this is the first round of search
    bool connected(int p, int q) { return find(p) == find(q); }

    int find(int p) {
        while (p != id[p]) {
            ++accessCnt;
            p = id[p];
        }
        return p;
    }

    void unionn(int p, int q) {
        // when union-ing, this is the second round of search
        int pRoot = find(p);
        int qRoot = find(q);

        if (pRoot == qRoot) return;
        // if they are not in the same root (2 roots are different)
        // merge the 2 roots to another root (here we pick q)
        id[pRoot] = qRoot;
        ++accessCnt;
        cout << "Array access for (" << p << "-" << q << ") is: " << accessCnt << " times" << endl;
        accessCnt = 0;
        cnt--;
    }
};

int main() {
    int N;
    cin >> N;
    UF uf(N);
    // printing the id[] array
    cout << uf;

    int p, q;
    while (cin >> p >> q) {
        if (uf.connected(p, q)) continue;
        uf.unionn(p,q);
    }

    cout << uf.count() << " components" << endl;

    return 0;
} 
