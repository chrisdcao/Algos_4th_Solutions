#include <iostream>
#include <vector>
using namespace std;

class UF_wrong {
private:   
    vector<int> id;
    int cnt;

public:
    UF_wrong(int N): cnt(N) {
        for (int i = 0; i < N; i++) id.push_back(i);
    }

    int count() { return cnt; }

    int find(int p) { return id[p]; }

    bool connected(int p, int q) { return find(p) == find(q); }

    // this union will be wrong right after the assignment of id[i] == id[q]
    // if id[i] == id[p] at that time and i < p
    // that will make in the next loop, the condition checking will be wrong
    // because id[p] (value at position 'p') is already changed in previous loops where i == p
    void unionn(int p, int q) {
        if (connected(p,q)) return;

        for (int i = 0; i < id.size(); i++) {
            if (id[i] == id[p]) id[i] = id[q];
        }


        for (auto s : id)
            cout << s << " ";
        cout << endl << endl;

        cnt--;
    }
};

int main() {
    int N;
    cin >> N;
    UF_wrong uf(N);

    int p, q;
    while (cin >> p >> q) {
        if (uf.connected(p,q)) continue;
        else {
            cout << "Connecting (" << p << " - " << q << ")"<< endl;
            uf.unionn(p,q);
        }
    }

    cout << uf.count() << " components" << endl;
}

// we can see that from connecting (4 - 3) onwards
// the connection starts gettings wrong
// because condition check starts getting wrong when coming back 
// to check id[p] when i has gone through the value of p before
// leaving the ending result to have 4 components (5 - 1 - 3 - 4)
// unmatch with the counting system (always right) that we setup: 2 components
