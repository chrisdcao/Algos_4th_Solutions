#include "RandomBag.h"

using namespace std;

class Connection {
private:
    int p;
    int q;

public:
    friend ostream& operator << (ostream& os, Connection& connectionPair) {
        os << connectionPair.p << "-" << connectionPair.q; 
        return os;
    }

    Connection() = default;
    Connection(int m, int n): p(m), q(n) {}
};

RandomBag<Connection> generateWRandomBag(int N) {
    int NxN = pow(N, 2);
    RandomBag<Connection> randomBag;

    int index = 0;

    for (int i = 0; i < N; i++) {
        for (int j = 0 ; j < N; j++) {
            Connection connectionPair(i,j);
            randomBag.add(connectionPair);
        }
    }

    return randomBag;
}

int main(int argc, char** argv) {
    int N = stoi(argv[1]);

    RandomBag<Connection> randomConnectionGrid = generateWRandomBag(N);
    int a = 1;

    for (auto s : randomConnectionGrid) {
        if (a % N == 0)
            cout << s << endl;
        else
            cout << s << " ";
        a++;
    }
    
    return 0;
}
