#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

class UF {
private:
    vector<int> id;
    vector<int> sz;
    int cnt;
    vector<int> height;

public:
    UF(int N): cnt(N) {
        for (int i = 0; i < N; i++) {
            id.push_back(i);
            sz.push_back(1);
            height.push_back(0);
        }
    }

    int count() { return cnt; }

    int find(int p) {
        int root = p;           // at first root is not root yet
        // we make it root by the same operation we do on
        // normal union-find
        // keep the node going until it's equal to root
        while (root != id[root])
            root = id[root];

        while (p != root) {
            int next = id[p];
            id[p] = root;
            p = next;
        }
        return root;
    }

    bool connected(int p, int q) { return find(p) == find(q); }

    void unionn(int p, int q) {
        int pRoot = find(p) ;
        int qRoot = find(q);

        if (pRoot == qRoot) return;

        if (sz[pRoot] > sz[qRoot]) {
            id[qRoot] = pRoot;
            sz[pRoot] += sz[qRoot];
        } else if (sz[pRoot] < sz[qRoot]) {
            id[pRoot] = qRoot;
            sz[qRoot] += sz[pRoot];
        } else {
            id[qRoot] = pRoot;
            sz[pRoot] +=  sz[qRoot];
            height[pRoot]++;
        }

        cout << "Member:";
        for (auto s : id) {
            printf("%2d ", s);
        }
        cout << endl;

        cout << "Height:";
        for (auto s : height) 
            printf("%2d ", s);
        cout << endl;

        cnt--;
    }
};

int main() {
    int N;
    cin >> N;
    UF uf(N);

    // Root '0' will achieve the height of '4'
    int p, q;
    while (cin >> p >> q) {
        if (uf.connected(p, q)) continue;
        else uf.unionn(p, q);
    }

    cout << uf.count() << " components" << endl;
}
