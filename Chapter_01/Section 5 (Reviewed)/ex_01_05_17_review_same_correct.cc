#include <iostream>
#include <cstdio>
#include <string>
#include <array>
#include <vector>
#include "Random.h"

using namespace std;

class UF {
private:
    vector<int> id;
    vector<int> sz;
    int cnt;

public:
    UF(int N): cnt(N) {
        for (int i = 0; i < N; i++) {
            id.push_back(i);
            sz.push_back(1);
        }
    }

    int count() { return cnt; }

    int find(int p) {
        while (p != id[p]) p = id[p];
        return p;
    }

    bool connected(int p, int q) { return find(p) == find(q); }

    void unionn(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(q);

        if (pRoot == qRoot) return;

        if (sz[pRoot] >= sz[qRoot]) {
            id[qRoot] = pRoot;
            sz[pRoot] += sz[qRoot];
        } else {
            id[pRoot] = qRoot;
            sz[qRoot] += sz[pRoot];
        }
        
        for (auto s : id)
            printf("%4d", s);
        cout << endl;

        for (auto s : sz)
            printf("%4d", s);
        cout << endl << endl;

        cnt--;
    }
};

int count(int N) {
    UF uf(N);
    int connectionGenerated = 0;

    while (uf.count() != 1) {
        int p = randomUniformDistribution(0, N-1);
        int q = randomUniformDistribution(0, N-1);

        if (!uf.connected(p, q)) {
            uf.unionn(p, q);
            connectionGenerated++;
        }
    }
    return connectionGenerated;
}

int main(int argc, char** argv) {
    int N = stoi(argv[1]);

    cout << count(N) << " connections generated!"<< endl;
}

