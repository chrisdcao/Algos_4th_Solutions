#include <iostream>
#include <vector>
#include "QuickFind.h"

using namespace std;

class WUF {
private:
    vector<int> id;
    int cnt;
    vector<int> sz;
    int accessCnt;
    
public:
    WUF(int N): cnt(N), accessCnt(0) {
        for (int i = 0; i < N; i++) {
            id.push_back(i);
            sz.push_back(1);
        }
    }

    int count() { return cnt; }

    int find(int p) { 
        return id[p]; 
    }

    bool connected(int p, int q) { return find(p) == find(q); }

    void unionn(int p, int q) {
        int pID = find(p);
        int qID = find(q);

        if (pID == qID) return;

        for (int i = 0; i < id.size(); i++) {
            if (sz[pID] < sz[qID]) {
                if (id[i] == pID) {
                    id[i] = qID;
                    sz[qID]++;
                    accessCnt++;
                }
            } else {
                if (id[i] == qID) {
                    id[i] = pID;
                    sz[pID]++;
                    accessCnt++;
                }
            }
        }

        //for (auto s : id)
            //cout << s << " ";
        //cout << endl;

        //for (auto s : sz)
            //cout << s << " ";
        //cout << endl;

        cout << "WUF value assigment count: " << accessCnt << " times" << endl;
        accessCnt = 0;

        cnt--;
    }
};

// I.  Does not change: 1, Cost of find() (which is O(1) in the original) and 2, Cost of loop traversal unionn() (the for() loop)
// II. Change: 1, The cost of assignment operation within the for() loop will be less (worst case n/2, when size at 'p' == size at 'q', if > n/2 then will perform the operation on the lesser one and will cost less than n/2)

// test client
// using ex_01_05_11_in.txt as input
int main() {
    int N;
    cin >> N;
    WUF wuf(N);
    UF uf(N);

    int p,q;
    while (cin >> p >> q) {
        cout << "(" << p <<  " - " << q << ")" << endl;

        if (uf.connected(p, q)) continue;
        else uf.unionn(p, q);

        if (wuf.connected(p, q)) continue;
        else wuf.unionn(p,q);
    }
}
