#include <iostream>
#include "RandomBag.h"

using namespace std;

int main() {
    RandomBag<int> randIntBag;

    randIntBag.add(0);
    randIntBag.add(1);
    randIntBag.add(2);
    randIntBag.add(3);

    for (auto s : randIntBag)
        cout << s << " ";
    cout << endl;

    return 0;
}
