#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>

using namespace std;

class UFByHeight {
private:
    vector<int> id;
    int cnt;
    vector<int> height;
    vector<int> sz;
    int step;

public:
    UFByHeight(int N): cnt(N), step(1) {
        for (int i = 0; i < N; i++) {
            id.push_back(i);
            // assume that a Tree having only a single Node (only Root) has Height 0
            height.push_back(0);
            sz.push_back(1);
        }
    }

    int count() { return cnt; }

    int maxHeight() {
        int maxHeight = 0;
        for (int i = 0; i < height.size(); i++) 
            maxHeight = max(maxHeight, (int) height[i]);
        return maxHeight;
    }

    int find(int p) {
        while (p != id[p]) p = id[p];
        return p;
    }

    bool connected(int p, int q) { return find(p) == find(q); }

    void unionn(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(q);

        if (qRoot == pRoot) return;

        if (height[pRoot] > height[qRoot]) {
            id[qRoot] = pRoot;
            sz[pRoot] += sz[qRoot];
        } else if (height[pRoot] < height[qRoot]) {
            id[pRoot] = qRoot;
            sz[qRoot] += sz[pRoot];
        } else {
            if (sz[pRoot] >= sz[qRoot]) {
                // Rebasing the Root of qTree (attach qRoot to pRoot) 
                // make the original Root Node of qTree goes down 1 level
                // and thus assign this new value to pTree
                height[pRoot] = height[qRoot] + 1;
                // all qRoot members are rebased to new branch
                // so qRoot is -1 in height
                id[qRoot] = pRoot;
                sz[pRoot] += sz[qRoot];
            } else {
                height[qRoot] = height[pRoot] + 1;
                id[pRoot] = qRoot;
                sz[qRoot] += sz[pRoot];
            }
        }

        cout << "Step " << step << ":\n";
        step++;

        for (auto s : id)
            printf("%2d ", s);
        cout << endl;

        for (auto s : height)
            printf("%2d ", s);
        cout << endl << endl;

        cnt--;
    }
};

int main() {
    int N;
    cin >> N;
    UFByHeight uf(N);

    int p, q;
    while (cin >> p >> q) {
        if (uf.connected(p, q)) continue;
        else uf.unionn(p, q);
    }

    cout << "maxHeight: " << uf.maxHeight() << endl;
    cout << "Components: " << uf.count() << endl;
}
































