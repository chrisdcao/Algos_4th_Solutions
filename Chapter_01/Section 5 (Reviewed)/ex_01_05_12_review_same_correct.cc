#include <iostream>
#include <vector>

using namespace std;

class UF {
private:
    vector<int> id;
    int cnt;

public:
    UF(int N): cnt(N) {
        for (int i = 0; i < N; i++) id.push_back(i); 
    }

    int count() { return cnt; }

    int find(int p) {
        int root = p;           // at first root is not root yet
        // we make it root by the same operation we do on
        // normal union-find
        // keep the node going until it's equal to root
        while (root != id[root])
            root = id[root];

        // keep traversing through all the p and attach them directly to their root
        while (p != root) {
            int next = id[p];
            id[p] = root;
            p = next;
        }
        return root;
    }

    bool connected(int p, int q) { return find(p) == find(q); }

    void unionn(int p, int q) {
        int pRoot = find(p) ;
        int qRoot = find(q);

        if (pRoot == qRoot) return;
        id[qRoot] = pRoot;

        for (auto s : id)
            cout << s << " ";
        cout << endl;

        cnt--;
    }
};

int main() {
    int N;
    cin >> N;
    UF uf(N);

    // path of length 4: 4 - 2 - 1 - 0 - 8
    int p, q;
    while (cin >> p >> q) {
        if (uf.connected(p, q)) continue;
        else uf.unionn(p, q);
    }

    cout << uf.count() << " components" << endl;
}
