#include <iostream>
#include <vector>

using namespace std;

class UF {
private:
    vector<int> id;
    int cnt;
    vector<int> sz;
    int accessCnt;

public:
    UF(int N): cnt(N), accessCnt(0) {
        for (int i = 0; i < N; i++) {
            id.push_back(i);
            sz.push_back(1);      // initCnt of all roots are 1, so that later when we adding the size of Roots it does not stuck at '0' forever
        }
    }

    int count() { return cnt; }

    int find(int p) {
        while (p != id[p]) {
            accessCnt++;
            p = id[p];
        }
        return p;
    }

    bool connected(int p, int q) { return find(p) == find(q); }

    void unionn(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(q);

        if (pRoot == qRoot) return;

        if (sz[pRoot] < sz[qRoot]) { 
            id[pRoot] = qRoot; 
            sz[qRoot] += sz[pRoot];
        } else { 
            id[qRoot] = pRoot; 
            sz[pRoot] += sz[qRoot];
        }
        accessCnt++;
        cout << "Array access for (" << p << "-" << q << ") is: " << accessCnt << " times" << endl;
        // printing vector id on top of vector sz
        // for better representation
        for (auto s : id) 
            cout << s << " ";
        cout << endl;
        // printing vector sz
        for (auto s: sz) 
            cout << s << " ";
        cout << endl;

        accessCnt = 0;

        cnt--;
    }
};

int main() {
    int N;
    cin >> N;
    UF uf(N);

    int p, q;
    while (cin >> p >> q) {
        if (uf.connected(p, q)) continue;
        uf.unionn(p,q);
    }

    cout << "\n" << uf.count() << " components" << endl;

    return 0;
} 
