#ifndef QUICKFIND_H
#define QUICKFIND_H

#include <iostream>
#include <vector>

using namespace std;

class UF {
private:
    vector<int> id;
    int cnt;
    int accessCnt;
    
public:
    UF(int N): cnt(N), accessCnt(0) {
        for (int i = 0; i < N; i++) id.push_back(i);
    }

    int count() { return cnt; }

    int find(int p) { return id[p]; }

    bool connected(int p, int q) { return find(p) == find(q); }

    void unionn(int p, int q) {
        int pID = find(p);
        int qID = find(q);

        if (pID == qID) return;

        for (int i = 0; i < id.size(); i++) {
            if (id[i] == pID) {
                id[i] = qID;
                accessCnt++;
            }
        }
        
        cout << "UF value assigment count:  " << accessCnt << " times" << endl;
        accessCnt = 0;

        cnt--;
    }
};

#endif
