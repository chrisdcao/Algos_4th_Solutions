#include <iostream>
#include <vector>
#include <memory>
#include <iterator>
using namespace std;

template <typename T>
class StackImpLinkedList {
private:
    int N;

    struct Node {
        T item;
        Node* next;
    };

    Node* head;
    Node* tail;

    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* initLoc) {
            current = initLoc;
        }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return this->current->item;
        }
    };

public:
    StackImpLinkedList() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void push(T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->next = head;
            head = temp;
        }
        N++;
    }

    T pop() {
        T item = head->item;
        if (N != 0) {
            Node* dp = head;
            head = head->next;
            delete dp;
            N--;
        } else 
            throw out_of_range("Underflow");
        return item;
    }

    T peek() {
        if (N != 0)
            return head->item;
        else
            throw out_of_range("Nothing left to peek!");
    }

    Iterator begin() { return Iterator(head); }
    Iterator end() { return Iterator(tail->next); }
};

int main() {
    StackImpLinkedList<string> string_stack;
    string_stack.push("thinh");
    string_stack.push("phong");
    string_stack.push("cuong");
    string_stack.push("khoa");
    cout << string_stack.peek() << endl;
    cout << string_stack.size() << endl;
    cout << endl;
    cout << string_stack.pop() << endl;
    cout << string_stack.size() << endl;
    cout << string_stack.peek() << endl;
    cout << endl;
    for (auto s : string_stack)
        cout << s << " ";
    cout << endl;
}
