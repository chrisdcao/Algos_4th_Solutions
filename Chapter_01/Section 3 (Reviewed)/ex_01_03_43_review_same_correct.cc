#include <iostream>
#include <string>
#include <cstring>
#include <dirent.h>
#include "Queue.h"
using namespace std;

string checkPathString(string strPath) {
    if (strPath[strPath.length() - 1] == '/') {
        strPath = strPath.substr(0, strPath.length() - 1);
    }
    return strPath;
}


Queue<string> listdir(string initStrPath, int depth) {
    static Queue<string> dirQueue;      // to save up everything from every recursion loop, the queue has to be static
    struct dirent* dir;
    DIR* dp = opendir(initStrPath.c_str());
    if (dp) {
        while ((dir = readdir(dp)) != NULL) {
            string strPath = "";            // default initialize the string path
            string space = "";
            if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0) continue;
            if (dir->d_type == DT_DIR) {
                initStrPath = checkPathString(initStrPath);
                string hold = dir->d_name;
                strPath = initStrPath + "/" + hold;
                for (int i = 0; i < depth; i++)
                    space += "   ";
                dirQueue.enqueue(space + hold);
                //cout << space << hold << endl;
                listdir(strPath, depth + 1);
            } else {
                initStrPath = checkPathString(initStrPath);
                for (int i = 0; i < depth; i++)
                    space += "   ";
                dirQueue.enqueue(space + dir->d_name);
                //cout << space << dir->d_name << endl;
            }
        }
        closedir(dp);
    }
    return dirQueue;
}

void printQueue(Queue<string> dirQueue) {
    while (!dirQueue.isEmpty())
        cout << dirQueue.dequeue() << endl;
}

int main() {
    printQueue(listdir("/home/khanhcao96", 0));
}
