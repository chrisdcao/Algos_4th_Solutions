We have a code fragment (in Java):

    Stack<Interger> stack = new Stack<Interger>();
    while (N > 0) {
        stack.push(N % 2);
        N = N / 2;
    }
    for (int d : stack) StdOut.print(d);
    StdOut.println();

The output of this will be:
1 1 0 0 1 0

Explanation:
push(50 % 2) = 0        stack: 0
N = 25
push(25 % 2) = 1        stack: 0 1
N = 12
push(12 % 2) = 0        stack: 0 1 0
N = 6
push(6 % 2) = 0         stack: 0 1 0 0
N = 3
push(3 % 2) = 1         stack: 0 1 0 0 1
N = 1
push(1 % 2) = 1         stack: 0 1 0 0 1 1
N = 0 -> end

Using Stack iterator to print is actually using a ReverseIterator (LIFO), and so the printing order will be reverse:

1 1 0 0 1 0

