#include <iostream>
#include <vector>
#include <memory>
#include <string>
using namespace std;

template <typename T>
class QueueImpLinkedList {
private:
    class Node {
    public:
        T item;
        Node* next;
        int index = -1;
    };
    vector<unique_ptr<Node>> allNodes;

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() -1;

        return allNodes.back().get();           // return the pointer that points to the obj
    } 

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {        // if the node we want to delete is not at the end of the vector
            allNodes[node->index].swap(allNodes.back());    // we swap it with the end of the vector (the last element)
            allNodes[node->index]->index = node->index;     // the index of the node we have just swap is now updated. The swap function will not swap the index as well, so we have to assign the index manually like this
        }
        allNodes.pop_back();        // delete the last element without returning any value in a vector
    }
    
    Node* head;
    Node* tail;
    int N;

    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* InitLoc) {
            current = InitLoc;
        }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return this->current->item;
        }
    };

public:
    QueueImpLinkedList() {
        N = 0;
        head = NULL;
        tail = NULL;
    }
    
    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void push(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }

    T pop () {
        T item = head->item;
        if (N != 0) {
            Node* dp = head;
            head = head->next;
            freeNode(dp);
            --N;
        } else
            throw out_of_range("Underflow");
        return item;
    }

    void deleteLast() {
        Node* oldTail = tail;
        Node* traverse = head;
        while (traverse->next->next != NULL) {
            traverse = traverse->next;
        }
        tail = traverse;
        traverse->next = NULL;
        freeNode(oldTail);
        --N;
    }

    Iterator begin() { return Iterator(head); }
    Iterator end() { return Iterator(tail->next); }

};

int main() {
    QueueImpLinkedList<string> stringList;
    stringList.push("phong");
    stringList.push("cuong");
    stringList.push("thinh");
    for (auto s : stringList)
        cout << s << " ";
    cout << endl;
    stringList.deleteLast();
    for (auto s : stringList)
        cout << s << " ";
    cout << endl;
}
