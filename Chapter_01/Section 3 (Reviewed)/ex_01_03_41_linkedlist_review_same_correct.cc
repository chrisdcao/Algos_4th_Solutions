#include <iostream>
using namespace std;

template <typename T>
class Queue {
public:
    struct Node {
        T item;
        Node* next;
    };

    Queue() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    Queue(Queue<T>& q) {
        // HAS TO REWORK THIS PART
        N = 0;
        const int threshold = q.size();
        for (int i = 0; i < threshold; i++) {
            T hold = q.dequeue();
            q.enqueue(hold);
            this->enqueue(hold);
        }
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void enqueue(T item) {
        Node* temp = new Node;
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }

    T dequeue() {
        if (head == NULL)
            throw out_of_range("No more item to dequeue!");
        T item = head->item;
        Node* dp = head;
        head = head->next;
        delete dp;
        --N;
        return item;
    }

    void printList() {
        Node* traverse = head;
        while (traverse != NULL) {
            cout << traverse->item << " ";
            traverse = traverse->next;
        }
    }

private:
    Node* head;
    Node* tail;
    int N;
};

int main() {
    Queue<string> stringQueue1;
    stringQueue1.enqueue("phong");
    stringQueue1.enqueue("thinh");
    stringQueue1.enqueue("cuong");
    stringQueue1.enqueue("khoa");
    cout << "Expected outcome: 4" << endl;
    cout << "Real outcome: ";
    cout << stringQueue1.size() << endl;
    cout << "Expected outcome: phong thinh cuong khoa" << endl;
    cout << "Real outcome: ";
    stringQueue1.printList();
    cout << endl;
    cout << endl;
    Queue<string> stringQueue2(stringQueue1);
    cout << "Expected outcome: phong thinh cuong khoa" << endl;
    cout << "Real outcome: ";
    stringQueue2.printList();
    cout << endl;
    cout << endl;
    cout << "Expected outcome: 4" << endl;
    cout << "Real outcome: ";
    cout << stringQueue2.size() << endl;
    cout << endl;
    cout << "Expected outcome: phong" << endl;
    cout << "Real outcome: ";
    cout << stringQueue2.dequeue() << endl;
    cout << "Expected outcome: 3" << endl;
    cout << "Real outcome: ";
    cout << stringQueue2.size() << endl;
    cout << endl;
    cout << "Expected outcome: phong thinh cuong khoa" << endl;
    cout << "Real outcome: ";
    stringQueue1.printList();
    cout << endl;
}
