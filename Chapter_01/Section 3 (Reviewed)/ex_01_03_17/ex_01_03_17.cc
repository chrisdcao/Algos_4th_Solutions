#include <iostream>
#include <string>
#include <sstream>

template <typename M, typename N, typename T>
class Array {
public:
    class Node {
    public:
        M item1;
        N item2;
        T item3;
        Node* next;
    };
    Array() {
        n = 0;
        Node* head = NULL;
        Node* tail = NULL;
    }
    int size() { return n; }
    bool isEmpty() { return head == NULL; }
    void push(M i1, N i2, T i3) {
        Node* temp = new Node;
        temp->item1 = i1;
        temp->item2 = i2;
        temp->item3 = i3;
        temp->next = NULL;
        if (n == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++n;
    }
    // still obey the FIFO, so don't have to create extra Queue class
    void printList() {
        Node* traverse = head;
        while (traverse != NULL) {
            cout << traverse->item1 << " ";
            for (int j = 0; j < 3; j++)
                cout << (traverse->item2)[j] << " ";
            cout << traverse->item3 << endl;
            traverse = traverse->next; 
        }
    }

private:
    int n;
    Node* head;
    Node* tail;
};

Array<string, int*, double> readDates(string& s) {
    Array<string, int*, double> transaction;
    for (auto& c : s) {
        if (c == '/')
            c = ' ';
        if (c == '-')
            c = ' ';
    }
    stringstream ss(s);
    string name;
    int* date = new int[3];
    int d;
    double money;
    ss >> name;
    for (int i = 0; i < 3; i++) {
        ss >> d;
        date[i] = d;
    }
    ss >> money;
    transaction.push(name, date, money);
    return transaction;
}

int main() {
    string s;
    getline(cin, s);
    Array<string, int*, double> result = readDates(s);
    result.printList();
}
