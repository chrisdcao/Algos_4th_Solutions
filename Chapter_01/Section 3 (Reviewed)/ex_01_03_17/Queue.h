#include <iostream>
#include <sstream>
#include <string>
using namespace std;

template<typename T>
class Queue {
public:
    Queue() {
        N = 0;
        i = 0;
        a = new T[1];
        queueSize = 1;
    }
    int size() { return N; }
    bool isEmpty() { return N == 0; }
    void enqueue(T item) {
        ++N;
        if (N == queueSize) {
            queueSize *= 2;
            resize(queueSize);
        }
        if (i > 0)
            --i;
        a[N-1] = item;
    }
    T dequeue() {
        T value = a[i];
        if (N > 0 && N == queueSize / 4) {
            queueSize /= 2;
            resize(queueSize);
        }
        a[i] = T();
        --N;
        ++i;
        return value;
    }
    void resize(int max) {
        T* temp = new T[max];
        for (int k = 0; k < N; k++)
            temp[k] = a[k];
        T* dp = a;
        a = temp;
        delete[] dp;
    }

private:
    T* a;
    int N;
    int i;              // act as running pointer
    int queueSize;
};
