import java.util.*;
import java.lang.*;

public class ex_01_03_35 {
    public static void main(String[] args) {
        RandomQueue<String> stringQueue = new RandomQueue<String>();
        stringQueue.enqueue("phong");
        stringQueue.enqueue("cuong");
        stringQueue.enqueue("thinh");
        stringQueue.enqueue("khanh");
        stringQueue.enqueue("hung");
        stringQueue.enqueue("long");
        stringQueue.enqueue("hang");
        System.out.println("The size before dequeue is: " + stringQueue.size());
        for (String s : stringQueue) 
            System.out.println(s);
        System.out.println();
        while (!stringQueue.isEmpty())
            System.out.println(stringQueue.dequeue());
        System.out.println("The size after dequeue is: " + stringQueue.size());
    }
}

public class RandomQueue<Item> implements Iterable<Item> {
    private Item[] a = (Item[]) new Object[1];
    private int N = 0;

    boolean isEmpty() { return N == 0; }
    int size() { return N; }
    void enqueue(Item item) {
        if (N == a.length)
            resize(a.length * 2);
        a[N++] = item;
    }
    // based on the exercise requirement, we remove the one in the last position, which is similar to that of a Stack rather than normal dequeue
    Item dequeue() {
        if (N > 0 && N == a.length / 4) 
            resize (a.length / 2);
        // randomize the things
        Random ran = new Random();
        int pos = ran.nextInt(N);
        Item temp = a[N-1];
        a[N-1] = a[pos];
        a[pos] = temp;
        Item item = a[N-1];
        a[N-1] = null;
        //decrement the count
        --N;
        return item;
    }

    Item sample() {
        Random ran = new Random();
        int pos = ran.nextInt(N);
        return a[pos];
    }

    void resize(int max)  {
        Item[] temp = (Item[]) new Object[max];
        for (int i = 0; i < N; i++)
            temp[i] = a[i];
        a = temp;
    }
    public Iterator<Item> iterator() { return new RandomIterator(); }

    private class RandomIterator implements Iterator<Item> {
        RandomIterator() {
            for (int j = 0; j < N; j++) {
                int r = j + StdRandom.uniform(N - j);
                Item temp = a[j];
                a[j] = a[r];
                a[r] = temp;
            }
        }
        private int i = N;
        public boolean hasNext() { return i > 0; }
        public Item next() { 
            return a[--i]; 
        }
        public void remove() {} 
    }
} 
