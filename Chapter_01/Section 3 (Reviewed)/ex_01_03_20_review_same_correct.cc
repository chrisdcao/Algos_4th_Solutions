#include <iostream>
#include <string>
using namespace std;

template <typename T>
class LinkedList {
public:
    struct Node {
        T item;
        Node* next;
    };
    LinkedList() {
        N = 0;
        head = NULL;
        tail = NULL;
    }
    int size() { return N; }
    bool isEmpty() { return N == 0; }
    void addNode(T item) {
        Node* temp = new Node;
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }
    void addNodeBeforeHead(T item) {
        Node* temp = new Node;
        temp->item = item;
        temp->next = head;
        head = temp;
    }
    void removeCurrentHead() {
        Node* oldhead = head;
        head = head->next;
        delete oldhead;
    }
    T addNodeAfterTail(T item) {
        Node* temp = new Node;
        temp->item = item;
        tail->next = temp;
        temp->next = NULL;
        tail = temp;
    }
    void printList() {
        Node* traverse = head;
        while (traverse != NULL) {
            cout << traverse->item << " ";
            traverse = traverse->next;
        }
        cout << endl;
    }
    int delete_kth(int k) {
        if (N == 0) {
            cout << "There is no element in the List!" << endl;
            return -1;
        }  else {
            if (k > N) {
                cout << "The item you looking for is out of bound!" << endl;
                return -1;
            } 
            else if (k == N && k != 1) {
                Node* traverse = head;
                for (int i = 1; i < k-1; i++) {     // loop for k times
                                                    // using human counting (starting from 1)
                    traverse = traverse->next;      
                }
                traverse->next = traverse->next->next;
                tail = traverse;                    // assign new tail
            }
            else if (k < N && k != 1) {
                Node* traverse = head;
                for (int i = 1; i < k-1; i++) { 
                    traverse = traverse->next; 
                }
                traverse->next = traverse->next->next;      // don't have to assign new tail 
            }
            else {
                Node* traverse = head;
                head = traverse->next;
                tail = head;
                traverse = NULL;
            }

        }
        --N;
        return 0;
    }

private:
    Node* head;
    Node* tail;
    int N;
};

int main() {
    LinkedList<string> list; 
    list.addNode("phong");
    list.addNode("thinh");
    list.addNode("cuong");
    list.addNode("khanh");
    list.printList();
    list.delete_kth(3);
    list.printList();
    cout << list.size() << endl;
    list.delete_kth(1);
    list.printList();
    cout << list.size() << endl;
    list.delete_kth(2);
    list.printList();
    cout << list.size() << endl;
}
