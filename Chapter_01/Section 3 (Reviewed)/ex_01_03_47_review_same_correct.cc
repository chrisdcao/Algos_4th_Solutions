#include <iostream>
#include <string>
#include "Queue.h"
#include "Stack.h"
#include "Steque.h"
#include "CircularLinkedList.h"
using namespace std;

template <typename T>
CircularLinkedList<T> CatenableQueues(Queue<T> queue1, Queue<T> queue2) {
    CircularLinkedList<T> queueList;

    while (!queue1.isEmpty()) {
       queueList.insert(queue1.dequeue());
    }
    while (!queue2.isEmpty()) {
       queueList.insert(queue2.dequeue());
    }

    return queueList;
}

template <typename T>
CircularLinkedList<T> CatenableStacks(Stack<T> stack1, Stack<T> stack2) {
    CircularLinkedList<T> stackList;

    while (!stack1.isEmpty()) {
       stackList.insert(stack1.pop());
    }
    while (!stack2.isEmpty()) {
       stackList.insert(stack2.pop());
    }

    return stackList;
}

template <typename T>
CircularLinkedList<T> CatenableSteques(Steque<T> steque1, Steque<T> steque2) {
    CircularLinkedList<T> stequeList;

    while (!steque1.isEmpty()) {
       stequeList.insert(steque1.pop());
    }
    while (!steque2.isEmpty()) {
       stequeList.insert(steque2.pop());
    }

    return stequeList;
}

int main() {
    CircularLinkedList<string> cList;
    Queue<string> queue1;
    Queue<string> queue2;
    queue1.enqueue("thinh");
    queue1.enqueue("thinh");
    queue1.enqueue("thinh");
    queue1.enqueue("thinh");
    queue2.enqueue("phong");
    queue2.enqueue("phong");
    queue2.enqueue("phong");
    queue2.enqueue("phong");
    cList = CatenableQueues(queue1, queue2);
    cList.printList();
}





