#include <iostream>
using namespace std;

template <typename T>
class LinkedList {
public:
    struct Node {
        T item;
        Node* next;
    };
    LinkedList() {
        N = 0;
        head = NULL;
        tail = NULL;
    }
    int size() { return N; }
    bool isEmpty() { return N == 0; }
    Node* getHead() { return head; }
    void addNode(T item) {
        Node* temp = new Node;
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }
    void printList() {
        Node* traverse = head;
        while(traverse != NULL) {
            cout << traverse->item << endl;
            traverse = traverse->item;
        }
        cout << endl;
    }

private:
    Node* head;
    Node* tail;
    int N;
};

using LinkedListNode = LinkedList<int>::Node;

// recursive gaming
int max(LinkedListNode* first) {
    if (first == NULL)
        return 0;
    else {
        if (first->item > max(first->next))
            return first->item;
        else
            return max(first->next);
    }
}

int main() {
    LinkedList<int> intList;
    intList.addNode(1);
    intList.addNode(27);
    intList.addNode(5);
    intList.addNode(2);
    intList.addNode(19);
    intList.addNode(8);
    LinkedListNode* first = intList.getHead();
    cout << max(first) << endl;
    intList.addNode(44);
    cout << max(first) << endl;
}
