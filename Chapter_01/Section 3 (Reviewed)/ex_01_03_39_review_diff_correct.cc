#include <iostream>
#include <string>
#include <memory>
#include <vector>
using namespace std;

template <typename T>
class RingBuffer {
private:
    class Node {
    public:
        T item;
        Node* next;
    };

    Node* head;
    Node* tail;
    int BUFFER_SIZE;
    int N = 0;          

public:
    RingBuffer() = default;
    RingBuffer(int max) {
        BUFFER_SIZE = max;
        head = NULL;              // because there are created based on unique_ptr, each of them will HAVE TO Point to different location. If we assign both of them to one value, then one will lose its value and one will gain the value
        tail = NULL;
        N = 0;
    }

    bool isFull() { return N == BUFFER_SIZE; }
    bool isEmpty() { return N == 0; }

    void write(T item) {
        if (isFull()) {
            cout << "Buffer is Full! New element cannot be inserted!" << endl;
            return;
        } else {
            Node* temp = new Node;
            temp->item = item;
            temp->next = NULL;
            if (N == 0) {
                head = temp;
                tail = temp;
            } else {
                if (N == BUFFER_SIZE - 1) {
                    temp->next = tail;
                } 
                head->next = temp;
                head = temp;
            }
        }
        N++;
    }

    T read() {
        if (isEmpty()) {
            cout << "No element to read!" << endl;
            throw out_of_range("Underflow!");
        } 
        T item = tail->item;
        tail = tail->next;
        --N;
        return item;
    }
    
    void printList() {
        Node* traverse = tail;
        while (traverse != head) {
            cout << traverse->item << endl;
            traverse = traverse->next;
        }
        cout << head->item << endl;
    }
};

int main() {
    RingBuffer<int> ringBuffer(6);
    ringBuffer.write(0);
    ringBuffer.write(1);
    ringBuffer.write(2);
    ringBuffer.write(3);
    ringBuffer.write(4);
    ringBuffer.write(5);
    ringBuffer.write(6);

    cout << "Current Status: " << ((ringBuffer.isFull()) ? "ringBuffer is Full!" : "ringBuffer is NOT full") << endl;
    ringBuffer.printList();
    cout << "Consumed: " << ringBuffer.read() << endl;
    ringBuffer.printList();
    cout << "Current Status: " << ((ringBuffer.isFull()) ? "ringBuffer is Full!" : "ringBuffer is NOT full") << endl;
    cout << "Consumed: " << ringBuffer.read() << endl;
    cout << "Consumed: " << ringBuffer.read() << endl;
    ringBuffer.printList();
}
