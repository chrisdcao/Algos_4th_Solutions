DoublingStackOfStrings means the stack will double whenever the number of items (strings) reach the capacity of the stack. The initial Stack starts at capacity (1)

We input the following lines:
it was - the best - of times - - - it was - the - -
with the '-' being the pop sign

Stack size will grow with this pattern: 
name        itemsNo         stacksize
it          1               2
was         2               4
-           1               4 -> 2 (when item = 1/4 stack we resize stack)
the         2               4
best        3               4
-           2               4
of          3               4
times       4               8
-           3               8
-           2               4
-           1               2
it          2               4
was         3               4
-           2               4
the         3               4
-           2               4
-           1               2


Size of array items: 1
Size of Stacksize: 2
what it prints: 'it' (the first one to be put in, the last out)
