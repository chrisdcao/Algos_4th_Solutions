#include <iostream>
#include <string>
#include <sstream>
using namespace std;

template <typename T>
class Stack {
private:
    struct Node {
        T item;
        Node* next;
    };

    Node * head;
    Node* tail;
    int N;
    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* InitLoc) {
            current = InitLoc;
        }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return this->current->item;
        }

    };

public:
    Stack() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void push(T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->next = head;
            head = temp;
        }
        N++;
    }
    
    T pop() {
        T item = head->item;
        if (N != 0) {
            Node* dp = head;
            head = head->next;
            delete dp;
            N--;
        } else 
            throw out_of_range("Underflow");
        return item;
    }

    Iterator begin() { return Iterator(head); }
    Iterator end() { return Iterator(tail->next); }
};

int main() {
    Stack<string> ops;
    Stack<int> vals;
    string s;
    while (cin >> s) {
        if (s == "+" || s == "-" || s == "*" || s == "/") {
            int val2 = vals.pop();
            int val1 = vals.pop();
            int val;
            if (s == "+") val = val1 + val2;
            if (s == "*") val = val1 * val2;
            if (s == "-") val = val1 - val2;
            if (s == "/") val = val1 / val2;
            vals.push(val);
        } else {
            int b = stoi(s, 0);
            vals.push(b);
        }
    }
    for (auto s : vals)
        cout << s;
    cout << endl;
}
