#include <iostream>
using namespace std;

template <typename T>
class QueueCircularLinkedList {
public:
    struct Node {
        T item;
        Node* next;
    };
    QueueCircularLinkedList() {
        N = 0;
        last = NULL;
    }
    int size() { return N; }
    bool isEmpty() { return N == 0; }
    // Since we can only use one Node (last) for the whole, 
    //  plus we have to follow FIFO (it's a Queue)
    // we have to traverse the list to link the nodes
    void addNode(T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            last = temp;
        } else if (N  == 1) {
            last->next = temp;
            temp->next = last;
        } else {
            Node* linker = last;
            for (int i = 1; i < N; i++) {
                linker = linker->next;
            }
            linker->next = temp;
            temp->next = last;
        }
        ++N;
    }
    // ver 1 of printList() where it loops forever
    void printList() {
        Node* traverse = last->next;
        while (traverse != NULL) {
            cout << traverse->item << endl;
            traverse = traverse->next;
        }
    }
    /* ver2 where it ends by printing last last
    void printList() {
        cout << last->item << endl;                 // we use 'last' to end the loop so we print it here
        Node* traverse = last->next;
        while (traverse != last) {
            cout << traverse->item << endl;
            traverse = traverse->next;
        }
    }*/

    Node* last;
    int N;
};

int main() {
    QueueCircularLinkedList<string> stringList;
    stringList.addNode("phong");
    stringList.addNode("cuong");
    stringList.addNode("thinh");
    stringList.addNode("khanh");
    stringList.addNode("phong");
    stringList.printList();                                 // create a loop because it's a circular list
}
