#include <iostream>
#include <iterator>
#include <string>
using namespace std;

template<typename T>
class Deque {
public:
    Deque() {
        N = 0;
        i = 0;
        a = new T[1];
        DequeSize = 1;
    }
    bool isEmpty() { return N == 0; }
    int size() { return N; }
    void pushLeft(T item)  {
        ++N;
        if (N == DequeSize) {
            DequeSize *= 2;
            resize(DequeSize);
        } 
        for (int i = N - 1; i >= 0; i--)            // we are calculating with the index, not size, so we have to decrement N (which is the size) by 1
            a[i+1] = a[i];
        a[0] = item;
    }
    void pushRight(T item) {
        ++N;
        if (N == DequeSize) {
            DequeSize *= 2;
            resize(DequeSize);
        }
        a[N-1] = item;
    }
    T popLeft() {
        T item = a[i];
        if (N != 0) {
            a[i] = T();
            a = a + 1;
            --N;
            ++i;
            if (N > 0 && N == DequeSize / 4) {
                DequeSize /= 2;
                resize(DequeSize);
            }
        } else {
            throw out_of_range("There is no element left in the array!");
        }
        return item;
    }
    T popRight() {
        T item = a[N-1];
        if (N != 0) {
            a[N-1] = T();
            --N;
            if (N > 0 && N == DequeSize / 4) {
                DequeSize /= 2;
                resize(DequeSize);
            }
        } else {
            throw out_of_range("There is no element left in the array!");
        }
        return item;
    }
    void resize(int max) {
        T* temp = new T[max];
        for (int i = 0; i < N; i++)
            temp[i] = a[i];
        delete[] a;
        a = temp;
    }

    class Iterator {
    public:
        Iterator(T* InitLoc) {
            current = InitLoc;
        }
        Iterator& operator++() {
            ++current; 
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return *(this->current);
        }
    private:
        T* current;
    };

    Iterator begin() { return Iterator(a); }
    Iterator end() { return Iterator(a + N); }

private:
    T* a;
    int N;
    int i;
    int DequeSize;
};

int main() {
    Deque<string> stringList; 
    stringList.pushRight("phong");
    stringList.pushLeft("thinh");
    stringList.pushRight("khanh");
    stringList.pushLeft("cuong");
    stringList.pushRight("khoa");
    cout << stringList.size() << endl;
    for (auto s : stringList)
        cout << s << endl;
    cout << endl;
    cout << stringList.popLeft() << endl;
    cout << endl;
    cout << stringList.popRight() << endl;
    cout << endl;
    for (auto s : stringList)
        cout << s << endl;
}
