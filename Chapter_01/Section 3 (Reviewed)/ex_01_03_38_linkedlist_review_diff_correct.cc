// LinkedList inplementation
// Solution use doubly linkedlist
// which reduce time complexity from O(n) to O(n/2) (traverse from head or tail based on the value of int k)
#include <iostream>
#include <string>
using namespace std;

template <typename Item>
class GeneralizedQueue {
public:
    GeneralizedQueue() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    struct Node {
        Item item;
        Node* next;
    };

    bool isEmpty() { return N == 0; }
    int size() { return N; }

    void insert(Item item) {
        Node* temp = new Node;
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }

    Item deleteAt(int k) {
        if (k == 0) {
            Node* oldhead = head;
            Item item = oldhead->item;
            head = head->next;
            delete oldhead;
            --N;
            return item;
        } else if (k >0 && k <= N) {
            Node* traverse = head;
            for (int i = 0; i < k - 1; i++)
                traverse = traverse->next;
            Node* dp = traverse->next;
            Item item = dp->item;
            if (k == N) {
                tail = traverse;
                traverse->next = NULL;
            } else {
                traverse->next = traverse->next->next;
            }
            delete dp; 
            --N;
            return item;
        } else 
            throw out_of_range("Not in the range of current queue!");
    }

    void printList() {
        Node* traverse = head;
        while (traverse != NULL) {
            cout << traverse->item << endl;
            traverse = traverse->next;
        }
        cout << endl;
    }

private:
    Node* head;
    Node* tail;
    int N;
};
 
int main() {
    GeneralizedQueue<string> stringQueue;
    stringQueue.insert("phong");
    stringQueue.insert("cuong");
    stringQueue.insert("khanh");
    stringQueue.insert("thinh");
    stringQueue.printList();
    stringQueue.deleteAt(2);
    stringQueue.printList();
    stringQueue.deleteAt(2);
    stringQueue.printList();
}
