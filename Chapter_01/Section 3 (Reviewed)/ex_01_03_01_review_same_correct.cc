#include <iostream>
#include <string>
#include <memory>
using namespace std;

class FixedCapacityStackOfStrings {
public:
    FixedCapacityStackOfStrings() = default;

    FixedCapacityStackOfStrings(int cap) {
        N = 0;
        capacity = cap;
        arr = new string[cap];
    }

    ~FixedCapacityStackOfStrings() { delete[] arr; }

    bool isEmpty() { return N == 0; }
    int size() { return N; }
    
    void push(string item) {
        arr[N++] = item;
    } 

    string pop() {
        string temp;
        if (!isEmpty())
            temp = arr[--N];
        else {
            cout << "No element to pop" << endl;
            return "";
        }
        return temp;
    }

    bool isFull() {
        return N == capacity;
    }

private:
    int N;          // the number of items inside the stack
                    // not to be confused with StackCapacity
    int capacity;
    string* arr;    // point to the first element in the array
};

// test client
int main() {
    int cap = 5; 
    FixedCapacityStackOfStrings string_stack(cap);
    string_stack.push("phong");
    string_stack.push("phong");
    string_stack.push("thinh");
    string_stack.push("cuong");
    string_stack.push("cuong");
    // Before the pop
    if (string_stack.isFull())
        cout << "The stack is now full" << endl;
    else
        cout << "The stack still have more places" << endl;

    cout << string_stack.pop() << endl;
    cout << string_stack.pop() << endl;
    cout << string_stack.pop() << endl;
    cout << string_stack.pop() << endl;
    cout << string_stack.pop() << endl;
    // After the pop
    if (string_stack.isFull())
        cout << "The stack is now full" << endl;
    else
        cout << "The stack still have more places" << endl;
    return 0;
}
