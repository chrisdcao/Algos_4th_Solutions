#include <iostream>
#include <string>
#include "Stack.h"
using namespace std;

class Buffer {
private:
    int nLeft;
    int nRight;
    Stack<char> leftStack;
    Stack<char> rightStack;
        
public:
    Buffer() {
        nLeft = 0;
        nRight = 0;
    }


    int size() { return nLeft + nRight; }

    void insert(char c) {
        leftStack.push(c);
        nLeft++;
    }

    char deletec() {             // deleteing the char behind the cursor (which belongs to the right stack)
        return rightStack.pop();
    }

    void left(int k) {
        for (int i = 0; (i < k) && !leftStack.isEmpty(); i++) {
            rightStack.push(leftStack.pop());
            nLeft--;
            nRight++;
        }
    }
 
    void right(int k) {
        for (int i = 0; (i < k) && !rightStack.isEmpty(); i++) {
            leftStack.push(rightStack.pop());
            nRight--;
            nLeft++;
        }
    }

    void printBuffer() {
        leftStack.printList();
        rightStack.printList();
    }
};

int main() {
    Buffer buffer;
    buffer.insert('c');
    buffer.insert('u');
    buffer.insert('o');
    buffer.insert('n');
    buffer.insert('g');
    buffer.left(2);
    cout << "Expected outcome:" << "\nLeft Stack: o u c" 
                                << "\nRight Stack: n g" << endl;
    buffer.printBuffer();
    cout << endl;
    buffer.right(3);
    cout << "Expected outcome:" << "\nLeft Stack: g n o u c" 
                                << "\nRight Stack: NONE" << endl;
    buffer.printBuffer();
    cout << endl;
}
