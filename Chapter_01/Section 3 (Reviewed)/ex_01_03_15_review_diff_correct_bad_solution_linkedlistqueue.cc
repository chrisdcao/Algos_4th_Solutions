#include <iostream>
#include <string>
#include "Queue.h"
using namespace std;

// client

void kth_element_from_last(Queue<string>& queue, int k) {
    if (k > queue.size()) {
        cout << "Index Out of Bound, please try again!" << endl;
        return;
    }
    if (k < 0) {
        cout <<  "index cannot be negative" << endl;
        return;
    }
    int i = 0;
    for (auto s : queue) {
        ++i;
        if (i == queue.size() - k) 
            cout << s << endl;
    }
}

int main() {
    Queue<string> queue;
    queue.enqueue("phong");        
    queue.enqueue("thinh");
    queue.enqueue("cuong");
    queue.enqueue("khanh");
    queue.enqueue("khoa");
    kth_element_from_last(queue, 2);
    kth_element_from_last(queue, 11);
    kth_element_from_last(queue, 1);
}
