#include <iostream>
using namespace std;       

template <typename T>
class LinkedList {
public:
    struct Node {
        T item;
        Node* next;
    };
    LinkedList() {
        N = 0;
        head = NULL;
        tail = NULL;
    }
    int size() { return N; }
    bool isEmpty() { return N == 0; }
    T itemHead() { return head->item; }
    T itemTail() { return tail->item; }
    void addNode(T item) {
        Node* temp = new Node;
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }
    int removeAfter(Node* node) {
        if (node != NULL && node->next != NULL) {
            // if the next node is tail and we skip it, have to re-define the tail
            if (node->next->next == NULL) {
                node->next = node->next->next;
                tail = node;
            } else 
                node->next = node->next->next;
            --N;
            return 1;
        }
        cout << "Nothing to be done here!" << endl;
        return 0;
    }
    void printList() {
        Node* traverse = head;
        while (traverse != NULL) {
            cout << traverse->item << " ";
            traverse = traverse->next;
        }
        cout << endl;
    }
    Node* NodeAtIndex(int k) {
        if (k > N) {
            cout << "\n\nOyyyy!!! The maximum index of the list now is: " << N - 1 << endl;
            throw out_of_range("You have crossed the maximum index of the list!");
        } else {
            Node* traverse = head;
            for (int i = 0; i < k; i++) 
                traverse = traverse->next;
            return traverse;
        }
    }

private:
    Node* head;
    Node* tail;
    int N;
};

int main() {
    //LinkedList<string> list;
    //list.addNode("phong");      // index 0
    //list.addNode("khoa");       // index 1
    //list.addNode("thinh");      // index 2
    //list.addNode("khanh");      // index 3
    //list.printList();
    //cout << "\nThe tail before removal: " << list.itemTail() << endl;
    //list.removeAfter(list.NodeAtIndex(2));      // remove index After index 2, which is index 3
    //list.printList();
    //cout << "\nThe tail after removal: " << list.itemTail() << endl;
    //list.removeAfter(list.NodeAtIndex(4));      // remove index After index 2, which is index 3
    LinkedList<int> intList;
    intList.addNode(0);
    intList.addNode(1);
    intList.addNode(2);
    intList.addNode(3);
    intList.addNode(4);
    cout << "List before remove: ";
    intList.printList();
    cout << "List after removal: ";
    intList.removeAfter(intList.NodeAtIndex(0));
    intList.printList();
    cout << "Expected outcome: 0 2 3 4";
    cout << endl;
}
