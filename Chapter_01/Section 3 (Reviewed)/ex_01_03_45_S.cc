// I have not been able to solve this exercise
// and have to looked into solution
// the code below are solution-based
#include <iostream>
#include <vector>
#include <sstream>
#include <string>
using namespace std;

template <typename T>
class Stack {
public:
    Stack() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void push(T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->next = head;
            head = temp;
        }
        ++N;
    }

    void enqueue(T item) {
        Node* temp = new Node;
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }

    T pop() {
        T item = head->item;
        Node* dp = head;
        head = head->next;
        delete dp;
        --N;
        return item;
    }

    T peek() {
        T item = head->item;
        return item;
    }

protected:
    struct Node {
        T item;
        Node* next;
    };

private:
    Node* head;
    Node* tail;
    int N;
};


int main() {
    string s;
    cout << "Please input a sequence with minus being the pop()for the underflow test: ";
    getline(cin, s);
    stringstream ss(s);
    Stack<string> ops;
    Stack<string> vals;
    string word;


    // underflow test
    while(ss >> word) {
        if (word == "-") ops.push(word);
        else vals.push(word);
    }
    if (ops.size() > vals.size())
        cout << "Underflow!" << endl;
    else
        cout << "Not Underflow!" << endl;


    // permutation test
    cout << "Please input a permutation sequence for the test: " << endl;
    int input;
    vector<int> stack1;
    Stack<int> stack2;
    int current = 0;
    while (cin >> input) 
        stack1.push_back(input);
    for (auto integerValue : stack1) {
        if (stack2.isEmpty() || integerValue > stack2.peek()) {
            while (current < integerValue) {
                cout << current << endl;
                stack2.push(current);
                current++;
            }

            current++;
        } else if (integerValue == stack2.peek()) {
            stack2.pop();
        } else {
            cout << "cannot permut" << endl;
        }
    }
}
