We have a fragment of code (in Java):

Stack<String> stack = new Stack<String>();
while (!q.isEmpty())
    stack.push(q.dequeue());
while (!stack.isEmpty())
    q.enqueue(stack.pop());

By doing this, we will reverse the order of element in the queue
