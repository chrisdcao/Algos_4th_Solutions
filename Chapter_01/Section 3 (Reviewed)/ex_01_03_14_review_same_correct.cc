// đề bài yêu cầu sử dụng Array để code 
#include <iostream>
#include <string>
using namespace std;

template <typename T>
class ResizingArrayQueueOfStrings {
private:
    T* a;
    int queueSize;
    int N;
    int j;              // running pointer
    void resize(int max) {
        T* temp = new T[max];
        for (int i = 0; i < N; i++)
            temp[i] = a[i];
        T* dp = a;
        a = temp;
        delete[] dp;
    }

public:
    ResizingArrayQueueOfStrings() {
        N = 0;
        j = 0;
        a = new T[1];
        queueSize = 1;
    }
    bool isEmpty() { return N == 0; }
    int size() { return N; }
    void enqueue(T item) {
        a[N++] = item;
        if (N == queueSize) {
            queueSize *= 2;
            resize(queueSize);
        }
    }
    T dequeue() {
        T value = a[j];
        if (N != 0) {
            a[j] = T();
            ++j;
            --N;
        } else 
            throw out_of_range("Underflow");
        return value;
    }
    class Iterator {
        Iterator(T* initLoc) {
            current = initLoc;
        }
        Iterator& operator++() {
            ++current;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return *(this->current);
        }
    private:
        T* current;
    };

    Iterator begin() { return Iterator(a); }
    Iterator end() { return Iterator(a + N); }
};

int main() {
    ResizingArrayQueueOfStrings<string> queue;
    queue.enqueue("phong");
    queue.enqueue("thinh");
    queue.enqueue("cuong");
    queue.enqueue("phong");
    cout << queue.size() << endl;
    cout << queue.dequeue() << endl;
    cout << queue.dequeue() << endl;
    cout << queue.dequeue() << endl;
    cout << ((queue.isEmpty()) ? "empty" : "not empty") << endl;
    cout << queue.dequeue() << endl;
    cout << ((queue.isEmpty()) ? "empty" : "not empty") << endl;
}
