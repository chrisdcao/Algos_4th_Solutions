#include <iostream>
#include <string>
#include <sstream>
using namespace std;

template <typename T>
class Stack {
private:
    struct Node {
        T item;
        Node* next;
    };

    Node* head;
    Node* tail;

    int N;
    
    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* initLoc) {
            current = initLoc;
        }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return this->current->item;
        }
    };

public:
    Stack() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    bool isEmpty() { return N == 0; }
    int size() { return N; }

    void push(T item) {
        Node* tmp = new Node;
        tmp->item = item;
        if (N == 0) {
            tmp->next = NULL;
            head = tmp;
            tail = tmp;
        } else {
            tmp->next = head;
            head = tmp;
        }
        ++N;
    }

    T pop() {
        T item = head->item;
        if (N != 0) {
            Node* dp = head;
            head = head->next;
            delete dp;
            --N;
        } else 
            throw out_of_range("Underflow");
        return item;
    }
    
    Iterator begin() { return Iterator(head); }
    Iterator end() { return Iterator(tail->next); }
};

int main() {
    string s;
    Stack<string> operands;
    Stack<string> operators;
    Stack<string> result;
    string sumstring = "";
    int open = 0;
    int closebraces = 0;
    int oldOperandsSize = 0;
    while (cin >> s) {
        if (s == ")") {
            if (sumstring.size() > 0) {
                if (open == 0)
                    sumstring = "( " + sumstring + " )";
                else {
                    sumstring += " )";
                    --open;
                }
                oldOperandsSize = operands.size();
                operands.push(sumstring);
                sumstring = "";
            } else
                closebraces++;
        } else if ((s == "+" || s == "-" || s == "*" || s == "/") && (operands.size() != oldOperandsSize)) {
            operators.push(s);
            oldOperandsSize = operands.size();
        } else  {
            if (s == "+" || s == "-" || s == "*" || s == "/") 
                sumstring = sumstring + " " + s + " ";
            else {
                if (s == "(" && sumstring.size() != 0) {
                    sumstring = sumstring + " " + s + " ";
                    ++open;
                } else if (s == "(" && sumstring.size() == 0)  {
                    sumstring = sumstring + s + " ";
                    ++open;
                } else
                    sumstring += s;
            }
        }
    }
    if (sumstring.size() != 0) {
        operands.push(sumstring);
    }
    while (!operators.isEmpty()) {
        string op = operators.pop();
        string value2 = operands.pop();
        string value1 = operands.pop();
        if (closebraces != 0) {
            string input = "( " + value1 + " " + op + " " + value2 + " )";
            operands.push(input);
            --closebraces;
        } else {
            string input = value1 + " " + op + " " + value2;
            operands.push(input);
        }
    }
    for (auto s : operands)
        cout << s;
    cout << endl;
}
