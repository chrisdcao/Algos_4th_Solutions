#include <iostream>
#include <string>
#include "ResizingArrayStack.h"
using namespace std;

int main() {
    Stack<string> string_stack;
    string s;
    while (cin >> s) {
        string_stack.push(s);
    }
    string_stack.printList();
    while (!string_stack.isEmpty())
        cout << string_stack.pop() << endl;
    return 0;
}
