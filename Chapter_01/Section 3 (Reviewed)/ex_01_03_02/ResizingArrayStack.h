#include <iostream>
#include <vector>
#include <memory>
#include <iterator>
using namespace std;

template<typename T>
class Stack {
protected:
    struct Node {
        T item;
        Node* next;
    };
public:
    Stack() {
        N = 0;
        head = NULL;
        tail = NULL;
    }
    
    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void push (T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->next = head;
            head = temp;
        }
        ++N;
    }

    T pop() {
        T item = head->item;
        Node* dp = head;
        head = head->next;
        delete dp;
        --N;
        return item;
    }

    void printList() {
        Node* traverse = head;
        while (traverse != NULL) {
            cout << traverse->item << endl;
            traverse = traverse->next;
        }
        cout << endl;
    }

private:
    Node* head;
    Node* tail;
    int N;
};
