#include <iostream>
#include <sstream>
#include <string>
#include "ResizingArrayStack.h"
using namespace std;

int main() {
    string s;                   // to get the input
    ResizingArrayStack<char> brace_stack;
    getline(cin, s);
    stringstream ss(s);
    string word;                // to read the input from stringstream
    bool a = true;
    while (ss >> word) {
        for (auto& c : word) {
            if (c == ')') {
                if (brace_stack.pop() != '(') {
                    a = false;
                    break;
                }
            }
            else if (c == ']') {
                if (brace_stack.pop() != '[') {
                    a = false;
                    break;
                }
            }
            else if (c == '}') {
                if (brace_stack.pop() != '{') {
                    a = false;
                    break;
                }
            }
            else
                brace_stack.push(c);
        }
    }


    if (a) {
        for (auto x : brace_stack) {
            if (x == '[' || x == '(' || x == '{') {
                cout << "false" << endl;
                return -1;
            }
        }
        cout << "true" << endl;
    } else {
        cout << "false" << endl;
    }

    return 0;
}
