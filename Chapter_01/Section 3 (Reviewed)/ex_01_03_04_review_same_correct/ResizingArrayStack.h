#include <iostream>
using namespace std;

template <typename T>
class ResizingArrayStack {
public:
    ResizingArrayStack() {
        N = 0;
        a = new T[1];
        stackSize = 1;      // có 1 ô phần tử rỗng 
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    T pop() {
        T item = a[N - 1];      // index luôn nhỏ hơn size 1 bởi vì bắt đầu đánh số từ 0
        --N;
        if (N > 0 && N == stackSize / 4) {
            stackSize = stackSize / 2;
            resize(stackSize);
        }
        return item;
    }

    void push(T item) {
        ++N;
        if (N == stackSize) {
            stackSize = stackSize * 2;
            resize(stackSize);
        } 
        a[N-1] = item;
    }

    void resize(int max) {
        T* tmp = new T[max];
        T* dp = a;
        for (int i = 0; i < N; i++)
            tmp[i] = a[i];
        a = tmp;
        delete[] dp;
    }

    class ReverseArrayIterator {
    public:
        ReverseArrayIterator(T* InitLoc) {
            current = InitLoc;
        }
        bool operator!=(ReverseArrayIterator& rhs) {
            return this->current != rhs.current;
        }
        ReverseArrayIterator& operator++() {
            current--;
            return *this;
        }
        T operator*() {
            return *(this->current);
        }
    private:
        T* current;
    };

    ReverseArrayIterator begin() { return a + N - 1; }
    ReverseArrayIterator end() { return a - 1; }

private:
    T* a;
    int N;
    int stackSize;
};
