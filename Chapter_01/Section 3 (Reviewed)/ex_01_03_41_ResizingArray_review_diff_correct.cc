#include <iostream>
#include <string>
using namespace std;

template<typename T>
class Queue {
public:
    Queue() {
        N = 0;
        j = 0;
        a = new T[1];
        cap = 1;
    }

    Queue(Queue<T>& q) {
        // make an independent copy of Queue q
        N = q.size();
        j = 0;
        // based on our resizing algo, 
        // the cap will always be double of the size (if N == cap / 4 then cap = cap / 2 which is 2 * N)
        // or (if N == cap then cap = cap * 2)
        cap = q.size() * 2;
        T* temp = new T[q.size()];
        for (int i = 0; i < q.size(); i++)
            temp[i] = q[i];
        a = temp;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void enqueue(T item) {
        ++N;
        if (N == cap) {
            cap *= 2;
            resize(cap);
        }
        a[N-1] = item;
    }
    
    T dequeue() {
        T item = a[j];
        a[j] = T();
        a = a + 1;
        ++j;
        --N;
        if (N > 0 && N == cap / 4) {
            cap /= 2;
            resize(cap);
        }
        if (N == 0) 
            throw out_of_range("No more items left to dequeue!");
        return item;
    }

    void resize(int max) {
        T* temp = new T[max];
        for (int i = 0; i < N + j; i++)
            temp[i] = a[i];
        delete[] a;
        a = temp;
    }

    T operator[](int index) {
        return a[index];
    }

    class Iterator {
    public:
        Iterator(T* InitLoc) {
            current = InitLoc;
        }
        Iterator& operator++() {
            ++current;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return *(this->current);
        }

    private:
        T* current;
    };

    Iterator begin() { return Iterator(a); }
    Iterator end() { return Iterator(a + N); }

private:
    T* a;
    int N;
    int j;
    int cap;
};

int main() {
    Queue<string> stringQueue1;
    stringQueue1.enqueue("phong");
    stringQueue1.enqueue("thinh");
    stringQueue1.enqueue("cuong");
    stringQueue1.enqueue("khoa");
    cout << "Expected outcome: 4" << endl;
    cout << "Real outcome: ";
    cout << stringQueue1.size() << endl;
    cout << "Expected outcome: phong thinh cuong khoa" << endl;
    cout << "Real outcome: ";
    for(auto s : stringQueue1)
        cout << s << " ";
    cout << endl;
    cout << endl;
    Queue<string> stringQueue2(stringQueue1);
    cout << "Expected outcome: phong thinh cuong khoa" << endl;
    cout << "Real outcome: ";
    for(auto s : stringQueue2)
        cout << s << " ";
    cout << endl;
    cout << endl;
    cout << "Expected outcome: 4" << endl;
    cout << "Real outcome: ";
    cout << stringQueue2.size() << endl;
    cout << endl;
    cout << "Expected outcome: phong" << endl;
    cout << "Real outcome: ";
    cout << stringQueue2.dequeue() << endl;
    cout << "Expected outcome: 3" << endl;
    cout << "Real outcome: ";
    cout << stringQueue2.size() << endl;
    cout << endl;
    cout << "Expected outcome: phong thinh cuong khoa" << endl;
    cout << "Real outcome: ";
    for(auto s : stringQueue1)
        cout << s << " ";
    cout << endl;
}
