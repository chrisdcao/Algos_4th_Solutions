#include "Queue.h" 
using namespace std;

int main() {
    Queue<string> queue;
    queue.enqueue("phong");        
    queue.enqueue("thinh");
    queue.enqueue("cuong");
    queue.enqueue("khanh");
    queue.enqueue("khoa");
    for (auto s : queue)
        cout << s << endl;
    cout << endl;
}
