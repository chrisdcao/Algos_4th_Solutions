#include <iostream>
#include <string>
using namespace std;

static int operationCount;
static int initCount = -1;
template <typename T>
class Stack {
private:
    struct Node {
        T item;
        Node* next;
    };
    Node* tail;
    Node* head;
    int N;

    class Iterator {
    public:
        Iterator(Node* InitLoc) {
            initCount = operationCount;
            current = InitLoc;
        }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return this->current->item;
        }

    private:
        Node* current;
    };

public:
    Stack() {
        N = 0;
        operationCount = 0;
        head = NULL;
        tail = NULL;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void push(T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else  {
            temp->next = head;
            head = temp;
        }
        N++;
        operationCount++;
    }

    T pop() {
        T item = head->item;
        if (N != 0) {
            Node* dp = head;
            head = head->next;
            delete dp;
            --N;
            operationCount++;
        } else
            throw out_of_range("Underflow");
        return item;
    }

    T peek() {
        return head->item;
    }

    Iterator begin() { 
        if (initCount != -1) {
            if (initCount != operationCount)
                throw runtime_error("Concurrent Modified");
        }
        return Iterator(head); 
    }
    Iterator end() { 
        //Node* traverse = head;
        //for (int i = 0; i < 2; i++)               // to print within a range only
            //traverse = traverse->next;
        //return Iterator(traverse->next); 
        return Iterator(tail->next);
    }
};

int main() {
    Stack<string> string_stack;
    string_stack.push("thinh");
    string_stack.push("phong");
    string_stack.push("cuong");
    string_stack.push("khanh");
    string_stack.push("phong");
    string_stack.push("phong");
    for (auto s : string_stack)
        cout << s << endl;
    string_stack.push("phong");
    for (auto s : string_stack)
        cout << s << endl;
}
