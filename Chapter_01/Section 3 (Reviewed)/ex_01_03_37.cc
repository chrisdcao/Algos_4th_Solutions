#include <iostream>
#include <string>
using namespace std;

template <typename T>
class QueueCircularLinkedList {
public:
    struct Node {
        T item;
        Node* next;
    };
    QueueCircularLinkedList() {
        N = 0;
        last = NULL;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }
    Node* getHead() { return last; }

    void addNode(T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            last = temp;
        } else if (N  == 1) {
            last->next = temp;
            temp->next = last;
        } else {
            Node* linker = last;
            for (int i = 1; i < N; i++) {
                linker = linker->next;
            }
            linker->next = temp;
            temp->next = last;
        }
        ++N;
    }
    void printList() {
        Node* traverse = last->next;
        while (traverse != NULL) {
            cout << traverse->item << endl;
            traverse = traverse->next;
        }
    }
private:
    Node* last;
    int N;
};

using LinkedListNode = QueueCircularLinkedList<int>::Node;

int main(int argc, char** argv) {
    QueueCircularLinkedList<int> intList;
    for (int i = 0; i < strtol(argv[1], NULL, 10); i++) 
        intList.addNode(i);
    LinkedListNode* traverse = intList.getHead();
    while (traverse != traverse->next) {
        for (int i = 1; i < strtol(argv[2], NULL, 10) - 1; i++)
            traverse = traverse->next;
        cout << traverse->next->item << " ";
        traverse->next = traverse->next->next;
        traverse = traverse->next;
    }
    cout << traverse->item << endl;
}
