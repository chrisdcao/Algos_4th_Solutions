#include <iostream>
#include <string>
using namespace std;

template <typename T>
class GeneralizedQueue {
public:
    GeneralizedQueue() {
        N = 0;
        a = new T[1];
        cap = 1;
    }
    bool isEmpty() { return N == 0; }
    void insert(T item) {
        ++N;
        if (N == cap) {
            cap *= 2;
            resize(cap);
        }
        a[N-1] = item;
    }
    T deleteAt(int k) {
        if (k < 0) 
            throw runtime_error("You cannot input negative number!");
        else if (k >= N) 
            throw out_of_range("You have passed the maximum index!");
        else {
            T item = a[k];
            a[k] = T();
            for (int i = k + 1; i < N; i++) {
                a[i-1] = a[i];
            }
            --N;
            return item;
        }
    }
    void resize(int max) {
        T* temp = new T[max];
        for (int i = 0; i < N; i++)
            temp[i] = a[i];
        delete[] a;
        a = temp;
    }
    class Iterator {
    public:
        Iterator(T* InitLoc) {
            current = InitLoc;
        }
        Iterator& operator++() {
            ++current;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return *(this->current);
        }

    private:
        T* current;
    };

    Iterator begin() { return Iterator(a); }
    Iterator end() { return Iterator(a + N); }

private:
    T* a;
    int N;
    int cap;
};
 
int main() {
    GeneralizedQueue<string> stringQueue;
    stringQueue.insert("phong");
    stringQueue.insert("cuong");
    stringQueue.insert("khanh");
    stringQueue.insert("thinh");
    for (auto s : stringQueue) 
        cout << s << endl;
    cout << endl;
    stringQueue.deleteAt(2);
    for (auto s : stringQueue) 
        cout << s << endl;
    cout << endl;
    stringQueue.deleteAt(2);
    for (auto s : stringQueue) 
        cout << s << endl;
    stringQueue.deleteAt(2);
}
