#include <iostream>
using namespace std;

template <typename T>
class MoveToFront {
public:
    struct Node {
        T item;
        Node* next;
    };

    MoveToFront() {
        N = 0;
        head = NULL;
        tail = NULL;
    }
    
    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void addNode(T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else {
            if (head->item == temp->item) { --N; }
            else {
                Node* traverse = new Node;
                traverse->next = head;
                while (traverse->next != NULL) {
                    if (traverse->next->item == temp->item) {
                        Node* dp = traverse->next;
                        traverse->next = traverse->next->next;
                        if (dp == tail) 
                            tail = traverse; 
                        delete dp;
                        --N;
                    } else
                        traverse = traverse->next;
                }
                temp->next = head;
                head = temp;
            }
        }
        ++N;
    }

    void printList() {
        Node* traverse = head;
        while (traverse != NULL) {
            cout << traverse->item << " ";
            traverse = traverse->next;
        }
        cout << endl;
    }

private:
    Node* head;
    Node* tail;
    int N;
};

int main() {
    MoveToFront<char> charList;
    charList.addNode('c');
    charList.addNode('d');
    charList.addNode('p');
    charList.addNode('x');
    charList.addNode('c');
    cout << "Expected outcome: " << "c x p d " << endl;
    cout << "Expected size: " << "4" << endl;
    cout << "Real outcome: ";
    charList.printList();
    cout << "Real size: " << charList.size() << endl;
}
