#include <iostream>
#include <string>
using namespace std;

template <typename T>
class CircularLinkedList {
private:
    struct Node{
        T item;
        Node* next;
    };
    Node* last;
    int N;
public:
    int size() { return N; }
    bool isEmpty() { return N == 0; }

    CircularLinkedList() {
        N = 0;
        last = NULL;
    }

    void insert(T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            last = temp;
        } else if (N == 1) {
            temp->next = last;
            last->next = temp;
        } else {
            Node* traverse = last;
            for (int i = 1; i < N; i++)
                traverse = traverse->next;
            traverse->next = temp;
            temp->next = last;
        }
        ++N;
    }

    T pop() { 
        T item = last->next->item;
        if (N != 0) {
            Node* dp = last->next;
            last->next = last->next->next;
            delete dp;
            --N;
        }
        return item;
    }

    void printList() {
        Node* traverse = last->next;
        cout << last->item << endl;
        while(traverse != last) {
            cout << traverse->item << endl;
            traverse = traverse->next;
        }
    }

};
