#include <iostream>
using namespace std;

template <typename T>
class LinkedList {
public:
    struct Node {
        T item;
        Node* next;
    };
    LinkedList() {
        N = 0;
        head = NULL;
        tail = NULL;
    }
    int size() { return N; }
    bool isEmpty() { return N == 0; }
    Node* getHead() { return head; }
    void addNode(T item) {
        Node* temp = new Node;
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }
    void printList() {
        Node* traverse = head;
        while(traverse != NULL) {
            cout << traverse->item << endl;
            traverse = traverse->next;
        }
        cout << endl;
    }

private:
    Node* head;
    Node* tail;
    int N;
};

using LinkedListNode = LinkedList<int>::Node;
 
int max(LinkedListNode* first) {
    int value = 0;
    while (first != NULL ) {
        if (first->item > value)
            value = first->item;
        first = first->next;
    }
    return value;
}

int main() {
    LinkedList<int> intList;
    intList.addNode(1);
    intList.addNode(27);
    intList.addNode(5);
    intList.addNode(2);
    intList.addNode(19);
    intList.addNode(8);
    LinkedListNode* first = intList.getHead();
    cout << max(first) << endl;
    intList.addNode(44);
    cout << max(first) << endl;
}
