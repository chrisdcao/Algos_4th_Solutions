#include <iostream>
#include <string>
using namespace std;

template <typename T>
class Stack {
public:
    struct Node {
        T item;
        Node* next;
    };

    Stack() {
        N = 0;
        head = 0;
        tail = 0;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }
    Stack(Stack<T>& t) {
        N = 0;
        const int threshold = t.size();
        for(int i = 0; i < threshold; i++) {
            T hold = t.pop();
            t.enStack(hold);
            this->enStack(hold);
        }
    }

    void push(T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->next = head;
            head = temp;
        }
        ++N;
    }

    T pop() {
        T item = head->item;
        Node* dp = head;
        head = head->next;
        delete dp;
        --N;
        return item;
    }

    void printList() {
        Node* traverse = head;
        while(traverse !=  NULL) {
            cout << traverse->item << " ";
            traverse = traverse->next;
        }
        cout << endl;
    }

private:

    void enStack(T item) {
        Node* temp = new Node;
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }
    Node* head;
    Node* tail;
    int N;
};

int main() {
    Stack<string> stringStack1;
    stringStack1.push("phong");
    stringStack1.push("thinh");
    stringStack1.push("cuong");
    stringStack1.push("khoa");
    cout << "Expected outcome: 4" << endl;
    cout << "Real outcome: ";
    cout << stringStack1.size() << endl;
    cout << "Expected outcome: khoa cuong thinh phong" << endl;
    cout << "Real outcome: ";
    stringStack1.printList();
    cout << endl;
    cout << endl;
    Stack<string> stringStack2(stringStack1);
    cout << "Expected outcome: khoa cuong thinh phong" << endl;
    cout << "Real outcome: ";
    stringStack2.printList();
    cout << endl;
    cout << endl;
    cout << "Expected outcome: 4" << endl;
    cout << "Real outcome: ";
    cout << stringStack2.size() << endl;
    cout << endl;
    cout << "Expected outcome: khoa" << endl;
    cout << "Real outcome: ";
    cout << stringStack2.pop() << endl;
    cout << "Expected outcome: 3" << endl;
    cout << "Real outcome: ";
    cout << stringStack2.size() << endl;
    cout << endl;
    cout << "Expected outcome: khoa cuong thinh phong" << endl;
    cout << "Real outcome: ";
    stringStack1.printList();
}
