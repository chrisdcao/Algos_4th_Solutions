#include <iostream>
#include <string>
using namespace std;

template <typename T>
class Deque {
public:
    struct DoubleNode {
        T item;
        DoubleNode* prev;
        DoubleNode* next;
    };

    bool isEmpty() { return N == 0; 
    int size() { return N; }
    DoubleNode* getHead() { return head; }
    DoubleNode* getTail() { return tail; }

    Deque() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    void pushLeft(T item) {
        DoubleNode* temp = new DoubleNode;   
        temp->item = item;
        temp->prev = NULL;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->next = head;
            head->prev = temp;
            head = temp;
        }
        ++N;
    }
    void pushRight(T item) {
        DoubleNode* temp = new DoubleNode;
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            temp->prev = 0;
            tail = temp;
            head = temp;
        } else {
            temp->prev = tail;
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }
    T popLeft() {
        T popItem = head->item;
        DoubleNode* oldhead = head;
        head = head->next;
        head->prev = NULL;
        delete oldhead;
        --N; 
        return popItem;
    } 
    T popRight() {
        T popItem = tail->item;
        DoubleNode* oldtail = tail;
        tail = tail->prev;
        tail->next = NULL;
        delete oldtail;
        --N;
        return popItem;
    }
    void printList() {
        DoubleNode* traverse = head;
        while(traverse != NULL) {
            cout << traverse->item << " ";
            traverse = traverse->next;
        }
        cout << endl;
    }
    void printListR() {
        DoubleNode* traverse = tail;
        while(traverse != NULL) {
            cout << traverse->item << " ";
            traverse = traverse->prev;
        }
        cout << endl;
    }

private:
    DoubleNode* head;
    DoubleNode* tail;
    int N;
};
