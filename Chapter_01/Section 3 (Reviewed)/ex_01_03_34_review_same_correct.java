import java.util.*;
import java.lang.*;

public class ex_01_03_34 {
    public static void main(String[] args) {
        RandomBag<String> stringBag = new RandomBag<String>();
        stringBag.add("phong");
        stringBag.add("cuong");
        stringBag.add("thinh");
        stringBag.add("khanh");
        stringBag.add("hung");
        stringBag.add("long");
        stringBag.add("hang");
        System.out.println(stringBag.size());
        for (String s : stringBag)
            System.out.println(s);
    }
}

public class RandomBag<Item> implements Iterable<Item> {
    private Item[] a = (Item[]) new Object[1];
    private int N = 0;

    boolean isEmpty() { return N == 0; }
    int size() { return N; }

    public void add(Item item) {
        if (N == a.length) resize(2*a.length);
        a[N++] = item;
    }

    private void resize(int max) {
        Item[] temp = (Item[]) new Object[max];
        for (int i = 0; i < N; i++)
            temp[i] = a[i];
        a = temp;
    }

    public Iterator<Item> iterator() {
        return new RandomIterator();
    }

    private class RandomIterator implements Iterator<Item> {
        RandomIterator() {
            for (int j = 0; j < N; j++) {
                int r = j + StdRandom.uniform(N - j);
                Item temp = a[j];
                a[j] = a[r];
                a[r] = temp;
            }
        }
        private int i = N;
        public boolean hasNext() { return i > 0; }
        public Item next() { 
            return a[--i]; 
        }
        public void remove() {} 
    }
} 
