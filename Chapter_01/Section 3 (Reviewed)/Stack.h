#include <iostream>
#include <string>
using namespace std;

template <typename T>
class Stack {
private:
    struct Node {
        T item;
        Node* next;
    };
    Node* head;
    Node* tail;
    int N;

    class Iterator {
    public:
        Iterator(Node* InitLoc) {
            current = InitLoc;
        }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return this->current->item;
        }

    private:
        Node* current;
    };

public:
    Stack() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }
    Node* getHead() { return head; }

    void push(T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            tail = temp;
            head = temp;
        } else {
            temp->next = head;
            head = temp;
        }
        ++N;
    }

    T pop() {
        T item = head->item;
        if (N != 0) {
            Node* dp = head;
            head = head->next;
            delete dp;
            --N;
        }
        return item;
    }

    T peek() {
        if (head != NULL)
            return head->item;
        else 
            throw out_of_range("Nothing left to peek!");
    }

    void printList() {
        Node* traverse = head;
        while (traverse != NULL) {
            cout << traverse->item << " ";
            traverse = traverse->next;
        }
    }
    
    void printListR(Node* head) {
        if (head == NULL) return;
        printListR(head->next);
        cout << head->item << " ";
    }

    Iterator begin() { return Iterator(head); }
    Iterator end() { 
        //Node* traverse = head;
        //for (int i = 0; i < 2; i++)               // to print within a range only
            //traverse = traverse->next;
        //return Iterator(traverse->next); 
        return Iterator(tail->next);
    }
};
