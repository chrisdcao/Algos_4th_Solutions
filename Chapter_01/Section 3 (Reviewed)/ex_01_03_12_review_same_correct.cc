#include <iostream>
#include <string>
using namespace std;

template <typename T>
class Stack {
private:
    struct Node {
        T item;
        Node* next;
    };

    Node* head;
    Node* tail;
    int N;

    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* InitLoc) {
            current = InitLoc;
        }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return this->current->item;
        }
    };

public:
    Stack() {
        N = 0;
        tail = NULL;
        head = NULL;
    }

    void push(T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->next = head;
            head = temp;
        }
        ++N;
    }

    T pop() {
        T item = head->item;
        if (N != 0) {
            Node* dp = head;
            head = head->next;
            delete dp;
            --N;
        } else 
            throw out_of_range("Underflow");
        return item;
    }

    Iterator begin() { return Iterator(head); }
    Iterator end() { return (tail->next); }

};

Stack<string> copy(Stack<string> target) {
    Stack<string> Reversecopyversion;
    Stack<string> copyversion;
    for (auto s : target)
        Reversecopyversion.push(s);
    for (auto s : Reversecopyversion)
        copyversion.push(s);
    return copyversion;
}

int main() {
    Stack<string> stringList;
    stringList.push("phong");
    stringList.push("cuong");
    stringList.push("thinh");
    stringList.push("khanh");
    Stack<string> copyver = copy(stringList);
    for (auto s : stringList)
        cout << s << " ";
    cout << endl;
    cout << endl;
    for (auto s : copyver)
        cout << s << " ";
    cout << endl;
}
