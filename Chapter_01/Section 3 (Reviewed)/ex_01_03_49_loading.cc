#include <iostream>
#include "Stack.h"
using namespace std;

using Stack<int>::Node* = pNode;

template<typename T>
class Queue {
private:
    Stack<T> dataStack;
    Stack<T*> addressStack;
    Stack<int> DistanceStack;
    Stack<pNode> addressesOfDistanceStack;
    Stack<int> distanceOfDistanceStack;
    int N;

public:
    Queue() { N = 0; }

    void enqueue(T item) {
        if (N == 0) {
            dataStack.push(item);
            addressStack.push(dataStack.getHead());
        } else if (N == 1) {
            dataStack.push(item);
            DistanceStack.push(dataStack.getHead() - addressStack.getHead());
            addressesOfDistanceStack.push(DistanceStack.getHead());
        } else if (N == 2) {
            dataStack.push(item);
            DistanceStack.push(dataStack.getHead() - addressStack.getHead());
            addressOfDistanceStack.push(DistanceStack.getHead());
            distanceOfDistanceStack.push(addressOfDistanceStack.getHead() - addressOfDistanceStack.getHead()->next);
        } else {
            dataStack.push(item);
            DistanceStack.push(addressStack.getHead() - dataStack.getHead());
            addressOfDistanceStack.push(DistanceStack.getHead());
        }
        ++N;
    }

    T dequeue() {
        T item = addressStack.getHead() -  DistanceStack.getHead() + (addressesOfDistanceStack.getHead() + distanceOfDistanceStack.getHead() * j)->item;
        --N;
    }

    void printList() {
        auto traverse = dataStack.getHead();
        for (int i = 0; i < N; i++) {
            cout << traverse->item << endl;
            traverse = traverse->next;
        }
        cout << endl;
    }

};

int main() {
    Queue<string> stringQueue;
    stringQueue.enqueue("phong");
    stringQueue.enqueue("cuong");
    stringQueue.enqueue("thinh");
    stringQueue.printList();
}

