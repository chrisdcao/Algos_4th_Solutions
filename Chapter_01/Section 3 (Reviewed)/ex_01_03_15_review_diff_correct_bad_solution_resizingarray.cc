#include <iostream>
#include <string>
using namespace std;

// standard implementation (no modification)

template <typename T>
class ResizingArrayQueueOfStrings {
private:
    T* a;
    int queueSize;
    int N;
    int j;              // running pointer

    void resize(int max) {
        T* temp = new T[max];
        for (int i = 0; i < N; i++)
            temp[i] = a[i];
        T* dp = a;
        a = temp;
        delete[] dp;
    }

    class Iterator {
    public:
        Iterator(T* initLoc) {
            current = initLoc;
        }
        Iterator& operator++() {
            ++current;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return *(this->current);
        }
    private:
        T* current;
    };


public:
    ResizingArrayQueueOfStrings() {
        N = 0;
        j = 0;
        a = new T[1];
        queueSize = 1;
    }

    bool isEmpty() { return N == 0; }
    int size() { return N; }

    void push(T item) {
        ++N;
        if (N == queueSize) {
            queueSize *= 3;
            resize(queueSize);
        }
        a[N-1] = item;
    }

    T pop() {
        T value = a[j];
        a[j] = T();
        ++j;
        --N;
        return value;
    }
    Iterator begin() { return Iterator(a); }
    Iterator end() { return Iterator(a + N); }
};


// client

void kth_element_from_last(ResizingArrayQueueOfStrings<string>& queue, int k) {
    if (k > queue.size()) {
        cout << "Index Out of Bound, please try again!" << endl;
        return;
    }
    if (k < 0) {
        cout <<  "index cannot be negative" << endl;
        return;
    }
    int i = 0;
    for (auto s : queue) {
        ++i;
        if (i == queue.size() - k) 
            cout << s << endl;
    }
}

int main() {
    ResizingArrayQueueOfStrings<string> queue;
    queue.push("phong");        
    queue.push("thinh");
    queue.push("cuong");
    queue.push("khanh");
    queue.push("khoa");
    kth_element_from_last(queue, 2);
    kth_element_from_last(queue, 11);
    kth_element_from_last(queue, 1);
}
