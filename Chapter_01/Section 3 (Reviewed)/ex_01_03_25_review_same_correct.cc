#include <iostream>
using namespace std;

template <typename T>
class LinkedList {
private:
    struct Node {
        T item;
        Node* next;
    };
    Node* head;
    Node* tail;
    int N;
public:
    LinkedList() {
        N = 0;
        head = NULL;
        tail = NULL;
    }
    int size() { return N; }
    bool isEmpty() { return N == 0; }
    Node* createUnlinkedNode(T item) {
        Node* node = new Node();
        node->item = item;
        return node;
    }
    void addNode (T item) {
        Node* temp = new Node;
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }
    int insertAfter(Node* first, Node* second) { 
        if (first != NULL && second != NULL) {
            // first belong to a list, while the second is the one want to add into the exsiting list of 'first', after first
            Node* oldNode = first->next;
            if (first->next == NULL) {
                tail->next = second;
                second->next = NULL;
                tail = second;
            } else {
                first->next = second;
                second->next = oldNode;
            }
            return 1;
        } else {
            cout << "nothing to be done" << endl;
        }
        return 0;
    }
    Node* NodeAtIndex(int k) {
        if (k > N) {
            cout << "Oyyyy!! The current maximum index is: " << N-1 << endl;
            throw out_of_range("You have crossed the max index of the list!");
        } else {
            Node* traverse = head;
            for (int i = 0; i < k; i++)
                traverse = traverse->next;
            return traverse;
        }
    }
    void printList() {
        Node* traverse = head;
        while (traverse != NULL) {
            cout << traverse->item << " ";
            traverse = traverse->next;
        }
        cout << endl;
    }

};

int main() {
    //LinkedList<string> list;
    //auto decoy = list.createUnlinkedNode("cuong");
    //cout << "\nOriginal list (before insert):";
    //list.addNode("phong");
    //list.addNode("khanh");
    //list.addNode("Khoa");
    //list.addNode("thinh");
    //list.printList();
    //list.insertAfter(list.NodeAtIndex(2), decoy);
    //cout << "\nList (after 1st insert):";
    //list.printList();
    //cout << endl;
    //auto decoy2 = list.createUnlinkedNode("cuong");
    //cout << "\nList (after 2nd insert):";
    //list.insertAfter(list.NodeAtIndex(3), decoy2);
    //list.printList();
    LinkedList<int> intList;
    intList.addNode(0);
    intList.addNode(1);
    intList.addNode(2);
    intList.addNode(3);
    intList.addNode(4);
    cout << "Before insert: ";
    intList.printList();
    auto decoy = intList.createUnlinkedNode(99);
    intList.insertAfter(intList.NodeAtIndex(2), decoy);
    intList.printList();
}
