#include <iostream>
#include <string>
using namespace std;

template <typename T>
class LinkedList {
friend void remove(LinkedList<string>&, string);
public:
    struct Node {
        T item;
        Node* next;
    };
    LinkedList() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    Node* getHead() { return head; }

    void addNode(T item) {
        Node* temp = new Node;
        temp->item = item;        
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }

    void printList() {
        Node* traverse = head;
        while (traverse != NULL) {
            cout << traverse->item << endl;
            traverse = traverse->next;
        }
    }

private:
    Node* head;
    Node* tail;
    int N;
};

using LinkedListNode = LinkedList<string>::Node;

void remove(LinkedList<string>& stringList, string key) {
    LinkedListNode* traverse = new LinkedListNode;
    traverse->next = stringList.head;
    if (traverse->next->item == key) {
        traverse->next = traverse->next->next;
        stringList.head = traverse->next;
    } else {
        while (traverse->next != NULL) {
            if (traverse->next->item == key) {
                    traverse->next = traverse->next->next;
            }
            else
                traverse = traverse->next;
        }
    }
    --stringList.N;
}

int main() {
    LinkedList<string> list;
    list.addNode("phong");
    list.addNode("cuong");
    list.addNode("thinh");
    list.addNode("khanh");
    list.addNode("cuong");
    cout << list.size() << endl;
    remove(list, "phong");
    list.printList();
    cout << list.size() << endl;
}

