#include <iostream>
#include <string>
using namespace std;

template <typename T>
class ResizingArrayStack {
private:
    T* a;
    int N;
    int stackSize;
    void resize(int max) {
        T* temp = new T[max];
        for (int i = 0; i < N; i++)
            temp[i] = a[i];
        delete[] a;
        a = temp;
    }

    class Iterator {
    private:
        T* current;
    public:
        Iterator(T* initLoc) {
            current = initLoc;
        }
        
        Iterator& operator++() {
            ++current;
            return *this;
        }

        bool operator!=(Iterator& rhs) {
            return this->rhs != rhs.current;
        }

        T operator*() {
            return *(this->current);
        }
    };

public:
    ResizingArrayStack() {
        N = 0;
        a = new T[1];
        stackSize = 1;
    }
    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void push(T item) {
        a[N++] = item;
        if (N == stackSize) {
            stackSize *= 2;
            resize(stackSize);
        }
    }

    T pop() {
        return a[N--];
    }

    Iterator begin() { return Iterator(a); }
    Iterator end() { return Iterator(a + N); }
};


