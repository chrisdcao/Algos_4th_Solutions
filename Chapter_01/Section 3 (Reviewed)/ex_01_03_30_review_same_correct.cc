#include <iostream>
#include <string>
using namespace std;

template <typename T>
class LinkedList {
public:
    struct Node {
        T item;
        Node* next;
    };
    int size() { return N; }
    bool isEmpty() { return N == 0; }
    Node* getHead() { return head; }
    LinkedList() {
        N = 0;
        head = NULL;
        tail = NULL;
    }
    void addNode(T item) {
        Node* temp = new Node;
        temp->item = item; 
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }
private:
    Node* head;
    Node* tail;
    int N;
};

using LinkedListNode = LinkedList<string>::Node;

void printList(LinkedListNode* first) {
    LinkedListNode* traverse = first;
    while (traverse != NULL) {
        cout << traverse->item << endl;
        traverse = traverse->next;
    }
}
    

LinkedListNode* reverse (LinkedListNode* x) {
    LinkedListNode* first = x;
    LinkedListNode* reverse = NULL;
    while (first != NULL) {
        LinkedListNode* second = first->next;
        first->next = reverse;
        reverse = first;
        first = second;             // marking that first has reached the final node of the list (which points to NULL)
    }
    return reverse;
}

LinkedListNode* reverser(LinkedListNode* first) {
    if (first == NULL) return NULL;             // for the second->next = first command, so that the recursive could end
    if (first->next == NULL) return first;      
    LinkedListNode* second = first->next;
    LinkedListNode* rest = reverse(second);
    second->next = first;
    first.next = NULL;
    return rest;
}

int main() {
    LinkedList<string> stringlist;
    stringlist.addNode("phong");
    stringlist.addNode("cuong");
    stringlist.addNode("thinh");
    stringlist.addNode("khanh");
    LinkedListNode* first = stringlist.getHead();
    printList(reverse(first));
}
