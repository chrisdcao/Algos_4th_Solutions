#include <iostream>
#include <memory>
#include <vector>
#include <string>
using namespace std;

template<typename T>
class DoublyLinkedList {
private:
    class DoubleNode {
    public:
        T item;
        DoubleNode* next;
        DoubleNode* prev;
        //int index = -1;
    };

    //DoubleNode* newDoubleNode() {
        //allNodes.push_back(make_unique<DoubleNode>());
        //allNodes.back()->index = allNodes.size() - 1;
        //return allNodes.back().get();
    //}

    //void freeDoubleNode(DoubleNode* doublenode) {
        //if (doublenode->index < allNodes.size() - 1) {
            //allNodes[doublenode->index].swap(allNodes.back());
            //allNodes[doublenode->index]->index = doublenode->index;
        //}
        //allNodes.pop_back();
    //}

    //vector<unique_ptr<DoubleNode>> allNodes;

    DoubleNode* head;
    DoubleNode* tail;
    int N;

public:

    DoublyLinkedList() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void addDoubleNode(T item) {
        DoubleNode* temp = new DoubleNode();
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            temp->prev = NULL;
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            temp->prev = tail;
            tail = temp;
        }
        ++N;
    }

    void insertAtBeginning(T item) {
        DoubleNode* temp = new DoubleNode();
        temp->item = item;
        temp->next = head;
        head->prev = temp;
        temp->prev = NULL;
        head = temp;
        ++N;
        return;
    }

    void insertAtEnd(T item) {
        DoubleNode* temp = new DoubleNode();
        temp->item = item;
        tail->next = temp;    
        temp->prev = tail;
        temp->next = NULL;
        tail = temp;
        ++N;
        return;
    }

    void insertBeforeDoubleNode(T beforeItem, T item) {
        if (isEmpty()) return;              // no node to insert before it
        DoubleNode* traverse = head;
        while (traverse != NULL) {
            if (traverse->item == beforeItem) {
                if (traverse == head) insertAtBeginning(item);
                else {
                    DoubleNode* temp = new DoubleNode();
                    temp->item = item;
                    temp->next = traverse;
                    temp->prev = traverse->prev;
                    traverse->prev->next = temp;
                    traverse->prev = temp;
                    ++N;
                }
            }
            traverse = traverse->next;
        }
    }

    void insertAfterDoubleNode(T afterItem, T item) {
        if (isEmpty()) return;              // no node to insert after it
        DoubleNode* traverse = head;
        while (traverse != NULL) {
            if (traverse->item == afterItem) {
                if (traverse == tail) insertAtEnd(item);
                else {
                    DoubleNode* temp = new DoubleNode();
                    temp->item = item;
                    temp->prev = traverse;
                    temp->next = traverse->next;
                    traverse->next->prev = temp;
                    traverse->next = temp; 
                    ++N;
                }
            }
            traverse = traverse->next;
        }
    }

    T removeFromBeginning() {
        DoubleNode* oldHead = head;
        T item = oldHead->item;
        head = head->next;
        head->prev = NULL;
        //freeDoubleNode(oldHead);
        delete oldHead;
        --N;
        return item;
    }

    T removeFromEnd() {
        DoubleNode* oldTail = tail;
        T item = oldTail->item;
        tail = tail->prev;
        tail->next = NULL;
        //freeDoubleNode(oldTail);
        delete oldTail;
        --N;
        return item;
    }

    T removeDoubleNodeAt(int index) {
        if (isEmpty()) throw out_of_range("There's no element in the List!");
        else if (index > size() - 1) throw out_of_range("Underflow");
        else if (index == 0) { removeFromBeginning(); }
        else if (index == size() - 1) { removeFromEnd(); }
        else {
            DoubleNode* traverse = head;
            for (int i = 0; i < index - 1; i++)
                traverse = traverse->next;
            DoubleNode* dp = traverse->next;
            T item = traverse->next->item;
            traverse->next->next->prev = traverse;
            traverse->next = traverse->next->next;
            //freeDoubleNode(dp);
            delete dp;
            --N;
            return item;
        }
    }

    void printList() {
        DoubleNode* traverse = head;
        while(traverse != NULL) {
            cout << traverse->item << endl;
            traverse = traverse->next;
        }
        cout << endl;
    }

    void printListR() {
        DoubleNode* traverse = tail;
        while(traverse != NULL) {
            cout << traverse->item << endl;
            traverse = traverse->prev;
        }
        cout << endl;
    }
};

int main() {
    DoublyLinkedList<string> stringList;
    stringList.addDoubleNode("phong");
    stringList.addDoubleNode("khanh");
    stringList.addDoubleNode("cuong");
    stringList.addDoubleNode("thinh");
    stringList.insertAtBeginning("khoa");
    stringList.insertAtEnd("hung");
    stringList.removeDoubleNodeAt(2);
    stringList.printList();
    stringList.removeDoubleNodeAt(0);
    stringList.printList();
    stringList.removeDoubleNodeAt(3);
    stringList.printList();
    stringList.insertBeforeDoubleNode("khanh", "chris");
    stringList.insertAfterDoubleNode("cuong", "ryan");
    stringList.printList();
    stringList.removeFromBeginning();
    stringList.printListR();
    stringList.removeFromEnd();
    stringList.printList();
}
