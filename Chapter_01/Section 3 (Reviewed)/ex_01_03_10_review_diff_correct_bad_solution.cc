#include <iostream>
#include <string>
#include <sstream>
using namespace std;

template <typename T>
class Stack {
private:
    struct Node {
        T item;
        Node* next;
    };

    Node * head;
    Node* tail;
    int N;
    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* InitLoc) {
            current = InitLoc;
        }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return this->current->item;
        }

    };

public:
    Stack() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void push(T item) {
        Node* temp = new Node;
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->next = head;
            head = temp;
        }
        N++;
    }
    
    T pop() {
        T item = head->item;
        if (N != 0) {
            Node* dp = head;
            head = head->next;
            delete dp;
            N--;
        } else 
            throw out_of_range("Underflow");
        return item;
    }

    Iterator begin() { return Iterator(head); }
    Iterator end() { return Iterator(tail->next); }
};

int main() {
    Stack<string> operands;
    Stack<string> operatorInferior;
    Stack<string> operatorSuperior;
    string s;
    while (cin >> s) {
        if (s == "(");
        else if (s == "+" || s == "-") operatorInferior.push(s);
        else if (s == "*" || s == "/") operatorSuperior.push(s);
        else if (s == ")") {
            while (!operatorSuperior.isEmpty()) {
                string op = operatorSuperior.pop();
                string value2 = operands.pop();
                string value1 = operands.pop();
                string input = value1 + " " + value2 + " " + op + " ";
                operands.push(input);
            }
            while (!operatorInferior.isEmpty()) {
                string op = operatorInferior.pop();
                string value2 = operands.pop();
                string value1 = operands.pop();
                string input = value1 + " " + value2 + " " + op + " ";
                operands.push(input);
            }
        } 
        else
            operands.push(s);
    }

    while (!operatorSuperior.isEmpty() && operands.size() > 1) {
        string op = operatorSuperior.pop();
        string value2 = operands.pop();
        string value1 = operands.pop();
        string input = value1 + " " + value2 + " " + op + " ";
        operands.push(input);
    }
    while (!operatorInferior.isEmpty() && operands.size() > 1) {
        string op = operatorInferior.pop();
        string value2 = operands.pop();
        string value1 = operands.pop();
        string input = value1 + " " + value2 + " " + op + " ";
        operands.push(input);
    }

    for (auto s : operands)
        cout << s;
    cout << endl;
}
