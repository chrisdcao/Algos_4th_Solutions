#include <iostream>
#include "Deque.h"
using namespace std;

template <typename T>
class TwoStacks {
private:
    Deque<T> deque;
    int Nleft;
    int Nright;

public:

    TwoStacks() {
        Nleft = 0;
        Nright = 0;
    }

    int sizeLeft() { return Nleft; }
    int sizeRight() { return Nright; }

    bool StackLeftisEmpty() { return Nleft == 0; }
    bool StackRightisEmpty() { return Nright == 0; }

    void pushStackLeft(T item) {
        deque.pushLeft(item);
        Nleft++;
    }

    void pushStackRight(T item) {
        deque.pushRight(item);
        Nright++;
    }

    T popStackLeft() {
        Nleft--;
        return deque.popLeft();
    }

    T popStackRight() {
        Nright--;
        return deque.popRight();
    }

    void printLeftStack() {
        auto traverse = deque.getHead();
        for (int i = 0; i < Nleft; i++) {
            cout << traverse->item << " ";
            traverse = traverse->next;
        }
        cout << endl;
    }

    void printRightStack() {
        auto traverse = deque.getTail();
        for (int i = 0; i < Nright; i++) {
            cout << traverse->item << " ";
            traverse = traverse->prev;
        }
        cout << endl;
    }

    void printBothList() {
        deque.printList();
    }

};

int main() {
    TwoStacks<string> string2Stacks;

    string2Stacks.pushStackLeft("thinh");
    string2Stacks.pushStackLeft("phong");
    string2Stacks.pushStackLeft("cuong");

    string2Stacks.pushStackRight("khanh");
    string2Stacks.pushStackRight("khoa");
    string2Stacks.pushStackRight("long");

    string2Stacks.printRightStack();
    cout << endl;
    string2Stacks.printLeftStack();
    cout << endl;
    cout << string2Stacks.popStackLeft() << endl;
    cout << endl;
    cout << string2Stacks.popStackRight() << endl;
}
