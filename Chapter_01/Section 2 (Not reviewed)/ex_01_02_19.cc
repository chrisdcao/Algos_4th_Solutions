#include <iostream>
#include <string>
#include <sstream>
#include <ctime>
using namespace std;

class Date {
public:
    Date() = default;
    Date(int m, int d, int y) {
        if (m > 12 || m < 1) throw runtime_error("In-appropriate month input!");
        else month = m;

        if (d > 31 || d < 1) throw runtime_error("In-appropriate day input!");
        else day = d;

        if (y < 0) throw runtime_error("In-appropriate year input!");
        else year = y;
    }
    Date(string date) {
        int searchIndex, searchIndex2, searchIndex3;
        if (date.find("/") != string::npos) {
            searchIndex = date.find_first_of("/");
            stringstream ssmonth(date.substr(0, searchIndex));
            ssmonth >> month;
            searchIndex2 = date.find_first_of("/", searchIndex + 1);
            stringstream ssday(date.substr(searchIndex + 1, searchIndex2 + 1));
            ssday >> day;
            stringstream ssyear(date.substr(searchIndex2 + 1, date.size()));
            ssyear >> year;
        } else if (date.find("-") != string::npos) {
            searchIndex = date.find_first_of("-");
            stringstream ssmonth(date.substr(0, searchIndex));
            ssmonth >> month;
            searchIndex2 = date.find_first_of("-", searchIndex + 1);
            stringstream ssday(date.substr(searchIndex + 1, searchIndex2 + 1));
            ssday >> day;
            stringstream ssyear(date.substr(searchIndex2 + 1, date.size()));
            ssyear >> year;
        } else if (date.find(".") != string::npos) {
            searchIndex = date.find_first_of(".");
            stringstream ssmonth(date.substr(0, searchIndex));
            ssmonth >> month;
            searchIndex2 = date.find_first_of(".", searchIndex + 1);
            stringstream ssday(date.substr(searchIndex + 1, searchIndex2 + 1));
            ssday >> day;
            stringstream ssyear(date.substr(searchIndex2 + 1, date.size()));
            ssyear >> year;
        } else {
            searchIndex = date.find_first_of(" ");
            stringstream ssmonth(date.substr(0, searchIndex));
            ssmonth >> month;
            searchIndex2 = date.find_first_of(" ", searchIndex + 1);
            stringstream ssday(date.substr(searchIndex + 1, searchIndex2 + 1));
            ssday >> day;
            stringstream ssyear(date.substr(searchIndex2 + 1, date.size()));
            ssyear >> year;
        }
    }
    ~Date() {}

    int getMonth() { return month; }
    int getDay() { return day; }
    int getYear() { return year; }

    string toString() {
        return to_string(month) + "/" + to_string(day) + "/" + to_string(year);
    }
    string dayOfTheWeek() {
        std::tm t = {0, 0, 0, day, month - 1, year - 1900};
        mktime(&t);
        if (t.tm_wday == 0) return "Sunday";
        else if (t.tm_wday == 1) return "Monday";
        else if (t.tm_wday == 2) return "Tuesday";
        else if (t.tm_wday == 3) return "Wednesday";
        else if (t.tm_wday == 4) return "Thursday";
        else if (t.tm_wday == 5) return "Friday";
        else return "Saturday";
    }

private:
    int month;
    int day;
    int year;
};

int main () {
    string answer;
    do {
        string s;
        cout << "Please input a date: " << endl;
        cin >> s;
        Date sample(s);
        cout << "Month is: " << sample.getMonth() << ". Day is: " << sample.getDay() << ". Year is: " << sample.getYear() << endl; 
        cout << "Do you wanna continue? (Y/N): ";
        cin >> answer;
    } while (answer == "Y" || answer == "y");
    return 0;
}
