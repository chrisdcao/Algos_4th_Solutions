#include <iostream>
#include <string>
#include <ctime>
using namespace std;

class Date {
public:
    Date() = default;
    Date(int m, int d, int y) {
        if (m > 12 || m < 1) throw runtime_error("In-appropriate month input!");
        else month = m;

        if (d > 31 || d < 1) throw runtime_error("In-appropriate day input!");
        else day = d;

        if (y < 0) throw runtime_error("In-appropriate year input!");
        else year = y;
    }
    ~Date() {}

    int getMonth() { return month; }
    int getDay() { return day; }
    int getYear() { return year; }

    string toString() {
        return to_string(month) + "/" + to_string(day) + "/" + to_string(year);
    }
    string dayOfTheWeek() {
        // EXPLANATION:
        // month - 1, because C++ marks January start from 0, so 12 months will only reach 11 in the index (unlike human counting numbers which starts from 1, so we decrement 1 here)
        // year - 1900, because C++ mark 1900 as index[0] of its counting, so relatively, any year later than 1900 will have to minus 1900 to get the right index of itself in the C++ library
        // day is actually different from human count as well, but no matter how we change it's still different, so we just leave it and set the 'if-else' to print the correct 'human' day of the week
        std::tm t = {0, 0, 0, day, month - 1, year - 1900};
        mktime(&t);
        if (t.tm_wday == 0) return "Sunday";
        else if (t.tm_wday == 1) return "Monday";
        else if (t.tm_wday == 2) return "Tuesday";
        else if (t.tm_wday == 3) return "Wednesday";
        else if (t.tm_wday == 4) return "Thursday";
        else if (t.tm_wday == 5) return "Friday";
        else return "Saturday";
    }

private:
    int month;
    int day;
    int year;
};

int main () {
    string answer;
    do {
        int month, day, year;
        cout << "Please input mm/dd/yyyy: ";
        cin >> month >> day >> year;
        Date date(month, day, year);
        cout << date.getMonth() << date.getDay() << date.getYear() << endl;
        cout << date.toString() << endl;
        cout << date.dayOfTheWeek() << endl;
        cout << "Do you wanna continue? (Y/N): ";
        cin >> answer;
    } while (answer == "Y" || answer == "y");
    return 0;
}
