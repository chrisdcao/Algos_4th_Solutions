import java.util.*;
import java.lang.*;

// client
public class ex_01_02_01 {
    public static void main(String[] args) {
        // Prompt the user
        System.out.print("Please input how many elements you want: ");
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        // Initializing random points within the unit square
        double[] xArr = new double[n];
        double[] yArr = new double[n];
        double x, y;
        for (int i = 0; i < n; i++) {
            x = Math.random();
            y = Math.random();
            xArr[i] = x;
            yArr[i] = y;
        }
        // Initializing 'unique' distance/vector length from the points
        double xVec, yVec;
        int sum = 0, k = 0;
        for (int a = 1; a < n; a++)
            sum += n - a;
        double[] distPow2 = new double[sum];
        for (int j = 0; j < n - 1; j++) {
            for (int i = j + 1; i < n; i++) {
                xVec = xArr[j] - xArr[i];
                yVec = yArr[j] - yArr[i];
                distPow2[k] = Math.pow(xVec, 2) + Math.pow(yVec,2);
                ++k;
            }
        }
        // Extract the min value of distance from the array
        for (int i = 1; i < sum; i++) {
            if (distPow2[0] > distPow2[i])
                distPow2[0] = distPow2[i];
        }
        // Print the result
        System.out.println("The minimum distance of 2 points is: " + Math.sqrt(distPow2[0]));
    }
}
