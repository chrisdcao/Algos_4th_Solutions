#include <iostream>
#include <string>
#include <math.h>
using namespace std;

class VisualCounter {
public:
    VisualCounter() {
        cout << "Count starts at 0. Default threshold of 4 Operations and Values (4) are set!" << endl;
        count = 0;
        max = 4;
        N = 4;
        operation = 0;
    }
    VisualCounter(int c) {
        cout << "Count starts at " << c << ". Default threshold of 4 Operations and Values (4) are set!" << endl;
        count = c;
        max = 4;
        N = 4;
        operation = 0;
    }
    VisualCounter(int n, int m) {
        cout << "Max operation is set to (" << n << ") and max value is set to (" << m << "). Count defaultly starts at (0)." << endl;
        count = 0;
        max = 4;
        N = 4;
        operation = 0;
    }
    VisualCounter(int mn, string ind) {
        if (ind == "operation") {
            cout << "Only define the threshold of operation, count and value defaultly set to 0 & 4!" << endl;
            count = 0; 
            N = mn;
            max = 4;
            operation = 0;
        } else if (ind == "value") {
            cout << "Only define the threshold of value, count and operation defaultly set to 0 & 4!" << endl;
            count = 0;
            N = 4;
            max = mn;
            operation = 0;
        } else if (ind == "count") {
            cout << "Count is initialized to " << mn << ". Two thresholds are defaultly set at 4!" << endl;
            count = mn;
            N = 4;
            max = 4;
            operation = 0;
        } else
            throw runtime_error("In-appropriate argument!");
    }
    VisualCounter(int c, int n, int m) {
        cout << "Count starts at " << c << ". Thresholds of operations and value are: " << n << " & " << m << endl;
        count = c;
        N = n;
        max = m;
        operation = 0;
    }

    int maxOperation() { return N; }
    int maxValue() { return max; }
    int currentValue() { return count; }
    int currentOperation() { return count; }
    void increment() { 
        if (count >= maxValue()) 
            throw out_of_range("You have reached the Value threshold!");
        if (operation >= maxOperation())
            throw out_of_range("You have reached the Operation threshold!");
        ++count; 
        ++operation;
    }
    void decrement() {
        if (abs(count) >= maxValue()) 
            throw out_of_range("You have reached the Value threshold!");
        if (abs(operation) >= maxOperation())
            throw out_of_range("You have reached the Operation threshold!");
        --count; 
        ++operation;
    }

private:
    int operation;
    signed int count;
    int N, max;
    string indicator;
};

int main() {
    int count, n, max;
    string answer;
    cout << "(Optional) Please input the starting count (Y/N)";
    cin >> answer;
    if (answer == "Y" || answer == "y") {
        cout << "Please state the value: ";
        cin >> count;
    } 
    cout << "Please input the max value and max operation you want (required): "; 
    cin >> n >> max;
    VisualCounter countObj(n, max);
    countObj.increment();
    countObj.increment();
    countObj.increment();
    countObj.increment();
    countObj.increment();
    countObj.increment();
    cout << countObj.currentValue() << " " << countObj.currentOperation() << endl;
    return 0;
}
