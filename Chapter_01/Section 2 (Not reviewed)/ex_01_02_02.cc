#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <array>
using namespace std;

int main() {
    int N;
    cout << "Please input the number of intervals you want: ";
    cin >> N;
    vector<array<double, 2>> vecIntervals(N);
    // Initialize the Interval container
    for (int i = 0; i < N; i++) {
        // get random every runtime
        srand(time(0)+i);
        vecIntervals[i][0] = rand() * 0.0001; 
        srand(time(0)+i+N);
        vecIntervals[i][1] = rand() * 0.0001; 
    }
    for (int j = 0; j < N - 1; j++) {
        for (int i = j + 1; i < N; i++) {
            if (((vecIntervals[i][0] < vecIntervals[j][1]) && (vecIntervals[i][0] > vecIntervals[j][0])) || ((vecIntervals[i][1] < vecIntervals[j][1]) && (vecIntervals[i][1] > vecIntervals[j][0]))) {
                cout << "(" << vecIntervals[j][0] << ", " << vecIntervals[j][1] << ") & (" << vecIntervals[i][0] << ", " << vecIntervals[i][1] << ") intersects!" << endl;
            }
        }
    }
    return 0;
}
