#include <iostream>
#include <string>
using namespace std;

int main() {
    string string1 = "hello";
    string string2 = string1;
    string1 = "world";
    cout << string1 << endl;
    cout << string2 << endl;
}
// PRINTS OUT 'WORLD' AND 'HELLO'
