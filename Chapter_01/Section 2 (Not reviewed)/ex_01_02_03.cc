#include <iostream>
#include <random>
#include <cstdlib>
#include <vector>
#include <array>
#include <time.h>
#include <math.h>
using namespace std;

double dblrand(double min, double max) {
    double range = max - min;
    double div = RAND_MAX / range;
    return min + (rand() / div);
}

int main() {
    int N;                                  // Number of intervals
    double min, max, width, height,         // de lay so nhu de bai cho
           x0, y0, x1, y1,                  // co-ordinates of the intervals
           aOrigin, bOrigin, aComp, bComp,  // he so goc va hang so (ptrinh do thi)
           xSol, ySol;                      // nghiem cua he 2 ptrinh bac 1 (or matrix in other word)
    cout << "Please input the number of intervals: ";
    cin >> N;
    cout << "Please input the threshold (both < 1): Minimum then Maximum: ";
    cin >> min >> max;
    uniform_real_distribution<double> dis(min, max);
    default_random_engine generator;
    vector<array<double,2>> xCoord(N);
    vector<array<double,2>> yCoord(N);
    for (int i = 0; i < N; i++) {
        srand(time(0) + i);
        width = dis(generator);
        srand(time(0) + i + N);
        height = dis(generator);
        x0 = dblrand(0.00, 1 - width);
        x1 = x0 + width;
        xCoord[i][0] = x0;
        xCoord[i][1] = x1;
        y0 = dblrand(0.00, 1 - height);
        y1 = y0 + height;
        yCoord[i][0] = y0;
        yCoord[i][1] = y1;
    }
    // from the information in the 2 vector (in java would be array), we could use stdDraw to draw the graph
    // In C++ we will not draw but only use the data to compute
    for (int j = 0; j < N - 1; j++) {
        for (int i = j + 1; i < N; i++) {
            aOrigin = (yCoord[j][0] - yCoord[j][1]) / (xCoord[j][0] - xCoord[j][1]);
            bOrigin = yCoord[j][1] - xCoord[j][1] * aOrigin;
            aComp = (yCoord[i][0] - yCoord[i][1]) / (xCoord[i][0] - xCoord[i][1]);
            bComp = yCoord[i][1] - xCoord[i][1] * aComp;
            if (aComp != aOrigin) {
                xSol = (bOrigin - bComp) / (aComp - aOrigin);
                ySol = (bOrigin * aComp - bComp * aOrigin) / (aComp - aOrigin);
                if (((xSol == xCoord[j][0]) && (ySol == yCoord[j][0])) || ((xSol == xCoord[j][1]) && (ySol == yCoord[j][1])))
                    cout << "\tInterval " << j << " & " << "interval " << i << " intersects at the rear of Interval " 
                         << j << endl;
                else {
                    if ((xSol - xCoord[j][0]) * (xSol - xCoord[j][1]) < 0 && (ySol - yCoord[j][0]) * (ySol - yCoord[j][1]) < 0)
                        cout << "\tInterval " << j << " & " << "interval " << i << " intersects at (x , y) = (" 
                             << xSol << " , " << ySol << ")!" << endl;
                }
            } else {
                if(bOrigin == bComp && (((xCoord[i][0] - xCoord[j][0]) * (xCoord[i][0] - xCoord[j][1]) < 0) || ((xCoord[i][0] - xCoord[j][0]) * (xCoord[i][0] - xCoord[j][1]) < 0)))
                    cout << "\tInterval " << j << " & " << "interval " << i << " joins one another!" << endl;
            }
        }
    }
    return 0;
}

