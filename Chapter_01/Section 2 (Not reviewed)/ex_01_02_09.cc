#include <iostream>
using namespace std;

int counter,
    arrSize;

int BinarySearch(int, int*, int, int);

int main () {
    int a[] = {1, 2, 2, 5, 6, 6, 8, 9, 12};
    int lo = 0;
    int hi = sizeof(a) / sizeof(a[0]);
    arrSize = sizeof(a) / sizeof(a[0]);
    cout << "The key (5) at index [" <<  BinarySearch(5, a, lo, hi) << "] gone through " << counter << " keys!" <<  endl;
    cout << "The key (1) at index [" <<  BinarySearch(1, a, lo, hi) << "] gone through " << counter << " keys!" <<  endl;
    cout << "The key (12) at index [" <<  BinarySearch(12, a, lo, hi) << "] gone through " << counter << " keys!" <<  endl;
    cout << "The key (17) at index [" <<  BinarySearch(17, a, lo, hi) << "] gone through " << counter << " keys!" <<  endl;
    return 0;
}

int BinarySearch(int key, int a[], int lo, int hi) {
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key > a[mid]) {
            lo = mid + 1;
            if (mid < arrSize - 1)
                ++counter;
        }
        else if (key < a[mid]) {
            hi = mid - 1;
            if (mid < arrSize - 1)
                ++counter;
        }
        else {
            if (counter != 0)
                --counter;
            return mid;
        }
    }
    --counter;
    return -1;
}
