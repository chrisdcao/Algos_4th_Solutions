#include <iostream>
#include <string>
#include <exception>
using namespace std;

class Date {
public:
    Date() = default;
    Date(int m, int d, int y) {
        if (m > 12 || m < 1) throw runtime_error("In-appropriate month input!");
        else month = m;

        if (d > 31 || d < 1) throw runtime_error("In-appropriate day input!");
        else day = d;

        if (y < 0) throw runtime_error("In-appropriate year input!");
        else year = y;
    }
    ~Date() {}

    int getMonth() { return month; }
    int getDay() { return day; }
    int getYear() { return year; }
    bool operator==(Date);
    bool operator!=(Date);

    string toString() {
        return to_string(month) + "/" + to_string(day) + "/" + to_string(year);
    }

private:
    int month;
    int day;
    int year;
};

bool Date::operator==(Date that) {
    return ((this->month == that.month) && (this->day == that.day) && (this->year == that.year));
}

bool Date::operator!=(Date that) {
    return !(*this == that);
}
