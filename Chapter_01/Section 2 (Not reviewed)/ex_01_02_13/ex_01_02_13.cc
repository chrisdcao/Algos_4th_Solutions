#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include "chrisdate.h"
using namespace std;

class Transaction {
public:
    Transaction(string ai, Date thoigian, double soluong) {
        who = ai;
        when = thoigian;
        amount = soluong;
    }
    Transaction(string transaction) {
        giaodich = transaction;
    }
    string getWho() {
        return who;
    }
    Date getWhen() {
        return when;
    }
    double getAmount() {
        return amount;
    }
    string toString() {
        return who + " (" + when.toString() + "): " + to_string(amount) + "$";
    }
    bool equals(Transaction that) {
        return ((this->getWho() == that.getWho()) && (this->getWhen() == that.getWhen()) && (this->getAmount() == that.getAmount()));
    }
    int compareTo(Transaction that) {
        if (this->getAmount() > that.getAmount()) return 1;
        else if (this->getAmount() < that.getAmount()) return -1;
        else return 0;
    }
    int hashCode() {
        int j = giaodich.find_first_of("#");
        int i = giaodich.find_first_of(" ", j + 1);
        string mathang = giaodich.substr(j + 1, i);
        stringstream convert(mathang);
        int x = 0;
        convert >> x;
        return x;
    }

private:
    string who;
    string giaodich;
    Date when;
    double amount;
};

int main () {
    Date thinhdate(12, 03, 2020);
    Date cuongdate(12, 03, 2020);
    Transaction transObj("thinh", thinhdate, 1298.99);
    Transaction transObj1("thinh", thinhdate, 1298.99);
    Transaction transObj2("cuong", cuongdate, 1722.23);
    Transaction hashtest("Transaction ID: #100112339");
    cout << hashtest.hashCode() << endl;
    cout << transObj.toString() << endl;
    cout << transObj.getWho() << " " << transObj.getAmount() << endl;
    cout << transObj.compareTo(transObj1) << endl;
    cout << transObj.equals(transObj2) << endl;
    return 0;
}
