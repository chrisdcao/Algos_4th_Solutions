#include <iostream>
#include <string>
#include <iterator>
using namespace std;

int main() {
    string a, b;
    cout << "Please input 2 strings: ";
    cin >> a >> b;
    if (a.size() != b.size()) {
        cout << "These are NOT circular shift of each other!" << endl;
        return -1;
    }
    int dist;
    if (a.size() % 2 != 0)
        dist = a.size() / 2 + 1;
    else
        dist = a.size() / 2;
    for (int i = 0; i <= a.size() - dist; i++) {
        string aSearch = a.substr(i, dist);
        int j = b.find_first_of(aSearch[0], 0);
        while (j <= b.size() - dist) {
            if (j != -1) {
                int searchPoint = j;
                if (aSearch == b.substr(j, dist)) {
                    if (i != 0) {
                        for (int k = 0; k < i; k++) 
                            a.push_back(a[k]);
                        a.erase(0, i);
                    }
                    if (j != 0) {
                        for (int m = 0; m < j; m++) 
                            b.push_back(b[m]);
                        b.erase(0, j);
                    }
                    if (a.compare(b) == 0) {
                        cout << "These 2 are circular shift of each other" << endl;
                        return 0;
                    } else {
                        cout << "These 2 are NOT circular shift of each other" << endl;
                        return -1;
                    }
                } else {
                    j = b.find_first_of(aSearch[0], searchPoint + 1);
                }
            } else 
                break;
        }
    }
    return 0;
}
