#include <iostream>
#include <string>
#include <math.h>
using namespace std;

class Accumulator {
public:
    Accumulator() = default;
    Accumulator(double M, double S, int n) {
        m = M;
        s = S;
        N = n;
    }
    void addDataValue(double x) {
        N++;
        s=  s + 1.0 * (N - 1) / N * (x - m) * (x - m);
        m = m + (x - m) / N;
    }
    double mean() { return m; }
    double var() { return s / (N - 1);}
    double stddev() { return sqrt(this->var()); }
private:
    double m, s;
    int N; 
};

int main() {
    Accumulator am;
    am.addDataValue(2.5);
    am.addDataValue(2.5);
    am.addDataValue(2.5);
    am.addDataValue(2.5);
    cout << am.mean() << " " << am.var() << " " << am.stddev() << endl;
    return 0;
}
