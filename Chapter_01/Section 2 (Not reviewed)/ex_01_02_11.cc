#include <iostream>
#include <string>
#include <exception>
using namespace std;

class Date {
public:
    Date() = default;
    Date(int m, int d, int y) {
        if (m > 12 || m < 1) throw runtime_error("In-appropriate month input!");
        else month = m;

        if (d > 31 || d < 1) throw runtime_error("In-appropriate day input!");
        else day = d;

        if (y < 0) throw runtime_error("In-appropriate year input!");
        else year = y;
    }
    ~Date() {}

    int getMonth() { return month; }
    int getDay() { return day; }
    int getYear() { return year; }

    string toString() {
        return to_string(month) + "/" + to_string(day) + "/" + to_string(year);
    }

private:
    int month;
    int day;
    int year;
};

int main () {
    string answer;
    do {
        int month, day, year;
        cout << "Please input mm/dd/yyyy: ";
        cin >> month >> day >> year;
        Date date(month, day, year);
        cout << date.getMonth() << date.getDay() << date.getYear() << endl;
        cout << date.toString() << endl;
        cout << "Do you wanna continue? (Y/N): ";
        cin >> answer;
    } while (answer == "Y" || answer == "y");
    return 0;
}
