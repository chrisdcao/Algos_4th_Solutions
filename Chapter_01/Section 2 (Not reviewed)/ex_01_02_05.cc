#include <iostream>
#include <string>
using namespace std;

int main() {
    string s = "Hello World!";
    for (auto& i : s) {     // affect on the true value, not just the copy of the object
        i = toupper(i);
    }
    string r = s.substr(6, 11);
    cout << r << endl;
}
