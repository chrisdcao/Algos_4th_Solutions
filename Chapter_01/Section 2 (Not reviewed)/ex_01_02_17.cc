#include <iostream>
#include <string>
#include <math.h>
#include <exception>
using namespace std;

#define MAX_INT 2147483647
#define MIN_INT -2147483648

class Rational {
public:
    Rational() = default;
    Rational(long int n, long int d) throw()  {
        if (n >= MAX_INT || d >= MAX_INT || n <= MIN_INT || d <= MIN_INT) {
            throw runtime_error("You have surpass the maximum value that C++ integer can take!");
        }
        if (d == 0) {
            throw runtime_error("Cannot divide by zero!");
        }
        numerator = n;
        denominator = d;
    }
    Rational operator+(Rational);
    Rational operator-(Rational);
    Rational operator*(Rational);
    Rational operator/(Rational);
    bool operator==(Rational);
    bool operator!=(Rational);
    string toString() {
        return to_string(numerator) + "/" + to_string(denominator);
    }
    ~Rational() {} 
private:
    long int numerator;
    long int denominator;
};

int main() {
    Rational Rational1(22147483647, 1);
    Rational Rational2(2, 4);
    Rational Rational3(5, 7);
    cout << (Rational1/Rational2).toString() << endl;
    cout << (Rational1-Rational3).toString() << endl;
    cout << (Rational2+Rational3).toString() << endl;
}

// implementation
int gcd(int a, int b) {
    if (b == 0) return a;
    int r = a % b;
    return gcd(b, r);
}

Rational Rational::operator+(Rational b) {
    Rational result;
    int divisor;
    result.numerator = this->numerator * b.denominator + b.numerator * this->denominator;
    result.denominator = this->denominator * b.denominator;
    if (result.numerator > result.denominator) 
        divisor = gcd(result.numerator, result.denominator);
    else 
        divisor = gcd(result.denominator, result.numerator);
    if (divisor != 1) {
        result.numerator /= abs(divisor);
        result.denominator /= abs(divisor);
    }
    return result;
}

Rational Rational::operator-(Rational b) {
    b.numerator = -b.numerator;
    return operator+(b);
}

Rational Rational::operator*(Rational b) {
    Rational result;
    int divisor;
    result.numerator = this->numerator * b.numerator;
    result.denominator = this->denominator * b.denominator;
    if (result.numerator > result.denominator) 
        divisor = gcd(result.numerator, result.denominator);
    else 
        divisor = gcd(result.denominator, result.numerator);
    if (divisor != 1) {
        result.numerator /= abs(divisor);
        result.denominator /= abs(divisor);
    }
    return result;
}

Rational Rational::operator/(Rational b) {
    int temp = b.numerator;
    b.numerator = b.denominator;
    b.denominator = temp;
    return operator*(b);
}

bool Rational::operator==(Rational b) {
    int gcdthis, gcdb;
    if (this->numerator > this->denominator) 
        gcdthis = gcd(this->numerator, this->denominator);
    else 
        gcdthis = gcd(this->denominator, this->numerator);
    if (b.numerator > b.denominator) 
        gcdb = gcd(b.numerator, b.denominator);
    else 
        gcdb = gcd(b.denominator, b.numerator);
    if (gcdthis != 1) {
        this->numerator /= gcdthis;
        this->denominator /= gcdthis;
    }
    if (gcdb != 1) {
        b.numerator /= gcdb;
        b.denominator /= gcdb;
    }
    return ((this->numerator == b.numerator) && (this->denominator == b.denominator));
}

bool Rational::operator!=(Rational b) {
    return !(operator==(b));
}
