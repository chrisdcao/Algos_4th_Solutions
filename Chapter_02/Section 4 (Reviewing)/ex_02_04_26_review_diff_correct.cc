#include <iostream>
#include <vector>
#include <numeric>
#include "Random.h"
#include "ContainerUtility.h"
#include "MaxPQ.h"

using namespace std;

/* MaxHeap: Parent > child */
template <typename T>
class MaxHeapWithoutExchange {
private:

    vector<int> pq;
    int N = 0;

    void sink(int k) {
        T item = pq[k];
        /* we must have this 'bool' variable 
         * because the exchange index of the inside loop and the outside loop is going to be different (k BEFORE and k AFTER being assigned to j)*/
        bool exchangedInside = false;
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j] < pq[j+1]) j++;
            if (item >= pq[j]) {
                /* we have to take 'item' out for comparison, not pq[k] 
                 * since we are not constantly update pq[k] (swap on each iteration) with the item we want to compare in this non-exchange implement */
                pq[k] = item;
                exchangedInside = true;
                break;
            }
            pq[k] = pq[j];
            k = j;
        }
        if (!exchangedInside) pq[k] = item;
    } 

    /* swim without exchanges */
    void swim(int k) {
        T item = pq[k];
        bool exchangedInside = false;
        while (k > 1) {
            /* we have to take 'item' out for comparison, not pq[k] 
             * since we are not constantly update pq[k] (swap on each iteration) with the item we want to compare in this non-exchange implement */
            if (item <= pq[k/2]) {
                pq[k] = item;
                exchangedInside = true;
                break;
            }
            pq[k] = pq[k/2];
            k = k/2;
        }
        if (!exchangedInside && k == 1) pq[1] = item;
    }

public:

    MaxHeapWithoutExchange() = default;

    MaxHeapWithoutExchange(int maxN): N(0), pq(maxN+1) {}

    virtual ~MaxHeapWithoutExchange() {}
    friend ostream& operator << (ostream& os, MaxHeapWithoutExchange<T> maxPQ) {
        while (!maxPQ.isEmpty())
            os << maxPQ.delMax() << " ";
        return os;
    }

    void printCheckRealPQ() {
        for (auto s : pq)
            cout << s << " ";
        cout << endl;
    }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert(T item) {
        pq[++N] = item;
        swim(N);
    }

    T getMax() { return pq[1]; }

    T delMax() {
        T max = pq[1];
        swap(pq[1], pq[N--]);
        pq[N+1] = NULL;
        sink(1);
        return max;
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]); 
    MaxHeapWithoutExchange<int> maxPQ(N);
    MaxPQ<int> maxPQ_test(N);

    for (int i = 0; i < N; i++) {
        maxPQ.insert(i);
        maxPQ_test.insert(i);
    }

    cout << setw(30) << "The Heap inside is (test): ";
    maxPQ_test.printCheckRealPQ();
    cout << setw(30) << "The Heap inside is (ours): ";
    maxPQ.printCheckRealPQ();
    cout << endl;

    cout << setw(30) << "Normal printing of heap(test): ";
    cout << maxPQ_test << endl;
    cout << setw(30) << "Normal printing of heap(ours): ";
    cout << maxPQ << endl;

    return 0;

}
