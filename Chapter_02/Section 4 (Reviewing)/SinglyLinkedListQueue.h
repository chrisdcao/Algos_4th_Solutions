#include <iostream>
#include <sstream>
#include <memory>
#include <algorithm>
#include <numeric>
#include <random>
#include <vector>

using namespace std;

template <typename T>
class SinglyLinkedList {

    template <typename H> friend class Merge;

private:   

    class Node {
    public:
        T item;
        Node* next = NULL;
        int index = -1;
    };

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size()-1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;

    Node* head;
    Node* tail;
    int N;

public:
    
    SinglyLinkedList(): head(NULL), tail(NULL), N(0) {}

    Node* getHead() { return head; }

    Node* getTail() { return tail; }

    friend ostream& operator << (ostream& os, SinglyLinkedList& list) {
        Node* traverse = list.head;
        while (traverse != NULL) {
            os << traverse->item << " ";
            traverse = traverse->next;
        }
        return os;
    }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insertAtBegin(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->next = head;
        head = temp;
    }

    /* in this Queue-like implementation of linked-list
     * insert acts like insertAtEnd */
    void insert(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->next = NULL;
        if (N == 0) 
            head = tail = temp;
        else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }

    T removeAtBegin() {
        if (N == 0) {
            cout << "Underflow! Returning default value of type" << endl;
            return T();
        } 
        T item = head->item;
        if (N == 1)
            head = NULL;
        else {
            Node* oldHead = head;
            head = head->next;
            freeNode(head);
        }
        N--;
        return item;
    }

    Node* reverseIteratively(Node* head) {
        if (N <= 1) return head;
        Node* current = head;
        Node* next = head->next;
        Node* prev = NULL;
        while (next != NULL) {
            current->next = prev;
            prev = current;
            current = next;
            next = next->next;
        }
        current->next = prev;
        return current;
    }

    Node* reverseRecursively(Node* head) {
        if (head == NULL) return NULL;
        if (head->next == NULL) return head;
        Node* second = head->next;
        Node* newHead = reverseRecursively(second);
        second->next = head;
        head->next = NULL;
        return newHead;
    }

    void reverse(bool isRecursive = true) {
        if (isRecursive) head = reverseRecursively(head);
        else            head = reverseIteratively(head);
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

};
