#include "Random.h"
#include "ContainerUtility.h"
#include "SinglyLinkedList.h"

using namespace std;

template <typename T>
class OrderedLinkedListPQ {
private:

    SinglyLinkedList<T> pq;
    int N = 0;
    int cap = 1;

public:

    OrderedLinkedListPQ(): N(0), cap(0) {}

    OrderedLinkedListPQ(int maxN): N(0), cap(maxN) {}

    virtual ~OrderedLinkedListPQ() { /* not implemented */ }

    friend ostream& operator << (ostream& os, OrderedLinkedListPQ<T>& ollpq) {
        while(!ollpq.isEmpty()) 
            os << ollpq.delMax() << " ";
        return os;
    }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert(T item) {
        auto traverse = pq.getHead();
        if (N == 0 || item >= traverse->item) {
            pq.insert(item);
            ++N;
            return;
        /* this one is correct */
        } else if (N == 1) {
            auto temp = pq.newNode();
            temp->item = item;
            temp->next = NULL;
            pq.head->next = temp;
            ++N;
            return;
        } 
        /* from this one onwards things aren't correct */
        int count = 0;
        auto temp = pq.newNode();
        temp->item = item;
        while (item < traverse->item && traverse->next != NULL) {
            traverse = traverse->next;
            ++count;
        }
        if (traverse->next == NULL && item < traverse->item ) {
            temp->next = NULL;
            traverse->next = temp;
            ++N;
            return;
        }
        traverse = pq.getHead();
        for (int i = 0; i < count-1; i++) 
            traverse = traverse->next; 
        temp->next = traverse->next;
        traverse->next = temp;
        ++N;
    }

    T getMax() { return pq.getHead(); }

    T delMax() { 
        T max = pq.removeAtBegin();
        N--;
        return max;
    }

};

int main(int argc, char** argv) {
    
    int N = stoi(argv[1]);
    OrderedLinkedListPQ<int> orderedLinkedListPQ;

    for (int i = 0; i < N; i++) {
        orderedLinkedListPQ.insert(randomUniformDistribution(0, N-1));
    }

    /* cout << orderedLinkedListPQ << endl; */
    cout << ((orderedLinkedListPQ.isEmpty()) ? "empty" : "not_empty") << endl;
    cout << orderedLinkedListPQ << endl; 

    return 0;

}
