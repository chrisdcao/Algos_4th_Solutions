/* Heap_size: 31
 * Heap_Type: Max Heap (parent > child)
 * kth largset positions with: k = 2, 3, 4 */

Using_deduction: we can prove the following statements:
1. At i-th level of a Heap-tree, the number of leaves (possible positions) at that level will be 2^i (assuming 1st-level index = 0)
2. In a Distinct MaxHeap, the level of kth-largest element will be: (int) log(k+1), since we step 2x from 0 (the largest - 1st largest) to reach k (k-th largest)

/* From (1) && (2), we can infer --> */ Possible positions for k-th largest: 
(a) index đầu tiên của level chứa k-th largest  ---->  (b) index cuối cùng của level chứa k-th largest

Formula_formation:
/* with 1st-largest index = 0 */
(a) index đầu tiên của level chứa k-th largest = index của phần tử đứng cuối level trước level chứa k-th cộng 1:
            = Tổng số phần tử đứng trước level chứa k-th + 1 = 2^0 + 2 ^1 +... + 2^((int)log(k+1) - 1) - 1 + 1

(b) index cuối của level chứa k-th largest = (a)index đầu của level chứa k-th + (1)số phần tử ở level k trừ 1

Calculation:
/* since the exercise count 1st-largest index = 1, we plus 1 to each result, and minus one from k-th */
2-th largest range (formula's 1st largest): 
range: (1+1 -> 2+1) = (2 -> 3)

3-th largest range (formula's 2nd largest):
range: (1+1 -> 2+1) = (2 -> 3)

4-th largest range (formula's 3rd largest):
range: (3+1 -> 6+1) = (4 -> 7)

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* SOLUTION CHECK */
Bổ sung: có thể có hoán vị các phần tử giữa level k và (level(k-1) || level(k+1)) mà vẫn giữ nguyên cấu trúc Heap. Bởi vậy, các vị trí có thể của k-th largest bao gồm: (toàn bộ phần tử thuộc level chứa k) + (các vị trí hoán vị) (phức tạp, không thể công thức hóa được)

RIGHT_SOLUTION:
Heap-of-size-31 positions

                        1
           2                         3
      4           5             6            7
  8     9     10      11    12     13    14    15
16 17 18 19  20 21  22 23  24 25  26 27 28 29 30 31

Kth largest item  Can appear                         Cannot appear
2                 2,3                                1,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31
3                 2,3,4,5,6,7                        1,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31
4                 2,3,4,5,6,7,8,9,10,11,12,13,14,15  1,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31

