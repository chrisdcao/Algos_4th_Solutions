#include <iostream>
#include <vector>
#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

/* Bottom-up heap construction method */
template <typename T>
class HeapConstruction {

private:
    /* MaxPQ: parent > child */
    static void sink(vector<T>& pq, int k, int N) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j-1] < pq[j]) j++;
            if (pq[k-1] >= pq[j-1]) break;
            swap(pq[j-1], pq[k-1]);
            k = j;
        }
    }

public:

    static void sort(vector<T>& a) {
        int N = a.size();
        for (int k = N/2; k >= 1; k--)
            sink(a, k, N);
    }

};

/* MaxPQ implemenation */
template <typename T>
class MaxPQ {

private:

    int N = 0;
    vector<T> pq;

public:

    MaxPQ(vector<T>& a): N(a.size()) {
        for (int i = 0; i < N; i++)
            pq.push_back(a[i]);
        HeapConstruction<T>::sort(pq);
    }

    friend ostream& operator << (ostream& os, MaxPQ<T> maxPQ) {
        for (auto s : maxPQ.pq)
            os << s << " ";
        return os;
    }

};

/* test-client */
int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;

    for (int i = 0; i < N; i++)
        vec.push_back(i);

    random_shuffle(vec.begin(), vec.end());

    cout << setw(35) << "initial array: ";
    cout << vec << endl;

    MaxPQ<int> HeapConstruction(vec);

    cout << setw(35) << "MaxHeap-structure Constructed Array: ";
    cout << HeapConstruction << endl;

    return 0;

}
