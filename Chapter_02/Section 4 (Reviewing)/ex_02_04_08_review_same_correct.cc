Cannot formularize, so we just look at the Heap and list:

Heap-of-size-31 positions

                        1
           2                         3
      4           5             6            7
  8     9     10      11    12     13    14    15
16 17 18 19  20 21  22 23  24 25  26 27 28 29 30 31


Kth smallest item   Can appear                                                               Cannot appear
2(29th largest)     16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31                          Remaining indexes 
3(28th largest)     8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31    Remaining indexes 
4(26th largest)     8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31    Remaining indexes 

