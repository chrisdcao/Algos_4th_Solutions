TAKE A LOOK AT PAGE 323 for suggestion on explaining this
/* RELEVANT PIECE OF CODE */
/* MaxHeap: parent > child */
void sink(int k) {
    while (2 * k <= N) {
        it j = 2 * k;
        if (j < N && pq[j] < pq[j+1]) j++;
        if (pq[k] >= pq[j]) break;
        swap(pq[k], pq[j]);
        k = j;
    }
}

void swim(int k) {
    while (k > 1 && pq[k/2] < pq[k]) {
        swap(pq[k/2], pq[k]);
        k = k/2;
    }
}

/* [> COMPARISION COUNT <]
 * The maximum number of compares used in:
 * 1. Sink: the maximum amount of compares is 2logN  in this stage (compare until 2*k reaches N)
 * 2. Swim: The maximum amount of compares is 2logN in this stage as well (compare until k reduces to 1)
 * 
 * -> thus, the maximum amount of compares is 4logN (<= 2N) with (all N >= 1)
 * 
 * [> EXCHANGES COUNT <]
 * 1. Sink: The maximum amount of exchange is logN (assuming that we have to traverse until 2*k without the break)
 * 2. Swim: The maximum amount of exchange is logN (same explanation)
 * 
 * -> thus, the maximum amount of exchange is 2logN (<= N) with (N >= 1) */
