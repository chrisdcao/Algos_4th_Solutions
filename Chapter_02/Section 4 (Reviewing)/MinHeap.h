#ifndef MinHEAP_H
#define MinHEAP_H

#include <iostream>
#include <vector>
#include <numeric>
#include <random>
#include <iomanip>
#include <algorithm>

using namespace std;

template <typename T>
class MinHeap {

    template <typename Q> friend class MedianFind;

private:

    int N = 0;

    vector<T> pq;

    void sink(int k) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j] > pq[j+1]) j++;
            if (pq[k] <= pq[j]) break;
            swap(pq[k], pq[j]);
            k = j;
        }
    }

    void swim(int k) {
        while (k > 1 && pq[k/2] > pq[k]) {
            swap(pq[k], pq[k/2]);
            k = k/2;
        }
    }

public:

    friend ostream& operator << (ostream& os, MinHeap<T> minPQ) {
        while(!minPQ.isEmpty()) 
            os << minPQ.delMin() << " ";
        return os;
    }

    void printHeap() {
        for (auto s : pq)
            cout << s << " ";
        cout << endl;
    }

    MinHeap() = default;

    MinHeap(int MinN): N(0), pq(MinN+1) {}

    int size() { return N; }

    bool isEmpty() { return N==0; }

    void insert(T item) {
        pq[++N] = item;
        swim(N);
    }

    T getMin() { return pq[1]; }

    T delMin() {
        T min = pq[1];
        swap(pq[1], pq[N--]);
        sink(1);
        return min;
    }

};

#endif
