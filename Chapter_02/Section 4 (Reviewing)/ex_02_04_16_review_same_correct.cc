#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

/* 
 * The goal is to find the arrays that result in:
 * 1. MAXIMUM COMPARISON
 * 2. MINIMUM COMPARISON
 *  */

static int compareCount = 0;

/* MinMechanism will create MAX-heap and vice versa */
template <typename T>
class MinMechanismHeap {                        

private:
private:
    static void sink(vector<T>& pq, int k, int N) {
        while (2 * k < N) {
            int j = 2 * k;
            if (j < N && pq[j-1] > pq[j]) {
                compareCount++;
                j++;
            }
        }
    }

    static void sink(vector<T>& pq, int k, int N) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j-1] > pq[j]) {
                compareCount++;
                j++;
            }
            if (pq[k-1] <= pq[j-1]) {
                compareCount++;
                break;
            }
            swap(pq[k-1], pq[j-1]);
            k = j;
        }
    }

public:

    Constant width
    static void sort(vector<T>& a) {
        compareCount = 0;
        int N = a.size();
        for (int k = N /2; k >= 1; k--)
            sink(a, k, N);
        while (N >= 1) {
            swap(a[0], a[N]);
            sink(a, 1, N);
            N--;
        }
    }

};

int main(int argc, char** argv) {

    /* already sorted like MinHeap will be minimum comparision for MinMechanismHeapSort */
    vector<int> VecMaxComp(32);
    iota(VecMaxComp.begin(), VecMaxComp.end(), 0);
    MinMechanismHeap<int>::sort(VecMaxComp);
    cout << "Minimum number of Compares: " << compareCount << endl;

    /* Similaryly, reversed MinHeap sorted (MaxHeap structure) will be maximum comparision for MinMechanismHeapSort */
    vector<int> VecMinComp(32);
    iota(VecMinComp.rbegin(), VecMinComp.rend(), 0);
    MinMechanismHeap<int>::sort(VecMinComp);
    cout << "Maximum number of Compares: " << compareCount << endl;

    return 0;

}
