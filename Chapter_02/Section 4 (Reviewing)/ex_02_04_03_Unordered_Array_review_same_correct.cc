#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

template <typename T>
class UnorderedArrayPQ {
private:

    T* pq;
    int N = 0;
    int cap = 0;

    /* this max is correct */
    int findMax(T* array, int arraySize) {          /* insertionSort-like implementation of find Maximum */
        if (arraySize == 0) {
            cout << "Underflow!";
            return -1;
        } 
        if (arraySize == 1) { return 0; }
        int iMax = 0;
        for (int i = 1; i < arraySize; i++) {
            if (array[i] > array[iMax]) 
                iMax = i;
        }
        return iMax;
    }

    void resize(int maxSize) {
        T* temp = new T[maxSize];
        for (int i = 0; i < N; i++) 
            temp[i] = pq[i];
        delete[] pq;
        pq = temp; 
    }

public:

    friend ostream& operator << (ostream& os, UnorderedArrayPQ<T> unorderedArrayPQ) {
        while (!unorderedArrayPQ.isEmpty())
            os << unorderedArrayPQ.delMax() << " ";
        return os;
    }

    UnorderedArrayPQ(): N(0), cap(1) { pq = new T[1]; }

    UnorderedArrayPQ(int maxN): N(0), cap(maxN) { pq = new T[maxN]; }

    virtual ~UnorderedArrayPQ() {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert(T item) { 
        if (N == cap) {
            cap *= 2;
            resize(cap);
        }
        pq[N++] = item; 
    }

    T getMax() { return pq[findMax(pq, N)]; }

    T delMax() {
        int i = findMax(pq, N+1);
        T max = pq[i];
        /* exchange with end */
        swap(pq[i], pq[N--]);
        pq[N+1] = NULL;
        return max;
    }

};

int main(int argc, char** argv) {
    
    int N = stoi(argv[1]);
    UnorderedArrayPQ<int> unorderedArrayPQ(N);

    for (int i = 0; i < N; i++) {
        unorderedArrayPQ.insert(randomUniformDistribution(0, N-1));
    }
    cout << unorderedArrayPQ << endl;
    
    return 0;

}
