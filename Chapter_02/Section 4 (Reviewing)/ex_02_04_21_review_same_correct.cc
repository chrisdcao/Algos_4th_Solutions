#include <iostream>
#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

/* For stack implementation, it's actually easier than PQ implementation since we don't have to preserve any special structure
 * We just have to input and output in the stack-like order and we have a stack */
template <typename T>
class StackPQ {
private:

    int N = 0;
    vector<int> pq;

public:

    friend ostream& operator << (ostream& os, StackPQ<T> stack) {
        while (!stack.isEmpty())
            os << stack.pop() << " ";
        return os;
    }

    StackPQ() = default;

    StackPQ (int maxN): N(0), pq(maxN+1) {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void push(T item) { pq[++N] = item; }

    T pop() { return pq[N--]; }

};

/* Similarly, we have a Queue */
template <typename T>
class QueuePQ {
private:

    int N = 0;
    int traverse = 0;
    vector<int> pq;

public:

    friend ostream& operator << (ostream& os, QueuePQ<T> queue) {
        while (!queue.isEmpty())
            os << queue.dequeue() << " ";
        return os;
    }

    QueuePQ() = default;

    QueuePQ (int maxN): N(0), pq(maxN+1), traverse(0) {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void enqueue(T item) { 
        if (N == pq.size()) {
            cout << "No more capacity";
            return;
        }
        pq[++N] = item; 
    }

    T dequeue() { 
        if (N == 0) {
            cout << "Underflow! No more item to traverse, returning default value of the type: ";
            return T();
        }
        N--;
        return pq[++traverse]; 
    }

};

/* for randomize queue (randomly popping an element out, instead of following FIFO structure), we just have to randomize the return in dequeue() function */
template <typename T>
class RandomizedQueuePQ {
private:

    int N = 0;
    int traverse = 0;
    vector<int> pq;

public:

    friend ostream& operator << (ostream& os, RandomizedQueuePQ<T> randomizedQueue) {
        while (!randomizedQueue.isEmpty())
            os << randomizedQueue.dequeue() << " ";
        return os;
    }

    RandomizedQueuePQ() = default;

    RandomizedQueuePQ (int maxN): N(0), pq(maxN+1), traverse(0) {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void enqueue(T item) { 
        if (N == pq.size()) {
            cout << "No more capacity";
            return;
        }
        pq[++N] = item; 
    }

    T dequeue() { 
        if (N == 0) {
            cout << "Underflow! No more item to traverse, returning default value of the type: ";
            return T();
        }
        int i = randomUniformDistribution(0, N);
        /* we just swap the random element with the last element */
        swap(pq[i], pq[N]);
        /* then we return the randomed element */
        return pq[N--]; 
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    StackPQ<int> stack(N);
    QueuePQ<int> queue(N);
    RandomizedQueuePQ<int> randomizedQueue(N);

    for (int i = 0; i < N; i++) {
        stack.push(i);
        queue.enqueue(i);
        randomizedQueue.enqueue(i);
    }

    cout << "Stack: " << endl;
    cout << stack << endl;

    cout << "Queue: " << endl;
    cout << queue << endl;

    cout << "Randomized Queue: " << endl;
    cout << randomizedQueue << endl;

    return 0;

}


