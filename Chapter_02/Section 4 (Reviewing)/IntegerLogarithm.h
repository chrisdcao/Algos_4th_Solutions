#include <iostream>

using namespace std;

int intLog(int N) {
    int i = 1;
    int count = 0;
    while (i <= N) {
        i *= 2;
        count++;
    }
    return count-1;
}
