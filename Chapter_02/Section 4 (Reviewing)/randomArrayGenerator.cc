#include <iostream>
#include <vector>
#include "Random.h"
#include "ContainerUtility.h"
#include "MinHeap.h"
#include "MaxHeap.h"

using namespace std;

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));
    cout << vec << endl;
    sort(vec.begin(), vec.end());
    cout << vec << endl;
    int mid = 0 + (vec.size() - 0) / 2;

    if (vec.size() % 2 != 0) 
        cout << "median is: " << vec[mid] << endl;
    else 
        cout << "medians are: " << vec[mid] << " " << vec[mid-1] << endl;

    return 0;
    
}
