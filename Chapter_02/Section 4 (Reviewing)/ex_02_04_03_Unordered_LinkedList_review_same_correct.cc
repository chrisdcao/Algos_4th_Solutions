#include "Random.h"
#include "ContainerUtility.h"
#include "SinglyLinkedList.h"

using namespace std;

template <typename T>
class UnorderedLinkedListPQ {
private:

    SinglyLinkedList<T> pq;
    int N = 0;
    int cap = 1;

public:

    friend ostream& operator << (ostream& os, UnorderedLinkedListPQ<T>& ullpq) {
        while(!ullpq.isEmpty()) 
            os << ullpq.delMax() << " ";
        return os;
    }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    UnorderedLinkedListPQ() = default;

    UnorderedLinkedListPQ(int maxN): N(0), cap(maxN) {}

    virtual ~UnorderedLinkedListPQ() { /* not implemented */ }

    void insert(T item) {
        pq.insert(item);
        N++;
    }

    T getMax() {
        auto traverse = pq.getHead();
        T currentMax = traverse->item;
        while (traverse != NULL) {
            if (traverse->item > currentMax)
                currentMax = traverse->item;
        }
        return currentMax;
    }

    T delMax() {
        if (N == 0) throw out_of_range("underflow");
        auto traverse = pq.getHead();
        T currentMax = traverse->item;
        auto maxIndex = traverse;
        /* first iteration to find the value && position of the maximum value */
        while(traverse != NULL) {
            if (traverse->item > currentMax) {
                /* store the currentMax address into maxIndex */
                maxIndex = traverse;
                currentMax = maxIndex->item;
            }
            traverse = traverse->next;
        }
        /* if the first item is max */
        if (maxIndex == pq.getHead()) {
            N--;
            return pq.removeAtBegin();
        }
        /* second iteration for unlink */
        traverse = pq.getHead();
        /* stop one item before the item we need to remove */
        while (traverse->next != maxIndex)
            traverse = traverse->next;
        traverse->next = traverse->next->next;
        N--;
        return currentMax;
    }

};

int main(int argc, char** argv) {
    
    int N = stoi(argv[1]);
    UnorderedLinkedListPQ<int> unorderedLinkedListPQ;

    for (int i = 0; i < N; i++) {
        unorderedLinkedListPQ.insert(randomUniformDistribution(0, N-1));
    }

    /* cout << unorderedLinkedListPQ.delMax() << endl; */
    cout << unorderedLinkedListPQ << endl;
    
    return 0;

}
