#include <iostream>
#include <vector>

using namespace std;

// minHeap: Parent < child
template <typename T>
class MinHeap {
private:

    void exch( vector<T>& vec, int index1, int index2 ) {
        T temp = vec[ index1 ];
        vec[ index1 ] = vec[ index2 ];
        vec[ index2 ] = temp;
    }

    vector<T> pq;
    int N;

    void sink( int k ) {
        while ( 2 * k <= N )  {
            int j = 2 * k;
            if ( j < N && pq[ j ] > pq[ j+1 ] ) j++;
            if ( pq[ k ] <= pq[ j ] ) break;
            exch( pq, j, k );
            k = j;
        }
    }

    void swim( int k ) {
        while ( k > 1 && pq[ k/2 ] > pq[ k ]) {
            exch( pq, k, k/2 );
            k = k/2;
        }
    }

public:

    friend ostream& operator << ( ostream& os, MinHeap<T> minPQ ) {
        while ( !minPQ.isEmpty() ) 
            os << minPQ.delMin() << " ";
        return os;
    }

    friend void printPQ( MinHeap<T> minPQ ) {
        while ( !minPQ.isEmpty() ) 
            cout << minPQ.delMin() << endl;
    }
    
    friend void printPQExtraGap( MinHeap<T> minPQ ) {
        while ( !minPQ.isEmpty() ) 
            cout << minPQ.delMin() << endl << endl;
    }

    virtual ~MinHeap() {}

    MinHeap(): N( 0 ), pq( 1 ) {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert( T item ) {
        pq.push_back( item );
        swim( ++N );
    }

    T getMin() { return pq[ 1 ]; }

    T delMin() {
        T min = pq[ 1 ];
        exch( pq, 1, N-- );
        sink( 1 );
        return min;
    }
    
};
