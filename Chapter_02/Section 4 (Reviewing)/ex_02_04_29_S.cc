#include <iostream>
#include <string>
#include <iomanip>
#include <vector>
#include "ContainerUtility.h"

using namespace std;

template <typename T> class MinMaxPQ {

private:

    enum Orientation { MAX, MIN };

    class PQNode {
    public:
        T key;
        int minHeapIndex = 0;
        int maxHeapIndex = 0;
        PQNode() = default;
        PQNode( T key ): key( key ) {}
    };

    vector< PQNode > minPQ;
    vector< PQNode > maxPQ;
    int N = 0;

    bool less( vector< PQNode >& pq, int i, int j ) {
        return pq[ i ].key < pq[ j ].key;
    }

    bool greater( vector< PQNode >& pq, int i, int j ) {
        return pq[ i ].key > pq[ j ].key;
    }

    void exchange( vector< PQNode >& pq, int i, int j ) {
        T temp = pq[ i ].key;
        // int index1 = pq[ i ].minHeapIndex;
        // int index2 = pq[ i ].maxHeapIndex;
        pq[ i ].key = pq[ j ].key;
        // pq[ i ].minHeapIndex = pq[ j ].minHeapIndex;
        // pq[ i ].maxHeapIndex = pq[ j ].maxHeapIndex;
        pq[ j ].key = temp;
        // pq[ j ].minHeapIndex = index1;
        // pq[ j ].maxHeapIndex = index2;
    }

    void deleteItem( vector< PQNode >& pq, Orientation orientation, int index ) {
        exchange( pq, index, N+1 );
        pq.erase( pq.begin()+N+1 );

        if ( index == N + 1 ) return;

        sink( pq, index, orientation );
        swim( pq, index, orientation );
    }

    void swim( vector< PQNode >& pq, int k, Orientation orientation ) {
        while ( k > 1 ) {
            if ( ( orientation == MAX && less( pq, k/2, k ) ) 
              || ( orientation == MIN && greater( pq, k/2, k ) ) ) {
                exchange( pq, k/2, k );
                if ( orientation == MIN ) {
                    pq[ k ].minHeapIndex = k;
                    pq[ k/2 ].minHeapIndex = k / 2;
                } else {
                    pq[ k ].maxHeapIndex = k;
                    pq[ k/2 ].maxHeapIndex = k / 2;
                }
            } else break;
            k /= 2;
        }
        if ( orientation == MIN ) pq[ k ].minHeapIndex = k;
        else                      pq[ k ].maxHeapIndex = k;
    }

    void sink( vector< PQNode >& pq, int k, Orientation orientation ) {
        while ( 2 * k <= N ) {
            int j = 2 * k;
            if ( j < N && ( ( orientation == MAX && less( pq, j, j+1 ) ) || ( orientation == MIN && greater( pq, j, j+1 ) ) ) )
                j++;
            if ( ( orientation == MAX && greater( pq, j, k ) ) || ( orientation == MIN && less( pq, j, k ) ) ) {
                exchange( pq, k, j );
                if ( orientation == MIN ) {
                    pq[ k ].minHeapIndex = k;
                    pq[ j ].minHeapIndex = j;
                } else {
                    pq[ k ].maxHeapIndex = k;
                    pq[ j ].maxHeapIndex = j;
                }
            } else break;
            k = j;
        }

        if ( orientation == MIN ) pq[ k ].minHeapIndex = k;
        else                      pq[ k ].maxHeapIndex = k;
    }

public:

    MinMaxPQ(): minPQ( 1 ), maxPQ( 1 ), N( 0 ) {}

    bool isEmpty() { return size() == 0; }

    int size() { return N; }

    void insert( T key ) { 
        PQNode pqNode( key );
        N++;
        insertOnMinHeap( pqNode );
        insertOnMaxHeap( pqNode );
    }

    void insertOnMinHeap( PQNode pqNode ) {
        minPQ.push_back( pqNode );
        swim( minPQ, N, MIN );
    }

    void insertOnMaxHeap( PQNode pqNode ) {
        maxPQ.push_back( pqNode );
        swim( maxPQ, N, MAX );
    }

    T findMin() { 
        return ( isEmpty() ) ? throw runtime_error( "findMin(): Underflow" ) : minPQ[ 1 ].key; 
    }

    T findMax() { 
        return ( isEmpty() ) ? throw runtime_error( "findMax(): Underflow" ) : maxPQ[ 1 ].key; 
    }

    T deleteMax() { 
        if ( isEmpty() ) throw runtime_error( "deleteMax(): Underflow" );
        --N;
        T maxKey = maxPQ[ 1 ].key;
        int minHI = maxPQ[ 1 ].minHeapIndex;
        deleteItem( maxPQ, MAX, 1 );
        deleteItem( minPQ, MIN, minHI );
        return maxKey;
    }

    T deleteMin() {
        if ( isEmpty() ) throw runtime_error( "deleteMin(): Underflow" );
        --N;
        T minKey = minPQ[ 1 ].key;
        int maxHI = minPQ[ 1 ].maxHeapIndex;
        deleteItem( minPQ, MIN, 1 );
        deleteItem( maxPQ, MAX, maxHI );
        return minKey;
    }

};

int main( int argc, char** argv ) {
    MinMaxPQ< int > mmpq;
    int x; 

    mmpq.insert(10);
    mmpq.insert(2);
    mmpq.insert(40);
    mmpq.insert(1);


    cout << "delMax: " << mmpq.deleteMax() << " Expected: 40" << endl;
    cout << "delMin: " << mmpq.deleteMin() << " Expected: 1" << endl;
    cout << "delMax: " << mmpq.deleteMax() << " Expected: 40" << endl;
    cout << "delMin: " << mmpq.deleteMin() << " Expected: 1" << endl;
    cout << "delMax: " << mmpq.deleteMax() << " Expected: 40" << endl;
    cout << "delMin: " << mmpq.deleteMin() << " Expected: 1" << endl;

    cout << "findMax: " << mmpq.findMax() << " Expected: 10" << endl;
    cout << "findMin: " << mmpq.findMin() << " Expected: 2" << endl;

    mmpq.insert( 99 );
    mmpq.insert( -1 );

    cout << "findMax: " << mmpq.findMax() << " Expected: 99" << endl;
    cout << "findMin: " << mmpq.findMin() << " Expected: -1" << endl;

    cout << "delMax: " << mmpq.deleteMax() << " Expected: 99" << endl;
    cout << "delMin: " << mmpq.deleteMin() << " Expected: -1" << endl;

    return 0;

}
