/* Relevant code fragment: */

void sink(int k) {
    while (2 * k <= N) {
        int j = 2 * k;
        if (j < N && pq[j] < pq[j+1]) j++;
        if (pq[k] >= pq[j]) break;
        swap(pq[k], pq[j]);
        k = j;
    }
}

/* add extra condition using 2 * k instead of j */

void sink(int k) {
    while (2 * k <= N) {
        int j = 2 * k;
        if (2 * k < N)
            if (pq[j] < pq[j+1]) j++;
        if (pq[k] >= pq[j]) break;
        swap(pq[k], pq[j]);
        k = j;
    }
}
