#include <iostream>
#include <cmath>
#include <vector>
#include "Random.h"

using namespace std;

class ThreeDimensionPoint {
private:

    double x;
    double y;
    double z;

public:

    friend ostream& operator << (ostream& os, ThreeDimensionPoint threeDimensionPoint) {
        os << "x: " <<  threeDimensionPoint.getX() << " y: " << threeDimensionPoint.getY() << " z: " << threeDimensionPoint.getZ();
        return os;
    }

    friend istream& operator >> (istream& is, ThreeDimensionPoint& threeDimensionPoint) {
        is >> threeDimensionPoint.x >> threeDimensionPoint.y >> threeDimensionPoint.z;
        return is;
    }

    bool operator > (ThreeDimensionPoint& rhs) {
        return (pow(x,2) + pow(y,2) + pow(z,2)) > (pow(rhs.getX(),2) + pow(rhs.getY(),2) + pow(rhs.getZ(),2));
    }

    bool operator < (ThreeDimensionPoint& rhs) {
        return (pow(x,2) + pow(y,2) + pow(z,2)) < (pow(rhs.getX(),2) + pow(rhs.getY(),2) + pow(rhs.getZ(),2));
    }

    bool operator == (ThreeDimensionPoint& rhs) {
        return (pow(x,2) + pow(y,2) + pow(z,2)) == (pow(rhs.getX(),2) + pow(rhs.getY(),2) + pow(rhs.getZ(),2));
    }

    bool operator <= (ThreeDimensionPoint& rhs) {
        return operator==(rhs) || operator<(rhs);
    }

    bool operator >= (ThreeDimensionPoint& rhs) {
        return operator==(rhs) || operator>(rhs);
    }

    ThreeDimensionPoint() { /* not implemented */ }

    ThreeDimensionPoint(double a, double b, double c): x(a), y(b), z(c) {}

    double getX() { return x; }

    double getY() { return y; }

    double getZ() { return z; }
    
};

template <typename T>
class Quick3Way {

private:

    static void sort(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        int left = lo, i = lo + 1, right = hi;
        T pivot = a[lo];
        while (i <= right) {
            if (a[i] < pivot) swap(a[i++], a[left++]);
            else if (a[i] > pivot) swap(a[i], a[right--]);
            else i++;
        }
        sort(a, lo, left-1);
        sort(a, right+1, hi);
    }

public:

    static void sort(vector<T>& a) {
        uniformShuffle(a);
        sort(a, 0, a.size()-1);
    }

}; 

template <typename T>
class Insertion {

public:

    static void sort(vector<T>& a) {
        int N = a.size();
        for (int i = 0; i < N; i++) {
            for (int j = i; j > 0 && a[j] < a[j-1]; j--)
                swap(a[j], a[j-1]);
        }
    }

};
