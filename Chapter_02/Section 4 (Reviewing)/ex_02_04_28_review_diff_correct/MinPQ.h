#include <iostream>
#include <vector>

using namespace std;

/* MinPQ : Parent < Child */
template <typename T>
class MinPQ {
private:

    int N = 0;

    vector<T> pq;

    void sink(int k) {
        while (2 * k  < N) {
            int j = 2 * k;
            if (j < N && pq[j] > pq[j+1]) j++;
            if (pq[k] <= pq[j]) break;
            swap(pq[k], pq[j]);
            k = j;
        }
    }
         
    void swim(int k) {
        while (k > 1 && pq[k/2] > pq[k]) {
            swap(pq[k], pq[k/2]);
            k = k/2;
        }
    }

public:

    friend ostream& operator << (ostream& os, MinPQ<T> minPQ) {
        while (!minPQ.isEmpty())
            os << minPQ.delMin() << endl;
       return os;
    }

    MinPQ() = default;

    MinPQ(int maxN): N(0), pq(maxN+1) {}

    virtual ~MinPQ() {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert(T item) {
        pq[++N] = item;
        swim(N);
    }

    T getMin() { return pq[1]; };

    T delMin() {
        T min = pq[1];
        swap(pq[1], pq[N--]);
        sink(1);
        return min;
    }

};
