#include <iostream>
#include <vector>
#include "Random.h"

using namespace std;

int main(int argc, char** argv) {

    int N = stoi(argv[1]) * 3;

    for (int i = 1; i <= N; i++) {
        if (i % 3 == 0) {
            cout << randomUniformDistribution(0, N-1) << endl;
        }
        else
            cout << randomUniformDistribution(0, N-1) << " ";
    }

    return 0;

}
