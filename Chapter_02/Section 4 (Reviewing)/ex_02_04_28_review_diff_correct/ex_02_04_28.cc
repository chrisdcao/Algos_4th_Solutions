/* write a TopM CLIENT that  */
/* 
 * Input:
 * 1. reads: (x,y,z) from the input
 * 2. takes a value M frmo commandline
 * 
 * Requirement:
 * 1. prints M points that are closest to the origin in Euclidean distance 
 * 2. Estimate running time for N = 10^8 && M = 10^4
 *  */

#include <fstream>
#include "QuickSort.h"
#include "ThreeDimension.h"
#include "MinPQ.h"
using namespace std;

int main(int argc, char** argv) {

    int M = stoi(argv[1]);

    ifstream input("ex_02_04_28_in.txt");

    ThreeDimensionPoint threeDimensionPoint;

    MinPQ<ThreeDimensionPoint> vecOfPoints(M);

    for (int i = 0; i < M; i++) {
        input >> threeDimensionPoint;
        vecOfPoints.insert(threeDimensionPoint);
    }

    cout << vecOfPoints << endl;

    /* since our criteria of comparing the points is based on its distance to the origin, the sorting will result in smallest point distance being on the top */
    cout << "The point with minimum Euclidean Distance to the Origin is: " <<  vecOfPoints.getMin() << endl;

    return 0;

}
