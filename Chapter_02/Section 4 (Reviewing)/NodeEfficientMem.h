#ifndef MEMORYEFFICIENTNODE_H
#define MEMORYEFFICIENTNODE_H

#include <iostream>
#include <memory>
#include <vector>

using namespace std;

template <typename T> 
class Node {
public:
    /* original items of a Node */
    T item;
    Node* nextLeft = NULL;
    Node* nextRight = NULL;
    Node* prev = NULL;
    /* Memory management */
    int index = -1;
}

vector<unique_ptr<Node>> allNodes;

Node* newNode() {
    allNodes.push_back(make_unique<Node>());
    allNodes.back()->index = allNodes.size() - 1;
    return allNodes.back().get();
}

void freeNode(Node* node) {
    if (node->index < allNodes.size()) {
        allNodes[node->index].swap(allNodes.back());
        allNodes[node->index]->index = node->index;
    }
    allNodes.pop_back();
}

#endif
