#include <iostream>
#include <vector>
#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

/* Max Heap: Parent > child */
template <typename T>
class MaxPQ {
private:

    int N = 0;
    vector<T> pq;
    T min;
    bool minAssigned = false;

    void sink(int k) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j] < pq[j+1]) j++;
            if (pq[k] >= pq[j]) break;
            swap(pq[k], pq[j]);
            k = j;
        }
    }

    void swim(int k) {
        while (k > 1 && pq[k/2] < pq[k]) {
            swap(pq[k], pq[k/2]);
            k = k / 2;
        }
    }

public:

    friend ostream& operator << (ostream& os, MaxPQ<T> maxPQ) {
        while (!maxPQ.isEmpty())
            os << maxPQ.delMax() << " ";
        return os;
    }
    
    void printInsideVector() {
        for (auto s : pq)
            cout << s << " ";
        cout << endl;
    }

    bool isEmpty() { return N == 0; }

    int size() { return N; }

    MaxPQ() = default;

    MaxPQ(int maxN): N(0), pq(maxN+1), minAssigned(false) {}

    void insert(T item) {
        if (min > item || minAssigned == false) {
            min = item;
            minAssigned = true;
        }
        pq[++N] = item;
        swim(N);
    }

    T getMax() { return pq[1]; }

    T delMax() {
        T item = pq[1];
        swap(pq[1], pq[N--]);
        pq[N+1] = NULL;
        sink(1);
        return item;
    }

    T getMin() { return min; }

};
 
int main(int argc, char** argv) {
    
    int N = stoi(argv[1]);
    MaxPQ<int> maxPQ(N);
    for (int i = 0; i < N; i++) maxPQ.insert(i);

    /* cout << maxPQ << endl; */
    maxPQ.printInsideVector();
    cout << maxPQ.getMin() << endl;

    return 0;

}
