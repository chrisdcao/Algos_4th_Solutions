#include <iostream>
#include <string>
#include <numeric>
#include <iomanip>
#include <vector>
#include <cmath>
#include "MinHeapDynamic.h"

using namespace std;

class Point3D {

private:

    int x, y, z; 

public:

    friend ostream& operator << ( ostream& os, Point3D point3D ) {
        os << "( " <<  point3D.x << ", " << point3D.y << ", " << point3D.z << " )";
        return os;
    }

    friend istream& operator >> ( istream& is, Point3D& point3D ) {
        is >> point3D.x >> point3D.y >> point3D.z;
        return is;
    }

    Point3D() = default;

    Point3D( int x, int y, int z ): x( x ), y( y ), z( z ) {}

    int distanceToOrigin() { return pow( x,2 ) + pow( y,2 ) + pow( z,2 ); }

    bool operator > ( Point3D rhs ) { return distanceToOrigin() > rhs.distanceToOrigin(); }

    bool operator < ( Point3D rhs ) { return distanceToOrigin() < rhs.distanceToOrigin(); }

    bool operator == ( Point3D rhs ) { return distanceToOrigin() == rhs.distanceToOrigin(); }

    bool operator != ( Point3D rhs ) { return !operator==( rhs ); }

    bool operator >= ( Point3D rhs ) { return operator>( rhs ) || operator==( rhs ); }

    bool operator <= ( Point3D rhs ) { return operator<( rhs ) || operator==( rhs ); }

    Point3D& operator = ( Point3D rhs ) {
        if ( this == &rhs ) return *this;
        x = rhs.x;
        y = rhs.y;
        z = rhs.z;
        return *this;
    }

};

int main( int argc, char** argv ) { 

    int M = stoi( argv[ 1 ] );

    Point3D point3D;

    MinHeap<Point3D> pointsPQ;

    while ( cin >> point3D ) pointsPQ.insert( point3D );

    cout << M << " closest points to Origin are:\n";
    while ( M > 0 ) {
        cout << pointsPQ.delMin() << endl;
        M--;
    }

    return 0;
    
}
