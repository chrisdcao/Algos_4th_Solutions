#ifndef STACK_H
#define STACK_H
#include <iostream>
#include <vector>
#include <memory>
#include <string>
using namespace std;

template <typename T=int>
class Stack {
private:
    class Node {
    public:
        T item;
        Node* next;
        int index = -1;
    };

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    vector<unique_ptr<Node>> allNodes;

    int N;
    Node* tail;
    
    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* initLoc) {
            current = initLoc;
        }
        Iterator& operator++(int rhs) {
            Node* temp = current;
            current = current->next;
            return temp;
        }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return this->current->item;
        }
    };

public:
    Stack() {
        N = 0;
        tail = NULL;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void push(T item) {
        Node* temp = newNode();
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            tail = temp;
            tail =temp;
        } else {
            temp->next = tail;
            tail = temp;
        }
        ++N;
    }

    Iterator begin() { return Iterator(tail); }
    Iterator end() { return Iterator(NULL); }

    T pop() {
        T item = tail->item;
        if (N != 0) {
            Node* oldtail = tail;
            tail = tail->next;
            freeNode(oldtail);
            --N;
        } else 
        return item;
    }

    T peek() {
        return tail->item;
    }
};

#endif
