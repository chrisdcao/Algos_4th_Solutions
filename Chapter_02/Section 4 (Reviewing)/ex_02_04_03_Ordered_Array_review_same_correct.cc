#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

template <typename T>
class OrderedArrayPQ {

    template <typename H> friend class OrderedLinkedListPQ;

private:

    T* pq;
    int N = 0;
    int cap = 0;

    void resize(int maxSize) {
        T* temp = new T[maxSize];
        for (int i = 0; i < N; i++) 
            temp[i] = pq[i];
        delete[] pq;
        pq = temp; 
    }

public:

    friend ostream& operator << (ostream& os, OrderedArrayPQ<T> orderedArrayPQ) {
        while (!orderedArrayPQ.isEmpty())
            os << orderedArrayPQ.delMax() << " ";
        return os;
    }

    OrderedArrayPQ(): N(0), cap(1) { pq = new T[1]; }

    OrderedArrayPQ(int maxN): N(0), cap(maxN) { pq = new T[maxN]; }

    virtual ~OrderedArrayPQ() {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert(T item) { 
        if (N == cap) {
            cap *= 2;
            resize(cap);
        }
        int index = N;
        if (item < pq[N]) {
            for (int i = 0; i < N; i++) {
                if (item <= pq[i]) {
                    index = i;
                    break;
                }
            }
            for (int i = N; i >= index; i--) {
                pq[i+1] = pq[i];
            }
            pq[index] = item;
            N += 1;
            return;
        }
        pq[++N] = item; 
    }

    T getMax() { return pq[N]; }

    T delMax() {
        /* exchange with end */
        T max = pq[N];
        pq[N--] = NULL;
        return max;
    }

};

int main(int argc, char** argv) {
    
    int N = stoi(argv[1]);
    OrderedArrayPQ<int> orderedArrayPQ(N);

    for (int i = 0; i < N; i++) {
        orderedArrayPQ.insert(randomUniformDistribution(0, N-1));
    }
    cout << orderedArrayPQ << endl;
    
    return 0;

}
