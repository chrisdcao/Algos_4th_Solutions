/* PLEASE USE test.cc to generate array and return result, then use that generated array as input for this program and compare the output with the result of test.cc */
#include "MaxHeap.h"
#include "MinHeap.h"

using namespace std;

template <typename T>
class MedianPQ {
private:

    MaxHeap<T> maxPQ;
    MinHeap<T> minPQ;
    int N = 0;

public:

    MedianPQ() = default;

    MedianPQ(int maxN): maxPQ(maxN+1), minPQ(maxN+1) {}

    /* By using this the median is always at the top of the 2 sub-heaps (min and max), as we are arranging max elements in minOrder and min element in maxOrder
     * The min of all the max and the max of all the min when 2 sub-heaps have equal sizes is(are) the median(s) */
    void insert(T item) {
        if (N == 0 || item < maxPQ.getMax()) maxPQ.insert(item);
        else                                 minPQ.insert(item);
        // always balance the size of 2 heaps
        if (maxPQ.size() > minPQ.size() + 1) minPQ.insert(maxPQ.delMax());
        else                                 maxPQ.insert(minPQ.delMin()); 
        N++;
    }

    /* so we just extract the top element of each subheap and we get the median
     * for even-sized arrays, we return the most-left of the medians. This is to avoid the even-bias (when median number is 2 and we keep removing 2 median in even-size array, it will forever be even-size and will keep returning 2 median)*/
    // ~ O(1)
    T findMedian() {
        T median;
        if (minPQ.size() > maxPQ.size()) median = minPQ.getMin();
        else                             median = maxPQ.getMax();
        return median;
    }

    // ~ 2log(N/2)
    T delMedian() {
        T median;
        if (minPQ.size() > maxPQ.size()) median = minPQ.delMin();
        else                             median = maxPQ.delMax();
        N--;
        return median;
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    int x, count=0;
    MedianPQ<int> medianPQ(N);
    while (cin >> x) {
        medianPQ.insert(x);
        ++count;
        if (count == N) break;
    }

    cout << endl;
    cout << medianPQ.findMedian() << endl;

    return 0;

}
