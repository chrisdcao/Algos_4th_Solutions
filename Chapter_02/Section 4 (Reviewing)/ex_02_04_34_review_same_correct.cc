#include <iostream>
#include <vector>
#include "Random.h"

using namespace std;

// this is the dynamic implementation of IndexMaxPQ
template <typename Key> class IndexMaxPQ {

private:

    vector< int > pq;
    vector< int > qp;
    vector< Key > keys;
    int N = 0;

    void exchange( int i, int j ) {
        int temp = pq[ i ];
        pq[ i ] = pq[ j ];
        pq[ j ] = temp;
        qp[ pq[ i ] ] = i;
        qp[ pq[ j ] ] = j;
    }

    bool greater( int i, int j ) {
        return keys[ pq[ i ] ] > keys[ pq[ j ] ];
    }

    void sink( int k ) {
        while ( 2 * k <= N ) {
            int j = 2 * k;
            if ( j < N && greater( j+1, j ) ) j++;
            if ( ! greater( j, k ) ) break;
            exchange( k, j );
            k = j;
        }
    }

    void swim( int k ) {
        while ( k > 1 && greater( k, k/2 ) ) {
            exchange( k, k/2 );
            k = k/2;
        }
    }

public:

    IndexMaxPQ(): keys( 1 ), pq( 1 ), qp( 1 ), N( 0 ) {}

    bool contains( int k ) { return qp[ k ] != -1; }

    int size() { return N; }

    bool isEmpty() { return size() == 0; }

    Key getMax() { return keys[ getMaxIndex() ]; }

    int getMaxIndex() { return pq[ 1 ]; }

    void insert( int k, Key key ) {
        N++;
        pq.push_back( N );
        qp.push_back( k );
        keys.push_back( key );
        swim( N );
    }

    int delMax() {
        int max = pq[ 1 ];
        exchange( 1, N-- );
        sink( 1 );
        pq[ N+1 ] = -1;
        qp[ max ] = -1;
        return max;
    }

    void deleteKey( int k ) {
        if ( ! contains( k ) ) throw invalid_argument( "deleteKey(): Invalid index!" );
        int index = qp[ k ];
        exchange( index, N-- );
        swim( index );
        sink( index );
        pq[ N+1 ] = -1;
        qp[ k ] = -1;
    }

    void change( int k, Key key )  {
        if ( ! contains( k ) ) throw invalid_argument( "change(): Invalid index!" );
        keys[ k ] = key;
        swim( qp[ k ] );
        sink( qp[ k ] );
    }

    friend ostream& operator <<( ostream& os, const IndexMaxPQ< Key >& maxPQ ) {
        for ( int i = 0; i < maxPQ.pq.size(); i++ ) 
            os << i << " : " << maxPQ.pq[ i ] << endl;

        os << "\nqp:\n";
        for( int i = 0; i < maxPQ.qp.size(); i++ )
            os << i << " : " << maxPQ.qp[ i ] << endl;

        os << "\nkeys:\n";
        for( int i = 0; i < maxPQ.keys.size(); i++ )
            os << i << " : " << maxPQ.keys[ i ] << endl;
        
        return os;
    }

};

int main ( int argc, char** argv ) {

    int intArr[] = { 14, 12, 10, 8, 6, 4, 2, 0, 1, 3, 5, 7, 9, 11, 13 };
    IndexMaxPQ< int > MaxPQ;
    for ( int i = 0; i < sizeof( intArr ) / sizeof( int ); ++i ) {
        MaxPQ.insert( i, intArr[ i ] );
    }

    cout << "DEBUG: MaxPQ: \n" << MaxPQ << endl;
    MaxPQ.deleteKey( 4 );
    cout << "DEBUG: MaxPQ.deleteKey( 4 ): \n" << MaxPQ << endl;
    MaxPQ.change( 3, 20 );
    cout << "DEBUG: MaxPQ.change( 3, 20 ): \n" << MaxPQ << endl;
    cout << "DEBUG: MaxPQ.getMax(): \n" << MaxPQ.getMax() << endl;
    cout << "DEBUG: MaxPQ.getMaxIndex(): \n" << MaxPQ.getMaxIndex() << endl;
    cout << "DEBUG: MaxPQ.delMax(): \n" << MaxPQ.delMax() << endl;
    cout << "DEBUG: MaxPQ.contains( 4 ): \n" << MaxPQ.contains( 4 ) << endl;
    cout << "DEBUG: MaxPQ.contains( 10 ): \n" << MaxPQ.contains( 10 ) << endl;

    return 0;

}
