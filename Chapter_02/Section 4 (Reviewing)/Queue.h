#ifndef QUEUE_H
#define QUEUE_H

#include <iostream>
#include <string>
#include <vector>
#include <memory>
using namespace std;

template <typename T>
class Queue {
private:

    class Node {
    public:
        T item = T();
        Node* next=NULL;
        int index = -1;
    };

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;

    Node* head=NULL;
    Node* tail=NULL;
    int N=0;

    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* initLoc) {
            current = initLoc;
        }

        Iterator& operator++() {
            current = current->next;
            return *this;
        }

        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }

        T operator*() {
            return this->current->item;
        }
    };

public:

    Queue() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    Queue(Queue<T>& rhs) {
        for (auto s : rhs) 
            this->enqueue(s);
    }

    Queue(Queue<T>&& rhs) {
        for (auto s : rhs) 
            this->enqueue(s);
    }

    ~Queue() {}

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void enqueue(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }

    T dequeue() {
        if (head == NULL)
            throw out_of_range("Underflow");
        T item = head->item;
        Node* dp = head;
        head = head->next;
        freeNode(dp);
        --N;
        return item;
    }

    T peek() {
        T temp = head->item;
        return temp;
    }

    Iterator begin() { return Iterator(head); }
    Iterator end() { return Iterator(tail->next); }

    Queue& operator=(Queue& rhs) {
        while (!this->isEmpty())
            this->dequeue();
        for (auto s : rhs)
            enqueue(s);
        return *this;
    }

};

#endif
