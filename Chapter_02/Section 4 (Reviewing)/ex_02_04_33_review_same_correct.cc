#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <iomanip>

using namespace std;

// Max: parent > child
template <typename Key> class IndexMaxPQ {

private:

    vector< int > pq;
    vector< int > qp;
    vector< Key > keys;
    int N = 0;

    void exchange( int i, int j ) {
        int temp = pq[ i ];
        pq[ i ] = pq[ j ];
        pq[ j ] = temp;
        qp[ pq[ i ] ] = i;
        qp[ pq[ j ] ] = j;
    }

    bool less( int i, int j ) { 
        return keys[ i ] < keys[ j ]; 
    }

    void swim( int k ) {
        while ( k > 1 && less( pq[ k/2 ], pq[ k ] ) ) {
            exchange( k/2, k );
            k /= 2;
        }
    }

    void sink( int k ) {
        while ( 2 * k <= N ) {
            int j = 2 * k;
            if ( j < N && less( pq[ j ], pq[ j+1 ] ) ) j++;
            if ( ! less( pq[ k ], pq[ j ] ) ) break;
            exchange( k, j );
            k = j;
        }
    }

public:

    IndexMaxPQ( int maxN ): keys( maxN + 1 ), pq( maxN + 1 ), qp( maxN + 1 ) {}

    int size() { return N; }

    bool contains( int k ) { return qp[ k ] != -1; }

    bool isEmpty() { return size() == 0; }

    Key getMax() { return keys[ pq[ 1 ] ]; }

    int getMaxIndex() { return pq[ 1 ]; }

    void insert( int k, Key key ) {
        N++;
        pq[ N ] = k;
        keys[ k ] = key;
        qp[ k ] = N;
        swim( N );
    }

    void deleteKey( int k ) {
        if ( ! contains( k ) ) throw invalid_argument( "deleteKey(): Invalid index!" );;
        int index = qp[ k ];
        exchange( index, N-- );
        sink( index );
        swim( index );
        pq[ N+1 ] = -1;
        qp[ k ] = -1;
    }

    int delMax() {  
        int max = pq[ 1 ];
        exchange( 1, N-- );
        sink( 1 );
        pq[ N+1 ] = -1;
        qp[ max ] = -1;
        return max;
    }

    void change( int k, Key key ) {
        if ( ! contains( k ) ) throw invalid_argument( "change(): Invalid index!" );
        keys[ k ] = key;
        sink( qp[ k ] );
        swim( qp[ k ] );
    }

    friend ostream& operator <<( ostream& os, const IndexMaxPQ< Key >& MaxPQ ) {
        os << "\npq:\n";
        for ( int i = 0; i < MaxPQ.pq.size(); ++i ) {
            os << i << " : " << MaxPQ.pq[ i ] << endl;
        }

        os << "\nqp:\n";
        for ( int i = 0; i < MaxPQ.qp.size(); ++i ) {
            os << i << " : " << MaxPQ.qp[ i ] << endl;
        }

        os << "\nkeys:\n";
        for ( int i = 0; i < MaxPQ.keys.size(); ++i ) {
            os << i << " : " << MaxPQ.keys[ i ] << endl;
        }
        return os;
    }

};

int main ( int argc, char** argv ) {

    int intArr[] = { 14, 12, 10, 8, 6, 4, 2, 0, 1, 3, 5, 7, 9, 11, 13 };
    IndexMaxPQ< int > MaxPQ( sizeof( intArr ) / sizeof( int ) );
    for ( int i = 0; i < sizeof( intArr ) / sizeof( int ); ++i ) {
        MaxPQ.insert( i, intArr[ i ] );
    }

    cout << "DEBUG: MaxPQ: \n" << MaxPQ << endl;
    MaxPQ.deleteKey( 4 );
    cout << "DEBUG: MaxPQ.deleteKey( 4 ): \n" << MaxPQ << endl;
    MaxPQ.change( 3, 20 );
    cout << "DEBUG: MaxPQ.change( 3, 20 ): \n" << MaxPQ << endl;
    cout << "DEBUG: MaxPQ.getMax(): \n" << MaxPQ.getMax() << endl;
    cout << "DEBUG: MaxPQ.getMaxIndex(): \n" << MaxPQ.getMaxIndex() << endl;
    cout << "DEBUG: MaxPQ.delMax(): \n" << MaxPQ.delMax() << endl;
    cout << "DEBUG: MaxPQ.contains( 4 ): \n" << MaxPQ.contains( 4 ) << endl;
    cout << "DEBUG: MaxPQ.contains( 10 ): \n" << MaxPQ.contains( 10 ) << endl;

    return 0;

}
