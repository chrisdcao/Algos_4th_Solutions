#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

/* MinHeap: Parent < Child */
template <typename T>
class MinPQ {
private:

    vector<T> pq;
    int N = 0;

    void sink(int k) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j] > pq[j+1]) j++;
            if (pq[k] <= pq[j]) break;
            swap(pq[k], pq[j]);
            k = j;
        }
    }

    void swim(int k) {
        while (k > 1 && pq[k/2] > pq[k]) {
            swap(pq[k], pq[k/2]);
            k = k/2;
        }
    }

public:

    friend ostream& operator << (ostream& os, MinPQ<T> minPQ) {
        while (!minPQ.isEmpty()) 
            os << minPQ.delMin() << " ";
        return os;
    } 

    /* at each node, we check if either of its 2 local leaves are smaller than them
     * Since we finish checking 2 items at each traversal, the amount of distance to travel = size() / 2
     * If yes then return false
     * Total cost ~ Linear */
    friend bool checkIfMinPQ(MinPQ<T> minPQ) {
        for (int i = 1; i < minPQ.size() / 2; i++) 
            if ( minPQ.pq[i * 2] < minPQ.pq[i] || (i*2+1 < minPQ.size() && minPQ.pq[i*2 + 1] < minPQ.pq[i]))
                return false;
        return true;
    } 

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    MinPQ() = default;

    MinPQ(int maxN): pq(maxN+1), N(0) {}

    virtual ~MinPQ() {}

    void insert(T item) {
        pq[++N] = item;
        swim(N);
    }

    T getMin() { return pq[1]; }

    T delMin() {
        T item = pq[1];
        swap(pq[1], pq[N--]);
        sink(1);
        return item;
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    MinPQ<int> minPQ(N);

    for (int i = 0; i < N; i++)
        minPQ.insert(randomUniformDistribution(0, N-1));

    cout << minPQ << endl;

    cout << ((checkIfMinPQ(minPQ) ? "true" : "false"))  << endl;

    return 0;

}
