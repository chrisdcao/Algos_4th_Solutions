#include <iostream>
#include <numeric>
#include <string>
#include <iomanip>
#include "Random.h"

using namespace std;

class Sample {

private:

    //This is just a Pseudo-Node (just for better visualizing of the Heap tree, rather than being a real Node)
    class Node {
    public:
        double weight;
        double cumulativeWeight;
        Node() = default;
        Node( double w ): weight( w ) {}
        Node& operator=(Node&& rhs) {
            weight = rhs.weight;
            cumulativeWeight = rhs.cumulativeWeight;
            return *this;
        }

        Node& operator=(Node* rhs) {
            weight = rhs->weight;
            cumulativeWeight = rhs->cumulativeWeight;
            return *this;
        }

        Node& operator=(Node& rhs) {
            weight = rhs.weight;
            cumulativeWeight = rhs.cumulativeWeight;
            return *this;
        }
    };

    vector< Node > nodes;

    vector<double>::iterator vector_first;

    double sum = 0;

    // update all the nodes from the changed notes upwards, as they are the only ones being impacted
    void updateCumulativeWeights( int index, double difference ) {
        while ( index >= 1 ) {
            nodes[ index ].cumulativeWeight += difference;
            index /= 2;
        }
    }

    void computeCumulativeWeights() {
        for ( int i = nodes.size()-1; i > 1; i-- )
            nodes[ i/2 ].cumulativeWeight += nodes[ i ].cumulativeWeight + nodes[ i ].weight;
    }

public:

    // This sample will take a vector
    Sample( vector< double >& probabilities ): nodes( probabilities.size() + 1 ) {
        vector_first = probabilities.begin();
        for ( int i = 1; i <= probabilities.size(); i++ ) {
            // travelling on the index of heap, thus we have to minus 1 to travel normally on vector indexing
            double weight = probabilities[ i-1 ];
            nodes[ i ] = new Node( weight );
            sum += weight;
        }
        computeCumulativeWeights();
    }

    // the goal is to go to the element with the closest value to the random value
    // so basically we are picking branches so that chúng ta có thể đi hết đc giá trị của cái random value này 
    int random() { 
        // this will create 1/T probability ( the probability of uniform picking 1 value randomly from T )
        double randomValue = randomUniformDistribution( 0, sum );
        int index = 1;
        while ( randomValue < nodes[ index ].cumulativeWeight ) {
            // defaultly exploring the left subtree
            index *= 2;
            double leftSubtreeWeight = nodes[ index ].cumulativeWeight + nodes[ index ].weight;
            // if randomValue >= left weight, meaning that the ceiling of random value on left branch
            // we change to right node and set to explore its left subtree from there, with the cumulative weight changed
            if ( randomValue >= leftSubtreeWeight ) {
                randomValue -= leftSubtreeWeight;
                index++;
            }
        }
        return index-1;
    }

    void change( int i, double v ) {
        *(vector_first + i) = double(v);
        i++;
        // difference = new - old
        double difference = v - nodes[ i ].weight;
        nodes[ i ].weight = v;
        sum += difference;
        // we update one level above it forward, since the cumulative only takes all the values before it ( excluding itself )
        updateCumulativeWeights( i/2, difference );
    }

};


//  client
int main( int argc, char** argv ) {

    /*
        The sample will have a constructor taking array p[] and support the following operations:
            1. random(): return an index i with P = p[i]/T (where T = ∑ all elements in T
            2. change(i, v): change the value of p[i] → v
    */
    vector< double > weights = { 0.14,1.72,2.68,3.12 };

    Sample sample( weights );

    cout << "Randomly generated index i: " << sample.random() << endl;
    cout << "Change value at index 3 to 12.43\n"; 
    sample.change(3, 12.43); 

    for (auto s : weights) 
        printf("%.2f ", s);
    cout << endl;

    return 0;

}
