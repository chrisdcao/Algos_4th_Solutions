#include <iostream>
#include <vector>
#include <sstream>
#include <memory>

using namespace std;

template <typename T> 
class SinglyLinkedList {

    template <typename H> friend class OrderedLinkedListPQ;

private:
    class Node {
    public:
        T item;
        Node* next = NULL;
        int index = -1;
    };

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;

    int N;
    Node* head;

public:
    SinglyLinkedList(): N(0), head(NULL) {}

    friend ostream& operator<<(ostream& os, SinglyLinkedList& list) {
        auto traverse = list.head;
        while (traverse != NULL) {
            os << traverse->item << " ";
            traverse = traverse->next;
        }
        os << endl;
        return os;
    }

    Node* getHead() { return head; }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void insert(T item) {
        Node* temp = newNode();
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
        } else {
            temp->next = head;
            head = temp;
        }
        N++;
    }

    T removeAtBegin() {
        if (head == NULL) throw out_of_range("underflow");
        T item = head->item;
        Node* oldHead = head;
        head = head->next;
        freeNode(oldHead);
        --N;
        return item;
    }
    
    Node* reverseRecursively(Node* head) {
        if (head == NULL) return NULL;
        if (head->next == NULL) return head;
        Node* second = head->next;
        Node* newHead = reverseRecursively(second);
        second->next = head;
        head->next = NULL;
        return newHead;
    }
   
    Node* reverseIteratively(Node* head) {
        if (N <= 1) return head;
        else {
            Node* prev = NULL;
            Node* current = head;
            Node* next = head->next; 
            while (next != NULL) {
                current->next = prev; 
                prev = current;
                current = next;
                next = next->next;
            }
            current->next = prev;
            return current;
        }
    }

    void reverse(bool isRecursive=true) {
        if (isRecursive)
            head = reverseRecursively(head);
        else
            head = reverseIteratively(head);
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }
};
