#include <iostream>
#include <vector>
#include <numeric>
#include <cmath>
#include <iomanip>
#include "IntegerLogarithm.h"
#include "Random.h"
#include "ContainerUtility.h"
#include "MinHeap.h"

using namespace std;

/* MinHeap: Parent < child */
template <typename T>
class MinPQ {
private:

    int BinarySearchAncestor(T key, vector<T>& pq, int lo, int hi) {
        /* hi is the maximum in the vertical array, and so if key > hi then nothing to do */
        if (key < pq[lo]) return lo;
        if (key >= pq[hi]) return -1;
        while(intLog(hi/lo) > 1) {
            int mid = hi / pow(2,(intLog(hi/lo) / 2));
            if (key < pq[mid])      hi = mid;    
            else if (key > pq[mid]) lo = mid;
            else                    return mid;
        }
        return hi;
    }

    int N = 0;
    vector<T> pq;

    void moveDownFrom(int k, int current_end) {
        while (current_end > k) {
            pq[current_end] = pq[current_end/2];
            current_end /= 2;
        }
    }

    void sink(int k) {
        while(2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j] > pq[j+1]) j++;
            if (pq[k] <= pq[j]) break;
            swap(pq[k], pq[j]);
            k = j;
        }
    }

public:

    friend ostream& operator << (ostream& os, MinPQ<T> minPQ) {
        while(!minPQ.isEmpty())
            os << minPQ.delMin() << " ";
        return os;
    }

    MinPQ() = default;

    MinPQ(int maxN): N(0), pq(maxN+1) {}

    virtual ~MinPQ() {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void printHeap() {
        for (auto s : pq)
            cout << s << " ";
        cout << endl;
    }

    void insert(T item) {
        ++N;
        /* we only search from the node above the about-to-be inserted node up */
        int insert_index = BinarySearchAncestor(item, pq, 1, N/2);
        /* for printin the inside heap */
        bool inside = false;
        if (insert_index == -1)  {
            pq[N] = item;
            return;
        }
        moveDownFrom(insert_index, N);
        pq[insert_index] = item;
    }

    T delMin() {
        T min = pq[1];
        swap(pq[1], pq[N--]);
        sink(1);
        return min;
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    MinPQ<int> minPQ(N);
    MinHeap<int> minHeap(N);
    vector<int> vec;
    for (int i = 0; i < N; i++) {
        vec.push_back(randomUniformDistribution(0, N-1));
        minPQ.insert(vec[i]);
        minHeap.insert(vec[i]);
    } 
    cout << "input: " << vec << endl;
    cout << "Standard: ";
    minHeap.printHeap();
    cout << "Our Heap: ";
    minPQ.printHeap();
    cout << minPQ << endl;

    return 0;

}

/* CALCULATION EXPLANATION:
 * Assume we have a tree:
 *                          1
 *               2                     _3
 *         4            _5         _6            _7
 * 
 *    _8        9   _10   _11   _12   _13    _14     _15
 * 
 * _16  _17  _18   19    
 * 
 * We examine the case of BinarySearch on vertical array of {19,9,4,2,1}
 * I. Vertical Travel
 * We know that if we divide 19 by 2 we traverse ONE vertical-index up to index 9 (19/2 = 9). So divide by 2 = 1 traverse in Vertical Travel
 * -> Thus to get to index 1 from index 19 we need to divide 19 until = 1. We have:
 *         19/2/2/2/2/2...2 = 1        (call k as the steps we take, we have to divide  k numbers of 2)
 *     <=> 19 = 1 * (2*2*2...2)        
 *     <=> 19/1 = 2^k
 *     <=> log(19/1) = k
 * so log(19/1) = number of steps we take from 19 - 1. Generalize this idea, we have:
 *                 number of steps from [hi] -> [lo] = log(hi/lo) (lo > 0)
 *     or:         [lo] = [hi] / log(hi/lo)
 * So to retrieve index of mid, we only travel HALF the total steps from hi to lo, which is log(hi/lo)/2
 * ---->           [mid] = [hi]/2^(log(hi/lo)/2)
 * Everything else can be calculated based on this idea
 * */

