#ifndef MAXPQ_H
#define MAXPQ_H

#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <random>
#include "Random.h"
#include "Stack.h"
#include "ContainerUtility.h"

using namespace std;

/* MaxHeap: parent > child */
template <typename T>
class MaxPQ {
private:
    vector<T> pq;
    int N = 0;

    void sink(int k) {
        while (2 * k < N) {
            int j = 2 * k;
            if (j < N && pq[j] < pq[j+1]) j++;
            if (!(pq[j] > pq[k])) break;
            swap(pq[j], pq[k]);
            k = j;
        }
    }

    void swim(int k) {
        while (k > 1 && pq[k/2] < pq[k]) {
            swap(pq[k/2], pq[k]);
            k /= 2;
        }
    }

public:

    /* we only call on copy of maxPQ, cause we are doing remove operation */
    friend ostream& operator<<(ostream& os, MaxPQ<T> maxPQ) {
        while (!maxPQ.isEmpty()) 
            os << maxPQ.delMax() << " ";
        return os;
    }

    MaxPQ() = default;
    MaxPQ(int maxN): pq(maxN+1) {}

    virtual ~MaxPQ() { /* not implemented */ }

    int size() { return N; }

    void printCheckRealPQ() {
        for (auto s : pq)
            cout << s << " ";
        cout << endl;
    }

    bool isEmpty() { return N == 0; }

    void insert(T item) {
        pq[++N] = item;
        swim(N);
    }

    T getMax() { return pq[1]; }

    T delMax() {
        if (N == 0) {
            cout << "Underflow. Returning default value for type: " << endl;
            return T();
        }
        T max = pq[1];
        swap(pq[1], pq[N--]);
        pq[N+1] = NULL;
        sink(1);
        return max;
    }

};

#endif
