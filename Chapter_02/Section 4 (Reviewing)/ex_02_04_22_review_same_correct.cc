/* SINCE USING VECTOR DON'T NEED RESIZING, I WILL USE ARRAY IN THIS IMPLEMENTATION OF PQ */
#include <iostream>
#include "Random.h"
#include "ContainerUtility.h"
#include <memory>
#include <utility>
#include <iterator>

using namespace std;

/* MaxPQ: Parent > child */
template <typename T>
class MaxPQ {
private:

    T* pq;
    int N = 0;
    int cap = 0;

    void sink(int k) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j] < pq[j+1]) j++;
            if (pq[k] >= pq[j]) break;
            swap(pq[k], pq[j]);
            k = j;
        }
    }

    void swim(int k) {
        while (k > 1 && pq[k/2] < pq[k]) {
            swap(pq[k], pq[k/2]);
            k = k / 2;
        }
    }

    void resize(int max) {
        T* temp = new T[max];
        for (int i = 0; i < max; i++)
            temp[i] = pq[i];
        delete[] pq;
        pq = temp;
    }

public:

    friend ostream& operator << (ostream& os, MaxPQ<T> maxPQ) {
        while (!maxPQ.isEmpty()) 
            os << maxPQ.delMax() << " ";
        return os;
    }

    MaxPQ(): N(0), cap(0) {}

    MaxPQ(int maxN): N(0), cap(maxN+1) { pq = new T[maxN+1]; }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert(T item) {
        if (N == cap) {
            cap *= 2;
            resize(cap);
        }
        pq[++N] = item;
        swim(N);
    }

    T getMax() {
        if (N == 0) {
            cout << "underflow! returning the default value of the type!" << endl;
            return T();
        }
        return pq[1];
    }

    T delMax() {
        if (N == 0) {
            cout << "underflow! returning the default value of the type!" << endl;
            return T();
        }
        if (N == cap/4) {
            cap = cap/2;
            resize(cap);
        }
        T item = pq[1];
        swap(pq[1], pq[N--]);
        sink(1);
        pq[N+1] = NULL;
        return item;
    }

};

int main(int argc, char** argv) {
    
    int N = stoi(argv[1]);
    MaxPQ<int> maxPQ(N);
    for (int i = 0; i < N; i++)
        maxPQ.insert(randomUniformDistribution(0, N-1));

    cout << maxPQ << endl;

    return 0;

}
