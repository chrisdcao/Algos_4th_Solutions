/* current method of finding the maximum in constant time (in a MaxHeap): */
T getMax() { return pq[1]; }        /* constant time = 1 */

void insert(T item) {
    pq[++N] = item;
    /* keeping track of the current maximum by preserving the heap structure */
    swim(N);
}

if we use stack or queue, we fail to update the maximum if any delMax() operations happen - because at this point the structure might not be preserved anymore (while keeping track constantly help use to preserve the structure of the heap and be able to always use constant time to find the max)
