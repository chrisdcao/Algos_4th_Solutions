#include "Random.h"
#include "ContainerUtility.h"
#include "Stack.h"

using namespace std;

/* MaxHeap: parent > child */
/* The problem occurs whenever we finish the last layer and traverse to the prev one */
template <typename T>
class MaxPQExplicitLink {
private:

    /* Node class  */
    class Node {
    public:
        Node* prev = NULL;
        Node* nextLeft = NULL;
        Node* nextRight = NULL;
        T item;
    };

    /* Heap operations, we only swap the value not the link */
    void exch(Node* node1, Node* node2, string debug) {
        /* cout << debug << endl; */
        T temp = node1->item;
        node1->item = node2->item;
        node2->item = temp;
    }

    void swim(Node* currentNode) {
        Node* traverse = currentNode;
        while (traverse->prev != NULL) {
            if (traverse->item > traverse->prev->item) {
                exch(traverse, traverse->prev, "during swim");
                traverse = traverse->prev;
            } else break;
        }
    }

    void sink(Node* first) {
        Node* currentNode = first;
        while (currentNode->nextLeft != NULL) {
            Node* traverse = currentNode->nextLeft;
            if (currentNode->nextRight != NULL) {
                if (currentNode->nextRight->item > traverse->item) 
                    traverse = currentNode->nextRight;
            }
            if (currentNode->item >= traverse->item) break;
            exch(currentNode, traverse, "during sink");
            currentNode = traverse;
        }
    }

    int N = 0;
    Node* head;

public:

    friend ostream& operator << (ostream& os, MaxPQExplicitLink<T> maxPQ) {
        while (!maxPQ.isEmpty())
            os << maxPQ.delMax() << " ";
        return os;
    }

    MaxPQExplicitLink() = default;

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    /* total: ~ 2 logN */
    void insert(T item) {
        ++N;
        Node* temp = new Node;
        temp->item = item;
        if (N == 1) {
            head = temp;
            return;
        /* THE PROBLEM might lie in where we are creating the link from the insert */
        } else if (N == 2) {
            head->nextLeft = temp;
            temp->prev = head;
            /* cout << "N2 triggered" << endl; */
            if (temp->item > head->item) exch(head, temp, "during insert1");
            return;
        } else if (N == 3) {
            head->nextRight = temp;
            temp->prev = head;
            /* cout << "N3 triggered" << endl; */
            if (temp->item > head->item) exch(head, temp, "during insert2");
            return;
        }
        Node* traverse = head;
        Stack<int> stackInt;
        /* ~ logN, this calculation only works with binary-tree structure, thus we set i > 3 (minimum binary tree structure is 3) */
        for (int i = N; i  > 3; i = i/2) stackInt.push(i/2);
        /* ~ logN */
        while (!stackInt.isEmpty()) {
            if (stackInt.pop() % 2 == 1) traverse = traverse->nextRight;
            else                      traverse = traverse->nextLeft;
        }
        if (N  % 2 == 1) traverse->nextRight = temp;
        else             traverse->nextLeft = temp;
        temp->prev = traverse;
        swim(temp);
    }

    T delMax() {
        if (N == 0) throw out_of_range("Underflow!");
        if (N == 1) {
            T item = head->item;
            head = NULL;
            N--;
            return item;
        }
        T item = head->item;
        Node* traverse = head;
        Stack<int> stackInt;

        for (int i = N; i  > 3; i = i/2)  {
            /* cout << "i: " << i << endl; */
            stackInt.push(i/2);
        }

        while (!stackInt.isEmpty()) {
            /* cout << "stack is not empty" << endl; */
            if (stackInt.pop() % 2 == 0) traverse = traverse->nextLeft;
            else                         traverse = traverse->nextRight;
        }

        /* cout << "execute until here" << endl; */
        if (N % 2 == 1) {
            Node* oldLast = traverse->nextRight;
            /* cout << "size: " << N << "del N%2 == 1 triggered" << endl; */
            exch(head, oldLast, "during delMax 1");
            traverse->nextRight = NULL;
        } else {
            Node* oldLast = traverse->nextLeft;
            /* cout << "size: " << N << "del N%2 == 0 triggered" << endl; */
            exch(head, oldLast, "during delMax 2");
            traverse->nextLeft = NULL;
        }

        N--;
        sink(head);
        return item;
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    MaxPQExplicitLink<int> maxPQ;
    vector<int> vec;

    for (int i = 0; i < N; i++) 
        vec.push_back(randomUniformDistribution(0, N-1));

    cout << "The random array is: ";

    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    for (int i = 0; i < vec.size(); i++) 
        maxPQ.insert(vec[i]);

    cout << maxPQ << endl;

    return 0;

}
