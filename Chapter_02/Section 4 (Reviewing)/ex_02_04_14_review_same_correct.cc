/* Relevant piece of code: */
#include <iostream>
#include <numeric>
#include <algorithm>
#include "Random.h"

T delMax() {
    T item = pq[1];
    swap(pq[1], pq[N--]);
    sink(1);
    pq[N+1] = NULL;
    return item;
}

The minimum number of items that must be exchanged is 2, in the case of after removing and swapping with the last number that we obtain the highest-value element as possible (then sink will only proceed for one time)

Similarly, the best case of removing is that we obtain the maximum key possible after swap(pq[1], pq[N--]) so that we minimize the cost of the following sink() function (this is not easy since to preserve MaxHeap structure, we have to let the leaves be smaller than the root). 

For 2 successive remove, we have to do minimum of 5 exchanges

For 3 successive remove, we have to do minimum of 8 exchanges

