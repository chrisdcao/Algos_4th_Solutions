#ifndef INVERSIONS_H
#define INVERSIONS_H

#include <iostream>
#include <numeric>
#include <vector>
#include <iomanip>

using namespace std;

class Inversions {
public:

    Inversions() {}

    static long merge(vector<int>& a, vector<int>& aux, int lo, int mid, int hi) {
        long inversions = 0;

        for (int k = lo; k <= hi; k++) aux[k] = a[k];

        cout << "The aux vector:                     ";
        for (auto s : aux)
            cout << s << " ";
        cout << endl;

        int i = lo, j = mid + 1;

        for (int k = lo; k <= hi; k++) {
            if      (i > mid) a[k] = aux[j++];
            else if (j > hi)  a[k] = aux[i++];
            else if (aux[j] < aux[i]) {
                a[k] = aux[j++];
                /* because in a sorted array number ∈ [j, hi] should be > number ∈ [i, mid]
                 * thus any number ∈ j domain that < number ∈ [i, mid] would be opposite to that of the sorted array, which accumulate the inversions number
                 
                 * CALCULATION: 
                 * nếu j < i thì toàn bộ số từ i -> mid (nơi mà giá trị đều > i) đều sẽ > j
                 * so the number of inversions = ∑ số số hạng from current_i to mid */
                inversions += mid - i + 1;
            }
            else              a[k] = aux[i++];
        }

        cout << "The vector after inserted from aux: ";
        for (auto s : a)
            cout << s << " ";
        cout << endl;

        return inversions;
    }

    static long count(vector<int>& a, vector<int>& b, vector<int>& aux, int lo, int hi) {
        long inversions = 0;
        if (hi <= lo) return 0;
        int mid = lo + (hi - lo) / 2;
        // count left
        inversions += count(a, b, aux, lo, mid);
        // count right
        inversions += count(a, b, aux, mid+1, hi);
        // count when merge
        inversions += merge(b, aux, lo, mid, hi);
        return inversions;
    }

    static long count(vector<int>& a) {
        vector<int> aux(a.size());
        vector<int> b = a;
        long inversions = count(a, b, aux, 0, a.size()-1);
        return inversions;
    }

};

#endif
