#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

class Point2D {
    
    friend class Compare;

private:

    float x, y;

public:

    friend ostream& operator << ( ostream& os, Point2D point2D ) {
        os << "(" << point2D.x << ", " << point2D.y << ")";
        return os;
    }

    Point2D() = default;

    Point2D( float x, float y ): x( x ), y( y ) {}

    friend istream& operator >> ( istream& is, Point2D& point ) {
        is >> point.x >> point.y;
        return is;
    }

    float getX() { return x; }

    float getY() { return y; }

};

class Compare {

public:

    static int byX( Point2D point1, Point2D point2 ) {
        if ( point1.x < point2.x ) return -1;
        else if ( point1.x > point2.x ) return 1;
        else return 0;
    }

    static int byY( Point2D point1, Point2D point2 ) {
        if ( point1.y < point2.y ) return -1;
        else if ( point1.y > point2.y ) return 1;
        else return 0;
    }

    static int byO( Point2D point1, Point2D point2 ) {
        if ( point1.x + point1.y < point2.x + point2.y ) return -1;
        else if ( point1.x + point1.y > point2.x + point2.y ) return 1;
        else return 0;
    }

};

int compareByDistanceToPoint( Point2D point1, Point2D point2, Point2D point3 ) {

    float minusX1 = point1.getX() - point3.getX();
    float minusX2 = point2.getX() - point3.getX();
    float minusY1 = point1.getY() - point3.getY();
    float minusY2 = point2.getY() - point3.getY();
    if ( minusX1 + minusY1 < minusX2 + minusY2 ) return -1;
    else if ( minusX1 + minusY1 > minusX2 + minusY2 ) return 1;
    else return 0;
}

int compareByAngleToPoint( Point2D point1, Point2D point2, Point2D point3 ) {

    float minusX1 = point1.getX() - point3.getX();
    float minusX2 = point2.getX() - point3.getX();
    float minusY1 = point1.getY() - point3.getY();
    float minusY2 = point2.getY() - point3.getY();
    float angle1 = atan2( minusY1, minusX1 );
    float angle2 = atan2( minusY2, minusX2 );
    if ( angle1 > angle2 ) return 1;
    else if ( angle1 < angle2 ) return -1;
    else return 0;

}

int main( int argc, char** argv ) {

    Point2D point1, point2;
    string name;

    cout << "Please input point C coordinates (x & y): "; 
    Point2D point3;
    cin >> point3;

    ifstream input( "ex_02_05_25_base_data.txt" );

    cout << endl << endl;

    while ( input >> name >> point1 >> name >> point2 ) {
        
        cout << "Based on X: Point A" << point1
             << (( Compare::byX( point1, point2 ) == -1 ) ? " < " 
                : (( Compare::byX( point1, point2 ) == 0 ) ? " == " : " > " )) 
             << " Point B" << point2 << endl;

        cout << "Based on Y: Point A" << point1
             << (( Compare::byY( point1, point2 ) == -1 ) ? " < " 
                : (( Compare::byY( point1, point2 ) == 0 ) ? " == " : " > " )) 
             << " Point B" << point2 << endl;

        cout << "Based on Distance to Origin: Point A" << point1 
             << (( Compare::byO( point1, point2 ) == -1 ) ? " < " 
                : (( Compare::byO( point1, point2 ) == 0 ) ? " == " : " > " )) 
             << " Point B" << point2 << endl;

        cout << "Based on Distance to C" << point3 << ": Point A" << point1
             << (( compareByDistanceToPoint( point1, point2, point3 ) == -1 ) ? " < " 
                : (( compareByDistanceToPoint( point1, point2, point3 ) == 0 ) ? " == " : " > " )) 
             << " Point B" << point2 << endl;

        cout << "Based on Angle with C" << point3 << ": Point A" << point1
             << (( compareByAngleToPoint( point1, point2, point3 ) == -1 ) ? " < " 
                : (( compareByAngleToPoint( point1, point2, point3 ) == 0 ) ? " == " : " > " )) 
             << " Point B" << point2 << endl;

        cout << endl;

    }

    return 0;

}
