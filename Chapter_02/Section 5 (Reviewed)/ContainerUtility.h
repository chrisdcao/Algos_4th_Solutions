#ifndef CONTAINERUTILITY_H
#define CONTAINERUTILITY_H

#include <iostream>
#include <vector>
#include <iterator>
#include <utility>
#include <iomanip>

using namespace std;

ostream& operator<<(ostream& os, vector<int>& vec) {
    for (auto s : vec)
        os << s << " ";
    return os;
}

ostream& operator<<(ostream& os, vector<string>& vec) {
    for (auto s : vec)
        os << s << " ";
    return os;
}

template <typename T>
void vecRangePrint(vector<T>& a, int lo, int hi) {
    for (int i = lo; i < hi; i++)
        cout << a[i] << " ";
    cout << endl;
}

#endif
