#include <string>
#include <iostream>
#include <vector>
#include <numeric>
#include <iomanip>
#include <filesystem>

using namespace std;

namespace fs = filesystem;

template <typename T>
class MinHeap {
private:

    void exch( vector<T>& vec, int index1, int index2 ) {
        T temp = vec[ index1 ];
        vec[ index1 ] = vec[ index2 ];
        vec[ index2 ] = temp;
    }

    vector<T> pq;

    int N;

    void sink( int k ) {
        while ( 2 * k <= N ) {
            int j = 2 * k;
            if ( j < N && pq[ j ] > pq[ j+1 ] ) j++;
            if ( pq[ k ] <= pq[ j ] ) break;
            exch( pq, k, j );
            k = j;
        }
    }
    
    void swim( int k ) {
        while ( k > 1 && pq[ k ] < pq[ k/2 ] ) {
            exch( pq, k, k/2 );
            k = k/2;
        }
    }

public:

    friend ostream& operator << ( ostream& os, MinHeap minPQ ) { 
        while ( !minPQ.isEmpty() ) 
            os << minPQ.delMin() << " ";
        return os;
    }

    MinHeap(): N( 0 ), pq( 1 ) {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert( T item ) {
        pq.push_back( item );
        swim( ++N );
    }

    T delMin() {
        T min = pq[ 1 ];
        exch( pq, 1, N-- );
        sink( 1 );
        return min;
    }

    T getMin() { return pq[ 1 ]; }

};

int main( int argc, char** argv ) {

    string path = argv[ 1 ];
    MinHeap<string> pathPQ;
    string str;
    int rbegin;

    for ( const auto& entry : fs::directory_iterator( path ) ) {
        str = entry.path();
        rbegin = str.rfind( "/" );
        str = str.substr( rbegin+1, str.size()-rbegin );
        pathPQ.insert( str );
    }

    path += "/";

    while ( !pathPQ.isEmpty() ) 
        cout << path << pathPQ.delMin() << endl;

    return 0;

}
