#include "ex_02_05_09.h"

bool containsAlpha( string input ) {
    string ref = "abcdefghjklmnopqrst";
    for ( auto s : input ) 
        for ( auto n : ref )
            if ( s == n ) return true;
    return false;
}

// client
int main( int argc, char** argv ) {

    string str;
    MinHeap< Date > datePQ;
    MinHeap< int > volumePQ;

    while ( cin >> str && !cin.eof()) { 
        if ( containsAlpha( str )) datePQ.insert( str );
        else                       volumePQ.insert( stoi( str ));
    }

    int count = 0;

    while ( !volumePQ.isEmpty() || !datePQ.isEmpty()) { 

        if ( count % 2 == 0 && !datePQ.isEmpty())
            cout << datePQ.delMin() << "  ";
        ++count;
        if ( count % 2 == 1 && !volumePQ.isEmpty()) 
            cout << volumePQ.delMin() << endl;
        ++count;
    }

    return 0;

}
