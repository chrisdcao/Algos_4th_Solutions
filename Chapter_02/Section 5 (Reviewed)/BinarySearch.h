#include <iostream>
#include <vector>

using namespace std;

template <typename T>
int BinarySearchBound( T key, vector<T>& vec, int lo, int hi ) {

    while ( lo <= hi )  {
        int mid = lo + ( hi - lo ) / 2;
        if ( key < vec[ mid ] ) hi = mid - 1;
        else if ( key > vec[ mid ] ) lo = mid + 1;
        else return mid;
    }

    return -1;

}

template <typename T>
int BinarySearch( T key, vector<T>& vec ) {
    return BinarySearchBound( key, vec, 0, vec.size()-1 );
}

template <typename T>
int BinarySearchLeft( T key, vector<T>& vec ) {

    int lo = 0;
    int hi = vec.size()-1;

    // since hi here is the index, we use '<=' instead of '<'
    while ( lo <= hi ) {
        int mid = lo + ( hi - lo ) / 2;
        if ( key > vec[ mid ] ) lo = mid + 1;
        else if ( key < vec[ mid ] || ( mid > 0 && key == vec[ mid-1 ] )) hi = mid - 1;
        else {  
            int minIndex = BinarySearchBound( key, vec, lo, mid-1 );
            if ( minIndex == -1 ) return mid;
            return minIndex;
        }
    }

    return -1;

}

template <typename T>
int BinarySearchRight( T key, vector<T>& vec ) {

    int lo = 0;
    int hi = vec.size()-1;

    // since hi here is the index, we use '<=' instead of '<'
    while ( lo <= hi ) {
        int mid = lo + ( hi - lo ) / 2;
        if ( key > vec[ mid ] || ( mid > 0 && key == vec[ mid+1 ] )) lo = mid + 1 ;
        else if ( key < vec[ mid ] ) hi = mid - 1;
        else {  
            int maxIndex = BinarySearchBound( key, vec, mid+1, hi );
            if ( maxIndex == -1 ) return mid;
            return maxIndex;
        }
    }

    return -1;

}
