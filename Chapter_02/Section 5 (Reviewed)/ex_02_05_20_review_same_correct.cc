#include <iostream>
#include <vector>
#include <map>
#include "MaxHeap.h"

using namespace std;

class IdleGap;

class Process {

    friend IdleGap;

private:

    string processName;
    float startTime;
    float finishTime;
    float workingInterval;
    
public:

    friend ostream& operator << ( ostream& os, Process process ) {
        os << "[ " << process.processName << " ]. Non-Idle Time: " << process.workingInterval;
        return os;
    }

    Process() = default;

    Process( string name, float start, float end ): processName( name ), startTime( start ), finishTime( end ) {
        workingInterval = end - start;
    }

    // Sorting by the workingInterval
    bool operator > ( Process& rhs ) { return workingInterval > rhs.workingInterval; }

    bool operator == ( Process& rhs ) { return workingInterval == rhs.workingInterval; }

    bool operator != ( Process& rhs ) { return workingInterval != rhs.workingInterval; }

    bool operator < ( Process& rhs ) { return workingInterval < rhs.workingInterval; }

    bool operator <= ( Process& rhs ) { return operator==( rhs ) || operator<( rhs ); }

    bool operator >= ( Process& rhs ) { return operator==( rhs ) || operator>( rhs ); }

    void insert( string name, float start, float end ) {
        processName = name;
        startTime = start;
        finishTime = end;
        workingInterval =  finishTime - startTime;
    }

};

class IdleGap {

    friend Process;
    
private:

    float idleGap;
    string firstName, secondName;

public:

    friend ostream& operator << ( ostream& os, IdleGap idle ) {
        os << "Process 1: [ " << idle.firstName 
           << " ]. Process 2: [ " << idle.secondName
           << " ]\nIdle Time: " << idle.idleGap;
        return os;
    }

    IdleGap() = default;

    IdleGap( Process& process1, Process& process2 ): firstName( process1.processName ), 
                                                     secondName( process2.processName ) { 
                                                         idleGap = process2.startTime - process1.finishTime; 
                                                     }

    // sort by gap time
    bool operator > ( IdleGap& rhs ) { return idleGap > rhs.idleGap; }

    bool operator < ( IdleGap& rhs ) { return idleGap < rhs.idleGap; }

    bool operator == ( IdleGap& rhs ) { return idleGap == rhs.idleGap; }

    bool operator != ( IdleGap& rhs ) { return idleGap != rhs.idleGap; }

    bool operator <= ( IdleGap& rhs ) { return operator<( rhs ) || operator==( rhs ); }

    bool operator >= ( IdleGap& rhs ) { return operator>( rhs ) || operator==( rhs ); }

};

int main( int argc, char** argv ) {

    string name;
    float start, end;
    MaxHeap<Process> intervalPQ;
    MaxHeap<IdleGap> gapPQ;

    Process lastProcess;
    Process process;

    int count = 0;

    while ( cin >> name >> start >> end ) {
        count++;
        process.insert( name, start, end );
        intervalPQ.insert( process );
        if ( count >= 2 ) {
            IdleGap gap( lastProcess, process );
            gapPQ.insert( gap );
        }
        lastProcess = process;
    }

    cout << "The largest idle time are between these 2 processes:\n" << gapPQ.getMax() << endl << endl;

    cout << "The process with longest non-idle time:\n" << intervalPQ.getMax() << endl;

    return 0;

}
