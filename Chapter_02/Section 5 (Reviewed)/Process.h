#ifndef PROCESS_H
#define PROCESS_H

#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Process {

private:

    string processName;
    int processTime;

public:

    friend ostream& operator << ( ostream& os, Process process ) {
        os << setw( 20 ) << left << process.processName << " " << process.processTime;
        return os;
    }
    
    Process() = default;

    Process( string name, int time ): processName( name ), processTime( time ) {}

    bool operator == ( Process& rhs ) { return processTime == rhs.processTime; }

    bool operator > ( Process& rhs ) { return processTime > rhs.processTime; }

    bool operator < ( Process& rhs ) { return processTime < rhs.processTime; }

    bool operator <= ( Process& rhs ) { return operator<( rhs ) || operator==( rhs ); }

    bool operator >= ( Process rhs ) { return operator>( rhs ) || operator==( rhs ); }

};

#endif
