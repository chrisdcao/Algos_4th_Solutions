#include <iostream>
#include <algorithm>
#include <chrono>
#include <string>
#include <vector>
#include <filesystem>
#include "MinHeap.h"

using namespace std;

using namespace filesystem;

// recursively calculate the size of a dir 
int getDirSize( string path ) {

    if ( !is_directory( path ) ) return file_size( path );

    int sum = 0;

    for ( const auto& entry : directory_iterator( path )) {
        if ( is_directory( entry.path() )) sum += getDirSize( entry.path() );
        else                               sum += file_size( entry.path() );
    }

    return sum;

}

template <typename TP>
std::time_t to_time_t(TP tp)
{
    using namespace std::chrono;
    auto sctp = time_point_cast<system_clock::duration>(tp - TP::clock::now()
              + system_clock::now());
    return system_clock::to_time_t(sctp);
}

class FileTime {
private:

    string name;

    file_time_type ftime;

public:

    friend ostream& operator<< ( ostream& os, FileTime file ) {
        time_t cftime = to_time_t( file.ftime );
        os << file.name << ". Date Modified: " << asctime( localtime( &cftime ) );
        return os;
    }

    FileTime() = default;

    FileTime( string name, file_time_type ftime ): name( name ), ftime( ftime ) {  }

    bool operator > ( FileTime& rhs ) { return ftime > rhs.ftime; }

    bool operator < ( FileTime& rhs ) { return ftime < rhs.ftime; }

    bool operator == ( FileTime& rhs ) { return ftime == rhs.ftime; }

    bool operator >= ( FileTime& rhs ) { return operator>( rhs ) || operator==( rhs ); }

    bool operator <= ( FileTime& rhs ) { return operator<( rhs ) || operator==( rhs ); }

    void init( string fname, file_time_type time ) {
        this->name = fname;
        this->ftime = time;
    }

};


class File {
private:

    string name;

    int size;

public:

    friend ostream& operator << ( ostream& os, File file ) {
        os << file.name << ". Size: " << file.size << " bytes";
        return os;
    }

    File( string name, int size ): name( name ), size( size ) {  }

    File() = default;

    void init( string fname, int fsize ) {
        this->name = fname;
        this->size = fsize;
    }

    string getName() { return name; }

    int getSize() { return size; }

    bool operator > ( File& rhs ) { return size > rhs.size; }

    bool operator < ( File& rhs ) { return size < rhs.size; }

    bool operator == ( File& rhs ) { return size == rhs.size; }

    bool operator >= ( File& rhs ) { return operator>( rhs ) || operator==( rhs ); }

    bool operator <= ( File& rhs ) { return operator<( rhs ) || operator==( rhs ); }

};

string extractFileName( string path ) {

    int rbegin = path.rfind( "/" );

    string name = path.substr( rbegin+1, path.size()-rbegin);

    return name;

}

int main( int argc, char** argv ) {
    
    if ( argc == 3 ) {

        string sortingType = argv[ 1 ];

        string path = argv[ 2 ];

        if ( sortingType == "-t" ) { 
            // sort by time
            MinHeap<FileTime> filePQ;

            FileTime file;
            
            file_time_type time;

            for ( const auto& entry : directory_iterator( path ) ) {
                time = last_write_time( entry.path() );
                file.init( extractFileName( entry.path() ), time );
                filePQ.insert( file );
            }

            cout << "[ Sorted file ( by time ) in '" << path << "' ]:\n" << endl;

            printPQ( filePQ );

        }
        else if ( sortingType == "-n" ) {
            // sort by name
            MinHeap<string> pathPQ;

            string str;

            int rbegin;

            for ( const auto& entry : directory_iterator( path ) ) {
                str = entry.path();
                rbegin = str.rfind( "/" );
                str = str.substr( rbegin+1, str.size()-rbegin );
                pathPQ.insert( str );
            }

            cout << "[ Sorted file ( by name ) in '" << path << "' ]:\n" << endl;

            while ( !pathPQ.isEmpty() ) 
                cout << pathPQ.delMin() << endl << endl;

        }
        else { 
            // sort by size
            MinHeap<File> filePQ;

            File file;

            for ( const auto& entry : directory_iterator( path )) {
                if ( is_directory( entry ) ) {
                    // cout << getDirSize( entry.path() ) << endl;
                    file.init( extractFileName( entry.path() ), getDirSize( entry.path() ) );
                }
                else {
                    // cout << file_size( entry.path() ) << endl;
                    file.init( extractFileName( entry.path() ), file_size( entry.path() ) );
                }
                filePQ.insert( file );
            }

            cout << "[ Sorted file ( by size ) in '" << path << "' ]:\n" << endl;

            printPQExtraGap( filePQ );

        }

    } else {
        cout << "Please add either: '-t' ( to sort by last modified ), '-n' ( to sort by name ), or '-s' ( to sort by size ) before the absolute path!" << endl;
        return -1;
    }
    return 0;

}
