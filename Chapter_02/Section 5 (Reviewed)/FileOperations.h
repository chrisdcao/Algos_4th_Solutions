#include <iostream>
#include <string>
#include <vector>

using namespace std;

using namespace filesystem;

// recursively calculate the size of a dir 
int getDirSize( string path ) {

    if ( !is_directory( path ) ) return file_size( path );

    int sum = 0;

    for ( const auto& entry : directory_iterator( path )) {
        if ( is_directory( entry.path() )) sum += getDirSize( entry.path() );
        else                               sum += file_size( entry.path() );
    }

    return sum;

}
