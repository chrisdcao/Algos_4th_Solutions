#include "Process.h"

using namespace std;

// longest time rule -> maxHeap
// maxHeap: Parent > Child
template <typename T>
class MaxHeap {
private:

    void exch( vector<T>& pq, int index1, int index2 ) {
        T temp = pq[ index1 ];
        pq[ index1 ] = pq[ index2 ];
        pq[ index2 ] = temp;
    }

    int N;
    vector<T> pq;

    void sink( int k ) {
        while ( 2 * k <= N ) {
            int j = 2 * k;
            if ( j < N && pq[ j ] < pq[ j+1 ] ) j++;
            if ( pq[ k ] >= pq[ j ]) break;
            exch( pq, k, j );
            k = j;
        }
    }

    void swim( int k ) {
        while ( k > 1 && pq[ k ] > pq[ k/2 ]) {
            exch( pq, k, k/2 );
            k = k/2;
        }
    }

public:

    friend ostream& operator << ( ostream& os, MaxHeap<T> maxPQ ) {
        while ( !maxPQ.isEmpty())
            os << maxPQ.delMax() << endl;
        return os;
    }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    MaxHeap() = default;

    // using fix-size implementation because the exercise say we will get an info of the size during run time
    MaxHeap( int M ): N( 0 ), pq( M+1 ) {}

    void insert( T item ) {
        pq[ ++N ] = item;
        sink( N );
    }

    T delMax() {
        T max = pq[ 1 ];
        exch( pq, 1, N-- );
        sink( 1 );
        return max;
    }

};

int main( int argc, char** argv ) {

    int M = stoi( argv[ 1 ]);
    string processName, title1, title2;
    int processTime;
    MaxHeap<Process> processPQ( M );

    cin >> title1 >> title2;

    while ( M ) {
        cin >> processName >> processTime;
        Process processInfo( processName, processTime );
        processPQ.insert( processInfo );
        M--;
    }

    cout << processPQ;

    return 0;

}
