#include <iostream>
#include <iomanip>
#include <vector>
#include <string>

using namespace std;

// we will have to sort the process from lowest to highest processing time
// we use minHeap for this
template <typename T>
class MinHeap {
private:

    void exch( vector<T>& vec, int index1, int index2 ) {
        T item = vec[ index1 ];
        vec[ index1 ]  = vec[ index2 ];
        vec[ index2 ] = item;
    }

    int N;
    vector<T> pq;

    void sink( int k ) {
        while ( 2 * k <= N ) {
            int j = 2 * k;
            if ( j < N && pq[ j ] > pq[ j+1 ]) j++;
            if ( pq[ k ] <= pq[ j ]) break;
            exch( pq, k, j );
            k = j;
        }
    }

    void swim( int k ) {
        while ( k > 1 && pq[ k/2 ] > pq[ k ] ) {
            exch( pq, k, k/2 );
            k = k/2;
        }
    }

public:

    friend ostream& operator << ( ostream& os, MinHeap<T> minPQ ) {
        while ( !minPQ.isEmpty() )
            os << minPQ.delMin() << endl;
        return os;
    }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    MinHeap(): N( 0 ), pq( 1 ) {  }

    void insert( T item ) {
        pq.push_back( item );
        swim( ++N );
    }

    T delMin() {
        T min = pq[ 1 ];
        exch( pq, 1, N-- );
        sink( 1 );
        return min;
    }

};

class Process {

private:

    string processName;
    int processTime;

public:

    friend ostream& operator << ( ostream& os, Process process ) {
        os << setw( 20 ) << left << process.processName << " " << process.processTime;
        return os;
    }
    
    Process() = default;

    Process( string name, int time ): processName( name ), processTime( time ) {}

    bool operator == ( Process& rhs ) { return processTime == rhs.processTime; }

    bool operator > ( Process& rhs ) { return processTime > rhs.processTime; }

    bool operator < ( Process& rhs ) { return processTime < rhs.processTime; }

    bool operator <= ( Process& rhs ) { return operator<( rhs ) || operator==( rhs ); }

    bool operator >= ( Process rhs ) { return operator>( rhs ) || operator==( rhs ); }

};

int main( int argc, char** argv )  {

    string processName;
    int processTime;
    MinHeap< Process > processPQ;

    cin >> processName >> processName;

    while( cin >> processName >> processTime ) {
        Process processInfo( processName, processTime );
        processPQ.insert( processInfo );
    }

    cout << processPQ;

    return 0;

}
