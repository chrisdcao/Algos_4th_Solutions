#include <iostream>
#include <string>

using namespace std;

vector<string> split( string str, string delimiter ) {

    vector<string> stringVec;
    string copy = str + delimiter;

    while ( true ) { int end = copy.find_first_of( delimiter );
                     if ( end == -1 ) break;
                     string substring = copy.substr( 0, end );
                     stringVec.push_back( substring );
                     copy.erase( 0, end+1 ); }

    return stringVec;

}

class Email {
private:

    class Suffix {
    private:

        vector<string> splitSuffix;

    public:

        friend ostream& operator << ( ostream& os, Suffix suffix ) {
            int i = 0;
            for ( /*  */; i < suffix.splitSuffix.size()-1; i++ )
                os << suffix.splitSuffix[ i ] << ".";
            os << suffix.splitSuffix[ i ];
            return os;
        }

        Suffix() = default;

        Suffix( string input ): splitSuffix( split( input, "." )) {}

        bool operator>( Suffix& rhs ) {
            int N = 0;
            if ( splitSuffix.size() < rhs.splitSuffix.size() ) N = splitSuffix.size();
            else N = rhs.splitSuffix.size();
            for ( int i = 0; i < N; i++ ) {
                if ( splitSuffix[ i ] > rhs.splitSuffix[ i ]) return true;
                if ( splitSuffix[ i ] < rhs.splitSuffix[ i ]) return false;
            }
            if ( splitSuffix.size() > rhs.splitSuffix.size()) return true;
            else return false;
        }

        bool operator == ( Suffix& rhs ) {
            if ( splitSuffix.size() != rhs.splitSuffix.size()) return false;
            else {
                for ( int i = 0; i < splitSuffix.size(); i++ ) {
                    if ( splitSuffix[ i ] != rhs.splitSuffix[ i ]) return false;
                }
            }
            return true;
        }
        
        bool operator!=( Suffix& rhs ) { return !( operator==( rhs )); }

        bool operator<( Suffix& rhs ) { return !operator==( rhs ) && !operator>( rhs ); }

        bool operator>=( Suffix& rhs ) { return operator==( rhs ) || operator>( rhs ); }

        bool operator<=( Suffix& rhs ) { return operator==( rhs ) || operator<( rhs ); }

        Suffix& operator=( Suffix& rhs ) {
            splitSuffix = rhs.splitSuffix;
            return *this;
        }

    };
        
    string senderName;
    Suffix suffix;

public:

    friend ostream& operator << ( ostream& os, Email email ) {
        cout << email.senderName << "@" << email.suffix;
        return os;
    }

    Email() = default;

    Email( string input ) {
        int end = input.find_first_of( "@" );
        senderName = input.substr( 0, end );
        Suffix temp( input.substr( end+1, input.size() - end ));
        suffix = temp;
    }
    // email are totally compare on suffix, rather than human name, thus, binary search will return right suffixes
    bool operator < ( Email& rhs ) { return suffix < rhs.suffix; }
    
    bool operator == ( Email& rhs ) { return suffix == rhs.suffix; }

    bool operator > ( Email& rhs ) { return suffix > rhs.suffix; }

    bool operator != ( Email& rhs ) { return !( operator==( rhs )); }

    bool operator >=( Email& rhs ) { return operator==( rhs ) || operator>( rhs ); }

    bool operator <=( Email& rhs ) { return operator==( rhs ) || operator<( rhs ); }

    Email& operator = ( Email& rhs ) {
         senderName = rhs.senderName;
         suffix = rhs.suffix;
         return *this;
    }

};

template <typename T>
int BinarySearchBound( T key, vector<T> vec, int lo, int hi) {
    while ( lo <= hi ) {
        int mid = lo + ( hi - lo ) / 2;
        if ( key > vec[ mid ] ) lo = mid + 1;
        else if ( key < vec[ mid ] ) hi = mid - 1;
        else return mid;
    }
    return -1;
}

template <typename T>
int BinarySearchLeft( T key, vector<T> vec ) {
    int lo = 0; 
    int hi = vec.size()-1;
    while ( lo <= hi ) {
        int mid = lo + ( hi - lo ) / 2;
        if ( key > vec[ mid ] ) lo = mid + 1;
        else if ( key < vec[ mid ] || ( mid > 0 && key == vec[ mid-1 ])) hi = mid - 1;
        else { 
            int minIndex = BinarySearchBound( key, vec, lo, mid-1 );
            if ( minIndex == -1 ) return mid;
            else return minIndex;
        }
    }
    return -1;
}

template <typename T>
int BinarySearchRight( T key, vector<T> vec ) {
    int lo = 0; 
    int hi = vec.size()-1;
    while ( lo <= hi ) {
        int mid = lo + ( hi - lo ) / 2;
        if ( key < vec[ mid ] ) hi = mid - 1;
        else if ( key > vec[ mid ] || ( mid > 0 && key == vec[ mid+1 ])) lo = mid + 1;
        else {
            int maxIndex = BinarySearchBound( key, vec, mid+1, hi );
            if ( maxIndex == -1 ) return mid;
            else return maxIndex;
        }
    }
    return -1;
}
