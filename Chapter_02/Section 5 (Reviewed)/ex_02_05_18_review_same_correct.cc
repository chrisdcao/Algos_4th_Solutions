#include <iostream>
#include <algorithm>
#include <string>
#include <numeric>
#include <iomanip>
#include "Sorting_Algorithms.h"
#include "Random.h"

using namespace std;

template <typename Key> 
class Data {

    template <typename T> friend class ForceStable;

private:

    Key key;
    int index;

public:

    Data() = default;

    Data(Key key, int index): key(key), index(index) {}

    Key getKey() { return key; }

    int getIndex() { return index; }

    bool operator > (Data& rhs) { return key > rhs.key; }

    bool operator < (Data& rhs) { return key < rhs.key; }

    bool operator ==  (Data& rhs) { return key == rhs.key; }

    bool operator >= (Data& rhs) { return operator == (rhs) || operator > (rhs); }

    bool operator <= (Data& rhs) { return operator == (rhs) || operator < (rhs); }

};

template <typename T>
class ForceStable {

private:

    static void exchange(vector<T>& vec, int i, int j) {
        auto temp = vec[i].getKey();
        int temp_i = vec[i].getIndex();
        vec[i].key = vec[j].key;
        vec[i].index = vec[j].index;
        vec[j].key = temp;
        vec[j].index = temp_i;
    }

    static void insertionSortIndex(vector<T>& vec, int lo, int hi) {
        //  quick corner case
        if (lo >= hi) return;
        int N = hi - lo;
        for (int i = lo; i < hi; i++) {
            for (int j = i; j > lo && vec[j].index < vec[j-1].index; j--)
                exchange(vec, j, j-1);
        }
    }

public:

    static void sort(vector<T>& vec) {

        //  sort the keys by some Unstable sorting algorithm
        Selection<T>::sort(vec);

        int begin = 0, end = 1;

        //  restore the key to their stable positions after the sort
        //  Stable mean within a range of repeated Keys, the indices must be sorted
        while (end < vec.size()) {
            while (vec[begin].getKey() == vec[end].getKey()) end++;
            insertionSortIndex(vec, begin, end);
            begin = end;
            end = begin + 1;
        }

    }

};

int main(int argc, char** argv) {

    vector<Data<int>> vec;

    for (int i = 0; i < 20; i++) {
        Data<int> data(randomUniformDistribution(1,10), i);
        vec.push_back(data);
    }

    ForceStable<Data<int>>::sort(vec);

    cout << setw(16) << "The values: ";
    for (auto s : vec)
        cout << setw(2) << s.getKey() << " ";
    cout << endl;

    cout << setw(16) << "The indices: ";
    for (auto s : vec)
        cout << setw(2) << s.getIndex() << " ";
    cout << endl;

    return 0;

}
