#ifndef MAXHEAP_H
#define MAXHEAP_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;

template <typename T>
class MaxHeap {
private:

    void exch( vector<T>& vec, int index1, int index2 ) {
        T temp = vec[ index1 ];
        vec[ index1 ] = vec[ index2 ];
        vec[ index2 ] = temp;
    }

    vector<T> pq;

    int N;

    void sink( int k ) {
        while ( 2 * k <= N ) {
            int j = 2 * k;
            if ( j < N && pq[ j ] < pq[ j + 1 ]) j++;
            if ( pq[ k ] >= pq[ j ] ) break;
            exch( pq, j ,k );
            k = j;
        }
    }

    void swim( int k ) {
        while ( k > 1 && pq[ k/2 ] < pq[ k ] ) {
            exch( pq, k/2, k );
            k = k/2;
        }
    }

public:

    friend ostream& operator << ( ostream& os, MaxHeap& maxPQ ) {
        while ( !maxPQ.isEmpty() )
            os << maxPQ.delMax() << " ";
        return os;
    }

    MaxHeap(): N( 0 ), pq( 1 ) {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert( T item ) {
        pq.push_back( item );
        swim( N++ );
    }

    T getMax() { return pq[ 1 ]; }

    T delMax() {
        T max = pq[ 1 ];
        exch( pq, 1, N-- );
        sink( 1 );
        return max;
    }

};

#endif
