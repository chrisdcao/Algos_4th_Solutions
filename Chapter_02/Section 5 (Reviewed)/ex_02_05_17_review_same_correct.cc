#include <iostream>
#include <vector>
#include <numeric>
#include <iomanip>
#include <string>
#include "Random.h"
#include "Sorting_Algorithms.h"

using namespace std;

template <typename Value>
class Data {

public:

    int index;

    Value value;

    int getIndex() { return index; }

    Value getValue() { return value; }

    Data& insert(Value value, int index) {
        this->value = value;
        this->index = index;
        return *this;
    }

    bool operator > (Data& rhs) { return this->value > rhs.value; }

    bool operator < (Data& rhs) { return this->value < rhs.value; }

    bool operator == (Data& rhs) { return this->value == rhs.value; }

    bool operator <= (Data& rhs) { return operator==(rhs) || operator<(rhs); }

    bool operator >= (Data& rhs) { return operator==(rhs) || operator>(rhs); }

};

template <typename Type>
bool check(vector<Type> array, string sort_mode) {

    vector<Data<Type>> vecData;
    Data<Type> data;

    for (int i = 0; i < array.size(); i++) {
        data.insert(array[i], i);
        vecData.push_back(data);
    }

    if (sort_mode == "selection sort" || sort_mode == "selection")
        Selection<Data<Type>>::sort(vecData);
    else if (sort_mode == "insertion sort" || sort_mode == "insertion")
        Insertion<Data<Type>>::sort(vecData);
    else if (sort_mode == "merge sort" || sort_mode == "merge")
        Merge<Data<Type>>::sort(vecData);
    else if (sort_mode == "quick sort" || sort_mode == "quick")
        Quick<Data<Type>>::sort(vecData);
    else if (sort_mode == "quick sort 3 way" || sort_mode == "quick3way")
        Quick<Data<Type>>::sort3way(vecData);
    else if (sort_mode == "from lib")
        sort(vecData.begin(), vecData.end());
    else throw runtime_error("check(): Have no sort_mode <" + sort_mode + ">");

    Type lastValue = vecData[0].getValue();
    int lastIndex = vecData[0].getIndex();

    //  we iterate the sorted array,
    for (int i = 1; i < vecData.size(); i++) {
        Type currentValue = vecData[i].getValue();
        int currentIndex = vecData[i].getIndex();

        //  then when meet a sequence of repeated keys
        if (currentValue == lastValue)
            //  we check if their indices are ascending (since order is preserved)
            if (currentIndex < lastIndex) 
                //  if 'no' then it's unstable
                return false;

        lastValue = currentValue;
        lastIndex = currentIndex;
    }

    return true;

}

int main(int argc, char** argv) {

    cout << "Please input sort_mode (insertion, selection, merge, quick, quick3way): ";

    string sort_mode; cin >> sort_mode;

    vector<int> vec = { 1, 2, 7, 2, 9, 2, 2, 11, 12, 3, 3, 31, 3, 13 };

    uniformShuffle(vec);

    // You can change different sorting mode HERE including: "selection sort", "merge sort", "insertion sort", "quick sort", "quick sort 3"
    cout << sort_mode << " is " <<  ((check(vec, sort_mode) == 1) ? "Stable Sort" : "Unstable Sort") << endl;

    return 0;

}
