#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include "ContainerUtility.h"

using namespace std;

template <typename T>
vector<T> removeDupSorted(vector<T>& a) {
    int N = a.size();
    int ptr1 = 0;
    vector<T> removed;

    while (ptr1 < N) {
        while (a[ptr1] == a[ptr1+1]) ptr1++;
        removed.push_back(a[ptr1++]);
    }

    return removed; 
}

template <typename T>
class Heap {
private: 

    static void exch(vector<T>& pq, int k, int N) {
        T temp = pq[k];
        pq[k] = pq[N];
        pq[N] = temp;
    }

    static void sink(vector<T>& pq, int k, int N) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j] < pq[j+1]) j++;
            if (pq[k] >= pq[j]) break;
            swap(pq[k], pq[j]);
            k = j;
        }
    }

public:

    static void sort(vector<T>& a) {
        if (a.size() <= 1) return;
        int N = a.size();
        a.push_back("dummy");

        for (int i = N-1; i > 0; i--)
            a[i+1] = a[i];

        for (int k = N/2; k >= 1; k--) 
            sink(a, k, N);

        while (N > 1) {
            exch(a, 1, N--);
            sink(a, 1, N);
        }
    }

};

vector<string> deup(vector<string>& a) {
    Heap<string>::sort(a);
    return removeDupSorted<string>(a);
}

int main(int argc, char** argv) {

    vector<string> vec;
    string str;

    while (cin >> str && !cin.eof())
        vec.push_back(str);

    cout << setw(30) << "The input vector is: " << vec << endl;

    vector<string> result = deup(vec);

    cout << setw(30) << "The result is: " << result << endl;

    return 0;

}

