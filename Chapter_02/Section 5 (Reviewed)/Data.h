#ifndef DATA_H
#define DATA_H

#include <iostream>
#include <string>
#include <numeric>
#include <iomanip>

template <typename Value>
class Data {

    template <typename T> friend class MinPQ;

public:

    int idx;

    Value val;

    int index() { return idx; }

    Value value() { return val; }

    Data() = default;

    Data(Value value, int index): val(value), idx(index) {}

    Data& insert(Value value, int index) {
        this->val = value;
        this->idx = index;
        return *this;
    }

    bool operator > (Data& rhs) { return this->val > rhs.val; }

    bool operator < (Data& rhs) { return this->val < rhs.val; }

    bool operator == (Data& rhs) { return this->val == rhs.val; }

    bool operator <= (Data& rhs) { return operator==(rhs) || operator<(rhs); }

    bool operator >= (Data& rhs) { return operator==(rhs) || operator>(rhs); }

    void operator=(Data& rhs) {
        val = rhs.val;
        idx = rhs.idx;
    }

};

#endif
