#include <iostream>
#include <iomanip>
#include <utility>
#include <map>
#include <vector>
#include <string>

using namespace std;
// since they are sorted from small to large, we use MinHeap
// minHeap: parent < child
template <typename T>
class MinHeap {
private:

    void exch( vector<T>& vec, int index1, int index2 ) {
        T item = vec[ index1 ];
        vec[ index1 ] = vec[ index2 ];
        vec[ index2 ] = item;
    }

    vector<T> pq;
    int N;

    void sink( int k ) {
        while ( 2 * k <= N ) {
            int j = 2 * k;
            if ( j < N && pq[ j ] > pq[ j+1 ]) j++;
            if ( pq[ k ] <= pq[ j ]) break;
            exch( pq, k, j );
            k = j;
        }
    }

    void swim( int k ) {
        while ( k > 1 && pq[ k/2 ] > pq[ k ]) {
            exch( pq, k, k/2 );
            k = k/2;
        }
    }

public:

    friend ostream& operator << ( ostream& os, MinHeap<T> minPQ ) {
        while ( !minPQ.isEmpty()) 
            os << minPQ.delMin() << " ";
        return os;
    }

    friend void printPQ( MinHeap<T> minPQ ) {
        while ( !minPQ.isEmpty()) 
            cout << minPQ.delMin() << endl;
    }

    MinHeap(): N( 0 ), pq( 1 ) {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert( T item ) {
        pq.push_back( item );
        swim( ++N );
    }

    T delMin() {
        T min = pq[ 1 ];
        exch( pq, 1, N--);
        sink( 1 );
        return min;
    }

};

map<string, int> MONTH = {
    { "Jan", 1 },
    { "Feb", 2 },
    { "Mar", 3 },
    { "Apr", 4 },
    { "May", 5 },
    { "Jun", 6 },
    { "Jul", 7 },
    { "Aug", 8 },
    { "Sep", 9 },
    { "Oct", 10 },
    { "Nov", 11 },
    { "Dec", 12 },
};

class Date {
private:

    class Month {
    private:
        string monthName;
        int monthInt;
    public:

        friend ostream& operator << ( ostream& os, Month month ) {
            os << month.monthName;
            return os;
        }

        Month() = default;

        Month( string& m ) {
            for ( auto s : MONTH )
                if ( s.first == m ) {
                    monthInt = s.second;
                    break;
                }
            monthName = m;
        }

        bool operator > ( Month& rhs ) {
            return monthInt > rhs.monthInt;
        }

        bool operator < ( Month& rhs ) {
            return monthInt < rhs.monthInt;
        }

        bool operator == ( Month& rhs ) {
            return monthInt == rhs.monthInt;
        }

        bool operator != ( Month& rhs ) {
            return !( monthInt == rhs.monthInt );
        }

        bool operator >= ( Month& rhs ) {
            return operator>( rhs ) || operator==( rhs );
        }

        bool operator <= ( Month& rhs ) {
            return operator<( rhs ) || operator==( rhs );
        }

        Month& operator=( Month rhs ) {
            monthName = rhs.monthName;
            monthInt = rhs.monthInt;
            return *this;
        }

    };

    int day;
    Month month;
    int year;

public:

    friend ostream& operator << ( ostream& os, Date fullDate ) {
        if ( fullDate.year >= 10 )
            os << setw( 2 ) << right << fullDate.day << " " << fullDate.month << " " << setw( 2 ) << left << fullDate.year;
        else
            os << setw( 2 ) << right << fullDate.day << " " << fullDate.month << " " << "0" << fullDate.year;
        return os;
    }

    Date() = default;

    Date( string str ) {  
        int begin1 = str.find_first_of( "-" );
        int begin2 = str.rfind( "-" );
        day = stoi( str.substr( 0, begin1 ));
        year = stoi( str.substr( begin2 + 1, 2 ));
        string monthInfo = str.substr( begin1 + 1 , 3 );
        Month temp( monthInfo );
        month = temp;
    }

    bool operator > ( Date& rhs ) {
        if ( year == rhs.year ) { 
            if ( month == rhs.month ) {
                if ( day == rhs.day ) return false;
                else if ( day < rhs.day ) return false;
                else return true;
            }
            else if ( month < rhs.month ) return false;
            else return true;
        }
        else if ( year < rhs.year ) return false;
        else return true;
    }

    bool operator == ( Date& rhs ) {
        return year == rhs.year && day == rhs.day && month == rhs.month;
    }

    bool operator != ( Date& rhs ) {
        return !( year == rhs.year && day == rhs.day && month == rhs.month );
    }

    bool operator < ( Date& rhs ) {
        return !operator==( rhs ) && !operator>( rhs );
    }

    bool operator >= ( Date& rhs ) {
        return operator>( rhs ) || operator==( rhs );
    }

    bool operator <= ( Date& rhs ) {
        return operator<( rhs ) || operator==( rhs );
    }

    Date operator =( const Date& rhs ) {
        day = rhs.day;
        month = rhs.month;
        year = rhs.year;
        return *this;
    }

};
