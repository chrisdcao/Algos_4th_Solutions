#include <iostream>
#include <map>
#include "MinHeap.h"

using namespace std;

// creating an algorithm that sort the words not according to natural order but by custom order ( which is stated in the exercise requirements )
// assuming that the order given is from low -> high, we create this custom mapping table
map< char, int > customAlpha = {
    { 'R', 1 }, { 'W', 2 }, { 'Q', 3 }, { 'O', 4 }, { 'J', 5 }, { 'M', 6 }, { 'V', 7 }, { 'A', 8 }, { 'H', 9 }, { 'B', 10 }, { 'S', 11 }, { 'G', 12 }, { 'Z', 13 }, { 'X', 14 },
    { 'N', 15 }, { 'T', 16 }, { 'C', 17 }, { 'C', 18 }, { 'I', 19 }, { 'E', 20 }, { 'K', 21 }, { 'U', 22 }, { 'P', 23 }, { 'D', 24 }, { 'Y', 25 }, { 'F', 26 }, { 'L', 27 }
};

class California {
private:

    char firstChar;
    int charInt;
    string fullString;

public:

    friend ostream& operator << ( ostream& os, California cali ) {
        os << cali.fullString;
        return os;
    }

    California() = default;

    California( string input ) {
        fullString = input;
        firstChar = input[ 0 ]; 
        for ( auto s : customAlpha ) {
            if ( firstChar == s.first ) {
                charInt = s.second;
                break;
            }
        }
    }

    bool operator > ( California& rhs ) { return charInt > rhs.charInt; }

    bool operator==( California& rhs ) { return charInt == rhs.charInt; }

    bool operator != ( California& rhs ) { return charInt != rhs.charInt; }

    bool operator < ( California& rhs ) { return charInt < rhs.charInt; }

    bool operator >= ( California& rhs ) { return charInt > rhs.charInt || charInt == rhs.charInt; }

    bool operator <= ( California& rhs ) { return charInt < rhs.charInt || charInt == rhs.charInt; }

};

int main( int argc, char** argv ) {

    string str;
    MinHeap< California > caliPQ;
    while ( cin >> str ) {
        California california( str );
        caliPQ.insert( california );
    }

    printPQ( caliPQ );

    return 0;

}
