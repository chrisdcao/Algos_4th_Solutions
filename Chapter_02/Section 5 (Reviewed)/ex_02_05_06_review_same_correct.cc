#include <iostream>
#include <numeric>
#include <vector>
#include <random>
#include <algorithm>

using namespace std;

template <typename T>
class Select {
private:

    static void exchange(vector<T>& a, int i, int j) {
        T temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    static int partition(vector<T>& a, int lo, int hi) {
        int i = lo, j = hi + 1;
        T pivot = a[lo];
        while (true) {
            while (a[i++] < pivot) if (i == hi) break;
            while (a[--j] > pivot) if (j == lo) break;
            if (i >= j) break;
            exchange(a, i, j);
        }

        exchange(a, j, lo);
        return j;
    }

public:

    // select() recursive version
    static T select(vector<T>& a, int k, int lo, int hi) {
        if (lo >= hi) return a[k];

        int j = partition(a, lo, hi);

        if      (j > k) return select(a, k, lo, j-1);
        else if (j < k) return select(a, k, j+1, hi);
        else            return a[k];
    }

    // selecting the k smallest elements in a[]
    static T select(vector<T> a, int k) {
        random_shuffle(a.begin(), a.end());
        return select(a, k, 0, a.size() - 1);
    }

};

int main(int argc, char** argv) {

    vector<int> vec = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    cout << Select<int>::select(vec, 2) << endl;

    return 0;

}
