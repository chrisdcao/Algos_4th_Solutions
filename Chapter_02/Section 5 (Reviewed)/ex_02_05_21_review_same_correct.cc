#include <iostream>
#include <typeinfo>
#include <vector>

using namespace std;

// since the comparison is just the same as default C++ STL vector, we don't have to overload them
// can use them straight out of the box
template <typename T>
class MultiD {
private:

    static void exch( vector<T>& vec, int index1, int index2 ) {
        T temp = vec[ index1 ];
        vec[ index1 ] = vec[ index2 ];
        vec[ index2 ] = temp;
    }

    static void sink ( vector<T>& pq, int k, int N ) {
        while ( 2 * k <= N ) {
            int j = 2 * k;
            if ( j < N && pq[ j ] < pq[ j+1 ]) j++;
            if ( pq[ k ] >= pq[ j ] ) break;
            exch( pq, k, j );
            k = j;
        }
    }

public:
    // this is only used for vector that has starting point = 1 ( not 0 )
    static void sort( vector<T>& vec, int size ) {
        int N = size;
        for ( int k = N/2; k > 1; k-- ) 
            sink( vec, k, N );
        while ( N > 1 ) {
            exch( vec, 1, N-- );
            sink( vec, 1, N );
        }
    }

};

int main( int argc, char** argv ) {
    // test on 2D vectors
    vector<vector<int>> multi2DVec = { { 0 }, { 1,2,3 }, { 0,2,3 }, { 4,5,6 } };

    MultiD<vector<int>>::sort( multi2DVec, multi2DVec.size()-1 );

    int count = 0;
    cout << "Test on 2D vector:\n";
    for ( auto s : multi2DVec ) {
        cout << "{ ";
        if ( count != 0 ) {
            for ( auto x : s ) {
                if ( count != 0 )
                    cout << x << " ";
            }
        }
        if ( count != 0 )
            cout << ( ( count != multi2DVec.size()-1 ) ? "}\n" : "} }\n" );
        count ++;
    }

    cout << endl;

    // test on 3D vectors
    cout << "Test on 3D vector:\n";
    vector<vector<vector<int>>> multi3DVec = { { { 0 } }, 
                                             { { 1, 2 }, { 2, -2 }, { 3 } }, 
                                             { { 0,3 }, { 4,2 }, { 3, 0 } }, 
                                             { { 4, 4 }, { 5, 1 }, { 6, 3 } } 
                                           };

    MultiD<vector<vector<int>>>::sort( multi3DVec, multi3DVec.size()-1 );

    count = 0;
    cout << "{ ";
    for ( auto s : multi3DVec ) {
        if ( count != 0 )
            cout << (( count == 1 ) ? "{ " : "\n{ ");
        for ( auto t : s ) {
            if ( count != 0 )
                cout << "{ ";
            for ( auto u : t ) {
                if ( count != 0 )
                    cout << u << " ";
            }
            if ( count != 0 ) cout << "} ";
        }
        if ( count != 0 ) cout << "} ";
        count++;
    }
    cout << "} " << endl;

}
