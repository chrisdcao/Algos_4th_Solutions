#include <iostream>
#include <string>
#include <algorithm>
#include "ex_02_05_09.h"

using namespace std;

class Version {
private:

    int versionHeader;
    int versionBody;
    int versionTail;

public:

    friend ostream& operator << ( ostream& os, Version fullVersion ) {
        os << fullVersion.versionHeader << "." << fullVersion.versionBody << "." << fullVersion.versionTail;
        return os;
    }

    Version() = default;

    Version( string fullNumber ) {
        int lend = fullNumber.find_first_of( "." );
        int rbegin = fullNumber.rfind( "." );
        versionHeader = stoi( fullNumber.substr( 0, lend ));
        versionBody = stoi( fullNumber.substr( lend+1, rbegin-lend ));
        versionTail = stoi( fullNumber.substr( rbegin+1, fullNumber.size() - rbegin ));
    }

    bool operator > ( Version& rhs ) {
        if ( versionHeader == rhs.versionHeader ) {
            if ( versionBody == rhs.versionBody ) {
                if ( versionTail == rhs.versionTail ) return false;
                else if ( versionTail < rhs.versionTail ) return false;
                else return true;
            }
            else if ( versionBody < rhs.versionBody ) return false;
            else return true;
        }
        else if ( versionHeader < rhs.versionHeader ) return false;
        else return true;
    }

    bool operator == ( Version& rhs ) {
        return versionHeader == rhs.versionHeader && versionBody == rhs.versionBody && versionTail == rhs.versionTail;
    }

    bool operator < ( Version& rhs ) {
        return !operator==( rhs ) && !operator>( rhs );
    }

    bool operator <= ( Version& rhs ) {
        return operator==( rhs ) || operator<( rhs );
    }

    bool operator >= ( Version& rhs ) {
        return operator==( rhs ) || operator>( rhs );
    }

};

// test client
int main( int argc, char** argv ) {

    string str; 
    MinHeap< Version > versionPQ;

    while ( cin >> str )  {
        Version input( str );
        versionPQ.insert( input );
    }
    
    printPQ( versionPQ );

    return 0;

}
