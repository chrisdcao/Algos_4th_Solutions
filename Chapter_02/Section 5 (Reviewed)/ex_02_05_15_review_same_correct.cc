#include "ex_02_05_09.h"
#include "ex_02_05_15.h"
#include <climits>

using namespace std;

// I just have to sort the mails into a  PQ ( by doing that, I have a clusters of mails having similar suffix ) then store them back onto a vector ( or an array ) by deleting each elements from the PQ and gradually assign them to the vector. This will not cause extra demand for memory. By the end of this operations, PQ should be freed/deleted and we only have the array left
// total Sorting Cost + read Data: ~ O( N )
// space complexity: ~ O( N )

// since this array is sorted, I can use binary search operation
// -> when the user want to forge an email from certain domain( suffix ), then I just binary-search for the chunk with equal suffix value and recommend possible non-spam email-domains ( by also taking all of its neighbors )
// total Search Cost: ~ log( N )

// demo client
int main( int argc, char** argv ) {

    string input;
    // using Heap to read and sort data
    MinHeap< Email > emailPQ;
    vector< Email > vec;

    while ( cin >> input ) {
        Email email( input );
        emailPQ.insert( email );
    }

    // transfer sorted data to new container
    while ( !emailPQ.isEmpty() )
        vec.push_back( emailPQ.delMin() );


    //  input mail to forge from
    string mailForge = "thinh@students.mq.edu.au";

    cout << "____ input changable in main(), not _in.txt file ____ \n" << endl;
    cout << "Mail address to forge from: \n[ " << mailForge << " ]\n" << endl;
    Email key( mailForge );

    cout << "The list of similar-domain emails you can send to:\n{" << endl;

    int begin = BinarySearchLeft( key, vec );
    int end = BinarySearchRight( key, vec );

    for ( int i = begin; i <= end; i++ ) 
        cout << vec[ i ] << endl;

    cout << "}" << endl;

    return 0;

}
