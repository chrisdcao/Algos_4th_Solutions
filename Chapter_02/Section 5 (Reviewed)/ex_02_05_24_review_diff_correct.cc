#include "Random.h"
#include "Sorting_Algorithms.h"
#include "Data.h"

using namespace std;

/*  MinPQ: parent < child
 *  estimated time complexity: ~ O(N) */
template <typename T> 
class MinPQ {
private:

    /* The Data class is PURELY FOR INSPECTION (to show that the order is preserved)
     * This Stable PQ implementation CAN WORK WITH all types with or w/o index embedded in the class (like built-in types, for example) */
    vector<Data<T>> pq;
    int N = 0;

    /* instead of directly swapping between 2 places, we are doing sthg like insertion after making space, where the space is made by moving aside the elements to then insert the wanted node into the 'spare' space
     * we do it bi-directionally: 'child -> parent' and 'parent -> child'
     * this preserve the index order of both child and parent node family-of-similar-values after the insertion */
    void exchange_preserve_order(int lo, int hi) {
        int parent_idx = lo, child_idx = hi;

        Data<T> child_node  = pq[hi],
                parent_node = pq[lo];

        T parent_value = pq[lo].value(),
          child_value  = pq[hi].value();

        int one_after_first = -1;

        bool child_duplicate = false;

        for (int i = lo+1; i < hi; i++) {
            if (pq[i].value() == parent_value) {
                one_after_first = i; break;
            }
        }

        for (int i = hi-1; i > lo; i--) {
            if (pq[i].value() == child_value) {
                child_duplicate = true; break;
            }
        }

        int last_idx = parent_idx;

        if (child_duplicate) {
            // start from parent->child, moving all values of child twd parent
            for (int i = parent_idx; i <= child_idx; i++) {
                if (pq[i].value() == child_value) {
                    pq[last_idx] = pq[i];
                    last_idx = i;
                }
            }
        } 
        // child has no dups in range [lo, hi] -> assign parent straight = child
        else pq[parent_idx] = child_node;

        if (one_after_first != -1) {
            last_idx = child_idx;
            // current_state: parent = child w smallest idx, no more original parent node
            // start from child->parent, moving all value of parent
            for (int i = child_idx; i > parent_idx; i--) {
                if (pq[i].value() == parent_value) {
                    pq[last_idx] = pq[i];
                    last_idx = i;
                }
            }
            pq[one_after_first] = parent_node;
        }
        // if parent has no duplicate value in range [lo, hi] -> assign child straight = parent
        else pq[child_idx] = parent_node;
    }

    void sink(int k) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j] > pq[j+1]) j++;
            if (pq[k] <= pq[j]) break;
            exchange_preserve_order(k, j);
            k = j;
        }
    }

    void swim(int k) {
        while (k > 1 && pq[k/2] > pq[k]) {
            exchange_preserve_order(k/2, k);
            k = k/2;
        }
    }

public:

    MinPQ(int maxN): pq(maxN+1) {}

    virtual ~MinPQ() {}

    int size() { return N; }

    bool is_empty() { return N == 0; }

    void insert(T item, int i) {
        Data<T> data(item, i);
        pq[++N] = data;
        swim(N);
    }
    
    T get_min() { return pq[1]; }

    // return the whole data object so that we can see how the order is preserved later
    Data<T> del_min() {
        Data<T> data = pq[1];

        /* for this exchange(1, N) we have the same idea of moving and insertion, but will be single-directional
         * the value of the first item will be deleted, so the moving-and-insertion sẽ chỉ diễn ra 1 chiều (đó là chiều từ child lên node 1 (parent)) chứ không diễn ra theo chiều ngược lại: từ node 1 (parent) đổ xuống child, bởi chúng ta sẽ delete parent node with their current order/arrangement. Thus no need to re-arrange parent values & alter the parent similar-value nodes positions. */
        Data<T> key = pq[N];
        int min_index = N;
        for (int i = 1; i <= N; i++) {
            if (pq[i] == key) {
                min_index = i; break;
            }
        }
        pq[1] = pq[min_index];
        int last_index = min_index;
        for (int i = min_index; i <= N; i++) {
            if (pq[i] == key) {
                pq[last_index] =  pq[i];
                last_index = i;
            }
        }
        pq[N] = data;
        /* end of exchange(1, N) */

        --N;
        sink(1);
        return data;
    }

    void debug_print() {
        for (auto s : pq)
            cout << s.value() << " " << s.index() << endl;
    }

    // printing both the value and index for inspection
    friend ostream& operator << (ostream& os, MinPQ<T> minPQ) {
        while (!minPQ.is_empty()) {
            Data<T> data = minPQ.del_min();
            os << left << setw(2) << data.value() << " " << data.index() << endl;
        }
        return os;
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);

    MinPQ<int> minPQ(N);

    cout << "Original value and index: " << endl;
    for (int i = 1; i <= N; i++) {
        auto value = randomUniformDistribution(0, N/2);
        cout << left << setw(2) << value << " " << i << endl;
        minPQ.insert(value, i);
    }

    cout << "The PQ (stable-sorted) is: " << endl;

    cout << minPQ << endl;

    return 0;

}
