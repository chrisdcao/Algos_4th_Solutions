#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

class StrF {

private:

    string str;

    int frequency = 0;

public:

    StrF() = default;

    StrF( string str ): str( str ), frequency( 1 ) {}

    StrF( string str, int f ): str( str ), frequency( f ) {}

    friend istream& operator >> ( istream& is, StrF& saf ) {
        is >> saf.str;
        saf.frequency = 1;
        return is;
    }

    friend ostream& operator << ( ostream& os, StrF saf ) { 
        os << "String: " << saf.str;
        os << ". Frequency: " << fixed << setfill( '0' ) << setw( 2 ) << saf.frequency << endl;
        return os;
    }

    string getString() { return str; }

    int getFrequency() { return frequency; }

    bool operator<( StrF& rhs ) { return frequency < rhs.frequency; }

    bool operator>( StrF& rhs ) { return frequency > rhs.frequency; }

    bool operator==( StrF& rhs ) { return frequency == rhs.frequency; }

    bool operator!=( StrF& rhs ) { return !operator==( rhs ); }

    bool operator>=( StrF& rhs ) { return operator==( rhs ) || operator>( rhs ); }

    bool operator<=( StrF& rhs ) { return operator==( rhs ) || operator<( rhs ); }

};

// Parent > child
template <typename Key> class DynamicIndexMaxPQ {

private:

    vector< int > pq;
    vector< int > qp;
    vector< Key > keys;
    int N = 0;

    bool greater( int i, int j ) {
        return keys[ pq[ i ] ] > keys[ pq[ j ] ];
    }

    void exchange( int i, int j ) {
        int temp = pq[ i ]; 
        pq[ i ] = pq[ j ];
        pq[ j ] = temp;
        qp[ pq[ i ] ] = i;
        qp[ pq[ j ] ] = j;
    }

    void sink( int k ) {
        while ( 2 * k <= N ) {
            int j = 2 * k;
            if ( j < N && greater( j+1, j ) ) j++;
            if ( ! greater( j, k ) ) break;
            exchange( k, j );
            k = j;
        }
    }

    void swim( int k ) {
        while ( k > 1 && greater( k, k/2 ) ) {
            exchange( k/2, k );
            k /= 2;
        }
    }

public:
    
    DynamicIndexMaxPQ(): pq( 1 ), qp( 1 ), keys( 1 ), N( 0 ) {}

    Key& operator[] ( int index ) { return keys[ index ]; }

    bool contains( int k ) { return qp[ k ] != -1; }

    bool isEmpty() { return size() == 0; }

    int size() { return N; }

    int getMaxIndex() { return pq[ 1 ]; }

    Key getMax() { return keys[ pq[ 1 ] ]; }

    void insert( int k, Key key ) {
        N++;
        keys.push_back( key );
        pq.push_back( k );
        qp.push_back( N );
        swim( N );
    }

    void deleteKey( int k ) {
        if ( ! contains( k ) ) throw invalid_argument( "deleteKey(): invalid index" );
        int index = qp[ k ];
        exchange( index, N-- );
        swim( index );
        sink( index );
        pq[ N+1 ] = -1;
        qp[ k ] = -1;
    }

    int delMax() {
        int max = pq[ 1 ];
        exchange( 1, N-- );
        sink( 1 );
        pq[ N+1 ] = -1;
        qp[ max ] = -1;
        return max;
    }

    void change( int k, Key key ) {
        if ( ! contains( k ) ) throw invalid_argument( "change(): invalid index" );
        keys[ k ] = key;
        swim( qp[ k ] );
        sink( qp[ k ] );
    }

    friend ostream& operator <<( ostream& os, const DynamicIndexMaxPQ< Key >& MaxPQ ) {
        os << "\nSorted keys:\n";
        // bởi vì size này là size real của vector ( method riêng của class vector, bởi vậy nó đã tính cả one extra space chúng ta thêm đoạn đầu )
        // nên không thể dùng <= mà phải dùng < 
        for ( int i = 1; i < MaxPQ.pq.size(); ++i ) {
            os << i << " : " << MaxPQ.keys[ MaxPQ.pq[ i ] ] << endl;
        }

        os << "\nOriginal keys:\n";
        for ( int i = 1; i < MaxPQ.keys.size(); i++ ) {
            os << i << " : " << MaxPQ.keys[ i ] << endl;
        }
        return os;
    }

};

int main ( int argc, char** argv ) {

    DynamicIndexMaxPQ< StrF > maxPQ;
    StrF strf; 
    int count = 0;

    while ( cin >> strf ) {
        bool flag = false;
        if ( ! maxPQ.isEmpty() ) {
            for ( int i = 1; i <= maxPQ.size(); i++ ) { 
                if ( strf.getString() == maxPQ[ i ].getString() ) { 
                    StrF temp( maxPQ[ i ].getString(), maxPQ[ i ].getFrequency()+1 );
                    maxPQ.change( i, temp ); flag = true; break;
                }
            }
            if ( ! flag ) maxPQ.insert( ++count, strf );
        } else maxPQ.insert( ++count, strf );
    }

    cout << maxPQ << endl;

    return 0;

}
