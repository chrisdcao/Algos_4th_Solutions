#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "BinarySearch.h"
#include "MinHeap.h"

#define matchCommand BinarySearch<Command>
#define matchCommandLeft BinarySearchLeft<Command>
#define matchCommandRight BinarySearchRight<Command>
#define Simulator main

using namespace std;

class Command {
private:

    string name; 
    string id;
    float price;
    int amount;

public:

    Command() = default;

    Command( string name, string id, float price, int amount ): name( name ), id( id ), price( price ), amount( amount ) {  }

    Command( float price, int amount ): name( "None" ), id( "None" ), price( price ), amount( amount ) {}

    bool operator > ( Command& rhs ) { return price > rhs.price; }

    bool operator < ( Command& rhs ) { return price < rhs.price; }

    bool operator == ( Command& rhs ) { return price == rhs.price; }

    bool operator >= ( Command& rhs ) { return operator==( rhs ) || operator>( rhs ); }
   
    bool operator <= ( Command& rhs ) { return operator==( rhs ) || operator<( rhs ); }

    bool operator != ( Command& rhs ) { return !( operator==( rhs )); }

    friend istream& operator >> ( istream& is, Command& command ) {
        is >> command.name >> command.id >> command.price >> command.amount;
        return is;
    }

    Command& operator=( Command& rhs ) {
        name = rhs.name;
        id = rhs.id;
        price = rhs.price;
        amount = rhs.amount;
        return *this;
    }

    string getID() { return id; }
    float getPrice() { return price; }

    friend void printBuy( Command buyCommand ) {
        cout << "Buyer name: " << buyCommand.name << " Buyer ID: " << buyCommand.id << " Buy At: " << buyCommand.price << " Buy Amount: " << buyCommand.amount;
    }

    friend void printSell( Command SellCommand ) {
        cout << "Seller name: " << SellCommand.name << " | Seller ID: " << SellCommand.id << " | Sell At: " << SellCommand.price << " | Total Stock in Sale: " << SellCommand.amount;
    }

};

int Simulator( int argc, char** argv ) {

    ifstream input( "ex_02_05_22_database_sellCommand.txt" );
    Command sellCommand;
    MinHeap< Command > sellPQ;
    vector< Command > sortedSell;

    while ( input >> sellCommand ) 
        sellPQ.insert( sellCommand );

    while ( !sellPQ.isEmpty() ) 
        sortedSell.push_back( sellPQ.delMin() );

    cout << "Please input one/all buy command(s) to find matching sell command(s): " << endl;
    cout << "( or input manually with syntax form: name ID price amount )\n"  << endl;

    Command buyCommand;
    MinHeap< Command > buyPQ;

    while ( cin >> buyCommand ) 
        buyPQ.insert( buyCommand );

    if ( buyPQ.size() == 1 ) {
        cout << "Matching Sellers for Buy Command ID: " << buyCommand.getID() << " \n[ ";
        int index = matchCommand( buyCommand, sortedSell );
        printSell( sortedSell[ index ] );
        cout << " ]\n";
        return 0;
    }

    vector< Command > sortedBuy;

    while ( !buyPQ.isEmpty() )
        sortedBuy.push_back( buyPQ.delMin() );

    for ( int i = 0; i < sortedBuy.size(); i++ ) {
        int index1 = matchCommandLeft( sortedBuy[ i ], sortedSell );
        int index2 = matchCommandRight( sortedBuy[ i ], sortedSell );
        cout << "Matching Sellers for Buy Command ID: " << sortedBuy[ i ].getID() << endl;
        if ( index1 == -1 && index2 == -1 ) {
            cout << "No sellers at price: " << sortedBuy[ i ].getPrice() << endl;
        } else {
            for ( int j = index1; j <= index2; j++ ) {
                printSell( sortedSell[ j ]);
                cout << endl;
            }
        }
        cout << endl;
    }

    return 0;

}
