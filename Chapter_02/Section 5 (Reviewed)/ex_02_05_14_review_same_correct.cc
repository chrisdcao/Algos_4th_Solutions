#include "ex_02_05_09.h"
#include "ex_02_05_14.h"

using namespace std;

int main( int argc, char** argv )  {

    string input;
    MinHeap< Domain > domainPQ;
    
    while ( cin >> input && !cin.eof() ) {
        Domain domain( input );
        domainPQ.insert( domain );
    }
    // print the domain in reverse order for easy review and debug
    printPQ( domainPQ );

    return 0;

}
