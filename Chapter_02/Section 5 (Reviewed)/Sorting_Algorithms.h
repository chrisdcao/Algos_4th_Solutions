#ifndef SORTING_ALGORITHMS_H
#define SORTING_ALGORITHMS_H

#include <iostream>
#include <string>
#include <numeric>
#include <iomanip>
#include <random>
#include "Random.h"
#include <algorithm>

using namespace std;

template <typename T>
class Merge {
private:

    static void merge(vector<T>& org, vector<T>& aux, int low, int mid, int high) {
        for (int k = low ; k <= high; ++k) aux[k] = org[k];

        int i = low;
        int j = mid+1;
        for (int k = low; k <= high; ++k) {
            if (i > mid) org[k] = aux[j++];
            else if (j > high) org[k] = aux[i++];
            else if (aux[i] > aux[j]) org[k] = aux[j++];
            else org[k] = aux[i++];
        }
    }

public:

    static void sort(vector<T>& org) {
        int N = org.size();
        vector<T> aux(N);
        for (int len = 1; len < N; len *= 2) {
            for (int low = 0; low < N; low += len + len) {
                int mid = low + len - 1;
                int high = min(low + len + len - 1, N - 1);
                merge(org, aux, low, mid, high);
            }
        }
    }

    static void sort(vector<T>& org, int lo, int hi) {
        int N = hi - lo + 1;
        vector<T> aux(N);
        for (int len = 1; len < N; len *= 2) {
            for (int low = lo; low < hi; low += len + len) {
                int mid = low + len - 1;
                int high = min(low + len + len - 1, hi);
                merge(org, aux, low, mid, high);
            }
        }
    }

};

template <typename T>
class Quick {

private:

    static int partition(vector<T>& vec, int lo, int hi) {
        int i = lo, j = hi + 1; 
        T v = vec[lo];
        while (true) {
            while (vec[++i] < v) if (i == hi) break;
            while (vec[--j] > v) if (j == lo) break;
            if (i >= j) break;
            swap(vec[i], vec[j]);
        }
        swap(vec[j], vec[lo]);
        return j;
    }

    static void sort(vector<T>& vec, int lo, int hi) {
        if (hi <= lo) return;
        int j = partition(vec,lo,hi);
        sort(vec, lo, j-1);
        sort(vec, j+1, hi);
    }

    static void sort3way(vector<T>& vec, int lo, int hi) {
        int lt = lo, i = lo+1, rt = hi;
        T v = vec[lo];
        while (i < rt) {
            if      (vec[i] < v) swap(vec[lt++], vec[i++]);
            else if (vec[i] > v) swap(vec[i], vec[rt--]);
            else               i++;
        }
        sort3way(vec, lo, lt-1);
        sort3way(vec, rt+1, hi);
    }

public:

    static void sort(vector<T>& vec) {
        sort(vec, 0, vec.size()-1);
    }
    
    static void sort3way(vector<T>& vec) {
        sort(vec, 0, vec.size()-1);
    }

};

template <typename T>
class Selection {

public:

    static void sort(vector<T>& vec) {
        for (int i = 0; i < vec.size(); i++) {
            int iMin = i;
            for (int j = i+1; j < vec.size(); j++) {
                if (vec[j] < vec[iMin]) iMin = j;
            }
            swap(vec[i], vec[iMin]);
        }
    }

};

template <typename T>
class Insertion {

public:

    static void sort(vector<T>& vec) {
        for (int i = 0; i < vec.size(); i++) {
            for (int j = i; j > 0 && vec[j] < vec[j-1]; j--)
                swap(vec[j], vec[j-1]);
        }
    }

    static void sort(vector<T>& vec, int lo, int hi) {
        for (int i = lo; i < hi; i++) {
            for (int j = i; j > 0 && vec[j] < vec[j-1]; j--)
                swap(vec[j], vec[j-1]);
        }
    }

};

#endif
