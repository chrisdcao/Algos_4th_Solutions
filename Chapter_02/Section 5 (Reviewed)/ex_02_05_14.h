#include <iostream>
#include <vector>

using namespace std;

class Domain {
private:

    string domainHead;
    string domainTail;
    string domainBody;

public:

    friend ostream& operator << ( ostream& os, Domain domain ) {
        os << domain.domainTail << "." << domain.domainBody << "." << domain.domainHead;
        return os;
    }

    Domain() = default;

    Domain( string input ) {
        int lend = input.find_first_of( "." );
        int rbegin = input.rfind( "." );
        domainHead = input.substr( 0, lend );
        domainBody = input.substr( lend+1, rbegin-lend-1 );
        domainTail = input.substr( rbegin+1, input.size()-rbegin );
    }

    bool operator == ( Domain& rhs ) {
        return domainTail == rhs.domainTail && domainHead == rhs.domainHead && domainBody == rhs.domainBody;
    }

    bool operator > ( Domain& rhs ) {
        if ( domainTail == rhs.domainTail ) {
            if ( domainBody == rhs.domainBody ) {
                if ( domainHead == rhs.domainHead ) return false;
                else if ( domainHead < rhs.domainHead ) return false;
                else return true;
            }
            else if ( domainBody < rhs.domainBody ) return false;
            else return true;
        }
        else if ( domainTail < rhs.domainTail ) return false;
        else return true;
    }

    bool operator < ( Domain& rhs ) {
        return !operator==( rhs ) && !operator>( rhs );
    }

    bool operator >=( Domain& rhs ) {
        return operator>( rhs ) || operator==( rhs );
    }

    bool operator <=( Domain& rhs ) {
        return operator<( rhs ) || operator==( rhs );
    }

};
