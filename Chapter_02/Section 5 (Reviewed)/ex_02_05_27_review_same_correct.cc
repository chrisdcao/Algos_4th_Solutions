#include <iostream>
#include <numeric>
#include <string>
#include <iomanip>
#include "Random.h"

using namespace std;

//Only sort the indices (like the idea of IndexMinPQ)
template <typename T>
class Insertion {

public:

    //  pass a copy of the array to the function for sorting the indices
    static vector<int> indirectSort(vector<T> vec) {
        vector<int> indices(vec.size());
        iota(indices.begin(), indices.end(), 0);

        for (int i = 0; i < vec.size(); i++) {
            for (int j = i; j > 0 && vec[j] < vec[j-1]; j--)  {
                swap(vec[j], vec[j-1]);
                swap(indices[j], indices[j-1]);
            }
        }

        return indices;
    }

};

int main(int argc, char** argv) {

    vector<int> vec;

    for (int i = 0; i < 9; i++)
        vec.push_back(randomUniformDistribution(1, 9));

    vector<int> indices = Insertion<int>::indirectSort(vec);

    cout << endl;
    cout << left << setw(35) << "The array after sort is: ";
    for (auto s : vec)
        cout << setw(2) << s << " ";
    cout << "(preserved)" << endl << endl;

    cout << left << setw(35) << "The sorted indices array: ";
    for (auto s : indices)
        cout << setw(2) << s << " ";
    cout << endl << endl;

    cout << left << setw(35) << "Print arr using sorted indices:";
    for (auto s : indices)
        cout << setw(2) << vec[s] << " ";
    cout << "(sorted)" << endl << endl;

    return 0;

}
