#include <iostream>
#include <iomanip>
#include "Random.h"

using namespace std;

// for size <= 43 using insertion sort will be fastest ( most likely be the test case ), thus I will use it
void sortMatrix2DCol( int** matrix, int rowSize, int colSize ) {

    for ( int k = 0 ; k < colSize; k++ ) {
        for ( int i = 0; i < rowSize; i++ ) {
            for ( int j = i; j > 0 && matrix[ j ][ k ] < matrix[ j-1 ][ k ]; j-- )
                swap( matrix[ j ][ k ], matrix[ j-1 ][ k ] );
        }
    }

}

void sortMatrix2DRow( int** matrix, int rowSize, int colSize ) {

    for ( int k = 0; k < rowSize; k++) {
        for ( int i = 0; i < colSize; i++ ) {
            for ( int j = i; j > 0 && matrix[ k ][ j ] < matrix[ k ][ j-1 ]; j-- )
                swap( matrix[ k ][ j ], matrix[ k ][ j-1 ] );
        }
    }

}

// I'm going to justify the answer by sorting column first then row, then print out and see if we still have to column sorted 
int main( int argc, char** argv ) {

    int rowSize, colSize;

    cout << "Please input row and col size: "; cin >> rowSize >> colSize;

    // initializing the matrix
    int **matrix;
    matrix = new int*[rowSize];

    for (int i = 0; i < rowSize; ++i) 
      matrix[i] = new int[colSize];

    cout << "Our randomized matrix:\n" << endl;

    for ( int i = 0; i < rowSize; i++ ) {
        for ( int j = 0; j < colSize; j++ ) {
            matrix[ i ][ j ] = randomUniformDistribution( 0, 12 );
            cout << setw( 2 ) << matrix[ i ][ j ] << " ";
        }
        cout << endl << endl;
    }
    // sort each column first
    sortMatrix2DCol( matrix, rowSize, colSize );

    cout << "Our current matrix after Sorting On Each Col: " << endl;

    for ( int i = 0; i < rowSize; i++ ) {
        for ( int j = 0; j < colSize; j++ ) 
            cout << setw( 2 ) << matrix[ i ][ j ] << " ";
        cout << endl << endl;
    }

    // then sort each row
    sortMatrix2DRow( matrix, rowSize, colSize );

    cout << "Our current matrix after Sorting On Each Row: " << endl;

    for ( int i = 0; i < rowSize; i++ ) {
        for ( int j = 0; j < colSize; j++ ) 
            cout << setw( 2 ) << matrix[ i ][ j ] << " ";
        cout << endl << endl;
    }

    return 0;

}

// By the end, the result is that the columns are still sorted.
