#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

int main(int argc, char** argv) {

    string str;
    vector<string> vec;
    vector<string> result;

    while (cin >> str && !cin.eof()) 
        vec.push_back(str);

    for (int i = 0; i < vec.size(); i++) {
        for (int j = i+1; j < vec.size(); j++) {
            result.push_back(vec[i] + vec[j]);
        }
    }

    for (auto s : result)
        cout << s << " ";
    cout << endl;

    return 0;

}
