Insertion sort loop:
    
for (int i = 0; i < N; i++) {
    for (int j = i; j > 0 && a[j] < a[j-1]; j--)     // this loop break as soon as the condition is checked (for indentical array)
                                                     // making the cost to be ~ O(1)
        swap(a[j], a[j-1]);
}
// Cost for identical array: ~ O(N) (linear)

Selection sort loop:

for (int i = 0; i < N; i++) {
    int iMin = i;
    for (int j = i + 1; j < N; j++) {               // but the loop keep looping ~ O(N)
//                                                          ^
//                                                          |
//                                                          |
//                                                          |
        if (a[iMin] > a[j])                         // the condition break
            iMin = j;
    }
    swap(a[iMin],a[i]);
}
// Cost for identical array: ~ O(N ^ 2) (quad)



//---> Insertion Sort is faster than Selection Sort for identical arrays
