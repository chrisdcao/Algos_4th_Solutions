#ifndef RANDOM_H
#define RANDOM_H

#include <iostream>
#include <random>

using namespace std;

template<typename T>
T randomUniform(T range_from, T range_to) {
    std::random_device                  rand_dev;
    std::mt19937                        generator(rand_dev());
    std::uniform_real_distribution<T>   distr(range_from, range_to);
    return distr(generator);
}

int randomUniformDistribution(int range_from, int range_to) {
    random_device rand_dev;
    mt19937 generator(rand_dev());
    uniform_int_distribution<int> distr(range_from, range_to);
    return distr(generator);
}

#endif
