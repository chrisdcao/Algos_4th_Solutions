#include "Transaction.h"
#include <fstream>
#include <iomanip>

using namespace std;
 
template <typename T> 
class Shell {
public:
    static void sort(vector<T>& a) {
        int N = a.size();
        int h = 1;
        while (h < N/3) h = h * 3 + 1;
        while (h >= 1)  {
            for (int i = h; i < N; i++) {
                for (int j = i; j >= h && a[j] < a[j-h]; j -= h)
                    swap(a[j], a[j-h]);
            }
            h /= 3;
        }
    }
};

int main() {
    vector<Transaction> transactions;
    ifstream input("ex_02_01_22_in.txt");
    Transaction transaction;
    // initializing the transactions vector
    while (!input.eof() && getline(input, transaction)) 
        transactions.push_back(transaction);
    // sort
    Shell<Transaction>::sort(transactions);
    // print the result
    for (auto s : transactions)
        cout << s << endl;
}
