#include <iostream>
#include <map>
#include <iomanip>
#include <cstdio>
#include <vector>
#include <cstdio>
#include <utility>
#include <random>
#include <numeric>
#include <algorithm>
#include <cstring>
#include <string>

using namespace std;
using Card = pair<string, int>;

// Assume that: spades < hearts < clubs < diamonds
int compareCards(string suitA, string suitB) {
    if (suitA != suitB) {
        if (suitA == "spades") return -1;
        else if (suitA == "diamonds") return 1;
        else if (suitA == "hearts") {
            if (suitB == "spades") return 1;
            else return -1;
        } else if (suitA == "clubs")  {
            if (suitB == "diamonds") return -1;
            else return 1;
        }
    }
    return 0;
}

bool operator < (Card& card1, Card& card2) {
    return (compareCards(card1.first,card2.first) == 0 && card1.second < card2.second) || (compareCards(card1.first, card2.first) < 0);
}

bool operator == (Card& card1, Card& card2) {
    return (compareCards(card1.first,card2.first) == 0) &&  (card1.second == card2.second);
}

bool operator > (Card& card1, Card& card2) {
    return !(card1 < card2) && !(card1 == card2);
}

ostream& operator << (ostream& os, Card& card) {
    os << setw(2) << card.second << "(" << card.first << ")";
    return os;
}
