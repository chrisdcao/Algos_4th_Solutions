#include <iostream>
#include <string>
#include <vector>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include "Date.h"

using namespace std;

class Transaction {
private:
    string who;
    string transaction;
    Date when;
    double amount;

public:
    Transaction(string ai, Date thoigian, double soluong): who(ai), when(thoigian), amount(soluong) {}

    Transaction(string giaodich) { transaction = giaodich; }

    string getWho() { return who; }

    Date getWhen() { return when; }

    double getAmount() { return amount; }

    string toString() { return who + " (" + when.toString() + "): " + to_string(amount) + "$"; }

    friend ostream& operator << (ostream& os, Transaction& transac) {
        os << transac.toString();
        return os;
    }

    bool equals(Transaction that) {
        return ((this->getWho() == that.getWho()) && (this->getWhen() == that.getWhen()) && (this->getAmount() == that.getAmount()));
    }

    int compareTo(Transaction that) {
        if (this->getAmount() > that.getAmount()) return 1;
        else if (this->getAmount() < that.getAmount()) return -1;
        else return 0;
    }

    int hashCode() {
        int j = transaction.find_first_of("#");
        int i = transaction.find_first_of(" ", j + 1);
        string mathang = transaction.substr(j + 1, i);
        stringstream convert(mathang);
        int x = 0;
        convert >> x;
        return x;
    }

    bool operator < (Transaction& rhs) { return this->amount < rhs.amount; }
    bool operator > (Transaction& rhs) { return this->amount > rhs.amount; }
    bool operator == (Transaction& rhs) { return this->amount == rhs.amount; }
    bool operator != (Transaction& rhs) { return this->amount != rhs.amount; }
};

int main () {
    Date thinhdate(12, 03, 2020);
    Date cuongdate(12, 03, 2020);

    Transaction transObj("thinh", thinhdate, 1298.91723);
    Transaction transObj1("thinh", thinhdate, 1298.989221);
    Transaction transObj2("cuong", cuongdate, 1722.231231);
    Transaction hashtest("Transaction ID: #100112339");

    cout << hashtest.hashCode() << endl;
    cout << transObj.toString() << endl;
    cout << transObj << endl;
    cout << transObj.getWho() << " " << transObj.getAmount() << endl;
    cout << transObj.getAmount() << endl;

    cout << ((transObj1 > transObj2) ? "greater" : (transObj1 == transObj2) ? "equal" : "less")  << endl;

    return 0;
}
