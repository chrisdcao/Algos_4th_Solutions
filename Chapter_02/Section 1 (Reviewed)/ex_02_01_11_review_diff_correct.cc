#include <iostream>
#include <vector>
#include "Random.h"

using namespace std;

template <typename T>
class Shell {
public:
    static void sort(vector<T>& a) {
        int N = a.size();
        int h = 1;

        vector<int> incrementSequence;
        while (h < N/3) {
            incrementSequence.push_back(h);
            h = h * 3 + 1;
        }

        for (auto rIterator = incrementSequence.rbegin(); rIterator != incrementSequence.rend(); rIterator++) {
            int gap = *rIterator;
            for (int i = gap; i < N; i++) {
                for (int j = i; j >= gap && a[j] < a[j - gap]; j -= gap)
                    swap(a[j], a[j-gap]);
            }
        }
    }
};

int main(int argc, char** argv) {
    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0,N-1));
    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;
    Shell<int>::sort(vec);
    cout << "\nThe array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;
}
