class Shell {
    static void sort(vector<T>& a) {
        int N = a.size();
        int h = 1;
        while (h < N/3) h = h * 3 + 1;
        while (h >= 1) {
            for (int i = h; i < N; i++) {
                for (int j = i; j >= h && a[j] < a[j-h]; j -= h)
                    swap(a[j], a[j-h]);
            }
            h /= 3;
        }
    }
};
// THE BEST CASE FOR SHELLSORT IS WHEN THE ARRAY IS ALREADY SORTED
// BECAUSE THE INNER LOOP WILL INSTANTLY BREAK AFTER THE FIRST COMPARISION
// Depending on the h-width, it could be log2 or log3 etc.. of N
// TOTAL COST = N * logN

