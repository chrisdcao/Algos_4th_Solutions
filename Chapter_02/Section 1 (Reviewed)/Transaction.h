#include <iostream>
#include <string>
#include <vector>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include "Date.h"

using namespace std;

class Transaction {
private:
    string who;
    string transaction;
    Date when;
    double amount;

public:
    Transaction(): who(string()), transaction(string()), when(Date()), amount(0.00) {}
    Transaction(string ai, Date thoigian, double soluong): who(ai), when(thoigian), amount(soluong) {}

    Transaction(string giaodich) { transaction = giaodich; }

    string getWho() { return who; }

    Date getWhen() { return when; }

    double getAmount() { return amount; }

    string toString() { return who + " (" + when.toString() + "): " + to_string(amount) + "$"; }

    friend ostream& operator << (ostream& os, Transaction& transac) {
        os << setw(5) << transac.who << "  " <<  transac.when << "  " <<  setprecision(6) << transac.amount << "$";
        return os;
    }

    friend istream& getline(istream& is, Transaction& transaction) {
        string str;
        getline(is, str);
        if (str.size() != 0) {
            int i = str.find_first_of(" ");
            int j = str.find_last_of(" ");
            transaction.who = str.substr(0,i);
            transaction.when = str.substr(i+1,j-i-1);
            transaction.amount = stod(str.substr(j+1, str.size()-j-1));
        }
        return is;
    }

    Transaction& operator = (Transaction& rhs) {
        this->who = rhs.who;
        this->when = rhs.when;
        this->amount = rhs.amount;
        return *this;
    }

    bool equals(Transaction that) {
        return ((this->getWho() == that.getWho()) && (this->getWhen() == that.getWhen()) && (this->getAmount() == that.getAmount()));
    }

    int compareTo(Transaction that) {
        if (this->getAmount() > that.getAmount()) return 1;
        else if (this->getAmount() < that.getAmount()) return -1;
        else return 0;
    }

    int hashCode() {
        int j = transaction.find_first_of("#");
        int i = transaction.find_first_of(" ", j + 1);
        string mathang = transaction.substr(j + 1, i);
        stringstream convert(mathang);
        int x = 0;
        convert >> x;
        return x;
    }

    bool operator < (Transaction& rhs) { return this->amount < rhs.amount; }
    bool operator > (Transaction& rhs) { return this->amount > rhs.amount; }
    bool operator == (Transaction& rhs) { return this->amount == rhs.amount; }
    bool operator != (Transaction& rhs) { return this->amount != rhs.amount; }
};

void swap(Transaction& lhs, Transaction& rhs) {
    Transaction temp = lhs;
    lhs = rhs;
    rhs = temp;
}
