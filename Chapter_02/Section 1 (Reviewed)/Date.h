#include <iostream>
#include <string>
#include <exception>
using namespace std;

class Date {
private:
    int month;
    int day;
    int year;

public:
    Date() = default;
    Date(int m, int d, int y) {
        if (m > 12 || m < 1) throw runtime_error("In-appropriate month input!");
        else month = m;

        if (d > 31 || d < 1) throw runtime_error("In-appropriate day input!");
        else day = d;

        if (y < 0) throw runtime_error("In-appropriate year input!");
        else year = y;
    }
    ~Date() {}

    int getMonth() { return month; }
    int getDay() { return day; }
    int getYear() { return year; }
    bool operator==(Date);
    bool operator!=(Date);

    Date& operator=(Date& rhs) {
        this->month = rhs.month;
        this->day = rhs.day;
        this->year = rhs.year;
        return *this;
    }

    Date& operator=(string& str) {
        int i = str.find_first_of("/");
        int j = str.find_last_of("/");
        this->month = stoi(str.substr(0,i));
        this->day= stoi(str.substr(i+1,j-i-1));
        this->year = stoi(str.substr(j+1,str.size()-j-1));
        return *this;
    }

    Date& operator=(string&& str) {
        if (str.size() != 0) {
            int i = str.find_first_of("/");
            int j = str.find_last_of("/");
            this->month = stoi(str.substr(0,i));
            this->day= stoi(str.substr(i+1,j-i-1));
            this->year = stoi(str.substr(j+1,str.size()-j-1));
        }
        return *this;
    }

    friend istream& operator >> (istream& is, Date& date) {
        string str;
        is >> str;
        int i = str.find_first_of("/");
        int j = str.find_last_of("/");
        date.month = stoi(str.substr(0,i));
        date.day= stoi(str.substr(i+1,j-i-1));
        date.year = stoi(str.substr(j+1,str.size()-j-1));
        return is;
    }

    
    string toString() {
        if (month >= 10 && day >= 10)
            return to_string(month) + "/" + to_string(day) + "/" + to_string(year);
        else if (month < 10 && day >= 10)
            return "0" + to_string(month) + "/" + to_string(day) + "/" + to_string(year);
        else if (month >= 10 && day < 10)
            return to_string(month) + "/" + "0" + to_string(day) + "/" + to_string(year);
        return "0" + to_string(month) + "/" + "0" + to_string(day) + "/" + to_string(year);
    }

    friend ostream& operator << (ostream& os, Date& date) {
        os << date.toString();
        return os;
    }
};

bool Date::operator==(Date that) {
    return ((this->month == that.month) && (this->day == that.day) && (this->year == that.year));
}

bool Date::operator!=(Date that) {
    return !(*this == that);
}
