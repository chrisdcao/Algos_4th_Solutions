Insertion sort loop:

for (int i = 0; i < N; i++) {
    for (int j = i; j > 0 && a[j] < a[j-1]; j--)
        swap(a[j],a[j-1]);
}

//an array having only 3 values, being arranged randomly
//depends on the array
//If randomly, the array is already sorted, then the running time is linear (Insertion sort 'i' loop will still loop through all the element to check, even though j loop is not executed)
//If randomly, the array is near-reversed sorted or is reversed sorted, the running time is quadratic, j running to its full costing ~ N*(N-1)/2 steps
//If randomly, the array is mostly sorted or very uniformly distributed, the running time might be something in between (we break half of the j loop or more, making the first half or more linear, then the last half of the array could be mixed of linear and quadratic model)
