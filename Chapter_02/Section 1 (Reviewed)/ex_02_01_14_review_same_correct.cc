#include "Cards.h"
#include <deque>

using namespace std;

// THE IDEA IS TO DO SOMETHING SIMILAR TO SELECTION SORT
// EACH TURN WE SEARCH FOR THE MINIMUM OF THE 2 CARDS
// TAKE THE LARGER ONE TO THE BOTTOM
// After the first iteration (N steps) we will be left with the minimum one at the top. We put this down to the bottom. This element is sorted
// Now from the current top to bottom (except the one we just put down) is the 'unsorted part' 
// We repeat the same process for this part, with iteration of N - 1 sorted card (that was put down onto the bottom)
// keep going and we are performing something similar to selection sort, with a little restriction (only using top 2 cards, one a push to bottom operation)

// DEMONSTRATION

// implementation
template <typename T>
class RestrictedSelection {
public:
    static void sort(deque<T>& deque) {
        int N = deque.size();
        const int cardsNumber = deque.size();
        int sortedCards = 0;
        while (sortedCards != cardsNumber) {
            // all the sorted cards will be put the bottom
            // So that from the start to the end of the iteration we are only left with the 'unsorted' part
            for (int m = 0; m < sortedCards; m++) {
                deque.push_back(deque[0]);
                deque.pop_front();
            }

            // only look at the first two cards in the deque (deque[1] & deque[0])
            // and use restricted operation
            for (int a = 1; a < N - sortedCards; a++) {
                if (deque[1] > deque[0])
                    swap(deque[0], deque[1]);
                deque.push_back(deque[0]);
                deque.pop_front();
            }
            sortedCards++;
            N - sortedCards;
        }
    }
};

// test client
int main() {
    // Initialize an unsorted deck
    deque<Card> deckOfCards;
    string suitArray[4] = {"spades","hearts", "clubs", "diamonds"};
    for (auto suit : suitArray) {
        for (int rank = 1; rank <= 13; rank++) 
            deckOfCards.push_back(Card(suit, rank));
    }

    shuffle(deckOfCards.begin(), deckOfCards.end(), std::mt19937{std::random_device{}()});

    // Print the initial unsorted deck
    cout << "The Shuffled deck: " << endl;
    for (auto s : deckOfCards)
        cout << s << endl;
    cout << endl;

    // using restricted Selection sort
    RestrictedSelection<Card>::sort(deckOfCards);

    // Print the result
    cout << "The Sorted Deck: " << endl;
    for (auto s : deckOfCards)
        cout << s << endl;

    return 0;
}
