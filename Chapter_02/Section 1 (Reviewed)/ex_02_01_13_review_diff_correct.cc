#include "Cards.h"

// assume that ace = 1, and 11 = Jack, 12 = Queen, 13 = King
template <typename T>
class Shell {
public:
    static void sort(vector<T>& a) {
        int N = a.size();
        int h = 1;
        while (h < N/3) h = h * 3 + 1;
        while (h >= 1) {
            for (int i = h; i < N; i++) {
                for (int j = i; j >= h && a[j] < a[j-h]; j -= h)
                    swap(a[j], a[j-h]);
            }
            h /= 3;
        }
    }
};

int main() {
    vector<Card> deckOfCards;
    string suitArray[4] = {"spades","hearts", "clubs", "diamonds"};
    for (auto suit : suitArray) {
        for (int rank = 1; rank <= 13; rank++) 
            deckOfCards.push_back(Card(suit, rank));
    }
    shuffle(deckOfCards.begin(), deckOfCards.end(), std::mt19937{std::random_device{}()});
    cout << "The Shuffled deck: " << endl;
    for (auto s : deckOfCards)
        cout << s << endl;

    Shell<Card>::sort(deckOfCards);

    cout << endl;
    cout << "The Sorted Deck: " << endl;
    for (auto s : deckOfCards)
        cout << s << endl;

    return 0;
}

// EXPLANATION
// Sorting deck is like sorting numbers. Suit are just to complicate things up. We can re-written the deck into an all-integer array without changing the its meaning
// We have: Spades < Hearts < Clubs < Diamonds

// Let say we numbered Spades (smallest suit) values from 1-13 
// if we say 1 (hearts) (smallest of hearts) > all Spades, we could as well assign its value as '14' (14 > all numbers from 1-13)
// and so the whole Hearts Suit can be as well seen as: 14-26

// Similarly, 1 (clubs) (smallest of clubs) > all hearts (14-27) > all spades (1-13), so we could as well assign its value as '28'
// -> the whole Clubs can be seen as: 27-39
// And Diamonds can be seen as: 40-52
// so the deck could be re-written as: arrayOfCards = { 1, 2, 3, 4, ... 50, 51, 52 };

// To get a randomized deck, we can shuffle this array up
// In the end, the exercise goes back to sorting an unsorted all-integer array, using only 2 values at a time
// We can either use shellsort, or insertion sort
