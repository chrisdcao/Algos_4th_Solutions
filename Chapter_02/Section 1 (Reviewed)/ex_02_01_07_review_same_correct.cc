Selection Sort:
for (int i = 0; i < N; i++) {
    int iMin = i;
    for (int j = i+1; j < N; j++) 
        if (a[iMin] > a[j]) iMin = j;
    swap(a[iMin], a[i]);
}

Insertion Sort:
for (int i = 0; i < N; i++) {
    for (int j = i; j > 0 && a[j] < a[j-1]; j--) 
        swap(a[j],a[j-1]);
}

//Both traverse N*(N-1)/2 times for reversed array
//Selection Sort will only swap in linear time in total (one swap only cost O(1), for a reversed array to be back in order will cost N - 1 * O(1) swapping-time)
//While Insertion Sort have to do quadratic swapping-time (swap ([j] & [j - 1]) until j reaches index[0], for each swap, costing N^2/2 in total)
