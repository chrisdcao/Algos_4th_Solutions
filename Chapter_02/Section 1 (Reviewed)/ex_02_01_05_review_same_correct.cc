Insertion Sort:

static void sort(vector<T>& a) {
    int N = a.size();
    for (int i = 0; i < N; i++) {
        for (int j = i; j > 0 && a[j] < a[j-1]; j--) 
            swap(a[j], a[j-1]);
    }
}

// For each of the condition, describe an array of N items where that condition is always false when terminate

// The first condition: j > 0
// when the array is in reversed order, making j always has to be decreased until 1-- and break the loop at the next run (== 0)

// The second condition: a[j] < a[j-1] 
// when the array is already sorted
