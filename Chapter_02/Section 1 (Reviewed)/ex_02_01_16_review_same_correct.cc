#include <vector>
#include "Random.h"
#include <iomanip>
#include <iostream>
#include <algorithm>
#include <map>

using namespace std;

// just have to count how many elements of each value were there before the sort 
// and count them after the sort
// then compare

bool check(vector<int>& vec) {
    map<int,int> valueCount;
    bool repeat = false;
    for(auto s : vec) {
        if (valueCount.size() == 0)
                valueCount.insert(pair<int,int>(s,1));
        else {
            auto it = valueCount.begin();
            while (it != valueCount.end()) {
                if (s == it->first) {
                    repeat = true;
                    break;
                } else {
                    repeat = false;
                    it++;
                }
            }
            if (repeat == false) 
                valueCount.insert(pair<int,int>(s,1));
            else 
                it->second++;
        }
    }

    sort(vec.begin(), vec.end());

    int N = vec.size();
    int count = 0;
    int mark = vec[0];
    for(int i = 0; i < N; i++) {
        if (mark != vec[i]) {
            if (valueCount[vec[i-1]] != count)
                return false;
            count = 1;
        }
        else 
            count++;
        mark = vec[i];
    }
    return true;
}


int main(int argc, char** argv) {
    int N = stoi(argv[1]);
    vector<int> vec = {2, 20, -1, -30, 30, 5, 6, 8, -99, -3, 0, 4, 4, 4};
    //vector<int> vec;
    //for(int i = 0; i < N; i++)
        //vec.push_back(randomUniformDistribution(0, N-1));
    cout << ((check(vec)) ? "true" : "false") << endl;
}
