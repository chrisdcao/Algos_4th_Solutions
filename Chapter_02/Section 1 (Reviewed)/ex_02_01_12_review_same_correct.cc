#include <iostream>
#include <climits>
#include <cstdio>
#include <map>
#include <utility>
#include <vector>
#include <numeric>
#include "Random.h"

using namespace std;

static multimap<int, double> mmap;
static vector<double> values;
double mean = 0.00,
       sdev = 0.00,
       sum  = 0.00;

template <typename T>
class Shell {
public:
    static void sort(vector<T>& a) {
        int N = a.size();
        int h = 1;

        while (h < N/3) h = h * 3 + 1;

        while (h >= 1) {
            int compareCnt = 0;
            for(int i = h; i < N; i++) {
                for (int j = i; j >= h && a[j] < a[j-h]; j -= h) {
                    compareCnt++; 
                    swap(a[j], a[j-h]);
                }
            }
            mmap.insert(pair<int, double>(h, compareCnt*1.00/N));
            h /= 3;
        }
    }
};

int main(int argc, char** argv) {
    int N = stoi(argv[1]);
    int trialTimes = stoi(argv[2]);

    for (/**/; N < INT_MAX/10; N *= 10) {
        for (int m = 0; m < trialTimes; m++) {
            vector<double> vec;
            for (int i = 0; i < N; i++)
                vec.push_back(randomUniform<double>(0.00,(N-1)*1.00));

            Shell<double>::sort(vec);
        }
        cout << "After " << trialTimes << " times of calculating Compares By Array Size "
             << "of array size (" << N << ") : "  << endl;

        auto it = mmap.begin();
        int key = it->first;
        vector<double> values;
        while (it != mmap.end()) {
            if (it->first != key) {
                mean = sum / trialTimes;
                for (auto s : values)
                    sdev += pow((s - mean),2);
                sdev = sqrt(sdev/trialTimes);
                printf("%s%5d  %s%5.2f  %s%5.2f", "Increment: ", key, "Mean:", mean, "StdDeviation:", sdev);
                cout << endl;
                key = it->first;
                sum = mean = sdev = 0.00;
            }
            if (it->first == key) {
                sum += it->second;
                values.push_back(it->second);
            }
            key = it->first;
            it++;
        }
        mean = sum / trialTimes;
        for (auto s : values)
            sdev += pow((s - mean),2);
        sdev = sqrt(sdev/trialTimes);
        printf("%s%5d  %s%5.2f  %s%5.2f", "Increment: ", key, "Mean:", mean, "StdDeviation:", sdev);
        cout << endl;
    }
}

