#include <iostream>
#include <climits>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <random>
#include <string>
#include <utility>

using namespace std;

static int cnt=0;

template <typename T>
class Shell {
public:
    static void sort(vector<T>& a) {
        int N = a.size();
        int h = 1;
        while (h < N/3) h = h * 3 + 1;
        while (h >= 1) {
            //cout << h << endl;
            for (int i = h; i < N; i++) {
                for (int j = i; j >= h && a[j] < a[j-h]; j -= h) {
                    swap(a[j], a[j-h]);
                    cnt++;
                }
            }
            h /= 3;
        }
    }
};

int main() {
    vector<int> vec(100);
    iota(vec.begin(), vec.end(), 1);
    int maxCnt = 0;

    // a randomized array
    for (int i = 0; i < 10000; i++) {
        shuffle(vec.begin(), vec.end(), mt19937{random_device{}()});
        Shell<int>::sort(vec);
        maxCnt = max(maxCnt, (int) cnt);
        cnt = 0;
    }

    cout << maxCnt << endl;
}
