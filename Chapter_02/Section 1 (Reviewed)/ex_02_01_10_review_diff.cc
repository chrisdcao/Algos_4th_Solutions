//Shell sort h-sort:
int h = 1;
while (h < N/3) h = h * 3 + 1;
while (h >= 1) {
    for (int i = h; i < N; i++) {
        for (int j = i; j >= h && a[j] < a[j-h]; j -= h)
            swap(a[j], a[j-h]);
    }
}


//Selection sort
int iMin = i;
for (int i = 0; i < N; i++) {
    for (int j = i+1; j < N; j++)
        if (a[j] < a[iMin]) iMin = j;
    swap(a[iMin], a[i]);
}

// 1.
// if we do selection sort in the h-sort part, it will skip the first h-distanced part of the array (since i start from h, not 0)
// 2.
// Doing selection sort will averagely cost more time, since worst case of h-sort is selection sort (quadratic time)
// Defaultly, h-sort has a probability of costing less time than selection sort
// because there's a probability that after some division of h (h > 1) that the array is done sorting (done before distance-step = 1), or after some step of h = 1 that the sort is done
// while selection sort have step of 1 (for each i value), whether the array is done sorting or not, it will still run the 'j' pointer to check
