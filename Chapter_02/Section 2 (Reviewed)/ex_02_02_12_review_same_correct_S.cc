#include <iostream>
#include <algorithm>
#include <random>
#include <numeric>
#include <iomanip>
#include <vector>

using namespace std;

template <typename T>
class Merge {
private:

    static void selectionSort(vector<T>& a, int low, int high) {
        // because in the selectionSortBlocks function we make sure high is the index, not the size
        // thus the loop will travel till == high rather than just '<' like with size
        for (int i = low; i <= high; i++) {
            int iMin = i;
            for (int j = i + 1; j <= high; j++) 
                if (a[j] < a[iMin]) iMin = j;
            swap(a[i], a[iMin]);
        }
    }

public:
    
    static void selectionSortBlocks(vector<T>& a, int M) {
        //sort trong khoang 0 - [M-1], M - [2M-1], ... theo dinh luat chia so so hang
        for (int i = 0; i < a.size(); i+=M)
            selectionSort(a, i, i + M - 1);
    }

    static void merge(vector<T>& org, vector<T>& aux, int low, int mid, int high) {
        int auxIndex = 0;
        for (int m = low; m <= mid; m++) aux[auxIndex++] = org[m];

        int indexLeft = 0;
        int indexRight = mid+1;
        int arrIndex = low;

        while (indexLeft < aux.size() && indexRight <= high) {
            if (aux[indexLeft] < org[indexRight])
                org[arrIndex++] = aux[indexLeft++];
            else
                org[arrIndex++] = org[indexRight++];
        }

        while (indexLeft < aux.size())
            org[arrIndex++] = aux[indexLeft++];
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    int M = stoi(argv[2]);

    vector<int> vec(N);
    iota(vec.begin(), vec.end(), 0);
    random_shuffle(vec.begin(), vec.end());

    cout << "Array before sort: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    vector<int> aux(M);

    Merge<int>::selectionSortBlocks(vec, M);

    for (int i = N/M - 1; i > 0; i--) {
        for (int j = 0; j < i; j++) {
            int low = j * M;
            int mid = (j+1)*M - 1;
            int high = mid + M;
            Merge<int>::merge(vec, aux, low, mid, high);
        }
    }


    cout << "Array after sort: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    return 0;

}
