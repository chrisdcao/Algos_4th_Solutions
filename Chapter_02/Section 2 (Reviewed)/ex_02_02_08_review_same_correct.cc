//Algo 2.4. is Top-down MergeSort

Since merge() will be skipped whenever a[mid] <= a[mid+1], a sorted array will always skips "merge()" function (because sorted_array[mid] always <= sort_array[mid+1] at any given position)
-> The only operation is checking whether "a[mid] < a[mid+1] or not" in each call, which in total is linear
