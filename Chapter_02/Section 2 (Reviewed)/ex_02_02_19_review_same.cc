#include <iostream>
#include <vector>
#include "Random.h"

using namespace std;

template <typename T>
class Merge {
private:

    static int merge(vector<T>& org, vector<T>& aux, int low, int mid, int high) {
        int invCount = 0;
        for (int k = low; k <= high; ++k) aux[k] = org[k];
        int i = low;
        int j = mid + 1;
        for (int k = low; k <= high; ++k) { 
            if (i > mid) org[k] = aux[j++];
            else if (j > high) org[k] = aux[i++];
            /* we do this because the pre-condition of the function is sub-arrays are sorted, thus if aux[i] > aux[j] that means all the elements left of 'i' sub-array will be > a[j] (which = (mid-i), since the array is ascending-sorted, so the more go to right the larger the value, and we are traversing from the left) */
            else if (aux[i] > aux[j]) {
                org[k] = aux[j++];
                invCount += mid - i;
            } else if (aux[j] > aux[i]) {
                org[k] = aux[i++];
                invCount += high - j;
            } else org[k] = aux[i++];
        }
        return invCount;
    }

    static int sort(vector<T>& org, vector<T>& aux, int low, int high) {
        /* init the invCount */
        int mid, invCount = 0;
        /* then at each stage we increase the count */
        if (high > low) {
            mid = low + (high - low) / 2;
            /* inversions count will be the number of inversion in left part + inversion in right part + inversion in merge
             * This is because: after each sorted time, it becomes a new array with a new number of inversion required to reach 'sorted' stated. Thus we sum all these 'inversion variation' of the first array and we get the total inversion from 'initial state' to 'sorted state' */
            invCount += sort(org, aux, low, mid);
            invCount += sort(org, aux, mid+1, high);
            invCount += merge(org, aux, low, mid, high);
        }
        return invCount;
    }

public:

    static int sort(vector<T>& org) {
        vector<int> aux(org.size());
        return sort(org, aux, 0, org.size()-1);
    }

};

int main(int argc, char** argv)  {

    int x;
    vector<int> vec;
    while (cin >> x) 
        vec.push_back(x);

    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    int inversionCount = Merge<int>::sort(vec);

    cout << "The inversionCount of the array is: " << inversionCount << endl;

    cout << "The array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;
 
    return 0;

}
