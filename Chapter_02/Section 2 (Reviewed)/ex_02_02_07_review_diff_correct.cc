#include <iostream>
#include <algorithm>
#include <random>
#include <vector>
#include <numeric>

using namespace std;

static int compareCount = -1;
static int prevCompareCount = -1;
// assume that the hypothesis is true
static bool hypothesis = true;

template <typename T>
class Merge {
private:

    static void merge(vector<T>& org, vector<T>& aux, int low, int mid, int high) {
        prevCompareCount = compareCount;
        compareCount = 0;
        for (int k = low; k <= high; ++k) aux[k] = org[k];

        int i = low;
        int j = mid+1;
        for (int k = low; k <= high; ++k) {
            if (i > mid) org[k] = aux[j++];
            else if (j > high) org[k] = aux[i++];
            else if (aux[i] > aux[j]) org[k] = org[j++];
            else org[k] = aux[i++];
            compareCount++;
        }

        if (prevCompareCount != -1 && compareCount < prevCompareCount)  
            hypothesis = false;
    }

    static void sort(vector<T>& org, vector<T>& aux, int low, int high) {
        if (low >= high) return;
        int mid = low + (high - low) / 2;
        sort(org, aux, low, mid);
        sort(org, aux, mid+1, high);
        merge(org, aux, low, mid, high);
    }

public:

    static void sortTopDown(vector<T>& org) {
        vector<T> aux(org.size());
        sort(org, aux, 0, org.size() - 1);
    }

    static void sortBottomUp(vector<T>& org) {
        int N = org.size();
        vector<T> aux(N);
        for (int len = 1; len < N; len *= 2) {
            for (int low = 0; low < N - len; low += len * 2) {
                int mid = low + len - 1;
                int high = min(mid + len, N - 1);
                merge(org, aux, low, mid, high);
            }
        }
    }

};

int main(int argc, char** argv) { 

    for (int trialTimes = 0; trialTimes < 512; trialTimes++) {
        vector<int> org(stoi(argv[1]));
        iota(org.begin(), org.end(), 0);
        random_shuffle(org.begin(), org.end());

        Merge<int>::sortTopDown(org);
    }

    cout << "Monotonically Increasing:" << ((hypothesis == true) ? "true" : "false") << endl;

    return 0;

}
