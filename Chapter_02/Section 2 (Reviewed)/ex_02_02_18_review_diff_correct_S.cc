#include <climits>
#include "SinglyLinkedListQueue.h"    
#include "Random.h"

using namespace std;

#define List SinglyLinkedList

template <typename T>
class Merge {
    using NodePtr = SinglyLinkedList<int>::Node*;
private:

    static bool tossTheCoin() {
        int a = randomUniformDistribution(0, INT_MAX);
        if (a > INT_MAX / 2) return true;
        return false;
    }

    static NodePtr merge(NodePtr a, NodePtr b) {
        NodePtr result = NULL;
        if (a == NULL) return b;
        else if (b == NULL) return a;
        /* this is where we replace with toss the coin to have the randomness */
        if (tossTheCoin()) {
            result = a;
            result->next = merge(a->next, b);
        } else {
            result = b;
            result->next = merge(a, b->next);
        }
        return result;
    }

    static void FrontBackSplit(NodePtr source, NodePtr* frontRef, NodePtr* backRef) {
        NodePtr fast = source->next; 
        NodePtr slow = source;
        while (fast != NULL) {
            fast = fast->next;
            if (fast != NULL) {
                fast = fast->next;
                slow = slow->next;
            }
        }
        *frontRef = source;
        *backRef = slow->next;
        slow->next = NULL;
    }

    static void shuffle(NodePtr* headRef) {
        NodePtr head = *headRef;
        NodePtr a,b;
        /* neo-case for the recursion */
        if (head == NULL || head->next == NULL) return;
        /* keep splitting the array into smaller halves (until smallest - divide and conquer) */
        FrontBackSplit(head, &a, &b);
        /* shuffle each halves in each recursion */
        shuffle(&a);
        shuffle(&b);
        /* then we recursively merge it (to be able to link to the after-sub-array) */
        *headRef = merge(a,b);
    }

public:

    static void shuffle(List<T>& org) {
        NodePtr* headRef = &org.head;
        shuffle(headRef);
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    List<int> intList;
    for (int i = 0; i < N; i++) intList.insert(i);

    cout << "The array before shuffling is: ";
    cout << intList << endl;

    Merge<int>::shuffle(intList);
    cout << "The array after shuffling is: ";
    cout << intList << endl;

    return 0;

}

