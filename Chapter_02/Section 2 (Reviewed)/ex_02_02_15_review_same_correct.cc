#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <random>
#include <cmath>
#include <numeric>
#include "Queue.h"

using namespace std;

template <typename T>
class Merge {
private:

    static void merge(Queue<Queue<T>>& queueOfQueues) {
        Queue<T> queue1(queueOfQueues.dequeue());
        Queue<T> queue2(queueOfQueues.dequeue());
        Queue<T> org;

        while (!queue1.isEmpty() || !queue2.isEmpty()) {
            if (queue1.isEmpty()) org.enqueue(queue2.dequeue());
            else if (queue2.isEmpty()) org.enqueue(queue1.dequeue());
            else if (queue1.peek() > queue2.peek()) org.enqueue(queue2.dequeue());
            else org.enqueue(queue1.dequeue());
        }

        queueOfQueues.enqueue(org);
    }

public:

    static void BottomUpQueueSort(vector<T>& org) {
        Queue<Queue<T>> queueOfQueues;
        for (int i = 0; i < org.size(); i++) {
            Queue<T> queue;
            queue.enqueue(org[i]);
            queueOfQueues.enqueue(queue);
        }

        // keep merging until the queueOfQueues contains only one queue left
        // that's when the merging is finished
        while (queueOfQueues.size() != 1)
            merge(queueOfQueues);
        Queue<T> sortedQueue(queueOfQueues.dequeue());

        // put back all the sorted elements into the original array
        int arrIndex = 0;
        while (!sortedQueue.isEmpty()) 
            org[arrIndex++] = sortedQueue.dequeue();
    }

};

int main(int argc, char** argv) {

    vector<int> array(stoi(argv[1]));
    iota(array.begin(), array.end(), 0);
    random_shuffle(array.begin(), array.end());

    cout << setw(20) << "The unsorted array: ";
    for (auto s : array)
        cout << s << " ";
    cout << endl;

    Merge<int>::BottomUpQueueSort(array);

    cout << setw(20) << "The sorted array: ";
    for (auto s : array)
        cout << s << " ";
    cout << endl;

    return 0;

}
