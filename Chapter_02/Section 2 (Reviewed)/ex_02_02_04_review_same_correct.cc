yes it requires the sub-array to be sorted first

the loop of 'merge' function in  merge-sort is like this:

static void merge(vector<T>& org, vector<T>& aux, int low, int mid, int high) {
    // taking in the elements no matter it's sorted or not
    for (int k = low; k <= high; ++k) aux[k] = org[k];

    int i = low;
    int j = mid+1;
    for (int k = low; k <= high; ++k) {
        // the fisrt 2 conditions ensure that when one sub-array is out of element we defaultly take the remaining from the other sub-array
        if (i > mid) org[k] = aux[j++];
        else if (j > high) org[k] = aux[i++];
        // if the sub-array is unsorted, then the increments in this step will produce wrong result
        // because by incrementing 'j' and 'i' after each turn we are assuming that the next value (of the same sub-array) 
        // is larger than the current value (so that we can travel in linear without destroying the order preservation)
        // if the subs are unsorted, then incrementing in the same subarray might stumble a smaller value (i.e aux[j-1] < aux[j]), making the increment invalid
        else if (aux[i] > aux[j]) org[k] = aux[j++];
        else org[k] = aux[i++];
    }

}
