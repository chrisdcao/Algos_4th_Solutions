#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <random>

using namespace std;

static int call_time = 0;
template <typename T>
class Merge {
private:

    static void merge(vector<T>& org, vector<T>& aux, int low, int mid, int high) {
        // pre-condition: the sub-arrays are sorted
        int k = low;
        for (/**/; k <= mid; ++k)                  aux[k] = org[k];
        for (int l = high; l >= mid+1; --l && ++k) aux[k] = org[l];

        int indexLeft = low;
        int indexRight = high;
        int arrIndex = low;

        while (indexLeft <= mid) {
            // by this way j is limited in the boundary (since we start j from the right side)
            // j will only go from that outmost right inwards, making the comparison being narrowed only to the elements within the sub-array
            // and then when we perform the checking on i and mid, it ensures that once i is done sorting, the remaining is already in order
            // the below command is to check those above statement, compare to a normal merge-sort
            //cout << auxVec[i] << " " << auxVec[j] << endl;
            if (aux[indexLeft] <= aux[indexRight]) org[arrIndex++] = aux[indexLeft++];
            else                                   org[arrIndex++] = aux[indexRight--];
        }

        for (auto s : org)
            cout << s << " ";
        cout << endl;

    }

    static void sort(vector<T>& org, vector<T>& aux, int low, int high) {
        if (low >= high) return;
        int mid = low + (high - low) / 2;
        sort(org, aux, low, mid);
        sort(org, aux, mid+1, high);
        merge(org, aux, low, mid, high);
    }

public:

    static void sort(vector<T>& org) {
        vector<T> aux(org.size());
        sort(org, aux, 0, org.size() - 1);
    }

};

// Merge sort
int main(int argc, char** argv) {

    vector<int> arr(stoi(argv[1]));
    iota(arr.begin(), arr.end(), 0);

    cout << "Before sorting: ";
    random_shuffle(arr.begin(),arr.end());
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    Merge<int>::sort(arr);
    // mergesort function
    cout << "After sorting: ";
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    return 0;

}
