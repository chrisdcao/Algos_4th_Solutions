The minimum expected number of compares is when tree is balanced because that's when the tree's depth is shortest. Any imbalanced tree will have a higher depths, leading to more cost of traversing each time it compares with the root

Each time a node enter from outside into the tree, it has to make one comparison with the root, then as it goes down each level, it makes comparison with each child-root/leaves node on a side/branch of that level until it's decided to be placed at a position (based on whether it's larger or smaller) (0)
from (0) -> the total compares for each node = the level the node's in in relation to the level-0 root (1)(*NOT in relation to the child root, because the node's entering from outside, making it have traverse from the top root down) 
(i.e. a node entering into level 1 will have to make 1 comparison with the tree root, a node entering level 2 will make one comparison with tree and one comparison with level 1 child root = 2 comparisons, etc..). In short, a level-1 node has compare cost of 1, level-2 node compare cost of 2, level 3 nodes compare cost of 3, etc..-> node level-k cost k comparisons to be paced into the tree (2)
//from (1) -> the total compares for N nodes = Sigma (total travel cost from the root down to that level of a node) (3)
Since there's only 2 possibility of comparing (< || >=), so a binary tree will be the most efficient tree form. Any other tree form will increase the number of compares at each level with minimal reduction in tree height.
The total nodes at each level = 2 * number of elements in the previous level (growing rate of 2 for binary tree)
-> the total nodes at each level (general formula) = 2^k with k being tree height (4)
from (2) & (4) -> the number of compares for N nodes = 2^k*k with k being the tree height N elements can create for a bin tree
