#ifndef DOUBLYLINKEDLIST_H
#define DOUBLYLINKEDLIST_H

#include <iostream>
#include <string>
#include <memory>
#include <sstream>
#include <vector>

using namespace std;

template <typename T>
class DoublyLinkedList {
    template <typename H>
    friend class NaturalMergeLinkedList;
private:
    class Node {
    public:
        T item;
        Node* next=NULL;
        Node* prev=NULL;
        int index = -1;
    };

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;

    int N;
    Node* head;
    Node* tail;
    int isReversed=0;

public:
    DoublyLinkedList(): N(0), head(NULL), tail(NULL) {}

    friend ostream& operator << (ostream& os, DoublyLinkedList& list) {
        if (list.isReversed % 2 == 0) {
            auto traverse = list.head;
            while (traverse != NULL) {
                os << traverse->item << " ";
                traverse = traverse->next;
            }
        }
        else {
            auto traverse = list.tail;
            while (traverse != NULL) {
                os << traverse->item << " ";
                traverse = traverse->prev;
            }
        }
        os << endl;
        return os;
    }

    int size() { return N; }
    bool isEmpty() { return N==0; }

    Node* getHead() { return head; }
    Node* getTail() { return tail; }

    void insert(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            temp->prev = NULL;
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            temp->prev = tail;
            tail = temp;
        }
        ++N;
    }


    void insertAfter(int index, T item) {
        if (index > N - 1 || index < 0) return;
        else {
            if (index == N) 
                insert(item);
            else {
                if (index == N - 1) insert(item);
                if (index > N / 2) {
                    Node* traverse = tail;
                    for (int i = N-1; i > index-1; i--) 
                        traverse = traverse->prev;
                    Node* temp = newNode();
                    temp->item = item;
                    temp->prev = traverse;
                    temp->next = traverse->next;
                    traverse->next->prev = temp;
                    traverse->next = temp;
                } else {
                    Node* traverse = head;
                    for (int i = 0; i < index-1; i++) 
                        traverse = traverse->next;
                    Node* temp = newNode();
                    temp->item = item;
                    temp->prev = traverse;
                    temp->next = traverse->next;
                    traverse->next-prev = temp;
                    traverse->next = temp;
                }
                ++N;
            }
        }
    }

    void removeAtBegin() {
        if (head == NULL) 
            cout << "There's no element left to remove" << endl;
        else {
            Node* oldHead = head;
            head = head->next;
            head->prev = NULL;
            freeNode(oldHead);
            --N;
        }
    }

    void removeAtEnd() {
        if (tail == NULL)
            cout << "There's no element left to remove" << endl;
        else {
            Node* oldTail = tail;
            tail = tail->prev;
            tail->next = NULL;
            freeNode(oldTail);
            --N;
        }
    }

    void reverse() {
        ++isReversed;
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

};

#endif
