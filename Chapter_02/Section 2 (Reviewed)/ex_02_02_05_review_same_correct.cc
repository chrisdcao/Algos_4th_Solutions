N = 39 

MergeSort top-down will breakdown the tree from the top (size N) to bottom (clusters of size 2) by recursively breaking down 1 to half, half into 2 halves, each half into other 2 halves, etc.. (forming a binary tree)
Then from the bottom of the tree we travel back up to return value to each recursion, and at any given recursive depth and at any given branch, we sort left side until it's finished first then we move onto the right side
We have: N / 2 = 19
-> left major sub-array size: 19
   right major sub-array sz : 20
for left major: 
2 - 3 - 2 - 5 - 2 - 3 - 2 - 5 - 10 - 20
for right major: 
2 - 3 - 2 - 5 - 2 - 3 - 2 - 2 - 4 - 9 - 19
ending: 39
Total map:
2 - 3 - 2 - 5 - 2 - 3 - 2 - 5 - 10 - 20 - 2 - 3 - 2 - 5 - 2 - 3 - 2 - 2 - 4 - 9 - 19 - 39

MerSort bottom-up will sort and resolve all similar-sized clusters first, starting from smallest-sized cluster (pair), then it sort to larger clusters based on sorted smaller clusters (bottom tree up)
We have: smallest-size possible for sorting: 2, starting from 2, the growth of clusters should be:
2 - 4 - 8 - 16 - 32 - 39(N, stop)
we have 19 clusters of size 2
        9 clusters of size 4  - 3 left out and resolve at this stage
        4 clusters of size 8  - 7 left out and resolve at this stage
        2 clusters of size 16 
        1 clusters of size 32
        reach the end
Total map:
2 - 2 - 2 ..... - 2 (19) - 4 - 4 .... - 4 (9) - 3 - 8 - 8 - 8 - 8 (4) - 7 - 16 - 16 (2) - 32 (1) - 39


