#include <iostream>
#include <vector>

using namespace std;

template <typename T>
class Merge {
private:

    static int splitArrayAndCountInversions(vector<T> array, vector<T>& aux, int low, int high) {
        if (low >= high) return 0;
        int middle = low + (high - low) / 2;

        int inversions = splitArrayAndCountInversions(array, aux, low, middle);
        inversions += splitArrayAndCountInversions(array, aux, middle + 1, high);

        return inversions + countInversionsComparingBothParts(array, aux, low, middle, high);
    }

    static int countInversionsComparingBothParts(vector<T> array, vector<T> aux, int low, int middle, int high) {

        for(int i = low; i <= high; i++) 
            aux[i] = array[i];

        int leftIndex = low;
        int rightIndex = middle + 1;
        int arrayIndex = low;

        int inversions = 0;

        while (leftIndex <= middle && rightIndex <= high) {
            if (aux[leftIndex] <= aux[rightIndex]) {
                array[arrayIndex] = aux[leftIndex];
                leftIndex++;
            } else {
                /* because middle and leftIndex are just indexes but we want to get the NUMBER, thus we have to plus one (else minus of 2 indexes will be '1' unit less than the number) */
                inversions += middle - leftIndex + 1;
                array[arrayIndex] = aux[rightIndex];
                rightIndex++;
            }
            arrayIndex++;
        }

        while (leftIndex <= middle) {
            array[arrayIndex] = aux[leftIndex];

            leftIndex++;
            arrayIndex++;
        }

        return inversions;
    }

public:

    static int countInversions(vector<T> array) {
        vector<T> aux(array.size());
        return splitArrayAndCountInversions(array, aux, 0, array.size() - 1);
    }

};

int main(int argc, char** argv)  {

    int x;
    vector<int> vec;
    while (cin >> x) 
        vec.push_back(x);
    int inversionCount = Merge<int>::countInversions(vec);
    cout << "The inversionCount of the array is: " << inversionCount << endl;
    return 0;

}
