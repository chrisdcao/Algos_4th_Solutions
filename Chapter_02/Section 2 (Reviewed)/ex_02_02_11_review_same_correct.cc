#include <iostream>
#include <iterator>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <numeric>
#include <random>

using namespace std;

template <typename T>
class Merge {
private:

    static void InsertionSort(vector<T>& a, int low, int high) {
        if (low >= high) return;
        for (int i = low; i < high; i++) {
            for (int j = i+1; j > 0 && a[j] < a[j-1]; j--)
                swap(a[j], a[j-1]);
        }
    }

    static void merge(vector<T>& org, vector<T>& aux, int low, int mid, int high) {
        int i = low;
        int j = mid+1;

        for (int k = low; k <= high; ++k) {
            if (i > mid) org[k] = aux[j++];
            else if (j > high) org[k] = aux[i++];
            else if (aux[i] > aux[j]) org[k] = aux[j++];
            else org[k] = aux[i++];
        }
    }

    /* the boundary for small sub-array will be 43 (at which insertion still beats merge) */
    static void sort(vector<T>& org, vector<T>& aux, int low, int high) {
        /* we can actually do a corner case here for the whole array if number of mem < 43
         * bu since the exercise only asks substituition for the sub-array, so we skip */
        if (low >= high)  return;
        int mid = low + (high - low) / 2;
        /* improvement 1. use insertion sort for small subarray 
         * (<= 43 because at this size insertion still beats merge sort) */
        if (high - low <= 43) InsertionSort(aux, low, high);
        else {
            /* improvement 3. we have to sort from org to aux first
             * then when calling merge we will be putting the sorted sub-array from aux to org
             * this ensure the precnodition that we still usually do before taking in elements from the sub-arrays to the org array */
            sort(aux, org, low, mid);
            sort(aux, org, mid+1, high);
        }
        /* improvement 2. skip if [mid] < [mid+1]
         * this will be k-linear cost, as you only copy the whole sub-array into the aux[]
         * instead of costing you potentially k.logk */
        if (org[mid] < org[mid+1]) {
            for (int i = low; i < high; i++) {
                aux[i] = org[i];
            }
        }
        merge(org, aux, low, mid, high);
    }

public:

    static void sort(vector<T>& org) {
        /* improvment 3. eliminate copying to auxillary array */
        vector<T> aux;
        for (int i = 0; i < org.size(); i++) 
            aux.push_back(org[i]);
        sort(org, aux, 0, org.size() - 1);
    }

};

int main(int argc, char** argv) { 
    vector<int> vec(stoi(argv[1]));;
    iota(vec.begin(), vec.end(), 0);
    cout << setw(24) << "Array before sorting: ";
    random_shuffle(vec.begin(), vec.end());

    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    Merge<int>::sort(vec);

    cout << setw(24) << "Array after sorting: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;
}
