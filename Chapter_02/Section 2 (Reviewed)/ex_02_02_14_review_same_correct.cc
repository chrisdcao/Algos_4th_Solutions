#include <iostream>
#include <algorithm>
#include <random>
#include <numeric>
#include <vector>
#include "Queue.h"

using namespace std;

template <typename T>
class Merge {
public:

    static Queue<T> SortQueues(Queue<T>& sorted1, Queue<T>& sorted2) {
        Queue<T> org;
        for (int k = 0; k < sorted1.size() + sorted2.size(); ++k) {
            if (sorted1.isEmpty()) org.enqueue(sorted2.dequeue());
            else if (sorted2.isEmpty()) org.enqueue(sorted1.dequeue());
            else if (sorted1.peek() > sorted2.peek()) org.enqueue(sorted2.dequeue());
            else org.enqueue(sorted1.dequeue());
        }
        return org;
    }

};
 
int main(int argc, char** argv) {

    Queue<int> queue1;
    Queue<int> queue2;

    for (int i = 0; i < 10; i++)
        queue1.enqueue(i);
    cout << "Queue1: ";
    for (auto s : queue1)
        cout << s << " ";
    cout << endl;

    for (int i = 2; i < 10; i++)
        queue2.enqueue(i);
    cout << "Queue2: ";
    for (auto s : queue2)
        cout << s << " ";
    cout << endl;

    Queue<int> mergeResult = Merge<int>::SortQueues(queue1, queue2);
    cout << "After merging 2 queues: ";
    for (auto s : mergeResult)
        cout << s << " ";
    cout << endl;

    return 0;

}
