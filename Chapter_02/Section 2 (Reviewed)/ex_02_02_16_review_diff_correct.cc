#include <iostream>
#include <iomanip>
#include <algorithm>
#include <random>
#include <vector>
#include <numeric>
#include "Random.h"
#include "Queue.h"

using namespace std;

/*  If a sub-array sequence does NOT satisfy the ascending order, that's mean either: 
    1, that it contains only one member or multiple duplicate members  of one value (go both ways, ascending = descending) or 
    2, it's going in the reversed order
    in either cases, by reversing that sub-array order we will obtain a sub that has similar order to the sorted sub-array we are demanding
    From there we can perform merge (as we have satisfied the pre-condition of 2 sub-arrays being sorted before transferring to aux[])      */

template <typename T>
class MergeNaturalBottomUp {
public:

    static void merge(vector<T>& org, vector<T>& aux, int low, int mid, int high) {
        if (low >= high) return;
        for (int k = low; k <= high; ++k) aux[k] = org[k]; 

        int i = low;
        int j = mid+1;

        for (int k = low ; k <= high; ++k) {
            if (i > mid) org[k] = aux[j++];
            else if (j > high) org[k] = aux[i++];
            else if (aux[i] > aux[j]) org[k] = aux[j++];
            else org[k] = aux[i++];
        }
    }

    static void sort(vector<T>& org) {
        int N = org.size();
        vector<T> aux(N);
        Queue<T> queue;
        int low  = -1,
            mid  = -1,
            high = -1,
            i    =  0,
            count = 0,
            oldMid = 0,
            oldLow = 0;
        bool isSortedFlag = false,
             noDescendingFlag = false,
             round1 = false,
             round2 = false;
        while (isSortedFlag == false) {
            if (high == N - 1 || mid == N-1) {
                noDescendingFlag = true;
                low = mid = high = -1;
                i = count = 0;
            } 
            if (count % 2 == 1) {
                if (round2 == true) {
                    if (count == 1) oldMid = mid;
                    else            oldMid = mid+1;
                } else oldLow = low;
            }

            if (low == -1) low = i;
            else low = ++i; 
            while (org[i] <= org[i+1]) { ++i; }
            if (i >= low) ++count;
            if (count % 2 == 0 and count != 0)  {
                if (round2 == true) { merge(org, aux, mid, high, i); }
                else { merge(org, aux, oldLow, mid, i); }
                round1 = round2 = false;
                count = 0;
            }
            mid = i++;
            if (mid != N-1) {
                if (low != mid) round1 = true;
                if (noDescendingFlag == false) {
                    while (org[i] >= org[i+1] and i < N) { queue.enqueue(org[i++]); }
                    int lastNonAscending = --i;
                    if (i == N-1)
                        high = N-1;
                    else
                        high = lastNonAscending;
                    while (!queue.isEmpty()) { org[lastNonAscending--] = queue.dequeue(); }
                } else {
                    while (org[i] <= org[i+1]) { ++i; }
                    high = min(i,N-1);
                }
                if (high != mid) round2 = true;
                if (high > mid or (mid == N-1 and mid == high)) ++count;
                if (high != low and count % 2 == 0 and count != 0) { 
                    merge(org, aux, low, mid, high); 
                    round1 = round2 = false;
                    count = 0;
                }
                if (high - low >= (N-1)) isSortedFlag = true;
            }
        }
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));

    cout << setw(24) << "Array before sorting: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    MergeNaturalBottomUp<int>::sort(vec);

    cout << setw(24) << "Array after sorting: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    return 0;

}
