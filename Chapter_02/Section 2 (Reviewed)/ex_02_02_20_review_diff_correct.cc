#include <iostream>
#include <algorithm>
#include <numeric>
#include <random>
#include <vector>
#include <iomanip>
#include <utility>
#include "Random.h"

using namespace std;

template <typename T>
class Merge {

private: 

    static void merge(vector<pair<T,int>>& orgClone, vector<pair<T,int>>& aux, int low, int mid, int high) {
        for (int k = low; k <= high; ++k) aux[k] = orgClone[k];
        int i = low;
        int j = mid+1;
        for (int k = low; k <= high; ++k) {
            if (i > mid) orgClone[k] = aux[j++];
            else if (j > high) orgClone[k] = aux[i++];
            else if (aux[i].first > aux[j].first) orgClone[k] = aux[j++];
            else orgClone[k] = aux[i++];
        }
    }

    static void sort(vector<pair<T,int>>& orgClone, vector<pair<T, int>>& aux, int low, int high) {
        if (low >= high) return;
        int mid = low + (high - low) / 2;
        sort(orgClone, aux, low, mid);
        sort(orgClone, aux, mid+1, high);
        merge(orgClone, aux, low, mid, high);
    }

public:

    static vector<int> indirectSort(vector<T>& org) {
        int N = org.size();
        vector<pair<T,int>> aux(N);
        vector<pair<T,int>> orgClone;
        for (int i = 0; i < N; i++) 
            orgClone.push_back(make_pair(org[i], i));
        sort(orgClone, aux, 0, N-1);
        vector<int> indexArray;
        for (int i = 0; i < N; i++)
            indexArray.push_back(orgClone[i].second);
        return indexArray;
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;
    vector<int> indexReference;
    for (int i = 0; i < N; i++) {
        vec.push_back(randomUniformDistribution(0, N*10));
        indexReference.push_back(i);
    }

    cout << "The Array before sorting is: " << endl;
    for (auto s : vec)
        cout << setw(3) << s << " ";
    /* for referencing purpose */
    cout << endl;
    for (auto s : indexReference)
        cout << setw(3) << s << " ";
    cout << endl << endl;

    vector<int> perm = Merge<int>::indirectSort(vec);

    cout << "The array after sorting is: " << endl;
    /* DEBUG purpose only, the exercise does not require us to sort the array */
    sort(vec.begin(), vec.end());
    for (auto s : vec)
        cout << setw(3) << s << " ";
    cout << endl;
    for (auto s : perm)
        cout << setw(3) << s << " ";
    cout << endl;

    return 0;

}

