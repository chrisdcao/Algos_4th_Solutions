#include <iostream>
#include <cmath>
#include <iomanip>
#include <string>
#include <vector>
#include <random>
#include <numeric>
#include <algorithm>

using namespace std;

static int arrAccess = 0;

template <typename T>
class Merge {
private:

    static void merge(vector<T>& org, vector<T>& aux, int low, int mid, int high) {
        for (int k = low; k <= high; ++k) {
            arrAccess++;
            aux[k] = org[k];
        }

        int i = low;
        int j = mid+1;
        for (int k = low; k <= high; ++k) {
            if (i > mid) org[k] = aux[j++];
            else if (j > high) org[k] = aux[i++];
            else if (aux[i] > aux[j]) org[k] = aux[j++];
            else org[k] = aux[i++];
            arrAccess++;
        }
    }

    static void sort(vector<T>& org, vector<T>& aux, int low, int high) {
        if (low >= high) return;
        int mid = low + (high - low) / 2;
        sort(org, aux, low, mid);
        sort(org, aux, mid+1, high);
        merge(org, aux, low, mid, high);
    }

public:
    
    static void sortTopDown(vector<T>& org) {
        vector<T> aux(org.size());
        sort(org, aux, 0, org.size() - 1);
    }

    static void sortBottomUp(vector<T>& org) {
        int N = org.size();
        vector<T> aux(N);
        for (int len = 1; len < N; len *= 2) {
            for (int low = 0; low < N - len; low += len * 2) {
                int mid = low + len - 1;
                int high = min(mid + len, N - 1);
                merge(org, aux, low, mid, high);
            }
        }
    }

};

int main(int argc, char** argv) {

    vector<int> org(stoi(argv[1]));
    iota(org.begin(), org.end(), 0);
    random_shuffle(org.begin(), org.end());

    Merge<int>::sortTopDown(org);
    cout << setw(26) << "TOP-DOWN Array accessed: " << arrAccess << endl;

    arrAccess = 0;
    Merge<int>::sortBottomUp(org);
    cout << setw(26) << "BOTTOM-UP Array accessed: " << arrAccess << endl;

    return 0;

}
