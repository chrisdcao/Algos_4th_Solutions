#include <iostream>
#include <algorithm>
#include <random>
#include <numeric>
#include "SinglyLinkedListQueue.h"
#include "Random.h"

using namespace std;

/* this way we are not merging chunks first but actually keep merging from left to right, even when left gets bigger then we would be merging large chunks with smalller chunks
 * this ensures that we can keep linking the last to the next and will never lose the link, because until it is merged, the head of second chunk will never change (not being merge simultaneously with the right chunk)  */
/* Basically, this is not natural merge! It's not even divide and conquer.. */
template <typename T>
class Merge {

    using NodePtr = SinglyLinkedList<int>::Node*;

private:

    static NodePtr merge(SinglyLinkedList<T>& org, NodePtr low, NodePtr middle, NodePtr high) {
        NodePtr leftNode = low;
        NodePtr rightNode = middle->next;
        NodePtr newLow;
        NodePtr afterLastNode = high->next;
        NodePtr aux;

        if (leftNode->item <= rightNode->item) {
            newLow = leftNode;
            aux = leftNode;
            leftNode = leftNode->next;
        } else {
            newLow = rightNode;
            aux = rightNode;
            rightNode = rightNode->next;
        }

        while (leftNode != middle->next && rightNode != high->next) {
            if (leftNode->item <= rightNode->item) {
                aux->next = leftNode;
                aux = leftNode;
                leftNode = leftNode->next;
            } else {
                aux->next = rightNode;
                aux = rightNode;
                rightNode = rightNode->next;
            }
        }

        if (leftNode == middle->next) 
            aux->next = rightNode;
        else {
            aux->next = leftNode;
            middle->next = afterLastNode;
        }

        /* cout << org << endl; */

        /* how do we connect member to this new low of the subarray? */
        return newLow;
    }

public:


    static void sort(SinglyLinkedList<T>& org) {
        NodePtr sourceNode = org.head;
        if (sourceNode == NULL || sourceNode->next == NULL) return;
        
        NodePtr low = sourceNode,
                middle = sourceNode,
                high = sourceNode,
                currentNode = sourceNode;

        bool secondSubArray = false;

        /* we are traversing from left to right
         * boi vi ascend va descend mang tinh xen ke nen ko bao h co truong hop 2 descend ke nhau.
         * Va the la chung ta luon co the merge  */
        while (currentNode != NULL && currentNode->next != NULL) {
            if (currentNode->item > currentNode->next->item) {
                if (!secondSubArray) {
                    middle = currentNode;
                    secondSubArray = true;
                } else {
                    high = currentNode;
                    low = merge(org, low, middle, high);
                    middle = high;
                }
            }
            currentNode = currentNode->next;
        }

        if (high->next != NULL && currentNode != NULL)
            low = merge(org, low, middle, currentNode);

        org.head = low;
    }

};

int main(int argc, char** argv) {

    /* int N = stoi(argv[1]); */
    int x;
    SinglyLinkedList<int> intList;
    while (cin >> x)
        intList.insert(x);
    /* for (int i = 0; i < N; i++)
     *     intList.insert(randomUniformDistribution(0, N-1)); */

    cout << intList << endl;

    Merge<int>::sort(intList);

    cout << intList << endl;

    return 0;

}
