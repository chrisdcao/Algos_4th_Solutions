#include <iostream>
#include <string>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <vector>
#include "ChrisShuffle.h"

using namespace std;

/* My solution works even with 3 array different in length:
 * The idea is to use each elements of the first array as key (we will not sort this array so that it preserves its order, only then can we find the FIRST triplicate to occur, as required by the exercise)
 * Then use BinarySearch on each key with the other 2 arrays (these 2 arrays will be sorted)
 * Until the first key appears we stop the search and return the value 
 * If there's no occurence, then that means there's no triplicate (even when the other 2 have the key, because that means we can only have duplicate at max, not triplicate)  */

/* Total cost: 4N * logN */

template <typename T>
class Find {
private:
    /* Merge-Sort Section */
    static void merge(vector<T>& org, vector<T>& aux, int low, int mid, int high)  {
        for (int k = low; k <= high; ++k) aux[k] = org[k];
        int i = low;
        int j = mid+1;
        for (int k = low; k <= high; ++k) {
            if (i > mid) org[k] = aux[j++];
            else if (j > high) org[k] = aux[i++];
            else if (aux[i] > aux[j]) org[k] = aux[j++];  
            else org[k] = aux[i++];
        }
    }

    static void mergeSortBottomUp(vector<T>& org) {
        int N = org.size();
        vector<T> aux(N);
        for (int len = 1; len < N; len *= 2) {
            for (int low = 0; low < N - len; low += len * 2) {
                int mid = low + len - 1;
                int high = min(mid+len, N - 1);
                merge(org, aux, low, mid, high);
            }
        }
    }

    /* BinarySearch Section */
    static int binarySearchBound(T key, vector<T>& vec, int low, int high) {
        while (low <= high)  {
            int mid = low + (high - low) / 2;
            if (key > vec[mid]) low = mid + 1;
            else if (key < vec[mid]) high = mid - 1;
            else return mid;
        }
        return -1;
    }

    static int binarySearchMostLeft(T key, vector<T>& vec) {
        int low = 0;
        int high = vec.size() - 1;
        while (low <= high)  {
            int mid = low + (high - low) / 2;
            if (key > vec[mid]) low = mid + 1;
            else if (key < vec[mid] || mid > 0 && vec[mid] == vec[mid-1]) high = mid - 1;
            else {
                int indexLeft = binarySearchBound(key, vec, low, high);
                if (indexLeft == -1) return mid;
                else return indexLeft;
            }
        }
        return -1;
    }

public:

    /* return the most-left index of the element in the first array
     * and returning -1 if there's no triplicate */
    static T triplicate(vector<T>& vec1, vector<T>& vec2, vector<T>& vec3) {
        mergeSortBottomUp(vec2);
        mergeSortBottomUp(vec3);
        int i = 0;
        for (/* */; i < vec1.size(); i++) {
            int a = binarySearchMostLeft(vec1[i], vec2);
            int b = binarySearchMostLeft(vec1[i], vec3);
            if (a != -1 && b != -1) return vec1[i];
        }
        cout << "no triplicate found, returning first member in the first array!";
        return vec1[0];
    }

};

int main(int argc, char** argv) {

    /* setup */
    string x ;
    int str;
    vector<string> vec1, vec2, vec3;

    /* initializing the vectors in random order */
    vec1 = { "phong", "thinh", "cuong", "khanh", "khoa" };
    vec2 = { "khoa", "long", "hung", "hang", "imba" };
    vec3 = { "phong", "axe", "juggernaut", "khoa", "thinh" };

    /* print the vectors to check the input first */
    for (auto s : vec1) cout << s << " ";
    cout << endl;
    for (auto s : vec2) cout << s << " ";
    cout << endl;
    for (auto s : vec3) cout << s << " ";
    cout << endl;

    /* print out the result */
    cout << "Expected: khoa" << endl; 
    cout << "Real output: ";
    cout << Find<string>::triplicate(vec1, vec2, vec3) << endl << endl;

    /* initializing the vectors in random order */
    vector<string> vec4 = { "phong", "thinh", "cuong", "khanh", "khoa" };
    vector<string> vec5 = { "khoa", "thinh", "hung", "hang", "imba" };
    vector<string> vec6 = { "phong", "axe", "juggernaut", "khoa", "thinh" };

    /* print the vectors to check the input first */
    for (auto s : vec4) cout << s << " ";
    cout << endl;
    for (auto s : vec5) cout << s << " ";
    cout << endl;
    for (auto s : vec6) cout << s << " ";
    cout << endl;

    /* print out the result */
    cout << "Expected: thinh" << endl; 
    cout << "Real output: ";
    cout << Find<string>::triplicate(vec4, vec5, vec6) << endl;

    return 0;

}
