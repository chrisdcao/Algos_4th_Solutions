#include <iostream>
#include <random>
#include <vector>
#include <numeric>
#include <iomanip>
#include <algorithm>
#include "Random.h"

using namespace std;

/* this way of sorting will result in N * log N with log N having base of 3
 * We prove this:
 *     Let say after k divisions of N into thirds, we get down to 1 element. Since 1 has no comparability, we stop. 
 *     So we can say that k is the steps necessary to end the dividing process.
 *     Mathematically, we can represent this as:
 *     => N/3/3/3/3/3/3/..../3 = 1 (k division of N into thirds, we end at 1)
 *         -- k number of 3 --
 *    <=> N / (3 ^ k)          = 1
 *    <=> N = 3 ^ k <=> k = log3(N) 
 *     In the complete algorithm, at each division we traverse the array at ~ linear cost
 *     => Total cost: ~ N * log3(N) */
template <typename T>
class ThreeWayMerge {
private:

    static void merge(vector<T>& org, vector<T>& aux, int low, int onethird, int twothird, int high) {
        /* precondition: all the subarrays are sorted */
        for (int m = low; m <= high; ++m) aux[m] = org[m];
        int i = low;
        int j = onethird+1;
        int k = twothird+1;
        for (int m = low; m <= high; ++m) {
            if (i > onethird) {
                if (j > twothird) org[m] = aux[k++];
                else if (k > high) org[m] = aux[j++];
                else if (aux[j] > aux[k]) org[m] = aux[k++];
                else org[m] = aux[j++];
            } 
            else if (j > twothird) {
                if (i > onethird) org[m] = aux[k++];
                else if (k > high) org[m] = aux[i++];
                else if (aux[i] > aux[k]) org[m] = aux[k++];
                else org[m] = aux[i++];
            }
            else if (k > high) {
                if (i > onethird) org[m] = aux[j++];
                else if (j > twothird) org[m] = aux[i++];
                else if (aux[i] > aux[j]) org[m] = aux[j++];
                else org[m] = aux[i++];
            } 
            else if (aux[i] < aux[k] && aux[i] < aux[j]) org[m] = aux[i++];
            else if (aux[j] < aux[k] && aux[j] < aux[i]) org[m] = aux[j++];
            else                                         org[m] = aux[k++];
        }
    }

    static void sort(vector<T>& org, vector<T>& aux, int low, int high) {
        if (low >= high) return;
        int onethird = low + (high - low)  / 3;
        int twothird = onethird + (high - low) / 3;
        sort(org, aux, low, onethird);
        sort(org, aux, onethird+1, twothird);
        sort(org, aux, twothird+1, high);
        merge(org, aux, low, onethird, twothird, high);
    }

public:

    static void sort(vector<T>& org) {
        vector<T> aux(org.size());
        sort(org, aux, 0, org.size() - 1);
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1)); 

    cout << "Array before sorting: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    ThreeWayMerge<int>::sort(vec);

    cout << "Array after sorting: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    return 0;

}
