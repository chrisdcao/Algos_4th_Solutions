#include <iostream>
#include <random>
#include <algorithm>
#include <numeric>

using namespace std;

template <typename T>
class Shuffle {
public:

    static void uniform(vector<T> vec) {
        shuffle(vec.begin(), vec.end(), std::mt19937{std::random_device{}()});
    }

};
