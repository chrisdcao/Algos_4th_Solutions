We will analyze on the code fragment from quicksort:

static int partition(vector<T>& a, int lo, int hi) {
    int i = lo, j = hi + 1;
    T v = a[lo];
    while (true) {
        while (a[++i] < v) if (i == hi) break; 
        while (v < a[j--]) if (j == lo) break; 
        /* the loop keep going until this condition is met, and it's met when they are both in the middle (for repeated array)*/
        if (i >= j) break;
        swap(a[i], a[j]);
    }
    swap(a[lo], a[j]);
    return j;
}

/* so basically, sortOrg will keep dividing the array in half
 * So the total comparison should be ~ N * log N 
 * (for each division we travel a total distance of N (until the 2 pointers meet), and total division times = logN) */
static void sortOrg(vector<T>& a, int lo, int hi) {
    if (hi <= lo) return;
    int j = partition(a, lo, hi);
    sortOrg(a, lo, j-1);
    sortOrg(a, j+1, hi);
}

