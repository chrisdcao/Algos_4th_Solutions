#include <iostream>
#include "ContainerUtility.h"

using namespace std;

int main(int argc, char** argv) {
    
    int array[5] = {0, 1, 2, 3, 4 };
    swapArray(array, 0, 3);
    for (auto s : array)
        cout << s << " ";
    cout << endl;

    return 0;

}
