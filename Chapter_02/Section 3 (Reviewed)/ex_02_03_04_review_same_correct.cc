#include <iostream>
#include <algorithm>
#include <random>
#include <vector>
#include <iomanip>
#include <string>
#include "Random.h"

using namespace std;

int compareCount = 0;

template <typename T>
class Quick {
private:

    static void sort(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        int j = partition(a, lo, hi);
        sort(a, lo, j-1);
        sort(a, j+1, hi);
    }

    static int partition(vector<T>& a, int lo, int hi)  {
        int i = lo, j = hi + 1;
        T v = a[lo];
        while (true) {
            while (a[++i] < v) {
                ++compareCount;
                if (i == hi) break;
            }
            while (v < a[--j])  {
                ++compareCount;
                if (j == lo) break;
            }
            if (i >= j)  {
                ++compareCount;
                break;
            }
            swap(a[i], a[j]);
        }
        swap( a[lo], a[j]);;
        return j;
    }

public:

    static int sortWithoutShuffle(vector<T> a) {
        compareCount = 0;
        sort(a, 0, a.size() - 1);
        return compareCount;
    }

    static int sortWithShuffle(vector<T> a) {
        random_shuffle(a.begin(), a.end());
        compareCount = 0;
        sort(a, 0, a.size()-1);
        return compareCount;
    }

};

int main(int argc, char** argv) {

    int x;
    string str = "end";
    vector<int> vec1;
    vector<int> vec2;
    vector<int> vec3;
    vector<int> vec4;
    vector<int> vec5;
    vector<int> vec6;

    cout << "The worst case occur when the array is in reversed order OR the array is already sorted" << endl;
    cout << "Below will be the printout comparison of WITHOUT and WITH shuffle (worst case input already)" << endl;

    while (cin >> x) vec1.push_back(x);
    cin.clear();
    getline(cin, str);
    cout <<  setw(40) << "[Array 1] The compare numbers WITHOUT shuffle is: " << Quick<int>::sortWithoutShuffle(vec1) << endl;
    compareCount = 0;
    cout <<  setw(40) << "[Array 1] The compare numbers WITH shuffle is: " << Quick<int>::sortWithShuffle(vec1) << endl;
    compareCount = 0;
    cout << endl; 

    while (cin >> x) vec2.push_back(x);
    cin.clear();
    getline(cin, str);
    cout <<  setw(40) << "[Array 2] The compare numbers WITHOUT shuffle is: " << Quick<int>::sortWithoutShuffle(vec2) << endl;
    compareCount = 0;
    cout <<  setw(40) << "[Array 2] The compare numbers WITH shuffle is: " << Quick<int>::sortWithShuffle(vec2) << endl;
    compareCount = 0;
    cout << endl;

    while (cin >> x) vec3.push_back(x);
    cin.clear();
    getline(cin, str);
    cout <<  setw(40) << "[Array 3] The compare numbers WITHOUT shuffle is: " << Quick<int>::sortWithoutShuffle(vec3) << endl;
    compareCount = 0;
    cout <<  setw(40) << "[Array 3] The compare numbers WITH shuffle is: " << Quick<int>::sortWithShuffle(vec3) << endl;
    compareCount = 0;
    cout << endl;

    while (cin >> x) vec4.push_back(x);
    cin.clear();
    getline(cin, str);
    cout <<  setw(40) << "[Array 4] The compare numbers WITHOUT shuffle is: " << Quick<int>::sortWithoutShuffle(vec4) << endl;
    compareCount = 0;
    cout <<  setw(40) << "[Array 4] The compare numbers WITH shuffle is: " << Quick<int>::sortWithShuffle(vec4) << endl;
    compareCount = 0;
    cout << endl;

    while (cin >> x) vec5.push_back(x);
    cin.clear();
    getline(cin, str);
    cout <<  setw(40) << "[Array 5] The compare numbers WITHOUT shuffle is: " << Quick<int>::sortWithoutShuffle(vec5) << endl;
    compareCount = 0;
    cout <<  setw(40) << "[Array 5] The compare numbers WITH shuffle is: " << Quick<int>::sortWithShuffle(vec5) << endl;
    compareCount = 0;
    cout << endl;

    while (cin >> x) vec6.push_back(x);
    cin.clear();
    getline(cin, str);
    cout <<  setw(40) << "[Array 6] The compare numbers WITHOUT shuffle is: " << Quick<int>::sortWithoutShuffle(vec6) << endl;
    compareCount = 0;
    cout <<  setw(40) << "[Array 6] The compare numbers WITH shuffle is: " << Quick<int>::sortWithShuffle(vec6) << endl;

    return 0;

}
