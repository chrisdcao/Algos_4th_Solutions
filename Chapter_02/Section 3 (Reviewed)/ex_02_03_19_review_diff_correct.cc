#include <iostream>
#include <vector>
#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

template <typename T>
class QuickMedianOf5 {
private:

    static int partition(vector<T>& a, T arrayOfFive[5], int lo, int hi) {
        int i = lo, j = hi+1; 
        T pivot = medianOfFive(a, arrayOfFive, lo, hi);
        while (true) {
            while (a[++i] < pivot);
            while (pivot < a[--j]);
            if (i >= j) break;
            swap(a[i], a[j]);
        }
        swap(a[lo], a[j]);
        return j;
    }

    static void sort(vector<T>& a, T arrayOfFive[5], int lo, int hi)  {
        if (hi <= lo) return;
        int j = partition(a, arrayOfFive, lo, hi);
        sort(a, arrayOfFive, lo, j-1);
        sort(a, arrayOfFive, j+1, hi);
    }

    /* put the elements in an array of 5, then I use InsertionSort (fastest for size < 43). Since the array is sorted, the middle element will be the median of the array */
    static T medianOfFive(vector<T>& a, T arrayOfFive[5], int lo, int hi) {
        /* if the sub arrays have less than 5 then we just do it like normal quickSort */
        if (hi - lo < 3) return a[lo];
        int mid = lo + (hi - lo) / 2;
        int midLeft = lo + (mid - lo) / 2;
        int midRight = mid + (hi - mid ) / 2;
        /* intialize the array with 5 elements from the vector */
        arrayOfFive[0] = a[lo];
        arrayOfFive[1] = a[midLeft];
        arrayOfFive[2] = a[mid];
        arrayOfFive[3] = a[midRight];
        arrayOfFive[4] = a[hi];
        /* sort the array, this makes the median in the middle */
        insertionSort(arrayOfFive);
        /* Put the in-order elements back into the vector */
        a[lo] = arrayOfFive[0];
        a[midLeft] = arrayOfFive[1];
        a[mid] = arrayOfFive[2];
        a[midRight] = arrayOfFive[3];
        a[hi] = arrayOfFive[4];
        /* swap the median with lo, and from then on it's like normal quicksort */
        swap(a[mid], a[lo]);
        return a[lo];
    }

    static void insertionSort(T a[5]) {
        for (int i = 0; i < 5; i++) 
            for (int j = i; j > 0 && a[j] < a[j-1]; j--) {
                T temp = a[j];
                a[j] = a[j-1];
                a[j-1] = temp;
            }
    }

public:

    static void sort(vector<T>& a) {
        T arrayOfFive[5];
        uniformShuffle(a);
        /* optimized in not having to set bounds */
        T currentMax = a[0];
        int currentIndex = 0;
        for (int i = 0; i < a.size(); i++) {
            if (a[i] > currentMax) {
                currentMax = a[i];
                currentIndex = i;
            }
        }
        swap(a[currentIndex], a[a.size()-1]);
        sort(a, arrayOfFive, 0, a.size() - 1);
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;

    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N));

    cout << "The array before sorting: " << vec << endl;
    QuickMedianOf5<int>::sort(vec);
    cout << "The array after sorting: " << vec << endl;

    return 0;

}

