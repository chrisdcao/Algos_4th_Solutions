#include <iostream>
#include <iomanip>
#include <vector>
#include <random>
#include <numeric>
#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

template <typename T>
class QuickMedianOf3  {
private:

    static T medianOfThree(vector<T>& a, int lo, int hi) {
        int mid = lo + (hi - lo) / 2;
        if (a[lo] > a[hi]) swap(a[lo], a[hi]);
        if (a[mid] < a[lo]) swap(a[mid], a[lo]);
        if (a[mid] > a[hi]) swap(a[mid], a[hi]);
        /* swap the median value with lo position, thus we don't have to change the remaining code */
        swap(a[mid], a[lo]);
        return a[lo];
    }

    static int partition(vector<T>& a, int lo, int hi) {
        int i = lo, j = hi + 1;
        T pivot = medianOfThree(a, lo, hi);
        while (true) {
            while (a[++i] < pivot);
            while (pivot < a[--j]);
            if (i >= j) break;
            swap(a[i], a[j]);
        }
        swap(a[lo], a[j]);
        return j;
    }

    static void sort(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        int j = partition(a, lo, hi);
        sort(a, lo, j-1);
        sort(a, j+1, hi);
    }

public:
    
    static void sort(vector<T>& a) {
        uniformShuffle(a);

        T currentMax = a[0];
        int currentIndex = 0;

        for (int i = 0; i < a.size(); i++) {
            if (a[i] > currentMax) {
                currentMax = a[i];
                currentIndex = i;
            }
        }
        swap(a[currentIndex], a[a.size()-1]);

        sort(a, 0, a.size() - 1);
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;

    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N));

    cout << "The array before sorting: " << vec << endl;
    QuickMedianOf3<int>::sort(vec);
    cout << "The array after sorting: " << vec << endl;

    return 0;

}

