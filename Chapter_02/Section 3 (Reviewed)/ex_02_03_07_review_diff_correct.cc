#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <climits>
#include "Random.h"

using namespace std;

static int countSize0 = 0,
           countSize1 = 0,
           countSize2 = 0;
/* find the number of sub-arrays of size 0, 1, 2 (appear when recusion happens to divide the big array and keep applying the paritioning on it)
 * could prove by math or experiment-driven hypotheses */
template <typename T>
class Quick {
private:

    static int partition(vector<T>& a, int lo, int hi) {
        int i = lo, j = hi + 1;
        T v = a[lo];
        while (true) {
            while (a[++i] < v) if (i == hi) break;
            while (v < a[--j]) if (j == lo) break;
            if (i >= j) break;
            swap(a[i], a[j]);
        }
        swap(a[j], a[lo]);
        return j;
    }

    static void sort(vector<T>& a, int lo, int hi) {
        /* if (hi - lo < 0) that means there's no sub-array, that means we have an empty sub-array (array size 0) 
         * if (hi - lo == 0) that means 2 elements is the same element -> the sub-array has one element (array size 1)
         * if (hi - lo == 1) that means a[hi] is one index apart from a[lo], meaning array has 2 distincts element (array size 2) */
        /* by placing these conditions at the beginning, every loop of recursion (both left and right side) will have to increment up */
        if (hi - lo < 0) countSize0++;
        if (hi - lo == 0) countSize1++;
        if (hi - lo == 1) countSize2++;
        /* this place after so that we don't get skipped on size0 and size1 */
        if (hi <= lo) return;
        int j = partition(a, lo, hi);
        sort(a, lo, j-1);
        sort(a, j+1, hi);
    }

public:

    static void sort(vector<T>& a) {
        random_shuffle(a.begin(), a.end());
        sort(a, 0, a.size() - 1);
    }

};

/* test client to confirm my explanation */
int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec(N);
    /* init the array with distinct keys */
    iota(vec.begin(), vec.end(), randomUniformDistribution(0, N-1));
    uniformShuffle(vec);

    cout << "Array size is: " << vec.size() << endl;

    /* cout << "The array after shuffling is: ";
     * for (auto s : vec)
     *     cout << s << " ";
     * cout << endl;  */

    Quick<int>::sort(vec);

    cout << countSize0 << " " << countSize1 << " " << countSize2 << endl;

    return 0;

}
