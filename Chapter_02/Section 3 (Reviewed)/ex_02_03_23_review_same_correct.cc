#include <iostream>
#include <vector>
#include <random>
#include <numeric>
#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

template <typename T>
class JavaSystem {
private:

    static void insertionSort(vector<T>& a, int lo, int hi) {
        int N = hi - lo + 1;
        for (int i = 0; i < N; i++) {
            for (int j = i; j > 0 && a[j] < a[j-1]; j--)
                swap(a[j], a[j-1]);
        }
    }

    static T medianOfThree(vector<T>& a, int lo, int hi) {
        int mid = lo + (hi - lo) / 2;
        if (a[lo] > a[hi]) swap(a[lo], a[hi]);
        if (a[mid] < a[lo]) swap(a[lo], a[mid]);
        if (a[mid] > a[hi]) swap(a[mid], a[hi]);
        swap(a[lo], a[mid]);
        return a[lo];
    }

    static void sort(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        /* cutoff for small subarrays */
        if (hi - lo <= 43) {
            insertionSort(a, lo, hi);
            return;
        } else {
            int i = lo, j = hi+1, p = lo, q = hi;
            /* using median of 3 instead of normal pivot calculation */
            T pivot = medianOfThree(a, lo, hi);
            while (true) {
                if (a[i] == pivot) swap(a[i], a[p++]);
                else if (a[j] == pivot && j != hi + 1) swap(a[j], a[q--]);
                while (a[++i] < pivot) if (i == hi) break;
                while (a[--j] > pivot) if (j == lo) break;
                if (i >= j) break;
                swap(a[i], a[j]);
            }
            i = j + 1;
            while (p != lo) swap(a[--p], a[j--]);
            while (q != hi) swap(a[++q], a[i++]);
            sort(a, lo, j);
            sort(a, i, hi);
        }
    }

public:

    static void sort(vector<T>& a) {
        uniformShuffle(a);
        sort(a, 0, a.size()-1);
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));
    /* for (int i = 0; i < N; i++)
     *     vec.push_back(randomUniformDistribution(0, N)); */
    cout << "The array before sorting: " << vec << endl;
    JavaSystem<int>::sort(vec);
    cout << "The array after sorting: " << vec << endl;

    return 0;

}
