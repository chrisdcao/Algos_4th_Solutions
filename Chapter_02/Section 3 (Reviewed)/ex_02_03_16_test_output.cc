#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include "Random.h"
#include "VectorUtility.h"

using namespace std;

static int recursionCount = 0;
template <typename T>
class Quick {
private:

    static void sort(vector<T>& a, int lo, int hi) {
        recursionCount++;
        if (hi <= lo) return;
        int j = partition(a,lo,hi);
        sort(a,lo,j-1);
        sort(a,j+1, hi);
    }

    static int partition(vector<T>& a, int lo, int hi) {
        int i = lo, j = hi + 1;
        T v = a[lo];
        while(true) {
            while (a[++i] < v)  { if (i == hi) break; }
            while (v < a[--j])  { if (j == lo) break; }
            /* print out the position of i and j (after iterating) to know the position of partition */
            if (i >= j)  {
                cout << "i: " << i << " j: " << j  << endl;
                break;
            }
            swap(a[i], a[j]);
        }
        swap(a[lo], a[j]);
        return j;
    }

public:

    static int sort(vector<T>& a) {
        /* random_shuffle(a.begin(), a.end()); */
        recursionCount = 0;
        sort(a, 0, a.size() - 1);
        return recursionCount;
    }

};

int main(int argc, char** argv) {
    
    /* Input the output of ex_02_03_06 here to test the partition division */
    vector<int> assumedVector = {25,9,0,15,22,40,29,31,49,51};
    cout << "Assumed vector: " << assumedVector << endl;

    return 0; 

}

