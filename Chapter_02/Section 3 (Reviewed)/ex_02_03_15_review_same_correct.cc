/* 1. Overall Goal: to sort bolt and nuts in the same order, then logically nuts and bolds should fit(since logically large nuts should fit with large bolt)
 * 
 * 2. Method and Explanation:
 * Using nut as pivot, and I compare it with bolts and sort the bolts based on the NutPivots. Similarly, using bolt as pivot I can sort the nut. Using this method, I sort them simultaneously until both arrays are sorted in ascending order. Since they fit based on the size (large bolt with large nut) and the number of bolts = number of nuts, so logically after being sorted in the same order they should be fitted from first to last respectively
 * 
 * 3. Total expected cost: ~ 3N log N*/

#include <iostream>
#include <vector>
#include <numeric>
#include "Random.h"

using namespace std;

template <typename T> 
class Quick {
private:

    static void sortExternalPivot(vector<T>& vecNuts, vector<T>& vecBolts, int lo, int hi) {
        if (hi <= lo) return;

        int leftNuts = lo, iNuts = lo + 1,  rightNuts = hi;
        int leftBolts = lo, iBolts = lo + 1,  rightBolts = hi;
        /* Defaultly vecNuts[lo] to be vecNuts pivot (no operations or comparison), then find the matching value in the vecBolts, then (in vecBolts) swap that value with first. Summing from all recursion depth, this cost ~ N time at each depth
         * This will make the 2 array always have the same pivot at the same position*/
        int boltIndex = lo;
        for (/* */; boltIndex < hi; boltIndex++) {
            if (vecBolts[boltIndex] == vecNuts[lo]) break;
        }
        swap(vecBolts[boltIndex], vecBolts[lo]);
        /* From here we can take pivot from each other array, since we are NOT allowed to compare nuts to nuts, bolts to bolts directly */
        T nutsPivot = vecBolts[lo];
        T boltsPivot = vecNuts[lo];
        /* Other operations similar to normal quickSort */
        while (iNuts <= rightNuts) {
            if      (vecNuts[iNuts] < nutsPivot) swap(vecNuts[iNuts++], vecNuts[leftNuts++]);
            else if (vecNuts[iNuts] > nutsPivot) swap(vecNuts[iNuts], vecNuts[rightNuts--]);
            else                                 iNuts++;
        }

        while (iBolts <= rightBolts) {
            if      (vecBolts[iBolts] < boltsPivot) swap(vecBolts[iBolts++], vecBolts[leftBolts++]);
            else if (vecBolts[iBolts] > boltsPivot) swap(vecBolts[iBolts], vecBolts[rightBolts--]);
            else                                    iBolts++;
        }

        sortExternalPivot(vecNuts, vecBolts, lo, leftNuts-1);
        sortExternalPivot(vecNuts, vecBolts, rightNuts+1, hi);
    }

public:

    static void sortNutsAndBolts(vector<T>& vecNuts, vector<T>& vecBolts) {
        /* Initial Shuffle */
        uniformShuffle(vecNuts);
        uniformShuffle(vecBolts);
        /* like normal quickSort */
        sortExternalPivot(vecNuts, vecBolts, 0, vecNuts.size()-1);
    }

};

int main(int argc, char** argv) {

    /* INITIAL SETUP */
    /* setup 2 random-sorted Nuts and Bolts arrays */
    int N = stoi(argv[1]);
    vector<int> vecNuts(N);

    iota(vecNuts.begin(), vecNuts.end(), randomUniformDistribution(0, N));
    vector<int> vecBolts = vecNuts;

    uniformShuffle(vecNuts);
    uniformShuffle(vecBolts);

    Quick<int>::sortNutsAndBolts(vecNuts, vecBolts);

    for (auto nut : vecNuts)
        cout << nut << " ";
    cout << endl;

    for (auto bolt : vecBolts)
        cout << bolt << " ";
    cout << endl;

}
