#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <random>
#include <iomanip>
#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

/* The exercise requires that the best-case will be when partition divides the array in halves (2 subarrays not differ by more than 1 element) at any depth of recursion
 * This class generates an array based on that idea */
template <typename T>
class Quick{
private:

    static vector<T> generateRandomSortedArray(int N) {
        vector<T> vec;
        int value = randomUniformDistribution(0, N-1);
        for (int i = 0; i < N; i++) {
            vec.push_back(value);
            value += randomUniformDistribution(1, N-1);
        } 
        /* vector<T> vec(N);
         * iota(vec.begin(), vec.end(), 1); */
        return vec;
    }

    static void createBestCase(vector<T>& a, int lo, int hi) {
        if (lo >= hi) return;
        int mid = lo + (hi - lo) / 2;
        T midValue = a[mid];
        for (int i = mid; i > lo; i--) a[i] = a[i-1];
        a[lo] = midValue;
        createBestCase(a, lo+1, mid);
        createBestCase(a, mid+1, hi);
    }

public:

    static vector<T> bestCaseArray(int N) {
        vector<T> result = generateRandomSortedArray(N);
        createBestCase(result, 0, result.size() - 1);
        return result;
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec = Quick<int>::bestCaseArray(N);
    cout << vec << endl;

    return 0;

}
