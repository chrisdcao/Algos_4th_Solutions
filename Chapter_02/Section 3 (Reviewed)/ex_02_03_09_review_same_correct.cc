#include <iostream>
#include <algorithm>
#include <random>
#include <iomanip>
#include "Random.h"
#include <vector>

using namespace std;

/* EXPLANATION (for normal quicksort):
 * when the sorting only have 2 distinct keys, then all the recursions after the first will be wasted (as the array is already sorted from the first), so in the end it will still cost NlogN compares (condition checking)
 * When the soritng only has 3 distinct keys, then one major side (from the first recursion) will be wasted from there on, as there will be only 2 distinct keys need to be sorted on one side. Still, it has to proceed NlogN compares */
/* In both cases, using quickSort3Way will reduce the travel time to linear, explained below */
template <typename T>
class Quick {
private:

    static void partition(vector<T>& a, int lo, int hi) {
        int i = lo, j = hi + 1;
        T pivot = a[lo];
        while (true) {
            while (a[++i] < pivot) if (i == hi) break;
            while (pivot < a[--j]) if (j == lo) break;
            if (i >= j) break;
            swap(a[i], a[j]);
        }
        swap(a[lo], a[j]);
        return j;
    }

    static void sort(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        int j = partition(a, lo, hi);
        sort(a, lo, j-1);
        sort(a, j+1, hi);
    }

    static void sort3Way(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        int left = lo, i = lo + 1, right = hi;
        T pivot = a[lo];
        while (i <= right) {
            if (a[i] < pivot) swap(a[i++], a[left++]);
            else if (a[i] > pivot) swap(a[i], a[right--]);
            else ++i;
        }
        /* by using mergesort3Way with 2 and 3 distinct keys, recursion will stop right at the second call (for 2 distinc keys) and at third call (for 3 distinct keys), making the total cost ~ liniear */
        sort(a, lo, left-1);
        sort(a, right+1, hi);
    }

public:

    static void sort(vector<T>& a) {
        uniformShuffle(a);
        sort(a, 0, a.size() - 1);
    }

};

