#include <iostream>
#include <algorithm>
#include <random>
#include <vector>
#include "Random.h"

using namespace std;

/* since we have only 2 keys, after the partitioning the sorting is done (since all the smaller on the left and all larger on the right for 2 distinct is SORTED)
 * Thus we don't need to have recursion 
 * This partitioning is based on QuickSort3Way (rather than orginal QuickSort) */
template <typename T>
class Quick2Keys {
private:

    static void sort(vector<T>& a, int lo, int hi) {
        int left = lo, i = lo + 1, right = hi;
        T pivot = a[lo];
        while (i <= right) {
            if (a[i] < pivot) swap(a[i++], a[left++]);
            else if (a[i] > pivot) swap(a[i], a[right--]);
            else ++i;
        }
    }

public:

    static void sort(vector<T>& a) {
        sort(a, 0, a.size()-1);
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)    
        /* it will only take either 0 or 1 (2 keys) */
        vec.push_back(randomUniformDistribution(0,1));

    cout << "The array before sorting is: ";
    for (auto s :vec)
        cout << s << " ";
    cout << endl;

    Quick2Keys<int>::sort(vec);

    cout << "The array after sorting is: ";
    for (auto s :vec)
        cout << s << " ";
    cout << endl;

}
