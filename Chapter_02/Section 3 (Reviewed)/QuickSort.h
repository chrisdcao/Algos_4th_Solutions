#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include "Random.h"

using namespace std;

template <typename T>
class Quick {
private:

    static void sort(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        /* this one will give back the value of j being the one in the middle and also the median of the array */
        int j = partition(a,lo,hi);
        sort(a,lo,j-1);
        sort(a,j+1, hi);
    }

    /* in this partitioning process we are finding the place (in the array) for the pivot point we pick
     * in this case we pick low as our pivot point, so we are looking for the right place for 'low' value in the array */
    static int partition(vector<T>& a, int lo, int hi) {
        int i = lo, j = hi + 1;
        T v = a[lo];
        while(true) {
            /* we keep incremeneting the low, if it's smaller than v then we keep moving. Until it breaks it means we have found an 'i' value where a[i] does not belong to that group */
            while (a[++i] < v) if (i == hi) break;
            /* Done looking for an i that doesn't belong to the group. We move on to check the j until we found a 'j' that does not belong to the group */
            while (v < a[--j]) if (j == lo) break;
            /* this is to set the end for this while(true) loop, is when the i begin to look for repeated values (values we have already checked when we move the 'j' pointer) */
            if (i >= j) break;
            /* and now we swap the 2 'unsatisfied' elements in the 2 groups, so that they become satisfied
             * the 'while(true)' loop keeps going until the above condition is met. This means that all the elements are now in their partitioning place */
            cout << "i: " << i << " j: " << j  << endl;
            swap(a[i], a[j]);
        }
        /* since  we have to check 2 small while loops one last time before reaching (i>=j) condition, i will be 1 step more than where the median should be and j is right at the boundary of smaller and larger
         * so we swap j and lo is now at the boundary (median point - correct point) and j (which is < v) is at the top */
        swap(a[lo], a[j]);
        return j;
    }

public:

    static void sort(vector<T>& a) {
        random_shuffle(a.begin(), a.end());
        sort(a, 0, a.size() - 1);
    }

};
