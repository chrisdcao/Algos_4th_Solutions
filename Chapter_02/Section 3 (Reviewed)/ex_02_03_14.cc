The compare happens if either i or j is chosen as pivot (at any given recursion depth, not necessarily in the first recursion).

Therefore, we calculate the probability of i and j being picked from sequence [i, j] (probability that partition at any stage of recursion cuts into either i or j so that they will be chose as pivot in the next recursion), rather than probability of either i or j being picked from N

The probability that i OR j happens to be pivot (at any stage) is: 2/(j - i + 1), assuming that j > i
