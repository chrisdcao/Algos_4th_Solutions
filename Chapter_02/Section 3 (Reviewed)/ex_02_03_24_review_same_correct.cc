/* QUESTION: WHERE DO WE APPLY THE RE-ARRANGING TWO HALVES OF THE ARRAYS? */
#include <iostream>
#include <vector>
#include <random>
#include <numeric>
#include <iomanip>
#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

template <typename T>
class Sample {
private:

    static void insertionSort(vector<T>& a, int lo, int hi) {
        int N = hi - lo + 1;
        for (int i = 0; i < N; i++) {
            for (int j = i; j > 0 && a[j] < a[j-1]; j--)
                swap(a[j], a[j-1]);
        }
    }

    static void sort(vector<T>& a, int lo, int hi, int sampleSize) {
        if (lo >= hi) return;
        int j = partition(a, lo, hi, sampleSize);
        sort(a, lo, j-1, sampleSize);
        sort(a, j+1, hi, sampleSize);
    }

    static int partition(vector<T>& a, int lo, int hi, int sampleSize) {
        /* this implementation is implying that until the sample size > hi - lo + 1, we should keep on sorting the sample and exchange the sample with the lo to perform the normal QuickSort */
        if (sampleSize <= hi - lo + 1) {
            insertionSort(a, lo, lo + sampleSize - 1);
            /* we switch 'median' with 'lo' so that it becomes pivot */
            int pivotIndex = lo + sampleSize / 2;
            swap(a[lo], a[pivotIndex]);
        }
        /* normal QuickSorting from here */
        T pivot = a[lo];
        int i = lo, j = hi + 1;
        while (true) {
            while (a[++i] < pivot) if (i == hi) break;
            while (a[--j] > pivot) if (j == lo) break;
            if (i >= j) break;
            swap(a[i], a[j]);
        }
        swap(a[j], a[lo]);
        return j;
    }

public:

    static void sort(vector<T>& a, int sampleSize) {
        uniformShuffle(a);
        sort(a, 0, a.size() - 1, sampleSize);
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));
    /* for (int i = 0; i < N; i++)
     *     vec.push_back(randomUniformDistribution(0, N)); */
    cout << "The array before sorting: " << vec << endl;
    Sample<int>::sort(vec, 21);
    cout << "The array after sorting: " << vec << endl;

    return 0;

}

