#include <iostream>
#include <numeric>
#include <iomanip>
#include <random>
#include <algorithm>
#include "ContainerUtility.h"
#include "Random.h"

using namespace std;

template <typename T>
class QuickFast3Way {
private:

    static void sort(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        int i = lo, j = hi + 1, p = lo, q = hi;    
        T pivot = a[lo];
        while (true) {
            if (a[i] == pivot) swap(a[i], a[p++]);
            /* have to be else if to continually check the (i<=j below) */
            else if (a[j] == pivot && j != hi + 1) swap(a[j], a[q--]);
            while (a[++i] < pivot) if (i == hi) break;
            while (a[--j] > pivot) if (j == lo) break;
            if (i >= j) break;
            swap(a[i], a[j]);
        }
        i = j + 1;
        while (p != lo) swap(a[--p], a[j--]);
        while (q != hi) swap(a[++q], a[i++]);
        sort(a, lo, j);
        sort(a, i, hi);
    }

public:

    static void sort(vector<T>& a) {
        uniformShuffle(a);
        cout << "Array after init shuffle: " << a << endl << endl;
        sort(a, 0, a.size()-1);
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));
    /* for (int i = 0; i < N; i++)
     *     vec.push_back(randomUniformDistribution(0, N)); */
    cout << "The array before sorting: " << vec << endl;
    QuickFast3Way<int>::sort(vec);
    cout << "The array after sorting: " << vec << endl;

    return 0;

}
