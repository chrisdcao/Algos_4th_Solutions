#include <iostream>
#include <vector>
#include <numeric>
#include <iomanip>
#include "ContainerUtility.h"
#include "Random.h"

using namespace std;

template <typename T>
class Quick {
private:

    static int partition(vector<T>& a, int lo, int hi) {
        int i = lo, j = hi + 1;
        T pivot = a[lo];
        while (true) {
            /* in this algorithm 'i' will go to the end of the array in the worst case (rather than the end of the sub-array) */
            while (a[++i] < pivot);
            while (pivot < a[--j]);
            if (i >= j) break;
            swap(a[i], a[j]);
        }
        /* since we return j so wherever i stops doesn't matter. Plus the position of i will be resetted everytime we run again */
        swap(a[lo], a[j]);
        return j;
    }

    static void sort(vector<T>& a, int lo, int hi) {
        if (lo >= hi) return;
        int j = partition(a, lo, hi);
        sort(a, lo, j-1);
        sort(a, j+1, hi);
    }

public:

    static void sort(vector<T>& a) {
        uniformShuffle(a);
        T currentMax = a[0];
        int currentIndex = 0;
        for (int i = 0; i < a.size(); ++i) {
            if (a[i] > currentMax) {
                currentMax = a[i];
                currentIndex = i;
            }
        }
        swap(a[currentIndex], a[a.size() - 1]);
        sort(a, 0, a.size() - 1);
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec(N);
    iota(vec.begin(), vec.end(), 0);
    uniformShuffle(vec);

    cout << "Array before sorting: " << endl;
    cout << vec << endl;

    Quick<int>::sort(vec);

    cout << "Array after sorting: " << endl;
    cout << vec << endl;

    return 0;

}
