#include <iostream>
#include <vector>
#include "Random.h"
#include "ContainerUtility.h"
#include "Stack.h"

using namespace std;

class QuickSortRange {
public:
    int low;
    int high;
    QuickSortRange() = default;
    QuickSortRange(int lo, int hi): low(lo), high(hi) {}
};

template <typename T>
class QuickNonRecursive {

private:

    static int partition(vector<T>& a, int lo, int hi) {
        int i = lo, j = hi + 1;
        T pivot = a[lo];
        while (true) {
            while (a[++i] < pivot) if (i == hi) break;
            while (pivot < a[--j]) if (j == lo) break;
            if (i >= j) break;
            swap(a[i], a[j]);
        }
        swap(a[lo], a[j]);
        return j;
    }

    static void sort(vector<T>& array, int low, int high) {
        Stack<QuickSortRange> stack;
        QuickSortRange quickSortRange(low, high);
        /* we first init it with the largest range (the whole array - that needs to be sorted) */
        stack.push(quickSortRange);
        while (!stack.isEmpty()) {
            /* then we pop the stack out, store into currentQuickSortRange */
            QuickSortRange currentQuickSortRange = stack.pop();
            /* we partition with the current hi-lo range */
            int j = partition(array, currentQuickSortRange.low, currentQuickSortRange.high);
            /* Then we separate 2 partitioned arrays into 2 sub-arrays */
            QuickSortRange leftQuickSortRange(currentQuickSortRange.low, j-1);
            QuickSortRange rightQuickSortRange(j+1, currentQuickSortRange.high);
            /* and we store their sizes as well */
            int leftSubArraySize = j - currentQuickSortRange.low;
            int rightSubArraySize = currentQuickSortRange.high - j;

            /* the big if-else block is to test which sub-array to take from first (which is the larger one of the 2) */
            if (leftSubArraySize > rightSubArraySize) {
                if (leftSubArraySize > 1) stack.push(leftQuickSortRange);
                if (rightSubArraySize > 1) stack.push(rightQuickSortRange);
            } else {
                if (rightSubArraySize > 1) stack.push(rightQuickSortRange);
                if (leftSubArraySize > 1) stack.push(leftQuickSortRange);
            }
        }
    }

public:

    static void sort(vector<T>& a) {
        uniformShuffle(a);
        sort(a, 0, a.size() - 1);
    }
    
};
 
int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));

    cout << "The array before sorting is: " << vec << endl;

    QuickNonRecursive<int>::sort(vec);

    cout << "The array after sorting is: " << vec << endl;

    return 0;

}
