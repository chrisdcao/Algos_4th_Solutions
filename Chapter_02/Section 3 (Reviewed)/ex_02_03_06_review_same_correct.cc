#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <random>
#include "Random.h"
#include <cmath>

using namespace std;

static int compareCount = 0;

template <typename T>
class Quick {
private:

    static void sort(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        int j = partition(a, lo, hi);
        sort(a, lo, j-1);
        sort(a, j+1, hi);
    }

    static int partition(vector<T>& a, int lo ,int hi) {
        int i = lo, j = hi + 1;
        T pivot = a[lo];
        while (true) {
            /* even when failing, the condition still have to compare one last time (so that it knows the condition fails), and thus we have to have increment outoside */
            compareCount++;
            while (a[++i] < pivot) {
                compareCount++;
                if (i == hi) break;
            }
            compareCount++;
            while (pivot < a[--j])  {
                compareCount++;
                if (j == lo) break;
            }
            if (i >= j) break;
            swap(a[i], a[j]);
        }
        swap(a[lo], a[j]);
        return j;
    }

public:

     static int sort(vector<T>& a) {
        compareCount = 0;
        random_shuffle(a.begin(), a.end());
        sort(a, 0, a.size() - 1);
        return compareCount;
     }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;
    cout << "Doing trials on array of " << N << " elements" << endl;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));
    cout << vec.size() << endl;
    int approx = 2 *  N * log(N);
    cout << "The EXACT number of compares (Cn)         : " << Quick<int>::sort(vec) << endl;
    cout << "The APPROXIMATE number of compares (NlogN): " << approx << endl;

    return 0;

}
