#include <iostream>
#include <cstring>
#include <fstream>
#include <memory>
#include "SymbolTables.h"
#include "Queue.h"

using namespace std;

vector<string> split(string str, char delimiter) {
    int pos1 = 0;
    int pos2 = str.find(delimiter);
    vector<string> splittedStrings;
    while (pos2 < str.size()) {
        splittedStrings.push_back(str.substr(pos1,pos2-pos1));
        pos1 = pos2 + 1; // after the delimiter
        pos2 = str.find(delimiter, pos1);
    }
    if (pos1 != str.size()-1)
        splittedStrings.push_back(str.substr(pos1,str.size()-1));
    return splittedStrings;
}

int main() {
    string fileName; 
    char separator;
    cin >> fileName >> separator;
    ifstream input(fileName);
    LinearProbingHashST<string, Queue<string>*> st;
    
    while (!input.eof()) {
        string line; 
        getline(input,line);
        vector<string> a= split(line, separator); // split based on the separator we get from the arg above
        string key = a[0];
        for (int i = 1; i < a.size(); i++) {
            string val = a[i];
            if (!st.contains(key)) st.put(key, new Queue<string>()); // st<string, Queue*>
            else                   st.get(key)->enqueue(val);
        }
    }
    string query;
    while (cin >> query) {
        if (st.contains(query)){
            cout << "The value of key \"" << query << "\" are: ";
            for (auto s : *st.get(query))
                cout << s << " ";
        }
        cout << endl;
    }
}
