#include <iostream>
#include <vector>
#include <sstream>
#include <memory>
#include <cstring>
#include <iomanip>

using namespace std;

template <typename Key>
class BinarySearchST {
private:
    vector<Key> keys;

public:
    friend ostream& operator<<  (ostream& os, const BinarySearchST& st) {
        for (const auto& s : st.keys)
            os << s << " ";
        return os;
    }

    int size() { return keys.size(); }

    bool isEmpty() { return size() == 0; }

    void put(Key key) {
        if (isEmpty()) {
            keys.push_back(key);
            return;
        }
        int i = rank(key);
        if (keys[i] == key) return;
        keys.insert(keys.begin()+i, key);
    }

    void deleteKey(Key key) {
        int i = rank(key);
        if (keys[i] == key)
            keys.erase(keys.begin+i);
    }

    int rank(Key key) {
        int lo = 0, hi = keys.size()-1;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            if (key < keys[mid]) hi = mid-1;
            else if (key > keys[mid]) lo = mid + 1;
            else return mid;
        }
        return lo;
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }
};

template <typename Key>
class SET {
private:
    BinarySearchST<Key> st;

public:
    SET() = default;

    SET(int N): st(N) {}

    void add(Key key) { st.put(key); }

    void deleteKey(Key key) { st.deleteKey(key); }

    bool isEmpty() { return st.isEmpty(); }

    int size() { return st.size(); }

    string toString() { return st.toString(); }

};


int main() {
    SET<char> set;
    int length;
    cin >> length;
    char c;
    for (int i = 0; i < length; i++) {
        cin >> c;
        set.add(c);
    }
    cout << "Set: " << set.toString() << endl;
}
