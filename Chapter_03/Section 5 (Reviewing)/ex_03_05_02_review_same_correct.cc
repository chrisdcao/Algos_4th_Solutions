#include <iostream>
#include <vector>
#include <sstream>
#include <memory>
#include <cstring>
#include <iomanip>

using namespace std;

template <typename Key>
class SequentialSearchST {
private:
    class Node {
    public:
        Key key;
        int index = -1;
        Node* next = NULL;
    };

    Node* newNode(Key key) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        return allNodes.back().get();
    }

    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;
    int N = 0;
    Node* root = NULL;

public:
    friend ostream& operator << (ostream& os, const SequentialSearchST& st) {
        Node* temp = st.root;
        while (temp) {
            os << temp->key << " ";
            temp = temp->next;
        }
        return os;
    }

    int size() { return N; }

    bool isEmpty() { return size()==0; }

    SequentialSearchST() = default;

    void put(Key key) {
        Node* t = root;
        Node* prev = NULL;
        while (t) {
            if (t->key == key) return;
            prev = t;
            t = t->next;
        }
        if (prev == NULL) root = newNode(key);
        else prev->next = newNode(key);
        N++;
    }

    void deleteKey(Key key) {
        Node* t = root;
        if (t->key == key) {
            root = root->next;
            freeNode(t); N--;
        } else {
            Node* prev = NULL;
            while (t) {
                if (t->key == key) {
                    prev->next = t->next;
                    freeNode(t);
                    N--; return;
                }
                prev = t;
                t = t->next;
            }
        }
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

};

template <typename Key>
class SET {
private:
    SequentialSearchST<Key> st;

public:
    SET() = default;

    SET(int N): st(N) {}

    void add(Key key) { st.put(key); }

    void deleteKey(Key key) { st.deleteKey(key); }

    bool isEmpty() { return st.isEmpty(); }

    int size() { return st.size(); }

    string toString() { return st.toString(); }

};


int main() {
    SET<char> set;
    int length;
    cin >> length;
    char c;
    for (int i = 0; i < length; i++) {
        cin >> c;
        set.add(c);
    }
    cout << "Set: " << set.toString() << endl;
}
