#ifndef STRINGOPERATIONS_H
#define STRINGOPERATIONS_H

#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
#include <cstring>

using namespace std;

vector<string> split(string str, char delimiter) {
    int pos1 = 0;
    int pos2 = str.find(delimiter);
    vector<string> splittedStrings;
    while (pos2 < str.size()) {
        splittedStrings.push_back(str.substr(pos1,pos2-pos1));
        pos1 = pos2 + 1; // after the delimiter
        pos2 = str.find(delimiter, pos1);
    }
    if (pos1 != str.size()-1)
        splittedStrings.push_back(str.substr(pos1,str.size()-1));
    return splittedStrings;
}

vector<string> split(string str, string delimiter) {
    int pos1 = 0;
    int pos2 = str.find(delimiter);
    vector<string> splittedStrings;
    while (pos2 < str.size()) {
        splittedStrings.push_back(str.substr(pos1,pos2-pos1));
        pos1 = pos2 + 1; // after the delimiter
        pos2 = str.find(delimiter, pos1);
    }
    if (pos1 != str.size()-1)
        splittedStrings.push_back(str.substr(pos1,str.size()-1));
    return splittedStrings;
}
#endif
