#include <iostream>
#include <string>
#include <vector>
#include "SymbolTables.h"
#include "Bag.h"

using namespace std;

int main() {
    string str;
    int k;
    do {
        cin >> str >> k;
        if (k > str.size()) 
            cout << "k_grams must be < string size."
                 << " Please input again!" << endl;
    } while (k > str.size());
    
    RedBlackBST<string, int> bst;
    for (int a = 0; a < str.size()-k; a++) {
        string k_grams = str.substr(a,k);
        bst.put(k_grams, a);
    }
    
    for (auto s : bst.keys())
        cout << "k_grams: " << s << ". Index: " 
             << bst.get(s) << endl;
    
    return 0;
}
