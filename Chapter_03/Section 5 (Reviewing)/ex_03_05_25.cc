#include "SymbolTables.h"
#include "StringOperations.h"
#include <fstream>

using namespace std;

// Based on the exercise, no class will end on the start of the other class -> a teacher can take on classes as long as they don't have the same start time
// since we only have 5 time frame, it won't cost a lot of memory to store into a hashST though
// For the staff, I also assume that the number of staff highly likely won't cause overflow
int main() {
    SeparateChainingHashST<string, HashSET<string>*> st;
    ifstream input("ex_03_05_25_in.txt");
    while (!input.eof()) {
        string line;
        getline(input,line);
        vector<string> a = split(line,',');
        string key = a[0];
        for (int j = 1; j < a.size(); j++) {
            string str = a[j];
            if (!st.contains(key)) st.put(key, new HashSET<string>() );
            else                   st.get(key)->add(str);
        }
    }
    cout << "The teacher to assign: thinh" << endl;
    string name = "thinh";
    cout << "The desired time to assign: 1:00" << endl << endl;
    string time = "1:00";
    cout << "<!--return-->";
    if (st.contains(time)) {
        if (st.get(time)->contains(name)) 
            cout << "This teacher is already occupied at " << time << endl;
        else 
            cout << "Successfully assigned to teacher" << endl;
    }
}
