#ifndef QUEUE_H
#define QUEUE_H

#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include "Random.h"

using namespace std;

template <typename T>
class Queue {
private:

    class Node {
    public:
        T item = T();
        Node* next=NULL;
        int index = -1;
    };

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;

    Node* head=NULL;
    Node* tail=NULL;
    int N=0;

    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* initLoc) { current = initLoc; }

        Iterator& operator++() { current = current->next; return *this; }

        bool operator!=(Iterator& rhs) { return this->current != rhs.current; }
        bool operator==( Iterator& rhs ) { return this->current == rhs.current; }

        T operator*() { return this->current->item; }
    };

public:

    Queue() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    Queue(Queue<T>& rhs) {
        for (auto s : rhs) 
            this->enqueue(s);
    }

    Queue(Queue<T>&& rhs) {
        for (auto s : rhs) 
            this->enqueue(s);
    }

    ~Queue() {}

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void enqueue(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }

    T dequeue() {
        if (head == NULL)
            throw out_of_range("Underflow");
        T item = head->item;
        Node* dp = head;
        head = head->next;
        freeNode(dp);
        --N;
        return item;
    }

    T peek() {
        T temp = head->item;
        return temp;
    }

    Iterator begin() { return Iterator(head); }
    Iterator end() { return Iterator(NULL); }

    Queue& operator=(Queue& rhs) {
        while (!this->isEmpty())
            this->dequeue();
        for (auto s : rhs)
            enqueue(s);
        return *this;
    }

};

template <typename Key>
class RandomizedQueue {
private:
    class Node {
    public:
        Key key;
        int index = -1;
        Node* next = NULL;
    };
    
    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }
    
    Node* newNode(Key key) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        return allNodes.back().get();
    }

    class Iterator {
    private:
        Node* current = NULL;
    public:
        Iterator(Node* initLoc) { 
            current = initLoc; 
        }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        bool operator==(Iterator& rhs) {
            return current == rhs.current;
        }
        bool operator!=(Iterator& rhs) {
            return !operator==(rhs);
        }
        Key operator*() { return this->current->key; }
    };    
    
    Node* head = NULL;
    Node* tail = NULL;
    vector<unique_ptr<Node>> allNodes;
    int N = 0;

public:
    RandomizedQueue() = default;
    
    RandomizedQueue(const initializer_list<Key>& list) {
        for (const auto& s : list) { enqueue(s); }
    }
    int size() { return N; }
    
    bool isEmpty() { return N == 0; }
    
    void  enqueue(Key key) {
        if (head == NULL) {
            tail = newNode(key);
            head = tail;
        } else {
            Node* temp = newNode(key);
            tail->next = temp;
            tail = temp;
        }
        N++;
    }
    
    // the item remove is going to chosen randomly from the data structure
    Key dequeue() {
        if (isEmpty()) return NULL;
        if (size() == 1) {
            Key key = head->key;
            head=NULL; N--;
            return key;
        }
        int index = randomUniformDistribution(0,size()-1);
        if (index == 0) { 
            Node* oldHead = head;
            Key rv = oldHead->key;
            head = head->next;
            freeNode(oldHead); N--;
            return rv;
        }
        int count = 0;
        Node* ptr = head;
        Node* prevPtr = NULL;
        while (ptr->next) {
            if (count==index) {
                Node* oldNode = ptr;
                Key key = ptr->key;
                prevPtr->next = ptr->next;
                free(oldNode); N--;
                return key;
            }
            prevPtr = ptr;
            ptr = ptr->next;
            count++;
        }
        Key key = ptr->key;
        prevPtr->next = ptr->next;
        freeNode(ptr); N--; 
        return key;
    }
    
    Iterator begin() { 
        if (!head) throw logic_error("Queue is now empty");
        return Iterator(head); 
    }
    Iterator end() { return Iterator(NULL); }
    
};

#endif
