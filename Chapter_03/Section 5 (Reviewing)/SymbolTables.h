#ifndef SYMBOLTABLES_H
#define SYMBOLTABLES_H

#include <iostream>
#include <climits>
#include <cmath>
#include <cstring>
#include <string>
#include <memory>
#include <vector>
#include <iomanip>
#include <sstream>
#include "Queue.h"

using namespace std;

template <typename Key, typename Value>
class RedBlackBST {
    static const bool RED = true;
    static const bool BLACK = false;
private:
    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        bool color = RED;
        int N = 1;
        int index = -1;
        friend ostream& operator<<(ostream& os, const Node& h) {
            os << "Key: " << setprecision(10) << h.key << ". Value: " << h.val;
            return os;
        }
        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };

    Node* newNode(Key key, Value val, int N, bool color) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->color = color;
        allNodes.back()->N = N;
        return allNodes.back().get();
    }

    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    int size(Node* x) { return x ? x->N : 0; }
    
    bool isRed(Node* h) { return h ? h->color==RED : BLACK; }

    Node* put(Node* h, Key key, Value val) {
        if (!h) return newNode(key,val,1,RED);
        if (h->key > key) h->left = put(h->left,key,val);
        else if (h->key < key) h->right = put(h->right,key,val);
        else h->val = val;

        if (isRed(h->right) && !isRed(h->left)) h = rotateLeft(h);
        if (isRed(h->left) && isRed(h->left->left)) h = rotateRight(h);
        if (isRed(h->left) && isRed(h->right)) flipColor(h);

        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }

// remember that the deleting implmentation is ALWAYS make the Node we are about-to-step-into to be RED, so that when we delete the BLACK NODE are still balanced. When we back-propagate, we will re-balance the position of the red-links (to not violate the property, the number of modified RED link stays the same)
    //Node* deleteMin(Node* h) {
        //if (!h) return NULL;
        //if (!h->left) {
            //Node* rightChild = h->right;
            //freeNode(h);
            //return rightChild;
        //}
        //if (!isRed(h->left) && !isRed(h->left->left))
            //h = moveRedLeft(h);
        //h->left = deleteMin(h->left);
        //return balance(h);
    //}

    //Node* deleteMax(Node* h) {
        //if (!h) return NULL;
        //if (!h->right) {
            //Node* leftChild = h->left;
            //freeNode(h);
            //return leftChild;
        //}
        //if (!isRed(h->right) && !isRed(h->right->left))
            //h = moveRedRight(h);
        //h->right = deleteMax(h->right);
        //return h;
    //}

    //Node* deleteKey(Node* h, Key key) {
        //if (!h) return NULL;
        //if (h->key > key) {
            //if (!h->left) return NULL;
            //if (!isRed(h->left) && !isRed(h->left->left)) 
                //h = moveRedLeft(h);
            //h->left = deleteKey(h->left,key);
        //} else {
            //if (isRed(h->left))
                //h = rotateRight(h);
            //if (h->key == key && !h->right) {
                //freeNode(h);
                //return NULL;
            //}
            //if (!isRed(h->right) && !isRed(h->right->left)) 
                //h = moveRedRight(h);
            //if (h->key == key) {
                //Node* temp = h;
                //h = min(temp->right);
                //h->right = deleteMin(temp->right);
                //h->left = temp->left;
                //freeNode(h);
            //} else {
                //h->right =deleteKey(h->right,key);
            //}
        //}
        //return balance(h);
    //}

    Node * deleteMin( Node * h ) {
        if ( ! h ) return NULL;
        if ( ! h->left ) {
            Node* rightChild = h->right;
            freeNode( h );
            return rightChild;
        }
        if ( ! isRed( h->left ) && ! isRed( h->left->left ) ) {
            h = moveRedLeft( h );
        }
        h->left = deleteMin( h->left );
        return balance( h );
    }

    Node * deleteMax( Node * h ) {
        if ( ! h ) return NULL;

        if ( isRed( h->left ) ) {
            h = rotateRight( h );
        }

        if ( ! h->right ) {
            Node* leftChild = h->left;
            freeNode( h );
            return leftChild;
        }
        if ( ! isRed( h->right ) && ! isRed( h->right->left ) ) {
            h = moveRedRight( h );
        }
        h->right = deleteMax( h->right );
        return balance( h );
    }

    Node * deleteKey( Node * h, const Key& key ) {
        if ( ! h ) return NULL;
        if ( key < h->key ) {
            if ( ! h->left ) return NULL;

            if ( ! isRed( h->left ) && ! isRed( h->left->left ) ) {
                h = moveRedLeft( h );
            }

            h->left = deleteKey( h->left, key );
        } else {
            if ( isRed( h->left ) ) {
                h = rotateRight( h );
            }

            if ( key == h->key && ! h->right ) {
                Node* leftChild = h->left;
                freeNode( h );
                return leftChild;
            }

            if ( ! isRed( h->right ) && ! isRed( h->right->left ) ) {
                h = moveRedRight( h );
            }

            if ( key == h->key ) {
                Node * temp = h;
                h = min( temp->right );
                h->right = deleteMin( temp->right );
                h->left = temp->left;
                freeNode( temp );
            } else {
                h->right = deleteKey( h->right, key );
            }
        }
        return balance( h );
    }
    
    Node * max( Node * h ) const {
        if ( ! h ) return NULL;
        if ( ! h->right ) return h;
        return max( h->right );
    }

    Node* min(Node* h) {
        if (!h) return NULL;
        return h->left ? min(h->left) : h;
    }

    Node* get(Node* h, Key key) {
        if (!h) return NULL;
        if (h->key < key) return get(h->right,key);
        else if (h->key > key) return get(h->left,key);
        else return h;
    }

    Node* select(Node* h, int k) {
        if (!h) return NULL;
        int t = size(h->left);
        if (t > k) return select(h->left, k);
        else if (t < k) return select(h->right,k-t-1);
        else return h;
    }

// largest smaller
    Node* floor(Node* h, Key key) {
        if (!h) return NULL;
        if (h->key > key) return floor(h->left,key);
        else if (h->key == key) return h;
        Node* t = floor(h->right,key);
        return t ? t : h;
    }

// smallest larger
    Node* ceiling(Node* h, Key key) {
        if (!h) return NULL;
        if (h->key < key) return ceiling(h->right,key);
        else if (h->key == key) return h;
        Node* t = ceiling(h->left, key);
        return t ? t : h;
    }

    Node* rotateLeft(Node* h) {
        Node* x = h->right;
        h->right = x->left;
        x->left = h;
        x->color = h->color;
        h->color = RED;
        x->N = h->N;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* rotateRight(Node* h) {
        Node* x = h->left;
        h->left = x->right;
        x->right = h;
        x->N = h->N;
        x->color = h->color;
        h->N = size(h->left) + size(h->right) + 1;
        h->color = RED;
        return x;
    }

    Node* moveRedLeft(Node* h) {
        flipColor(h);
        if (isRed(h->right->left)) {
            h->right = rotateRight(h->right);
            h = rotateLeft(h);
            flipColor(h);
        }
        return h;
    }

    Node* moveRedRight(Node* h) {
        flipColor(h);
        if (isRed(h->left->left)) {
            h = rotateRight(h);
            flipColor(h);
        }
        return h;
    }

    Node* balance(Node* h) {
        if (isRed(h->right)) h=rotateLeft(h);
        if (isRed(h->left) && isRed(h->left->left)) h=rotateRight(h);
        if (isRed(h->left) && isRed(h->right)) flipColor(h);
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }

    void flipColor(Node* h) {
        h->color = !h->color;
        if (h->left) h->left->color = !h->left->color;
        if (h->right) h->right->color = !h->right->color;
    }

    int rank(Node* h, Key key) {
        if (!h) return 0;
        if (h->key < key) return rank(h->right,key) + size(h->left) + 1;
        else if (h->key > key) return rank(h->left,key);
        else return size(h->left);
    }

    void keys(Node* h, Queue<Key>& queue, Key lo, Key hi) {
        if (!h) return;
        if (h->key < lo) return;
        if (h->key > hi) return;
        keys(h->left,queue,lo,hi);
        queue.enqueue(h->key);
        keys(h->right,queue,lo,hi);
    }

    Node* root = NULL;
    vector<unique_ptr<Node>> allNodes;

public:
    Node* getRoot() { return root; }
    
    RedBlackBST() = default;
    
    RedBlackBST(const initializer_list<pair<Key,Value>>& list) {
        for (const auto& s : list) put(s.first,s.second);
    }
    
    string toString(Node* h) {
        if (!h) return "";
        string rv = "\n";
        rv += toString(h->left);
        rv += h->toString();
        rv += toString(h->right);
        return rv;
    }
    
    string toString() {
        return toString(root);
    }
    
    int size() { return size(root); }

    bool isEmpty() { return size() == 0; }

    void put(Key key, Value val) {
        root = put(root,key,val);
        root->color = BLACK;
    }

    void deleteMin() {
        if (!root) return;
        if (!isRed(root->left) && !isRed(root->right)) root->color = RED;
        root = deleteMin(root);
        if (root) root->color = BLACK;
    }

    void deleteMax() {
        if (!root) return;
        if (!isRed(root->left) && !isRed(root->right)) root->color = RED;
        root = deleteMax(root);
        if (root) root->color = BLACK;
    }

    void deleteKey(Key key) {
        if (!root) return;
        if (!get(root,key)) return;
        if (!isRed(root->left) && !isRed(root->right)) root->color = RED;
        root = deleteKey(root,key);
        if (root) root->color = BLACK;
    }

    Key max() {
        Node * rv = max( root );
        if ( rv ) {
            return rv->key;
        } else {
            throw runtime_error( "Max not found" );
        }
    }

    Key min() { return min(root)->key; }

    Value get(Key key) {
        Node* rv = get(root,key);
        return rv ? rv->val : NULL;
    }

    Key select(int k) {
        Node* rv = select(root,k);
        return rv ? rv->key : NULL;
    }

    Key floor(Key key) {
        Node* rv = floor(root,key);
        return rv ? rv->key : NULL;
    }

    Key ceiling(Key key) {
        Node* rv = ceiling(root,key);
        return rv ? rv->key : NULL;
    }

    Queue<Key> keys(Key lo, Key hi) {
        Queue<Key> queue;
        keys(root,queue,lo,hi);
        return queue;
    }

    Queue<Key> keys() { return keys(min(),max()); }
 
};

const int PRIMES[] = {
    1, 1, 3, 7, 13, 31, 61, 127, 251, 509, 1021, 2039, 4093, 8191, 16381,
    32749, 65521, 131071, 262139, 524287, 1048573, 2097143, 4194301,
    8388593, 16777213, 33554393, 67108859, 134217689, 268435399,
    536870909, 1073741789, 2147483647
};

template <typename Key, typename Value>
class LinearProbingHashST {
private:
    int hash(Key key) { 
        int hashCode = std::hash<Key>{}(key) & INT_MAX;
        hashCode = logM < 26 ? hashCode % PRIMES[logM + 5] : hashCode;
        return hashCode % M;
    }

    void resize(int cap) {
        Key** keyTemp = new Key*[cap];
        Value** valTemp = new Value*[cap];
        memset(keyTemp,0x0,sizeof(Key*)*cap);
        memset(valTemp,0x0,sizeof(Value*)*cap);
        int oldM = M;
        M = cap;
        logM = log(M) / log(2);
        if (keys) {
            for (int i = 0; i < M; i++) {
                if (keys[i]) {
                    int j;
                    for (j = hash(*keys[i]); keyTemp[j]; j = (j+1)%M);
                    keyTemp[j] = keys[i];
                    valTemp[j] = vals[i];
                }
            }
        }
        keys = keyTemp; vals = valTemp;
        keyTemp = 0x0; valTemp = 0x0;
        delete keyTemp; delete valTemp;
    }

    int getIndex(Key key) {
        int i = hash(key);
        while (keys[i]) {
            if (*keys[i] == key) return i;
            i = (i+1)%M;
        }
        return -1;
    }

    Key** keys = 0x0;
    Value** vals = 0x0;
    int M = 17;
    int N = 0;
    int logM;

public:
    int size() { return N; }
    
    bool isEmpty() { return size() == 0; }
    
    LinearProbingHashST(): LinearProbingHashST(17) {}

    LinearProbingHashST(int cap) { resize(cap); }

    void put(Key key, Value val) {
        if (N >= M/2) resize(M*2);
        int i;
        for (i = hash(key); keys[i]; i=(i+1)%M) {
            if (keys[i]) {
                if (*keys[i] == key) {
                    delete vals[i];
                    vals[i] = new Value(val);
                    return;
                }
            }
        }
        keys[i] = new Key(key);
        vals[i] = new Value(val);
        N++;
    }

    void deleteKey(Key key) {
        if (!contains(key)) return;
        int i = hash(key);
        while (*keys[i] != key) i = (i+1)%M;
        keys[i] = 0x0;
        vals[i] = 0x0;
        i = (i+1)%M;
        while (keys[i]) {
            Key keyToRedo = *keys[i];
            Value valToRedo = *vals[i];
            keys[i] = 0x0;
            vals[i] = 0x0;
            N--;
            put(keyToRedo, valToRedo);
            i = (i+1)%M;
        }
        N--;
        if (N > 0 && N == M/8) resize(M*2);
    }

    Value get(Key key) { 
        int i = getIndex(key);
        return i == -1 ? 0x0 : *vals[i];
    }

    bool contains(Key key) {
        return getIndex(key) != -1;
    }

    friend ostream& operator << (ostream& os, const LinearProbingHashST& st) {
        for (int i =0 ; i < st.M; i++) {
            if (st.keys[i])
                os << to_string(i) << ". Key: " << *st.keys[i] << ". Value: " << *st.vals[i] << "\n";
        }
        return os;
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

    Queue<Key> keyQueue() {
        Queue<Key> queue;
        for (int i = 0; i < M; i++) {
            if (keys[i])
                queue.enqueue(*keys[i]);
        }
        return queue;
    }
    
};

template <typename Key>
class HashSET {
private:
    int hash(Key key) {
        int hashCode = std::hash<Key>{}(key) & INT_MAX;
        if (logM < 26) hashCode = hashCode % PRIMES[logM + 5];
        return hashCode % M;
    }
    
    void resize(int cap) {
        Key** keyTemp = new Key*[cap];
        memset(keyTemp, 0x0, sizeof(Key*)*cap);
        int oldM = M;
        M = cap;
        logM = log(M) / log(2);
        if (keys)
            for (int i = 0; i < oldM; i++)
                if (keys[i]) {
                    int j;
                    for (j = hash(*keys[i]); keyTemp[j]; j = (j+1)%M);
                    keyTemp[j] = keys[i];
                }
        keys = keyTemp;
        keyTemp = 0x0; delete keyTemp;
    }
    
    Key** keys = 0x0;
    int M = 17;
    int N = 0;
    int logM;
    
    int getIndex(Key key) {
        int i = hash(key);
        while (keys[i]) {
            if (*keys[i] == key) return i;
            i = (i+1) % M;
        }
        return -1;
    }
    
public:
    friend ostream& operator<<(ostream& os, const HashSET& set) {
        for (int i = 0; i < set.M; i++)
           if (set.keys[i])  {
               os << to_string(i) << ". Key: " << *set.keys[i] << endl;
           }
        return os;
    }
    
    HashSET(): HashSET(17) {}
    
    HashSET(int expectedSize) { resize(expectedSize * 2); }
    
    int size() { return N; }
    
    bool isEmpty() { return size() == 0; }

    void add(Key key) {
        if (N >= M/2) resize(M*2);
        int i;
        for (i = hash(key); keys[i]; i = (i+1)%M) {
            if (keys[i])
                if (*keys[i] == key) return;
        }
        keys[i] = new Key(key);
        N++;
    }
    
    void deleteKey(Key key) {
        if (!contains(key)) return;
        int i = hash(key);
        while (*keys[i] != key) i = (i+1)%M;
        keys[i] = 0x0;
        i = (i+1) % M;
        while (keys[i]) {
            Key keyToRedo = *keys[i];
            keys[i] = 0x0;
            N--;
            add(keyToRedo);
            i = (i+1) % M;
        }
        N--;
        if (N > 0 && N == M/8) resize(M/2);
    }
    
    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }
    
    Queue<Key> keyQueue() {
        Queue<Key> queue;
        for (int i = 0; i < M; i++)
            if (keys[i])
                queue.enqueue(*keys[i]);
        return queue;
    }
    
    bool contains(Key key) {
        return getIndex(key) != -1;
    }
    
};

template <typename Key, typename Value>
class SeparateChainingHashST {
private:
    class Node {
    public:
        Key key;
        Value val;
        int index = -1;
        Node* next = NULL;
        friend ostream& operator<<(ostream& os, const Node& h) {
            os << "Key: " << h.key << ". Value: " << h.val << ". Next: ";
            if (h.next) os << h.next->key;
            else        os << "null";
            return os;
        }
        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };
    
    Node* newNode(Key key, Value val) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        return allNodes.back().get();
    }
    
    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    int hash(Key key) {
        int hashCode = std::hash<Key>{}(key) & INT_MAX;
        if (logM < 26) hashCode = hashCode % PRIMES[logM + 5];
        return hashCode % M;
    }
    
    Node* getValue(Key key) {
        Node* ptr = st[hash(key)];
        while (ptr) {
            if (ptr->key == key) return ptr;
            ptr = ptr->next;
        }
        return NULL;
    }
    
    vector<Node*> st;
    vector<unique_ptr<Node>> allNodes;
    int M = 17;
    int N = 0;
    int logM;
    
public:
    SeparateChainingHashST(): SeparateChainingHashST(17) {}
    
    SeparateChainingHashST(int cap): M(cap) { 
        logM = log(M)/log(2);
        st.resize(cap); 
    }
    
    SeparateChainingHashST(const initializer_list<pair<Key,Value>>& st): SeparateChainingHashST(st.size()*2) {
        for (const auto& s : st) put(s.first,s.second);
    }
    
    int size() { return N; }
    
    bool isEmpty() { return size() == 0; }
    
    void put(Key key, Value val) {
        Node* ptr = st[hash(key)];
        if (ptr) {
            if (ptr->key == key) { ptr->val = val; return; }
            while (ptr->next) {
                if (ptr->key == key) { ptr->val = val; return; }
                ptr = ptr->next;
            }
            ptr->next = newNode(key,val);
        } else {
            st[hash(key)] = newNode(key,val);
        }
        N++;
    }
    
    void deleteKey(Key key) {
        Node* ptr = st[hash(key)];
        Node* prevPtr = NULL;
        while (ptr) {
            if (ptr->key == key) {
                if (prevPtr) 
                    prevPtr->next = ptr->next;
                else {
                    if (ptr->next) st[hash(key)] = ptr->next;
                    else           st[hash(key)] = NULL;
                }
                N--; return;
            } 
            prevPtr = ptr;
            ptr = ptr->next;
        }
    }
    
    Value get(Key key) {
        Node* rv = getValue(key);
        return rv ? rv->val : NULL;
    }
    
    bool contains(Key key) {
        return getValue(key) != NULL;
    }
    
    void setValue(Key key, Value newVal) {
        Node* rv = getValue(key);
        if (rv) rv->val = newVal;
    }
    
    string toString() {
        string str = "\n";
        for (int i = 0; i < M; i++) {
            Node* ptr = st[i];
            if (ptr) {
                str += to_string(i) + " " +  ptr->toString() + "\n";
                ptr = ptr->next;
            }
        }
        return str;
    }
    
    Queue<Key> keyQueue() {
        Queue<Key> queue;
        for (int i = 0; i < M; i++) {
            Node* ptr = st[i];
            while (ptr) {
                queue.enqueue(ptr->key);
                ptr = ptr->next;
            }
        }
        return queue;
    }
    
};

#endif
