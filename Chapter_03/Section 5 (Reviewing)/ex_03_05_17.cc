#include <iostream>
#include <string>
#include <vector>
#include "SymbolTables.h"
#include <initializer_list>

using namespace std;

template <typename Key>
class MathSET {
private:
    HashSET<Key> set;
    
public:
    MathSET() = default;
    
    MathSET(const vector<Key>& universe) {
        for (int i = 0; i < universe.size(); i++)
            set.put(universe[i]);
    }
   
    MathSET(initializer_list<Key> universe) {
        for (const auto& key : universe)
            set.add(key);
    }
    
    MathSET(const Key* universe) {
        for (const auto& key : universe) set.add(key);
    }
    
    void add(Key key) { set.add(key); }
    
    MathSET<Key> complement(const vector<Key>& universe) {
        MathSET<Key> rv;
        for (auto key : universe)
            if (!set.contains(key)) 
                rv.add(key);
        return rv;
    }

    void unionWith(MathSET<Key> a) {
        for (auto key : a.keyQueue()) 
            if (!contains(key)) add(key);
    }
    
    void intersection(MathSET<Key> a) {
        for (auto key : this->keyQueue())
            if (!a.contains(key)) deleteKey(key);
    }
    
    void deleteKey(Key key) { set.deleteKey(key); }
    
    bool contains(Key key) { return set.contains(key); }
    
    bool isEmpty() { return set.isEmpty(); }
    
    int size() { return set.size(); }
    
    Queue<Key> keyQueue() {
        return set.keyQueue();
    }
    
    string toString() {
        return set.toString();
    }

};

int main() {
    MathSET<int> set = {7,6,6,5};
    MathSET<int> set2 = {1,2,3,4};
    MathSET<int> set3 = {2,3,4,5};
    cout << "SET 1:" << endl;
    cout << set.toString() << endl;
    cout << "SET 2:" << endl;
    cout << set2.toString() << endl;
    cout << "SET 3:" << endl;
    cout << set3.toString() << endl;
    
    cout << "After set.intersection(set3): " << endl;
    set.intersection(set3);
    cout << set.toString() << endl;
    
    cout << "After set.intersection(set2): " << endl;
    set.intersection(set2);
    cout << set.toString() << endl;
    
    cout << "After set.unionWith(set2)" << endl;
    set.unionWith(set2);
    cout << set.toString() << endl;
    
    cout << "After set.unionWith(set3)" << endl;
    set.unionWith(set3);
    cout << set.toString() << endl;
    
    vector<int> universe = {7,8,9};
    cout << "universe: ";
    for (const auto& s : universe)
        cout << s << " "; 
    cout << endl << endl;
    cout << "The complement set between universe and current SET 1: " << endl;
    cout << set.complement(universe).toString() << endl;
    
}
