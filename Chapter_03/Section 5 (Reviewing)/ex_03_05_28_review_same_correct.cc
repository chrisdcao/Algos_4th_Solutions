#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <string>
#include <memory>
#include "SymbolTables.h"
#include "Queue.h"

using namespace std;

template <typename Key, typename Value>
class RandomAccessST {
private:
    SeparateChainingHashST<Key, Value> st;
    RandomizedQueue<Key> randomQueue;
    
public:
    RandomAccessST() = default;
    
    RandomAccessST(const initializer_list<pair<Key,Value>>& list) {
        for (const auto& s : list) enqueue(s.first,s.second);
    }
    
    int size() { return randomQueue.size(); }
    
    void enqueue(Key key, Value val) {
        st.put(key,val);
        randomQueue.enqueue(key);
    }
    
    Key dequeue() {
        Key randomKey = randomQueue.dequeue();
        st.deleteKey(randomKey);
        return randomKey;
    }
    
    Queue<Key> keys() { return randomQueue; }
    
    Value get(Key key) { return st.get(key); }
    
    string toString() {
        ostringstream oss;
        for (const auto& s : randomQueue)
            oss << "Key: " << s << ". Value: " << get(s) << endl;
        return oss.str();
    }
};

int main() {
    RandomAccessST<char,int> ras;
    ras = {{'A',1},{'B',2},{'C',3},{'D',4}};
    cout << ras.toString() << endl;
    ras.dequeue();
    cout << ras.toString() << endl;
    ras.dequeue();
    cout << ras.toString() << endl;
    ras.dequeue();
    cout << ras.toString() << endl;
}

