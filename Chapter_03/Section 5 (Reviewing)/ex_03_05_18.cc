#include <iostream>
#include <initializer_list>
#include <sstream>
#include <cstring>
#include <string>
#include <vector>
#include <memory>
#include <cmath>
#include <climits>
#include "Queue.h"

using namespace std;

const int PRIMES[] = {
    1, 1, 3, 7, 13, 31, 61, 127, 251, 509, 1021, 2039, 4093, 8191, 16381,
    32749, 65521, 131071, 262139, 524287, 1048573, 2097143, 4194301,
    8388593, 16777213, 33554393, 67108859, 134217689, 268435399,
    536870909, 1073741789, 2147483647
};

template <typename Key>
class MultiHashSET {
private:
    int hash(Key key) {
        int hashCode = std::hash<Key>{}(key) & INT_MAX;
        if (logM < 26) hashCode = hashCode % PRIMES[logM + 5];
        return hashCode % M;
    }
    
    void resize(int cap) {
        Key** keyTemp = new Key*[cap];
        memset(keyTemp, 0x0, sizeof(Key*)*cap);
        int oldM = M;
        M = cap;
        logM = log(M) / log(2);
        if (keys)
            for (int i = 0; i < oldM; i++)
                if (keys[i]) {
                    int j;
                    for (j = hash(*keys[i]); keyTemp[j]; j=(j+1)%M);
                    keyTemp[j] = keys[i];
                }
        keys = keyTemp;
        keyTemp = 0x0; delete keyTemp;
    }
    
    int getIndex(Key key) {
        int i = hash(key);
        while (keys[i]) {
            if (*keys[i] == key) return i;
            i = (i+1) % M;
        }
        return -1;
    }
    
    Key** keys = 0x0;
    int M = 17;
    int N = 0;
    int logM;
    
public:
    MultiHashSET(): MultiHashSET(M) {}
    
    MultiHashSET(int cap) { resize(cap); }
    
    MultiHashSET(const initializer_list<int>& list) {
        resize(M); // since this is a direct constructor (unlike ex17), we have to manually resize for it to have default of cap 17
        for (const auto& s : list) add(s);
    }
    
    void add(Key key) {
        if (N >= M/2) resize(M*2);
        int i;
        for (i = hash(key); keys[i]; i = (i+1)%M); // just search for the next blank (no return on equal anymore)
        keys[i] = new Key(key);
        N++;
    }
    
    void deleteKey(Key key) {
        if (!contains(key)) return;
        int i = hash(key);
        while (keys[i]) {
            if (*keys[i] == key)
                keys[i] = 0x0;
            i = (i+1) % M;
        }
        i = (i+1) % M;
        while (keys[i]) {
            Key keyToRedo = *keys[i];
            keys[i] = 0x0;
            N--;
            add(key);
            i = (i+1)%M;
        }
        N--;
        if (N > 0 && N <= M/8) resize(M/2);
    }
    
    bool contains(Key key) { return getIndex(key) != -1; }
    
    friend ostream& operator<<(ostream& os, const MultiHashSET& set) {
        for (int i = 0; i < set.M; i++)
            if (set.keys[i]) 
                os << to_string(i) << ". Key: " << *set.keys[i] << endl;
        return os;
    }
    
    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }
    
    Queue<Key> keyQueue() {
        Queue<Key> queue;
        for (int i = 0; i < M; i++)
            if (keys[i]) 
                queue.enqueue(*keys[i]);
        return queue;
    }
    
};

template <typename Key>
class SeparateChainingMutliSET {
private:
    class Node {
    public:
        Key key;
        int index = -1;
        Node* next = NULL;
        friend ostream& operator<<(ostream& os, const Node& h) {
            os << "Key: " << h.key << ". Next: ";
            if (h.next) os << h.next->key;
            else        os << "null";
            return os;
        }
        
        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };
    
    Node* newNode(Key key) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() -1;
        allNodes.back()->key = key;
        return allNodes.back().get();
    }
    
    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }
    
    int hash(Key key) { 
        int hashCode = std::hash<Key>{}(key) & INT_MAX;
        if (logM < 26) hashCode = hashCode % PRIMES[logM+5];
        return hashCode % M;
    }
    
    Node* getNode(Key key) {
        Node* ptr = st[hash(key)];
        while (ptr) {
            if (ptr->key == key) return ptr;
            ptr = ptr->next;
        }
        return NULL;
    }
    
    vector<Node*> st;
    vector<unique_ptr<Node>> allNodes;
    int M = 17;
    int N = 0;
    int logM;
    
public:
    SeparateChainingMutliSET(): SeparateChainingMutliSET(M) {}
    
    SeparateChainingMutliSET(int cap): M(cap) { st.resize(cap); }
    
    SeparateChainingMutliSET(const initializer_list<int>& list) { 
        st.resize(M);
        for (auto& s : list) add(s); 
    }
    
    void add(Key key) {
        Node* ptr = st[hash(key)];
        if (!ptr) 
            st[hash(key)] = newNode(key);
        else {
            while (ptr->next) 
                ptr = ptr->next;
            ptr->next = newNode(key);
        }
        N++;
    }
    
    void deleteKey(Key key) {
        Node* ptr = st[hash(key)];
        Node* prevPtr = NULL;
        while (ptr) {
            if (ptr->key == key) {
                if (!prevPtr) {
                    // if this bucket has more than just this one element, and all after != key
                    // then we just remove the equal element (not the whole list)
                    if (ptr->next) st[hash(key)] = ptr->next; 
                    else           st[hash(key)] = NULL;
                } else
                    prevPtr->next = ptr->next;
                N--; return;
            }
            prevPtr = ptr;
            ptr = ptr->next;
        }
    }
    
    void deleteKeys(Key key) {
        while (contains(key)) 
            deleteKey(key);
    }
    
    bool contains(Key key) {
        return getNode(key) != NULL;
    }
    
    string toString() {
        string str ="\n";
        for (int i= 0; i < M; i++) {
            Node* ptr = st[i];
            while (ptr) {
                str += to_string(i) + ". " + ptr->toString() + "\n";
                ptr = ptr->next;
            }
        }
        return str;
    }
    
};

template <typename Key>
class BinarySearchMultiSET {
private:
    class Node {
    public:
        Key key;
        int index = -1;
        Node* left = NULL;
        Node* right = NULL;
        int N = 1;
        
        friend ostream& operator<<(ostream& os, const Node& h) {
            os << "Key: " << h.key << ". Right: ";
            if (h.right) os << h.right->key;
            else         os << "null";
            os << ". Left: ";
            if (h.left) os << h.left->key;
            else        os << "null";
            os << endl;
            return os;
        }
        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };
    
    Node* newNode(Key key, int N) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() -1;
        allNodes.back()->key = key;
        allNodes.back()->N = N;
        return allNodes.back().get();
    }
    
    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }
    
    int size(Node* h) { return h ? h->N : 0; }
    
    Node* put(Node* h, Key key) {
        if (!h) return newNode(key,1);
        if (h->key > key) h->left = put(h->left,key);
        else if (h->key <= key) h->right = put(h->right,key);
        h->N = size(h->left) +  size(h->right) + 1;
        return h;
    }
    
    Node* deleteKey(Node* h, Key key) {
        if (!h) return NULL;
        if (h->key < key) h->right = deleteKey(h->right, key);
        else if (h->key > key) h->left = deleteKey(h->left, key);
        else {
            if (!h->left) { return h->right; }
            if (!h->right) { return h->left; }
            Node* temp = h;
            h = min(temp->right);
            h->right = deleteMin(temp->right);
            h->left = temp->left;
            freeNode(temp);
        }
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }
    
    Node* min(Node* h) { return h ? h->left ? min(h->left) : h : h; }
    
    Node* deleteMin(Node* h) {
        if (!h) return NULL;
        if (!h->left) return h->right;
        h->left = deleteMin(h->left);
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }
    
    string toString(Node* h) {
        if (!h) return "";
        string rv = "";
        rv += toString(h->left);
        rv += h->toString();
        rv += toString(h->right);
        return rv;
    }
    
    int rank(Node* h, Key key) {
        if (!h) return 0;
        if (h->key > key) return rank(h->left,key);
        else if (h->key < key) return size(h->left) + rank(h->right,key) + 1;
        else return size(h->left);
    }
    
    Node* select(Node* h, int k) {
        if (!h) return NULL;
        int t = size(h->left);
        if (t > k) return select(h->left, k);
        else if (t < k) return select(h->right,k-t-1);
        else return h;
    }
    
    Node* getNode(Node* h, Key key) {
        if (!h) return NULL;
        if (h->key > key) return getNode(h->left,key);
        else if (h->key < key) return getNode(h->right,key);
        else return h;
    }
    
    Node* root = NULL;
    vector<unique_ptr<Node>> allNodes;
    
public:
    BinarySearchMultiSET() = default;
    
    BinarySearchMultiSET(const initializer_list<Key>& list) {
        for (const auto& key : list) put(key);
    }
    
    int rank(Key key) { return rank(root,key); }
    
    bool isEmpty() { return size() == 0; }
    
    int size() { return size(root); }
    
    void put(Key key) { root = put(root,key); }
    
    string toString() { return toString(root); }
    
    void deleteKey(Key key) {
        if (isEmpty()) return;
        while (contains(key))
            root = deleteKey(root,key);
    }
    
    Key select(int k) { 
        Node* rv = select(root,k);
        return rv ? rv->key : NULL;
    }
    
    bool contains(Key key) {
        return getNode(root,key) != NULL;
    }
};

template <typename Key>
class MultiSET {
    static const bool RED = true;
    static const bool BLACK = false;
private:
    class Node {
    private:
        Key key;
        int index = -1;
        bool color = RED;
        int N = 1;
        Node* left = NULL;
        Node* right = NULL;
    };
    
    Node* newNode(Key key, bool color, int N) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() -1;
        allNodes.back()->color = color;
        allNodes.back()->key = key;
        allNodes.back()->N = N;
        return allNodes.back().get();
    }
    
    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }
    
    int size(Node* h) { return h ? h->N : 0; }

public:
};

int main() {
    cout << "MutliHashSET: " << endl;
    MultiHashSET<int> set = {-1,1,1,2,3,4};
    cout << set.toString() << endl;
    
    SeparateChainingMutliSET<int> set2 = {1,1,1,1,2,3,4};
    cout << set2.toString() << endl;
    set2.deleteKeys(1);
    cout << set2.toString()  << endl;
    
    BinarySearchMultiSET<int> set3 = {4,5,6,1,1,1,2,3};
    cout << set3.toString() << endl;
    set3.deleteKey(1);
    cout << set3.toString() << endl;
    
}
