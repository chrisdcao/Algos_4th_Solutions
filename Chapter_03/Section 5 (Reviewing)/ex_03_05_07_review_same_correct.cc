#include <iostream>
#include <cstring>
#include <vector>
#include "Queue.h"
#include <sstream>

using namespace std;

template <typename Key>
class RedBlackBST {
    static const bool RED = true;
    static const bool BLACK = false;
private:
    class Node {
    public:
        Key key;
        Node* left = NULL;
        Node* right = NULL;
        int index = -1;
        bool color = RED;
        int N = 1;
        friend ostream& operator<<(ostream& os, const Node& h) {
            os << h.key << " ";
            return os;
        }

        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };

    Node* newNode(Key key, int N, bool color) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->N = N;
        allNodes.back()->color = color;
        return allNodes.back().get();
    }

    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    int size(Node* h) { return h ? h->N : 0; }

    bool isRed(Node* h) { return h ? h->color : BLACK; }

    Node* put(Node* h, Key key) {
        if (!h) return newNode(key, 1, RED);
        if (h->key > key) h->left = put(h->left, key);
        else if (h->key < key) h->right = put(h->right,key);
        return balance(h);
    }

    Node* balance(Node* h) {
        if (isRed(h->right) && !isRed(h->left)) h = rotateLeft(h);
        if (isRed(h->left) && isRed(h->left->left)) h = rotateRight(h);
        if (isRed(h->left) && isRed(h->right)) flipColor(h);
        return h;
    }

    void flipColor(Node* h) {
        h->color = !h->color;
        if (h->left) h->left->color = !h->left->color;
        if (h->right) h->right->color = !h->right->color;
    }

    Node* moveRedLeft(Node* h) {
        flipColor(h);
        if (isRed(h->right->left)) {
            h->right = rotateRight(h->right);
            h = rotateLeft(h);
            flipColor(h);
        }
        return h;
    }

    Node* moveRedRight(Node* h) {
        flipColor(h);
        if (isRed(h->left->left)) {
            h = rotateRight(h);
            flipColor(h);
        }
        return h;
    }

    Node* rotateLeft(Node* h) {
        Node* x = h->right;
        h->right = x->left;
        x->left = h;
        x->color = h->color;
        x->N = h->N;
        h->color = RED;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* rotateRight(Node* h) {
        Node* x = h->left;
        h->left = x->right;
        x->right = h;
        x->color = h->color;
        x->N = h->N;
        h->color = RED;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* deleteMax(Node* h) {
        if (!h) return NULL;
        if (!h->right) {
            Node* leftChild = h->left;
            freeNode(h);
            return leftChild;
        }
        if (!isRed(h->right->left) && !isRed(h->right)) h = moveRedRight(h);
        h->right = deleteMax(h->right);
        return balance(h);
    }

    Node* deleteMin(Node* h) {
        if (!h) return NULL;
        if (!h->left) {
            Node* rightChild = h->right;
            freeNode(h);
            return rightChild;
        }
        if (!isRed(h->left->left) && !isRed(h->left)) h = moveRedLeft(h);
        h->left = deleteMin(h->left);
        return balance(h);
    }

    Node* deleteKey(Node* h, Key key) {
        if (!h) return NULL;
        if (h->key > key) {
            if (!h->left) return NULL;
            if (!isRed(h->left)) h = moveRedLeft(h);
            h->left = deleteKey(h->left,key);
        } else {
            if (isRed(h->left)) h = rotateRight(h);
            if (h->key == key && !h->right) {
                Node* leftChild = h->left;
                freeNode(h);
                return leftChild;
            }
            if (!isRed(h->right) && !isRed(h->right->left)) h = moveRedRight(h);
            if (h->key == key) {
                Node* temp = h;
                h = min(temp->right);
                h->right = deleteMin(temp->right);
                h->left = temp->left;
                freeNode(temp);
            } else h->right = deleteKey(h->right, key);
        }
        return balance(h);
    }

    Node* min(Node* h) {
        return h ? h->left ? min(h->left) : h : NULL;
    }

    Node* max(Node* h) {
        return h ? h->right ? max(h->right) : h : NULL;
    }

    Node* select(Node* h, int k) {
        if (!h) return NULL;
        int t = size(h->left);
        if (t > k) return select(h->left, k);
        else if (t < k) return select(h->right, k-t-1);
        else return h;
    }

    int rank(Node* h, Key key) {
        if (!h) return 0;
        if (h->key < key) return 1 + size(h->left) + rank(h->right, key);
        else if (h->key > key) return rank(h->left,key);
        else return size(h->left);
    }

    Node* ceiling(Node* h, Key key) {
        if (!h) return NULL;
        if (h->key < key) return ceiling(h->right, key);
        else if (h->key == key) return h;
        Node* temp = ceiling(h->left, key);
        return temp ? temp : h;
    }

    Node* floor(Node* h, Key key) {
        if (!h) return NULL;
        if (h->key > key) return floor(h->left, key);
        else if (h->key == key) return h;
        Node* temp = floor(h->right, key);
        return temp ? temp : h;
    }

    string toString(Node* h) {
        if (!h) return "";
        string rv = toString(h->left);
        rv += h->toString();
        rv += toString(h->right);
        return rv;
    }

    Node* root = 0x0;
    vector<unique_ptr<Node>> allNodes;

public:
    RedBlackBST() = default;

    int size() { return size(root);}

    bool isEmpty() { return size() == 0; }

    void put(Key key) {
        root = put(root,key);
        root->color = BLACK;
    }

    Key ceiling(Key key) { return ceiling(root, key)->key; }

    Key floor(Key key) { return floor(root, key)->key; }

    int rank(Key key) { return rank(root, key); }

    Key select(int k) { return select(root, k)->key; }

    Key min() { return min(root)->key; }

    Key max() { return max(root)->key; }

    void deleteMin() { 
        if (!root) return;
        if (!isRed(root->left) && !isRed(root->right)) root->color = RED;
        root = deleteMin(root);
        if (root) root->color = BLACK;
    }

    void deleteMax() {
        if (!root) return;
        if (!isRed(root->left) && !isRed(root->right)) root->color = RED;
        root = deleteMax(root);
        if (root) root->color = BLACK;
    }

    void deleteKey(Key key) {
        if (!root) return;
        if (!isRed(root->left) && !isRed(root->right)) root->color = RED;
        root = deleteKey(root, key);
        if (root) root->color = BLACK;
    }

    string toString() { return toString(root); }
};

class SETint {
private:
    RedBlackBST<int> bst;
public: 
    SETint() = default;

    void add(int key) { bst.put(key); }

    void deleteKey(int key) { bst.deleteKey(key); }

    bool contains(int key) { return bst.select(rank(key)) == key; }

    int size() { return bst.size(); }

    bool isEmpty() { return size() != 0; }

    string toString() { return bst.toString(); }

    int min() { return bst.min(); }

    int max() { return bst.max(); }

    int floor(int key) { return bst.floor(key); }

    int ceiling(int key) { return bst.ceiling(key); }

    void deleteMin(int key) { return bst.deleteMin(); }

    void deleteMax(int key) { return bst.deleteMax(); }

    int select(int k) { return bst.select(k); }

    int rank(int key) { return bst.rank(key); }
};

class SETdouble {
private:
    RedBlackBST<double> bst;
public: 
    SETdouble() = default;

    void add(double key) { bst.put(key); }

    void deleteKey(double key) { bst.deleteKey(key); }

    bool contains(double key) { return bst.select(rank(key)) == key; }

    int size() { return bst.size(); }

    bool isEmpty() { return size() != 0; }

    string toString() { return bst.toString(); }

    double min() { return bst.min(); }

    double max() { return bst.max(); }

    double floor(double key) { return bst.floor(key); }

    double ceiling(double key) { return bst.ceiling(key); }

    void deleteMin(double key) { return bst.deleteMin(); }

    void deleteMax(double key) { return bst.deleteMax(); }

    double select(int k) { return bst.select(k); }

    int rank(double key) { return bst.rank(key); }
};

int main() {
    SETint set;
    SETdouble dset;
    set.add(1);
    set.add(2);
    set.add(13);
    set.add(14);
    dset.add(1.21);
    dset.add(2.23);
    dset.add(13.33);
    dset.add(14.56);
    cout << set.toString() << endl;
    cout << dset.toString() << endl;
}
