#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstring>
#include "StringOperations.h"
#include "SymbolTables.h"
#include "Bag.h"

using namespace std;

LinearProbingHashST<string, Bag<string>*> invert(LinearProbingHashST<string, Bag<string>*> st) {
    LinearProbingHashST<string, Bag<string>*> ts;
    for (auto key : st.keyQueue()) {
        for (auto val : *st.get(key)) {
            if (!ts.contains(val))
                ts.put(val, new Bag<string>());
            ts.get(val)->add(key);
        }
    }
    return ts;
}

int main() {
    LinearProbingHashST<string, Bag<string>*> st;
    string fileName;
    cin >> fileName;
    ifstream input(fileName);
    string separator;
    cin >> separator;
    
    while (!input.eof()) {
        string str;
        getline(input, str);
        vector<string> array = split(str,separator);
        string key = array[0];
        if (!st.contains(key)) st.put(key, new Bag<string>());
        for (int j = 1; j < array.size(); j++) {
            string val = array[j];
            st.get(key)->add(val);
        }
    }
    
    cout << "The original ST is: " << endl;
    for (auto key : st.keyQueue()) { 
        cout << "Key: " << key << ". Value: ";
        for (auto val : *st.get(key))
            cout << val << " ";
        cout << endl;
    }
    cout << endl;
    
    cout << "The reversed TS is: " << endl;
    LinearProbingHashST<string, Bag<string>*> ts = invert(st);
    for (auto key : ts.keyQueue()) { 
        cout << "Key: " << key << ". Value: ";
        for (auto val : *ts.get(key))
            cout << val << " ";
        cout << endl;
    }
    cout << endl;
    
    return 0;
}
