#ifndef RANDOM_H
#define RANDOM_H

#include <iostream>
#include <algorithm>
#include <random>

using namespace std;

template <typename T>
T randomUniform(T range_from, T range_to) {
    std::random_device                  rand_dev;
    std::mt19937                        generator(rand_dev());
    std::uniform_real_distribution<T>   distr(range_from, range_to);
    return distr(generator);
}

static int randomUniformDistribution(int range_from, int range_to) {
    random_device rand_dev;
    mt19937 generator(rand_dev());
    uniform_int_distribution<int> distr(range_from, range_to);
    return distr(generator);
}

static int RandomUniformDistribution(int range_from, int range_to) {
    random_device rand_dev;
    mt19937 generator(rand_dev());
    uniform_int_distribution<int> distr(range_from, range_to);
    return distr(generator);
}

static int random_uniform_distribution(int range_from, int range_to) {
    random_device rand_dev;
    mt19937 generator(rand_dev());
    uniform_int_distribution<int> distr(range_from, range_to);
    return distr(generator);
}

template <typename T>
static void uniformShuffle(vector<T>& vec) {
    shuffle(vec.begin(), vec.end(), std::mt19937{std::random_device{}()});
}

template <typename T>
static void uniform_shuffle(vector<T>& vec) {
    shuffle(vec.begin(), vec.end(), std::mt19937{std::random_device{}()});
}

static char randomUniformChar() {
    string reference_string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int i = randomUniformDistribution(0,reference_string.size()-1);
    return reference_string[i];
}

#endif

