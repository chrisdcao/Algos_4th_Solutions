#include <iostream>
#include <sstream>
#include <vector>
#include <memory>

using namespace std;

template <typename Key>
class MultiSET {

    static const bool RED = true;
    static const bool BLACK = false;

private:
    class Node {
    public:
        Key key;
        Node* left = NULL;
        Node* right = NULL;
        int N = 1;
        bool color = RED;
        int index = -1;
        friend ostream& operator << (ostream& os, const Node& h) {
            os << "Key: " << h.key << ". Size: " << h.N << ". Color: " << (h.color ? "RED" : "BLACK");
            os << ". Left: ";
            if (h.left) os << h.left->key;
            else        os << "null";
            os << ". Right: ";
            if (h.right) os << h.right->key;
            else         os << "null";
            os <<"\n";
            return os;
        }

        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };

    Node* newNode(Key key, int N, bool color) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->key = key;
        allNodes.back()->N = N;
        allNodes.back()->color = color;
        return allNodes.back().get();
    }

    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    int size(Node* h) { return h ? h->N : 0; }

    bool isRed(Node* h) { return h ? h->color == RED : BLACK; }

    Node* put(Node* h, Key key) {
        if (!h) return newNode(key,1,RED);
        if (h->key <= key) h->right = put(h->right, key);
        else if (h->key > key) h->left = put(h->left, key);
        return balance(h);
    }

    Node* deleteKey(Node* h, Key key) {
        if (!h) return NULL;
        if (h->key > key) {
            if (!h->left) return NULL;
            if (!isRed(h->left) && !isRed(h->left->left)) h = moveRedLeft(h);
            h->left =deleteKey(h->left,key);
        } else {
            if (isRed(h->left)) h = rotateRight(h);
            // we have to add extra checking: 'h->key == key' because this else does NOT ONLY cover the h->key == key case
            if (h->key == key && !h->right) { // this means that there'll be no dups as we only put dups into right branch
                Node* leftChild = h->left;
                freeNode(h);
                return leftChild;
            }
            if (!isRed(h->right) && !isRed(h->right->left)) h = moveRedRight(h);
            if (h->key == key) {
                Node* temp = h;
                h = min(temp->right);
                h->right = deleteMin(temp->right);
                h->left = temp->left;
                freeNode(temp);
            } else {
                h->right = deleteKey(h->right,key);
            }
        }
        return balance(h);
    }

    Node* select(Node* h, int k) {
        if (!h) return NULL;
        int t = size(h->left);
        if (t > k) return select(h->left,k);
        else if (t < k) return select(h->right,k-t-1);
        else return h;
    }

    int rank(Node* h, Key key) {
        if (!h) return 0;
        if (h->key > key) return rank(h->left,key);
        else if (h->key < key) return size(h->left) + rank(h->right,key) + 1;
        else return size(h->left);
    }

    Node* deleteMin(Node* h) {
        if (!h) return NULL;
        if (!h->left) return h->right;
        if (!isRed(h->left) && !isRed(h->left->left)) h = moveRedLeft(h);
        h->left = deleteMin(h->left);
        return balance(h);
    }

    Node* min(Node* h) {
        return h ? h->left ? min(h->left) : h : NULL;
    }

    Node* rotateLeft(Node* h) {
        Node* x = h->right;
        h->right = x->left;
        x->left = h;
        x->N = h->N;
        x->color = h->color;
        h->N = size(h->left) + size(h->right) + 1;
        h->color = RED;
        return x;
    }

    Node* rotateRight(Node* h) {
        Node* x = h->left;
        h->left = x->right;
        x->right = h;
        x->N = h->N;
        x->color = h->color;
        h->N = size(h->left) + size(h->right) + 1;
        h->color = RED;
        return x;
    }

    Node* moveRedLeft(Node* h) {
        flipColor(h);
        if (isRed(h->right->left)) {
            h->right = rotateRight(h->right);
            h = rotateLeft(h);
            flipColor(h);
        }
        return h;
    }

    Node* moveRedRight(Node* h) {
        flipColor(h);
        if (isRed(h->left->left)) {
            h = rotateRight(h);
            flipColor(h);
        }
        return h;
    }

    void flipColor(Node* h) {
        h->color = !h->color;
        h->left->color = !h->left->color;
        h->right->color = !h->right->color;
    }

    Node* balance(Node* h) {
        if (isRed(h->right) && !isRed(h->left)) h = rotateLeft(h);
        if (isRed(h->left) && isRed(h->left->left)) h = rotateRight(h);
        if (isRed(h->left) && isRed(h->right)) flipColor(h);
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }

    Node* root = NULL;
    vector<unique_ptr<Node>> allNodes;

    string toString(Node* h) {
        if (!h) return "";
        string rv = toString(h->left);
        rv += h->toString();
        rv += toString(h->right);
        return rv;
    }

public:

    Key select(int k) {
        Node* rv = select(root,k);
        return rv ? rv->key : NULL;
    }

    int rank(Key key) { return rank(root, key); }

    bool contains(Key key) { return select(rank(key)) == key; }

    void put(Key key) {
        root = put(root,key);
        root->color = BLACK;
    }
    
    void deleteKey(Key key) {
        if (!root) return;
        while (contains(key)) {
            if (!isRed(root->left) && !isRed(root->right)) root->color = RED;
            root = deleteKey(root, key);
            if (root) root->color = BLACK;
        }
    }

    string toString() { return toString(root); }

};

int main() {
    MultiSET<char> set;
    int length;
    cin >> length;
    char c;
    for (int i = 0; i < length; i++) {
        cin >> c;
        set.put(c);
    }
    cout << set.toString() << endl;

    cout << "deleteKey('Y'): " << endl;
    set.deleteKey('Y');
    cout << set.toString() << endl;
}
