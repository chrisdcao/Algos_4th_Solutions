#include <iostream>
#include <vector>
#include <cmath>
#include "SymbolTables.h"
#include "Queue.h"

using namespace std;

class SparseVector {
private:
    // for the sparse vector, 'key' is the index of the value
    LinearProbingHashST<int, double> st;
    
public:
    int size() { return st.size(); }
    
    bool isEmpty() { return size() == 0; }
    
    void put(int i, double x) {
        st.put(i,x);
    }
    
    double get(int i) {
        if (!st.contains(i)) return 0.0;
        return st.get(i);
    }
    
    double dot(double* that) {
        double sum = 0.0;
        for (int i : st.keyQueue()) 
            sum += that[i] * this->get(i);
        return sum;
    }
    
    Queue<int> keyQueue() {
        return st.keyQueue();
    }

    SparseVector& operator+(SparseVector& rhs) {
        for (auto key : rhs.keyQueue()) {
            if (!st.contains(key)) {
                st.put(key, rhs.get(key));
            } else {
                double sum = rhs.get(key) + st.get(key);
                if (sum == 0.0)
                    st.deleteKey(key);
                else
                    st.put(key, rhs.get(key) + st.get(key));
            }
        }
        return *this;
    }
        
    string toString() {
        return st.toString();
    }
    
};

int main() {
    int length, length2;
    cin >> length >> length2;
    int x;
    double d;
    
    SparseVector svec;
    for (int i = 0; i < length; i++) {
        cin >> x >> d;
        svec.put(x,d);
    }
    
    SparseVector rhs;
    for (int i = 0; i < length2; i++) {
        cin >> x >> d;
        rhs.put(x,d);
    }
    
    cout << svec.toString() << endl;
    
    cout << rhs.toString() << endl;
    
    cout << (rhs + svec).toString() << endl;
    
    return 0;
}
