#include <iostream>
#include <string>
#include <cstring>
#include <vector>

using namesapce std;

class DeDup {
int main() {
    HashSET<string> set;
    set = new HashSet<String>();

    while (cin) {
        cin >> key;
        // if the set not already have the key, put the key in
        // do we really have to manually check for the existence of the key in the data structure?
        if (!set.contains(key)) {
            set.add(key);
            cout << key << endl;
        }
    }
}

class WhiteFilter {
    int main(int argc, char** argv) {
        HashSET<string> set;
        set = new HashSET<string>;
        for (int i = 0; i < argc; i++)
            set.add(argv[i]); // we add all the args from the command line
        while (cin) {
            string word;
            cin >> word;
            // then check if the set of command line args contains the input words
            if (set.contains(word))
                cout << word;
        }
    }
}

#include <fstream>
using namespace std;

public class LookupCSV {
    void main(int argc, char** argv) {
        string inputFileName = argv[1];
        ifstream input(inputFileName);
        int keyField = stoi(argv[2]);
        int valField = stoi(argv[3]);
        string line;

        vector<string> tokens;
        ST<string, string> st = new ST<string, string>;

        while (!input.eof()) { // this is pretty weird, must research about this from java to C++
            input >> line;
            tokens.push_back(line.split(","));
            string key = tokens(keyField);
            string val = tokens(valField);
            st.put(key, val);
        }

        while (cin) {
            string query;
            cin >> query;
            if (st.contains(query))
                cout << st.get(query) << endl;
        }
    }
}


class LookupIndex {

}

int main(int argc, char** argv) {
    string inputFileName = argv[1];
    ifstream input(inputFileName);
    string sp = argv[2]; // this is the split character

    ST<string,queue<string>> st = new ST<string,Queue<string>>;
    ST<string,queue<string>> ts = new ST<string,Queue<string>>; // ts is the inverted index version of st

    while (!input.eof()) {
        string line;
        string a[] = getline(input,line).split(); // this split() is like Python split, which is supposed to return a list/array of strings that have been splitted from the line. I'm writing pseudo-code here, might not be C++ correct
        string key = a[0]; // the first word in the file is going to be the key
        for (int i = 0; i < a.length; i++) {
            string val = a[i];
            if (!st.contains(key)) st.put(key, new Queue<string>()); // open up a slot for the key with the co-op value as a queue
            if (!ts.contains(val)) ts.put(val, new Queue<string>());
            st.get(key).enqueue(val);
            ts.get(val).enqueue(key);
        }
    }

    while (cin) {
        string query;
        cin >> query;
        if (st.contains(quey))
            for (const auto s : st.get(Query))
                cout << " " << s;

        if (ts.contains(query))
            for (const auto s : ts.get(Query))
                cout << " " << s;
    }
}

class FileIndex {
    int main(int argc, char** argv) {
        ST<string,SET<File>> st= new ST<string,SET<File>>();

        for (string filename : args) {
            File file = new File(filename);
            ifstream input(file);
            while (input) {
                string word;
                input >> word;
                if (!st.contains(word)) 
                    st.put(word, new SET<File>());
                st.get(word).add(file);
            }
        }
        while (cin) {
            string query;
            cin >> query;
            if (st.contains(query))
                for (File file : st.get(query))
                    cout << " " << file.getName();
        }
    }
}


double a[][] = new double[N][N];
double x[] = new double[N];
double b[] = new double[N];

for (int i = 0; i < N; i++) {
    sum = 0.0;
    for (int j = 0; j < N; j++) 
        sum += a[i][j] * x[j];
    b[i] = sum;
}

class SparseVector {
private:
    HashST<int,double> st; // it's basically a hash map

public:
    SparseVector() { st = new Hash<int, double>() };

    int size() { return st.size();}

    void put(int i, double x) { st.put(i,x);}

    double get(int i) {  // get key 'i'
        if (!st.contains(i)) return 0;
        else return st.get(i);
    }

    double dot(double that[]) { // dot product with another vector
        double sum = 0.0;
        for (int i : st.keys()) 
            sum += that[i] * this.get(i);
        return sum;
    }
};

