#include <iostream>
#include <sstream>
#include <cstring>
#include <iomanip>
#include <vector>
#include "Queue.h"

using namespace std;

template <typename Value>
class STint {
    static const bool RED = true;
    static const bool BLACK = false;
private:
    class Node {
    public:
        int key;
        Value val;
        Node* left = 0x0;
        Node* right = 0x0;
        bool color = RED;
        int N = 1;
        int index = -1;

        friend ostream& operator <<( ostream& out, const Node& h ) {
            out << " | " << h.key << " : " << h.val << " ; size : " << h.N << " ; color : " << h.color;
            if ( h.left ) {
                out << " ; left : " << h.left->key;
            } else {
                out << " ; left : null";
            }

            if ( h.right ) {
                out << " ; right : " << h.right->key;
            } else {
                out << " ; right : null";
            }
            out << " | \n";
            return out;
        }

        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };

    Node* newNode(int key, Value val, int N, bool color) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        allNodes.back()->color = color;
        return allNodes.back().get();
    }

    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    int size(Node* h) { return h?h->N:0; }

    bool isRed(Node* h) { return h? h->color == RED : BLACK; }

    Node* put(Node* h, int key, Value val) {
        if (!h) return newNode(key,val,1,RED);

        if (h->key > key) h->left = put(h->left,key,val);
        else if (h->key < key) h->right = put(h->right,key,val);
        else h->val = val;

        return balance(h);
    }

    Node* min(Node* h) { return h->left ? min(h->left) : h->left; }

    Node* max(Node* h) { return h->right ? max(h->right) : h->right; }

    Node* deleteMin(Node* h) {
        if (!h) return NULL;
        if (!h->left) {
            Node* rightChild = h->right;
            freeNode(h);
            return rightChild;
        }
        // meaning that the node we are stepping into is node a 3-node (3 node could either be at child or grandchild)
        if (!isRed(h->left) && !isRed(h->left->left)) h = moveRedLeft(h);
        h->left = deleteMin(h->left);
        return balance(h);
    }

    Node* deleteMax(Node* h) {
        if (!h) return NULL;
        if (!h->right) {
            Node* leftChild = h->left;
            freeNode(h); return leftChild;
        }
        if (!isRed(h->right) && !isRed(h->right->left)) h = moveRedRight(h);
        h->right = deleteMax(h->right);
        return balance(h);
    }

    Node* deleteKey(Node* h, int key) {
        if (!h) return NULL; 
        if (h->key > key) {
            if (!h->left) return NULL;
            if (!isRed(h->left)) h = moveRedLeft(h);
            h->left = deleteKey(h->left,key);
        } else {
            if (isRed(h->left)) h = rotateRight(h);
            if (h->key == key && !h->right) {
                Node* leftChild = h->left;
                freeNode(h);
                return leftChild;
            }
            if (!isRed(h->right) && !isRed(h->right->left)) h = moveRedRight(h);
            if (h->key == key) {
                Node* temp = h;
                h = min(temp->right);
                h->right = deleteMin(temp->right);
                h->left = temp->left;
                freeNode(temp);
            } else {
                h->right = deleteKey(h->right,key);
            }
        }
        return balance(h);
    }

    Node* get(Node* h, int key) {
        if (!h) return NULL;
        if (h->key > key) return get(h->left, key);
        else if (h->key < key) return get(h->right, key);
        else return h;
    }

    Node* select(Node* h, int k) {
        if (!h) return NULL;
        int t = sizeof(h->left);
        if (t < k) return select(h->right,k-t-1);
        else if (t > k) return select(h->left,k);
        else return h;
    }

// largest smaller
    Node* floor(Node* h, int key) {
        if (!h) return NULL;
        if (h->key > key) return floor(h->left,key);
        else if (h->key == key) return h;
        Node* temp = floor(h->right, key);
        return temp ? temp : h;
    }

// smallest larger
    Node* ceiling(Node* h, int key) {
        if (!h) return NULL;
        if (h->key < key) return ceiling(h->right, key);
        else if (h->key == key) return h;
        Node* temp = ceiling(h->left,key);
        return temp ? temp : h;
    }

    Node* rotateLeft(Node* h) {
        Node* x = h->right;
        h->right = x->left;
        x->left = h;
        x->color = h->color;
        x->N = h->N;
        h->color = RED;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* rotateRight(Node* h) {
        Node* x = h->left;
        h->left = x->right;
        x->right = h;
        x->color = h->color;
        x->N = h->N;
        h->color = RED;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* moveRedLeft(Node* h) {
        flipColor(h);
        if (isRed(h->right->left)) {
            h->right = rotateRight(h->right);
            h = rotateLeft(h);
            flipColor(h);
        }
        return h;
    }

    Node* moveRedRight(Node* h) {
        flipColor(h);
        if (isRed(h->left->left)) {
            h = rotateRight(h);
            flipColor(h);
        }
        return h;
    }

    void flipColor(Node* h) {
        h->color = !h->color;
        if (h->left) h->left->color = !h->left->color;
        if (h->right) h->right->color = !h->right->color;
    }

    Node* balance(Node* h) {
        if (!isRed(h->left) && isRed(h->right)) h = rotateLeft(h);
        if (isRed(h->left) && isRed(h->left->left)) h = rotateRight(h);
        if (isRed(h->right) && isRed(h->left)) flipColor(h);
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }

    int rank(Node* h, int key) {
        if (!h) return 0;
        if (h->key < key) return rank(h->right,key) + size(h->left) + 1;
        else if (h->key > key) return rank(h->left,key);
        else return size(h->left);
    }

    void keys(Node* h, Queue<int>& queue, int lo, int hi) {
        if (!h) return;
        if (h->key < lo) return;
        if (h->key > hi) return;
        keys(h->left,queue,lo,hi);
        queue.enqueue(h->key);
        keys(h->right,queue,lo,hi);
    }

    string toString(Node* h) {
        if (!h) return "";
        string rv = toString(h->left);
        rv += h->toString();;
        rv += toString(h->right);
        return rv;
    }

    Node* root = NULL;
    vector<unique_ptr<Node>> allNodes;
    
public:
    STint() = default;

    int size() { return size(root); }

    bool isEmpty() { return size() == 0; }

    void put(int key, Value val) {
        root = put(root,key,val);
        root->color = BLACK;
    }

    void deleteKey(int key) {
        if (!root) return;
        if (!isRed(root->left) && !isRed(root->right)) root->color = RED;
        root = deleteKey(root,key);
        if (root) root->color = BLACK;
    }

    Queue<int> keys(int lo, int hi) {
        Queue<int> queue;
        keys(root,queue,lo,hi);
        return queue;
    }

    int max() { return max(root)->key; }

    int min() { return min(root)->key; }

    Queue<int> keys() { return keys(min(),max()); }

    string toString() { return toString(root); }

};

template <typename Value>
class STdouble {
    static const bool RED = true;
    static const bool BLACK = false;
private:
    class Node {
    public:
        double key;
        Value val;
        Node* left = 0x0;
        Node* right = 0x0;
        bool color = RED;
        int N = 1;
        int index = -1;

        friend ostream& operator <<( ostream& out, const Node& h ) {
            out << " | " << setprecision(3) << h.key << " : " << h.val << " ; size : " << h.N << " ; color : " << h.color;
            if ( h.left ) {
                out << " ; left : " << h.left->key;
            } else {
                out << " ; left : null";
            }

            if ( h.right ) {
                out << " ; right : " << h.right->key;
            } else {
                out << " ; right : null";
            }
            out << " | \n";
            return out;
        }

        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };

    Node* newNode(double key, Value val, int N, bool color) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        allNodes.back()->color = color;
        return allNodes.back().get();
    }

    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    int size(Node* h) { return h?h->N:0; }

    bool isRed(Node* h) { return h? h->color == RED : BLACK; }

    Node* put(Node* h, double key, Value val) {
        if (!h) return newNode(key,val,1,RED);

        if (h->key > key) h->left = put(h->left,key,val);
        else if (h->key < key) h->right = put(h->right,key,val);
        else h->val = val;

        return balance(h);
    }

    Node* min(Node* h) { return h->left ? min(h->left) : h->left; }

    Node* max(Node* h) { return h->right ? max(h->right) : h->right; }

    Node* deleteMin(Node* h) {
        if (!h) return NULL;
        if (!h->left) {
            Node* rightChild = h->right;
            freeNode(h);
            return rightChild;
        }
        // meaning that the node we are stepping into is node a 3-node (3 node could either be at child or grandchild)
        if (!isRed(h->left) && !isRed(h->left->left)) h = moveRedLeft(h);
        h->left = deleteMin(h->left);
        h->N = size(h->left) + size(h->right) + 1;
        return balance(h);
    }

    Node* deleteMax(Node* h) {
        if (!h) return NULL;
        if (!h->right) {
            Node* leftChild = h->left;
            freeNode(h); return leftChild;
        }
        if (!isRed(h->right) && !isRed(h->right->left)) h = moveRedRight(h);
        h->right = deleteMax(h->right);
        return balance(h);
    }

    Node* deleteKey(Node* h, double key) {
        if (!h) return NULL; 
        if (h->key > key) {
            if (!h->left) return NULL;
            if (!isRed(h->left) && !isRed(h->left->left)) h = moveRedLeft(h);
            h->left = deleteKey(h->left,key);
        } else {
            if (isRed(h->left)) h = rotateRight(h);
            if (h->key == key && !h->right) {
                Node* leftChild = h->left;
                freeNode(h);
                return leftChild;
            }
            if (!isRed(h->right) && !isRed(h->right->left)) h = moveRedRight(h);
            if (h->key == key) {
                Node* temp = h;
                h = min(temp->right);
                h->right = deleteMin(temp->right);
                h->left = temp->left;
                freeNode(temp);
            } else {
                h->right = deleteKey(h->right,key);
            }
        }
        return balance(h);
    }

    Node* get(Node* h, double key) {
        if (!h) return NULL;
        if (h->key > key) return get(h->left, key);
        else if (h->key < key) return get(h->right, key);
        else return h;
    }

    Node* select(Node* h, int k) {
        if (!h) return NULL;
        int t = sizeof(h->left);
        if (t < k) return select(h->right,k-t-1);
        else if (t > k) return select(h->left,k);
        else return h;
    }

// largest smaller
    Node* floor(Node* h, double key) {
        if (!h) return NULL;
        if (h->key > key) return floor(h->left,key);
        else if (h->key == key) return h;
        Node* temp = floor(h->right, key);
        return temp ? temp : h;
    }

// smallest larger
    Node* ceiling(Node* h, double key) {
        if (!h) return NULL;
        if (h->key < key) return ceiling(h->right, key);
        else if (h->key == key) return h;
        Node* temp = ceiling(h->left,key);
        return temp ? temp : h;
    }

    Node* rotateLeft(Node* h) {
        Node* x = h->right;
        h->right = x->left;
        x->left = h;
        x->color = h->color;
        x->N = h->N;
        h->color = RED;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* rotateRight(Node* h) {
        Node* x = h->left;
        h->left = x->right;
        x->right = h;
        x->color = h->color;
        x->N = h->N;
        h->color = RED;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* moveRedLeft(Node* h) {
        flipColor(h);
        if (isRed(h->right->left)) {
            h->right = rotateRight(h->right);
            h = rotateLeft(h);
            flipColor(h);
        }
        return h;
    }

    Node* moveRedRight(Node* h) {
        flipColor(h);
        if (isRed(h->left->left)) {
            h = rotateRight(h);
            flipColor(h);
        }
        return h;
    }

    void flipColor(Node* h) {
        h->color = !h->color;
        if (h->left) h->left->color = !h->left->color;
        if (h->right) h->right->color = !h->right->color;
    }

    Node* balance(Node* h) {
        if (!isRed(h->left) && isRed(h->right)) h = rotateLeft(h);
        if (isRed(h->left) && isRed(h->left->left)) h = rotateRight(h);
        if (isRed(h->right) && isRed(h->left)) flipColor(h);
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }

    int rank(Node* h, double key) {
        if (!h) return 0;
        if (h->key < key) return rank(h->right,key) + size(h->left) + 1;
        else if (h->key > key) return rank(h->left,key);
        else return size(h->left);
    }

    void keys(Node* h, Queue<int>& queue, int lo, int hi) {
        if (!h) return;
        if (h->key < lo) return;
        if (h->key > hi) return;
        keys(h->left,queue,lo,hi);
        queue.enqueue(h->key);
        keys(h->right,queue,lo,hi);
    }

    string toString(Node* h) {
        if (!h) return "";
        string rv = toString(h->left);
        rv += h->toString();;
        rv += toString(h->right);
        return rv;
    }

    Node* root = NULL;
    vector<unique_ptr<Node>> allNodes;
    
public:
    STdouble() = default;

    int size() { return size(root); }

    bool isEmpty() { return size() == 0; }

    void put(double key, Value val) {
        root = put(root,key,val);
        root->color = BLACK;
    }

    void deleteKey(double key) {
        if (!root) return;
        if (!isRed(root->left) && !isRed(root->right)) root->color = RED;
        root = deleteKey(root,key);
        if (root) root->color = BLACK;
    }

    Queue<int> keys(int lo, int hi) {
        Queue<int> queue;
        keys(root,queue,lo,hi);
        return queue;
    }

    int max() { return max(root)->key; }

    int min() { return min(root)->key; }

    Queue<int> keys() { return keys(min(),max()); }

    string toString() { return toString(root); }

};

int main() {
    STint<string> st;
    STdouble<string> dst;
    st.put(1,"thinh");
    st.put(2,"phong");
    st.put(3,"cuong");

    dst.put(1.94,"thinh");
    dst.put(4.22,"phong");
    dst.put(3.72,"cuong");

    cout << st.toString() << endl;
    cout << dst.toString() << endl;
}
