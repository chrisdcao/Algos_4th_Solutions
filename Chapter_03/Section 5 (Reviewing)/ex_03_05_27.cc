#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <string>
#include <memory>
#include "SymbolTables.h"

using namespace std;

// double là chỉ số sắp xếp cho cây, chỉ số này giúp Tree thực hiện được thao tác addFront và addBack (thay vì bị sorted-add như thông thường)
// chúng ta thực hiện bằng cách để chỉ số double này làm Key:
// mỗi key mới khi thực hiện addFront chúng ta sẽ gen ra 1 chỉ số double sao cho chỉ số là min -> value sẽ được add vào min, sau đó khi truy xuất keyFront sẽ truy xuất thông qua min(). Tư duy tương tự vs addBack
template <typename Key> 
class List {
    const double CHANGE_STEP = 0.00001;
    const int INIT_VALUE = 50000;
    
private:
    // this class only have Key as the template, the value is just for indexing and finding operations
    SeparateChainingHashST<Key, double> st;
    RedBlackBST<double, Key> ts; // value, key as we want to put the data in with manipulation
    
public:
    List() = default;
    
    List(int cap): st(cap) {}
    
    List(const initializer_list<Key>& list): st(list.size() * 2) {
        for (const auto& key : list) addBack(key);
    }
    
    void addFront(Key key) {
        // we just have to check if one of the ST contains the key (for certain the other will contains it too)
        // check using the one with fastest item find
        //if (st.contains(key)) return;
        if (st.contains(key)) {
            deleteKey(key);
        }
        if (isEmpty()) {
            st.put(key, INIT_VALUE);
            ts.put(INIT_VALUE, key);
            return;
        }
        double newValMin = ts.min() - CHANGE_STEP;
        st.put(key,newValMin);
        ts.put(newValMin, key);
    }
    
    void addBack(Key key) {
        //if (st.contains(key)) return;
        cout << "key to add: " << key << endl;
        if (isEmpty()) {
            st.put(key, INIT_VALUE);
            ts.put(INIT_VALUE, key);
            return;
        }
        if (st.contains(key)) {
            deleteKey(key);
        }
        cout << ts.getRoot() << endl;
        cout << ts.max() << endl;
        double newValMax = ts.max() + CHANGE_STEP;
        cout << "st.putKey " << key << endl;
        st.put(key,newValMax);
        cout << "ts.putKey " << key << endl;
        ts.put(newValMax,key);
    }
    
    Key deleteFront() {
        Key key = ts.get(ts.min());
        ts.deleteMin();
        st.deleteKey(key);
        return key;
    }
    
    Key deleteBack() {
        Key key = ts.get(ts.max());
        ts.deleteMax();
        st.deleteKey(key);
        return key;
    }
    
    void deleteKey(Key key) {
        double val = st.get(key);
        // this is causing the seg fault when deleting a key that's not in the tree
        ts.deleteKey(val);
        cout << "process ts.delete:" << setprecision(10) << val << endl;
        st.deleteKey(key);
        cout << "process st.delete:" << setprecision(10) << key << endl;
    }
    
    void add(int i, Key key) {
        if (st.contains(key)) return; // getting the key using hash is fastest
        if (isEmpty()) {
            st.put(key, INIT_VALUE);
            ts.put(INIT_VALUE, key);
            return;
        }
        if (i == 0) { addFront(key); return; } 
        else if (i == size()) { addBack(key); return; }
        // since this i is seen as the index counting from 0 (which is the min() in the tree based on how we are putting keys in)
        // we are counting from the min() to the relative_i, thus we have to convert this index to proper index
        double currentMinIndex = ts.min();
        // before_i_index = (i-1) * CHANGE_STEP;
        // after_i_index  =  i * CHANGE_STEP;
        double converted_i = currentMinIndex + (i - 1 + i) / 2 * CHANGE_STEP;
        st.put(key, converted_i);
        ts.put(converted_i, key);
    }
    
    Key deleteKeyAt(int i) {
        double currentMinIndex = ts.min();
        double converted_i = currentMinIndex + i * CHANGE_STEP;
        Key key = ts.get(converted_i);
        // using contains(key) from st is faster, thus we HAVE TO have the corner case
        if (!st.contains(key)) return NULL;
        st.deleteKey(key);
        ts.deleteKey(converted_i);
        return key;
    }
    
    bool contains(Key key) { return st.contains(key); }
    
    bool isEmpty() { return size() == 0; }
    
    int size()  { return st.size(); }
    
    string toString()  { 
        ostringstream oss;
        for (auto s : ts.keys()) 
            oss << ts.get(s) << " ";
        return oss.str();
    }
    
    string debug() {
        return ts.toString();
    }
};

int main() {
    List<int> list = {4,5,2,1,34,5,2,2,542,23};
    list.addFront(12);
    list.addBack(123);
    list.addBack(2);
    cout << list.toString() << endl;
    
    RedBlackBST<int,int> bst={{1,2}, {2,3}, {3,4}, {4,5}};
    cout << bst.max() << endl;
    bst.deleteKey(123);
}
