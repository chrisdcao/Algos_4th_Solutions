#include <iostream>
#include <sstream>
#include <unordered_map>
#include <typeinfo>
#include <cstring>

using namespace std;

template <typename Value>
class HashSTint {
private:
    int hash(int key) { return 11*std::hash<int>{}((int)key)%M; }

    int hash(int key, int cap) { return 11*std::hash<int>{}((int)key)%cap; }

    void resize(int cap) {
        int** keyTemp = new int*[cap];
        Value** valTemp = new Value*[cap];
        memset(keyTemp,0x0,sizeof(int*)*cap);
        memset(valTemp,0x0,sizeof(Value*)*cap);
        if (keys) {
            for (int i = 0; i < M; i++) {
                if (keys[i]) {
                    int j;
                    for (j = hash(*keys[i], cap); keyTemp[j]; j=(j+1)%cap);
                    keyTemp[j] = keys[i];
                    valTemp[j] = vals[i];
                }
            }
        }
        keys = keyTemp; vals = valTemp;
        keyTemp = 0x0; valTemp = 0x0;
        delete keyTemp; delete valTemp;
        M = cap;
    }

    int getIndex(int key) {
        int i = hash(key);
        while (keys[i]) {
            if (*keys[i] == (key)) return i;
            i = (i+1)%M;
        }
        return i;
    }

    int** keys = 0x0;
    Value** vals = 0x0;
    int M = 17;
    int N = 0;

public:
    friend ostream& operator << (ostream& os, const HashSTint& st) {
        for (int i = 0; i < st.M; i++)
            if (st.keys[i])
                os << to_string(i) << ". Key: " << *st.keys[i] << ". Value: " << *st.vals[i] << "\n";
        return os;
    }

    HashSTint(): HashSTint(17) {}

    HashSTint(int cap) { resize(cap); }

    void put(int key, Value val) {
        if (N>=M/2) resize(M*2);
        int i;
        for (i = hash(key); keys[i]; i = (i+1)%M) {
            if (keys[i]) {
                if (*keys[i] == (key)) {
                    delete vals[i];
                    vals[i] = new Value(val);
                    return;
                }
            }
        }
        keys[i] = new int(key);
        vals[i] = new Value(val);
        N++;
    }

    void deleteKey(int key) {
        if (!contains(key)) return;
        int i = hash(key);
        while (*keys[i] != key) i=(i+1)%M;
        keys[i] = 0x0;
        vals[i] = 0x0;
        i = (i+1)%M;
        while (keys[i]) {
            int keyToRedo = *keys[i];
            Value valToRedo = *vals[i];
            keys[i] = 0x0;
            vals[i] = 0x0;
            N--;
            put(keyToRedo, valToRedo);
            i = (i+1)%M;
        }
        N--;
        if (N > 0 && N == M/8) resize(M/2);
    }

    bool contains(int key) {
        return getIndex(key) != -1;
    }

    Value get(int key) {
        int i = getIndex(key);
        return i == -1 ? NULL : *vals[i];
    }

    string toString() {
        ostringstream oss;
        oss << (*this);
        return oss.str();
    }

};

template <typename Value>
class HashSTdouble {
private:
    double hash(double key) { return 11*std::hash<int>{}((int)key)%M; }

    double hash(double key, double cap) { return 11*std::hash<int>{}((int)key)%cap; }

    void resize(double cap) {
        double** keyTemp = new double*[cap];
        Value** valTemp = new Value*[cap];
        memset(keyTemp,0x0,sizeof(double*)*cap);
        memset(valTemp,0x0,sizeof(Value*)*cap);
        if (keys) {
            for (double i = 0; i < M; i++) {
                if (keys[i]) {
                    double j;
                    for (j = hash(*keys[i], cap); keyTemp[j]; j=(j+1)%cap);
                    keyTemp[j] = keys[i];
                    valTemp[j] = vals[i];
                }
            }
        }
        keys = keyTemp; vals = valTemp;
        keyTemp = 0x0; valTemp = 0x0;
        delete keyTemp; delete valTemp;
        M = cap;
    }

    double getIndex(double key) {
        double i = hash(key);
        while (keys[i]) {
            if (*keys[i] == (key)) return i;
            i = (i+1)%M;
        }
        return i;
    }

    double** keys = 0x0;
    Value** vals = 0x0;
    double M = 17;
    double N = 0;

public:
    friend ostream& operator << (ostream& os, const HashSTdouble& st) {
        for (double i = 0; i < st.M; i++)
            if (st.keys[i])
                os << to_string(i) << ". Key: " << *st.keys[i] << ". Value: " << *st.vals[i] << "\n";
        return os;
    }

    HashSTdouble(): HashSTdouble(17) {}

    HashSTdouble(double cap) { resize(cap); }

    void put(double key, Value val) {
        if (N>=M/2) resize(M*2);
        double i;
        for (i = hash(key); keys[i]; i = (i+1)%M) {
            if (keys[i]) {
                if (*keys[i] == (key)) {
                    delete vals[i];
                    vals[i] = new Value(val);
                    return;
                }
            }
        }
        keys[i] = new double(key);
        vals[i] = new Value(val);
        N++;
    }

    void deleteKey(double key) {
        if (!contains(key)) return;
        double i = hash(key);
        while (*keys[i] != key) i=(i+1)%M;
        keys[i] = 0x0;
        vals[i] = 0x0;
        i = (i+1)%M;
        while (keys[i]) {
            double keyToRedo = *keys[i];
            Value valToRedo = *vals[i];
            keys[i] = 0x0;
            vals[i] = 0x0;
            N--;
            put(keyToRedo, valToRedo);
            i = (i+1)%M;
        }
        N--;
        if (N > 0 && N == M/8) resize(M/2);
    }

    bool contains(double key) {
        return getIndex(key) != -1;
    }

    Value get(double key) {
        double i = getIndex(key);
        return i == -1 ? NULL : *vals[i];
    }

    string toString() {
        ostringstream oss;
        oss << (*this);
        return oss.str();
    }

};

int main() {
    HashSTint<string> st;
    HashSTdouble<string> dst;
    st.put(3,"200");
    st.put(4,"100");
    st.put(56,"10");

    dst.put(3,"200");
    dst.put(4,"100");
    dst.put(56,"10");
    cout << st.toString() << endl;
    cout << dst.toString() << endl;
}
