#include <iostream>
#include <sstream>
#include <cstring>
#include <vector>
#include <memory>
#include "Queue.h"
#include <climits>

using namespace std;

const int PRIMES[] = {
    1, 1, 3, 7, 13, 31, 61, 127, 251, 509, 1021, 2039, 4093, 8191, 16381,
    32749, 65521, 131071, 262139, 524287, 1048573, 2097143, 4194301,
    8388593, 16777213, 33554393, 67108859, 134217689, 268435399,
    536870909, 1073741789, 2147483647
};

template <typename Key, typename Value>
class SeparateChainingMultiST {
private:
    class Node {
    public:
        Key key;
        Value val;
        int index = -1;
        Node* next = NULL;
        friend ostream& operator<<(ostream& os, const Node& h) {
            os << "Key: " << h.key << ". Value: " << h.val << ". Next: ";
            if (h.next) os << h.next->key;
            else        os << "null";
            return os;
        }
        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };
    
    Node* newNode(Key key, Value val) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() -1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        return allNodes.back().get();
    }
    
    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }
    
    int hash(Key key) {
        int hashCode = std::hash<Key>{}(key) & INT_MAX;
        if (logM < 26) hashCode = hashCode % PRIMES[logM+5];
        return hashCode % M;
    }

    Node* get(Key key) {
        Node* ptr = st[hash(key)];
        while (ptr) {
            if (ptr->key == key) return ptr;
            ptr = ptr->next;
        }
        return NULL;
    }
    
    vector<unique_ptr<Node>> allNodes;
    vector<Node*> st;
    int M = 17;
    int N = 0;
    int logM;
    
public:
    SeparateChainingMultiST(): SeparateChainingMultiST(M) {}
        
    SeparateChainingMultiST(int cap): M(cap) {st.resize(cap);}
    
    SeparateChainingMultiST(const initializer_list<pair<Key,Value>>& list): M(list.size()) {
        st.resize(list.size());
        for (const auto& s : list) 
            add(s.first,s.second);
    }
    
    void add(Key key, Value val) {
        Node* ptr = st[hash(key)];
        if (!ptr) 
            st[hash(key)] = newNode(key,val);
        else {
            while(ptr->next)
                ptr = ptr->next;
            ptr->next = newNode(key,val);
        }
        N++;
    }
    
    void deleteSingleKey(Key key) {
        Node* ptr = st[hash(key)];
        Node* prevPtr = NULL;
        while (ptr) {
            if (ptr->key == key) {
                if (!prevPtr) {
                    if(ptr->next) st[hash(key)] = ptr->next;
                    else          st[hash(key)] = NULL;
                } else
                    prevPtr->next = ptr->next;
                N--; return;
            }
            prevPtr = ptr;
            ptr = ptr->next;
        }
    }
    
    bool contains(Key key) { return get(key)!=NULL; }
    
    void deleteKey(Key key) { 
        while (contains(key)) 
            deleteSingleKey(key); 
    }
    
    Queue<Value> getAll(Key key) {
        Queue<Value> queue;
        if (!contains(key)) return queue;
        Node* ptr = st[hash(key)];
        while (ptr) {
            if (ptr->key == key) 
                queue.enqueue(ptr->val);
            ptr = ptr->next;
        }
        return queue;
    }
    
    Value getVal(Key key) {
        Node* rv = get(key);
        return rv ? rv->val : NULL;
    }
    
    string toString() {
        string rv = "\n";
        for (int i = 0; i < M; i++) {
            Node* ptr = st[i];
            while (ptr)  {
                rv += to_string(i) + ". " + ptr->toString() + "\n";
                ptr = ptr->next;
            }
        }
        return rv;
    }
    
};

template <typename Key, typename Value>
class BinarySearchMultiST {
private:
    class Node {
    public:
        Key key;
        Value val;
        int index = -1;
        Node* left = NULL;
        Node* right = NULL;
        int N = 1;
        friend ostream& operator<<(ostream& os, const Node& h) {
            os << "Key: " << h.key << ". Value: " << h.val;
            os << ". Left: "; 
            if (h.left) os << h.left->key;
            else        os << "null";
            os << ". Right: ";
            if (h.right) os << h.right->key;
            else         os << "null";
            os << endl;
            return os;
        }
        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };
    
    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }
    
    Node* newNode(Key key, Value val, int N) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        return allNodes.back().get();
    }
    
    int size(Node* h) { return h ? h->N : 0; }
    
    Node* put(Node* h, Key key, Value val) {
        if (!h) return newNode(key,val,1);
        if (h->key > key) h->left = put(h->left, key, val);
        else if (h->key <= key) h->right = put(h->right,key,val);
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }
    
    Node* deleteKey(Node* h, Key key) {
        if (!h) return NULL;
        if (h->key > key) h->left = deleteKey(h->left, key);
        else if (h->key < key) h->right = deleteKey(h->right,key);
        else {
            if (!h->right) { return h->left; }
            if (!h->left) { return h->right; }
            Node* t = h;
            h = min(t->right);
            h->right = deleteMin(t->right);
            h->left = t->left;
            freeNode(t);
        }
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }
    
    Node* min(Node* h) {
        return h ? h->left ? min(h->left) : h : h;
    }
    
    Node* deleteMin(Node* h) {
        if (!h) return NULL;
        if (!h->left) return h->right;
        h->left = deleteMin(h->left);
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }
    
    Node* select(Node* h, int k)  {
        if (!h) return NULL;
        int t = size(h->left);
        if (t < k) return select(h->right,k-t-1);
        else if (t > k) return select(h->left, k);
        else return h;
    }
    
    int rank(Node* h, Key key) {
        if (!h) return 0;
        if (h->key > key) return rank(h->left,key);
        else if (h->key < key) return size(h->left) + rank(h->right) + 1;
        else return size(h->left);
    }
    
    Node* get(Node* h, Key key) {
        if (!h) return NULL;
        if (h->key > key) return get(h->left,key);
        else if (h->key < key) return get(h->right,key);
        else return h;
    }
    
    void getAll(Node* h, Queue<Value>& queue, Key key) {
        if (!h) return;
        if (h->key > key) return getAll(h->left,queue,key);
        else if (h->key < key) return getAll(h->right,queue,key);
        else {
            queue.enqueue(h->val);
            return getAll(h->right,queue,key);
        }
    }
        
    vector<unique_ptr<Node>> allNodes;
    Node* root = NULL;
    
    string toString(Node* h) {
        if (!h) return "";
        string rv = "";
        rv += toString(h->left);
        rv += h->toString();
        rv += toString(h->right);
        return rv;
    }
    
public:
    BinarySearchMultiST() = default;
    
    BinarySearchMultiST(const initializer_list<pair<Key,Value>>& list) {
        for (const auto& s : list) put(s.first,s.second);
    }
    
    int size() { return size(root); }
    
    bool isEmpty() { return size() == 0; }
    
    void put(Key key, Value val) {
        root = put(root,key,val);
    }
    
    void deleteKey(Key key) {
        while (contains(key)) 
            root = deleteKey(root,key);
    }
    
    bool contains(Key key) {
        return get(root,key) != NULL;
    }
    
    Value get(Key key) {
        Node* rv = get(root,key);
        return rv ? rv->val : NULL;
    }
    
    Key select(int k) {
        Node* rv = select(root, k);
        return rv ? rv->key : NULL;
    }
    
    Queue<Value> getAll(Key key) {
        Queue<Value> queue;
        getAll(root,queue,key);
        return queue;
    }
    
    string toString() { return toString(root); }
};

int main() {
    SeparateChainingMultiST<int,double> st = {
        {1,1.81}, {1,2.22}, {2,1.34}, 
        {3,4.54}, {5,6.23}, {7,7.21}, 
        {8,7.76}, {8,10.11}, {1, 0.1}
    };
    cout << st.toString() << endl;
    
    cout << "getAll(1): ";
    for (auto s : st.getAll(1))
        cout << s << " ";
    cout << endl << endl;
    
    cout << "After deleteKey(1):";
    st.deleteKey(1);
    cout << st.toString() << endl;
    cout << endl;
    
    BinarySearchMultiST<int,double> bst = {
        {1,1.81}, {1,2.22}, {2,1.34}, 
        {3,4.54}, {5,6.23}, {7,7.21}, 
        {8,7.76}, {8,10.11}, {1, 0.1}
    };
    cout << "BinarySearchST: "  << endl;
    cout << bst.toString() << endl;
    
    cout << "getAll(1): " << endl;
    for (auto s : bst.getAll(1))
        cout << "Value: " << s << endl;
    cout << endl << endl;
    
    cout << "After deleteKey(1):" << endl;
    bst.deleteKey(1);
    cout << bst.toString() << endl;
}

