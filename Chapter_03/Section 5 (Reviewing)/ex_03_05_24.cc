#include <iostream>
#include <cstring>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

class Interval {
public:
    string begin = "";
    string end = "";
    int index = -1;
    Interval() = default;
    Interval(string begin,string end, int index): begin(begin), end(end), index(index) {}
    friend istream& operator >> (istream& is, Interval& interval) {
        is >> interval.begin;
        is >> interval.end;
        is >> interval.index;
        return is;
    }
    friend ostream& operator << (ostream& os, const Interval& interval) {
        os << "Index: " << interval.index << ": [" << interval.begin << " - " << interval.end << "]";
        return os;
    }
    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }
};

int findInterval(const string& interval, const vector<Interval>& intervals) {
    int N = intervals.size();
    for (int i = 0; i < N; i++) {
        if (intervals[i].begin <= interval 
            && intervals[i].end >= interval) 
            return i;
    }
    return -1;
};

int main() {
    int length;
    cin >> length;
    vector<Interval> intervals;
    string begin,end;
    for (int i = 0; i < length; i++) {
        cin >> begin >> end;
        Interval input(begin,end,i);
        intervals.push_back(input);
    }
    cout << "Please input a number to find interval: ";
    string interval;
    cin >> interval; 
    cout << endl;
    cout << "intervals: " << endl;
    for (const auto& s : intervals)
        cout << s << endl;
    cout << endl;
    const int result = findInterval(interval, intervals);
    if (result != -1)
        cout << "The input " << interval << " belongs to intervals[" << result << "]" << endl;
    else 
        cout << "No interval for input" << endl;
    
    return 0;
}
