#ifndef BAG_H
#define BAG_H

#include <iostream>
#include <vector>
#include <cstring>
#include <memory>

using namespace std;

template <typename Key>
class Bag {
private:
    class Node {
    public:
        Key key;
        Node* next = NULL;
        int index = -1;
    };
    
    Node* newNode(Key key) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->key = key;
        return allNodes.back().get();
    }
    
    void freeNode(Node* x) {
        if (x->index < allNodes.size()-1) {
            allNodes[x->index].swap(allNodes.back());
            allNodes[x->index]->index = x->index;
        }
        allNodes.pop_back();
    }
    
    class Iterator {
    private:
        Node* currentLoc = NULL;
    public:
        Iterator(Node* initLoc) {
            currentLoc = initLoc;
        }
        Iterator& operator++() {
            currentLoc = currentLoc->next;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return currentLoc != rhs.currentLoc;
        }
        Key operator*() {
            return currentLoc->key;
        }
    };    
    
    vector<unique_ptr<Node>> allNodes;
    int N = 0;
    Node* head = NULL;
    Node* tail = NULL;
    
public:
    int size() { return N; }
    
    bool isEmpty() { return size() == 0; }
    
    void add(Key key) {
        Node* temp = newNode(key);
        if (head == NULL) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        N++;
    }
    
    Iterator begin() { return Iterator(head); }
    
    Iterator end() { return NULL; }
};

#endif
