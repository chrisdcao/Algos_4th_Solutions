#ifndef DOUBLYLINKEDLIST_H
#define DOUBLYLINKEDLIST_H

#include <iostream>
#include <vector>
#include <initializer_list>
#include <sstream>
#include <memory>

using namespace std;

template <typename Key> 
class DoublyLinkedList {
private:
    class Node {
    public:
        Key key;
        int index = -1;
        Node* next = NULL;
        Node* prev = NULL;
        friend ostream& operator<<(ostream& os, const Node& s) {
            os << "Key: " << s.key;
            os << ". Prev: ";
            if (s.prev) os << s.prev->key;
            else         os << "null";
            os << ". Next: ";
            if (s.next) os << s.next->key;
            else         os << "null";
            os << endl;
            return os;
        }
        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };
    
    Node* newNode(Key key) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        return allNodes.back().get();
    }
    
    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }
    
    Node* head = NULL;
    Node* tail = NULL;
    int N = 0;
    vector<unique_ptr<Node>> allNodes;
    
public:
    Key getHeadKey() { 
        return head ? head->key : NULL; 
    }
    
    Key getTailKey() { 
        return tail ? tail->key : NULL; 
    }
    
    string toString() {
        string str="";
        Node* ptr = head;
        while (ptr) {
            str += ptr->toString();
            ptr = ptr->next;
        }
        return str;
    }
    
    DoublyLinkedList() = default;
    
    DoublyLinkedList(const initializer_list<Key>& list) {
        for (const auto& s : list) addBack(s);
    }
    
    int size() { return N; }
    
    bool isEmpty() { return size() == 0; }
    
    void addFront(Key key) {
        if (head == NULL) addBack(key);
        else {
            Node* temp = newNode(key);
            temp->next = head;
            head->prev = temp;
            head = temp;
        }
        N++;
    }
    
    void addBack(Key key) {
        if (head == NULL) {
            tail = newNode(key);
            head = tail;
        } else {
            Node* temp = newNode(key);
            temp->prev = tail;
            tail->next = temp;
            tail = temp;
        }
        N++;
    }
    
    void add(Key key) { addBack(key); }
    
    Key popFront() {
        if (isEmpty()) return NULL;
        Node* oldHead =head;
        Key rv = oldHead->key;
        head = head->next;
        freeNode(oldHead); N--;
        return rv;
    }
    
    Key popBack() { popAt(N-1); }
    
    Key popAt(int i) {
        if (isEmpty()) return NULL;
        else if (i == 0) popFront();
        else {
            Node* ptr = head;
            Node* prevPtr = NULL;
            int count = 0;
            while (ptr->next) {
                if (count == i) {
                    Key rv = ptr->key;
                    Node* oldPtr = ptr;
                    prevPtr->next = ptr->next;
                    freeNode(oldPtr); N--;
                    return rv;
                }
                prevPtr = ptr;
                ptr = ptr->next;
                count++;
            }
            Key rv = ptr->key;
            prevPtr->next = ptr->next;
            freeNode(ptr); N--;
            return rv;
        }
    }
    
};

#endif
