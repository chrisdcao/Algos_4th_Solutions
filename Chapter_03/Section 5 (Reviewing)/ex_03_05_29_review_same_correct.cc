#include <iostream>
#include <sstream>
#include <initializer_list>
#include <iomanip>
#include <vector>
#include <string>
#include <memory>
#include "Queue.h"
#include "SymbolTables.h"

using namespace std;

template <typename Key>
class UniQueue {
private:
    Queue<Key> queue;
    SeparateChainingHashST<Key,int> st;

public:
    UniQueue() = default;
    UniQueue(const initializer_list<Key>& list) {
        for (const auto& s : list) enqueue(s);
    }
    int size() { return queue.size(); }
    
    bool isEmpty() { return queue.isEmpty(); }
    
    void enqueue(Key key) {
        if (st.contains(key)) return;
        queue.enqueue(key);
        st.put(key,1);
    }
    
    Key dequeue() {
        return queue.dequeue();
    }
    
    string toString() {
        ostringstream oss;
        for (const auto& s : queue)
            oss << s << " ";
        return oss.str();
    }
};

int main() {
    UniQueue<int> uq = {1,2,3,4,5};
    cout << uq.toString() << endl;
    uq.dequeue();
    cout << uq.toString() << endl;
    // the queue will ignore the inputted key in all history
    uq.enqueue(1);
    cout << uq.toString() << endl;
}
