#include <iostream>
#include <cstring>
#include <sstream>
#include <string>
#include "SymbolTables.h"

template <typename Key>
class SET {
private:
    RedBlackBST<Key,int> bst;

public:

    SET() = default;

    SET(int N): bst(N) {}

    void add(Key key) { bst.put(key, 1); }

    void deleteKey(Key key) { bst.deleteKey(key); }

    bool isEmpty() { return bst.empty(); }

    int size() { return bst.size(); }

    friend ostream& operator << (ostream& os, SET& set) {
        for (auto s : set.bst.keys())
            os << s << " ";
        return os;
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

};

template <typename Key>
class HashSET {
private:
    LinearProbingHashST<Key,int> st; // value is just dummy and thus is fixed of type 'int' value = 1

public:

    HashSET() = default;

    HashSET(int N): st(N) {}

    void add(Key key) { st.put(key, 1); }

    void deleteKey(Key key) { st.deleteKey(key); }

    bool contains(Key key) { return st.contains(key); }

    bool isEmpty() { return st.size() == 0; }

    int size() { return st.size(); }

    friend ostream& operator << (ostream& os, HashSET& set) {
        for (auto s : set.st.keyQueue())
            os << s << " ";
        return os;
    }

    string toString() { 
        ostringstream oss;
        oss << *this;
        return oss.str();
    }
};

int main() {
    SET<char> set;
    HashSET<char> hashSet;

    int length;
    cin >> length;
    char c;
    for (int i = 0; i < length; i++) {
        cin >> c;
        set.add(c);
        hashSet.add(c);
    }

    cout << "Set: " << set.toString() << endl;
    cout << "Hash set: " << hashSet.toString() << endl;

}
