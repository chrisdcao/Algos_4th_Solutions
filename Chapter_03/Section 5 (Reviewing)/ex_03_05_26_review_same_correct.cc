#include "DoublyLinkedList.h"
#include "SymbolTables.h"

using namespace std;

template <typename Key>
class LRUCache {
private:
    SeparateChainingHashST<Key,int> st;
    DoublyLinkedList<Key> dbl;
    
public:
    LRUCache() = default;
    
    LRUCache(const initializer_list<Key>& list) {
        for (const auto& s : list) access(s);
    }
    
    int size() { return dbl.size(); }
    
    bool isEmpty() { return dbl.isEmpty(); }
    
    void access(Key key) {
        if (st.contains(key)) {
            int keyIndex = st.get(key);
            // since the total num of elem not change, we just need to swap the index of currentHead with newHead in ST
            st.setValue(dbl.getHeadKey(), st.get(key));
            st.setValue(key, 0);
            dbl.popAt(keyIndex); // delete at the index
            dbl.addFront(key);
        } else {
            dbl.addFront(key);
            if (!st.isEmpty())
                for (auto s : st.keyQueue())
                    st.setValue(s, st.get(s)+1);
            st.put(key,0);
        }
    }
    
    void remove() {
        if (isEmpty()) return;
        Key removedKey = dbl.popBack();
        st.deleteKey(removedKey);
    }
    
    string toString() { return dbl.toString(); }
};

int main() {
    LRUCache<int> lru = {1,2,3,4,5};
    cout << lru.toString() << endl;
    lru.remove();
    cout << lru.toString() << endl;
    lru.remove();
    cout << lru.toString() << endl;
    lru.remove();
    cout << lru.toString() << endl;
    lru.remove();
    cout << lru.toString() << endl;
}
