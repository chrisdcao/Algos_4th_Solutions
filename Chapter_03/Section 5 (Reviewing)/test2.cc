#include <iostream>
#include <cstring>

using namespace std;

template <typename Key, typename Value>
class LinearProbingHashST {
private:
    int hash(Key key) { return 11 * std::hash<Key>{}(key) % M; }
    int hash(Key key, int cap) { return 11 * std::hash<Key>{}(key) % cap; }

    void resize(int cap) {
        Key** keyTemp = new Key*[cap];
        Value** valTemp = new Value*[cap];
        memset(keyTemp, 0x0, sizeof(Key*)*cap);
        memset(valTemp, 0x0, sizeof(Value*)*cap);
        for (int i = 0; i < M; i++) {
            if (keys[i]) {
                int j;
                // find the nearest empty place
                for (j = hash(*keys[i],cap); keyTemp[j]; j = (j+1)%M);
                keyTemp[j] = keys[i];
                valTemp[j] = vals[i];
            }
        }
        keys = keyTemp; vals = valTemp;
        keyTemp = NULL; valTemp = NULL;
        delete keyTemp; delete valTemp;
        M = cap;
    }

    Key** keys;
    Value** vals;
    int M = 17;
    int N = 0;

public:
    LinearProbingHashST(): LinearProbingHashST(17) {}

    LinearProbingHashST(int cap) { 
        keys = new Key*[cap];
        vals = new Value*[cap];
        memset(keys,0x0,sizeof(Key*)*cap);
        memset(vals,0x0,sizeof(Value*)*cap);
        M = cap;
    }
};

int main() {
    LinearProbingHashST<string,int> st;
    cout << "program executed" << endl;
}

