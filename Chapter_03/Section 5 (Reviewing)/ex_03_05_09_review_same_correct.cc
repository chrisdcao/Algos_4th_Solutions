#include <iostream>
#include <sstream>
#include <memory>
#include <vector>

using namespace std;

template <typename Key, typename Value>
class BinarySearchTree {
private:
    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        int index = -1;
        int N = 1;
        friend ostream& operator << (ostream& os, const Node& h) {
            os << "Key: " << h.key << ". Value: " << h.val << ". Size: " << h.N;
            os << ". Left: ";
            if (h.left) os << h.left->key;
            else        os << "null";
            os << ". Right: ";
            if (h.right) os << h.right->key;
            else         os << "null";
            os << endl;
            return os;
        }

        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };

    Node* newNode(Key key, Value val, int N) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        return allNodes.back().get();
    }

    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    int size(Node* h) { return h ? h->N : 0; }

    Node* put(Node* h, Key key, Value val) {
        if (!h) return newNode(key,val,1);
        if (h->key > key) h->left = put(h->left,key,val);
        else if (h->key <= key) h->right = put(h->right,key,val);
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }

    Node* deleteKey(Node* h, Key key) { // have to tweak so that it deletes all the duplicated keys
        if (!h) return NULL;
        if (h->key > key) h->left = deleteKey(h->left,key);
        else if (h->key < key) h->right = deleteKey(h->right, key);
        else {
            if (!h->right) { // since the dups resides in the right side, if rightChild == NULL then NO dups
                Node* leftChild = h->left;
                freeNode(h); 
                return leftChild;
            }
            Key dup = h->key;
            while (h) {
                if (h->key != dup) break;
                if (!h->right) { // since the dups resides in the right side, if rightChild == NULL then NO dups
                    Node* leftChild = h->left;
                    freeNode(h); 
                    return leftChild;
                }
                Node* temp = h;
                h = min(temp->right); 
                h->right = deleteMin(temp->right);
                h->left = temp->left;
                freeNode(temp);
            }
        }
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }

    Node* min(Node* h) {
        return h ? h->left ? min(h->left) : h : NULL;
    }
    
    Node* max(Node* h) {
        return h ? h->right ? max(h->right) : h : NULL;
    }

    Node* deleteMin(Node* h) {
        if (!h) return NULL;
        if (!h->left) return h->right;
        h->left = deleteMin(h->left);
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }

    Node* deleteMax(Node* h) {
        if (!h) return NULL;
        if (!h->right) return h->left;
        h->right = deleteMax(h->right);
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }

    string toString(Node* h) {
        if (!h) return "";
        string rv = toString(h->left);
        rv += h->toString();
        rv += toString(h->right);
        return rv;
    }

    vector<unique_ptr<Node>> allNodes;
    Node* root = NULL;

public:
    BinarySearchTree() = default;

    int size() { return size(root); }

    void put(Key key, Value val) { root = put(root,key,val); }

    void deleteKey(Key key) { root = deleteKey(root,key); }

    string toString() { return toString(root); }
};

int main() {
    BinarySearchTree<char,int> bst;
    int length;
    cin >> length;
    char c;
    for (int i = 0; i < length; i++) {
        cin >> c;
        bst.put(c,i);
    }
    cout << bst.toString();
    cout << "Current tree size: " << bst.size() << endl << endl;

    cout << "delete key 'Y':" << endl;
    bst.deleteKey('Y');
    cout << bst.toString();
    cout << "Current tree size: " << bst.size() << endl << endl;

    cout << "delete key 'N':" << endl;
    bst.deleteKey('N');
    cout << bst.toString();
    cout << "Current tree size: " << bst.size() << endl << endl;
}
