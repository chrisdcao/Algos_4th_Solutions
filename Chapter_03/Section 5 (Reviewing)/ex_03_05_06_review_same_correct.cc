#include <iostream>
#include <cmath>
#include <sstream>
#include <climits>
#include <cstring>

using namespace std;

const int PRIMES[] = {
    1, 1, 3, 7, 13, 31, 61, 127, 251, 509, 1021, 2039, 4093, 8191, 16381,
    32749, 65521, 131071, 262139, 524287, 1048573, 2097143, 4194301,
    8388593, 16777213, 33554393, 67108859, 134217689, 268435399,
    536870909, 1073741789, 2147483647
};

class HashSETint {
private:
    int hash(int key) {
        int hash = std::hash<int>{}(key) & INT_MAX;
        if (logM < 26)
            hash = hash % PRIMES[logM + 5];
        return hash % M;
    }

    void resize(int cap) {
        int** keyTemp = new int*[cap];
        memset(keyTemp, 0x0, sizeof(int*)*cap);
        int oldM = M;
        M = cap;
        logM = (int) log(M) / log(2);
        if (keys) {
            for (int i = 0; i < oldM; i++) {
                if (keys[i]) {
                    int j;
                    for (j = hash(*keys[i]);keyTemp[j];j=(j+1)%M);
                    keyTemp[j] = keys[i];
                }
            }
        }
        keys = keyTemp;
        keyTemp = 0x0; delete keyTemp;
    }

    int getIndex(int key) {
        int i = hash(key);
        while (keys[i]) {
            if (*keys[i] == key) return i;
            i = (i+1)%M;
        }
        return -1;
    }

    int** keys = 0x0;
    int logM;
    int M = 17;
    int N = 0;

public:
    friend ostream& operator << (ostream& os, const HashSETint& set) {
        for (int i = 0; i < set.M; i++)
            if (set.keys[i])
                os << to_string(i) << ". Key: " << *set.keys[i] << "\n";
        return os;
    }

    HashSETint(): HashSETint(17) {}

    HashSETint(int cap) { resize(cap); }

    void put(int key) {
        if (N >= M/2) resize(M*2);
        int i;
        for (i = hash(key); keys[i]; i = (i+1)%M)
            if (*keys[i] == key) return;
        keys[i] = new int(key);
        N++;
    }

    void deleteKey(int key) {
        if (!contains(key)) return;
        int i = hash(key);
        while (*keys[i] != key) i = (i+1)%M;
        keys[i] = 0x0;
        i = (i+1)%M;
        while (keys[i]) {
            int keyToRedo = *keys[i];
            keys[i] = 0x0;
            N--;
            put(keyToRedo);
            i = (i+1)%M;
        }
        N--;
        if (N > 0 && N == M/8) resize(M/2);
    }

    bool contains(int key) { return getIndex(key) != -1; }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

};

class HashSETdouble {
private:
    int hash(double key) {
        int hash = std::hash<int>{}((int)key) & INT_MAX;
        if (logM < 26)
            hash = hash % PRIMES[logM + 5];
        return hash % M;
    }

    void resize(int cap) {
        double** keyTemp = new double*[cap];
        memset(keyTemp, 0x0, sizeof(double*)*cap);
        int oldM = M;
        M = cap;
        logM = (int) log(M) / log(2);
        if (keys) {
            for (int i = 0; i < oldM; i++) {
                if (keys[i]) {
                    int j;
                    for (j = hash(*keys[i]);keyTemp[j];j=(j+1)%M);
                    keyTemp[j] = keys[i];
                }
            }
        }
        keys = keyTemp;
        keyTemp = 0x0; delete keyTemp;
    }

    int getIndex(double key) {
        int i = hash(key);
        while (keys[i]) {
            if (*keys[i] == key) return i;
            i = (i+1)%M;
        }
        return -1;
    }

    double** keys = 0x0;
    int logM;
    int M = 17;
    int N = 0;

public:
    friend ostream& operator << (ostream& os, const HashSETdouble& set) {
        for (int i = 0; i < set.M; i++)
            if (set.keys[i])
                os << to_string(i) << ". Key: " << *set.keys[i] << "\n";
        return os;
    }

    HashSETdouble(): HashSETdouble(17) {}

    HashSETdouble(int cap) { resize(cap); }

    void put(double key) {
        if (N >= M/2) resize(M*2);
        int i;
        for (i = hash(key); keys[i]; i = (i+1)%M)
            if (*keys[i] == key) return;
        keys[i] = new double(key);
        N++;
    }

    void deleteKey(double key) {
        if (!contains(key)) return;
        int i = hash(key);
        while (*keys[i] != key) i = (i+1)%M;
        keys[i] = 0x0;
        i = (i+1)%M;
        while (keys[i]) {
            double keyToRedo = *keys[i];
            keys[i] = 0x0;
            N--;
            put(keyToRedo);
            i = (i+1)%M;
        }
        N--;
        if (N > 0 && N == M/8) resize(M/2);
    }

    bool contains(double key) { return getIndex(key) != -1; }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

};

int main() {
    int length;
    cin >> length;
    int x;
    HashSETint setInt;
    HashSETdouble setDouble;
    for (int i = 0; i < length; i++) {
        cin >> x;
        setInt.put(x);
    }
    double d;
    for (int i = 0; i < length; i++) {
        cin >> d;
        setDouble.put(d);
    }
    
    cout << setInt.toString() << endl;
    cout << setDouble.toString() << endl;
}
