#include <iostream>
#include <sstream>
#include <vector>
#include <memory>
#include <climits>
#include <cstring>
#include "Queue.h"

using namespace std;

template <typename Key, typename Value>
class LinearProbingHashST {
private:
    int hash(Key key) { return 11 * std::hash<Key>{}(key)%M; }

    int hash(Key key, int cap) { return 11 * std::hash<Key>{}(key)%cap; }

    void resize(int cap) {
        cout << "resize is called" << endl;
        Key** keyTemp = new Key*[cap];
        Value** valTemp = new Value*[cap];
        memset(keyTemp,0x0,sizeof(Key*)*cap);
        memset(valTemp,0x0,sizeof(Value*)*cap);
        if (keys) {
            for (int i = 0; i < M; i++) { 
                if (keys[i]) {
                    int j;
                    for (j = hash(*keys[i],cap); keyTemp[j]; j = (j+1)%cap);
                    keyTemp[j] = keys[i];
                    valTemp[j] = vals[i];
                }
            }
        }
        keys = keyTemp; vals = valTemp;
        keyTemp = 0x0; valTemp = 0x0;
        delete keyTemp; delete valTemp;
        M = cap;
        return;
    }

    int getIndex(Key key) {
        int i = hash(key);
        while (keys[i]) {
            if (*keys[i] == key) return i;
            i = (i+1)%M;
        }
        return -1;
    }

    Key** keys = 0x0;
    Value** vals = 0x0;
    int M = 17;
    int N = 0;

public:
    friend ostream& operator<< (ostream& os, const LinearProbingHashST& st) {
        for (int i = 0; i < st.M; i++) {
            if (st.keys[i])
                os << to_string(i) << ". Key: " << *st.keys[i] << ". Value: " << *st.vals[i] << "\n";
        }
        return os;
    }
    LinearProbingHashST(): LinearProbingHashST(17) {}
    
    LinearProbingHashST(int cap) { resize(cap); }
    
    void put(Key key, Value val) {
        if (N >= M/2) resize(M*2);
        int i;
        for (i = hash(key); keys[i]; i=(i+1)%M); // just find the nearest empty place, without detecting the dups
        keys[i] = new Key(key);
        vals[i] = new Value(val);
        N++;
        return;
    }

    void deleteKey(Key key) {
        if (!contains(key)) return;
        int i = hash(key);
        while (*keys[i] != key) i=(i+1)%M;
        while (keys[i]) {
            if (*keys[i] != key) break; // NULL-safe for mingw32, though this still runs okay on Linux Environment
            keys[i] = 0x0;
            vals[i] = 0x0;
            i = (i+1)%M;
        }
        while (keys[i]) {
            Key keyToRedo = *keys[i];
            Value valToRedo = *vals[i];
            keys[i] = 0x0;
            vals[i] = 0x0;
            N--;
            put(keyToRedo, valToRedo);
            i = (i+1)%M;
        }
        N--;
        if (N > 0 && N == M/8) resize(M/2);
    }

    Value get(Key key) {
        int i = getIndex(key);
        return i == -1 ? NULL : *vals[i];
    }

    bool contains(Key key) {
        return getIndex(key) != -1;
    }

    Value operator[](Key key) {
        return get(key);
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

    Queue<Key> keyQueue() {
        Queue<Key> queue;
        for (int i =0 ; i < M; i++) {
            if (keys[i]) {
                queue.enqueue(*keys[i]);
            }
        }
        return queue;
    }
};

int main() {
    int length;
    cin >> length;
    string str;
    LinearProbingHashST<string, int> stProb(4);
    for (int i = 0; i < length; i++) {
        cin >> str;
        stProb.put(str, i);
    }
    cout << stProb.toString() << endl;
    cout << "delete Key: N from Linear hash!" << endl;
    stProb.deleteKey("N");
    cout << stProb.toString() << endl;
}

