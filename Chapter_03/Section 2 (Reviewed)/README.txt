Binary tree print in this section will have the following format:

6[1]<0>                                |    with 1 Node format being:  Key[idx_in_tree]<lvl_in_tree>
3[2]<1> 8[3]<1>                        |                               i.e. 6[1]<0>: Key: 6
2[4]<2> 4[5]<2> 7[6]<2> 11[7]<2>       |                                             idx_in_tree: 1
5[11]<3> 12[15]<3>                     |                                             lvl_in_tree: 0 

The height layout is correctly represented. For the children:

Since a child_index can only be either = (parent_index * 2) or (parent_index * 2 + 1), any child_index larger than max of that (i.e. parent_index * 2 + 1) surely belongs to another parent
i.e. Read part of the tree above:

            6[1]<0>
           /       \
    3[2]<1>         8[3]<1>
    /      \         /     \
2[4]<2>  4[5]<2>  7[6]<2>  11[7]<2> 

We know that key 7 and 11 are children of key 8 because: 
1. Their indices [6] & [7] >> [2] * 2 + 1 (max child index of key 3), thus they cannot be children of 3
2. And 6 = 3 * 2; 7 = 3 * 2 + 1 && the heights are correctly represented => they are children of key 8
