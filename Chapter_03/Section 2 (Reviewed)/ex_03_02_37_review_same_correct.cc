#include <iostream>
#include <vector>
#include <iomanip>
#include <numeric>
#include <string>
#include "Queue.h"
#include "Random.h"

using namespace std;

template < typename Key, typename Value > 
class BinarySearchTree {
private:

    // NODE OPERATION
    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        int N = 0;
        int index = -1;
    };

    // safe-memory node operations
    Node* newNode( Key key, Value val, int N ) {
        allNodes.push_back( make_unique< Node >() ); 
       
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;
    Node* root = NULL;

    // TREE OPERATION
    int size( Node* x ) {   
        return ( x == NULL ) ? 0 : x->N;    // calculate how many child nodes are underneath current node 
    }  

    Node* put( Node* x, Key key, Value val ) {
        if      ( x == NULL )    return newNode( key, val, 0 );
        if      ( x->key < key ) x->right = put( x->right, key, val );
        else if ( x->key > key ) x->left = put( x->left, key, val );
        else                     x->val = val;
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* erase( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key < key ) x->right = erase( x->right, key );
        else if ( x->key > key ) x->left = erase( x->left, key );
        else {
            if ( x->right == NULL ) return x->left;
            if ( x->left == NULL ) return x->right;
            Node* temp = x;
            x = min( temp->right );
            x->right = eraseMin( temp->right );
            x->left = temp->left;
        }
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* min( Node* x ) {   
        return ( x->left == NULL ) ? x : min( x->left );   
    }

    Node* max( Node* x ) {   
        return ( x->right == NULL ) ? x : max( x->right );   
    }

    Node* eraseMin( Node* x ) {
        if ( x->left == NULL ) return x->right;
        x->left = eraseMin( x->left );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* eraseMax( Node* x ) {
        if ( x->right == NULL ) return x->left;
        x->right = eraseMax( x->right );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    // largest smaller
    Node* floor( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        // exhaust left until nhỏ hơn 
        else if ( x->key > key ) return floor( x->left, key );
        // thì tìm max của nhánh nhỏ hơn đó -> floor
        Node* t = floor( x->right, key );
        return ( t != NULL ) ? t : x;
    }

    // smallest larger
    Node* ceiling( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x-> key < key ) return ceiling( x->right, key );
        Node* t = ceiling( x->left, key );
        return ( t != NULL ) ? t : x;
    }

    Value get( Node* x, Key key ) {
        if      ( x == NULL )    return NULL;
        if      ( x->key > key ) return get( x->left, key );
        else if ( x->key < key ) return get( x->right, key );
        else                     return x->val;
    }

    Node* select( Node* x, int k ) {
        if ( x == NULL ) return NULL;
        // t is the size of left side of current x
        int t = size( x->left );
        // if the left size > t, then probably the index within left branch
        if      ( t > k ) select( x->left, k );
        // else if right size > t, then probably the index within righ branch
        // nếu như không search left branch nữa thì search theo index của right branch. Và bởi vì tại mỗi branch index được đánh lại, nên index = k - t - 1
        else if ( t < k ) select( x->right, k-t-1 );
        else              return x;
    }

    int rank( Node* x, Key key ) {
        if      ( x == NULL )    return 0;
        if      ( x->key > key ) rank( x->left, key );
        else if ( x->key < key ) 1 + size( x->left ) + rank( x->right, key );
        else                     return size( x->left );
    }

    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

public:

    // we go in breadth-first search mechanism, where we always check if the 2 neighbors of the current node is empty or not first, before going to hte next level
    void printLevel( Node* x ) {
        Queue< Node* > queue; 
        queue.enqueue( x );
        // we keep popping the 'parents' of the same level, as a means of exploring breadth first
        while ( !queue.isEmpty() ) {
            Node* current = queue.dequeue();
            cout << current->key << " ";

            if ( current->left != NULL ) 
                queue.enqueue( current->left );

            if ( current->right != NULL )
                queue.enqueue( current->right );
        }
    }

    int size() {   
        return size( root );   
    }

    bool isEmpty() {   
        return size() == 0;   
    }

    void put( Key key, Value value ) {   
        root = put( root, key, value );   
    }

    void erase( Key key ) {   
        root = erase( root, key );   
    }

    Key min() {   
        return min( root )->key;
    }

    Key max() {   
        return max( root )->key;
    }

    void eraseMin() {   
        root = eraseMin( root );   
    }

    void eraseMax() {   
        root =eraseMax( root );   
    }

    Key floor( Key key ) {   
        if ( key < min() ) {
            throw runtime_error( "floor(): No floor for inputted key" );
        }
        return floor( root, key )->key;
    }

    Key ceiling( Key key ) {
        if ( key > max() ) {
            throw runtime_error( "celing(): No ceiling for inputted key" );
        }
        return ceiling( root, key )->key;
    }

    Value get( Key key ) {   
        if ( rank( key ) == 0 && root->key != key ) {
            throw out_of_range( "get(): Key inputted not exist" );
        }
        return get( root, key );
    }

    Key select( int k ) {   
        if ( k > size() ) throw out_of_range( "select(): index out of range" );
        return select( root, k )->key;   
    }

    int rank( Key key ) {   
        return rank( root, key );   
    }

    Queue< Key > keys() {   
        return keys( min(), max() );   
    }

    Queue< Key > keys( int lo, int hi ) {
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    Node* getRoot() { return root; }

};

int main( int argc, char** argv ) {
    
    int n = stoi( argv[ 1 ] );
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, 12 );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl << endl;

    bst.printLevel( bst.getRoot() );

    return 0;

}
