#include <iostream>
#include <iomanip>
#include <numeric>
#include <vector>
#include "Queue.h"
#include "Random.h"
#include "BinarySearchTree.h"

using namespace std;

// test for BinarySearchTree < int, int >
bool isOrdered( BinarySearchTree< int, int >::Node* x, int min, int max ) {
    // always check with the global min max?
    if ( x->key < min || x->key > max )  {
        cout << "out of min(), max() triggered!" << endl;
        return false;
    }

    // Bởi vì chúng ta đang làm negative check (tức là dừng và gán false tại trường hợp sai (check trường hợp sai)) thay vì tìm và gán true cho trường hợp đúng, bởi vậy nên giả thuyết ban đầu chúng ta đưa ra phải là Cây đang true (hay nói cách khác right and left wing cả 2 đều phải đc gán true)
    bool rightWing = true, leftWing = true;

    // traversing in: left, visit, right ordr
    if ( x->left != NULL ) {
        // check if the BST structure is preserved
        if ( x->left->key > x->key ) {
            cout << "left branch > key triggered: " << x->left->key << " " << x << endl;
            return false;
        }
        leftWing = isOrdered( x->left, min, max );
    }

    if ( x->right != NULL ) {
        if ( x->right->key < x->key ) {
            cout << "right branch < key triggered" << x->key << " " << x->right->key << endl;
            return false;
        }
        rightWing = isOrdered( x->right, min, max );
    }

    return ( leftWing == false || rightWing == false ) ? false : true;
}

int main( int argc, char** argv ) {
    
    int n = stoi( argv[ 1 ] );
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, 12 );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    bst.printTree();

    cout << ( ( isOrdered( bst.rootNode(), bst.min(), bst.max() ) == 0 ) ? "false" : "true" ) << endl;

    return 0;

}
