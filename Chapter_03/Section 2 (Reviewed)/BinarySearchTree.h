#ifndef BINARYSEARCHTREE_H
#define BINARYSEARCHTREE_H

#include <iostream>
#include <map>
#include <utility>
#include <climits>
#include <vector>
#include <iomanip>
#include <numeric>
#include <string>
#include "Queue.h"
#include "Random.h"

using namespace std;

template < typename Key, typename Value > 
class BinarySearchTree {
public:
    
    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        int height = 0;
        int N = 1;
        int index = -1;
        int indexInTree = 1;
    };
    
private:

    Node* newNode( Key key, Value val, int N, int height, int indexInTree ) {
        allNodes.push_back( make_unique< Node >() ); 
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        allNodes.back()->indexInTree = indexInTree;
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->height = height;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;
    Node* root = NULL;
    int maxHeight = 0;

    int size( Node* x ) {   
        return ( x == NULL ) ? 0 : x->N;    
    }  

    Node* put( Node* x, Key key, Value val, int height, int indexInTree ) {
        if      ( x == NULL ) {
            if ( maxHeight < height ) {
                maxHeight = height;
            }
            return newNode( key, val, 1, height, indexInTree );
        }
        if      ( x->key < key ) x->right = put( x->right, key, val, height+1, indexInTree * 2 + 1 );
        else if ( x->key > key ) x->left = put( x->left, key, val, height+1, indexInTree * 2 );
        else                     x->val = val;
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }
    
    Node* erase( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key < key ) {
            x->right = erase( x->right, key );
        }
        else if ( x->key > key ) {
            x->left = erase( x->left, key );
        }
        else {
            if ( x->right == NULL ) {
                Node* leftChild = x->left;
                
                if ( x->left != NULL ) {
                    updateHeightFrom( x->left );
                    updateIndexFrom( x->left );
                }
                freeNode( x );
                return leftChild;
            }
            if ( x->left == NULL ) {
                Node* rightChild = x->right;
                
                if ( x->right != NULL ) {
                    updateHeightFrom( x->right );
                    updateIndexFrom( x->right );
                }
                freeNode( x );
                return rightChild;
            }

            Node* temp = x;
            x = min( temp->right );
            x->height = temp->height;

            if ( x->right != NULL ) {
                updateHeightFrom( x->right );
                x->right->height += 1;
                if ( x->right->left != NULL ) {
                    updateIndexFrom( x->right->left );
                }
                if ( x->right->right != NULL ) {
                    updateIndexFrom( x->right->right );
                }
            }
            x->right = eraseMin( temp->right );
            x->left = temp->left;
            freeNode( temp );
        }
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }
    
    void updateHeightFrom( Node* x ) {
        if ( x == NULL ) return;
        if ( x->left != NULL ) {
            updateHeightFrom( x->left );
        }
        if ( x->right != NULL ) {
            updateHeightFrom( x->right );
        }
        x->height = x->height - 1;
    }

    void updateIndexFrom( Node* x ) {
        if ( x == NULL ) return;
        if ( x->left != NULL ) {
            updateIndexFrom( x->left );
        }
        if ( x->right != NULL ) {
            updateIndexFrom( x->right );
        }
        x->indexInTree /= 2;
    }

    Node* min( Node* x ) {   
        return ( x->left == NULL ) ? x : min( x->left );   
    }

    Node* max( Node* x ) {
        return ( x->right == NULL ) ? x : max( x->right );
    }

    Node* eraseMin( Node* x ) {
        if ( x->left == NULL ) {
            if ( x->right != NULL ) {
                x->right->indexInTree /= 2;
                x->right->height -= 1;
            }
            return x->right;
        }
        x->left = eraseMin( x->left );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* eraseMax( Node* x ) {
        if ( x->right == NULL ) {
            if ( x->left != NULL ) {
                x->left->indexInTree /= 2;
                x->left->height -= 1;
            }
            return x->left;
        }
        x->right = eraseMax( x->right );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    
    Node* floor( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x->key > key ) return floor( x->left, key );
        Node* t = floor( x->right, key );
        return ( t != NULL ) ? t : x;
    }

    
    Node* ceiling( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x-> key < key ) return ceiling( x->right, key );
        Node* t = ceiling( x->left, key );
        return ( t != NULL ) ? t : x;
    }

    Value get( Node* x, Key key ) {
        if      ( x == NULL )    return NULL;
        if      ( x->key > key ) return get( x->left, key );
        else if ( x->key < key ) return get( x->right, key );
        else                     return x->val;
    }

    Node* select( Node* x, int k ) {
        if ( x == NULL )  return NULL;
        int t = size( x->left );
        if      ( t > k ) return select( x->left, k );
        else if ( t < k ) return select( x->right, k-t-1 );
        else              return x;
    }

    int rank( Node* x, Key key ) {
        if      ( x == NULL )    return 0;
        if      ( x->key > key ) return rank( x->left, key );
        else if ( x->key < key ) return 1 + size( x->left ) + rank( x->right, key );
        else                     return size( x->left );
    }

    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

    class NodePrint {
    public:
        Key key;
        int height;
        int indexInTree;
        NodePrint( Key key, int height, int indexInTree ): key( key ), height( height ), indexInTree( indexInTree ) {}
        bool operator > ( NodePrint& rhs ) {
            return key > rhs.key;
        }
        bool operator < ( NodePrint& rhs ) {
            return key < rhs.key;
        }
        bool operator == ( NodePrint& rhs ) {
            return key == rhs.key;
        }
        bool operator >= ( NodePrint& rhs ) {
            return operator == ( rhs ) || operator > ( rhs );
        }
        bool operator <= ( NodePrint& rhs ) {
            return operator == ( rhs ) || operator < ( rhs );
        }
        bool operator != ( NodePrint& rhs ) {
            return ! operator == ( rhs );
        }
    };

public:

    void printTree() {
        int currentHeight = 0;
        vector< NodePrint > treeMap;

        while ( currentHeight <= maxHeight ) {
            for ( auto& s : allNodes ) {
                if ( s.get()->height == currentHeight ) {
                    NodePrint nodeToPrint( s.get()->key, s.get()->height, s.get()->indexInTree );
                    treeMap.push_back( nodeToPrint );
                }
            }
            currentHeight++;
        }

        sort( treeMap.begin(), treeMap.end() );

        currentHeight = 0;

        while ( currentHeight <= maxHeight ) {
            for ( auto& s : treeMap ) {
                if ( s.height == currentHeight ) {
                    cout << s.key << "[" << s.indexInTree << "]" << "<" << s.height << ">" << " ";
                } 
            }
            cout << endl;
            currentHeight++;
        }
    }

    int size() {   
        return size( root );
    }

    bool isEmpty() {   
        return size() == 0;   
    }

    void put( Key key, Value value ) {   
        root = put( root, key, value, 0, 1 );   
    }

    void erase( Key key ) {   
        root = erase( root, key );   
    }

    Key min() {   
        return min( root )->key;
    }

    Key max() {   
        return max( root )->key;
    }

    void eraseMin() {   
        root = eraseMin( root );   
    }

    void eraseMax() {   
        root =eraseMax( root );   
    }

    Key floor( Key key ) {   
        if ( key < min() ) {
            throw runtime_error( "floor(): No floor for inputted key" );
        }
        return floor( root, key )->key;
    }

    Key ceiling( Key key ) {
        if ( key > max() ) {
            throw runtime_error( "celing(): No ceiling for inputted key" );
        }
        return ceiling( root, key )->key;
    }

    Value get( Key key ) {   
        return get( root, key );
    }

    Key select( int k ) {   
        return select( root, k )->key;   
    }

    int rank( Key key ) {   
        return rank( root, key );   
    }

    Queue< Key > keys() {   
        return keys( min(), max() );   
    }

    Queue< Key > keys( int lo, int hi ) {
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    Key getRoot() {
        return root->key;
    }

    Node* rootNode() {
        return root;
    }

};

template < typename Key > 
class BinarySearchTreeKeyOnly {
private:

    
    class Node {
    public:
        Key key;
        Node* left = NULL;
        Node* right = NULL;
        int height = 0;
        int N = 1;
        int indexInTree = 1;
        int index = -1;
    };

    
    Node* newNode( Key key, int N, int height, int indexInTree ) {
        allNodes.push_back( make_unique< Node >() ); 
        allNodes.back()->indexInTree = indexInTree;
        allNodes.back()->key = key;
        allNodes.back()->N = N;
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->height = height;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;
    Node* root = NULL;
    int maxHeight = 0;

    
    int size( Node* x ) {   
        return ( x == NULL ) ? 0 : x->N;    
    }  

    Node* put( Node* x, Key key, int height, int indexInTree ) {
        if      ( x == NULL ) {
            if ( maxHeight < height ) {
                maxHeight = height;
            }
            return newNode( key, 1, height, indexInTree );
        }
        if      ( x->key < key ) x->right = put( x->right, key, height+1, indexInTree * 2 + 1 );
        else if ( x->key > key ) x->left = put( x->left, key, height+1, indexInTree * 2 );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    
    Node* erase( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key < key ) {
            x->right = erase( x->right, key );
        }
        else if ( x->key > key ) {
            x->left = erase( x->left, key );
        }
        else {
            if ( x->right == NULL ) {
                Node* leftChild = x->left;
                
                if ( x->left != NULL ) {
                    updateHeightFrom( x->left );
                    updateIndexFrom( x->left );
                }
                freeNode( x );
                return leftChild;
            }
            if ( x->left == NULL ) {
                Node* rightChild = x->right;
                
                if ( x->right != NULL ) {
                    updateHeightFrom( x->right );
                    updateIndexFrom( x->right );
                }
                freeNode( x );
                return rightChild;
            }
            Node* temp = x;
            x = min( temp->right );
            x->height = temp->height;
            if ( x->right != NULL ) {
                updateHeightFrom( x->right );
                x->right->height += 1;
                if ( x->right->left != NULL ) {
                    updateIndexFrom( x->right->left );
                }
                if ( x->right->right != NULL ) {
                    updateIndexFrom( x->right->right );
                }
            }
            x->right = eraseMin( temp->right );
            x->left = temp->left;
            freeNode( temp );
        }
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    
    void updateHeightFrom( Node* x ) {
        if ( x == NULL ) return;
        if ( x->left != NULL ) {
            updateHeightFrom( x->left );
        }
        if ( x->right != NULL ) {
            updateHeightFrom( x->right );
        }
        x->height = x->height - 1;
    }

    Node* min( Node* x ) {   
        return ( x->left == NULL ) ? x : min( x->left );   
    }

    Node* max( Node* x ) {
        return ( x->right == NULL ) ? x : max( x->right );
    }

    Node* eraseMin( Node* x ) {
        if ( x->left == NULL ) {
            if ( x->right != NULL ) {
                x->right->height -= 1;
                x->right->indexInTree /= 2;
            }
            return x->right;
        }
        x->left = eraseMin( x->left );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* eraseMax( Node* x ) {
        
        if ( x->right == NULL ) {
            
            if ( x->left != NULL ) {
                x->left->height -= 1;
                x->left->indexInTree /= 2;
            }
            return x->left;
        }
        x->right = eraseMax( x->right );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    
    Node* floor( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        
        else if ( x->key > key ) return floor( x->left, key );
        
        Node* t = floor( x->right, key );
        return ( t != NULL ) ? t : x;
    }

    
    Node* ceiling( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x-> key < key ) return ceiling( x->right, key );
        Node* t = ceiling( x->left, key );
        return ( t != NULL ) ? t : x;
    }

    Node* select( Node* x, int k ) {
        if ( x == NULL ) return NULL;
        
        int t = size( x->left );
        
        if      ( t > k ) select( x->left, k );
        
        
        else if ( t < k ) select( x->right, k-t-1 );
        else              return x;
    }

    int rank( Node* x, Key key ) {
        if      ( x == NULL )    return 0;
        if      ( x->key > key ) rank( x->left, key );
        else if ( x->key < key ) 1 + size( x->left ) + rank( x->right, key );
        else                     return size( x->left );
    }

    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

    class NodePrint {
    public:
        Key key;
        int height;
        int indexInTree;
        NodePrint( Key key, int height, int indexInTree ): key( key ), height( height ), indexInTree( indexInTree ) {}
        bool operator > ( NodePrint& rhs ) {
            return key > rhs.key;
        }
        bool operator < ( NodePrint& rhs ) {
            return key < rhs.key;
        }
        bool operator == ( NodePrint& rhs ) {
            return key == rhs.key;
        }
        bool operator >= ( NodePrint& rhs ) {
            return operator == ( rhs ) || operator > ( rhs );
        }
        bool operator <= ( NodePrint& rhs ) {
            return operator == ( rhs ) || operator < ( rhs );
        }
        bool operator != ( NodePrint& rhs ) {
            return ! operator == ( rhs );
        }
    };

public:

    void printTree() {
        int currentHeight = 0;
        vector< NodePrint > treeMap;

        while ( currentHeight <= maxHeight ) {
            for ( auto& s : allNodes ) {
                if ( s.get()->height == currentHeight ) {
                    NodePrint nodeToPrint( s.get()->key, s.get()->height, s.get()->indexInTree );
                    treeMap.push_back( nodeToPrint );
                }
            }
            currentHeight++;
        }

        sort( treeMap.begin(), treeMap.end() );

        currentHeight = 0;

        while ( currentHeight <= maxHeight ) {
            for ( auto& s : treeMap ) {
                if ( s.height == currentHeight ) {
                    cout << s.key << "[" << s.indexInTree << "]" << "<" << s.height << ">" << " ";
                } 
            }
            cout << endl;
            currentHeight++;
        }
    }

    int size() {   
        return size( root );
    }

    bool isEmpty() {   
        return size() == 0;   
    }

    void put( Key key ) {   
        root = put( root, key, 0, 1 );   
    }

    void erase( Key key ) {   
        root = erase( root, key );   
    }

    Key min() {   
        return min( root )->key;
    }

    Key max() {   
        return max( root )->key;
    }

    void eraseMin() {   
        root = eraseMin( root );   
    }

    void eraseMax() {   
        root =eraseMax( root );   
    }

    Key floor( Key key ) {   
        if ( key < min() ) {
            throw runtime_error( "floor(): No floor for inputted key" );
        }
        return floor( root, key )->key;
    }

    Key ceiling( Key key ) {
        if ( key > max() ) {
            throw runtime_error( "celing(): No ceiling for inputted key" );
        }
        return ceiling( root, key )->key;
    }

    Key select( int k ) {   
        if ( k > size() ) throw out_of_range( "select(): index out of range" );
        return select( root, k )->key;   
    }

    int rank( Key key ) {   
        return rank( root, key );   
    }

    Queue< Key > keys() {   
        return keys( min(), max() );   
    }

    Queue< Key > keys( int lo, int hi ) {
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    Key getRoot() {
        return root->key;
    }

};

bool hasNoDuplicates( BinarySearchTree< int, int >::Node* x ) {
    bool leftWing = true, rightWing = true;
    
    if ( x->left != NULL ) {
        if ( x->key  == x->left->key ) {
            return false;
        }
        leftWing = hasNoDuplicates( x->left );
    }

    if ( x->right != NULL ) {
        if ( x->key == x->right->key ) {
            return false;
        }
        rightWing = hasNoDuplicates( x->right );
    }

    return ( ! leftWing || ! rightWing ) ? false : true;
}

bool isOrdered( BinarySearchTree< int, int >::Node* x, int min, int max ) {
    if ( x->key < min || x->key > max )  {
        cout << "out of min(), max() triggered!" << endl;
        return false;
    }

    bool rightWing = true, leftWing = true;

    if ( x->left != NULL ) {
        if ( x->left->key > x->key ) {
            cout << "left branch > key triggered: " << x->left->key << " " << x << endl;
            return false;
        }
        leftWing = isOrdered( x->left, min, max );
    }

    if ( x->right != NULL ) {
        if ( x->right->key < x->key ) {
            cout << "right branch < key triggered" << x->key << " " << x->right->key << endl;
            return false;
        }
        rightWing = isOrdered( x->right, min, max );
    }

    return ( leftWing == false || rightWing == false ) ? false : true;
}

bool isBinaryTree( BinarySearchTree< int, int >::Node* x ) {
    if ( x->left == NULL && x->right != NULL ) { 
        if ( x->N != x->right->N + 1 ) {
            return false;
        }
    }
    else if ( x->left != NULL && x->right == NULL ) {
        if ( x->N != x->left->N + 1 ) {
            return false;
        }
    }
    else if ( x->left != NULL && x->right != NULL ) {
        if ( x->N != x->left->N + x->right->N + 1 ) {
            return false;
        }
    }
    else {
        if ( x->N != 1 ) {
            return false;
        }
    }

    bool leftWing = true, rightWing = true;

    if ( x->left != NULL ) {
        leftWing = isBinaryTree( x->left );
    }

    if ( x->right != NULL ) {
        rightWing = isBinaryTree( x->right );
    }

    return ( !leftWing || !rightWing ) ? false : true;
}

#endif

