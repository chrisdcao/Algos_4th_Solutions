#include <iostream>
#include <iomanip>
#include <numeric>
#include <vector>
#include "Queue.h"
#include "Random.h"
#include "BinarySearchTree.h"

using namespace std;

bool hasNoDuplicates( BinarySearchTree< int, int >::Node* x ) {
    bool leftWing = true, rightWing = true;
    
    if ( x->left != NULL ) {
        if ( x->key  == x->left->key ) {
            return false;
        }
        leftWing = hasNoDuplicates( x->left );
    }

    if ( x->right != NULL ) {
        if ( x->key == x->right->key ) {
            return false;
        }
        rightWing = hasNoDuplicates( x->right );
    }

    return ( ! leftWing || ! rightWing ) ? false : true;
}

int main( int argc, char** argv ) {
    
    int n = stoi( argv[ 1 ] );
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, 12 );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    bst.printTree();

    cout << ( ( hasNoDuplicates( bst.rootNode() ) == 0 ) ? "false" : "true" ) << endl;

    return 0;

}
