#include <iostream>
#include <vector>
#include <memory>
#include <iomanip>
#include <string>
#include "Random.h"
#include "Queue.h"
#include "Stack.h"

using namespace std;

template < typename Key, typename Value > 
class BinarySearchTree {
private:

    class Node {
    public:
        Node* left = NULL;
        Node* right = NULL;
        Key key;
        Value val;
        int index = -1;
        int N = 1;
    };

    Node* newNode( Key key, Value val, int N ) {
        allNodes.push_back( make_unique< Node >() );
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->N = N;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;
    Node* root = NULL;

    int size( Node* x ) {
        return ( x == NULL ) ? 0 : x->N;
    }

    // Does change the size of the tree
    Node* put( Node* x, Key key, Value val ) {
        if      ( x == NULL )    return newNode( key, val, 1 );
        if      ( x->key > key ) x->left = put( x->left, key, val );
        else if ( x->key < key ) x->right = put( x->right, key, val );
        else  x->val = val;
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* erase( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key > key ) x->left = erase( x->left, key );
        else if ( x->key < key ) x->right = erase( x->right, key );
        else {
            if ( x->right == NULL ) {
                return x->left;
            }
            if ( x->left == NULL ) {
                return x->right;
            }
            Node* temp = x;
            x = min( temp->right );
            x->right = eraseMin( temp->right );
            x->left = temp->left;
        }
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* eraseMin( Node* x ) {
        if ( x->left == NULL ) return x->right;
        x->left = eraseMin( x->left );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* eraseMax( Node* x ) {
        if ( x->right == NULL ) return x->left;
        x->right = eraseMax( x->right );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    // Doesn't change the size of the tree
    Node* min( Node* x ) {
        return ( x->left == NULL ) ? x : min( x->left );
    }

    Node* max( Node* x ) {
        return ( x->right == NULL ) ? x : max( x->right );
    }

    Value get( Node* x, Key key ) {
        if      ( x == NULL )    return NULL;
        if      ( x->key > key ) return get( x->left, key );
        else if ( x->key < key ) return get( x->right, key );
        else return x->val;
    }

    Node* select( Node* x, int k ) {
        if ( x == NULL )  return NULL;
        int t = size( x->left );
        if      ( t > k ) return select( x->left, k );
        else if ( t < k ) return select( x->right, k-t-1 );
        else return x;
    }

    // largest smaller
    Node* floor( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key > key ) {
            floor( x->left, key );
        } 
        else if ( x->key == key ) {
            return x;
        }
        Node* t = floor( x->right, key );
        return ( t == NULL ) ? x : t;
    }

    Node* ceiling( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key < key ) {
            ceiling( x->right, key );
        }
        else if ( x->key == key ) {
            return x;
        }
        Node* t = ceiling( x->left, key );
        return ( t == NULL ) ? x : t;
    }

    int rank( Node* x, Key key ) {
        if ( x == NULL )    return 0;
        if ( x->key > key ) return rank( x->left, key );
        if ( x->key < key ) return 1 + size( x->left ) + rank( x->right, key );
        else                return size( x->left );
    }

public:

    int size() {
        return size( root );
    }

    Key floor( Key key ) {
        return floor( root, key )->key;
    }

    Key ceiling( Key key ) {
        return ceiling( root, key )->key;
    }

    void put( Key key, Value val ) {
        root = put( root, key, val );
    }

    void erase( Key key ) {
        root = erase( root, key );
    }

    void eraseMin() {
        root = eraseMin( root );
    }

    void eraseMax() {
        root = eraseMax( root );
    }

    Key min() {
        return min( root )->key;
    }

    Key max() {
        return max( root )->key;
    }

    Value get( Key key ) {
        return get( root, key );
    }

    Key select( int k ) {
        return select( root, k )->key;
    }

    Queue< Key > keys() {
        Node* traverse = NULL;
        Queue< Key > queue;
        Stack< Node* > stack;

        if ( root->left != NULL ) {
            traverse = root->left;
            while ( true ) {
                while ( traverse != NULL ) {
                    while ( traverse->left != NULL ) {
                        stack.push( traverse );
                        traverse = traverse->left;
                    }
                    queue.enqueue( traverse->key );
                    traverse = traverse->right;
                }
                while ( stack.peek() != NULL && !stack.isEmpty() ) {
                    if ( stack.peek()->right == NULL )
                        queue.enqueue( stack.pop()->key );
                    else {
                        queue.enqueue( stack.peek()->key );
                        break;
                    }
                }
                // if all of those nodes NOT having any right child, we stop
                if ( stack.isEmpty() ) break;
                traverse = stack.pop()->right;
            }
        }
        // after exploring all left branch of root, we push back root
        queue.enqueue( root->key );

        // exhaust root right branch, if it has one
        if ( root->right != NULL ) {
            traverse = root->right;
            while ( true ) {
                while ( traverse != NULL ) {
                    while ( traverse->left != NULL ) {
                        stack.push( traverse );
                        traverse = traverse->left;
                    }
                    queue.enqueue( traverse->key );
                    traverse = traverse->right;
                }
                while ( stack.peek() != NULL && !stack.isEmpty() ) {
                    if ( stack.peek()->right == NULL )
                        queue.enqueue( stack.pop()->key );
                    else {
                        queue.enqueue( stack.peek()->key );
                        break;
                    }
                }
                if ( stack.isEmpty() ) break;
                traverse = stack.pop()->right;
            }
        }
        return queue;
    }

};

int main( int argc, char** argv ) {
    
    int n = stoi( argv[ 1 ] );
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, n );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl;

    return 0;

}
