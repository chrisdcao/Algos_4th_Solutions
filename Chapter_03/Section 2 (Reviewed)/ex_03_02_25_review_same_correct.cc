#include "BinarySearchTree.h"
#include <algorithm>
#include <numeric>
#include <iomanip>
#include <vector>
#include "Stack.h"

using namespace std;

void sort( vector< int >& values, BinarySearchTreeKeyOnly< int >& bst, int lo, int hi ) {
    if ( lo > hi ) return;
    int mid = lo + ( hi - lo ) / 2;
    bst.put( values[ mid ] );
    cout << values[ mid ] << " ";

    sort( values, bst, lo, mid - 1 );
    sort( values, bst, mid + 1, hi );
}

void sortToBinarySearch( vector< int >& values, BinarySearchTreeKeyOnly< int >& bst ) {
    sort( values, bst, 0, values.size() - 1 );
}

int main( int argc, char** argv ) {

    vector< int > input = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
    sort( input.begin(), input.end() );

    BinarySearchTreeKeyOnly< int > bst;

    sortToBinarySearch( input, bst );
    cout << endl;
    bst.printTree();

    return 0;

}
