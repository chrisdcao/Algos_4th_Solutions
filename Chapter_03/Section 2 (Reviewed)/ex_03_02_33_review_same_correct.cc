#include <iostream>
#include <numeric>
#include <string>
#include <iomanip>
#include "Random.h"
#include "Queue.h"
#include "BinarySearchTreeStatic.h"

using namespace std;

int main( int argc, char** argv ) {
    
    int n = stoi( argv[ 1 ] );
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, 12 );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    bst.printTree();

    // build the method as member function for simpler construction
    cout << ( ( bst.checkSelectRank( bst.rootNode(), 0 ) == 0 ) ? "false" : "true" ) << endl;

    return 0;

}
