#include <iostream>
#include <numeric>
#include <iomanip>
#include "Random.h"
#include "BinarySearchTree.h"
#include "Queue.h"

using namespace std;

bool isBST( BinarySearchTree< int, int >::Node* x, int min, int max ) {
    if ( !isBinaryTree( x ) )        return false;
    // this min(), max() cannot be passed like this, we have to take them as the extra params of the function
    if ( !isOrdered( x, min, max ) ) return false;
    if ( !hasNoDuplicates( x ) )     return false;

    return true;
}

int main( int argc, char** argv ) {
    
    int n = stoi( argv[ 1 ] );
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, 12 );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    cout << ( ( isBST( bst.rootNode(), bst.min(), bst.max() ) == 0 ) ? "false" : "true" ) << endl;

    return 0;

}
