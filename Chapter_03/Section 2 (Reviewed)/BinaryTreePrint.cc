#include <iostream>
#include <map>
#include <utility>
#include <climits>
#include <vector>
#include <iomanip>
#include <numeric>
#include <string>
#include "Queue.h"
#include "Random.h"

using namespace std;

template < typename Key, typename Value > 
class BinarySearchTree {
private:

    // NODE OPERATION
    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        int height = 0;
        // assume that the tree index starting counting from 1
        int indexInTree = 1;
        int N = 1;
        int index = -1;
    };

    // safe-memory node operations
    Node* newNode( Key key, Value val, int N, int height, int indexInTree ) {
        allNodes.push_back( make_unique< Node >() ); 
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->indexInTree = indexInTree;
        allNodes.back()->height = height;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;
    Node* root = NULL;
    int maxHeight = 0;

    // TREE OPERATION
    int size( Node* x ) {   
        return ( x == NULL ) ? 0 : x->N;    // calculate how many child nodes are underneath current node 
    }  

    Node* put( Node* x, Key key, Value val, int height, int indexInTree ) {
        if      ( x == NULL ) {
            if ( maxHeight < height ) {
                maxHeight = height;
            }
            return newNode( key, val, 1, height, indexInTree );
        }
        if      ( x->key < key ) x->right = put( x->right, key, val, height+1, indexInTree * 2 + 1 );
        else if ( x->key > key ) x->left = put( x->left, key, val, height+1, indexInTree * 2 );
        else                     x->val = val;
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    // TODO: ADD THIS PATCH WITH HEIGHT UPDATE INTO OTHER VERSIONS
    Node* erase( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key < key ) {
            x->right = erase( x->right, key );
        }
        else if ( x->key > key ) {
            x->left = erase( x->left, key );
        }
        else {
            if ( x->right == NULL ) {
                Node* leftChild = x->left;
                // we have to update all the left child of 'x' when 'x' is removed from its original position (all children of 'x' will move up 1 level)
                if ( x->left != NULL ) {
                    updateHeightFrom( x->left );
                    updateIndexFrom( x->left );
                }
                freeNode( x );
                return leftChild;
            }
            if ( x->left == NULL ) {
                Node* rightChild = x->right;
                // we have to update all the right child of 'x' when 'x' is removed from its original position (all children of 'x' will move up 1 level)
                if ( x->right != NULL ) {
                    updateHeightFrom( x->right );
                    updateIndexFrom( x->right );
                }
                freeNode( x );
                return rightChild;
            }
            Node* temp = x;
            x = min( temp->right );
            x->indexInTree = temp->indexInTree;
            x->height = temp->height;
            /*
             * !! RIGHT CHILD HEIGHT UPDATE 
             *
             * If we take a node from BELOW x to replace current x (which will always be the case), that makes that all the child of that node up 1 level after the parent is moved to x position for replacement (because the nearest child will be the new parent and all underneath follows)
             *
             * -> We update the weight of the right child-branch of that node (-1 at each height to indicate moving up 1 level). There's no way we moving 2 level because we delete only 1 Node at a time
             *
             *
             * !! LEFT CHILD HEIGHT UPDATE 
             *
             * We DON'T have to care about the left child-branch height because min() function has already exhausted left-branch of temp->right, meaning that there CERTAINLY WILL BE NO CHILDREN on the left-branch of Node 'x' after the x = min(temp->right) assignment)
             *
             */
            if ( x->right != NULL ) {
                // This height update includes (x->right)'s height as well
                updateHeightFrom( x->right );
                if ( x->right->left != NULL ) {
                    updateIndexFrom( x->right->left );
                }
                if ( x->right->right != NULL ) {
                    updateIndexFrom( x->right->right );
                }
                // In the eraseMin() below, we will decrease (x->right)'s height once again (tích hợp sẵn trong hàm eraseMin()), making it double subtraction. Thus, we have to +1 here to compensate upfront
                x->right->height += 1;
            }
            // eraseMin will minus x->right once again
            x->right = eraseMin( temp->right );
            x->left = temp->left;
            freeNode( temp );
        }
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    void updateIndexFrom( Node* x ) {
        if ( x == NULL ) return;
        if ( x->left != NULL ) {
            updateIndexFrom( x->left );
        }
        if ( x->right != NULL ) {
            updateIndexFrom( x->right );
        }
        // moving up one level
        x->indexInTree /= 2;
    }

    // this function decrement the height of all nodes from X downward (all the children of X) by 1
    void updateHeightFrom( Node* x ) {
        if ( x == NULL ) return;
        if ( x->left != NULL ) {
            updateHeightFrom( x->left );
        }
        if ( x->right != NULL ) {
            updateHeightFrom( x->right );
        }
        x->height -= 1;
    }

    Node* min( Node* x ) {   
        return ( x->left == NULL ) ? x : min( x->left );   
    }

    Node* max( Node* x ) {
        return ( x->right == NULL ) ? x : max( x->right );
    }

    Node* eraseMin( Node* x ) {
        // In the end, replace the current 'x' with its right child if its left child is Empty/NULL 
        if ( x->left == NULL ) {
            // if there's actually a right child, then this right child, replacing its parent, will up 1 level
            if ( x->right != NULL ) {
                x->right->height -=1;
                x->right->indexInTree /= 2;
            }
            return x->right;
        }
        x->left = eraseMin( x->left );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* eraseMax( Node* x ) {
        // In the end, replace the current 'x' with its left child if its right child is Empty/NULL 
        if ( x->right == NULL ) {
            // if there's actually a left child, then this left child, replacing its parent, will up 1 level
            if ( x->left != NULL ) {
                x->left->height -= 1;
                x->left->indexInTree /=2;
            }
            return x->left;
        }
        x->right = eraseMax( x->right );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    // largest smaller
    Node* floor( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        // exhaust left until nhỏ hơn 
        else if ( x->key > key ) return floor( x->left, key );
        // thì tìm max của nhánh nhỏ hơn đó -> floor
        Node* t = floor( x->right, key );
        return ( t != NULL ) ? t : x;
    }

    // smallest larger
    Node* ceiling( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x-> key < key ) return ceiling( x->right, key );
        Node* t = ceiling( x->left, key );
        return ( t != NULL ) ? t : x;
    }

    Value get( Node* x, Key key ) {
        if      ( x == NULL )    return NULL;
        if      ( x->key > key ) return get( x->left, key );
        else if ( x->key < key ) return get( x->right, key );
        else                     return x->val;
    }

    Node* select( Node* x, int k ) {
        if ( x == NULL ) return NULL;
        // t is the size of left side of current x
        int t = size( x->left );
        // if the left size > t, then probably the index within left branch
        if      ( t > k ) select( x->left, k );
        // else if right size > t, then probably the index within righ branch
        // nếu như không search left branch nữa thì search theo index của right branch. Và bởi vì tại mỗi branch index được đánh lại, nên index = k - t - 1
        else if ( t < k ) select( x->right, k-t-1 );
        else              return x;
    }

    int rank( Node* x, Key key ) {
        if      ( x == NULL )    return 0;
        if      ( x->key > key ) rank( x->left, key );
        else if ( x->key < key ) 1 + size( x->left ) + rank( x->right, key );
        else                     return size( x->left );
    }

    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

    class NodePrint {
    public:
        Key key;
        int height;
        int indexInTree;
        NodePrint( Key key, int height, int indexInTree ): key( key ), height( height ), indexInTree( indexInTree ) {}
        bool operator > ( NodePrint& rhs ) {
            return key > rhs.key;
        }
        bool operator < ( NodePrint& rhs ) {
            return key < rhs.key;
        }
        bool operator == ( NodePrint& rhs ) {
            return key == rhs.key;
        }
        bool operator >= ( NodePrint& rhs ) {
            return operator == ( rhs ) || operator > ( rhs );
        }
        bool operator <= ( NodePrint& rhs ) {
            return operator == ( rhs ) || operator < ( rhs );
        }
        bool operator != ( NodePrint& rhs ) {
            return ! operator == ( rhs );
        }
    };

public:

    void printTree() {
        int currentHeight = 0;
        vector< NodePrint > treeMap;

        while ( currentHeight <= maxHeight ) {
            for ( auto& s : allNodes ) {
                if ( s.get()->height == currentHeight ) {
                    NodePrint nodeToPrint( s.get()->key, s.get()->height, s.get()->indexInTree );
                    treeMap.push_back( nodeToPrint );
                }
            }
            currentHeight++;
        }

        sort( treeMap.begin(), treeMap.end() );

        currentHeight = 0;

        while ( currentHeight <= maxHeight ) {
            for ( auto& s : treeMap ) {
                if ( s.height == currentHeight ) {
                    cout << s.key << "[" << s.indexInTree << "]" << "<" << s.height << ">" << " ";
                } 
            }
            cout << endl;
            currentHeight++;
        }
    }

    int size() {   
        return size( root );
    }

    bool isEmpty() {   
        return size() == 0;   
    }

    void put( Key key, Value value ) {   
        root = put( root, key, value, 0, 1 );   
    }

    void erase( Key key ) {   
        root = erase( root, key );   
    }

    Key min() {   
        return min( root )->key;
    }

    Key max() {   
        return max( root )->key;
    }

    void eraseMin() {   
        root = eraseMin( root );   
    }

    void eraseMax() {   
        root =eraseMax( root );   
    }

    Key floor( Key key ) {   
        if ( key < min() ) {
            throw runtime_error( "floor(): No floor for inputted key" );
        }
        return floor( root, key )->key;
    }

    Key ceiling( Key key ) {
        if ( key > max() ) {
            throw runtime_error( "celing(): No ceiling for inputted key" );
        }
        return ceiling( root, key )->key;
    }

    Value get( Key key ) {   
        if ( rank( key ) == 0 && root->key != key ) {
            throw out_of_range( "get(): Key inputted not exist" );
        }
        return get( root, key );
    }

    Key select( int k ) {   
        if ( k > size() ) throw out_of_range( "select(): index out of range" );
        return select( root, k )->key;   
    }

    int rank( Key key ) {   
        return rank( root, key );   
    }

    Queue< Key > keys() {   
        return keys( min(), max() );   
    }

    Queue< Key > keys( int lo, int hi ) {
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    Key getRoot() {
        return root->key;
    }

};

int main( int argc, char** argv ) {
    
    int n = stoi( argv[ 1 ] );
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, 20 );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl;
    cout << left << setw( 21 ) << "The data of the tree: ";
    for ( auto s : bst.keys() ) cout << s << " ";
    cout << endl;
    cout << endl << endl;

    bst.printTree();

    cout << endl << endl;
}
