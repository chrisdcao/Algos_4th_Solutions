#include <iostream>
#include <numeric>
#include <iomanip>
#include <vector>
#include <memory>
#include "Queue.h"
#include "Random.h"

using namespace std;

// omits rank + select + count field N
template < typename Key, typename Value > 
class BinarySearchTree {
private:

    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        int index = -1;
    };

    Node* newNode( Key key, Value val ) {
        allNodes.push_back( make_unique< Node >() );
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;

    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

    // Node affecting the size of the tree
    Node* min( Node* x ) { return ( x->left == NULL ) ? x : min( x->left ); }

    Node* max( Node* x ) { return ( x->right == NULL ) ? x : max( x->right ); }

    Node* floor( Node* x , Key key ) { 
        if ( x == NULL ) return NULL;
        else if ( x->key == key ) return x;
        else if ( x->key > key ) return floor( x->left, key );
        Node* t = floor( x->right, key );
        return ( t == NULL ) ? x : t;
    }

    Node* ceiling( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        else if ( x->key == key ) return x;
        else if ( x->key < key ) return ceiling( x->right, key );
        Node* t = ceiling( x->left, key );
        return ( t == NULL ) ? x : t;
    }

    Value get( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key > key ) return get( x->left, key );
        else if ( x->key < key ) return get( x->right, key );
        else return x->val;
    }

    // affecting size of the tree
    Node* put( Node* x, Key key, Value val ) {
        if ( x == NULL ) {
            N++;
            return newNode( key, val );
        }
        if      ( x->key > key ) x->left = put( x->left, key, val );
        else if ( x->key < key ) x->right = put( x->right, key, val );
        else                     x->val = val;
        return x;
    }

    Node* erase( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key > key ) x->left = erase( x->left, key );
        else if ( x-> key < key ) x->right = erase( x->right, key );
        else {
            if ( x->right == NULL ) {
                N--;
                return x->left;
            }
            if ( x->left == NULL ) {
                N--;
                return x->right;
            }
            Node* temp = x;
            x = min( temp->right );
            x->right = eraseMin( temp->right );
            x->left = temp->left;
        }
        return x;
    }

    Node* eraseMin( Node* x ) {
        if ( x->left == NULL ) {
            N--;
            return x->right;
        }
        x->left = eraseMin( x->left );
        return x;
    }

    Node* eraseMax( Node* x ) {
        if ( x->right == NULL ) {
            N--;
            return x->left;
        }
        x->right = eraseMax( x->right );
        return x;
    }

    Node* root = NULL;
    int N = 0;

public:

    BinarySearchTree(): root( NULL ), N( 0 ) {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    Value get( Key key ) {  return get( root, key ); }

    void put( Key key, Value val ) { root = put( root, key, val ); }

    void eraseMin() { root = eraseMin( root ); }

    void eraseMax() { root = eraseMax( root ); }

    void erase( Key key ) { root = erase( root, key ); }

    Key floor( Key key ) { return ( floor( root, key ) == NULL ) ? throw runtime_error( "floor(): No Floor for this key" ) : floor( root, key )->key; }

    Key ceiling( Key key ) { return ( ceiling( root, key ) == NULL ) ? throw runtime_error( "ceiling(): No Ceiling for this key" ) : ceiling( root, key )->key; }

    Key min() { return min( root )->key;  }

    Key max() { return max( root )->key; }

    Queue< Key > keys( Key lo, Key hi ) {
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    Queue< Key > keys() { return keys( min(), max() ); }

};

int main( int argc, char** argv ) {
    
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    // THIS PIECE OF CODE IS TO DEBUG CERTAIN CASES
    /*
     * int i = 0, x;
     * while ( cin >> x ) {
     *     cout << x << endl;
     *     bst.put( x, i++ );
     * }
     */
    int n = stoi( argv[ 1 ] );
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, 12 );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "eraseMin(): ";
    bst.eraseMin();
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "eraseMax(): ";
    bst.eraseMax();
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "erase( 3 ): ";
    bst.erase( 3 );
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "ceiling( 4 ): " << bst.ceiling( 4 ) << endl << endl;
    cout << left << setw( 21 ) << "floor( 5 ): " << bst.floor( 5 ) << endl << endl;

    cout << left << setw( 21 ) << "get( 4 ): " << bst.get( 4 ) << endl << endl;

    return 0;

}
