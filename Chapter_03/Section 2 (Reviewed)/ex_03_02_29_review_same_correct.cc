#include <iostream>
#include <iomanip>
#include <numeric>
#include <vector>
#include "Queue.h"
#include "Random.h"
#include "BinarySearchTree.h"

using namespace std;

// in-order traversal: left-visit-right
bool isBinaryTree( BinarySearchTree< int, int >::Node* x ) {
    if ( x->left == NULL && x->right != NULL ) { 
        if ( x->N != x->right->N + 1 ) {
            return false;
        }
    }
    else if ( x->left != NULL && x->right == NULL ) {
        if ( x->N != x->left->N + 1 ) {
            return false;
        }
    }
    else if ( x->left != NULL && x->right != NULL ) {
        if ( x->N != x->left->N + x->right->N + 1 ) {
            return false;
        }
    }
    else {
        if ( x->N != 1 ) {
            return false;
        }
    }

    bool leftWing = true, rightWing = true;

    if ( x->left != NULL ) {
        leftWing = isBinaryTree( x->left );
    }

    if ( x->right != NULL ) {
        rightWing = isBinaryTree( x->right );
    }

    return ( !leftWing || !rightWing ) ? false : true;
}

int main( int argc, char** argv ) {
    
    int n = stoi( argv[ 1 ] );
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, 12 );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    cout << ( ( isBinaryTree( bst.rootNode() ) == 0 ) ? "false" : "true" ) << endl;

    return 0;

}
