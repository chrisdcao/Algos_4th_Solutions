#include <iostream>
#include <string>
#include <numeric>
#include <iomanip>
#include <vector>
#include "Random.h"

using namespace std;

// Calculate the internal path length (aka number of compares) of a Perfectly Balanced tree
int optCompares( int N ) {
    int elemsTaken = 0;
    int numOfCompares = 0;
    int i = 0;

    while ( elemsTaken + pow( 2, i ) < N ) {
        elemsTaken += pow( 2, i );
        // number of compares =  ∑ (current level * num of elems in that level)
        numOfCompares += i * pow( 2, i );
        i++;
    }

    // then the remains at the last level layer of the tree * the level index
    numOfCompares += ( N - elemsTaken ) * i;

    return numOfCompares;
}

int main( int argc, char** argv ) {

    int n = stoi( argv[ 1 ] );

    cout << "The optimal compares of perfectly balanced BST size " << n << " is: " << optCompares( n ) << endl;

    return 0;

}
