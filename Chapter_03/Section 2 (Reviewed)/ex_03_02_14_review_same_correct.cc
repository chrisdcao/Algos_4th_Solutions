#include <iostream>
#include <string>
#include <numeric>
#include <iomanip>
#include <vector>
#include <memory>
#include "Random.h"
#include "Queue.h"

using namespace std;

template < typename Key, typename Value > 
class BinarySearchTree {
private:
    
    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        int N = 0;
        int index = -1;
    };

    Node* newNode( Key key, Value val, int N ) {
        allNodes.push_back( make_unique< Node >() );
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;
    Node* root = NULL;

    int size( Node* x ) { 
        return ( x == NULL ) ? 0 : x->N;
    }

    Node* put( Node* x, Key key, Value val ) {
        if ( x == NULL ) return NULL;
        if ( x->key > key ) x->left = put( x->left, key, val );
        else if ( x->key < key ) x->right = put( x->right, key, val );
        else x->val = val;
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* erase( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key > key ) x->left = erase( x->left, key );
        else if ( x->key < key ) x->right = erase( x->right, key );
        else {
            Node* temp = x;
            x->right = min( temp->right );
            x->right = eraseMin( temp->right );
            x->left = temp->left;
        }
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* min( Node* x ) {
        return ( x->left == NULL ) ? x : min( x->left );
    }

    Node* max( Node* x ) {
        return ( x->right == NULL ) ? x : max( x->right );
    }


    Node* eraseMin( Node* x ) {
        if ( x->left == NULL ) return x->right;
        x->left = eraseMin( x->left );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* eraseMax( Node* x ) {
        if ( x->right == NULL ) return x->left;
        x->right = eraseMax( x->right );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* floor( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x->key > key ) return floor( x->left, key );
        Node* t = floor( x->right, key );
        return ( t == NULL ) ? x : t;
    }

    Node* ceiling( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x->key < key ) return ceiling( x->right, key );
        Node* t = ceiling( x->left, key );
        return ( t == NULL ) ? x : t;
    }

    int rank( Node* x, Key key ) {
        if      ( x == NULL )    return 0;
        if      ( x->key > key ) return rank( x->left, key );
        // return the floor?
        else if ( x->key < key ) return 1 + size( x->left ) + rank( x->right, key );
        else                     return size( x->left );
    }

    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

public:

    // ITERATIVE IMPLEMENTATION
    // largest smaller
    Key floor( Key key ) {
        if ( root == NULL ) throw out_of_range( "floor(): Tree is EMPTY!" );
        if ( key < min() ) throw logic_error( "floor(): NO floor for inputted key!" );
        Node* x = root;
        Node* f = NULL;
        while ( x != NULL ) {
            if ( x->key > key ) {
                x = x->left;
            }
            else if ( x->key == key ) {
                return x->key;
            }
            // find max of the smaller side
            else {
                if ( f == NULL ) {
                    f = x;
                }
                else {
                    if ( f->key < x->key ) {
                        f = x;
                    }
                }
                x = x->right;
            }
        }
        return f->key;
    }

    Key ceiling( Key key ) {
        if ( root == NULL ) throw out_of_range( "floor(): Tree is EMPTY!" );
        if ( key > max() ) throw logic_error( "floor(): NO ceiling for inputted key!" );
        Node* x = root;
        Node* c = NULL;
        while ( x != NULL ) {
            if ( x->key < key ) {
                x = x->right;
            }
            else if ( x->key == key ) {
                return x->key;
            }
            else {
                if ( c == NULL ) {
                    c = x;
                }
                else {
                    if ( c->key > x->key ) {
                        c = x;
                    }
                }
                x = x->left;
            }
        }
        return c->key;
    }

    int rank( Key key ) {
        Node* x = root;
        while ( x != NULL ) {
            if ( x->key > key ) x = x->left;
            else if ( x->key < key ) x = x->right;
            else return size( x->left );
        }
        return ( x == NULL ) ? 0 : 1 + size( x->left );
    }

    Key select( int k ) {
        if ( root == NULL ) throw out_of_range( "select(): The tree is empty" );
        Node* x = root;
        int t = size( x->left );
        while ( true ) {
            if ( t > k ) {
                x = x->left;
                if ( x == NULL ) {
                    break;
                }
                t = size( x->left );
            }
            else if ( t < k ) {
                x = x->right;
                if ( x == NULL ) {
                    break;
                }
                t = size( x->left );
                k = k-t-1;
            }
            else return x->key;
        }
        return ( x == NULL ) ? NULL : x->key;
    }

    Key min() {
        if ( root == NULL ) throw out_of_range( "max(): Tree is EMPTY!" );
        Node* x = root;
        while ( x->left != NULL ) x = x->left;
        return x->key;
    }

    Key max() {
        if ( root == NULL ) throw out_of_range( "max(): Tree is EMPTY!" );
        Node* x = root;
        while ( x->right != NULL ) x = x->right;
        return x->key;
    }

    int size() { 
        return size( root );
    }

    void put( Key key, Value val ) {
        if ( size() == 0 ) {
            root = newNode( key, val, 0 );
        }
        else {
            Node* x = root;
            string nextLink = "none";
            while ( x != NULL ) {
                if ( x->key > key ) {
                    nextLink = "left";
                    if ( x->left == NULL ) break;
                    x = x->left;
                }
                else if ( x->key < key ) {
                    nextLink = "right";
                    if ( x->right == NULL ) break;
                    x = x->right;
                }
                // this will result in no weight at each node update
                else {
                    x->val = val;
                    return;
                }
            }
            if ( nextLink == "left" )
                x->left = newNode( key, val, 0 );
            else if ( nextLink == "right" )
                x->right = newNode( key, val, 0 );
            // else if ( nextLink == "none" )
                // x = newNode( key, val, 0 );

            Node* t = root;

            // update the weight at each node on the travel path if we create a new node (excluding the root)
            while ( t != NULL ) {
                if ( t->key > key ) {
                    t = t->left;
                    if ( t == NULL ) break;
                    else t->N += 1;
                }
                else if ( t->key < key ) {
                    t = t->right;
                    if ( t == NULL ) break;
                    else t->N += 1;
                }
                else break;
            }
        }
        // update the root
        root->N = size( root->left ) + size( root->right ) + 1;
    }

    Queue< Key > keys( Key lo, Key hi ) {
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    Queue< Key > keys() {
        return keys( min(), max() );
    }

};

int main( int argc, char** argv ) {

    int n = stoi( argv[ 1 ] );
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, 12 );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl;

    cout << "min(): " <<  bst.min() << "\nmax(): " << bst.max() << endl;

    cout << "floor( 4 ): " << bst.floor( 4 ) << endl;

    cout << "ceiling( 4 ): " << bst.ceiling( 4 ) << endl;

    cout << "rank( 3 ): " << bst.rank( 3 ) << endl;

    cout << "select( 4 ): " << bst.select( 4 ) << endl;

    return 0;

}
