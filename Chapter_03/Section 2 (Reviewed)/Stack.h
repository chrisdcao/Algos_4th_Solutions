#ifndef STACK_H
#define STACK_H
#include <iostream>
#include <vector>
#include <memory>
#include <string>
using namespace std;

template <typename T>
class Stack {
private:
    class Node {
    public:
        T item;
        Node* next;
        int index = -1;
    };

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    vector<unique_ptr<Node>> allNodes;

    int N;
    Node* tail;

public:

    Stack() {
        N = 0;
        tail = NULL;
    }

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void push(T item) {
        Node* temp = newNode();
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            tail = temp;
        } else {
            temp->next = tail;
            tail = temp;
        }
        ++N;
    }

    T pop() {
        if (N != 0) {
            T item = tail->item;
            Node* oldtail = tail;
            tail = tail->next;
            freeNode(oldtail);
            --N;
            return item;
        }
        else return NULL;
    }

    T peek() {
        return ( tail == NULL ) ? NULL : tail->item;
    }
};

#endif
