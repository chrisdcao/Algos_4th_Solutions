#include <iostream>
#include <numeric>
#include <string>
#include <iomanip>
#include "Random.h"
#include "Queue.h"

using namespace std;

// Bro Phong has stated that this can only have BEST = amortised time ( ~N )
template < typename Key, typename Value >
class ThreadedST {
public:

    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        Node* succ = NULL;
        Node* pred = NULL;
        int N = 0;
        int index = -1;
    };

private:

    Node* newNode( Key key, Value val, int N, Node* predecessor, Node* successor ) {
        allNodes.push_back( make_unique< Node >() ); 
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->pred = predecessor;
        allNodes.back()->succ = successor;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;
    Node* root = NULL;

    int size( Node* x ) {   return ( x == NULL ) ? 0 : x->N; }  

    Node* erase( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key < key ) x->right = erase( x->right, key );
        else if ( x->key > key ) x->left = erase( x->left, key );
        else {
            // this is similar to deleting a node in between 2 nodes, that you re-assign the prev->next = ->next && next->prev = prev;
            x->succ->pred = x->pred;
            x->pred->succ = x->succ;

            if ( x->right == NULL ) return x->left;
            if ( x->left == NULL ) return x->right;

            Node* temp = x;
            x = min( temp->right );
            x->right = eraseMin( temp->right );
            x->left = temp->left;
            freeNode( temp );
        }
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* put( Node* x, Key key, Value val, Node* predecessor, Node* successor ) {
        if ( x == NULL ) return newNode( key, val, 1, predecessor, successor );
        if ( x->key < key ) {
            if      ( predecessor == NULL )       predecessor = x;
            else if ( predecessor->key < x->key ) predecessor = x;
            x->right = put( x->right, key, val, predecessor, successor );
        }
        else if ( x->key > key ) {
            if      ( successor == NULL )       successor = x;
            else if ( successor->key > x->key ) successor = x;
            x->left = put( x->left, key, val, predecessor, successor );
        }
        else x->val = val;

        if ( x->right != NULL ) {
            if ( x->succ == NULL ) x->succ = min( x->right );
            else if ( x->succ->key > min( x->right )->key )
                x->succ = min( x->right );
        }
        if ( x->left != NULL ) {
            if ( x->pred == NULL ) x->pred = max( x->left );
            else if ( x->pred->key < max( x->left )->key )
                x->pred = max( x->left );
        }

        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* min( Node* x ) {   return ( x->left == NULL ) ? x : min( x->left );   }

    Node* max( Node* x ) {   return ( x->right == NULL ) ? x : max( x->right );   }

    Node* eraseMin( Node* x ) {
        if ( x->left == NULL ) {
            x->succ->pred = NULL;
            Node* rightChild = x->right;
            freeNode( x );
            return rightChild;
        }
        x->left = eraseMin( x->left );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* eraseMax( Node* x ) {
        if ( x->right == NULL ) {
            x->pred->succ = NULL;
            Node* leftChild = x->left;
            freeNode( x );
            return leftChild;
        }
        x->right = eraseMax( x->right );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    // largest smaller
    Node* floor( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x->key > key ) return floor( x->left, key );
        Node* t = floor( x->right, key );
        return ( t != NULL ) ? t : x;
    }

    // smallest larger
    Node* ceiling( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x-> key < key ) return ceiling( x->right, key );
        Node* t = ceiling( x->left, key );
        return ( t != NULL ) ? t : x;
    }

    Value get( Node* x, Key key ) {
        if      ( x == NULL )    return NULL;
        if      ( x->key > key ) return get( x->left, key );
        else if ( x->key < key ) return get( x->right, key );
        else                     return x->val;
    }

    Node* select( Node* x, int k ) {
        if ( x == NULL ) return NULL;
        int t = size( x->left );
        if      ( t > k ) select( x->left, k );
        else if ( t < k ) select( x->right, k-t-1 );
        else              return x;
    }

    int rank( Node* x, Key key ) {
        if      ( x == NULL )    return 0;
        if      ( x->key > key ) rank( x->left, key );
        else if ( x->key < key ) 1 + size( x->left ) + rank( x->right, key );
        else                     return size( x->left );
    }

    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

    void nodes( Node* x, Queue< Node* >& queue, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) nodes( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x );
        if ( x->key < hi ) nodes( x->right, queue, lo, hi );
    }

public:

    int size() {   return size( root );   }

    bool isEmpty() {   return size() == 0;   }

    void put( Key key, Value value ) {   root = put( root, key, value, NULL, NULL );   }

    Key next( Key key ) {
        if ( key == max() ) return NULL;
        Node* x = root;
        while ( x != NULL ) {
            if      ( x->key > key ) x = x->left;
            else if ( x->key < key ) x = x->right;
            else                     return x->succ->key;
        }
        return NULL;
    }

    Key prev( Key key ) {
        if ( key == min() ) return NULL;
        Node* x = root;
        while ( x != NULL ) {
            if      ( x->key > key ) x = x->left;
            else if ( x->key < key ) x = x->right;
            else                     return x->pred->key;
        }
        return NULL;
    }

    void erase( Key key ) {   root = erase( root, key );   }

    Key min() {   return min( root )->key;   }

    Key max() {   return max( root )->key;   }

    void eraseMin() {   root = eraseMin( root );   }

    void eraseMax() {   root = eraseMax( root );   }

    Key floor( Key key ) {   
        if ( key < min() ) {
            throw runtime_error( "floor(): No floor for inputted key" );
        }
        return floor( root, key )->key;
    }

    Key ceiling( Key key ) {
        if ( key > max() ) {
            throw runtime_error( "celing(): No ceiling for inputted key" );
        }
        return ceiling( root, key )->key;
    }

    Value get( Key key ) {   
        if ( rank( key ) == 0 && root->key != key ) {
            throw out_of_range( "get(): Key inputted not exist" );
        }
        return get( root, key );
    }

    Key select( int k ) {   
        if ( k > size() ) throw out_of_range( "select(): index out of range" );
        return select( root, k )->key;   
    }

    int rank( Key key ) {   return rank( root, key );   }

    Queue< Key > keys() {   return keys( min(), max() );   }

    Queue< Key > keys( Key lo, Key hi ) {
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    Queue< Node* > nodes( Key lo, Key hi ) {
        Queue< Node* > queue;
        nodes( root, queue, lo, hi );
        return queue;
    }

    Queue< Node* > nodes() {   return nodes( min(), max() );   }
};

int main( int argc, char** argv ) {
    
    int n = stoi( argv[ 1 ] );
    ThreadedST< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, 12 );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() )
        cout << s << " ";
    cout << endl << endl;

    cout << left << setw( 21 ) << "DEBUG: " << endl;
    for ( auto s : bst.nodes() ) {
        cout << "Current Key: " << s->key;
        cout << ". Prev: ";
        if ( s->pred == NULL ) cout << "None";
        else cout << s->pred->key;
        cout << ". Next: ";
        if ( s->succ == NULL ) cout << "None";
        else cout << s->succ->key;
        cout << endl;
    }
    cout << endl << endl;

    bst.eraseMin();
    cout << endl << "eraseMin()" << endl;
    bst.eraseMax();
    cout << "eraseMax()" << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() )
        cout << s << " ";
    cout << endl << endl;

    cout << left << setw( 21 ) << "DEBUG: " << endl;
    for ( auto s : bst.nodes() ) {
        cout << "Current Key: " << s->key;
        cout << ". Prev: ";
        if ( s->pred == NULL ) cout << "None";
        else cout << s->pred->key;
        cout << ". Next: ";
        if ( s->succ == NULL ) cout << "None";
        else cout << s->succ->key;
        cout << endl;
    }
    cout << endl << endl;

    cout << "bst.erase(6)" << endl; 
    bst.erase( 6 );

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() )
        cout << s << " ";
    cout << endl << endl;
    
    cout << left << setw( 21 ) << "DEBUG: " << endl;
    for ( auto s : bst.nodes() ) {
        cout << "Current Key: " << s->key;
        cout << ". Prev: ";
        if ( s->pred == NULL ) cout << "None";
        else cout << s->pred->key;
        cout << ". Next: ";
        if ( s->succ == NULL ) cout << "None";
        else cout << s->succ->key;
        cout << endl;
    }
    cout << endl << endl;

    return 0;

}
