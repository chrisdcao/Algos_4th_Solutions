#include <iostream>
#include <string>
#include <numeric>
#include <iomanip>
#include <vector>
#include <memory>
// these are just for debugging
#include <utility>
#include <map>
///
#include "Random.h"
#include "Queue.h"

using namespace std;

template < typename Key, typename Value > 
class BinarySearchTree {
private:
    
    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        int N = 0;
        int index = -1;
    };

    Node* newNode( Key key, Value val, int N ) {
        allNodes.push_back( make_unique< Node >() );
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;
    Node* root = NULL;

    int size( Node* x ) { 
        return ( x == NULL ) ? 0 : x->N;
    }

    Node* put( Node* x, Key key, Value val ) {
        if ( x == NULL ) return NULL;
        if ( x->key > key ) x->left = put( x->left, key, val );
        else if ( x->key < key ) x->right = put( x->right, key, val );
        else x->val = val;
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* erase( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key > key ) x->left = erase( x->left, key );
        else if ( x->key < key ) x->right = erase( x->right, key );
        else {
            Node* temp = x;
            x->right = min( temp->right );
            x->right = eraseMin( temp->right );
            x->left = temp->left;
        }
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* min( Node* x ) {
        return ( x->left == NULL ) ? x : min( x->left );
    }

    Node* max( Node* x ) {
        return ( x->right == NULL ) ? x : max( x->right );
    }

    Node* eraseMin( Node* x ) {
        if ( x->left == NULL ) return x->right;
        x->left = eraseMin( x->left );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* eraseMax( Node* x ) {
        if ( x->right == NULL ) return x->left;
        x->right = eraseMax( x->right );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* floor( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x->key > key ) return floor( x->left, key );
        Node* t = floor( x->right, key );
        return ( t == NULL ) ? x : t;
    }

    Node* ceiling( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x->key < key ) return ceiling( x->right, key );
        Node* t = ceiling( x->left, key );
        return ( t == NULL ) ? x : t;
    }

    Node* select( Node* x, int k ) {
        if  ( x == NULL ) return NULL;
        int t = size( x->left );
        if      ( t > k ) select( x->left, k );
        else if ( t < k ) select( x->right, k-t-1 );
        else              return x;
    }
    
    int rank( Node* x, Key key ) {
        if      ( x == NULL )    return 0;
        if      ( x->key > key ) rank( x->left, key );
        else if ( x->key < key ) 1 + size( x->left ) + rank( x->right, key );
        else                     return size( x->left );
    }
    
    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

    // DEBUG: Check if the weight update is correct
    void keysWeight( Node* x, map< Key, int >& map, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) keysWeight( x->left, map, lo, hi );
        if ( x->key >= lo && x->key <= hi ) map.insert( make_pair( x->key, x->N ) );
        if ( x->key < hi ) keysWeight( x->right, map, lo, hi );
    }

public:

    int size() { 
        return size( root );
    }

    Value get( Key key ) {
        Node* x = root;
        while ( x != NULL ) {
            if ( x->key > key ) x = x->left;
            else if ( x->key < key ) x = x->right;
            else return x->val;
        }
        return NULL;
    }

    void put( Key key, Value val ) {
        if ( size() == 0 ) {
            root = newNode( key, val, 0 );
        }
        else {
            Node* x = root;
            string nextLink = "none";
            while ( x != NULL ) {
                if ( x->key > key ) {
                    nextLink = "left";
                    if ( x->left == NULL ) break;
                    x = x->left;
                }
                else if ( x->key < key ) {
                    nextLink = "right";
                    if ( x->right == NULL ) break;
                    x = x->right;
                }
                // this will result in no weight at each node update
                else {
                    x->val = val;
                    return;
                }
            }
            if ( nextLink == "left" )
                x->left = newNode( key, val, 0 );
            else if ( nextLink == "right" )
                x->right = newNode( key, val, 0 );

            Node* t = root;

            // update the weight at each node on the travel path if we create a new node (excluding the root)
            while ( t != NULL ) {
                if ( t->key > key ) {
                    t = t->left;
                    if ( t == NULL ) break;
                    else t->N += 1;
                }
                else if ( t->key < key ) {
                    t = t->right;
                    if ( t == NULL ) break;
                    else t->N += 1;
                }
                else break;
            }
        }
        // update the root
        root->N = size( root->left ) + size( root->right ) + 1;
    }

    Key min() {
        return min( root )->key;
    }

    Key max() {
        return max( root )->key;
    }

    Queue< Key > keys( Key lo, Key hi ) {
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    // DEBUG: Check if the weight update is correct
    map< Key, int > keysAndWeight() {
        map< Key, int > key_weight;
        keysWeight( root, key_weight, min(), max() );
        return key_weight;
    }

    Queue< Key > keys() {
        return keys( min(), max() );
    }

};

int main( int argc, char** argv ) {

    int n = stoi( argv[ 1 ] );
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, 12 );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "get( 4 ): " << bst.get( 4 ) << endl << endl;

    // DEBUG: print the number of child (or in other words weight) of that node
    for ( auto s : bst.keysAndWeight() ) {
        printf( "key: %d\tweight: %d\n", s.first, s.second );
    }

    return 0;

}
