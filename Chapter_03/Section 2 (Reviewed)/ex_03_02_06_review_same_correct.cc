#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <numeric>
#include "Random.h"
#include "Queue.h"

using namespace std;

/*
 * so we only have on exercise, with 2 implementations being supportive of each other (similar format to what we have been doing so far):
 * 1. The recursive one to do something
 * 2. The iterative one to call on the recursive one and do something
 */
template <typename Key, typename Value> 
class BinarySearchTree {

private:

    class Node {
    public:
        Key key;
        Value val;
        Node* right = NULL;
        Node* left = NULL;
        int height = 1;
        int N = 1;
        int index = -1;
    };

    Node* newNode( Key key, Value val, int N, int height ) {
        allNodes.push_back( make_unique< Node >() );
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->val = val;
        allNodes.back()->key = key;
        allNodes.back()->N = N;
        allNodes.back()->height = height;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->N < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;

    int size( Node* x ) { return (x == NULL) ? 0 : x->N; }

    int getHeightIteratively( Node* x ) { return ( x == NULL ) ? 0 : x->height; }

    int getHeightRecursively( Node* h, int count ) const {
        if ( !h ) return count;
        // Bằng cách không cộng 2 thằng (như mình đã làm) thì sẽ không phải re-assign value
        int leftHeight = getHeightRecursively( h->left, count + 1 );
        // như vậy nếu nếu hit NULL thì count hiện tại sẽ tiếp đi nhánh phải (chứ không phải count return)
        int rightHeight = getHeightRecursively( h->right, count + 1 );
        return ( leftHeight < rightHeight ) ? rightHeight : leftHeight;
    }

    Value get( Node* x, Key key ) {
        if      ( x == NULL )    return NULL;
        if      ( x->key > key ) return get( x->left, key );
        else if ( x->key < key ) return get( x->right, key );
        else                     return x->val;
    }

    Node* select( Node* x, int k ) {
        if  ( x == NULL ) return NULL;
        int t = size( x->left );
        if      ( t > k ) return select( x->left, k );
        else if ( t < k ) return select( x->right, k-t-1 );
        else return x;
    }

    Node* put( Node* x, Key key, Value val ) {
        if      ( x == NULL )    return newNode( key, val, 1, 1 );
        if      ( x->key > key ) x->left = put( x->left, key, val );
        else if ( x->key < key ) x->right = put( x->right, key, val );
        else                     x->val = val;
        x->N = size( x->left ) + size( x->right ) + 1;
        x->height = std::max( getHeightIteratively( x->left ), getHeightIteratively( x->right ) ) + 1;
        return x;
    }

    Node* erase( Node* x, Key key ) {
        if      ( x == NULL )    return NULL;
        if      ( x->key > key ) x->left = erase( x->left, key );
        else if ( x->key < key ) x->right = erase( x->right, key );
        else {
            if ( x->left == NULL ) return x->right;
            if ( x->right == NULL ) return x->left;
            Node* t = x;
            x = min( t->right );
            x->right = eraseMin( t->right );
            x->left = t->left;
        }
        x->N = size( x->left ) + size( x->right ) + 1;
        x->height = std::max( getHeightIteratively( x->right ), getHeightIteratively( x->left ) ) + 1;
        return x;
    }

    Node* eraseMin( Node* x ) {
        if ( x->left == NULL ) return x->right;
        x->left = eraseMin( x->left );
        x->N = size( x->left ) + size( x->right ) + 1;
        x->height = std::max( getHeightIteratively( x->right ), getHeightIteratively( x->left ) ) + 1;
        return x;
    }

    Node* eraseMax( Node* x ) {
        if ( x->right == NULL ) return x->left;
        x->right = eraseMax( x->right );
        x->N = size( x->left ) + size( x->right ) + 1;
        x->height = std::max( getHeightIteratively( x->right ), getHeightIteratively( x->left ) ) + 1;
        return x;
    }

    Node* min( Node* x ) { return ( x->left == NULL ) ? x : min( x->left ); }

    Node* max( Node* x ) { return ( x->right == NULL ) ? x : max( x->right ); }

    int rank( Node* x, Key key ) {
        if      ( x == NULL )    return 0;
        if      ( x->key > key ) rank( x->left, key );
        else if ( x->key < key ) rank( x->right, key );
        else return size( x );
    }

    Node* floor( Node* x, Key key ) {
        if ( x == NULL )     return NULL;
        if ( x->key > key )  return floor( x->left, key );
        if ( x->key == key ) return x;
        Node* t = floor( x->right, key );
        return ( t == NULL ) ? x : t;
    }

    Node* ceiling( Node* x, Key key ) {
        if      ( x == NULL )     return NULL;
        if      ( x->key < key )  return ceiling( x->right, key );
        else if ( x->key == key ) return x;
        Node* t = ceiling( x->left, key );
        return ( t == NULL ) ? x : t;
    }

    Node* root = NULL;

public:

    BinarySearchTree() = default;

    int getHeightIteratively() { return getHeightIteratively( root ); }

    int getHeightRecursively( Node* h ) { return getHeightRecursively( h, 0 ); }

    int size() { return size( root ); }

    Value get( Key key ) { return get( root, key ); }

    Key select( int k ) { return select( root, k ); }

    void put( Key key, Value val ) { root = put( root, key, val ); }

    void erase( Key key ) { root = erase( root, key ); }

    void eraseMin() { root = eraseMin( root ); }

    Key min() { return min( root )->key; }

    Key max() { return max( root )->key; }

    int rank( Key key ) { return rank( root, key ); }

    Key floor( Key key ) { 
        Node* t= floor( root, key );
        return ( t == NULL ) ? NULL : t->key;
    }

    Queue< Key > keys() { return keys( min(), max() ); }

    Queue< Key > keys( Key lo, Key hi ) { 
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

    Node* getRoot() { return root; }

};

int main(int argc, char** argv) {

    int n = stoi(argv[1]);
    BinarySearchTree<int, int> bst;

    cout << "The order of coming in: ";
    for (int i = 0; i < n; i++) {
        int a = random_uniform_distribution(0, n);
        cout << a << " ";
        bst.put(a, i);
    }
    cout << endl;

    cout << "bst keys: ";

    for (auto s : bst.keys())
        cout << s << " ";
    cout << endl;

    cout << bst.getHeightIteratively() << endl;
    cout << bst.getHeightRecursively( bst.getRoot() ) << endl;

    return 1;

}
