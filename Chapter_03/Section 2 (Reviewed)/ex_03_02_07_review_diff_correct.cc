#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <numeric>
#include "Random.h"
#include "Queue.h"

using namespace std;

template <typename Key, typename Value> 
class BinarySearchTree {

private:

    class Node {
    public:
        Key key;
        Value val;
        Node* right = NULL;
        Node* left = NULL;
        int N = 1;
        int index = -1;
        int height = 0;
    };

    Node* newNode( Key key, Value val, int N, int height ) {
        allNodes.push_back( make_unique< Node >() );
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->val = val;
        allNodes.back()->key = key;
        allNodes.back()->N = N;
        allNodes.back()->height = height;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->N < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;

    int getAvgComparesRecursively( Node* x, int currentHeight, int& inPathLength ) {
        const int CURRENT_HEIGHT = currentHeight;
        if ( x->left != NULL )
            inPathLength += getAvgComparesRecursively( x->left, currentHeight + 1, inPathLength );
        currentHeight = CURRENT_HEIGHT;
        if ( x->right != NULL )
            inPathLength += getAvgComparesRecursively( x->right, currentHeight + 1, inPathLength );

        return currentHeight;
    }

    int size( Node* x ) { return (x == NULL) ? 0 : x->N; }

    Value get( Node* x, Key key ) {
        if      ( x == NULL )    return NULL;
        if      ( x->key > key ) return get( x->left, key );
        else if ( x->key < key ) return get( x->right, key );
        else                     return x->val;
    }

    Node* select( Node* x, int k ) {
        if  ( x == NULL ) return NULL;
        int t = size( x->left );
        if      ( t > k ) return select( x->left, k );
        else if ( t < k ) return select( x->right, k-t-1 );
        else return x;
    }

    float totalPathLength = 0.00;

    Node*  put( Node* x, Key key, Value val, int height ) {
        if ( x == NULL ) {
            totalPathLength += height;
            return newNode( key, val, 1, height );
        }
        if      ( x->key > key )  x->left = put( x->left, key, val, height + 1 );
        else if ( x->key < key )  x->right = put( x->right, key, val, height + 1 );
        else                      x->val = val;
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* erase( Node* x, Key key ) {
        if      ( x == NULL )    return NULL;
        if      ( x->key > key ) x->left = erase( x->left, key );
        else if ( x->key < key ) x->right = erase( x->right, key );
        else {
            if ( x->left == NULL ) {
                totalPathLength -= x->height;
                if ( x->right != NULL )
                    totalPathLength -= size( x->right );
                return x->right;
            }
            if ( x->right == NULL ) {
                totalPathLength -= x->height;
                if ( x->left != NULL )
                    totalPathLength -= size( x->right );
                return x->left;
            }
            Node* t = x;
            x = min( t->right );
            x->right = eraseMin( t->right );
            x->left = t->left;
        }
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* eraseMin( Node* x ) {
        if ( x->left == NULL ) {
            totalPathLength -= x->height;
            if ( x->right != NULL )
                totalPathLength -= size( x->right );
            return x->right;
        }
        x->left = eraseMin( x->left );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* min( Node* x ) { return ( x->left == NULL ) ? x : min( x->left ); }

    Node* max( Node* x ) { return ( x->right == NULL ) ? x : max( x->right ); }

    int rank( Node* x, Key key ) {
        if      ( x == NULL )    return 0;
        if      ( x->key > key ) rank( x->left, key );
        else if ( x->key < key ) rank( x->right, key );
        else return size( x );
    }

    Node* floor( Node* x, Key key ) {
        if ( x == NULL )     return NULL;
        if ( x->key > key )  return floor( x->left, key );
        if ( x->key == key ) return x;
        Node* t = floor( x->right, key );
        return ( t == NULL ) ? x : t;
    }

    Node* root = NULL;

public:

    float getAvgComparesIteratively() {
        // print for DEBUGGING purpose
        cout << "Internal Path Length( Iterative ): " << totalPathLength << endl;
        cout << "Size: " << size() << endl;
        return ( float ) totalPathLength * 1.00 / ( float ) size() + 1;
    }

    float getAvgComparesRecursively() {
        int inPathLength = 0;
        getAvgComparesRecursively( root, 0, inPathLength );
        // print for DEBUGGING purpose
        cout << "Internal Path Length: " << inPathLength << endl;
        cout << "Size: " << size() << endl;
        return ( float ) inPathLength * 1.00 / ( float ) size() + 1;
    }

    BinarySearchTree() = default;

    int size() { return size( root ); }

    Value get( Key key ) { return get( root, key ); }

    Key select( int k ) { return select( root, k ); }

    void put( Key key, Value val ) { root = put( root, key, val, 0 ); }

    void erase( Key key ) { root = erase( root, key ); }

    void eraseMin() { root = eraseMin( root ); }

    Key min() { return min( root )->key; }

    Key max() { return max( root )->key; }

    int rank( Key key ) { return rank( root, key ); }

    Key floor( Key key ) { 
        Node* t= floor( root, key );
        return ( t == NULL ) ? NULL : t->key;
    }

    Queue< Key > keys() { return keys( min(), max() ); }

    Queue< Key > keys( Key lo, Key hi ) { 
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

};

int main(int argc, char** argv) {

    int n = stoi(argv[1]);
    BinarySearchTree<int, int> bst;

    cout << "The order of coming in: ";
    for (int i = 0; i < n; i++) {
        int a = random_uniform_distribution(0, n);
        cout << a << " ";
        bst.put(a, i);
    }
    cout << endl;

    cout << "Keys in the BST is: ";
    for ( auto s : bst.keys() )
        cout << s << " ";
    cout << endl;

    cout << setprecision( 3 ) << bst.getAvgComparesRecursively() << endl;
    cout << setprecision( 3 ) << bst.getAvgComparesIteratively() << endl;

    cout << endl << "\tAfter erase(4) (if have):" << endl;
    bst.erase( 4 );

    cout << "Keys in the BST is: ";
    for ( auto s : bst.keys() )
        cout << s << " ";
    cout << endl;

    cout << setprecision( 3 ) << bst.getAvgComparesRecursively() << endl;
    cout << setprecision( 3 ) << bst.getAvgComparesIteratively() << endl;

    return 0;

}
