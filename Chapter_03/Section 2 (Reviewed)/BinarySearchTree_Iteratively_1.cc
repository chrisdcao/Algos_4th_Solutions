#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <numeric>
#include <iomanip>
#include <memory>
#include "Stack.h"
#include "Random.h"
#include "sortedKeys.h"

using namespace std;

template < typename Key, typename Value >
class BinarySeachTree {
private:

    class Node {

    };

    Node* root = NULL;
    vector< unique_ptr< Node > > allNodes;



public:

    Queue < Key > keys( Key lo, Key hi ) {
        Stack< Node* > backtrackNodes;
        Queue< Key > sortedKeys;
        if ( !root ) throw out_of_range( "keys(): Tree is empty!" );
        if ( root->left ) {
            x = root->left;
            backtrackNodes.push( x );
            while ( !backtrackNodes.isEmpty() ) {
                if ( x->left ) {
                    x = x->left;
                    if ( x ) backtrackNodes.push( x );
                } else if ( x->right ) {
                    sortedKeys.enqueue( x->key );
                    x = x->right;
                } else {
                    sortedKeys.enqueue( x->key );
                    while ( !x->left && !x->right )
                        x = backtrackNodes.pop();
                }
            }
        }
        x = root;
        if ( root->right ) {
            x = root->left;
            backtrackNodes.push( x );
            while ( !backtrackNodes.isEmpty() ) {
                if ( x->left ) {
                    x = x->left;
                    if ( x ) backtrackNodes.push( x );
                } else if ( x->right ) {
                    sortedKeys.enqueue( x->key );
                    x = x->right;
                } else {
                    sortedKeys.enqueue( x->key );
                    while ( !x->left && !x->right )
                        x = backtrackNodes.pop();
                }
            }
        }
        return sortedKeys;
    }
};

