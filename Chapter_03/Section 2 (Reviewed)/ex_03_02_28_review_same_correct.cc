#include <iostream>
#include <map>
#include <utility>
#include <climits>
#include <vector>
#include <iomanip>
#include <numeric>
#include <string>
#include "Queue.h"
#include "Random.h"

using namespace std;

template < typename Key, typename Value > 
class BinarySearchTree {
private:

    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        int N = 1;
        int index = -1;
        void operator = ( Node* rhs ) {
            key = rhs->key;
            val = rhs->val;
            left = rhs->left;
            right = rhs->right;
            N = rhs->N;
            index = rhs->index;
        }

        void operator = ( Node& rhs ) {
            key = rhs->key;
            val = rhs->val;
            left = rhs->left;
            right = rhs->right;
            N = rhs->N;
            index = rhs->index;
        }

        void operator = ( Node&& rhs ) {
            key = rhs->key;
            val = rhs->val;
            left = rhs->left;
            right = rhs->right;
            N = rhs->N;
            index = rhs->index;
        }
    };

    Node* newNode( Key key, Value val, int N ) {
        allNodes.push_back( make_unique< Node >() ); 
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;
    Node* root = NULL;
    Node* cachedNode = NULL;

    int size( Node* x ) {   return ( x == NULL ) ? 0 : x->N; }  

    Node* put( Node* x, Key key, Value val ) {
        if ( x == NULL ) {
            cachedNode = newNode( key, val, 1 );
            return cachedNode;
        }
        if      ( x->key < key ) x->right = put( x->right, key, val );
        else if ( x->key > key ) x->left = put( x->left, key, val );
        else {
            x->val = val;
            cachedNode = x;
        }
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* erase( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key > key ) x->left = erase( x->left, key );
        else if ( x->key < key ) x->right = erase( x->right, key );
        else {
            if ( x->right == NULL ) return x->left;
            if ( x->left == NULL ) return x->right;
            Node* temp = x;
            x = min( temp->right );
            x->right = eraseMin( temp->right );
            x->left = temp->left;
        }
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* min( Node* x ) {   return ( x->left == NULL ) ? x : min( x->left );   }

    Node* max( Node* x ) { return ( x->right == NULL ) ? x : max( x->right ); }

    Node* eraseMin( Node* x ) {
        if ( x->left == NULL ) return x->right;
        x->left = eraseMin( x->left );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* eraseMax( Node* x ) {
        if ( x->right == NULL ) return x->left;
        x->right = eraseMax( x->right );
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    Node* floor( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x->key > key ) return floor( x->left, key );
        Node* t = floor( x->right, key );
        return ( t != NULL ) ? t : x;
    }

    Node* ceiling( Node* x, Key key ) {
        if ( x == NULL ) return NULL;
        if ( x->key == key ) return x;
        else if ( x-> key < key ) return ceiling( x->right, key );
        Node* t = ceiling( x->left, key );
        return ( t != NULL ) ? t : x;
    }

    Value get( Node* x, Key key ) {
        if      ( x == NULL )    return NULL;
        if      ( x->key > key ) return get( x->left, key );
        else if ( x->key < key ) return get( x->right, key );
        else                     return x->val;
    }

    Node* select( Node* x, int k ) {
        if ( x == NULL ) return NULL;
        int t = size( x->left );
        if      ( t > k ) return select( x->left, k );
        else if ( t < k ) return select( x->right, k-t-1 );
        else              return x;
    }

    int rank( Node* x, Key key ) {
        if      ( x == NULL )    return 0;
        if      ( x->key > key ) rank( x->left, key );
        else if ( x->key < key ) 1 + size( x->left ) + rank( x->right, key );
        else                     return size( x->left );
    }

    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( x == NULL ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

public:

    int size() {   return size( root );   }

    void printCachedNode() {
        cout << "Cached Node: (" << cachedNode->key << ", " << cachedNode->val << ")" << endl;
    }

    bool isEmpty() {   return size() == 0;   }

    void put( Key key, Value value ) {   
        if ( cachedNode != NULL ) {
            if ( key == cachedNode->key ) {
                cachedNode->val = value;
                return;
            }
        }
        root = put( root, key, value );   
    }

    void erase( Key key ) {   root = erase( root, key );   }

    Key min() {   return min( root )->key; }

    Key max() {   return max( root )->key; }

    void eraseMin() {   root = eraseMin( root );   }

    void eraseMax() {   root =eraseMax( root );   }

    Key floor( Key key ) {   
        if ( key < min() ) {
            throw runtime_error( "floor(): No floor for inputted key" );
        }
        return floor( root, key )->key;
    }

    Key ceiling( Key key ) {
        if ( key > max() ) {
            throw runtime_error( "celing(): No ceiling for inputted key" );
        }
        return ceiling( root, key )->key;
    }

    Value get( Key key ) {   
        if ( cachedNode != NULL ) {
            if ( key == cachedNode->key ) {
                return cachedNode->val;
            }
        }
        return get( root, key );
    }

    Key select( int k ) {   
        if ( k > size() ) throw out_of_range( "select(): index out of range" );
        return select( root, k )->key;   
    }

    int rank( Key key ) {   return rank( root, key );   }

    Queue< Key > keys() {   return keys( min(), max() );   }

    Queue< Key > keys( int lo, int hi ) {
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    Key getRoot() {   return root->key;   }

};

int main( int argc, char** argv ) {
    
    int n = stoi( argv[ 1 ] );
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: " << endl;
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, 12 );
        cout << "Data: (" << data << ", " << i << setw( 5 ) << ").";
        bst.put( data, i );
        bst.printCachedNode();
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "eraseMin(): ";
    bst.eraseMin();
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "eraseMax(): ";
    bst.eraseMax();
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "erase( 3 ): ";
    bst.erase( 3 );
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "ceiling( 4 ): " << bst.ceiling( 4 ) << endl << endl;
    cout << left << setw( 21 ) << "floor( 5 ): " << bst.floor( 5 ) << endl << endl;

    cout << left << setw( 21 ) << "get( 4 ): " << bst.get( 4 ) << "\t\t (return Value of Key '4', if Key '4' not here return NULL-converted value of type(int) (defaultly = 0))" << endl << endl;
    cout << left << setw( 21 ) << "select( 2 ): " << bst.select( 2 ) << "\t\t (return the Key at which there are 2 keys smaller)" << endl << endl;

    return 0;

}

