// Code fragment of Frequency Counter
int main( int argc, char** argv ) {
    
    int minlen = stoi( argv[ 1 ]);
    BinarySearchST<String, Integer> st;

    while ( cin >> word && !cin.eof() ) {
        if ( word.size() >= minlen ) {
            // the number of call to get() = number of call to else
            // with W words with D being distinct, we have W-D calls to else, making W-D calls to put and W-D calls to get
            // we insert based on the distinctness of <Key>, NOT the total -> number of call to put(): ~ D ( number of distinct words )
            if( !st.contains( word ) ) st.put( word,1 );
            else                       st.put( word, st.get( word ) + 1 );
        }
    }
    at the end of while: 1. calls to get(): ( W - D )
                         2. calls to put(): ( W - D ) + D = W
    String max = "";
    st.put( max, 0 );

    // since we traverse the whole table ( function keys() ), we will call D time to get() of word, and D time to get() of max -> ~ 2 * D 
    for ( String word : st.keys())
        if ( st.get( word ) > st.get( max ))
            max = word;

    return 0;

}

for W words with D words being distinct, the number is:
+ The number of put() issued by FrequencyCounter is : W
+ The number of get() in FrequencyCounter is : W - D + 2D = W + D
