#ifndef ORDEREDSINGLYLISTST_H
#define ORDEREDSINGLYLISTST_H

#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include <utility>
#include <iterator>

#define null NULL
#define String string
#define Integer int

using namespace std;

// this is the implementation of unordered list
template <typename Key, typename Value> 
class OrderedListST {
private:

    class Pair {
    public:
        Key key;
        Value val;
        Pair( Key key, Value val ): key( key ), val( val ) {}
        friend ostream& operator << ( ostream& os, Pair pair ) {
            os << "Key: " << pair.key << ". Val: " << pair.val;
            return os;
        }
        bool operator > ( Pair& rhs ) { return key > rhs.key; }
        bool operator < ( Pair& rhs ) { return key < rhs.key; }
        bool operator == ( Pair& rhs ) { return key == rhs.key; }
        bool operator != ( Pair& rhs ) { return !operator==( rhs ); }
        bool operator <= ( Pair& rhs ) { return operator>( rhs ) || operator==( rhs ); }
        bool operator >= ( Pair& rhs ) { return operator>( rhs ) || operator==( rhs ); }
    };

    class Node {  
    public:
        Pair pair;
        Node* next = null;
        Node( Key k, Value v, Node* n ): pair( k, v ), next( n ) {}
        Node( Key k, Value v ): pair( k, v ) {}
    };

    Node* first = null;
    int N = 0;

    class Iterator {

        Node* current;

    public:

        Iterator( Node* initLoc ): current( initLoc ) {}

        bool operator==( Iterator& rhs ) { return rhs.current == current; }

        bool operator!=( Iterator& rhs ) { return !operator==( rhs ); }

        Iterator& operator++() {  
            current = current->next;
            return *this;
        }

        Pair& operator*(  ) { return this->current->pair; }

    };


public:

    friend ostream& operator<< ( ostream& os, OrderedListST<Key, Value> st ) {
        Node* traverse = st.first;
        while ( traverse != null ) {
            os << "Key: " << traverse->pair.key << ". Value: " << traverse->pair.val << endl;
            traverse = traverse->next;
        }
        return os;
    }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    Value get( Key key ) {
        for ( Node* x = first; x != null; x = x->next )
            if ( key == x->pair.key )
                return x->pair.val;
        return null;
    }

    // we must do insertion sort here if we want this singly linked list to be ordered
    void put( Key key, Value val ) {

        if ( !isEmpty() ) {
            Node* x = first;
            // this 1st phrase is having some problem
            if ( key <= x->pair.key ) {
                if ( key < x->pair.key )
                    first = new Node( key, val, first ); 
                else
                    x->pair.val = val;
                N++; return;
            }  else if ( N == 1 ) {
                Node* temp = new Node( key, val );
                first->next = temp; 
                N++; return;
            }

            Node* y = first->next;

            while ( y != null ) {  
                if ( key > y->pair.key ) { y = y->next; 
                                           x = x->next; }
                else if ( key == y->pair.key )  {
                    y->pair.val = val; 
                    N++; return;
                }
                else break;
            }
            
            Node* temp = new Node( key, val, y );
            x->next = temp;
        } 
        else first = new Node( key, val, first );
        N++;
    }

    Iterator begin() { return Iterator( first ); }
    Iterator end() { return Iterator( null ); }

};

#endif
