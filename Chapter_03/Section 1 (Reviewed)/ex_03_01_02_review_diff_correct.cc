#include <iostream>
#include <string>
#include <numeric>
#include <vector>
#include "JavaOperations.h"

using namespace std;

#define null NULL
#define String string
#define Integer int

// vector implementation ( memory safe )
template <typename Key, typename Value>
class ST {
private:

    int N = 0;
    vector<Key> keys;
    vector<Value> vals;

    // since these are two separate types vectors, we cannot create a C++ iterator for this, 
    // once we learn to cerate map with only one type ( that's a pair of 2 types ) then we can create the iterator for this

public:

    friend ostream& operator << ( ostream& os, ST<Key, Value> st ) {
        for  ( int i = 0; i < st.size(); i++ )
        os << "Key: " << st.keys[ i ] << ". Value: " << st.vals[ i ] << endl;
        return os;
    }

    ST(): N( 0 ) {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void put( Key key, Value val ) {
        keys.push_back( key );
        vals.push_back( val );
        if ( !isEmpty() ) {
            int i = rank( key );
            if ( keys[ i ] == key ) { vals[ i ] = val; return; }
            for ( int j = N; j > i; j-- ) {
                keys[ j ] = keys[ j-1 ];
                vals[ j ] = vals[ j-1 ];
            }
            keys[ i ] = key; vals[ i ] = val;
        } 
        N++;
    }

    Value get( Key key ) { return ( contains( key )) ? vals[ rank( key )] : null; }

    void erase( Key key ) {
        if ( isEmpty() ) { cout << "underflow\n"; return; }
        if ( !contains[ key ] ) { cout << "key is not in the list\n"; return; }
        int i = rank( key );
        // shift every elements to the left by 1, 
        // overriding the current element found by rank()
        for ( int j = i; j < N; j++ ) {
            keys[ j ] = keys[ j+1 ]; 
            vals[ j ] = vals[ j+1 ];
        }
        // pop out the last elements
        keys.pop_back(); vals.pop_back();
    }

    // checking: "rank( key ) < N" is not neccessary for vector because we build one by one rather than extending the vector with pre-occupied space and pre-init values
    bool contains( Key key ) { return keys[ rank( key )] == key; }

    int size( Key lo, Key hi ) {
        if ( isEmpty() ) return 0;
        return ( contains( hi )) ? rank( hi ) - rank( lo ) + 1 : rank( hi ) - rank( lo );
    }

    // find the largest key that's less or equal to the given key
    Key floor( Key key ) {
        // if the key < smallest of the keys -> there's no value
        if ( key < keys[ 0 ]) return null;
        int i = rank( key );
        return ( keys[ i ] > key ) ? keys[ i-1 ] : keys[ i ];
    }

    // find the smallest key that's larger/equal to given key
    Key ceiling( Key key ) {
        if ( key > keys[ N-1 ]) return null;
        int i = rank( key );
        return ( keys[ i ] < key ) ? keys[ i+1 ] : keys[ i ];
    }
    
    Key select( int k ) { return keys[ k ]; }

    Key min() { return keys[ 0 ]; }
    Key max() { return keys[ N-1 ]; }

    void deleteMin() { erase( min()); }
    void deleteMax() { erase( max()); }

    int rank( Key key ) {
        int lo = 0;
        int hi = N-1;
        while ( lo <= hi ) {
            int mid = lo + ( hi - lo ) / 2;
            if ( key > keys[ mid ]) lo = mid + 1;
            else if ( key < keys[ mid ] ) hi = mid - 1;
            else return mid;
        }
        return lo;
    }
};

int main( int argc, char** argv ) {

    ST<String, Integer> table;

    table.put( "Thinh", 10 );
    table.put( "Khanh", 10 );
    table.put( "Phong", 10 );
    table.put( "Cuong", 10 );

    cout << table.get( "Thinh" ) << endl;
    cout << table << endl;

    return 0;

}
