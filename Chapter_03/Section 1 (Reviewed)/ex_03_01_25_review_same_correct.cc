#include <iostream>
#include <vector>
#include <iomanip>

#define String string
#define Integer int
#define null NULL

using namespace std;

// any operations related to keys will assign the last accessed value to the key
// any operations related to finding will have to check the value of the key in cache first
template <typename Key, typename Value> 
class BinarySearchST {
private:

    vector<Key> keyVec;
    vector<Value> vals;
    int N = 0;
    // the index of the latest accessed
    int keyCache = -1;

public:

    BinarySearchST() = default;

    int size()  { return N; }

    bool isEmpty() { return N == 0; }

    bool contains( Key key ) { 
        if ( !isEmpty() ) {
            // CACHE ADVANTAGE
            if ( keyVec[ keyCache ] == key ) return true;
            //////////////////
            int i = rank( key );
            if ( keyVec[ i ] == key ) { keyCache = i;       // reload the cache if this is the last accessed operation of cache
                                        return true; }
        }
        return false;
    }

    void put( Key key, Value val ) {
        if ( !isEmpty() ) {
            // CACHE ADVANTAGE
            if  ( keyVec[ keyCache ] == key ) { vals[ keyCache ] = val; return; }
            //////////////////
            int i = rank( key );
            if ( keyVec[ i ] == key ) { 
                vals[ i ] = val; 
                keyCache = i;
                return; 
            }
            keyVec.insert( keyVec.begin()+i, key );
            vals.insert( vals.begin()+i, val );
        }
        else {
            keyCache = 0;
            keyVec.push_back( key );
            vals.push_back( val );
        }
        N++;
    }

    Value get( Key key ) {
        if ( isEmpty() ) throw out_of_range( "get(): The Table is empty!" );
        // CACHE ADVANTAGE
        if ( key == keyVec[ keyCache ] ) return vals[ keyCache ];
        //////////////////
        int i = rank( key );
        if ( keyVec[ i ] == key ) {
            keyCache = i;
            return vals[ i ];
        }
        throw runtime_error( "get(): No such key!" );
    } 

    // this will auto-throw an exception if the key is not in the vector
    Key select( int index ) { 
        return ( index > size() ) ? throw out_of_range( "select(): Index out of bound!" ) : keyVec[ index ]; 
    }

    void erase( Key key ) {
        if ( isEmpty() ) throw out_of_range( "erase(): The Table is empty!" );
        // CACHE ADVANTAGE
        if ( keyVec[ keyCache ] == key ) {
            keyVec.erase( keyVec.begin()+keyCache );
            vals.erase( vals.begin()+keyCache );
            return;
        }
        //////////////////
        int i = rank( key );
        if ( keyVec[ i ] == key )  {
            keyVec.erase( keyVec.begin()+i );
            vals.erase( vals.begin()+i );
        }
        else throw runtime_error( "erase(): NO SUCH KEY!'" );
    }

    Key max() { return keyVec[ N-1 ]; }
    Key min() { return keyVec[ 0 ]; }

    void eraseMax() { erase( max() ); }
    void eraseMin() { erase( min() ); }

    vector<Key>& keys() { return keyVec; }

    vector<Key> keys( Key lo, Key hi ) {  
        vector<Key> keyVecCopy( keyVec.begin()+rank( lo ), keyVec.begin+rank( hi ) );
        return keyVecCopy;
    }

    int rank( Key key ) {
        if ( isEmpty() ) throw out_of_range( "rank(): The Table is empty!" );
        // CACHE ADVANTAGE
        if ( key == keyVec[ keyCache ] ) return keyCache;
        //////////////////
        int lo = 0, hi = size()-1;
        while ( lo <= hi ) {
            int mid = lo + ( hi - lo ) / 2;
            if ( key > keyVec[ mid ] ) lo = mid + 1;
            else if ( key < keyVec[ mid ] ) hi = mid - 1;
            else return mid;
        }
        return lo;
    }

};

int main( int argc, char** argv ) {
    
    int minlen = stoi( argv[ 1 ]);
    BinarySearchST<String, Integer> st;

    int processedCount = 0;
    string latestWord = "";
    string word = "";

    while ( cin >> word && !cin.eof() ) {
        if ( word.size() >= minlen ) {
            if( !st.contains( word ) ) {
                st.put( word,1 );
                latestWord = word;
            }
            else {
                st.put( word, st.get( word ) + 1 );
                latestWord = word;
            }
        }
        processedCount++;
    }

    String max = "";
    st.put( max, 0 );

    for ( String word : st.keys())
        if ( st.get( word ) > st.get( max ))
            max = word;

    cout << "[ MAX ] Key: \"" << max <<  "\" |  Value: " << st.get( max ) << endl;
    cout << "The number of words processed through input: " << processedCount << endl;
    cout << "The last word got through put() function: \"" << latestWord << "\"" << endl;

    return 0;

}
