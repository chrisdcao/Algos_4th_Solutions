#include <iostream> 
#include <vector>
#include <string>
#include <climits>
#include <numeric>
#include <iomanip>

using namespace std;

#define null NULL
#define String string
#define Integer int

template <typename Key, typename Value> 
class BinarySearchST {
private:

    vector<Key> keyVec;
    vector<Value> vals;
    int N = 0;

public:

    BinarySearchST() = default;

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    bool contains( Key key ) { 
        return ( isEmpty() ) ? false : ( keyVec[ interpolationSearch( key ) ] == key ); 
    }

    void put( Key key, Value val ) {
        if ( !isEmpty() ) {
            int i = interpolationSearch( key );
            if ( keyVec[ i ] == key ) { vals[ i ] = val; return; } 
            keyVec.insert( keyVec.begin()+i+1, key );
            vals.insert( vals.begin()+i+1, val );
        } else {
            keyVec.push_back( key ); vals.push_back( val );
        }
        N++;
    }

    void erase( Key key ) {
        int i = interpolationSearch( key );
        if ( keyVec[ i ] == key ) {
            keyVec.erase( keyVec.begin()+i );
            vals.erase( vals.begin()+i );
        }
        else throw runtime_error( "erase(): NO SUCH KEY" );
    }

    Key min() { return keyVec[ 0 ]; }
    Key max() { return keyVec[ size()-1 ]; }

    void eraseMin() { erase( min() ); }
    void eraseMax() { erase( max() ); }

    // Interpolation search implementation
    // this is actually not being able to find the key in certain cases
    int interpolationSearch( Key key ) {
        int lo = 0, hi = size()-1;
        if ( key < keyVec[ lo ] ) return lo;
        if ( key > keyVec[ hi ]  ) return hi;
        // we assume that the key has to be within the range for this interpolation search to work
        // but for interpolationSearch inside the Symbol Table, we don't return -1 but has to return lo ( the nearest location for insertion ) for the put() function to work. Thus, we have to cut these 2 corner case before getting to this function
        while ( lo <= hi && key >= keyVec[ lo ] && key <= keyVec[ hi ]) {
            if ( keyVec[ hi ] == keyVec[ lo ] ) break;
            int mid = lo + ( (key - keyVec[ lo ]) * ( ( hi - lo ) / (keyVec[ hi ] - keyVec[ lo ]) ) );
            if ( key > keyVec[ mid ] ) lo = mid + 1;
            else if ( key < keyVec[ mid ] ) hi = mid - 1;
            else return mid;
        }
        return lo;
    }

    Value& get( Key key ) {  
        return vals[ interpolationSearch( key )];
    }

    vector<Key>& keys() { return keyVec; }

};

// this exercise is assuming that the Key type supports arithmetic operations
int main( int argc, char** argv ) {
    
    int minVal = stoi( argv[ 1 ]);
    BinarySearchST<Integer, Integer> st;

    int number;

    while ( cin >> number && !cin.eof() ) {
        if ( number >= minVal ) {
            if( !st.contains( number ) ) st.put( number,1 );
            else                         st.put( number, st.get( number ) + 1 );
        }
    }

    for ( const auto& s : st.keys() )
        cout << "Key: " << s << ". Frequency: " << st.get( s ) << endl;

    return 0;

}
