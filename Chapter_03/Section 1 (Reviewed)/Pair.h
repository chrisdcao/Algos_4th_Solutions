#ifndef PAIR_H
#define PAIR_H

#include <iostream>
#include <string>

using namespace std;

template <typename Key, typename Value>
class PairKV {
    template <typename k, typename v> friend class BST;
    template <typename k, typename v> friend class SequentialSearchST;
private:
    Key key;
    Value val;
public:
    friend ostream& operator << ( ostream& os, PairKV& pair ) {
        os << "Key: " << pair.key << ". Value: " << pair.val;
        return os;
    }
    PairKV() = default;
    virtual ~PairKV() {}
    PairKV( PairKV* pairKV ): key( pairKV->key ), val( pairKV->val ) { }
    PairKV( PairKV&& pairKV ): key( pairKV.key ), val( pairKV.val ) { }
    PairKV( PairKV& pairKV ): key( pairKV.key ), val( pairKV.val ) { }
    PairKV( Key& k, Value& v ): key( ( Key ) k ), val( v ) {}
    PairKV( Key&& k, Value&& v ): key( ( Key ) k ), val( v ) {}
    bool operator > ( PairKV& rhs ) { return key > rhs.key; }
    bool operator < ( PairKV& rhs ) { return key < rhs.key; }
    bool operator == ( PairKV& rhs ) { return key == rhs.key; }
    bool operator != ( PairKV& rhs ) { return key != rhs.key; }
    bool operator >= ( PairKV& rhs ) { return key >= rhs.key; }
    bool operator <= ( PairKV& rhs ) { return key <= rhs.key; }
    void operator=( PairKV& rhs ) { key = rhs.key; val = rhs.val; }
    void operator=( PairKV&& rhs ) { key = rhs.key; val = rhs.val; }
};

#endif
