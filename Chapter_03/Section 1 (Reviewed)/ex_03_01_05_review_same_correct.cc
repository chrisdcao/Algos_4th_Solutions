#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include <utility>
#include "SinglyLinkedListQueue.h"
#include <iterator>

#define null NULL
#define String string
#define Integer int
#define SLL SinglyLinkedList

using namespace std;

// this is the implementation of unordered list
template <typename Key, typename Value> 
class UnorderedListST {
private:

    class Node {  
    public:
        Key key;
        Value val;
        Node* next = null;
        Node( Key k, Value v, Node* n ): key( k ), val( v ), next( n ) {}
        Node( Key k, Value v ): key( k ), val( v ), next( null ) {}
        Node() = default;
        void operator=( Node* rhs ) {
            val = rhs->val;
            key = rhs->key;
            next = rhs->next;
        }
    };

    SLL<Key> selectionSort() {
        SLL<Key> sll;
        for ( Node* i = first; i != null; i = i->next  )
            sll.insertSorted( i->key );
        return sll;
    }

    class Iterator {
    private:
        Node* current;
    public:
        Iterator( Node* initLoc ) { current = initLoc; }
        bool operator==( Iterator&& rhs ) { return current == rhs.current; }
        bool operator!=( Iterator&& rhs ) { return current != rhs.current; }
        bool operator==( Iterator& rhs ) { return current == rhs.current; }
        bool operator!=( Iterator& rhs ) { return current != rhs.current; }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        Key operator*() { return this->current->key; }
    };

    Node* first = null;
    Node* lbegin = null;
    Node* lend = null;
    int N = 0;

public:

    friend ostream& operator<< ( ostream& os, UnorderedListST<Key, Value> st ) {
        Node* traverse = st.first;
        while ( traverse != null ) {
            os << "Key: " << traverse->key << ". Value: " << traverse->val << endl;
            traverse = traverse->next;
        }
        return os;
    }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    Value get( Key key ) {
        for ( Node* x = first; x != null; x = x->next )
            if ( key == x->key )
                return x->val;
        return null;
    }

    void put( Key key, Value val ) {
        for ( Node* x = first; x != null; x = x->next ) 
            if ( key == x->key ) {
                x->val = val; return;
            }
        first = new Node( key, val, first );
        N++;
    }

    void erase( Key key ) {
        if ( !isEmpty() ) {
            Node* x = first;
            if ( size() == 1 ) {
                if ( x->key == key ) { 
                    N--;
                    x = null; return; 
                }
                else { cout << "No such key\n"; return; }
            }
            Node* y = first->next;
            while ( y != null ) {
                if ( y->key == key ) break;
                y = y->next;
                x = x->next;
            }
            if ( y == null ) { cout << "No such key\n"; return; }
            x->next = y->next;
            N--;
        } else cout << "No such key" << endl;
    }

    SLL<Key> sortedKeys() { return selectionSort(); }
    UnorderedListST unsortedKeys() { return *this; }

    // since this is the unordered list, we only sort when this function is called
    Iterator begin() { 
        if ( lbegin == null )
            return Iterator( first ); 
        else
            return Iterator( lbegin );
    }
    Iterator end() { 
        if ( lend == null )
            return Iterator( null ); 
        else
            return Iterator( lend->next );
    }

};

int main( int argc, char** argv ) {

    UnorderedListST<String, Integer> stringst;

    stringst.put( "Thinh", 10 );
    stringst.put( "Cuong", 10 );
    stringst.put( "Phong", 10 );
    stringst.put( "Khanh", 10 );
    stringst.put( "Khoa", 10 );
    stringst.put( "Khanh", 8 );
    stringst.put( "Khoa", 7 );

    cout << "Print out the keys in sorted order:\n";
    for ( auto s : stringst.sortedKeys() )
        cout << s << endl;
    cout << endl;

    cout << "Delete Key: \"Thinh\"\n";
    stringst.erase( "Thinh" );

    cout << "Print out the keys in unsorted order:\n";
    for ( auto s : stringst.unsortedKeys() )
        cout << s << endl;
    cout << endl;

    cout << "The original unordered-list:\n" <<  stringst;

    return 0;

}
