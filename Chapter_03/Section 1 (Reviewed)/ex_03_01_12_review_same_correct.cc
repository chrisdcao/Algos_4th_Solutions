#include <iostream>
#include <string>
#include <numeric>
#include <iomanip>
#include <vector>
#include <initializer_list>
#include "Pair.h"
#include "MergeSort.h"

#define String string
#define Integer int
#define null NULL
#define BinarySearchST BST

using namespace std;

template <typename Key, typename Value>
class BinarySearchST {

    using Pair = PairKV<Key, Value>;

private:

    int N = 0;
    vector<Pair> st;
    Pair* start = null; 
    Pair* finish = null;

public:

    BST() = default;

    BST( vector<Pair>& rhs ) { operator=( rhs ); }
    BST( vector<Pair>&& rhs ) { operator=( rhs ); }

    virtual ~BST() {}

    // so this vector argument might not be sorted, we have to sort before taking its member in
    BST& operator=( vector<Pair>& rhs ) {
        Merge<Pair>::sort( rhs );
        if ( !isEmpty() ) st.clear();
        for ( auto& s : rhs ) putPair( s );
        N = rhs.size();
        return *this;
    }

    BST& operator=( vector<Pair>&& rhs ) {
        Merge<Pair>::sort( rhs );
        if ( !isEmpty() ) st.clear();
        for ( auto& s : rhs ) putPair( s );
        N = rhs.size();
        return *this;
    }

    // accessing by index, returning Pair
    Pair& operator[]( int index ) { return st[ index ]; }
    // accessing by Key, retrieving Value
    Value& operator[]( Key key ) { return st[ rank( key )].val; }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    bool contains( Key key ) {  
        if ( isEmpty() ) throw out_of_range( "Underflow" );
        return ( st[ rank( key )].key == key );
    }

    void put( Key key, Value val ) {
        st.push_back( new Pair( key, val ));
        if ( !isEmpty()) {
            int i = rank( key );
            if ( st[ i ].key == key ) {
                st[ i ].val = val; return;
            }
            else {
                for ( int j = N; j > i; j-- )
                    st[ j ] = st[ j-1 ];
                st[ i ] = new Pair( key, val );
            }
        } 
        N++;
    }

    void putPair( Pair& pair ) {  
        Key key = pair.key;
        Value val = pair.val;
        put( key, val );
    }

    void erase( Key key ) {
        int i = rank( key );
        if ( st[ i ].key == key ) {
            st.erase( st.begin()+i );
            N--;
        }
        else throw runtime_error( "No such key" );
    }

    Key min() { return st[ 0 ]; }
    Key max() { return st[ N-1 ]; }

    void eraseMax() { erase( min()); N--; }
    void eraseMin() { erase( max()); N--; }

    vector<Pair>& keys() { return st; }
    vector<Pair>& keys( Key lo, Key hi ) {  
        if ( lo > hi ) throw out_of_range( "inappropriate range" );
        if ( isEmpty() ) throw out_of_range( "underflow" );
        // assign the threshold to this bound
        begin = &st[ 0 ];
        finish = &st[ 0 ];
        for ( int i = 0; i < lo; i++ ) start++;
        for ( int j = 0; j < hi; j++ ) finish++;
        return st;
    }

    int rank( Key key ) {
        if  ( isEmpty() ) throw out_of_range( "Underflow" );
        int lo = 0, hi = size()-1;
        while ( lo <= hi ) {
            int mid = lo + ( hi - lo );
            if ( key > st[ mid ].key ) lo = mid + 1;
            else if ( key < st[ mid ].key ) hi = mid - 1;
            else return mid;
        }
        return lo;
    }

};

#define Pair PairKV

int main( int argc, char** argv ) {

    BST<String, Integer> stringst;
    vector<Pair<String, Integer>> vec;
    vec.push_back( new Pair( String( "thinh" ), 8 ));
    vec.push_back( new Pair( String( "phong" ), 10 ));
    vec.push_back( new Pair( String( "cuong" ), 7 ));
    vec.push_back( new Pair( String( "khanh" ), 2 ));
    vec.push_back( new Pair( String( "khoa" ), 10 ));

    // using the overloaded assignment operator
    stringst = vec;
    // using the constructor
    BST<String, Integer> stringst2( vec );

    for ( auto s : stringst.keys() )
        cout << s << endl;

    cout << endl;

    for ( auto s : stringst2.keys() )
        cout << s << endl;

    return 0;

}
