#include <iostream>
#include <numeric>
#include <iomanip>
#include <vector>

#define null NULL
#define String string
#define Integer int
#define OST OrderedSinglyLinkedListST

using namespace std;

template <typename Key, typename Value>
class OrderedSinglyLinkedListST {
private:

    class Node {
    public:
        Key key;
        Value val;
        Node* next;
        int index = -1;

        Node(Key k, Value v, Node* n): key(k), val(v), next(n) {}
        Node(Key k, Value v): key(k), val(v) {}
        Node() = default; 

        void operator=(Node* rhs) { 
            val = rhs.val;
            key = rhs.key;
            next = rhs.next;
        }
    };

    vector<unique_ptr<Node>> allNodes;

    Node* newNode(Key key, Value val, Node* next) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->next = next;
        return allNodes.back().get();
    }

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        return allNodes.back().get();
    }

    Node* newNode(Key key, Value val) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size()-1) {
            //swapping using index
            allNodes[node->index].swap(allNodes.size()-1);
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    class Iterator {  
    private:
        Node* current;
    public:
        Iterator( Node* initLoc ) { current = initLoc; }
        bool operator==( Iterator&& rhs ) { return current == rhs.current; }
        bool operator==( Iterator& rhs ) { return current == rhs.current; }
        bool operator!=( Iterator&& rhs ) { return current != rhs.current; }
        bool operator!=( Iterator& rhs ) { return current != rhs.current; }
        Iterator& operator++() { current = current->next; return *this;  }
        Key operator*() { return current->key; }
    };

    int N = 0;
    Node* first = null;
    Node* start = null;
    Node* finish = null;

public:

    OST() = default;

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    bool contains( Key key ) {
        Node* x = first;
        while( x != null ) {
            if( x->key == key ) return true;
            x = x->next;
        }
        return false;
    }

    void put( Key key, Value val ) {
        if ( isEmpty() )
            first = newNode( key, val, first );
        else if ( key < first->key ) 
            first = newNode( key, val, first );
        else if ( N == 1 ) {
            if ( key == first->key )
                first->val = val;
            else {
                Node* temp = newNode( key, val );
                first->next = temp;
            }
        } else {
            Node* x = first;
            Node* y = first->next;
            while ( y != null ) {
                if ( key > y->key ) {
                    x = x->next; y = y->next;
                }
                else if ( key == y->key ) {
                    y->val = val; return;
                }
                else {
                    Node* temp = newNode( key, val, y );
                    x->next = temp; break;
                }
            }
        }
        N++;
    }

    void erase( Key key ) {
        if ( isEmpty() ) throw out_of_range( "Undeflow!" );
        if ( N > 1 ) {
            Node* x = first;
            Node* y = first->next;
            while ( y != null ) {
                if ( y->key == key ) break;
                x = x->next; y = y->next;
            }
            ( y != null ) ? x->next = y->next : throw runtime_error( "No such key!" );
        } 
        else ( first->key == key ) ? first = null : throw runtime_error( "No such key!" );
        N--;
    }
    
    Value get( Key key ) {
        if ( isEmpty() ) throw out_of_range( "Underflow!" );
        Node* x = first;
        while ( x != null ) {
            if ( x->key == key ) return x->val;
            x = x->next;
        }
        throw runtime_error( "No such key!" );
    }

    Key min() { return first->key; }

    Key max() { 
        Node* x = first;
        while ( x->next != null ) x = x->next;
        return x->key;
    }

    void eraseMin() { first = first->next; }

    void eraseMax() { 
        Node* x = first;
        while ( x->next != null ) x = x->next;
        x = null;
    }

    OST& keys() { return *this; }

    OST& keys( Key lo, Key hi ) {
        if ( lo > hi ) 
            throw out_of_range( "Inappropriate range!" );
        if ( !contains( lo ) || !contains( hi )) 
            throw runtime_error( "No such key!" );
        start = first;
        while ( start->key != lo ) start = start->next;
        finish = start;
        while ( finish->key != hi ) finish = finish->next;
        return *this;
    }

    Iterator begin() {
        if ( start != null ) return Iterator( start );
        else                 return Iterator( first );
    }

    Iterator end() { return Iterator( finish ); }

};

// Frequency couter client
// keep track of latest call to put(), print the last word inserted 
// and the <number of words> processed in the input stream prior to this insertion
int main( int argc, char** argv ) {
    
    int minlen = stoi( argv[ 1 ]);
    OST<String, Integer> st;

    int processedCount = 0;
    string latestWord = "";
    string word = "";

    while ( cin >> word && !cin.eof() ) {
        if ( word.size() >= minlen ) {
            if( !st.contains( word ) ) {
                st.put( word,1 );
                latestWord = word;
            }
            else {
                st.put( word, st.get( word ) + 1 );
                latestWord = word;
            }
        }
        processedCount++;
    }

    String max = "";
    st.put( max, 0 );

    for ( String word : st.keys())
        if ( st.get( word ) > st.get( max ))
            max = word;

    cout << "[ MAX ] Key: \"" << max <<  "\" |  Value: " << st.get( max ) << endl;
    cout << "The number of words processed through input: " << processedCount << endl;
    cout << "The last word got through put() function: \"" << latestWord << "\"" << endl;

    return 0;

}
