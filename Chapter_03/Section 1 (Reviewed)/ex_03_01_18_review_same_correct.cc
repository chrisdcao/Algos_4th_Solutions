2 cases can happen:

1. The key exists in the list: the rank() method is based on the BinarySearch + the vectors are ordered, thus in this case it will work like normal Binarysearch function and guaranteed to be correct

2. The key DOES NOT exist in the list: BinarySearch will traverse and keep deducing the range until 1 element is left ( that's when the stopping condition is met: lo > hi ), it return the index one-past-endhi-element, meaning it will return one-to-the-right of the place that's the floor of the key, or in other words the smallest-larger key compare to the input -> correct for put() and ceiling() function

=> it either returns the exact position ( if container contains key ) or ceiling ( smallest larger ) key position
