#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <iomanip>
#include "BinarySearchST.h"

using namespace std;

template <typename Key, typename Value>
float avgGPA( BST<Key, Value> table ) {
    float sum = 0.00;
    for ( int i = 0; i < table.size(); i++ )
        sum += table[table[i]];
    return sum / table.size();
}

int main(int argc, char** argv) {

    BST<string, float> grades;
    grades.put("A+", 4.33); 
    grades.put("A", 4.00); 
    grades.put("A-", 3.67);
    grades.put ("B+", 3.33); 
    grades.put("B", 3.00); 
    grades.put("B-", 2.67);
    grades.put("C+", 2.33);
    grades.put("C", 2.00); 
    grades.put("C-", 1.67);
    grades.put("D", 1.00);
    grades.put("F", 0.00);

    for ( int i = 0; i < grades.size(); i++ )
        cout << grades[i] << endl;

    cout << "Average GPA: " << fixed << setprecision( 2 ) << avgGPA( grades ) << endl;

    return 0;
}
