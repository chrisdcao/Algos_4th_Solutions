#ifndef JAVAOPERATIONS_H
#define JAVAOPERATIONS_H

#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <algorithm>
#include <iomanip>

using namespace std;

template <typename T>
int compareBetween(T item1, T item2) 
{
    if (item1 > item2) return 1;
    else if (item1 < item2) return -1;
    else return 0;
}

#endif
