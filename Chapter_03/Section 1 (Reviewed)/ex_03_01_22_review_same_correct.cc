#include <iostream>
#include <vector>
#include <string>
#include <numeric>
#include <algorithm>
#include <iomanip>

#define null NULL
#define String string
#define Integer int

using namespace std;

// unordered array implementation of ST
template <typename Key, typename Value>
class SelfOrganizingArrayST {
private:

    vector<Key> keys;
    vector<Value> vals;
    int N = 0;

public:

    friend ostream& operator << ( ostream& os, SelfOrganizingArrayST st ) {
        for ( int i =0; i < st.size() ; i++ )
            os << "Key: " << st.keys[ i ] << ". Value: " << st.vals[ i ] << endl;
        return os;
    }

    SelfOrganizingArrayST() = default;

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    bool contains( Key key ) { return rank( key ) != -1; }

    void put( Key key, Value val ) {
        keys.push_back( key );
        vals.push_back( val );
        N++;
    }

    Value get( Key key ) {
        for ( int i = 0; i < keys.size(); i++ ) {
            if ( key == keys[ i ]) return vals[ i ];
        }
        return null;
    }

    // whenever a search hit, re-arrange the array
    int rank( Key key ) {
        for ( int i = 0; i < keys.size(); i++ ) {
            if ( key == keys[ i ]) {
                Key tempKey = key; Value tempVal = vals[ i ];
                for ( int j = i; j > 0; j-- ) {
                    keys[ j ] = keys[ j-1 ]; vals[ j ] = vals[ j-1 ];
                }
                keys[ 0 ] = tempKey; vals[ 0 ] = tempVal;
                return i;
            }
        }
        return -1;
    }

    Key min() { 
        Key minKey = keys[ 0 ];
        for ( int i = 0; i < N; i++ )
            if ( keys[ i ] < minKey ) minKey = keys[ i ];
        return ( isEmpty()) ? throw out_of_range( "Underflow" ) : minKey;
    }

    Key max() {  
        Key maxKey = keys[ 0 ];
        for ( int i = 0; i < N; i++ )
            if ( keys[ i ] > maxKey ) maxKey = keys[ i ];
        return ( isEmpty()) ? throw out_of_range( "Underflow" ) : maxKey;
    }

    Key erase( Key key ) {
        int i = rank( key );
        if ( i == -1 ) throw runtime_error( "No such key!" );
        keys.erase( keys.begin()+i );
        vals.erase( vals.begin()+i );
        N--;
    }

    Key eraseMin() { erase( min()); }
    Key eraseMax() { erase( max()); }

};
  
int main( int argc, char** argv ) {

    SelfOrganizingArrayST<String, Integer> stringst;

    stringst.put( "thinh", 2 );
    stringst.put( "khanh", 3 );
    stringst.put( "cuong", 7 );
    stringst.put( "phong", 12 );
    stringst.put( "khoa", 5 );

    // print out for testing purpose
    cout << "original list ( unordered-implementation ):\n" << stringst << endl;

    stringst.rank( "phong" );
    cout << "list after rank1:\n" << stringst << endl;
    stringst.rank( "thinh" );
    cout << "list after rank2:\n" << stringst;

    return 0;

}
