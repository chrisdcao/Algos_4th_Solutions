#include <iostream>
#include <iomanip>
#include <climits>
#include <numeric>

using namespace std;

template <typename T>
bool isSorted( vector<T>& vec ) {
    for ( int i = 1; i < vec.size(); i++ )
        if ( vec[ i ] < vec[ i-1 ] )
            return false;
    return true;
}
