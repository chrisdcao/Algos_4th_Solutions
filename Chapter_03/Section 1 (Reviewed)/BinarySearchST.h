#include <iostream>
#include <vector>
#include <iomanip>

#define String string
#define Integer int
#define null NULL
#define BinarySearchST BST

using namespace std;

template <typename Key, typename Value> 
class BinarySearchST {
private:

    vector<Key> keyVec;
    vector<Value> vals;
    int N = 0;

public:

    // this printing method is for debugging purpose
    friend ostream& operator << ( ostream& os, BinarySearchST st ) {
        for ( int i = 0; i < st.size(); i++ ) 
            os << "Key: " << st.keyVec[ i ] << ". Value: " << st.vals[ i ] << endl;
        return os;
    }

    BinarySearchST() = default;

    bool contains( Key key ) { return ( isEmpty()) ? false : ( keyVec[ rank( key )] == key ); }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void put( Key key, Value val ) {
        if ( !isEmpty()) {
            int i = rank( key );
            if ( keyVec[ i ] == key ) { vals[ i ] = val; return; }
            else {
                keyVec.push_back( key ); vals.push_back( val );
                for ( int j = N; j > i; j-- ) {
                    keyVec[ j ] = keyVec[ j-1 ]; vals[ j ] = vals[ j-1 ];
                }
                keyVec[ i ] = key; vals[ i ] = val;
            }
        } 
        else { keyVec.push_back( key ); 
               vals.push_back( val ); }
        N++;
    }

    void erase( Key key ) {
        int i = rank( key );
        if ( keyVec[ i ] == key ) {
            keyVec.erase( keyVec.begin()+i );
            vals.erase( vals.begin()+i );
            N--;
        }
        else throw runtime_error( "No such key" );
    }

    Key ceiling( Key key ) { 
        if ( key > max()) {
            cout << "No larger key found! Returning maximum:\n";
            return max();
        }
        return keys[ rank( key )];
    }

    Key floor( Key key ) {
        if ( key < min() ) {
            cout << "No smaller keys found! Returning minimum:\n";
            return min();
        }
        int i = rank( key );
        return ( keyVec[ i ] <= key ) ? keyVec[ i ] : keyVec[ i-1 ];
    }

    Key min() { return keyVec[ 0 ]; }
    Key max() { return keyVec[ N-1 ]; }

    void eraseMin() { erase( min()); }
    void eraseMax() { erase( max()); }

    int rank( Key key ) {
        if ( isEmpty() ) throw out_of_range( "Underflow" );
        int lo = 0;
        int hi = size() - 1;
        while ( lo <= hi ) {
            int mid = lo + ( hi - lo ) / 2;
            if ( key < keyVec[ mid ] ) hi = mid - 1;
            else if ( key > keyVec[ mid ] ) lo = mid + 1;
            else return mid;
        }
        return lo;
    }

    Value get( Key key ) { 
        return ( contains( key )) ? vals[ rank( key )] : throw runtime_error( "No such key!" ); 
    }

    Value& operator[] ( Key key ) { return vals[ rank( key )]; }

    Key& operator[] (int k) { return keyVec[k]; }

    vector<Key>& keys() { return keyVec; }

};
