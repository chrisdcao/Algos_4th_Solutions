#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <numeric>
#include "OrderedSinglyListST.h" 

using namespace std;

class Time {
private:

    int hour;
    int minutes;
    int seconds;

public:

    friend ostream& operator << ( ostream& os, Time& time ) {
        os << setfill( '0' ) << setw( 2 ) << time.hour << ":" 
           << setfill( '0' ) << setw( 2 ) << time.minutes << ":" 
           << setfill( '0' ) << setw( 2 ) << time.seconds;
        return os;
    }

    Time( String input ) {
        int rbegin = input.find_first_of( ":" );
        int lend = input.rfind( ":" );
        hour = stoi( input.substr( 0, rbegin ));
        minutes = stoi( input.substr( rbegin+1, lend-( rbegin+1 )));
        seconds = stoi( input.substr( lend+1, input.size()-lend ));
    }

    Time( const char* in ) {
        string input = in;
        int rbegin = input.find_first_of( ":" );
        int lend = input.rfind( ":" );
        hour = stoi( input.substr( 0, rbegin ));
        minutes = stoi( input.substr( rbegin+1, lend-( rbegin+1 )));
        seconds = stoi( input.substr( lend+1, input.size()-lend ));
    }

    bool operator > ( Time& rhs ) {
        if ( hour == rhs.hour ) {
            if ( minutes == rhs.minutes ) {
                if ( seconds == rhs.seconds ) return false;
                else if ( seconds < rhs.seconds ) return false;
                else return true;
            } 
            else if ( minutes < rhs.minutes ) return false;
            else return true;
        } 
        else if ( hour < rhs.hour ) return false;
        else return true;
    }

    bool operator == ( Time& rhs )  {
        return hour==rhs.hour && minutes==rhs.minutes && seconds==rhs.seconds;
    }
    
    bool operator != ( Time& rhs ) { return !operator==( rhs ); }

    bool operator < ( Time& rhs ) { return !operator==( rhs ) && !operator>( rhs ); }

    bool operator >= ( Time& rhs ) { return operator==( rhs ) || operator>( rhs ); }

    bool operator <= ( Time& rhs ) { return operator==( rhs ) || operator<( rhs ); }

};


// Event adt has similar structure to pair<>, containing the key( which is time ) and val( which is place )
// but since the OrderedSinglyList I built also accepts 2 different values for constructor, this class doesn't need to be used
class Event {
private:

    Time time;
    String place;

public:

    friend ostream& operator << ( ostream& os, Event event ) {
        os << event.time << " " << event.place;
        return os;
    }

    Event( Time t, String p ): time( t ), place( p ) {}

    bool operator < ( Event rhs ) { return time < rhs.time; }
    bool operator > ( Event rhs ) { return time > rhs.time; }
    bool operator == ( Event rhs ) { return time == rhs.time; }
    bool operator != ( Event rhs ) { return !( time == rhs.time ); }
    bool operator >= ( Event rhs ) { return time >= rhs.time; }
    bool operator <= ( Event rhs ) { return time <= rhs.time; }

};

int main( int argc, char** argv ) {

    OrderedListST<Time, String> event;

    event.put( "09:00:00", "Chicago" );
    event.put( "09:19:32", "Chicago" );
    event.put( "09:35:21", "Chicago" );
    event.put( "09:01:10", "Houston" );
    event.put( "09:00:59", "Chicago" );

    // cout << event;
    for ( auto s : event )
        cout << s << endl;

    return 0;

}
