#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include <utility>
#include <iterator>

#define null NULL
#define String string
#define Integer int

using namespace std;

// this is the implementation of unordered list
template <typename Key, typename Value> 
class OrderedListST {
private:

    class Node {
    public:
        Key key;
        Value val;
        Node* next;
        int index = -1;

        Node(Key k, Value v, Node* n): key(k), val(v), next(n) {}
        Node(Key k, Value v): key(k), val(v) {}
        Node() = default; 

        void operator=(Node* rhs) { 
            val = rhs.val;
            key = rhs.key;
            next = rhs.next;
        }
    };

    vector<unique_ptr<Node>> allNodes;

    Node* newNode(Key key, Value val, Node* next) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->next = next;
        return allNodes.back().get();
    }

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        return allNodes.back().get();
    }

    Node* newNode(Key key, Value val) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size()-1) {
            //swapping using index
            allNodes[node->index].swap(allNodes.size()-1);
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    class Iterator {
    private:
        Node* current;
    public:
        Iterator( Node* initLoc ) { current = initLoc; }
        bool operator==( Iterator&& rhs ) { return current == rhs.current; }
        bool operator!=( Iterator&& rhs ) { return current != rhs.current; }
        bool operator==( Iterator& rhs ) { return current == rhs.current; }
        bool operator!=( Iterator& rhs ) { return current != rhs.current; }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        Key operator*() { return this->current->key; }
    };

    Node* first = null;
    Node* lbegin = null;
    Node* lend = null;
    int N = 0;

public:

    friend ostream& operator<< ( ostream& os, OrderedListST<Key, Value> st ) {
        Node* traverse = st.first;
        while ( traverse != null ) {
            os << "Key: " << traverse->key << ". Value: " << traverse->val << endl;
            traverse = traverse->next;
        }
        return os;
    }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    Value get( Key key ) {
        for ( Node* x = first; x != null; x = x->next )
            if ( key == x->key )
                return x->val;
        return null;
    }

    // we must do insertion sort here if we want this singly linked list to be ordered
    void put( Key key, Value val ) {

        if ( !isEmpty() ) {
            Node* x = first;
            // this 1st phrase is having some problem
            if ( key <= x->key ) {
                if ( key < x->key )
                    first = newNode( key, val, first ); 
                else
                    x->val = val;
                N++; return;
            }  else if ( N == 1 ) {
                Node* temp = newNode( key, val );
                first->next = temp; 
                N++; return;
            }

            Node* y = first->next;

            while ( y != null ) {  
                if ( key > y->key ) { y = y->next; 
                                           x = x->next; }
                else if ( key == y->key )  {
                    y->val = val; return;
                }
                else break;
            }
            
            Node* temp = newNode( key, val, y );
            x->next = temp;
        } 
        else first = newNode( key, val, first );
        N++;
    }

    // iterate through the range only
    OrderedListST& keys( Key lo, Key hi ) {
        if ( lo > hi || isEmpty() ) throw out_of_range( "Out of range" );

        lbegin = first;
        lend = first;

        while ( lbegin->key != lo ) lbegin = lbegin->next;
        while ( lend->key != hi ) lend = lend->next;
        return *this;
    }

    OrderedListST& keys() { return *this; }

    Iterator begin() { 
        if ( lbegin == null )
            return Iterator( first ); 
        else
            return Iterator( lbegin );
    }
    Iterator end() { 
        if ( lend == null )
            return Iterator( null ); 
        else
            return Iterator( lend->next );
    }

};


int main( int argc, char** argv ) {

    OrderedListST<String, Integer> stringst;

    stringst.put( "Thinh", 10 );
    stringst.put( "Cuong", 10 );
    stringst.put( "Phong", 10 );
    stringst.put( "Khanh", 10 );
    stringst.put( "Khoa", 10 );
    stringst.put( "Khanh", 8 );
    stringst.put( "Khoa", 7 );

    cout << "Print out the whole list:\n";
    for ( auto s : stringst.keys() )
        cout << s << endl;
    cout << endl;

    cout << "Print out in range:\n";
    for ( auto s : stringst.keys( "Cuong", "Khoa" ) )
        cout << s << endl;

    return 0;

}
