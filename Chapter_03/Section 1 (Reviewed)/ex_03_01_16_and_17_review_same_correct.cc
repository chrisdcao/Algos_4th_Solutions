#include <iostream>
#include <vector>
#include <iomanip>

#define String string
#define Integer int
#define null NULL
#define BinarySearchST BST

using namespace std;

template <typename Key, typename Value> 
class BinarySearchST {
private:

    vector<Key> keyVec;
    vector<Value> vals;
    int N = 0;

public:

    // this printing method is for debugging purpose
    friend ostream& operator << ( ostream& os, BinarySearchST st ) {
        for ( int i = 0; i < st.size(); i++ ) 
            os << "Key: " << st.keyVec[ i ] << ". Value: " << st.vals[ i ] << endl;
        return os;
    }

    BinarySearchST() = default;

    bool contains( Key key ) { return ( isEmpty()) ? false : ( keyVec[ rank( key )] == key ); }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void put( Key key, Value val ) {
        if ( !isEmpty()) {
            int i = rank( key );
            if ( keyVec[ i ] == key ) { vals[ i ] = val; return; }
            else {
                keyVec.push_back( key ); vals.push_back( val );
                for ( int j = N; j > i; j-- ) {
                    keyVec[ j ] = keyVec[ j-1 ]; vals[ j ] = vals[ j-1 ];
                }
                keyVec[ i ] = key; vals[ i ] = val;
            }
        } 
        else { keyVec.push_back( key ); 
               vals.push_back( val ); }
        N++;
    }
    // ex_03_01_16
    void erase( Key key ) {
        int i = rank( key );
        if ( keyVec[ i ] == key ) {
            keyVec.erase( keyVec.begin()+i );
            vals.erase( vals.begin()+i );
            N--;
        }
        else throw runtime_error( "No such key" );
    }

    Key ceiling( Key key ) { 
        if ( key > max())
            throw runtime_error("ceiling(): no key found!");
        return keyVec[ rank( key )];
    }
    // ex_03_01_17
    // floor: return the largest key that's <= key
    Key floor( Key key ) {
        int i = rank( key );
        // the 'i > N-1' is in case of rank exhaust on the right side, then when lo == hi we will execute on last increment of mid (where mid is now already at hi), resulting in current lo = hi + 1
        if ( key < min() || i > N-1 )
            throw runtime_error("floor(): no such key");
        return ( keyVec[ i ] <= key ) ? keyVec[ i ] : keyVec[ i-1 ];
    }
    
    Key min() { return keyVec[ 0 ]; }
    Key max() { return keyVec[ N-1 ]; }

    void eraseMin() { erase( min()); }
    void eraseMax() { erase( max()); }

    int rank( Key key ) {
        if ( isEmpty() ) throw out_of_range( "Underflow" );
        int lo = 0;
        int hi = size() - 1;
        while ( lo <= hi ) {
            int mid = lo + ( hi - lo ) / 2;
            if ( key < keyVec[ mid ] ) hi = mid - 1;
            else if ( key > keyVec[ mid ] ) lo = mid + 1;
            else return mid;
        }
        return lo;
    }

    Value get( Key key ) { 
        return ( contains( key )) ? vals[ rank( key )] : throw runtime_error( "No such key!" ); 
    }

    Value& operator[] ( Key key ) { return vals[ rank( key )]; }

    vector<Key>& keys() { return keyVec; }

};

int main( int argc, char** argv ) {

    // BinarySearchST<String, Integer> stringst;

    // stringst.put( "thinh", 2 );
    // stringst.put( "khanh", 3 );
    // stringst.put( "cuong", 7 );
    // stringst.put( "phong", 12 );
    // stringst.put( "khoa", 5 );

    // // print out for testing purpose
    // cout << stringst << endl;

    // stringst.erase( "thinh" );

    // cout << "list after delete key \"thinh\":\n";
    // // print out for testing purpose
    // cout << stringst << endl;

    // cout << "Floor: " << stringst.floor( "linh" ) << endl;
    // cout << "Expected: " << "khoa" << endl << endl;
    // cout << "Floor: " << stringst.floor( "khanh" ) << endl;
    // cout << "Expected: " << "khanh" << endl;

    // return 0;

    BinarySearchST<Integer, String> intst;
    intst.put(5, "thinh");
    intst.put(10, "khanh");
    intst.put(15, "khoa");
    intst.put(20, "cuong");
    intst.put(25, "phong");
    intst.put(30, "kien");

    // print out for testing purpose
    cout << intst << endl;

    // cout << "Floor: " << intst.floor( 60 ) << endl;
    // cout << "Expected: " << 30 << endl;
    // cout << "Floor: " << intst.floor( 0 ) << endl;
    // cout << "Expected: None, since the smallest key (5) is not <= 0" << endl << endl;

    cout << "Ceiling: " << intst.ceiling( 60 ) << endl;
    cout << "Expected: None, since the largest key (30) is not >= 60" << endl;
    cout << "Ceiling: " << intst.ceiling( 0 ) << endl;
    cout << "Expected: " << 5 << endl << endl;

    return 0;
}
