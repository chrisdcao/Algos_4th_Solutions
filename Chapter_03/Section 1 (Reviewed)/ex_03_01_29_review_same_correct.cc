#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <iomanip>

using namespace std;
 
template <typename Key, typename Value> 
class BinarySearchST {
private:

    vector<Key> keyVec;
    vector<Value> vals;

    int N = 0;

public:

    BinarySearchST() = default;

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    // the smallest larger
    Key ceiling( Key key ) {  return keyVec[ rank( key ) ]; }
    // the largest smaller or equal
    Key floor( Key key ) { 
        return ( keyVec[ rank( key ) ] == key ) ? key : keyVec[ rank( key ) - 1 ]; 
    }

    void put( Key key, Value val ) {
        if ( !isEmpty() ) {
            int i = rank( key );
            if ( keyVec[ i ] == key ) {
                vals[ i ] = val; 
                return;
            }
            else {
                keyVec.insert( keyVec.begin()+i, key );
                vals.insert( vals.begin()+i, val );
            }
        } 
        else {
            keyVec.push_back( key ); 
            vals.push_back( val );
        }
        N++;
    }

    void erase( Key key ) {
        int i = rank( key );
        if ( keyVec[ i ] == key ) {
            keyVec.erase( keyVec.begin()+i );
            vals.erase( vals.begin()+i );
            N--;
        }
        else throw runtime_error( "erase(): There's no such key!" );
    }
    // no need to define throw for this because we will have vector throw in stock
    Key select( int index ) { return keyVec[ index ]; }
    Value get( Key key ) {
        int i = rank( key );
        return ( keyVec[ i ] == key ) ? vals[ i ] : throw runtime_error( "get(): No such key!" );
    }

    Key min() { return keyVec[ 0 ]; }
    Key max()  { return keyVec[ N-1 ]; }

    void eraseMin() { erase( min() ); }
    void eraseMax()  { erase( max() ); }

    int rank( Key key ) {
        if ( isEmpty() ) throw out_of_range( "rank(): Table is Empty!" );
        int lo = 0; int hi = size()-1;
        while ( lo <= hi ) {
            int mid = lo + ( hi - lo ) / 2;
            if ( key < keyVec[ mid ] ) hi = mid - 1;
            else if ( key > keyVec[ mid ] ) lo = mid + 1;
            else return mid;
        }
        return lo;
    }

    vector<Key>& keys() { return keyVec; }

};

// test client
int main( int argc, char** argv ) {

    BinarySearchST<string, int> stringst;
    stringst.put( "thinh", 10 );
    stringst.put( "cuong", 11 );
    stringst.put( "phong", 12 );
    stringst.put( "khanh", 21 );
    stringst.put( "khoa", 123 );

    cout << setw( 42 ) << left << "The original list: ";
    for ( auto s : stringst.keys() )
        cout << s << " ";
    cout << endl;

    cout << setw( 42 ) << left << "min() and max() respectively: " << "( " << stringst.min() << ", " << stringst.max() << " )" << endl;

    stringst.eraseMax();
    stringst.eraseMin();
    
    cout << setw( 42 ) << left << "The list after erase both min and max:";
    for ( auto s : stringst.keys() )
        cout << s << " ";
    cout << endl;

    cout << setw( 42 ) << left << "The Value at key <Khoa>: " << stringst.get( "khoa" ) << endl;
    cout << setw( 42 ) << left << "The Key at index[ 1 ]: " << stringst.select( 1 ) << endl;

    cout << setw( 42 ) << left << "The largest smaller of \"linh\": " << stringst.floor( "linh" ) << endl;
    cout << setw( 42 ) << left << "The smallest larger of \"linh\": " << stringst.ceiling( "linh" ) << endl;

    return 0;

}
