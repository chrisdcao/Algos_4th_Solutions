#ifndef SINGLYLINKEDLIST_H
#define SINGLYLINKEDLIST_H

#include <iostream>
#include <sstream>
#include <memory>
#include <algorithm>
#include <numeric>
#include <random>
#include <vector>

#define null NULL

using namespace std;

template <typename T>
class SinglyLinkedList {
private:   

    class Node {
    public:
        T item;
        Node* next = NULL;
        int index = -1;
        Node( T item ): item( item ), next( NULL ) {}
        Node( T item, Node* next ): item( item ), next( next ) {}
    };

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size()-1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;

    Node* head;
    Node* tail;
    int N;

    class Iterator {
        Node* current;
    public:
        Iterator( Node* initLoc ) { current = initLoc; }
        bool operator==( Iterator&& rhs ) { return current == rhs.current; }
        bool operator!=( Iterator&& rhs ) { return current != rhs.current; }
        bool operator==( Iterator& rhs ) { return current == rhs.current; }
        bool operator!=( Iterator& rhs ) { return current != rhs.current; }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        T operator*() { return current->item; }
    };

public:
    
    SinglyLinkedList(): head(NULL), tail(NULL), N(0) {}

    Node* getHead() { return head; }

    Node* getTail() { return tail; }

    friend ostream& operator << (ostream& os, SinglyLinkedList& list) {
        Node* traverse = list.head;
        while (traverse != NULL) {
            os << traverse->item << " ";
            traverse = traverse->next;
        }
        return os;
    }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insertAtBegin(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->next = head;
        head = temp;
    }

    /* in this Queue-like implementation of linked-list
     * insert acts like insertAtEnd */
    void insert(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->next = NULL;
        if (N == 0) 
            head = tail = temp;
        else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }

    void insertSorted( T item ) {

        if ( !isEmpty() ) {
            Node* x = head;
            // this 1st phrase is having some problem
            if ( item <= x->item ) {
                head = new Node( item, head ); 
                N++; return;
            }  else if ( N == 1 ) {
                Node* temp = new Node( item );
                head->next = temp; 
                N++; return;
            }

            Node* y = head->next;

            while ( y != null ) {  
                if ( item > y->item ) { y = y->next; 
                                      x = x->next; }
                else break;
            }
            
            Node* temp = new Node( item, y );
            x->next = temp;
        } 
        else head = new Node( item, head );
        N++;
    }

    // iterate through the range only

    Node* reverseIteratively(Node* head) {
        if (N <= 1) return head;
        Node* current = head;
        Node* next = head->next;
        Node* prev = NULL;
        while (next != NULL) {
            current->next = prev;
            prev = current;
            current = next;
            next = next->next;
        }
        current->next = prev;
        return current;
    }

    Node* reverseRecursively(Node* head) {
        if (head == NULL) return NULL;
        if (head->next == NULL) return head;
        Node* second = head->next;
        Node* newHead = reverseRecursively(second);
        second->next = head;
        head->next = NULL;
        return newHead;
    }

    void reverse(bool isRecursive = true) {
        if (isRecursive) head = reverseRecursively(head);
        else            head = reverseIteratively(head);
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

    Iterator begin() { return Iterator( head ); }
    Iterator end() { return Iterator( null ); }

};

#endif
