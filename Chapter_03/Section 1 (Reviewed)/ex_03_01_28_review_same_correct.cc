#include <iostream>
#include <vector>
#include <numeric>
#include <iomanip>

using namespace std;

#define null NULL
#define String string
#define Integer int

template <typename Key, typename Value>
class BinarySearchST {
private:

    vector<Key> keyVec;
    vector<Value> vals;
    int N = 0;

public:

    BinarySearchST() = default;

    virtual ~BinarySearchST() {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    // THIS IS THE REQUIREMENT FROM THE EXERCISE
    void put( Key key, Value val ) {
        // if the list isEmpty() <or> key > all keyVec in the list
        // then we just push back and return. Total cost: ~ O( 1 )
        keyVec.push_back( key ); vals.push_back( val );
        // else insert like normal BinarySearchST. Total cost: ~ O( N )
        if ( !isEmpty() && key <= keyVec[ N-1 ]) {  
            int i = rank( key );
            if ( keyVec[ i ] == key ) { vals[ i ] = val; return; }
            else {
                for ( int j = N; j > i; j-- ) {
                    keyVec[ j ] = keyVec[ j-1 ]; vals[ j ] = vals[ j-1 ];
                }
                keyVec[ i ] = key; vals[ i ] = val;
            }
        }
        N++;
    }

    void erase( Key key ) {
        int i = rank( key );
        if ( keys[ i ] == key ) {
            keyVec.erase( keyVec.begin()+i );
            vals.erase( vals.begin()+i );
        } 
        else throw runtime_error( "No such key" );
    }

    Key min() { return keys[ 0 ]; }
    Key max() { return keys[ N-1 ]; }

    void eraseMax() { erase( max()); }
    void eraseMin() { erase( min()); }

    int rank( Key key ) {
        if ( isEmpty() ) throw out_of_range( "Underflow" );
        int lo = 0;
        int hi = size()-1;
        while ( lo <= hi ) {
            int mid = lo + ( hi - lo ) / 2;
            if ( key > keyVec[ mid ] ) lo = mid + 1;
            else if ( key < keyVec[ mid ] ) hi = mid - 1;
            else return mid;
        }
        return lo;
    }
    vector<Key>& keys() { return keyVec; }

    vector<Key> keys( int lo, int hi ) { 
        int start = rank( lo );
        int finish = rank( hi );
        vector<Key> keyVecCopy( keyVec.begin()+start, keyVec.begin()+finish );
        return keyVecCopy;
    }

};

int main( int argc, char** argv ) {

    BinarySearchST<String, Integer> stringst;
    stringst.put( "thinh", 8 );
    stringst.put( "phong", 10 );
    stringst.put( "cuong", 7 );
    stringst.put( "khanh", 2 );
    stringst.put( "khoa", 10 );

    for ( auto s : stringst.keys())
        cout << s << endl;
    
    return 0;

}

