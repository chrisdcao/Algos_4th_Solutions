#include <iostream>
#include <vector>
#include <string>
#include <numeric>
#include <iomanip>
#include "Certification.h"

#define null NULL
#define String string
#define Integer int

using namespace std;

template <typename Key, typename Value>
class BinarySearchST {
private:

    vector<Key> keyVec;
    vector<Value> vals;
    int N = 0;

public:

    BinarySearchST() = default;

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    bool contains( Key key ) { return keyVec[ rank( key ) ] == key; }

    void put( Key key, Value val ) {
        ///// TEST /////
        // test integrity
        cout << ( ( isSorted( keyVec ) ) ? "array is still sorted" : "array is not sorted" ) << endl;
        // test data variant
        for ( int i = 0; i < size(); i++ )
            cout << ( ( i == rank( select( i ) ) ) ? "Passed" : "Fail" ) << endl;
        ////////////////
        if ( !isEmpty() ) {
            int i = rank( key );
            if ( keyVec[ i ] == key ) { vals[ i ] = val; return; }
            keyVec.insert( keyVec.begin()+i, key );
            vals.insert( vals.begin()+i, val ); 
        } 
        else {
            keyVec.push_back( key ); 
            vals.push_back( val );
        }
        N++;
    }

    Key select( int index ) {
        if ( index < 0 ) throw logic_error( "select(): index cannot be negative!" );
        if ( index > size()-1 ) throw out_of_range( "select(): index out of bound!" );
        return keyVec[ index ];
    }

    Key min() { return keyVec[ 0 ]; }
    Key max()  { return keyVec[ N-1 ]; }

    Value get( Key key ) { return ( contains( key ) ) ? vals[ rank( key ) ] 
                                                      : throw runtime_error( "get(): No such key!" ); }

    void erase( Key key ) {
        int i = rank( key );
        ///// TEST /////
        // test integrity
        cout << ( ( isSorted( keyVec ) ) ? "array is still sorted" : "array is not sorted" ) << endl;
        // test data variant
        for ( int j = 0; j < size(); j++ )
            cout << ( ( j == rank( select( j ) ) ) ? "Passed" : "Fail" ) << endl;
        ////////////////
        if ( keyVec[ i ] == key ) {
            keyVec.erase( keyVec.begin()+i );
            vals.erase( vals.begin()+i );
            N--;
        }
        else throw runtime_error( "erase(): No such key!" );
    }

    void eraseMax() { erase( max() ); }
    void eraseMin() { erase( min() ); }

    int rank( Key key ) {
        if ( isEmpty() ) throw out_of_range( "rank(): Empty Table!" );
        int lo = 0, hi = size()-1;
        while ( lo <= hi ) {
            int mid = lo + ( hi - lo ) / 2;
            if ( key > keyVec[ mid ] ) lo = mid + 1;
            else if ( key < keyVec[ mid ] ) hi = mid - 1;
            else return mid;
        }
        return lo;
    }

    vector<Key>& keys() { return keyVec; }

    vector<Key> keys( Key lo, Key hi ) {
        if ( lo > hi ) throw logic_error( "keys(): lo > hi" );
        vector<Key> keyVecCopy( keyVec.begin()+rank( lo ), keyVec.begin()+rank( hi ) );
        return keyVecCopy;
    }

};

int main( int argc, char** argv ) {

    BinarySearchST<String, Integer> stringst;

    stringst.put( "Thinh", 11 );
    stringst.put( "Cuong", 12 );
    stringst.put( "Khoa", 13 );
    stringst.put( "Khanh", 20 );
    stringst.put( "Phong", 40 );
    stringst.put( "Kin", 1000 );

    stringst.erase( "Thinh");
    stringst.erase( "Khanh" );
    stringst.erase( "Phong" );
    stringst.erase( "Kin" );
    stringst.eraseMax();
    stringst.eraseMin();

    return 0;

}
