#include "BinarySearchST.h"
#include "Queue.h"

#define null NULL
#define String string
#define Integer int

using namespace std;

// Frequency Counter
int main( int argc, char** argv ) {
    
    int minlen = stoi( argv[ 1 ]);
    BinarySearchST<String, Integer> st;

    string word = "";

    int current_max_freq = 1;
    Queue<string> max_words;

    while ( cin >> word && !cin.eof() ) {
        if ( word.size() >= minlen ) {
            if( !st.contains( word ) ) {
                st.put( word,1 );
            }
            else {
                st.put( word, st[word] + 1 );
                // we'll just find the max frequency right from here
                current_max_freq = max(current_max_freq, st[word]);
            }
        }
    }

    for (auto word : st.keys())
        if (st[word] == current_max_freq)
            max_words.enqueue(word);

    cout << "Current max frequency for words length > " << minlen << " is: " << current_max_freq << endl;
    cout << "The word(s): ";
    while (!max_words.isEmpty())
        cout << "<" << max_words.dequeue() << ">" << ((max_words.size() == 0) ? "\n" : " | ");

    return 0;

}
