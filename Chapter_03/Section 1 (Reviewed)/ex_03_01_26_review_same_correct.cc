#include <iostream>
#include <string>
#include <numeric>
#include <fstream>
#include <iomanip>
#include <vector>
#include "Pair.h"

using namespace std;

#define String string
#define Integer int
#define null NULL
#define SSST SequentialSearchST 

using namespace std;

// I use unordered array implemetation, since I don't know how the client want the table sorted
template <typename Key, typename Value>
class SequentialSearchST {

    using Pair = PairKV<Key, Value>;

private:

    int N = 0;
    vector<Pair> st;

    void merge( vector< Pair >& orgVec, vector< Pair >& auxVec, int low, int mid, int high, string sortBy ) {
        for ( int k = low; k <= high; ++k ) auxVec[ k ] = orgVec[ k ];
        int i = low, j = mid+1;
        if ( sortBy == "value" ) {
            for ( int k = low; k <= high; ++k ) {
                if ( i > mid ) orgVec[ k ] = auxVec[ j++ ];
                else if ( j > high ) orgVec[ k ] = auxVec[ i++ ];
                // comparing the frequency instead of normal value
                else if ( auxVec[ i ].val > auxVec[ j ].val ) orgVec[ k ] = auxVec[ j++ ];
                else if ( auxVec[ i ].val <= auxVec[ j ].val ) orgVec[ k ] = auxVec[ i++ ];
            }
        }
        else {
            for ( int k = low; k <= high; ++k ) {
                if ( i > mid ) orgVec[ k ] = auxVec[ j++ ];
                else if ( j > high ) orgVec[ k ] = auxVec[ i++ ];
                // in the original comparing the 'key' is taken as criteria
                else if ( auxVec[ i ] > auxVec[ j ] ) orgVec[ k ] = auxVec[ j++ ];
                else orgVec[ k ] = auxVec[ i++ ];
            }
        }
    }

    void sort( vector< Pair >& orgVec, vector< Pair >& auxVec, int low, int high, string sortBy ) {
        if ( high <= low ) return;
        int mid = low + ( high - low ) / 2;
        sort( orgVec, auxVec, low, mid, sortBy );
        sort( orgVec, auxVec, mid+1, high, sortBy );
        merge( orgVec, auxVec, low, mid, high, sortBy );
    }

    void mergeSort( vector< Pair >& orgVec, string sortBy ) {
        vector< Pair > auxVec( orgVec.size() );
        sort( orgVec, auxVec, 0, orgVec.size() - 1, sortBy );
    }

public:

    SSST() = default;

    virtual ~SSST() {}

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    bool contains( Key key ) {
        for ( auto s : st ) if ( s.key == key ) return true;
        return false;
    }

    void put( Key key, Value val ) {
        for ( auto& s : st ) {
            if ( s.key == key ) { s.val = val; return; }
        }
        st.push_back( new Pair( key, val ));
        N++;
    }

    void putPair( Pair& pair ) {  
        Key key = pair.key;
        Value val = pair.val;
        put( key, val );
    }

    void putPair( Pair&& pair ) {  
        Key key = pair.key;
        Value val = pair.val;
        put( key, val );
    }

    vector<Pair> sortedByValue() {
        vector<Pair> copy( st.begin(), st.end() );
        mergeSort( copy, "value" );
        return copy;
    }

    vector<Pair> sortedByKey() {
        vector<Pair> copy( st.begin(), st.end() );
        mergeSort( copy, "key" );
        return copy;
    }

    Value& get( Key key ) {
        if ( isEmpty() ) throw out_of_range( "get(): Table is Empty" );
        for ( auto& s : st )
            if ( s.key == key ) return s.val;
        throw runtime_error( "get(): Table has no such Key" );
    }

    vector<Pair>& originalOrder() { return st; }

    Pair& operator[]( int index ) { return st[ index ]; }

};

#define Pair PairKV

int main( int argc, char** argv ) {

    SSST<String, Integer> stringst;
    vector<String> dictionary;

    ifstream input( "dictionary.txt" );

    string str, word;

    while ( input >> str && !input.eof() ) dictionary.push_back( str );

    while ( cin >> word && !cin.eof() ) {
        for ( auto s : dictionary ) {
            if ( word == s ) {
                if( !stringst.contains( word ) ) stringst.put( word,1 );
                else stringst.put( word, stringst.get( word ) + 1 );
            }
        }
    }

    cout << "Sorted By Frequency ( ascending ):\n";
    for ( auto s : stringst.sortedByValue() )
        cout << s << endl;

    cout << endl;

    cout << "Sorted By Order appeared in Dictionary File ( dictionary.txt ):\n";
    for ( auto s : stringst.originalOrder() )
        cout << s << endl;

    return 0;

}
