#ifndef MERGESORT_H
#define MERGESORT_H 

#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

template <typename T>
class Merge {
private:

    static void merge( vector<T>& aux, vector<T>& vec, int lo, int mid, int hi ) {
        for ( int k = lo; k <= hi; ++k ) aux[ k ] = vec[ k ];
        int i = lo;
        int j = mid+1;
        for ( int k = lo; k <= hi; k++ ) {
            if      ( i > mid )            vec[ k++ ] = aux[ j++ ];
            else if ( j > hi )             vec[ k++ ] = aux[ i++ ];
            else if ( aux[ i ] > aux[ j ]) vec[ k++ ] = aux[ j++ ];
            else                           vec[ k++ ] = aux[ i++ ];
        }
    }

    static void sort( vector<T>& aux, vector<T>& vec, int lo, int hi ) {
        if ( lo >= hi ) return;
        int mid = lo + ( hi - lo ) / 2;
        sort( aux, vec, lo, mid );
        sort( aux, vec, mid+1, hi );
        merge( aux, vec, lo, mid , hi );
    }

public:

    static void sort( vector<T>& vec ) {
        vector<T> aux( vec.size() );
        sort( aux, vec, 0, vec.size()-1 );
    }

};

#endif
