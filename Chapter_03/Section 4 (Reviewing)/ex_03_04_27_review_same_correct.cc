#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include "Queue.h"
#include <sstream>

using namespace std;

template <typename Key, typename Value>
class SeparateChainingHashST {
private:
    class Node {
    public:
        Key key;
        Value val;
        Node* next = NULL;
        int index = -1;
        int childNum = 1;
        int hashNum = -1;
        friend ostream& operator << (ostream& os, const Node& h) {
            os << "Key: " << h.key << ". Value: " << h.val << ". Next: ";
            if (h.next) {
                os << h.next;
            } else {
                os << "null";
            }
            return os;
        }

        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };

    int sizeOfChild(Node* h) { return h ? h->childNum : 0; }

    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    Node* newNode(Key key, Value val, int childNum, int hashNum) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->childNum = childNum;
        allNodes.back()->hashNum = hashNum;
        return allNodes.back().get();
    }

    int hash11(Key key) { return 11 * std::hash<Key>{}(key) % M; }

    int hash17(Key key) { return 17 * std::hash<Key>{}(key) % M; }
    
    Node* getValue(Key key) {
        Node* ptr11 = st[hash11(key)];
        Node* ptr17 = st[hash17(key)];
        while (ptr11) {
            if (ptr11->key == key) return ptr11;
            ptr11 = ptr11->next;
        }
        while(ptr17) {
            if (ptr17->key == key) return ptr17;
            ptr17 = ptr17->next;
        }
        return NULL;
    }

    vector<Node*> st;
    vector<unique_ptr<Node>> allNodes;
    int N = 0;
    int M = 17;

public:
    SeparateChainingHashST(): SeparateChainingHashST(17) {}

    SeparateChainingHashST(int M) { st.resize(M); }

    void put(Key key, Value val) {
        Node* ptr11 = st[hash11(key)];
        Node* ptr17 = st[hash17(key)];
        // even if one is shorter than the other, we still have to search in the long one to see if the key already exist and we only replacing the key
        // so we just need to see the hashType of the key (if the key exists it will either be 11 or 17, else -1)
        if (getHashType(key) == 11) {
            while (ptr11) {
                if (ptr11->key == key) {
                    ptr11->val = val;
                    return;
                }
                ptr11 = ptr11->next;
            }
        } else if (getHashType(key) == 17) {
            while (ptr17) {
                if (ptr17->key == key) {
                    ptr17->val = val;
                    return;
                }
                ptr17 = ptr17->next;
            }
        } else {
            if (!ptr11) {
                st[hash11(key)] = newNode(key, val, 1, 11);
                return;
            } 
            else if (!ptr17) {
                st[hash17(key)] = newNode(key, val, 1, 17);
                return;
            }
            else {
                if (ptr17->childNum > ptr11->childNum) {
                    while(ptr11->next) {
                        ptr11->childNum++; // update all the childNum along the way
                        ptr11 = ptr11->next;
                    }
                    ptr11->next = newNode(key, val, 1, 11);
                } else {
                    while(ptr17->next) {
                        ptr17->childNum++;
                        ptr17 = ptr17->next;
                    }
                    ptr17->next = newNode(key, val, 1, 17);
                }
            }
        }
        N++;
    }

    int getHashType(Key key) {
        return getValue(key) ? getValue(key)->hashNum : -1;
    }

    bool contains(Key key) {
        return getValue(key) ? true : false;
    }

    void deleteKey(Key key) {
        int hashType = getHashType(key);
        if (hashType == 11) {
            Node* ptr11 = st[hash11(key)];
            Node* prevPtr = NULL;
            while (ptr11->next) {
                if (ptr11->key == key) {
                    if (prevPtr) {
                        prevPtr->next = ptr11->next;
                        freeNode(ptr11);
                    } else {
                        st[hash11(key)] = NULL;
                    }
                    N--; return;
                }
                ptr11 = ptr11->next;
            }
        } 
        else if (hashType == 17) {
            Node* ptr17 = st[hash17(key)];
            Node* prevPtr = NULL;
            while (ptr17->next) {
                if (ptr17->key == key) {
                    if (prevPtr) {
                        prevPtr->next = ptr17->next;
                        freeNode(ptr17);
                    } else {
                        st[hash17(key)] = NULL;
                    }
                    N--; return;
                }
                ptr17 = ptr17->next;
            }
        }
    }

    // These printing functions just care about printing non-NULL keys regardless of hashing type
    Queue<Key> keys() {
        Queue<Key> queue;
        for (int i = 0; i < M; i++) {
            Node* ptr = st[i];
            while (ptr) {
                queue.enqueue(ptr->key);
                ptr = ptr->next;
            }
        }
        return queue;
    }

    string toString() {
        string str = "\n";
        for (int i = 0; i < M; i++) {
            Node* ptr = st[i];
            while (ptr) {
                str += to_string(i) + " " + ptr->toString() + "\n";
                ptr = ptr->next;
            }
        }
        return str;
    }

};

int main() {
    int length;
    cin >> length;
    char x;
    SeparateChainingHashST<char, int> st;
    for (int i = 0; i < length; i++) {
        cin >> x;
        st.put(x, i);
    }

    cout << st.toString() << endl;

}
