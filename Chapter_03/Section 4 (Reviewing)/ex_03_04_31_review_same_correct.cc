#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include "Queue.h"
#include <sstream>

using namespace std;

// linear probing implementation,  as we must always keep the table < half full (expand whenever 1/2 mark is hit)
template <typename Key, typename Value>
class CuckooHashST { // LinearProbing Special
private: 
    class Entry {
        Key key;
        Value val;
        Entry(Key key, Value val): key(key), val(val) {}
    };

    template <typename T>
    class HashFunction {
    private:
        int mA, mB; // coeff for hash function
        int mLgSize;
    public:
        HashFunction(int coeffA, int coeffB, int lgSize): mA(coeffA), mB(coeffB), mLgSize(lgSize) {}

        int hash(Key key) {
            if (!key) return 0;
            const int hashCode = std::hash<Key>{}(key);
            const int upper = hashCode >>> 16;
            const int lower = hashCode & INT_MAX;

            return (upper * mA + lower*mB) >>> (32-mLgSize);
        }
    };

public:
};

