#include <iostream>
#include <vector>
#include <memory>
#include <sstream>
#include <climits>
#include <cstring>
#include <string>
#include "Queue.h"

using namespace std;

// find the smallest M possible (no matter the 'a')
int main() {
    //  M >= 10 because the number of bucket must be at least the number of input in order to at least contains them at different indices
    // assume original mod of a number 'k' to 'M' = 'r', then if we multiply that 'k' number by 'a': new_mod = r * a. If new_mod > M then it is reset to newer_mod = r * a - M;
    // In each loop, we will try from a = 1 to a = M because if a * (M' > M) then all the numbers are guaranteed to be > 24 which the make the mods are then reset to repeat itself
    int k[10] = { 1, 3, 5, 8, 12, 13, 16, 18, 19, 24 }; // integer representation of the letter indices in a 26-letter alphabet of (S E A R C H X M P L - sorted), assuming that 'A' index in the alphabet is '1'
    int a = -1, M = -1, j = 0;
    int flag = false;
    
    for (M = 10;  M < INT_MAX; M++) {
        for (a = 1; a < M; a++) {
            int mod[M];
            memset(mod, -1, sizeof(mod)); // -1 indicating the current index is empty
            for (j = 0; j < 10; j++) {
                int value = (a * k[j]) % M;
                if (mod[value] != -1) 
                    break;
                else {
                    if (j == 9) { flag = true; break; }
                    mod[value] = value;
                }
            }
            if (flag) break;
        }
        if (flag) break;
    }

    if (flag) {
        cout << "a: " << a << ". M: " << M << endl;
        return 0;
    } else {
        cout << "No a and M in range" << endl;
        return -1;
    }
}
