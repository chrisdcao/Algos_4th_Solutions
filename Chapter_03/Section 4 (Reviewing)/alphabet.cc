#include <iostream>
#include <vector>
#include <map>

using namespace std;

int main() {

    map<char,int> alphabet;
    int j = 1;
    for(char i = 'a'; i <= 'z'; i++) {
        alphabet.insert(make_pair(i,j++));
    }

    for (const auto& s : alphabet) {
        if (s.first == 's' || s.first == 'e'  || s.first == 'a' || s.first == 'r' || s.first == 'c' || s.first == 'h' || s.first == 'x' || s.first == 'm' || s.first == 'p' || s.first == 'l')
            cout << s.first << " " << s.second << endl;
    }

}
