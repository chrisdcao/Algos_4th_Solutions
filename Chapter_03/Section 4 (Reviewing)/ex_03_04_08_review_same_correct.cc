#include <iostream>
#include <utility>
#include <map>
#include <cmath>
#include <vector>
#include <climits>
#include <memory>
#include <sstream>
#include <cstring>
#include "Queue.h"
#include "Random.h"
using namespace std;

template <typename Key, typename Value>
class SeparateChainingHashST {
private:
    class Node {
    public:
        Key key;
        Value val;
        long long index = -1;
        Node* next = NULL;
        friend ostream& operator << (ostream& os, const Node& h) {
            os << "Key: " << h.key << ". Value: " << h.val << ". Next: ";
            if (h.next) { os << h.next->key; } 
            else        { os << "null"; }
            return os;
        }
        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };

    Node* newNode(Key key, Value val) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        return allNodes.back().get();
    }

    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    long long hash(Key key) { return std::hash<Key>{}(key) % M; }

    vector<Node*> st;
    vector<unique_ptr<Node>> allNodes;
    long long M = 17;
    long long N = 0;

    Node* getNode(Key key) { 
        Node* ptr = st[hash(key)];
        while(ptr) {
            if (ptr->key == key) return ptr;
            ptr = ptr->next;
        }
        return NULL;
    }

public:
    SeparateChainingHashST(): SeparateChainingHashST(17) {}
    SeparateChainingHashST(long long M): M(M) { st.resize(M); }

    void put(Key key, Value val) {
        Node* ptr = st[hash(key)];
        if (ptr) {
            if (ptr->key == key) {
                ptr->val = val;
                return;
            }
            while (ptr->next) {
                if (ptr->key == key) { ptr->val = val; return; }
                ptr = ptr->next;
            }
            ptr->next = newNode(key, val);
        } else {
            st[hash(key)] = newNode(key, val);
        }
        N++;
    }

    void deleteKey(Key key) {
        Node* ptr = st[hash(key)];
        Node* prevPtr = NULL;
        while(ptr) {
            if (ptr->key == key) {
                if (prevPtr) prevPtr->next = ptr->next;
                else st[hash(key)] = NULL;
                N--; return;
            }
            prevPtr = ptr;
            ptr = ptr->next;
        }
    }

    Value get(Key key) {
        Node* rv = getNode(key);
        return rv ? rv->val : NULL;
    }

    bool contains(Key key) { return getNode(key) ? true : false; }

    Queue<Key> keys() {
        Queue<Key> queue;
        for (long long i = 0; i < M; i++) {
            Node* ptr = st[i];
            while (ptr) {
                queue.enqueue(ptr->key);
                ptr = ptr->next;
            }
        }
        return queue;
    }

    string toString() {
        string str = "\n";
        for (long long i = 0; i < M; i++) {
            Node* ptr = st[i];
            while (ptr) {
                str += to_string(i) + " " + ptr->toString() + "\n";
                ptr = ptr->next;
            }
        }
        return str;
    }

    long long emptyCount() {
        long long count = 0;
        for (long long i = 0; i < M; i++) {
            Node* ptr = st[i];
            if (!ptr) count++;
        }
        return count;
    }

};

// TODO: recheck this exercise
int main() {
    long long Ns[] = { 10, 100, 1000, 10000, 100000, 1000000 };
    long long trials = 10;
    map<string, long long> map;
    vector<int> vec;
    long long k = 0;

    for (const auto& N : Ns) {
        // distinct keys being pushed into vec
        for (/*  */; k < N; k++) 
            vec.push_back(k);
        long long sum = 0; 
        for (int i = 0; i < trials; i++) {
            shuffle(vec.begin(), vec.end(), std::mt19937{std::random_device{}()});
            SeparateChainingHashST<char, long long> st(N);
            for (long long j = 0; j < N; j++)
                st.put(vec[j], j);
            sum += st.emptyCount();
        }
        map.insert(make_pair<string, long long>("N: " + to_string(N), sum/trials));
    }

    for (const auto& s : map)
        cout << s.first << ". Average number of empty list: " << s.second << endl;

    return 0;
}
