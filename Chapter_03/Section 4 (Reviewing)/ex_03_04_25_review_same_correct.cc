#include <iostream>
#include <vector>
#include <cstring>
#include <climits>
#include <unordered_map>
#include "Queue.h"
#include "Date.h"

using namespace std;

class Transaction {
private:
// since all of the data variables are const, we can use the same hash value whenever the hashCode() is called (because the computation will yield the same result anw)
    string who;
    Date when;
    double amount;
// so we create the cachedHash variable
    int cachedHash = -1;

public:
    Transaction() = default;

    double hashCode(double amount) {
        return std::hash<double>{}(amount);
    }

    int hashCode(string name) {
        return std::hash<string>{}(name);
    }

    Transaction(string who, Date when, double amount): who(who), when(when), amount(amount), cachedHash(-1) {}

    int hashCode()  {
        int hash;
        if (cachedHash == -1) {
            cout << "Cache miss" << endl;
            hash = 17;
            // from all the values, compute to 1 single hash value representing the index?
            hash = 31 * hash + hashCode(who);
            hash = 31 * hash + when.hashCode();
            hash = 31 * hash + hashCode(amount);
            cachedHash = hash;
        } else {
            cout << "Cached hit" << endl;
            hash = cachedHash;
        }
        return hash;
    }
};

int main() {
    Date date(17,10,1996);
    Transaction transaction("Khanh", date, 5.0);
    transaction.hashCode();
    transaction.hashCode();
    transaction.hashCode();

    return 0;
}
