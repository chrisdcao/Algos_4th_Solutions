#include <iostream>
#include <string>
#include <sstream>
#include <regex>
#include <iomanip>
#include <cstring>

using namespace std;

class Date {
private:
    int day = -1;
    int month = -1;
    int year = -1;
    int cachedHash = -1;;
    string separator = "";

public:
    friend ostream& operator << (ostream& os, const Date& date) {
        os << setfill('0') << setw(2) << date.day 
           << date.separator << setfill('0') << setw(2) << date.month 
           << date.separator << setfill('0') << setw(4) << date.year;
        return os;
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

    int hashCode() {
        int hash;
        if (cachedHash == -1) {
            hash = 17;
            hash = 31 * hash + std::hash<int>{}(day);
            hash = 31 * hash + std::hash<int>{}(month);
            hash = 31 * hash + std::hash<int>{}(year);
            cachedHash = hash;
        } else 
            hash = cachedHash;
        return hash;
    }

    Date() = default;
    
    Date(int dd, int mm, int yyyy) {
        if (isDateValid(dd,mm,yyyy)) {
            day = dd;
            month = mm;
            year = yyyy;
        }
    }

    Date(string date) {
        // these regex already ensure the range of day/month in common sense
        const char DATE_SLASH[] = "[0-1][0-9]/[0-1][0-9]/[0-9][0-9][0-9][0-9]";
        const char DATE_DOT[] = "[0-1][0-9].[0-1][0-9].[0-9][0-9][0-9][0-9]";
        const char DATE_HYPHEN[] = "[0-1][0-9]-[0-1][0-9]-[0-9][0-9][0-9][0-9]";
        if (regex_match(date, regex(DATE_SLASH))) {
            int dayLastIndex = date.find_first_of('/');
            int monthLastIndex = date.find_last_of('/');
            separator = "/";
            day = stoi(date.substr(0, 2));
            month = stoi(date.substr(dayLastIndex+1, 2));
            year = stoi(date.substr(monthLastIndex+1, 4));
        } else if (regex_match(date, regex(DATE_DOT))) {
            int dayLastIndex = date.find_first_of('.');
            int monthLastIndex = date.find_last_of('.');
            separator = ".";
            day = stoi(date.substr(0, 2));
            month = stoi(date.substr(dayLastIndex+1, 2));
            year = stoi(date.substr(monthLastIndex+1, 4));
        } else if (regex_match(date, regex(DATE_HYPHEN))) {
            int dayLastIndex = date.find_first_of('-');
            int monthLastIndex = date.find_last_of('-');
            separator = "-";
            day = stoi(date.substr(0, 2));
            month = stoi(date.substr(dayLastIndex+1, 2));
            year = stoi(date.substr(monthLastIndex+1, 4));
        } else 
            throw logic_error("Date(string date): Input not valid!");
    }

    void changeDay(int dd) {
        if (isDateValid(dd, this->month, this->year)) // check if the date is compatible with current stuff
            this->day = dd;
        else throw runtime_error("insertDay(): ");
    }

    void changeMonth(int mm) {
        if (isDateValid(this->day, mm, this->year))
            this->month = mm;
        else throw runtime_error("insertMonth(): ");
    }

    void changeYear(int yyyy) {
        if (isDateValid(this->day, this->month, yyyy))
            this->year = yyyy;
        else throw runtime_error("insertYear(): ");
    }

    bool isDateValid(int dd, int mm, int yyyy) {
        if (yyyy < 0 || dd < 1 || mm < 1) return false;
        if (mm > 12) return false;

        if (mm == 2) { // special month
            if (yyyy % 4 == 0) // LEAP YEAR
                return dd <= 29;
            else
                return dd <= 28;
        } else if (mm >= 1 && mm <= 7) {
            if (dd % 2 == 1) return dd <= 31;
            else             return dd <= 30;
        } else {
            if (dd % 2 == 1) return dd <= 30;
            else             return dd <= 31;
        }
        // for DEBUG purpose
        throw logic_error("isDateValid(): a corner case haven't covered!");
    }

    bool operator == (Date& rhs) {
        return year == rhs.year && month == rhs.month && day == rhs.day;
    }

    bool operator != (Date& rhs) {
        return !operator==(rhs);
    }

    bool operator >=(Date& rhs) {
        return year >= rhs.year && month >= rhs.month && day >= rhs.day;
    }
    
    bool operator > (Date& rhs) {
        return operator!=(rhs) && operator>=(rhs);
    }

    bool operator <= (Date& rhs) {
        return year <= rhs.year && month <= rhs.month && day <= rhs.day;
    }

    bool operator < (Date& rhs) {
        return operator!=(rhs) && operator<=(rhs);
    }

};
