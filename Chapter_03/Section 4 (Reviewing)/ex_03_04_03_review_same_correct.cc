#include <iostream>
#include <vector>
#include <memory>
#include <climits>
#include <sstream>
#include "Queue.h"

using namespace std;

template <typename Key, typename Value>
class SeparateChainingHashST {
private:
    class Node {
    public:
        Key key;
        Value val;
        Node* next = NULL;
        int index = -1;
        int numOfEntries = 1;
        friend ostream& operator << (ostream& os, const Node& h) {
            os << "Key: " << h.key << ". Value: " << h.val 
               << ". Current num of Entries: " << h.numOfEntries << ". Next: ";
            if   (h.next) { os << h.next->key; } 
            else          { os << "null"; }
            return os;
        }

        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };

    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    Node* newNode(Key key, Value val, int numOfEntries) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->numOfEntries = numOfEntries;
        return allNodes.back().get();
    }

    int hash(Key key) { return (std::hash<Key>{}(key) & INT_MAX) % M; }

    Node* getNode(Key key) {
        Node* ptr = st[hash(key)];
        while(ptr) {
            if (ptr->key == key) return ptr;
            ptr = ptr->next;
        }
        return NULL;
    }

    void recursiveFreeNode(Node* h) {
        if (h == NULL) return;
        recursiveFreeNode(h->next);
        freeNode(h); N--;
    }

    int M = 17;
    int N = 0;
    vector<Node*> st;
    vector<unique_ptr<Node>> allNodes;

public:

    SeparateChainingHashST(): SeparateChainingHashST(17) {}

    SeparateChainingHashST(int M): M(M) { st.resize(M); }

    void put(Key key, Value val) {
        Node* ptr = st[hash(key)];
        if (ptr) {
            if (ptr->key == key) { ptr->val = val; return; }
            while (ptr->next) {
                if (ptr->key == key) { ptr->val = val; return; }
                ptr = ptr->next;
            }
            ptr->next = newNode(key, val, ++N);
        } else {
            st[hash(key)] = newNode(key, val, ++N);
        }
    }

    void deleteKey(Key key) {
        Node* ptr = st[hash(key)];
        Node* ptrPrev = NULL;
        while (ptr) {
            if (ptr->key == key) {
                if (ptrPrev) ptrPrev->next = ptr->next;
                else st[hash(key)] = NULL;
                N--; return;
            }
        }
    }

    void deleteKeysEntriesLargerThan(int k) {
        for (int i = 0; i < M; i++) {
            Node* ptr = st[i];
            Node* prevPtr = NULL;
            // the deeper into the collision bucket, the larger/higher the number of entries -> at any node that has num of entries > k, we delete all the nodes after it in the list
            while (ptr) {
                if (ptr->numOfEntries > k) { 
                    if (prevPtr) {
                        recursiveFreeNode(ptr); 
                        prevPtr->next = NULL;
                    } else {
                        st[i] = NULL;
                    }
                    break;
                }
                prevPtr = ptr;
                ptr = ptr->next; 
            }
        }
    }

    bool contains(Key key) { return get(key); }

    Value get(Key key) { return getNode(key) ? getNode(key)->val : NULL; }

    Queue<Key> keys() {
        Queue<Key> queue;
        for (int i = 0; i < M; i++) {
            Node* ptr = st[i];
            while (ptr) {
                queue.enqueue(ptr);
                ptr = ptr->next;
            }
        }
        return queue;
    }

    string toString() {
        string str = "\n";
        for (int i = 0; i < M; i++) {
            Node* ptr = st[i];
            while (ptr) {
                str += to_string(i) + " " + ptr->toString() + "\n";
                ptr = ptr->next;
            }
        }
        return str;
    }

};

int main() {
    int length;
    cin >> length;
    string str;
    SeparateChainingHashST<string, int> st;
    for (int i = 0; i < length; i++) {
        cin >> str;
        st.put(str, i);
    }

    cout << st.toString() << endl;
    st.deleteKeysEntriesLargerThan(5);
    cout << st.toString() << endl;

    return 0;
}
