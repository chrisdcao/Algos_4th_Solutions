#include <iostream>
#include <climits>
#include <cstring>
#include <memory>
#include <sstream>
#include "Queue.h"

using namespace std;

template <typename Key, typename Value> 
class LinearProbingHashST {
private:
    int hash(Key key, int capacity) { return 11 *  std::hash<Key>{}(key) % capacity; }

    int hash(Key key) { return 11 * std::hash<Key>{}(key) % M; }

    void resize(int cap) {
        cout << "resize is called" << endl;
        Key** tempKey = new Key*[cap];
        Value** tempVal = new Value*[cap];
        memset(tempKey, '\0', sizeof(Key*)*cap);
        memset(tempVal, '\0', sizeof(Value*)*cap);
        for (int j = 0; j < M; j++) {
            bool flag = false;
            if (keys[j]) {
                int i;
                for (i = hash(*keys[j], cap); tempKey[i]; i = (i+1) % cap);
                tempKey[i] = keys[j];
                tempVal[i] = vals[j];
            }
        }
        keys = tempKey; 
        vals = tempVal;
        M = cap;
        tempKey = NULL; delete tempKey;
        tempVal = NULL; delete tempVal;
    }

    Key** keys;
    Value** vals;
    int M = 17;
    int N = 0;

public:
    friend ostream& operator << (ostream& os, const LinearProbingHashST& st) {
        for (int i = 0; i < st.M; i++) {
            if (st.keys[i]) {
                os << i <<  " Key: " << *st.keys[i] << ". Value: " << *st.vals[i] << "\n";
            }
        }
        return os;
    }

    LinearProbingHashST(): LinearProbingHashST(17) {}

    LinearProbingHashST(int M): M(M) { 
        keys = new Key*[M];
        vals = new Value*[M];
        memset(keys,'\0',sizeof(Key*)*M);
        memset(vals,'\0',sizeof(Value*)*M);
    }

    void put(Key key, Value val) {
        if (N >= M/2) resize(M * 2);
        int i;
        for (i = hash(key); keys[i]; i = (i + 1) % M) {
            if (*keys[i] == key) {
                delete vals[i];
                vals[i] = new Value(val); 
                return;
            }
        }
        keys[i] = new Key(key);
        vals[i] = new Value(val);
        N++;
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

};

int main() {
    int length;
    cin >> length;
    string str;
    LinearProbingHashST<string, int> st;
    for (int i = 0; i < length; i++) {
        cin >> str;
        st.put(str, i);
    }
    cout << endl;
    cout << st.toString() << endl;
}
