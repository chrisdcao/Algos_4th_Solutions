#include <iostream>
#include <vector>
#include <memory>
#include <climits>
#include "Queue.h"
#include <sstream>

using namespace std;

template <typename Key, typename Value>
class SeparateChainingHashST {
private:
    class Node {
    public:
        Key key;
        Value val;
        int index = -1;
        Node* next = NULL;
        friend ostream& operator <<  (ostream& os, const Node& h) {
            os << "Key: " << h.key << ". Value: " << h.val << ".Next: ";
            if (h.next) {
                os << h.next->key;
            } else {
                os << "null";
            }
            return os;
        }
        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };

    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    Node* newNode(Key key, Value val) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        return allNodes.back().get();
    }

    int hash(Key key) { return (std::hash<Key>{}(key) & INT_MAX) % M; }

    Node* getNode(Key key) {
        Node* ptr = st[hash(key)];
        while (ptr) {
            if (ptr->key == key) return ptr;
            ptr = ptr->next;
        }
        return NULL;
    }

    int M = 17;
    int N = 0;
    vector<Node*> st;
    vector<unique_ptr<Node>> allNodes;

public:
    SeparateChainingHashST(): SeparateChainingHashST(17) {}
    SeparateChainingHashST(int M): M(M) { st.resize(M); }

    Value get(Key key) {
        Node* returnValue = getNode(key);
        return returnValue ? returnValue->val : NULL;
    }

    bool contains(Key key) { return get(key); }

    void put(Key key, Value val) {
        Node* ptr = st[hash(key)];
        if (ptr) {
            if (ptr->key == key) {
                ptr->val = val;
                return;
            }
            while (ptr->next) {
                if (ptr->key == key) {
                    ptr->val = val;
                    return;
                }
                ptr = ptr->next;
            }
            ptr->next = newNode(key, val);
        } else {
            st[hash(key)] = newNode(key,val);
        }
        N++;
    }

    void deleteKey(Key key) {
        Node* ptr = st[hash(key)];
        Node* beforePtr = NULL;
        while (ptr) {
            if (ptr->key == key) {
                if (beforePtr) beforePtr->next = ptr->next;
                else           ptr = NULL;
                N--; return;
            }
            beforePtr = ptr;
            ptr = ptr->next;
        }
    }

    Queue<Key> keys() {
        Queue<Key> queue;
        for (int i =0 ; i < M; i++) {
            Node* ptr = st[i];
            while(ptr) {
                queue.enqueue(ptr->key);
                ptr = ptr->next;
            }
        }
        return queue;
    }

    string toString() {
        string str = "\n";
        for (int i=0; i<M; i++) {
            Node* ptr = st[i];
            while (ptr) {
                str += to_string(i) + " " + ptr->toString() + "\n";
                ptr = ptr->next;
            }
        }
        return str;
    }
};

int main() {
	SeparateChainingHashST<string, int> st;
  int length;
  cin >> length;
  string str;
  for (int i=0;i<length;i++) {
    cin >> str;
    st.put(str,i);
  }

  cout << st.toString() << endl;

  return 0;
}
