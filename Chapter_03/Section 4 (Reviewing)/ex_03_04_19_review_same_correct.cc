#include <iostream>
#include <sstream>
#include <vector>
#include <memory>
#include <climits>
#include <cstring>
#include "Queue.h"

using namespace std;

template <typename Key, typename Value>
class SeparateChainingHashST {
private:
    class Node {
    public:
        Key key; 
        Value val;
        Node* next = NULL;
        int index = -1;
        friend ostream& operator << (ostream& os, const Node& h) {
            os << "Key: " << h.key << ". Value: " << h.val << ". Next: ";
            if (h.next) {
                os << h.next->key;
            } else {
                os << "null";
            }
            return os;
        }

        string toString() {
            ostringstream oss;
            oss << *this;
            return oss.str();
        }
    };

    Node* newNode(Key key, Value val) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        return allNodes.back().get();
    }

    void freeNode(Node* h) {
        if (h->index < allNodes.size()-1) {
            allNodes[h->index].swap(allNodes.back());
            allNodes[h->index]->index = h->index;
        }
        allNodes.pop_back();
    }

    int hash(Key key) { return (std::hash<Key>{}(key) & INT_MAX) % M; }

    Node* getNode(Key key) {
        Node* ptr = st[hash(key)];
        while (ptr) {
            if (ptr->key == key) return ptr;
            ptr = ptr->next;
        }
        return NULL;
    }

    int M = 17;
    int N = 0;
    vector<Node*> st;
    vector<unique_ptr<Node>> allNodes;

public:
    SeparateChainingHashST(): SeparateChainingHashST(17) {}

    SeparateChainingHashST(int M): M(M) { st.resize(M); }

    void put(Key key, Value val) {
        Node* ptr = st[hash(key)];
        if (ptr) {
            if (ptr->key == key) {
                ptr->val = val;
                return;
            }
            while(ptr->next) {
                if (ptr->key == key) {
                    ptr->val = val;
                    return;
                }
                ptr = ptr->next;
            }
            ptr->next = newNode(key, val);
        } else {
            st[hash(key)] = newNode(key, val);
        }
        N++;
    }

    void deleteKey(Key key) {
        Node* ptr = st[hash(key)];
        Node* prevPtr = NULL;
        while (ptr) {
            if (ptr->key == key) {
                if (prevPtr) {
                    prevPtr->next = ptr->next;
                } else {
                    st[hash(key)] = NULL;
                }
                N--; return;
            }
            prevPtr = ptr;
            ptr = ptr->next;
        }
    }

    string toString() {
        string str = "\n";
        for (int i =0 ; i < M; i++) {
            Node* ptr = st[i];
            while (ptr) {
                str += to_string(i) + " " + ptr->toString() + "\n";
                ptr = ptr->next;
            }
        }
        return str;
    }

    bool contains(Key key) { return getNode(key) != NULL; }

    Value get(Key key) { 
        Node* rv = getNode(key);
        return rv ? rv->val: NULL;
    }

    Queue<Key> keys() {
        Queue<Key> queue;
        for (int i = 0; i < M; i++) {
            Node* ptr = st[i];
            while (ptr) {
                queue.enqueue(ptr->key);
                ptr = ptr->next;
            }
        }
        return queue;
    }

};

template <typename Key, typename Value>
class LinearProbingHashST {
private:
    int hash(Key key) { return std::hash<Key>{}(key) % M; }

    int hash(Key key, int capacity) { return std::hash<Key>{}(key) % capacity; }

    void resize(int cap) {
        cout << "resize to " << cap << " is called" << endl;
        Key** tempKey = new Key*[cap];
        Value** tempVal = new Value*[cap];
        memset(tempKey, '\0', sizeof(Key*)*cap);
        memset(tempVal, '\0', sizeof(Value*)*cap);
        for (int j = 0; j < M; j++) {
            if (keys[j]) {
                int i;
                for (i = hash(*keys[j], cap); tempKey[i]; i = (i+1) % cap);
                tempKey[i] = keys[j];
                tempVal[i] = vals[j];
            }
        }
        keys = tempKey;
        vals = tempVal;
        tempKey = NULL; delete tempKey;
        tempVal = NULL; delete tempVal;
        M = cap;
    }

    int M = 17;
    int N = 0;
    Key** keys;
    Value** vals;

    Value* getValuePtr(Key key) {
        int i;
        for (i = hash(key); keys[i]; i = (i + 1) % M) {
            if (keys[i]) {
                if (*keys[i] == key) 
                    return vals[i];
            }
        }
        return NULL;
    }
    
public:
    friend ostream& operator << (ostream& os, LinearProbingHashST& st) {
        for (int i = 0; i < st.M; i++) {
            if (st.keys[i])
                os << to_string(i) << " Key: " << *st.keys[i] << ". Value:" << *st.vals[i] << "\n";
        }
        return os;
    }

    LinearProbingHashST(): LinearProbingHashST(17) {}

    LinearProbingHashST(int M): M(M) {
        keys = new Key*[M];
        vals = new Value*[M];
        memset(keys,'\0',sizeof(Key*)*M);
        memset(vals,'\0', sizeof(Value*)*M);
    }

    void put(Key key, Value val) {
        if (N >= M/2) resize(2 * M);
        int i;
        for (i = hash(key); keys[i]; i = (i + 1) % M) {
            if (*keys[i] == key) {
                delete vals[i];
                vals[i] = new Value(val);
                return;
            }
        }
        keys[i] = new Key(key);
        vals[i] = new Value(val);
        N++;
    }

    void deleteKey(Key key) {
        if (!contains(key)) return;
        int i = hash(key);
        while (*keys[i] != key)
            i = (i + 1) % M;
        keys[i] = NULL;
        vals[i] = NULL;
        i = (i + 1) % M;
        while (keys[i]) {
            Key keyToRedo = *keys[i];
            Value valToRedo = *vals[i];
            keys[i] = NULL;
            vals[i] = NULL;
            N--;
            put(keyToRedo, valToRedo);
            i = (i + 1) % M;
        }
        N--;
        if (N > 0 && N == M/8) resize(M/2);
    }

    Queue<Key> keyQueue() {
        Queue<Key> queue;
        for (int i = 0; i < M; i++) {
            if (keys[i]) {
                queue.enqueue(*keys[i]);
            }
        }
        return queue;
    }

    Value get(Key key) {
        Value* rv = getValuePtr(key);
        return rv ? *rv : NULL;
    }

    bool contains(Key key) { 
        return getValuePtr(key) != NULL;
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

};

int main() {
    int length;
    cin >> length;
    string str;
    LinearProbingHashST<string, int> stProb(length);
    SeparateChainingHashST<string, int> stChain(length);
    for (int i = 0; i < length; i++) {
        cin >> str;
        stProb.put(str, i);
        stChain.put(str, i);
    }
    cout << "LinearProbing keys(): ";
    for (auto s : stProb.keyQueue())
        cout << s << " ";
    cout << endl;

    cout << "SeparateChain keys(): ";
    for (auto s : stChain.keys())
        cout << s << " ";
    cout << endl;
}
