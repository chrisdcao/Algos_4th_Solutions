#include <iostream>
#include <sstream>
#include <vector>
#include <memory>
#include <climits>
#include <cstring>
#include "Queue.h"

using namespace std;

template <typename Key, typename Value>
class LinearProbingHashST {
private:
    int hash(Key key) { return std::hash<Key>{}(key) % M; }

    int hash(Key key, int capacity) { return std::hash<Key>{}(key) % capacity; }

    int secondHash(Key key) { 
        int hash2 = std::hash<Key>{}(key) % M;
        hash2 = (hash2 != 0) ? hash2 : M+1;
        return hash2;
    }

    void resize(int cap) {
        cout << "resize to " << cap << " is called" << endl;
        Key** tempKey = new Key*[cap];
        Value** tempVal = new Value*[cap];
        memset(tempKey, '\0', sizeof(Key*)*cap);
        memset(tempVal, '\0', sizeof(Value*)*cap);
        for (int j = 0; j < M; j++) {
            if (keys[j]) {
                int i;
                for (i = hash(*keys[j], cap); tempKey[i]; i = (i+1) % cap);
                tempKey[i] = keys[j];
                tempVal[i] = vals[j];
            }
        }
        keys = tempKey;
        vals = tempVal;
        tempKey = NULL; delete tempKey;
        tempVal = NULL; delete tempVal;
        M = cap;
    }

    int M = 17;
    int N = 0;
    Key** keys;
    Value** vals;

    Value* getValuePtr(Key key) {
        int i;
        // since the put in order has been changed (to the double-layer hashing), every search operations must be changed accordingly
        for (i = hash(key); keys[i]; i = (i + secondHash(key)) % M) {
            if (keys[i]) {
                if (*keys[i] == key) 
                    return vals[i];
            }
        }
        return NULL;
    }
    
public:
    friend ostream& operator << (ostream& os, LinearProbingHashST& st) {
        for (int i = 0; i < st.M; i++) {
            if (st.keys[i])
                os << to_string(i) << " Key: " << *st.keys[i] << ". Value:" << *st.vals[i] << "\n";
        }
        return os;
    }

    LinearProbingHashST(): LinearProbingHashST(17) {}

    LinearProbingHashST(int M): M(M) {
        keys = new Key*[M];
        vals = new Value*[M];
        memset(keys,'\0',sizeof(Key*)*M);
        memset(vals,'\0', sizeof(Value*)*M);
    }

    void put(Key key, Value val) {
        if (N >= M/2) resize(2 * M);
        int i;
        for (i = hash(key); keys[i]; i = (i + secondHash(key))%M) { // two layers of hashing (since secondHash is already a hashCode() of i, then afterwards we hash to get i value)
            if (*keys[i] == key) {
                delete vals[i];
                vals[i] = new Value(val);
                return;
            }
        }
        keys[i] = new Key(key);
        vals[i] = new Value(val);
        N++;
    }

    void deleteKey(Key key) {
        if (!contains(key)) return;
        int i = hash(key);
        while (*keys[i] != key)
            i = (i + secondHash(key)) % M;
        keys[i] = NULL;
        vals[i] = NULL;
        i = (i + secondHash(key)) % M;
        while (keys[i]) {
            Key keyToRedo = *keys[i];
            Value valToRedo = *vals[i];
            keys[i] = NULL;
            vals[i] = NULL;
            N--;
            put(keyToRedo, valToRedo);
            i = (i + secondHash(key)) % M;
        }
        N--;
        if (N > 0 && N == M/8) resize(M/2);
    }

    Value get(Key key) {
        Value* rv = getValuePtr(key);
        return rv ? *rv : NULL;
    }

    bool contains(Key key) { 
        return getValuePtr(key) != NULL;
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

};

int main() {
    int length;
    cin >> length;
    string str;
    LinearProbingHashST<string, int> stProb(length);
    for (int i = 0; i < length; i++) {
        cin >> str;
        stProb.put(str, i);
    }
    cout << stProb.toString() << endl;
    cout << "delete Key: E from Linear hash!" << endl;
    stProb.deleteKey("E");
    cout << stProb.toString() << endl;
}

