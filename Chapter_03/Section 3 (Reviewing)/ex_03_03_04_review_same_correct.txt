The shortest height happens when all keys are 3-node, and last level is full
The longest height happens when all keys are 2-node

For both cases, since it's a 2-3 tree, the tree will always be balanced

-> range is within [3-node height, 2-node height] of balanced tree

if all 3-node:
Call the tree height = x
Let tree height starts from 0

Since the tree is balanced, the number of elements at:
level 0 = 3 ^ 0
level 1 = 3 ^ 1 = (level0 * 3^1)
level 2 = 3 ^ 2 = level1 * 3 = (level0 * 3^1) * 3 = (level0 * 3^2)
level 3 = 3 ^ 3 = level2 * 3 = (level0 * 3^2) * 3 = level0 * 3^3)
etc.. 
level x-1 = 3 ^ (x-1) = level(x-2) * 3 = (level0 * 3^(x-2)) * 3 = level0 * 3^(x-1)

-> To get to tree_height x from level 0, we have to multiply 3^(x-1) to level0_number_of_elements
In other words, having [3^(x-1) + 1] elements added to an intial empty tree create tree_height of x

-> if we have [N] elements:
=>  3^(x-1) + 1 = N
<=> 3^(x-1)     ~ N
<=> x - 1       ~ log3(N)
<=> x           ~ log3(N)

=> we can get a ~ log3(N) height, given that it's a balanced-all-3-node tree
Similaryly for all 2-nodes, we can get a log2N height, given  that 2-3 tree is always balanced

=> height of 2-3 tree at its best (where all are 3 nodes) ~ log3N and at its worse (all are 2 nodes) ~ log2N, anything height in between is a mixture of 2 and 3 nodes
=> height of 2-3 tree ∈ [~log3N, ~log2N]
