#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iomanip>
#include "Queue.h"
#include "Random.h"

using namespace std;

static const bool RED = true;
static const bool BLACK = false;

template < typename Key, typename Value > 
class RedBlackBST{
private:

    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        int N = 0;
        int index = -1;
        bool color;
    };

    Node* newNode( Key key, Value val, int N, bool color ) {
        allNodes.push_back( make_unique< Node >() );
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        allNodes.back()->color = color;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;
    Node* root = NULL;

    Node* min( Node* x ) { return ( !x->left ) ? x : min( x->left ); }

    Node* max( Node* x ) { return ( !x->right ) ? x : max( x->right ); }

    int size( Node* x ) { return ( !x ) ? 0 : x->N; }

    Node* rotateLeft( Node* h ) {
        Node* x = h->right;
        h->right = x->left;
        x->left = h;
        x->color = h->color;
        h->color = RED;
        x->N = h->N;
        h->N = size( h->left ) + size( h->right ) + 1;
        return x;
    }

    Node* rotateRight( Node* h ) {
        Node* x = h->left;
        h->left = x->right;
        x->right = h;
        x->color = h->color;
        h->color = RED;
        x->N = h->N;
        h->N = size( h->left ) + size( h->right ) + 1;
        return x;
    }

    void flipColors( Node* h ) {
        h->color = RED;
        h->left->color = BLACK;
        h->right->color = BLACK;
    }

    bool isRed( Node* x ) { return ( !x ) ? false : x->color == RED; }

    Node* put( Node* h, Key key, Value val ) {
        if      ( !h )           return newNode( key, val, 1, RED );
        if      ( h->key > key ) h->left = put( h->left, key, val );
        else if ( h->key < key ) h->right = put( h->right, key, val );
        else h->val = val;
        if ( isRed( h->right ) && !isRed( h->left ) ) h = rotateLeft( h );
        if ( isRed( h->left ) && !isRed( h->left->left ) ) h = rotateRight( h );
        if ( isRed( h->left ) && isRed( h->right ) ) flipColors( h );
        h->N = size( h->left ) + size( h->right ) + 1;
        return h;
    }

    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( !x ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

public:

    void put( Key key, Value val ) {
        root = put( root, key, val );
        root->color = BLACK;
    }

    int size() { return size( root ); }

    bool isEmpty() { return size() == 0; }

    Key min() { return min( root )->key; }

    Key max() { return max( root )->key; }

    Queue< Key > keys( Key lo, Key hi ) {
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    Queue< Key > keys() { return keys( min(), max() ); }

};

int main( int argc, char** argv ) {

    RedBlackBST< char, int > redBlackBST;
    string input = "EASYQUTION";

    cout << "data coming in: ";
    for ( int i = 0; i < input.size(); i++ ) {
        cout << input[ i ] << " ";
        redBlackBST.put( input[ i ], i );
    }
    cout << endl;

    for ( auto c : redBlackBST.keys() )
        cout << c << ", ";
    cout << endl;

    return 0;

}
