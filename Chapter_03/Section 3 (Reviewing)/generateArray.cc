#include <iostream>
#include <vector>
#include "Random.h"

using namespace std;

int main(int argc, char** argv) {

    int N = stoi(argv[1]);

    vector<int> vec;

    for (int i = 0; i < N; i++) {
        vec.push_back(randomUniformDistribution(0,21));
    }

    for (const auto& s : vec)
        cout << s << " ";
    cout << endl;

    return 0;

}
