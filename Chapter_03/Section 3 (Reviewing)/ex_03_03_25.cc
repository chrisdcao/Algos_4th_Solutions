#include <iostream>
#include <vector>
#include <memory>
#include "Queue.h"
#include "Random.h"

using namespace std;

template <typename Key, typename Value>
class RB234Tree {
    static const bool RED = true;
    static const bool BLACK = false;

private:
    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        int N = 1;
        bool color;
        int index = -1;
    };

    vector<unique_ptr<Node>> allNodes;
    Node* root = NULL;

    void freeNode(Node* x) {
        if (x->index < allNodes.size()-1) {
            allNodes[x->index].swap(allNodes.back());
            allNodes[x->index]->index = x->index;
        }
        allNodes.pop_back();
    }

    Node* newNode(Key key, Value val, int N, bool color) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->color = color;
        allNodes.back()->N = N;
        return allNodes.back().get();
    }

    int size(Node* x) {
        return x ? x->N : 0;
    }

    Node* put(Node* x, Key key, Value val) {
        if (!x) {
            return newNode(key, val, 1, RED);
        }

        // by bringing this before the recursive call, we allow 2 red links at any given root (on left and right child) until a new node is inserted 
        // SO 2-3-4 tree maintains 2 red links on 2 sides rather than 2 continously red link (which cannot be maintained due to rotateRight condition checking)
        if (isRed(x->left) && isRed(x->right))  {
            flipColors(x);
        }

        if (x->key > key) {
            x->left = put(x->left, key, val);
        } else if (x->key < key) {
            x->right = put(x->right, key, val);
        } else {
            x->val = val;
        }

        if (isRed(x->right) && !isRed(x->left))  {
            x = rotateLeft(x);
        }
        if (isRed(x->left) && isRed(x->left->left)) {
            x = rotateRight(x);
        }

        x->N = size(x->left) + size(x->right) + 1;
        return x;
    }

    bool isRed(Node* x) {
        return x ? (x->color == RED) : false;
    }

    void flipColors(Node* x) {
        x->color = !x->color;
        x->left->color = !x->left->color;
        x->right->color = !x->right->color;
    }

    Node* rotateLeft(Node* h) {
        Node* x = h->right;
        h->right = x->left;
        x->left = h;
        x->color = h->color;
        h->color = RED;
        x->N = h->N;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* rotateRight(Node* h) {
        Node* x = h->left;
        h->left = x->right;
        x->right = h;
        x->color = h->color;
        h->color = RED;
        x->N = h->N;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Value get(Node* x, Key key)  {
        if (!x)  return NULL;
        
        if (x->key > key)  {
            return get(x->left, key);
        } else if (x->key < key) {
            return get(x->right, key);
        } else {
            return x->val;
        }
    }
    
    Node* select(Node* x, int k) {
        if (!x) return NULL;

        int t = size(x->left);
        if (t > k) {
            return select(x->left, k);
        } else if (t < k) {
            return select(x->right, k-t-1);
        } else {
            return x;
        }
    }

    Node* max(Node* x)  {
        return (!x->right) ? x : max(x->right);
    }

    Node* min(Node* x) {
        return (!x->left) ? x : min(x->left);
    }

    // largest smaller
    Node* floor(Node* x, Key key) {
        if (!x) return NULL;
     
        if (x->key > key)  {
            return floor(x->left, key);
        } else if (x->key == key)  {
            return x;
        }
        Node* t = floor(x->right, key);

        return t ? t : x;
    }

    // smallest larger
    Node* ceiling(Node* x, Key key) {
        if (!x) return NULL;
        
        if (x->key < key) {
            return ceiling(x->right, key);
        } else if (x->key == key) {
            return x; 
        }
        Node* t = ceiling(x->left, key);

        return t ? t : x;
    }

    void keys(Node* x, Queue<Key>& queue, Key lo, Key hi) {
        if (!x) return;

        if (x->key > lo) {
            keys(x->left, queue, lo, hi);
        }
        if (x->key >= lo && x->key <= hi) {
            queue.enqueue(x->key);
        }
        if (x->key < hi) {
            keys(x->right, queue, lo, hi);
        }
    }

    bool isValid234Tree(Node* x) {
        if (!x) return true;    // 2 black links -> correct

        if (!isRed(x->left) && isRed(x->right)) {
            return false;
        }
        if (isRed(x->left) && isRed(x->left->left)) {
            return false;
        }
        if (isRed(x->right) && isRed(x->right->right)) {
            return false;
        }
        if (isRed(x->right) && isRed(x->right->left)) {
            return false;
        }

        return isValid234Tree(x->left) && isValid234Tree(x->right);
    }

    bool isBST(Node* x) {
        if (!x) return true;

        if (x->left) {
            if (x->left->key > x->key) {
                return false;
            }
        }
        if (x->right) {
            if (x->right->key < x->key) {
                return false;
            }
        }

        return isBST(x->left) && isBST(x->right);
    }

    bool isSubtreeConsistent(Node* x) {
        if (!x) return true;

        int totalSubtreeCount = 0;
        if (x->left) {
            totalSubtreeCount += size(x->left);
        }
        if (x->right) {
            totalSubtreeCount += size(x->right);
        }

        if (size(x) != totalSubtreeCount + 1) {
            return false;
        }

        return isSubtreeConsistent(x->left) && isSubtreeConsistent(x->right);
    }

public:

    int size() {
        return size(root->N);
    }

    bool isEmpty() {
        return size() == 0;
    }

    void put(Key key, Value val) {
        root = put(root, key, val);
        root->color = BLACK;
    }

    Key ceiling(Key key) {
        return ceiling(root)->key;
    }

    Key floor(Key key) {
        return floor(root)->key;
    }

    Queue<Key> keys(Key lo, Key hi) {
        Queue<Key> queue;
        keys(root, queue, lo, hi);
        return queue;
    }

    Queue<Key> keys() {
        return keys(min(), max());
    }

    Key min() {
        return min(root)->key;
    }

    Key max() {
        return max(root)->key;
    }

    bool isValid234Tree() {
        return isValid234Tree(root);
    }

    bool isBST() {
        return isBST(root);
    }

    bool isSubtreeConsistent() {
        return isSubtreeConsistent(root);
    }

};

int main(int argc, char** argv) {
    int N = stoi(argv[1]);

    RB234Tree<int, int> tree;
    for (int i = 0; i < N; i++) {
        tree.put(randomUniformDistribution(0, N-1),i);
        cout << ((tree.isValid234Tree()) ? "true" : "false") << endl;
    }

    cout << "Is subtree consistent? " << ((tree.isSubtreeConsistent()) ? "true" : "false") << endl;

    for (const auto s : tree.keys()) {
        cout << s << " ";
    }
    cout << endl;

    return 0;
}
