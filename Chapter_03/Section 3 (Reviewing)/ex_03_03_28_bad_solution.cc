#include <iostream>
#include <vector>
#include <memory>
#include "Queue.h"
#include "Random.h"

using namespace std;

template <typename Key, typename Value>
class BottomUp234Tree {
    static const bool RED = true;
    static const bool BLACK = false;

private:

    class Node {
    public:
        Key key;
        Value val;
        Node* left;
        Node* right;
        int N = 1;
        bool color = RED;
        int index = -1;
        // just for tree-printing
    };

    void freeNode(Node* x) {
        if (x->index < allNodes.size() - 1)  {
            allNodes[x->index].swap(allNodes.back());
            allNodes[x->index]->index = x->index;
        }
        allNodes.pop_back();
    }

    Node* newNode(Key key, Value val, int N, bool color) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;

        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        allNodes.back()->color = color;

        return allNodes.back().get();
    }

    vector<unique_ptr<Node>> allNodes;
    Node* root = NULL;

    int size (Node* x) { return (x) ? x->N: 0; }

    bool isRed(Node* x) { return (!x) ? BLACK : x->color == RED; }

    Node* put(Node* x, Key key, Value val) {
        if (!x) return newNode(key, val, 1, RED);

        if (x->key > key) x->left = put(x->left, key, val);
        else if (x->key < key) x->right = put(x->right, key, val);
        else x->val = val;

        if (isRed(x->right) && !isRed(x->left)) x = rotateLeft(x);
        if (isRed(x->left) && isRed(x->left->left)) x = rotateRight(x);
        if (isRed(x->right) && isRed(x->left))
            if (x->left->key == key || x->right->key == key)
                flipColors(x);


        x->N = size(x->left) + size(x->right) + 1;
        return x;
    }

    Node* moveRedLeft(Node* h) {
        flipColors(h);
        if (isRed(h->right->left)) {
            h->right = rotateRight(h->right);
            h = rotateLeft(h);
            flipColors(h);
        }
    }

    Node* moveRedRight(Node* h) {

    }

    Node* rotateRight(Node* h) {
        Node* x = h->left;
        h->left = x->right;
        x->right = h;
        x->color = h->color;
        h->color = RED;
        x->N = h->N;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* rotateLeft(Node* h) {
        Node* x = h->right;
        h->right = x->left;
        x->left = h;
        x->color = h->color;
        h->color = RED;
        x->N = h->N;
        h->N = size(h->right) + size(h->left) + 1;
        return x;
    }

    void flipColors(Node* x) {
        x->color = !x->color;
        x->left->color = !x->left->color;
        x->right->color = !x->right->color;
    }

    Node* min(Node* x) {
        if (!x) return NULL;
        return (!x->left) ? x : min(x->left);
    }

    Node* max(Node* x) {
        if (!x) return NULL;
        return (x->right) ? max(x->right) : x;
    }

    Node* deleteMin(Node* x) {
        if (!x) return NULL;

        if (!x->left) {
            freeNode(x);
            return NULL;
        }

        // not stepping into a 3-node, then we have to artificially create one
        if (!isRed(x->left) && !isRed(x->left->left)) {
            x = moveRedLeft(x);
        }

        x->left = deleteMin(x->left);

        x->N = size(x->left) + size(x->right) + 1;

        return balance(x);
    }

    Node* deleteMax(Node* x) {
        if (!x) return x;

        if (!x->right) {
            freeNode(x);
            return NULL;
        }

        // since it's a normal RB tree, we don't have right-leaning and thus we only check the right->left to see if it's a 3-node
        if (!isRed(x->right) && !isRed(x->right->left)) {
            x = moveRedRight(x);
        }

        x->right = deleteMax(x->right);

        x->N = size(x->left) + size(x->right) + 1;

        return x;
    }

    Node* deleteKey(Node* x, Key key) {

    }

    int heightRecursively(Node* x, int count)  {
        if (!x) return count;

        int leftHeightCount = height(x->left, count+1);
        int rightHeightCount = height(x->right, count+1);

        return (leftHeightCount > rightHeightCount) ? leftHeightCount : rightHeightCount;
    }

    bool isValid234Tree(Node* x) {
        // if till the end no 'false' condition is reached then it's true for the whole tree
        if (!x) return true;

        if (!isRed(x->left) && isRed(x->right)) return false;
        if (isRed(x->left) && isRed(x->left->left)) return false;
        if (isRed(x->left) && isRed(x->left->right)) return false;
        if (isRed(x->right) && isRed(x->right->right)) return false;
        if (isRed(x->right) && isRed(x->right->left)) return false;

        return isValid234Tree(x->left) && isValid234Tree(x->right);
    }

    bool isSubtreeCountConsistent(Node* x) {
        if (!x) return true;

        int subtreeCount = 0;
        if (x->left)
            subtreeCount += size(x->left);
        if (x->right)
            subtreeCount += size(x->right);

        return isSubtreeCountConsistent(x->left) && isSubtreeCountConsistent(x->right);
    }

    void keys(Node* x, Queue<Key>& queue, Key lo, Key hi) {
        if (!x) return;
        if (x->key < lo) return;
        if (x->key > hi) return;
        keys(x->left, queue, lo, hi);
        queue.enqueue(x->key);
        keys(x->right, queue,lo,hi);
    }

public:

    int size() { return size(root); }

    bool isEmpty() { return size() == 0; }

    void put(Key key, Value val) {
        root = put(root, key, val);
        root->color = BLACK;
    }

    void deleteMin() {
        if (!root) return;
        // we color the root RED if both children are BLACK so that we don't step into a 2-node
        if (!isRed(root->left) && !isRed(root->right)) { root->color = RED; }
        root = deleteMin(root);
        if (!isEmpty()) root->color = BLACK;
    }

    void deleteMax() {
        if (!root) return;
        if (!isRed(root->left) && !isRed(root->right)) { root->color = RED; }
        root = deleteMax(root);
        if (!isEmpty()) root->color = BLACK;
    }

    bool isValid234Tree() { return isValid234Tree(root); }

    bool isSubtreeCountConsistent() { return isSubtreeCountConsistent(root); }

    Key min() { return min(root)->key; }

    Key max() { return max(root)->key; }

    Queue<Key> keys(Key lo, Key hi) {
        Queue<Key> queue;
        keys(root, queue, lo, hi);
        return queue;
    }

    Queue<Key> keys() { return keys(min(), max()); }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);

    BottomUp234Tree<int, int> tree;
    for (int i = 0; i < N; i++) {
        tree.put(randomUniformDistribution(0, N-1),i);
        cout << ((tree.isValid234Tree()) ? "true" : "false") << endl;
    }

    cout << "Is subtree consistent? " << ((tree.isSubtreeCountConsistent()) ? "true" : "false") << endl;

    for (const auto s : tree.keys()) {
        cout << s << " ";
    }

    cout << endl;

    return 0;


}
