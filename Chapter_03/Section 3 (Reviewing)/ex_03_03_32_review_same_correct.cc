#include <iostream>
#include <memory>
#include <vector>
#include <iomanip>
#include "Random.h"
#include "Queue.h"

using namespace std;

template <typename Key, typename Value>
class AVLTree 
{
private:

// NODE OPERATIONS
	class Node 
	{
	public:
		Key key;
		Value val;
		Node* left;
		Node* right;
		int height = 0;
		int N = 1;
		int index = -1;
	};

	void freeNode(Node* x) 
	{
		if (x->index < allNodes.size()-1) 
		{
			allNodes[x->index].swap(allNodes.back());
			allNodes[x->index]->index = x->index;
		}
		allNodes.pop_back();
	}

	Node* newNode(Key key, Value val, int N, int height) 
	{
		allNodes.push_back(make_unique<Node>());
		allNodes.back()->index = allNodes.size()-1;
		allNodes.back()->key = key;
		allNodes.back()->val = val;
		allNodes.back()->N = N;
		allNodes.back()->height = height;
		return allNodes.back().get();
	}
// END OF NODE OPERATIONS

	int size(Node* h) 
	{
		return h ? h->N : 0;
	}

	int heightSafeNull(Node* h) 
	{
		return h ? h->height : 0;
	}

	int heightNullSafe(Node* h) { return !h ? 0 : h->height; }

	Node* put(Node* h, Key key, Value val) 
	{
		if (!h) return newNode(key, val, 1, 0);
		if (h->key > key) 
			h->left = put(h->left, key, val);
		else if (h->key < key) 
			h->right = put(h->right, key, val);
		else 
			h->val = val;
		h->N = size(h->left) + size(h->right) + 1;
		h->height = std::max(heightNullSafe(h->left), heightNullSafe(h->right)) + 1;
		return balance(h);
	}

	Node* deleteMin(Node* h) 
	{
		if (!h) return NULL;
		if (!h->left) return h->right;
		h->left = deleteMin(h->left);
		h->N = size(h->left) + size(h->right) + 1;
		h->height = std::max(heightSafeNull(h->left), heightSafeNull(h->right)) + 1;
		return balance(h);
	}

	Node* deleteMax(Node* h) 
	{
		if (!h) return NULL;
		if (!h->right) return h->left;
		h->right = deleteMax(h->right);
		h->N = size(h->left) + size(h->right) + 1;
		h->height = std::max(heightSafeNull(h->left), heightSafeNull(h->right)) + 1;
		return balance(h);
	}

	Node* deleteKey(Node* h, Key key) 
	{
		if (!h) 
			return NULL;
		if (h->key > key) 
			h->left = deleteKey(h->left, key);
		else if (h->key < key) 
			h->right = deleteKey(h->right, key);
		else 
		{
			Node* temp = h;
			h = min(temp->right);
			h->right = deleteMin(temp->right);
			h->left = temp->left;
			freeNode(h);
		}
		h->N = size(h->left) + size(h->right) + 1;
		h->height = std::max(heightSafeNull(h->left), heightSafeNull(h->right)) + 1;
		return balance(h);
	}

	Node* balance(Node* h) 
	{
		if (balanceFactor(h) > 1) 
		{ // left > right -> step into right -> make sure x->left->right < x->left->left
			if (balanceFactor(h->left) < 0)	// if left < right
				h->left = rotateLeft(h->left);
			h = rotateRight(h);
		}
		if (balanceFactor(h) < -1)	// x->left < x->right 
		{ // step into left -> make sure x->right->left < x->right->right
			if (balanceFactor(h->right) > 0) 
				h->right = rotateRight(h->right);
			h = rotateLeft(h);
		}
		return h;
	}

	Node* rotateLeft(Node* h) 
	{
		Node* x = h->right;
		h->right = x->left;
		x->left = h;
		x->N = h->N;
		h->N = size(h->left) + size(h->right) + 1;
		x->height = std::max(heightSafeNull(x->right), heightSafeNull(x->left)) + 1;
		h->height = std::max(heightSafeNull(h->right), heightSafeNull(h->left)) + 1;
		return x;
	}

	Node* rotateRight(Node* h) 
	{
		Node* x = h->left;
		h->left = x->right;
		x->right = h;
		x->N = h->N;
		h->N = size(h->left) + size(h->right) + 1;
		x->height = std::max(heightSafeNull(x->right), heightSafeNull(x->left)) + 1;
		h->height = std::max(heightSafeNull(h->right), heightSafeNull(h->left)) + 1;
		return x;
	}

	int balanceFactor(Node* h) 
	{
		if (!h) return 0;
		return heightNullSafe(h->left) - heightNullSafe(h->right);
	}

	Node* min(Node* h)  
	{
		if (!h) return NULL;
		return h->left ? min(h->left) : h;
	}

	Node* max(Node* h) 
	{
		if (!h) return NULL;
		return h->right ? max(h->right) : h;
	}

	// smallest larger
	Node* ceiling(Node* h, Key key) 
	{
		if (!h) 
			return NULL;
		if (h->key < key) 
			return ceiling(h->right, key);
		else if (h->key == key) 
			return h;
		Node* temp = ceiling(h->left, key);
		return temp ? temp : h;
	}

	// largest smaller
	Node* floor(Node* h, Key key) 
	{
		if (!h) 
			return NULL;
		if (h->key > key) 
			return floor(h->left, key);
		else if (h->key == key) 
			return h;
		Node* temp = floor(h->right, key);
		return temp ? temp : h;
	}

	Node* get(Node* h, Key key) 
	{
		if (!h) 
			return NULL;
		if (h->key > key) 
			return get(h->left, key);
		else if (h->key < key) 
			return get(h->right, key);
		else 
			return h;
	}

	Node* select(Node* h, int k) 
	{
		if (!h) 
			return NULL;
		int t = size(h->left);
		if (t > k) 
			return select(h->left, k);
		else if (t < k) 
			return select(h->right, k-t-1);
		else 
			return h;
	}

	int rank(Node* h, Key key) 
	{
		if (!h) return 0;
		if (h->key > key) return rank(h->left, key);
		else if (h->key < key) return 1 + size(h->left) + rank(h->right, key);
		else return size(h->left);
	}

	void keys(Node* h, Queue<Key>& queue, Key lo, Key hi) 
	{
		if (!h) return;
		if (h->key < lo) return;
		if (h->key > hi) return;
		keys(h->left, queue, lo, hi);
		queue.enqueue(h->key);
		keys(h->right, queue, lo, hi);
	}

	Node* root = NULL;
	vector<unique_ptr<Node>> allNodes;

public:

	int size() 
	{
		return size(root);
	}

	bool isEmpty() 
	{
		return (size() == 0);
	}

	int height() 
	{
		return heightSafeNull(root);
	}

	Key max() 
	{
		return max(root)->key;
	}

	Key min() 
	{
		return min(root)->key;
	}

	Value get(Key key) 
	{
		return get(root, key) ? get(root,key)->val : NULL;
	}

	Key select(int k) 
	{
		return select(root, k) ? select(root, k)->key : NULL;
	}

	void deleteMin() 
	{
		root = deleteMin(root);
	}

	void deleteMax() 
	{
		root = deleteMax(root);
	}

	void deleteKey(Key key) 
	{
		root = deleteKey(root, key);
	}

	void put(Key key, Value val) 
	{
		root = put(root, key, val);
	}

	// smallest larger
	Key ceiling(Key key) 
	{
		if (!root) 
			return NULL;
		if (key > max())
			throw logic_error("ceiling(): No ceiling for inputted key!");
		return ceiling(root, key)->key;
	}

	// largest smaller
	Key floor(Key key) 
	{
		if (!root) 
			return NULL;
		if (key < min())
			throw logic_error("floor(): No floor for inputted key!");
		return floor(root, key)->key;
	}

	Queue<Key> keys(Key lo, Key hi) 
	{
		Queue<Key> queue;
		keys(root, queue, lo, hi);
		return queue;
	}

	Queue<Key> keys() 
	{
		return keys(min(), max());
	}

	bool isSubtreeCountConsistent(Node* x) 
	{
		if (!x) return true;
		int totalSubtreeCount = 0;
		if (x->left) 
			totalSubtreeCount += size(x->left);
		if (x->right)
			totalSubtreeCount += size(x->right);
		if (totalSubtreeCount + 1 != size(x))
			return false;
		return isSubtreeCountConsistent(x->left) + isSubtreeCountConsistent(x->right);
	}

	bool isSubtreeCountConsistent() 
	{
		return isSubtreeCountConsistent(root);
	}

	bool isAVL(Node* x) 
	{
		if (!x) return true;
		if (balanceFactor(x) > 1 || balanceFactor(x) < -1) 
			return false;
		return isAVL(x->left) && isAVL(x->right);
	}

	bool isAVL() 
	{
		return isAVL(root);
	}
};

// TODO: Finish the tree height test for this implementation
// you might take a look at the guy for the test template
int main( int argc, char** argv ) 
{
	AVLTree< int, int > avl;

	cout << left << setw( 21 ) << "The data coming in: ";

	// int x;
	// while ( cin  >> x ) {
		// cout << x << " ";
		// avl.put( x, 1 );
	// }
	int n = stoi( argv[ 1 ] );
	for ( int i = 0; i < n; i++ ) {
		int data = randomUniformDistribution( 1, 12 );
		cout << data << " ";
		avl.put( data, i );
	}
	cout << endl << endl;

	cout << "Is AVL?: " << (avl.isAVL() ? "true" : "false") << endl;
	cout << "Is subtree consistent? " << (avl.isSubtreeCountConsistent() ? "true" : "false") << endl;

	cout << left << setw( 21 ) << "Tree.keys(): ";
	for ( auto s : avl.keys() ) {
		cout << s << " ";
	}
	cout << endl << endl;

	cout << left << setw( 21 ) << "deleteMin(): ";
	avl.deleteMin();
	for ( auto s : avl.keys() ) {
		cout << s << " ";
	}
	cout << endl << endl;

	cout << left << setw( 21 ) << "deleteMax(): ";
	avl.deleteMax();
	for ( auto s : avl.keys() ) {
		cout << s << " ";
	}
	cout << endl << endl;

	cout << left << setw( 21 ) << "deleteKey( 3 ): ";
	avl.deleteKey( 3 );
	for ( auto s : avl.keys() ) {
		cout << s << " ";
	}
	cout << endl << endl;

	cout << left << setw( 21 ) << "ceiling( 4 ): " << avl.ceiling( 4 ) << endl << endl;

	cout << left << setw( 21 ) << "floor( 5 ): " << avl.floor( 5 ) << endl << endl;

	cout << left << setw( 21 ) << "get( 4 ): " << avl.get( 4 ) << endl << endl;
	cout << left << setw( 21 ) << "select( 2 ): " << avl.select( 2 ) << endl << endl;

	return 0;
}

