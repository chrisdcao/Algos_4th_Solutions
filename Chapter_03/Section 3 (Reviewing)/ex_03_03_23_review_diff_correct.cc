#include <iostream>
#include <vector>
#include <numeric>
#include <iomanip>
#include <string>
#include "Queue.h"
#include "Random.h"

using namespace std;

static const bool RED = true;
static const bool BLACK = true;

template <typename Key, typename Value>
class RedBlackNoRestriction {
private:

    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        bool color;
        int N;
        int index = -1;
    };

    int heightRecursively( Node* x, int currentHeight ) {
        if ( !x ) return currentHeight;
        int leftHeight = heightRecursively( root->left, currentHeight++ );
        int rightHeight = heightRecursively( root->right, currentHeight++ );
        return ( leftHeight > rightHeight ) ? leftHeight : rightHeight;
    }

    Node* newNode( Key key, Value val, int N, bool color) {
        allNodes.push_back( make_unique< Node >() );
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        allNodes.back()->color = color;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector< unique_ptr< Node > > allNodes;
    Node* root = NULL;

    bool isRed( Node* x ) { return ( !x ) ? false : x.color == RED; }

    int size( Node* x ) { return ( !x ) ? 0 : x->N; }

    Node* min( Node* x ) { return ( !x->left ) ? x : min( x->left ); }

    Node* max( Node* x ) { return ( !x->right ) ? x : max( x->right ); }

    // SMART INSERT: insert new node with black if parent == 3-node else red link
    Node* put( Node* x, Key key, Value val, bool color = RED ) {
        if ( !x ) {
            return ( color == RED ) ? newNode( key, val, 1, BLACK )
                                    : newNode( key, val, 1, RED);
        }
        if      ( x->key > key ) x->left = put( x->left, key, val, x->color );
        else if ( x->key < key ) x->right = put( x->right, key, val, x->color );
        else                     x->val = val;
        x->N = size( x->left ) + size( x->right ) + 1;
        return x;
    }

    void keys( Node* x, Queue< Key >& queue, Key lo, Key hi ) {
        if ( !x ) return;
        if ( x->key > lo ) keys( x->left, queue, lo, hi );
        if ( x->key >= lo && x->key <= hi ) queue.enqueue( x->key );
        if ( x->key < hi ) keys( x->right, queue, lo, hi );
    }

public:

    int heightRecursive( Node* x ) { return heightRecursive( x, 0 ); }

    int size() { return size( root ); }

    bool isEmpty() { return size() == 0; }

    void put( Key key, Value val ) { 
        root = put( root, key, val ); 
        root->color = BLACK;
    }

    Key min() { return min( root )->key; }

    Key max() { return max( root )->key; }

    Queue< Key > keys( Key lo, Key hi ) {
        Queue< Key > queue;
        keys( root, queue, lo, hi );
        return queue;
    }

    Queue< Key > keys() { return keys( min(), max() ); }

};

int main( int argc, char** argv ) {

    RedBlackNoRestriction< int, int > redBlackNoRestriction;
    int N = 12;

    cout << "Data coming in: ";
    for ( int i = 0; i < N; i++ ) {
        int data = randomUniformDistribution( 0, N );
        cout << data << " ";
        redBlackNoRestriction.put( data, i );
    }
    cout << endl;

    for ( auto s : redBlackNoRestriction.keys() )
        cout << s << " ";
    cout << endl;

    return 0;

}
