#include <iostream>
#include "Queue.h"
#include <vector>
#include <memory>
#include "Random.h"

using namespace std;

// IMPLEMENTING A VERSION THAT DOES NOT USE RECURSION FOR PUT
template<typename Key, typename Value>
class SingleTopDownBST {
    static const bool RED = true;
    static const bool BLACK = false;

private:
    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        bool color;
        int N = 1;
        int index = -1;
    };

    vector<unique_ptr<Node>> allNodes;
    Node* root = NULL;

    void freeNode(Node* x) {
        if (x->index < allNodes.size()-1) {
            allNodes[x->index].swap(allNodes.back());
            allNodes[x->index]->index = x->index;
        }
        allNodes.pop_back();
    }

    Node* newNode(Key key, Value val, int N, bool color) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->N = N;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->color = color;
        return allNodes.back().get();
    }

    int size(Node* h) { return (h) ? h->N : 0; }
    // if x is null then its color is black
    bool isRed(Node* x) { return x ? x->color == RED : BLACK; }

    Node* rotateLeft(Node* h) {
        if (!h or !h->right) return h;
        Node* x = h->right;
        h->right = x->left;
        x->left = h;
        x->color = h->color;
        x->N = h->N;
        h->color = RED;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* rotateRight(Node* h) {
        if (!h or !h->left) return h;
        Node* x = h->left;
        h->left = x->right;
        x->right = h;
        x->color = h->color;
        x->N = h->N;
        h->color = RED;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* get(Node* x, Key key) {
        while (x) {
            if (x->key > key) x = x->left;
            else if (x->key < key) x =x->right;
            else return x;
        }
        return NULL;
    }

    Node* select(Node* x, int k) {
        if (!x) return NULL;
        int t = size(x->left);
        if (t > k) return select(x->left, k);
        else if (t < k) return select(x->right, k-t-1);
        else return x;
    }

    bool isSubtreeCountConsistent(Node* x) {
        if (!x) return true;

        int totalSubtreeCount = 0;
        if (x->left) {
            totalSubtreeCount += size(x->left);
        }
        if (x->right) {
            totalSubtreeCount += size(x->right);
        }
        if (x->N != totalSubtreeCount + 1) return false;

        return isSubtreeCountConsistent(x->left) && isSubtreeCountConsistent(x->right);
    }

    bool isValid234Tree(Node* x) {
        if (!x) return true;

        // The only type of 4-node allowed in the 234 tree is the one with 2 red links on 2 sides -> any other type of 'connection' is wrong
        if (isRed(x->right) && !isRed(x->left)) return false;
        if (isRed(x->left) && isRed(x->left->left)) return false;
        if (isRed(x->left) && isRed(x->left->right)) return false;
        if (isRed(x->right) && isRed(x->right->right)) return false;
        if (isRed(x->right) && isRed(x->right->left)) return false;
        
        return isValid234Tree(x->left) && isValid234Tree(x->right);
    }

    Node* min(Node* h) {
        if (!h) return NULL;

        return (h->left) ? min(h->left) : h;
    }

    Node* max(Node* h) {
        if (!h) return NULL;

        return (h->right) ? max(h->right) : h;
    }

    Node* deleteMin(Node* h) {
        if (!h) return NULL;
        if (!h->left) {
            freeNode(h);
            return NULL;
        }
        if (!isRed(h->left) && !isRed(h->left->left)) {
            h = moveRedLeft(h);
        }
        h->left = deleteMin(h->left);
        return balance(h);
    }

    void keys(Node* x, Queue<Key>& queue, Key lo, Key hi) {
        if (!x) return;
        if (x->key < lo) return;
        if (x->key > hi) return;
        keys(x->left, queue, lo, hi);
        queue.enqueue(x->key);
        keys(x->right, queue, lo, hi);
    }

    void flipColors(Node* h) {
        h->color = !h->color;
        h->left->color = !h->left->color;
        h->right->color = !h->right->color;
    }

public:

    bool isValid234Tree() { return isValid234Tree(root); }

    bool isSubtreeCountConsistent() {
        return isSubtreeCountConsistent(root);
    }

    int size() { return size(root); }
    
    bool isEmpty() { return size() == 0; }

    Value get(Key key) {
        Node* rv = get(root, key);
        return (rv) ? rv->val : NULL;
    }
    bool contains(Key key) {
        if (!key) throw runtime_error("contains(): Key is empty!");
        return get(key) != NULL;
    }

    void put(Key key, Value val) {
        if (!key || !val) return;

        if (root == NULL) {
            root = newNode(key, val, 1, BLACK);
            return;
        }

        Node* currentNode = root;
        Node* godParentNode = NULL;
        Node* grandParentNode = NULL;

        Node* parentNode = NULL;

        bool bContainsNewNode = contains(key);

        // set cái tách 4-node đầu tiên bởi luôn tách trước khi enter 
        while (currentNode) {
            if (!bContainsNewNode) currentNode->N += 1;

            // tách 4-node trước khi enter
            // bởi chúng ta cho phép tích trữ tới 2 red links 2 bên của 1 node (1 4-node) cho tới khi có node mới được thêm
            // -> chúng ta phải liên tục check liệu node đã full chưa (là 4-node) trên đường đi xuống và break nếu true
            if (isRed(currentNode->left) && isRed(currentNode->right)) {
                flipColors(currentNode);
            }

            // việc lưu trữ các 4-nodes gây ra khả năng tạo được 2 red links 1 bên ở tầng parent nối tới grand parent
            // điều đó khiến chúng ta phải điều chỉnh cả tầng parent và grandparent, và phải update kết nối sau khi hoàn thành ở tầng godparent
            if (grandParentNode) {
                if (isRed(grandParentNode->left) && isRed(grandParentNode->left->left)) {
                    grandParentNode = rotateRight(grandParentNode);
                    updateParentReference(godParentNode, grandParentNode);
                }
            }

            if (isRed(currentNode->right) && !isRed(currentNode->left)) {
                currentNode = rotateLeft(currentNode);
                updateParentReference(parentNode, currentNode);
            }

            if (isRed(currentNode->left) && isRed(currentNode->left->left)) {
                currentNode = rotateRight(currentNode);
                updateParentReference(parentNode, currentNode);
            }

            if (key < currentNode->key) {
                // if have reached the spot then assign directly
                if (!currentNode->left) {
                    currentNode->left = newNode(key, val, 1, RED);
                    break;
                }
                // everytime we climb down we have to re-assign all the relations
                godParentNode = grandParentNode;
                grandParentNode = parentNode;
                parentNode = currentNode;
                currentNode = currentNode->left;

            } else if (key > currentNode->key) {
                if (!currentNode->right) {
                    currentNode->right = newNode(key, val, 1, RED);
                    break;
                }
                godParentNode = grandParentNode;
                grandParentNode = parentNode;
                parentNode = currentNode;
                currentNode = currentNode->right;
            } else {
                currentNode->val = val;
                break;
            }
        }

        if (currentNode) {
            if (isRed(currentNode->left) && isRed(currentNode->right)) {
                flipColors(currentNode);
            } 
            if (isRed(currentNode->right) && !isRed(currentNode->left)) {
                currentNode = rotateLeft(currentNode);
                updateParentReference(parentNode, currentNode);
            }
            if (parentNode) {
                if (isRed(parentNode->left) && isRed(parentNode->left->left)) {
                    parentNode = rotateRight(parentNode);
                    updateParentReference(grandParentNode, parentNode);
                }
            }
        }

        root->color = BLACK;
    }

    Node* balance(Node* x) {
        if (!x) return NULL;
        if (!isRed(x->left) && isRed(x->right)) rotateLeft;
        if (isRed(x->left) && isRed(x->left)) rotateRight;
        if (isRed(x->left) && isRed(x->right)) flipColors(x);
        return x;
    }

    void updateParentReference(Node* parent, Node* child) {
        if (!parent) {
            root = child;
        } else {
            bool bCurrentNodeLeftChild = child->key < parent->key;
            if (bCurrentNodeLeftChild) {
                parent->left = child;
            } else {
                parent->right = child;
            }
        }
    }

    Key max() { return max(root)->key; }

    Key min() { return min(root)->key; }

    Key select(int k) { return select(root)->key; }

    Queue<Key> keys(Key lo, Key hi) {
        Queue<Key> queue;
        keys(root, queue, lo, hi);
        return queue;
    }

    Queue<Key> keys() {
        return keys(min(), max());
    }

};

int main(int argc, char** argv) {
    int N = stoi(argv[1]);

    SingleTopDownBST<int, int> tree;
    for (int i = 0; i < N; i++) {
        tree.put(randomUniformDistribution(0, N-1),i);
        cout << ((tree.isValid234Tree()) ? "true" : "false") << endl;
    }

    cout << "Is subtree consistent? " << ((tree.isSubtreeCountConsistent()) ? "true" : "false") << endl;

    for (const auto s : tree.keys()) {
        cout << s << " ";
    }
    cout << endl;

    return 0;
}
