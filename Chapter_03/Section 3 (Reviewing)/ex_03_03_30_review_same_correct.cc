#include <iostream>
#include <vector>
#include <memory>
#include <sstream>
#include "Queue.h"
#include <numeric>

using namespace std;

// TODO: implement the cache Node for put() and get() functions
template <typename Key, typename Value>
class RedBlackBST {
    static const bool RED = true;
    static const bool BLACK = false;

private:
    class Node {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        bool color = NULL;
        int N;
        int index = -1;
    };

    Node* newNode(Key key, Value val, int N, bool color) {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->color = color;
        allNodes.back()->N = N;
        return allNodes.back().get();
    }

    void freeNode(Node* x) {
        if (x->index < allNodes.size() - 1) {
            allNodes[x->index].swap(allNodes.back());
            allNodes[x->index]->index = x->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;
    Node* root = NULL;
    Node* cachedNode = NULL;

    int size(Node* x) { return x ? x->N : 0; }

    Node* put(Node* x, Key key, Value val) {
        if (!x) {
            cachedNode = newNode(key, val, 1, RED);
            return cachedNode;
        }

        if (x->key > key) x->left = put(x->left, key, val);
        else if (x->key < key) x->right = put(x->right, key, val);
        else x->val = val;

        if (!isRed(x->left) && isRed(x->right)) x = rotateLeft(x);
        if (isRed(x->left) && isRed(x->left->left)) x = rotateRight(x);
        if (isRed(x->left) && isRed(x->right)) flipColors(x);

        x->N = size(x->left) + size(x->right) + 1;

        return x;
    }

    bool isRed(Node* x) { return (!x) ? BLACK : x->color == RED; }

    int height(Node* x, int count) {
        if (!x) return count;

        int leftCount = height(x->left, count + 1);
        int rightCount = height(x->right, count + 1);

        return (leftCount > rightCount) ? leftCount : rightCount;
    }

    Node* rotateLeft(Node* h) {
        Node* x = h->right;
        h->right = x->left;
        x->left = h;
        x->color = h->color;
        x->N = h->N;
        h->color = RED;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* rotateRight(Node* h) {
        Node* x = h->left;
        h->left = x->right;
        x->right = h;
        x->color = h->color;
        h->color = RED;
        x->N = h->N;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    void flipColors(Node* h) {
        h->color = !h->color;
        if (h->left) h->left->color = !h->left->color;
        if (h->right) h->right->color = !h->right->color;
    }

    Node* deleteMin(Node* h) {
        if (!h) return NULL;
        if (!h->left) {
            freeNode(h);
            return NULL;
        }
        // not stepping into a 3-node, then we have to artificially create one
        if (!isRed(h->left) && !isRed(h->left->left)) {
            h = moveRedLeft(h);
        }
        h->left = deleteMin(h->left);
        return balance(h);
    }

    Node* deleteMax(Node* h) {
        if (!h) return NULL;
        if (isRed(h->left)) {
            h = rotateRight(h);
        }
        if (!h->right) {
            return NULL;
        }
        // the goal is to transform to on the way down to eventually have the one we delete to have RED color (and we will balance back everything on the way up)
        if (!isRed(h->right->left) && !isRed(h->right)) {
            h = moveRedRight(h);
        }
        h->right = deleteMax(h->right);
        return balance(h);
    }

    Node* moveRedLeft(Node* h) {
        // assume that node 'h' is red and h->left && h->left->left are all BLACK
        // make node left or one of its children red (just have to make the node we are about to step in a 2-node)
        flipColors(h);
        if (isRed(h->right->left)) {
            h->right = rotateRight(h->right);
            h = rotateLeft(h);
            flipColors(h);
        }
        return h;
    }

    Node* moveRedRight(Node* h) {
        // assume that node 'h' is red and h->left && h->left->left are all BLACK
        // make node left or one of its children red (just have to make the node we are about to step in a 2-node)
        flipColors(h);
        if (!isRed(h->left->left)) {
            h = rotateRight(h);
            flipColors(h);
        }
        return h;
    }

    Node* select(Node* x, int k) {
         if (!x) return NULL;

         int t = size(x->left);
         if (t > k) return select(x->left, k);
         else if (t < k) return select(x->right, k-t-1);
         else return x;
    }

    Node* floor(Node* x, Key key) {
        if (!x) return NULL;
        if (x->key > key) return floor(x->left, key);
        else if (x->key == key) return x;
        Node* t = floor(x->right, key);
        return t ? t : x;
    }

    Node* ceiling(Node* x, Key key) {
        if (!x) return NULL;
        if (x->key < key) return ceiling(x->right, key);
        else if (x->key == key) return x;
        Node* t = ceiling(x->left, key);
        return t ? t : x;
    }

    Node* balance(Node* h) {
        if (isRed(h->right))                        h = rotateLeft(h);
        if (isRed(h->left) && isRed(h->left->left)) h = rotateRight(h);
        if (isRed(h->left) && isRed(h->right))      flipColors(h);
        h->N = size(h->left) + size(h->right) + 1;
        return h;
    }

    Node* max(Node* x) { 
        if (!x) return NULL;
        return (!x->right) ? x : max(x->right); 
    }

    Node* min(Node* x) { 
        if (!x) return NULL;
        return (!x->left) ? x : min(x->left); 
    }

    Node* get(Node* x, Key key) {
        if (!x) return NULL;
        if (x->key > key) return get(x->left, key) ;
        else if (x->key < key) return get(x->right, key);
        else {
            cachedNode = x;
            return x;
        }
    }

    // so the logic of deleting key here is different from the one online: which is we just have to ensure that we are going to delete a RED (to not cause any imbalance of Black) and then re-balance the tree on the way up
    Node* deleteKey(Node* h, Key key) {
        if (!h) return NULL;
        if (h->key > key) {
            //when travelling on the left we apply the same transformation we did on the deleteMin()
            if (!h->left) return NULL;

            if (!isRed(h->left) && !isRed(h->left->left)) h = moveRedLeft(h);

            h->left = deleteKey(h->left, key);
        } else {
            // we rotateRight on our way down -> to always obtain a red node on our right (if only the left is red)
            if (isRed(h->left)) 
                h = rotateRight(h);
            
            if (key == h->key && !h->right) {
                freeNode(h);
                return NULL;
            }

            // if the above rotation (to make right node RED) fail, we are doing to do moveRedRight, which ensure we get a RED on the right side of the node
            if (!isRed(h->right) && !isRed(h->right->left)) h = moveRedRight(h);

            if (key == h->key) {
                Node* temp = h;
                h = min(temp->right);
                h->right = deleteMin(h->right);
                h->left = temp->left;
                freeNode(temp);
            } else {
                h->right = deleteKey(h->right, key);
            }
        }
        h->N = size(h->left) + size(h->right) + 1;
        return balance(h);
    }

    void keys(Node* x, Queue<Key>& queue, Key lo, Key hi) {
        if (!x) return;
        if (x->key < lo) return;
        if (x->key > hi) return;
        keys(x->left, queue, lo, hi);
        queue.enqueue(x->key);
        keys(x->right, queue, lo, hi);
    }

    int rank(Node* x, Key key) {
        if (!x) return 0;
        if (x->key > key) return rank(x->left, key);
        else if (x->key < key) return 1 + size(x->left) + rank(x->right, key);
        else return size(x->left);
    }

public:

    void printCachedNode() {
        cout << "current cached Node: ";
        cout << "(" << cachedNode->key << ", " << cachedNode->val << ")" << endl;
    }

    RedBlackBST() {}

    int height() { return height(root, -1); }

    void deleteKey(Key key) {
        if (isEmpty()) return;
        if (!isRed(root->right) && !isRed(root->left)) { root->color = RED; }
        root = deleteKey(root, key);
    }

    virtual ~RedBlackBST() {}

    Value get(Key key) { 
        if (key == cachedNode->key) return cachedNode->val;
        Node* rv = get(root, key);
        return (rv) ? rv->val : throw runtime_error("No key found");
    }

    void put(Key key, Value val) { 
        if (cachedNode)
            if (key == cachedNode->key) {
                cachedNode->val = val;
                return;
            }
        root = put(root, key, val); 
        root->color = BLACK;
    }

    int size() { return size(root); }

    bool isEmpty() { return size() == 0; }

    bool contains(Key key) { 
        return get(root, key) ;
    }

    Key min() {
        Node* rv =  min(root);
        return (rv) ? rv->val : throw runtime_error("Key not found");
    }

    Key max() {
        Node* rv = max(root);
        return (rv) ? rv->val : throw runtime_error("Key not found");
    }

    Key select(int k) {
        Node* rv = select(root, k);
        return (rv) ? rv->key : throw runtime_error("key not found");
    }

    Key floor(Key key) {
        Node* result = floor(root, key);
        return (result) ? result->val : throw runtime_error("Key not found");
    }

    Key ceiling(Key key) {
        Node* result = ceiling(root, key);
        return (result) ? result->val : throw runtime_error("Key not found");
    }

    int rank(Key key) {
        return rank(root, key);
    }

    void deleteMin() {
        if (!root) return;
        if (!isRed(root->left) && !isRed(root->right)) { root->color = RED; }
        root = deleteMin(root);
        if (!isEmpty()) root->color = BLACK;
    }

    void deleteMax() {
        if (!root) return;
        // since the moveRedRight function and the dleeteMax only works if 'h' is red (which begins from root), we have to paint root RED if either of the children is red
        if (!isRed(root->left) && !isRed(root->right)) { root->color = RED; }
        root = deleteMax(root);
        if (!isEmpty()) root->color = BLACK;
    }

    Queue<Key> keys(Key lo, Key hi) {
        Queue<Key> queue;
        keys(root, queue, lo, hi);
        return queue;
    }

    Queue<Key> keys() { return keys(min(), max()); }

};

template< typename Key, typename Value >
const bool RedBlackBST< Key, Value >::RED;

template< typename Key, typename Value >
const bool RedBlackBST< Key, Value >::BLACK;

int main( int argc, char ** argv ) {

    string keyArr[] = { "S", "E", "A", "R", "C", "H", "X", "M", "P", "L", "L", "L", "L" };
    string valueArr[] = { "S", "E", "A", "R", "C", "H", "X", "M", "P", "L", "L", "L", "L" };

    RedBlackBST< string, string > bst;
    for ( int i = 0; i < sizeof( keyArr ) / sizeof( string ); ++i ) {
        bst.put( keyArr[ i ], valueArr[ i ] );
        bst.printCachedNode();
    }

    cout << "DEBUG: bst: " << endl;
    for (auto s : bst.keys())
        cout << s << " ";
    cout << endl;

    cout << "DEUBG: bst.size(): ";
    cout << bst.size() << endl;

    cout << "DEBUG: bst.height(): ";
    cout << bst.height() << endl;

    cout << "DEBUG: bst.min(): ";
    cout << bst.min() << endl;

    cout << "DEBUG: bst.max(): ";
    cout << bst.max() << endl;

    cout << "DEBUG: bst.get( L ): ";
    cout << bst.get( "L" ) << endl;
    bst.printCachedNode();

    cout << "DEBUG: bst.get( X ): ";
    cout << bst.get( "X" ) << endl;
    bst.printCachedNode();

    cout << "DEBUG: bst.get( S ): ";
    cout << bst.get( "S" ) << endl;
    bst.printCachedNode();

    return 0;
}
